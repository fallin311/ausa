<?php

class SubscriptionController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/full';

    public function filters() {
        return array(
            'https +update',
        );
    }

    public function actionIndex($id = null) {
        if ($id == null) {
            $id = app()->user->getActiveVendor();
        }

        $vendor = Vendor::model()->with(array('plan'))->findByPk($id);
        Shared::debug("actionIndex ->" . $vendor->plan_id);
        //first check if this user has access to this particular vendor
        if (Auth::hasVendorAccess($vendor)) {
            if ($vendor->plan_id) {
                $model = new Subscription();
                $model->initSubscription($vendor);
                $this->render('subscription_info', array(
                    'model' => $model,
                    'vendor' => $vendor,
                    'plan' => $vendor->plan,
                ));
            } else {
                $this->redirect(url('/subscription/plan/' . $id));
            }
        } else {
            Yii::app()->user->loginRequired();
        }
    }

    /**
     * Cancel recurring subscription on the payment gateway
     */
    public function actionUnsubscribe($id = null) {
        if ($id == null) {
            $id = app()->user->getActiveVendor();
        }
        $vendor = Vendor::model()->with(array('users', 'plan'))->findByPk($id);

        if (Auth::hasVendorAccess($vendor)) {
            $subscription = new Subscription();
            $subscription->initSubscription($vendor);

            Shared::debug($_POST);

            if (isset($_POST['unsubscribe'])) {
                $subscription->cancelSubscription($vendor);
                app()->user->setFlash('success', 'Your recurring payment has been cancelled.');
                app()->user->redirectHome();
            }

            if ($subscription->currentlyRecurring) {
                $this->render('unsubscribe', array(
                    'model' => $vendor
                ));
            } else {
                app()->user->setFlash('error', 'There is no recurring payment set.');
                app()->user->redirectHome();
            }
        } else {
            Yii::app()->user->loginRequired();
        }
    }

    public function actionPlan($id = null, $p = null) {
        if ($id == null) {
            $id = app()->user->getActiveVendor();
        }
        $vendor = Vendor::model()->with(array('users', 'plan'))->findByPk($id);
        //Shared::debug("actionIndex ->".$vendor->plan_id);

        if (Auth::hasVendorAccess($vendor)) {
            if ($p != null) {
                $plan = Plan::model()->findByPk((int) $p);
                if (is_object($plan) && $plan->hasAccess()) {
                    $vendor->plan_id = $plan->plan_id;
                    $vendor->save(false, array('plan_id'));
                }
            }

            if ($vendor->plan_id && !app()->user->isAdmin()) {
                $this->redirect(url('/subscription/index/' . $id));
            }
            if (app()->user->isAdmin()) {
                $criteria = new CDbCriteria;
                $criteria->condition = 'active = 1';
                $availablePlans = Plan::model()->findAll($criteria);
            } else if (app()->user->isSalesman()) {
                $criteria = new CDbCriteria;
                $criteria->condition = 'active = 1 AND (access_level="public" OR access_level="st")';
                $availablePlans = Plan::model()->findAll($criteria);
            } else {
                $criteria = new CDbCriteria;
                $criteria->condition = 'active = 1 AND access_level="public"';
                $availablePlans = Plan::model()->findAll($criteria);
            };

            $this->render('select_plan', array(
                'vendor' => $vendor,
                'availablePlans' => $availablePlans,
            ));
        } else {
            Yii::app()->user->loginRequired();
        }
    }

    public function actionUpdate($id, $p = 2) {

        $vendor = Vendor::model()->with('users', 'plan')->findByPk($id);
        //Shared::debug("within actionUpdate");
        //first check if this user has access to this particular vendor
        if (Auth::hasVendorAccess($vendor)) {
            $model = new Subscription();

            Shared::debug($vendor->plan);
            if (!$vendor->plan) { //Todo: Need to revisit this. Can a vendor have pg_id and not plan_id? or have plan_id and not pg_id?
                Shared::debug("Could not find vendor->plan");
                $vendor->plan_id = (int) $p;
                $vendor->plan = Plan::model()->findByPk($vendor->plan_id);
                Shared::debug($vendor->plan);
            }

            $model->initSubscription($vendor);
            //Shared::debug("has vendor access");
            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation($model);
            Shared::debug($_REQUEST);

            if (isset($_POST['Subscription'])) {
                Shared::debug($_POST);
                $model->attributes = $_POST['Subscription'];
                // Shared::debug($model->attributes);
                $model->validate();
                // Shared::debug("post subsciption is set");

                if (!$model->hasErrors()) {
                    //Shared::debug("it is valid");
                    // disable validation when saving
                    Shared::debug("Run saving methods here.. ");
                    $model->save($vendor);

                    //Shared::debug($model);

                    $vendor->max_offers = $model->maxOffers;
                    $vendor->max_banners = $model->maxBanners;
                    $vendor->max_branches = $model->maxBranches;
                    $vendor->max_categories = $model->maxCategories;
                    if($vendor->plan->free_duration > 0){
                        $days = $vendor->plan->free_duration;
                    }else{
                        $days = 33;
                    }
                    $vendor->expiration = date("Y-m-d", strtotime(date("Y-m-d") . ' +'. $days));
                    //Shared::debug($vendor->max_categories);
                    $vendor->updated_on = date("Y-m-d H:i:s");
                    $vendor->save(false);


                    if (isset($_POST['isAjaxRequest'])) {
                        app()->user->setFlash('success', 'Subscription changes have been saved.');
                        echo $vendor->vendor_id;
                        app()->end();
                    } else {
                        $this->redirect(array('dashboard'));
                    }
                } else {
                    //Shared::debug("model has errors");
                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
            }

            $this->render('update', array(
                'model' => $model,
                'vendor' => $vendor,
                'plan' => $vendor->plan,
            ));
        } else {
            Yii::app()->user->loginRequired();
        }
    }

    public function actionUpdateBilling($id) {
        $vendor = Vendor::model()->findByPk($id);
        //Shared::debug("within actionUpdateBilling");
        //first check if this user has access to this particular vendor
        if (Auth::hasVendorAccess($vendor)) {
            $model = new Subscription();
            $model->initSubscription($vendor);
            //Shared::debug($model);
            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation($model);

            if (isset($_POST['Subscription'])) {
                Shared::debug($_POST);
                $model->attributes = $_POST['Subscription'];
                // Shared::debug($model->attributes);
                $model->validate();
                Shared::debug($model->getErrors());
                // Shared::debug("post subsciption is set");

                if (!$model->hasErrors()) {
                    //Shared::debug("it is valid");
                    // disable validation when saving
                    Shared::debug("Run SOAP methods here to update Customer Payment Method.. ");
                    Shared::debug($model);
                    //Save Vendor here first TODO
                    //Execute SOAP function here..
                    //$model->quickUpdatePaymentMethod($vendor);
                    $model->save($vendor);
                    app()->user->setFlash('success', 'Payment information has been saved.');
                    echo "1";
                    app()->end();
                } else {
                    Shared::debug("model has errors");
                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
            }


            $this->render('update_billing', array(
                'model' => $model,
                'vendor' => $vendor,
            ));
        } else {
            Yii::app()->user->loginRequired();
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model = null) {
        //Shared::debug($_POST);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'billing-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}

//class
?>

<?php

/**
 * This controller is used to access app data from mobile phone application
 * All responses should be JSONp encoded.
 */
class ApiV2Controller extends Controller {

    public function actionLogin($login, $pin, $chapter = null) {
        $member = Member::apiAuthenticate(urldecode($login), (int) $pin);
        $response = array(
            'success' => 0,
            'errorMessage' => ''
        );
        //Shared::debug($login, $pin);

        if ($member == null) {
            // nope, it is still possible that it is a vendor user
            $user = User::model()->findByAttributes(array('mobile_username' => $login, 'mobile_password' => $pin));
            //Shared::debug($user);
            if ($user == null) {
                $response['errorMessage'] = 'We cannot recognize your login or access PIN.';
            } else {
                if ($chapter == null) {
                    // lookup available chapters
                    $chapters = $user->getAccessibleChapterIds($user);
                    //Shared::debug($chapters);
                    if (count($chapters) > 1) {
                        $response = array(
                            'success' => 1,
                            'chapters' => $chapters
                        );
                    } else if (count($chapters) == 1) {
                        $response['member'] = $user->getInfoForApi(Chapter::model()->findByPk($chapters[0]));
                        $response['success'] = 1;
                    }
                } else {
                    // the chapter is already selected
                    $_chapter = Chapter::model()->findByPk($chapter);
                    if (is_object($_chapter)) {
                        $response['member'] = $user->getInfoForApi($_chapter);
                        $response['success'] = 1;
                    } else {
                        $response['errorMessage'] = 'This chapter is currently not available.';
                    }
                }
            }
        } else {
            $memberships = $member->memberships;
            if (count($memberships) == 1) {
                $membership = $memberships[0];
            } else if (count($memberships) > 0) {
                // look for active memberships, it is possible, there is only one active
                $active = array();
                foreach ($memberships as $_membership) {
                    if ($membership->active)
                        $active[] = $_membership;
                }
                if (count($active) == 1) {
                    $membership = $active[0];
                } else {
                    // there are more options. Inform the member and let him choose.
                    // this output has to be the same as it is for user
                    $chapters = array();
                    foreach ($active as $_membership) {
                        $chapters[] = array(
                            'chapter_id' => $_membership->chapter_id,
                            'city' => $_membership->chapter->city,
                            'chapter_name' => $_membership->chapter->chapter_name);
                    }
                    $response = array(
                        'success' => 1,
                        'chapters' => $chapters
                    );
                }
            }
            // one chapter
            if ($membership) {
                // check expiration
                if ($membership->isActive()) {
                    // we should check for linebreaks ... any field with line break will fuck it up
                    $response['member'] = $member->getInfoForApi(true, $membership);
                    $response['success'] = 1;
                    //Shared::debug($response);
                } else {
                    $response['errorMessage'] = 'Your AUSA membership has expired. Please contact your chapter.';
                }
            } else {
                $response['errorMessage'] = 'Your account has been disabled. Please contact your chapter.';
            }
        }
        //Shared::debug($response);
        $this->jsonify($response);
    }

    /**
     * The function changes pin and sends email only if the member used email
     * address as login information.
     * @param type $login
     */
    public function actionNewPin($login, $newEmail = null) {
        //Shared::debug("reset password $login, $newEmail");
        $response = Member::forgotPin($login, $newEmail);
        //Shared::debug($response);
        $this->jsonify($response);
    }

    /**
     * Returns all the necessary information used inside the cellphone
     * @param string $a authentication token
     * @param integer $v version (date, load only items newer than this unix timestamp)
     * @param integer $w width of the screen
     * @param integer $ch chapter id
     */
    public function actionGetData($a, $v = null, $w = 240, $ch = null) {
        //Shared::debug($ch, 'chapter');
        $member = Member::model()->byToken($a)->find();

        // looking for standard member object, not user
        if (is_object($member)) {
            //Shared::debug("loading data for user " . $member->email_address);
            // start loading one by one, we can skip cache for now
            if ($v != null && $v) {
                // make it one minute less just in case
                $version = Shared::toDatabase(((int) $v) - 60);
                //Shared::debug("Loading data from $version");
            } else {
                //Shared::debug("No version data available");
                $version = null;
            }

            // detect active chapter
            if ($ch == null) {
                $chapterId = $member->membership->chapter_id;
            } else {
                $chapterId = $ch;
            }

            // get list of all offers first (might be cached)
            $allOffers = Offer::getApiList($chapterId, $version);

            // merge it with member data
            $memberOffers = MemberOffer::getMemberOffers($member);
            //Shared::debug($memberOffers);
            foreach ($memberOffers as $offer) {
                if (isset($allOffers[$offer->offer_id])) {
                    $allOffers[$offer->offer_id]['favorite'] = $offer->favorite + 0;
                    $allOffers[$offer->offer_id]['redemptions'] = array();
                    foreach ($offer->redemptions as $redemption) {
                        $allOffers[$offer->offer_id]['redemptions'][] = array('date' => strtotime($redemption->redemption_activity) * 1000);
                    }
                }
            }

            // get list of all vendors
            $allVendors = Vendor::getApiList($chapterId, $version);

            // and merge it with list of favorites
            $favs = FavoriteStore::model()->findAllByAttributes(array('member_id' => $member->member_id));
            foreach ($favs as $fav) {
                if (isset($allVendors[$fav->vendor_id])) {
                    $allVendors[$fav->vendor_id]['favorite'] = 1;
                }
            }

            $output = array(
                'vendors' => array_merge($allVendors), // remove the array keys
                'offers' => array_merge($allOffers),
                'banners' => Banner::getApiList($chapterId, $version, $w),
                'categories' => Category::getApiList($version),
                'success' => 1,
                'timestamp' => time() . ''
            );
            //Shared::debug($output);
            $this->jsonify($output);
        } else {
            // it might be a user object
            $user = User::model()->findByAttributes(array('access_token' => $a));
            if (is_object($user)) {
                if ($v != null && $v) {
                    // make it one minute less just in case
                    $version = Shared::toDatabase(((int) $v) - 60);
                    //Shared::debug("Loading data from $version");
                } else {
                    //Shared::debug("No version data available");
                    $version = null;
                }

                // detect active chapter
                if ($ch == null) {
                    $chapters = $user->getAccessibleChapterIds($user);
                    if (count($chapters)) {
                        $chapterId = $chapters[0]['chapter_id'];
                    } else {
                        $this->jsonify(array(
                            'success' => 0,
                            'errorMessage' => 'Please select differenct chapter.'
                        ));
                    }
                } else {
                    $chapterId = $ch;
                }

                $output = array(
                    'vendors' => array_merge(Vendor::getApiList($chapterId, $version, $user)), // remove the array keys
                    'offers' => array_merge(Offer::getApiList($chapterId, $version, $user)),
                    'banners' => Banner::getApiList($chapterId, $version, $w, $user),
                    'categories' => Category::getApiList($version),
                    'success' => 1,
                    'timestamp' => time() . ''
                );

                //Shared::debug($output);
                $this->jsonify($output);
                app()->end();
            }

            //Shared::debug($a, 'not found');
            $this->jsonify(array(
                'success' => 0,
                'errorMessage' => 'Could not authenticate, please try to log in again.'
            ));
        }
    }

    /**
     * Stores data from the cellphone to server. Synchronizes viewed / redeemed offers
     * the request has to come from POST JSON request and has the following structure:
     * 
     * banners: [
     *   {
     *     id: 443, // banner_id
     *     clicks: 1, // number of clicks
     *     impressions: 10, // number of impressions
     *   }, ...
     * ],
     * offers: [
     *   {
     *     id: 112, // offer_id
     *     favorite: 0, // binnary 0 or 1 to mark favorite offer
     *     clicks: [
     *       {
     *          date: '1434814144', // unix timestamp
     *          lat: '31.16584134', // optional latitude
     *          lon: '108.36848684' // optional longitude
     *       }, ...
     *     ],
     *     redemptions: [
     *       {
     *          date: '143818644', // unix timestamp
     *          lat: '31.16584134', // optional latitude
     *          lon: '108.36848684', // optional longitude
     *          redemptionType: 'swipe', // optional type (swipe, qr, email, print)
     *          ticketValue: '23.49', // dollar value of the ticket after the discount was applied. Optional
     *      }, ...
     *     ]
     * ],
     * vendors: [
     *      {
     *          id: 223,
     *          favorite: 0 // or 1
     *      }
     * ]
     * 
     * @param type $a authentication token
     * @param type $v version from which we want to download the data
     */
    public function actionSyncData($a) {
        // allow access from everywhere, request is just regular ajax
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        //Shared::debug($_POST);
        //Shared::debug($_GET);

        $member = Member::model()->byToken($a)->find();
        if (is_object($member) && isset($_POST['json'])) {
            //Shared::debug($_POST);
            $data = json_decode($_POST['json'], true);
            //Shared::debug($data);

            // throws error: Cannot execute queries while other unbuffered queries are active
            // there is no time to fix it now, but it has to do something with prepared statements
            //Yii::app()->db->createCommand("LOCK TABLES offer WRITE, banner WRITE")->execute();

            if (isset($data['offers'])) {
                foreach ($data['offers'] as $offerData) {
                    $model = Offer::model()->findByPk($offerData['id']);
                    if ($model != null) {
                        $model->syncData($member, $model, $offerData);
                    }
                }
            }

            if (isset($data['banners'])) {
                foreach ($data['banners'] as $bannerData) {
                    $model = Banner::model()->findByPk($bannerData['id']);
                    if (is_object($model)) {
                        $model->syncData($bannerData);
                        $model->save();
                    }
                }
            }

            //Yii::app()->db->createCommand("UNLOCK TABLES")->execute();
            // create list of favorite stores
            if (isset($data['vendors'])) {
                FavoriteStore::syncData($member, $data['vendors']);
            }

            $this->jsonify(array(
                'success' => 1
            ));
        } else {
            //Shared::debug($a, 'not found');
            $this->jsonify(array(
                'success' => 0,
                'errorMessage' => 'Could not authenticate, please try to log in again.'
            ));
        }
    }

    public function actionGetDashBanner($a) {
        $member = Member::model()->byToken($a)->find();
        $output = array(
            'success' => 0
        );
        if (is_object($member)) {
            $chapterId = $member->membership->chapter_id;

            // now we have to find published banners from that chapter for today
            $sql = "SELECT banner_id, banner_name, UNIX_TIMESTAMP(DATE_ADD(b.end_date, INTERVAL 1 DAY)) AS 'end_date' ,
                panorama_image, target, external_url, offer_id
                FROM banner b
                LEFT JOIN vendor v ON b.vendor_id = v.vendor_id
                LEFT JOIN vendor_membership m ON v.vendor_id = m.vendor_id
                WHERE b.dashboard=1 AND m.chapter_id=$chapterId AND b.published=1 AND v.disabled=0 AND v.published=1
                AND b.start_date < NOW() AND DATE_ADD(b.end_date, INTERVAL 1 DAY) > NOW()";
            //::debug($sql);
            $result = app()->db->createCommand($sql)->queryAll();
            if (count($result) > 0) {
                $output['success'] = 1;
                $output['id'] = $result[0]['banner_id'];
                $output['endDate'] = $result[0]['end_date'];
                $output['offerId'] = $result[0]['offer_id'];
                $output['target'] = $result[0]['target'];
                $output['externalUrl'] = $result[0]['external_url'];
                $output['imgUrl'] = app()->params['staticUrl'] . '/image/display/' . $result[0]['panorama_image'];
            }
        }
        //Shared::debug($output);

        $this->jsonify($output);
    }

    /**
     * Return json / jsonp encoded result
     * @param type $response
     */
    private function jsonify($response) {

        if (isset($_REQUEST['callback'])) {
            // JSONP
            header('content-type: application/javascript; charset=utf-8');
            echo $_REQUEST['callback'] . '(' . json_encode($response) . ')';
        } else {
            // REST
            header('content-type: application/json; charset=utf-8');
            echo json_encode($response);
        }
    }

}

?>

<?php

/**
 * Manage Members and Member upload
 * @author Ondrej Nebesky <ondrej@inradiussystems.com>
 */
class MemberController extends Controller {

    // default one column layout
    public $layout = '//layouts/full';

    public function filters() {
        return array(
            'https +register',
        );
    }
    
    /**
     * Renders list of users for admin and search form for chapter user
     */
    public function actionIndex() {
        if (app()->user->isAdmin() || app()->user->isChapterUser()) {

            // setup a model for ajax grid filtering
            $model = new Member('search');
            if (isset($_GET['Member'])) {
                //Shared::debug($_GET['Member']);
                $model->attributes = $_GET['Member'];
            }

            if (app()->user->isChapterUser()) {
                // If it's a chapter user, take them to the unique Chapter Members page
                if(app()->user->getFlash('success')){
                    app()->user->setFlash('success', 'Member changes have been saved.');
                }
                $this->redirect(url('chapter/members'));
            }
            $this->render('index', array('model' => $model));
        } else {
            app()->user->loginRequired();
        }
    }

    /**
     * Members should not be deleted from the database. However, you can
     * always use workbench to do so.
     * @throws CException
     */
    public function actionDelete() {
        // not implemented yet...
        throw new CException("not implemented yet...");
    }

    /**
     * Create new model through a form.
     */
    public function actionCreate() {
        Yii::import('application.models.behaviors.UploadMemberBehavior');
        $model = new Member;
        $model->attachBehavior('uploadMember', new UploadMemberBehavior());
        
        // load default values except expiration date (member registration sets that after payment process)
        $model->loadDefaults();
        $model->source = 'internal';
        
        $membership = $model->getTempMembership();
        //$model->setExpirationDate(strtotime(Shared::toDatabase(time()) . " +12 month"));
        $membership->expiration_date = date('Y-m-d', strtotime(Shared::toDatabase(time()) . " +12 month"));

        if (app()->user->isAdmin() || app()->user->isChapterUser()) {
            $renderArray = array('model' => $model, 'membership' => $membership);
            if (app()->user->isChapterUser()) {
                // include chapter info for dashboard
                $chapterModel = Chapter::model()->findByPk(app()->user->getActiveChapter());
                $renderArray = array('model' => $model, 'membership' => $membership, 'chmodel' => $chapterModel);
            }

            $this->performAjaxValidation($model, $membership);

            if (isset($_POST['Member']) && isset($_POST['Membership'])) {
                Shared::debug($_POST);
                
                // validate and set the right format of all values
                $model->setAttributesFromForm($_POST);
                
                // virtual attribute in UploadMemberBehavior
                if (isset($_POST['Member']['sendRegistrationEmail'])){
                    $model->sendRegistrationEmail = true;
                }

                // set current timestamp, important for caching
                $model->updated_on = Shared::timeNow();
                if ($membership->active){
                    $res = $model->activateMembership();
                } else {
                    $res = $model->saveMembership();
                }
                if ($res) {
                    // ajax request is handled through javascript
                    if (isset($_POST['isAjaxRequest'])) {
                        echo $model->member_id;
                        app()->end();
                    } else {
                        $this->redirect(array('/member/update' . $model->member_id));
                    }
                } else {
                    Shared::debug($model->getErrors());
                    $membership = $model->getTempMembership();
                    // validation issue, just display results
                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        foreach ($membership->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
            }
            $this->render('create', $renderArray);
        } else {
            app()->user->loginRequired();
        }
    }

    /**
     * Update one member
     * @param type $id Member ID
     */
    public function actionUpdate($id) {
        Yii::import('application.models.behaviors.UploadMemberBehavior');
        $model = Member::model()->findByPk($id);
        $model->attachBehavior('uploadMember', new UploadMemberBehavior());
        if ($model != null && (app()->user->isAdmin() || app()->user->isChapterUser())) {
            // temporary solution considers only one membership per member
            $memberships = $model->memberships;
            if (count($memberships)) {
                // load existing membership and pass it for further valdiation
                $membership = $memberships[0];
                $model->setMembership($membership);
            } else {
                // this user is not a member of this chapter yet
                $membership = $model->getTempMembership();
            }
            $this->performAjaxValidation($model, $membership);

            if (isset($_POST['Member']) && isset($_POST['Membership'])) {
                $model->setAttributesFromForm($_POST);
                $model->updated_on = Shared::timeNow();

                if ($model->saveMembership()) {
                    if (isset($_POST['isAjaxRequest'])) {
                        app()->user->setFlash('success', 'Member changes have been saved.');
                        echo $model->member_id;
                        app()->end();
                    } else {
                        $this->redirect(array('/member'));
                    }
                } else {
                    $membership = $model->getTempMembership();
                    // validation issue
                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        foreach ($membership->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
            }
            $this->render('update', array('model' => $model,
                'membership' => $membership));
        } else {
            app()->user->loginRequired();
        }
    }

    /**
     * Display a form for mass upload of members. Function actionUploadCsv is the ajax upload handler.
     */
    public function actionUpload() {
        if (app()->user->isAdmin() || app()->user->isChapterUser()) {
                $this->render('upload');
        } else {
            app()->user->loginRequired();
        }
    }

    /**
     * File upload handler
     */
    public function actionUploadCsv() {
        if (app()->user->isAdmin() || app()->user->isChapterUser()) {
            // upload component validates the file at this point
            // and process by chucks through cron job later on
            Yii::import('application.components.UploadComponent');

            $allowedExtensions = array("csv");
            // max file size in bytes - 10MB
            $sizeLimit = 10 * 1024 * 1024;

            $uploader = new UploadComponent($allowedExtensions, $sizeLimit);
            $result = $uploader->handleUpload();
            //Shared::debug($result);
            if ($result['success'] == 1) {
                //Shared::debug("uploaded");
                // create the cron job
                $info = MemberUpload::getCsvFileInfo($result['path']);
                $job = CronJob::createUploadJob($info, $result['path']);
                if ($job->hasErrors()) {
                    Shared::debug($job->getErrors());
                    $result['success'] = 0;
                }
            }
            // upload is initiated by AJAX, let it know the results
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model, $membership = null) {
        //Shared::debug($_POST);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'member-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if ($membership != null && isset($_POST['ajax']) && $_POST['ajax'] === 'membership-form') {
            echo CActiveForm::validate($membership);
            Yii::app()->end();
        }
    }

    /**
     * Register a member from the public site. 
     */
    public function actionRegister() {
        //Yii::import('application.models.behaviors.UploadMemberBehavior');
        //Yii::import('application.models.behaviors.ParamsBehavior');
        $model = new Member('register');
        $model->attachBehavior('uploadMember', new UploadMemberBehavior());
        $model->attachBehavior('params', new ParamsBehavior());

        $model->loadDefaults();
        $membership = $model->getTempMembership();
        $payment = new MembershipPayment();
        $payment->loadDefaults($model);

        //$this->performAjaxValidation($model, $membership);

        if (isset($_POST['Member']) && isset($_POST['Membership'])) {
            Shared::debug($_POST);
            // validate and set the right format of all values
            $model->setAttributesFromForm($_POST);

            // set current timestamp, important for caching
            $model->updated_on = Shared::timeNow();

            // search for conflicts - notice that we don't search for phone number
            // conflicts since there is no way how to deliver new pin without email address
            $member = Member::findByWhatever($model->email_address);
            if (is_object($member)) {
                $member->attachBehavior('uploadMember', new UploadMemberBehavior());
                $member->attachBehavior('params', new ParamsBehavior());
                $membership = $member->findMembership($_POST['Membership']['chapter_id']);
                Shared::debug("Member exists");
                if ($member->activated) {
                    // nope, you cannot do it, you have to login and change your information
                    $model->addError('email_address', 'Our records indicate that you are already a member. Please use your email address ' . CHtml::encode($member->email_address) . ' to login in our phone app. Use &quot;Forgot Pin&quot; feature if you did not recieve your PIN.');
                } else {
                    // you can proceed, but we have to update the existing record
                    $member->setAttributesFromForm($_POST);
                    $member->updated_on = Shared::timeNow();
                    $model = $member;
                }
            }

            //$payment->member = $model;
            $payment->attributes = $_POST['MembershipPayment'];
            $payment->prepare($model);
            $model->payment = $payment;

            if (!$model->hasErrors() && $model->validateMembership() && $payment->validate()) {
                //if () {
                    if (isset($_POST['final'])) {
                        // process the payment
                        $result = $payment->process();
                        if ($result['success']) {
                            // activate the member
                            if ($model->activateMembership(true)) {
                                $response = array(
                                    'action' => 'success',
                                    'confirm' => $this->renderPartial('_register_success', array('model' => $model, 'membership' => $membership, 'payment' => $payment), true)
                                );
                            } else {
                                $response = array(
                                    'action' => 'register',
                                    'error' => '<ul><li>We could not activate your account. Please send us a message using Contact Us form.</li></ul>'
                                );
                            }
                            echo json_encode($response);
                            app()->end();
                        } else {
                            // probably credit card problem
                            $payment->addError('cardNumber', $result['error']);
                        }
                    } else {
                        // save member to database, display next step
                        $model->activated = 0;
                        $model->setParameter('payment', $_POST['MembershipPayment']);
                        $model->saveMembership();

                        $response = array(
                            'action' => 'confirm',
                            'confirm' => $this->renderPartial('_register_confirm', array('model' => $model, 'membership' => $membership, 'payment' => $payment), true)
                        );

                        echo json_encode($response);
                        app()->end();
                    }
                //}
            }

            // there was a validation problem
            $membership = $model->getTempMembership();
            // validation issue, just display results
            if (isset($_POST['isAjaxRequest'])) {
                CActiveForm::validate(array($model,$membership));
                $errors = '<ul>';
                foreach ($model->getErrors() as $error) {
                    $errors .= '<li>' . $error[0] . '</li>';
                }
                foreach ($membership->getErrors() as $error) {
                    $errors .= '<li>' . $error[0] . '</li>';
                }
                foreach ($payment->getErrors() as $error) {
                    $errors .= '<li>' . $error[0] . '</li>';
                }
                $errors .= '</ul>';
                $response = array(
                    'action' => 'register',
                    'errors' => $errors
                );
                echo json_encode($response);
                app()->end();
            }
        }
        $this->render('register', array('model' => $model, 'membership' => $membership, 'payment' => $payment));
    }

}
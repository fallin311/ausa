<?php

class BranchController extends Controller
{
	/*public function actionCreate()
	{
		$this->render('index');
	}*/

        public function actionUpdate($id = null)
	{
            if ($id == null && isset($_POST['Branch']['branch_id'])){
                $id = $_POST['Branch']['branch_id'];
                unset($_POST['Branch']['branch_id']);
            }
            if (is_numeric($id)){
                $model = Branch::model()->findByPk($id);
            }else{
                $model = new Branch;
            }
            if (Auth::hasVendorAccess($model) || ($model->isNewRecord && (app()->user->getActiveVendor()) || app()->user->isAdmin())){
                
                $this->performAjaxValidation($model);
                
                if (isset($_POST['Branch'])){
                    // prevent changing vendors
                    if (isset($_POST['Branch']['vendor_id']) && !$model->isNewRecord){
                        unset($_POST['Branch']['vendor_id']);
                    }
                    
                    // save the changes
                    $model->attributes = $_POST['Branch'];
                    Shared::debug($model->attributes);
                    if (isset($_POST['ajax']) || $_POST['submit']){
                        if ($model->save()){
                            echo "true";
                        }else{
                            Shared::debug($model->getErrors());
                            // just get the formatted errors
                            $output = '<ul>';
                            foreach ($model->getErrors() as $error){
                                $output .= "<li>" . $error[0] . "</li>";
                            }
                            echo $output . "</ul>";
                        }
                        app()->end();
                    }else{
                        // regular save
                        $model->save();
                    }
                }
                Shared::debug("rendering branch update");
                $this->render('_form', array('model' => $model, 'vendor' => $model->vendor));
            }
	}
        
        public function actionUpdateJson($id){
            Shared::debug($id);
            $model = Branch::model()->findByPk($id);
            $output = array(
                'success' => 0
            );
            if (Auth::hasVendorAccess($model)){
                $output['success'] = 1;
                $output['data'] = $model->attributes;
            }
            Shared::debug($output);
            header('content-type: application/json; charset=utf-8');
            echo json_encode($output);
        }
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
        
        /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        //Shared::debug($_POST);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'branch-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
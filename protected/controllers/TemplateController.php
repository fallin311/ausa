<?php

class TemplateController extends Controller
{
    public $layout = '//layouts/full';
    
    public function actionIndex(){
        if (app()->user->isAdmin()) {
            $dataProvider = new CActiveDataProvider('OfferTemplate');
            //$dataProvider = Vendor::model()->getDataProvider();
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            Yii::app()->user->loginRequired();
        }
    }
    
    public function actionCreate(){
        if (app()->user->isAdmin()) {
            $model = new OfferTemplate;
            if (isset($_POST['OfferTemplate'])) {
                Shared::debug($_POST['OfferTemplate']);
                $model->attributes = $_POST['OfferTemplate'];
                if ($model->save())
                    $this->redirect(array('index'));
            }

            $this->render('create', array(
                'model' => $model,
            ));
        } else {
            app()->user->loginRequired();
        }
    }
    
    public function actionUpdate($id){
        if (app()->user->isAdmin()) {
            $model = OfferTemplate::model()->findByPk($id);
            if (isset($_POST['OfferTemplate'])) {

                $model->attributes = $_POST['OfferTemplate'];
                if ($model->save())
                    $this->redirect(array('index'));
            }

            $this->render('update', array(
                'model' => $model,
            ));
        } else {
            app()->user->loginRequired();
        }
    }
}
?>

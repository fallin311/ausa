<?php

/**
 * Dashboard banner is displayed only one at the time and in general
 * these actions are available for admins only. 
 */
class DashBannerController extends Controller {

    public $layout = '//layouts/full';

    /**
     * List of active / expired banners visible for the vendor.
     * Display warning page when the vendor does not have banners active.
     */
    public function actionIndex() {
        $chapter = app()->user->getActiveChapter();
        // if user was redirected from dashBanner in order to select a chapter,
        // send them back to dashBanner after setting that chapter - t.s.
        $setChapter = app()->request->getParam('postChapter');

        // for admins only
        if (!app()->user->isAdmin()) {
            app()->user->redirectHome();
        }

        if ($chapter) {
            $model = Chapter::model()->findByPk($chapter);

            $dataProvider = new CActiveDataProvider('Banner', array(
                'criteria' => array(
                    // vendor has to be member of chapter
                    'condition' => 'dashboard=1',
                    'with' => array('vendor'),
                ),
                    ));
            $this->render('//banner/dashboard/index', array('chapter' => $model, 'dataProvider' => $dataProvider));
        } elseif($setChapter) {
            $chapter = Chapter::model()->findByPk($setChapter);
            if (Auth::hasChapterAccess($chapter)) {
                app()->user->setActiveChapter($setChapter);
            }
            $this->redirect(url('/dashBanner'));
        } else {
            if (app()->user->isGuest) {
                // non logged in user has to sign in
                app()->user->loginRequired();
            } else {
                app()->user->setFlash('setChapter', 'Please select a chapter first.');
                $this->redirect(url('/chapter/index'));
            }
        }
    }

    public function actionCreate() {

        $chapter = app()->user->getActiveChapter();

        // for admins only
        if (!app()->user->isAdmin()) {
            app()->user->redirectHome();
        }
        if ($chapter) {
            $model = new Banner;
            //$model->loadDefaults();
            // preload default values

            $model->start_date = date('Y-m-d', time());
            $model->end_date = $model->start_date;
            $model->target = 'external';
            $model->layout = 'image';
            $model->dashboard = 1;

            $vendor = Vendor::model()->findByPk(app()->user->getActiveVendor());

            $this->performAjaxValidation($model);
            Shared::debug($_POST);
            if (isset($_POST['Banner'])) {
                Shared::debug($_POST);
                $model = Banner::create($_POST['Banner'], $model);
                Shared::debug($model);

                if ($model->save()) {
                    if (isset($_POST['isAjaxRequest'])) {
                        echo $model->banner_id;
                        app()->end();
                    } else {
                        $this->redirect(array('/banner/dashboard/update' . $model->banner_id));
                    }
                } else {
                    // validation issue
                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }

                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
            }

            $this->render('//banner/dashboard/create', array(
                'model' => $model,
                'vendor' => $vendor));
        } else {
            if (app()->user->isGuest) {
                // non logged in user has to sign in
                app()->user->loginRequired();
            } else {
                // this is probably just admin trying to access the list outside the chapter view
                // this is not intuitive, we have to change it later
                $this->redirect(url('/chapter/index'));
            }
        }
    }

    public function actionUpdate($id) {

        Shared::debug($id, 'banner_id');

        // for admins only
        if (!app()->user->isAdmin()) {
            app()->user->redirectHome();
        }

        $model = Banner::model()->findByPk($id);
        if (is_object($model) && $model->dashboard == 1) {
            $vendor = $model->vendor;

            $this->performAjaxValidation($model);
            Shared::debug($_POST);
            if (isset($_POST['Banner'])) {
                Shared::debug($_POST);
                $model = Banner::create($_POST['Banner'], $model);

                if ($model->save()) {
                    if (isset($_POST['isAjaxRequest'])) {
                        echo $model->banner_id;
                        app()->end();
                    } else {
                        $this->redirect(array('/dashBanner/update' . $model->offer_id));
                    }
                } else {
                    // validation issue
                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }

                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
            }

            $this->render('//banner/dashboard/update', array(
                'model' => $model,
                'vendor' => $vendor));
        } else {
            // not a dashboard banner or no model at all
            $this->redirect(array('/dashBanner'));
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        Shared::debug($_POST);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'banner-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
<?php

class BannerController extends Controller {

    public $layout = '//layouts/full';

    /**
     * List of active / expired banners visible for the vendor.
     * Display warning page when the vendor does not have banners active.
     */
    public function actionIndex() {
        $vendor = app()->user->getActiveVendor();
        $setVendor = app()->request->getParam('postBannerVendor');
        if ($vendor) {
            $model = Vendor::model()->findByPk($vendor);
            if ($model->isBannerEnabled()) {
                $dataProvider = new CActiveDataProvider('Banner', array(
                            'criteria' => array(
                                'condition' => 'vendor_id=' . $vendor . ' AND dashboard != 1',
                            ),
                        ));
                $this->render('index', array('vendor' => $model, 'dataProvider' => $dataProvider));
            } else {
                $this->render('disabled', array('vendor' => $model));
            }
        } elseif($setVendor) {
            $vendor = Vendor::model()->findByPk($setVendor);
            if (Auth::hasVendorAccess($vendor)) {
                app()->user->setActiveVendor($setVendor);
                $this->redirect(url('/banner'));
            }
        } else {
            app()->user->loginRequired(true,false,true);
        }
    }

    public function actionCreate() {

        $model = new Banner;
        $model->loadDefaults();

        $vendor = Vendor::model()->findByPk(app()->user->getActiveVendor());
        Shared::debug($vendor);
        if ($vendor != null && $vendor->isBannerEnabled()) {

            $this->performAjaxValidation($model);
            Shared::debug($_POST);
            if (isset($_POST['Banner'])) {
                Shared::debug($_POST);
                $model = Banner::create($_POST['Banner'], $model);

                if ($model->save()) {
                    if (isset($_POST['isAjaxRequest'])) {
                        echo $model->banner_id;
                        app()->end();
                    } else {
                        $this->redirect(array('/banner/update' . $model->banner_id));
                    }
                } else {
                    // validation issue
                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }

                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
            }

            $this->render('create', array(
                'model' => $model,
                'vendor' => $vendor));
        } else {
            app()->user->loginRequired();
        }
    }
    
    public function actionUpdate($id) {

        Shared::debug($id, 'banner_id');
        $model = Banner::model()->findByPk($id);

        if (Auth::hasVendorAccess($model)) {
            $vendor = $model->vendor;
            if (!$vendor->isBannerEnabled() || $model->dashboard == 1){
                $this->render('disabled', array('vendor' => $vendor));
            }
            
            $this->performAjaxValidation($model);
            Shared::debug($_POST);
            if (isset($_POST['Banner'])) {
                Shared::debug($_POST);
                $model = Banner::create($_POST['Banner'], $model);

                if ($model->save()) {
                    if (isset($_POST['isAjaxRequest'])) {
                        echo $model->banner_id;
                        app()->end();
                    } else {
                        $this->redirect(array('/banner/update' . $model->offer_id));
                    }
                } else {
                    // validation issue
                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }

                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
            }

            $this->render('update', array(
                'model' => $model,
                'vendor' => $vendor));
        } else {
            app()->user->loginRequired();
        }
    }

    public function actionGetPublishingInfo($bannerId, $startDate, $endDate){
        $model = Banner::model()->findByPk($bannerId);
        if (Auth::hasVendorAccess($model)){
            echo json_encode($model->getPublishingInfo(CHtml::decode($startDate), CHtml::decode($endDate)));
        } else{
            throw new CHttpException(403, 'Access Denied.');
        }
    }
    
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        Shared::debug($_POST);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'banner-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
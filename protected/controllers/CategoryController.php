<?php

class CategoryController extends Controller
{
    public $layout = '//layouts/full';
    
    public function actionIndex(){
        if (app()->user->isAdmin()) {
            app()->user->setHomeRole();
            $id = app()->user->getUser()->user_id;
            $model = User::model()->findByPk($id);
            $dataProvider = new CActiveDataProvider('Category', array('criteria'=>array('order'=>'category_name ASC')));
            //$dataProvider = Vendor::model()->getDataProvider();
            $this->render('index', array(
                'dataProvider' => $dataProvider,
                'model' => $model,
            ));
        } else {
            Yii::app()->user->loginRequired();
        }
    }
    
    public function actionCreate(){
        if (app()->user->isAdmin()) {
            app()->user->setHomeRole();
            $model = new Category;
            if (isset($_POST['Category'])) {
                $model->attributes = $_POST['Category'];
                if ($model->save())
                    $this->redirect(array('index'));
            }

            $this->render('create', array(
                'model' => $model,
            ));
        } else {
            app()->user->loginRequired();
        }
    }
    
    public function actionUpdate($id){
        if (app()->user->isAdmin()) {
            app()->user->setHomeRole();
            $model = Category::model()->findByPk($id);
            if (isset($_POST['Category'])) {

                $model->attributes = $_POST['Category'];
                if ($model->save())
                    $this->redirect(array('index'));
            }

            $this->render('update', array(
                'model' => $model,
            ));
        } else {
            app()->user->loginRequired();
        }
    }
}
?>

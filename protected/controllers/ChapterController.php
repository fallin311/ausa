<?php

class ChapterController extends Controller {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    //public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    /* public function filters() {
      return array(
      'accessControl', // perform access control for CRUD operations
      );
      } */

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    /* public function accessRules() {
      return array(
      array('allow', // allow all users to perform 'index' and 'view' actions
      'actions' => array('index', 'view'),
      'users' => array('*'),
      ),
      array('allow', // allow authenticated user to perform 'create' and 'update' actions
      'actions' => array('create', 'update'),
      'users' => array('@'),
      ),
      array('allow', // allow admin user to perform 'admin' and 'delete' actions
      'actions' => array('admin', 'delete'),
      'users' => array('admin'),
      ),
      array('deny', // deny all users
      'users' => array('*'),
      ),
      );
      } */

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    /* public function actionView($id) {
      $this->render('view', array(
      'model' => $this->loadModel($id),
      ));
      } */

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        if (app()->user->isAdmin()) {
            $model = new Chapter;
            $user = User::create(array());
            $userChapter = new UserChapter;

            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation($model, $user);

            if (isset($_POST['Chapter']) && isset($_POST['User'])) {
                //Shared::debug($_POST);
                $model->attributes = $_POST['Chapter'];
                $user->attributes = $_POST['User'];

                $model->validate();
                $user->validate();
                if (!$model->hasErrors() && !$user->hasErrors()) {
                    $model->save(false);
                    $user->save(false);
                    app()->user->setActiveChapter($model->chapter_id);
                    $userChapter = UserChapter::create($user, $model);
                    if ($userChapter != null)
                        $userChapter->save();
                    if (isset($_POST['isAjaxRequest'])) {
                        echo $model->chapter_id;
                        app()->end();
                    }
                } else {
                    // validation errors
                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        foreach ($user->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
            }

            $this->render('create', array(
                'model' => $model,
                'user' => $user,
                'userChapter' => $userChapter
            ));
        } else {
            Yii::app()->user->loginRequired();
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {

        $model = $this->loadModel($id);

        if (Auth::hasChapterAccess($model)) {
            app()->user->setActiveChapter($id);
            $user = $model->getContactUser();
            if ($user == null)
                $user = User::create(array());

            // there should be one already anyway
            $userChapter = UserChapter::findByUser($user, $model);
            if ($userChapter == null) {
                //Shared::debug("no contact information");
                $userChapter = new UserChapter;
            }

            $this->performAjaxValidation($model, $user);

            if (isset($_POST['Chapter'])) {
                $model->attributes = $_POST['Chapter'];

                if (isset($_POST['User'])) {
                    //Shared::debug("got user");
                    $user->attributes = $_POST['User'];
                }

                $model->validate();
                $user->validate();

                if (!$model->hasErrors() && !$user->hasErrors()) {

                    $model->save(false);
                    $user->save(false);

                    // just to make sure ... when it wasn't created with chapter
                    if ($userChapter->isNewRecord) {
                        // Shared::debug("creating user vendor");
                        $userChapter = UserChapter::create($user, $model);
                        $userChapter->save();
                    }

                    if (isset($_POST['isAjaxRequest'])) {
                        app()->user->setFlash('success', 'Chapter settings have been saved.');
                        app()->end();
                    } else {
                        $this->redirect(array('dashboard'));
                    }
                } else {
                    // valdiation failed
                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        foreach ($user->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        echo $errors . '</ul>';
                        app()->end();
                    }
                }

                /* if ($model->save()) {
                  if (app()->user->isAdmin()) {
                  $this->redirect(url('/chapter'));
                  } else {
                  $this->redirect(url('/chapter/dashboard'));
                  }
                  } */
            }
            $this->render('update', array(
                'model' => $model,
                'user' => $user,
                'userChapter' => $userChapter
            ));
        } else {
            Yii::app()->user->loginRequired();
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (app()->user->isAdmin()) {
            if (Yii::app()->request->isPostRequest) {

                //To-do: Should we do this? - Also delete users associated with this Chapter.. Vk
                //$chapterUsers = 
                // we only allow deletion via POST request
                $this->loadModel($id)->delete();

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
            else
                throw new CHttpException(400, 'Invalid request. Permission not granted.');
        }else {
            throw new CHttpException(403, 'Access Denied. This operation is for Admins only.');
        }
    }

    /**
     * Lists all models if admin.. if chapter user is logged in, then redirect to their dashboard
     */
    public function actionIndex() {

        $dataProvider = new CActiveDataProvider('Chapter',
                        array(
                            'pagination' => array('pageSize' => 10),
                            'criteria' => array(
                                'condition' => 'entity_id=' . Entity::model()->getEntity(),
                            ),
                ));
        //Shared::debug(app()->user->getActiveChapter());
        $chapter = Chapter::model()->findByPk(app()->user->getActiveChapter());
        if (app()->user->isAdmin()) { //if admin, redirect to Index view to show all chapters
            $id = app()->user->getUser()->user_id;
            $model = User::model()->findByPk($id);
            $this->render('index', array(
                'chapterProvider' => $dataProvider,
                'model' => $model,
                // the user should be redirected to dashbanners if true (after selecting a chapter)
               'dashBanRedirect' => (app()->user->hasFlash('setChapter')?true:false),
            ));
        } else if (Auth::hasChapterAccess($chapter)) { //else if chapter user, redirect to his dashboard
            $this->redirect(url('/chapter/dashboard'));
        } else { // else redirect to Login required
            Yii::app()->user->loginRequired();
        }
    }

    /**
     * Manages all models.
     */
    /* public function actionAdmin() {
      $model = new Chapter('search');
      $model->unsetAttributes();  // clear any default values
      if (isset($_GET['Chapter']))
      $model->attributes = $_GET['Chapter'];

      $this->render('admin', array(
      'model' => $model,
      ));
      } */

    /**
     * Switch to Chapter Dashboard
     * @param type $id
     */
    public function actionGo($id) {
        $chapter = Chapter::model()->findByPk($id);
        if (Auth::hasChapterAccess($chapter)) {
            app()->user->setActiveChapter($id);
            $this->redirect(url('/chapter/dashboard'));
        }
        $this->redirect(url('/site/index'));
    }

    /**
     * Dashboard actions which grab json data and render in-line
     * 
     */
    public function actionMembers() {
        if (app()->user->isAdmin() || app()->user->isChapterUser()) {
            $member = new Member('search');
            $model = Chapter::model()->findByPk(app()->user->getActiveChapter());
            if (isset($_GET['Member'])) {
                $member->attributes = $_GET['Member'];
            }
            $this->render('_members', array('member' => $member, 'model' => $model));
        } else {
            app()->user->loginRequired();
        }
    }

    public function actionVendors() {
        $chapterId = app()->user->getActiveChapter();
        if ($chapterId) {
            $model = Chapter::model()->findByPk($chapterId);
            $vendorProvider = new CActiveDataProvider('VendorMembership',
                            array(
                                'pagination' => array('pageSize' => 10),
                                'criteria' => array(
                                    'with' => array(
                                        'vendor' => array(
                                            'alias' => 'vendor'
                                        ),
                                    ),
                                    'condition' => 'chapter_id=' . $chapterId . ' AND active=1 AND vendor.expiration>NOW() AND vendor.published=1',
                                //'order' => 'vendor_name'
                                ),
                    ));
            $chapterName = $model->chapter_name;
            $this->render('_vendors', array('model' => $model, 'dataProvider' => $vendorProvider, 'chapterName' => $chapterName));
        } else {
            // Let the vendor controller actionIndex take care of them
            $this->redirect(url('vendor/index'));
        }
    }

    /**
     * Render main chapter dasboard
     */
    public function actionDashboard() {
        $chId = app()->user->getActiveChapter();

        $model = Chapter::model()->findByPk($chId);

        if (Auth::hasChapterAccess($model)) {

            $stProvider = SalesTeam::model()->findAllByAttributes(array(
                'chapter_id' => $chId,
                'disabled' => 0,
                    ));


            //$dependency = new CDbCacheDependency('SELECT CONCAT(vmax,cmax) FROM(SELECT MAX(vendor.updated_on) AS vmax, MAX(offer.updated_on) AS cmax FROM vendor, offer) updateConcat');
            //Shared::debug($dependency);
            //$vendorProvider = new CActiveDataProvider(VendorMembership::model()->cache(3600, $dependency),
            $vendorProvider = new CActiveDataProvider('VendorMembership',
                            array(
                                'pagination' => array('pageSize' => 15),
                                'criteria' => array(
                                    'with' => array(
                                        'vendor' => array(
                                            'alias' => 'vendor'
                                        ),
                                        'vendor.salesTeams',
                                        'vendor.activeOffers'
                                    ),
                                    'condition' => 't.chapter_id=' . $chId . ' AND t.active=1',
                                    'order' => 'vendor_name'
                                ),
                    ));
            //Shared::debug($chId);
            //$ch = Chapter::model()->findByPk($chId);
            // $chapterName = $ch->chapter_name;

            $this->render('dashboard', array(
                'model' => $model,
                'stProvider' => $stProvider,
                'noOfSts' => count($stProvider),
                'vendorProvider' => $vendorProvider,
            ));
        } else {
            app()->user->redirectHome();
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Chapter::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model, $user = nlull) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'chapter-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if ($user != null && isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($user);
            Yii::app()->end();
        }
    }

}

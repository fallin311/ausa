<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
                'foreColor' => 0x7B3D0A,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'https +index', // Force https, but only on login page
            'entity + index',
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        //$this->render('index');
        //Shared::debug(Entity::model()->cleanEntity(app()->getBaseUrl(true)));
        if (app()->user->isGuest) {
            
            $this->render(Entity::model()->findByPk(app()->user->getEntity())->landing_page);            
        } else {
            app()->user->redirectHome();
        }
    }

    public function actionContact() {
        $model = new ContactForm('site');
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            //Shared::debug($model->attributes);

            if ($model->validate()) {
                $mail = new AOEmail('siteContactUs');

                $mail->addPlaceholders(array(
                    'name' => CHtml::encode($model->name),
                    'subject' => CHtml::encode($model->subject),
                    'body' => CHtml::encode($model->body),
                    'senderEmail' => CHtml::encode($model->email),
                ));

                //$mail->addRecipient('admin@ausaoffers.com', 'AUSA Offers Admin');
                $mail->addRecipient('vk@ausaoffers.com', 'Vk Melarkod');
                //$mail->setSender($model->email, $model->name);
                $mail->send();


                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('_contact', array('model' => $model));
    }

    public function actionTerms() {
        $company = 'AUSA Offers';
        $this->render('_terms', array('companyName' => $company));
    }

    public function actionPrivacy() {
        $this->render('_privacy');
    }

    public function actionVendors() {
        $this->render('_vendors');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}
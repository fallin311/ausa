<?php

class ImageController extends Controller {

    public $layout = '//layouts/full';

    /** displays the test screen */
    public function actionTest($id = null) {
        if ($id == null) {
            $model = Image::create();
        } else {
            $model = Image::model()->findByPk((int) $id);
        }
        $this->render('test', array('model' => $model));
    }

    /**
     * First stage of ajax image upload 
     */
    public function actionProcess() {
        //Shared::debug($_REQUEST);

        if (isset($_REQUEST['config'])) {
            $config = $_REQUEST['config'];
        } else {
            $config = 'default';
        }
        // possibly get image from dtb from the request in case we are uploading
        if (isset($_REQUEST['image_id'])) {
            $model = Image::model()->findByPk($_REQUEST['image_id']);
            // permission check
            if ($model->vendor_id) {
                if (Auth::hasVendorAccess($model)) {
                    if ($model->isTemplate()) {
                        //Shared::debug("has access");
                        // we don't want to overwrite template file from any
                        // screen, but imageUpdate
                        if (app()->user->isAdmin()) {
                            if (!isset($_REQUEST['replace'])) {
                                // admin screen
                                $model = Image::create();
                                //Shared::debug("replacing image");
                            }
                        }
                    }
                } else {
                    // go away
                    throw new CException('You do not have access to this image.');
                }
            } else if (isset($_REQUEST['vendor_id'])) {
                // new image and it belongs to vendor
                $vendor = Vendor::model()->findByPk((int) $_REQUEST['vendor_id']);
                if (Auth::hasVendorAccess($vendor)) {
                    $model = Image::create();
                    $model->vendor_id = $_REQUEST['vendor_id'];
                }
            } else {

                //Shared::debug("no vendor id");
                // might be chapter or sales team ... not admin
                if (app()->user->isAdmin()) {
                    //Shared::debug("we got admin");
                    // update it

                    if ($model->isTemplate() && !isset($_REQUEST['replace'])) {
                        // admin screen
                        $model = Image::create();
                        // Shared::debug("replacing image");
                    } else {
                        // Shared::debug($model->isTemplate());
                    }
                } else {
                    $st = SalesTeam::model()->findByPk(app()->user->getActiveSt());
                    if (!is_object($st) || $st->logo != $model->image_id) {
                        // is it chapter?
                        $chapter = Chapter::model()->findByPk(app()->user->getActiveChapter());
                        if (!is_object($chapter) || $chapter->logo != $model->image_id) {
                            // go away
                            throw new CException('You do not have access to this image.');
                        }
                    }
                }
            }
            if ($model == null) {
                $model = Image::create();
            }
        } else {
            // new image
            if (app()->user->isGuest) {
                throw new CException('You do not have access to this image.');
            }
            $model = Image::create();
        }

        Yii::import('ext.imageupload.ImageUploadComponent');
        // list of valid extensions, ex. array("jpeg", "xml", "bmp")
        // this should be replaced by config array
        $uploader = new ImageUploadComponent($config);
        //Shared::debug("uploader");
        $result = $uploader->handleUpload($model);

        Shared::debug($result);
        if (!isset($result['success']))
            $result['success'] = 0;
        // Shared::debug($result);
        if (isset($result['crop']) && $result['crop']) {
            // we still need to crop it. The second step will be processed by FinishUpload
            // and therefore we need to save the progress to database
            $model->cropped = 0;
        }
        if ($result['success']) {
            $model->save();
            Shared::debug("saved with res: " . $model->resolution);

            // create media file
            $result['imageId'] = $model->image_id;
            $result['longId'] = $model->long_id;
        } else {
            //Shared::debug("something went wrong");
        }
        //Shared::debug($result);
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
    }

    /**
     * Resize the image and store it back to database 
     */
    public function actionFinishUpload() {
        //Shared::debug($_REQUEST, 'resizing image after upload');
        if (isset($_REQUEST['image_id'])) {
            $image = Image::model()->findByPk($_REQUEST['image_id']);

            // it might be called twice
            if ($image != null && $image->cropped == 0) {
                //Shared::debug("loaded with res: " . $image->resolution);
                Yii::import('ext.imageupload.ImageUploadComponent');
                $config = ImageUploadComponent::getConfig($_REQUEST['configuration']);
                // what is this?
                $dimensions = explode(',', $_REQUEST['dimensions']);
                $canvas = $image->resizeCrop($dimensions[0], $dimensions[1], $dimensions[2], $dimensions[3], $config['cropDimensions']['width'], $config['cropDimensions']['height']);
                $image->saveCanvasToString($canvas);
                $image->resolution = $config['cropDimensions']['width'] . 'x' . $config['cropDimensions']['height'];
                $image->cropped = 1;
                $image->updated_on = Shared::toDatabase(time());
                $image->save();
                //echo $image->getUrl($config['previewDimensions']['width'], $config['previewDimensions']['height'], true);
                //Shared::debug("cropped");
                $result = array(
                    'url' => $image->getUrl($config['previewDimensions']['width'], $config['previewDimensions']['height'], true),
                    'fullUrl' => $image->getUrl(),
                    'imageId' => $image->image_id,
                    'longId' => $image->long_id,
                    'success' => 1
                );
                echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                Yii::app()->end();
            } else {
                //Shared::debug("cropped already");
            }
        }
        // this is not completely right, but it is gonna work
        echo htmlspecialchars(json_encode(array('success' => 0)), ENT_NOQUOTES);
    }

    /**
     * Display single image loaded from database
     * @param integer $img 
     */
    public function actionDisplay($img) {
        // find the image first
        $image = Image::model()->findByAttributes(array('long_id' => $img));
        if ($image == null) {

            // nothing found, lets display dummy image
            header("Content-type: image/png");
            header("Content-Disposition: inline; filename=no_photo.jpg");
            readfile(YiiBase::getPathOfAlias('webroot') . '/images/no_photo.jpg');
            Yii::app()->end();
        } else {
            $image->display();
        }
        Yii::app()->end();
    }

    public function actionUpdate($id) {
        if (app()->user->isAdmin()) {
            $model = Image::model()->findByPk($id);
            if (isset($_POST['Image'])) {
                if ($_POST['Image']['category'] > 0) {
                    $_POST['Image']['category'] = Image::$categories[$_POST['Image']['category']];
                }
                $model->attributes = $_POST['Image'];
                if ($model->save())
                    $this->redirect(array('index'));
            }

            $this->render('update', array(
                'model' => $model,
            ));
        } else {
            app()->user->loginRequired();
        }
    }

    public function actionCreate() {
        if (app()->user->isAdmin()) {
            $model = Image::create();
            if (isset($_POST['Image'])) {
                //Shared::debug($_POST['Image']);
                if (isset($_POST['Image']['image_id'])) {
                    try {
                        $model = $this->loadModel($_POST['Image']['image_id']);
                    } catch (Exception $exc) {
                        $model = Image::create();
                        $model->addError('file_content', 'Please upload image first');
                    }
                }

                if ($_POST['Image']['category'] > 0) {
                    $_POST['Image']['category'] = Image::$categories[$_POST['Image']['category']];
                }
                $model->attributes = $_POST['Image'];
                if (!$model->hasErrors() && $model->save())
                    $this->redirect(array('index'));
            }

            $this->render('create', array(
                'model' => $model,
            ));
        } else {
            app()->user->loginRequired();
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (app()->user->isAdmin()) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                $this->loadModel($id)->delete();

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        } else {
            app()->user->loginRequired();
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Image::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Display list of all images
     */
    public function actionIndex() {
        if (app()->user->isAdmin()) {
            $id = app()->user->getUser()->user_id;
            $model = User::model()->findByPk($id);
            $dataProvider = new CActiveDataProvider('Image', array(
                'criteria' => array(
                    'with' => 'vendor'
                ),
                'sort' => array(
                    'defaultOrder' => 't.updated_on DESC',
                )
            ));
            $this->render('index', array(
                'dataProvider' => $dataProvider,
                'model' => $model,
            ));
        } else {
            app()->user->loginRequired();
        }
    }

}

?>

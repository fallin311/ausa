<?php

class VendorController extends Controller {

    public function filters() {
        return array(
            'https +register', // Force https, but only on login page
        );
    }

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main';

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the register form
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
                'foreColor' => 0x7B3D0A,
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        if (app()->user->isAdmin() || app()->user->isSalesman()) {
            $model = new Vendor;
            $user = User::create(array());
            $user->login_disabled = true;
            $userVendor = new UserVendor;

            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation($model, $user);

            // associate the model with chapter
            if ($model->chapterId == null) {
                $stId = app()->user->getActiveSt();
                if ($stId) {
                    $st = SalesTeam::model()->findByPk($stId);
                    $chapterId = $st->chapter_id;
                } else {
                    // might be an admin, just find the first active chapter
                    // in future, the form will ask which chapter should be used
                    if (isset($_POST['Vendor']['chapterId'])) {
                        $chapterId = $_POST['Vendor']['chapterId'];
                    } else {
                        $chapter = Chapter::model()->find(array('condition' => 'disabled=0'));
                        $chapterId = $chapter->chapter_id;
                    }
                }
            }

            if ($model->salesTeamId == null) {
                // find the sales team
                if (isset($_POST['Vendor']['salesTeamId'])) {
                    $stId = $_POST['Vendor']['salesTeamId'];
                    $vendor['stId'] = $stId;
                }
            }

            if (isset($_POST['Vendor']) && isset($_POST['User'])) {
                $model->attributes = $_POST['Vendor'];
                $user->attributes = $_POST['User'];
                $userVendor->send_redemption_email = isset($_POST['UserVendor']['send_redemption_email']) ? $_POST['UserVendor']['send_redemption_email'] : false;
                $vendor = $model->attributes;
                $vendor['chapterId'] = $chapterId;
                $vendor['salesTeamId'] = $stId;

                $output = Vendor::create(array('vendor' => $vendor, 'user' => $user->attributes, 'userVendor' => $userVendor->attributes));
                if ($output['success']) {
                    // sweeet, lets go to more advanced view

                    if (isset($_POST['Vendor']['salesmenArr'])) {
                        $model->salesmenArr = $_POST['Vendor']['salesmenArr'];
                    }
                    $model->saveSalesman();

                    if (isset($_POST['isAjaxRequest'])) {
                        echo $output['vendor']->vendor_id;
                        app()->user->setActiveVendor($output['vendor']->vendor_id);
                        //Shared::debug($output['vendor']->vendor_id);
                        app()->end();
                    } else {
                        Shared::debug("Going to redirect here..l ");
                        $this->redirect(array('/vendor/update' . $output['vendor']->vendor_id));
                    }
                } else {
                    // probably some errors
                    if (isset($output['vendor']))
                        $model = $output['vendor'];

                    if (isset($output['user'])) {
                        $user = $output['user'];
                        $user->validate(); // ensure that validation of users runs
                    }

                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        foreach ($user->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
                /* if ($model->save())
                  $this->redirect(array('view', 'id' => $model->vendor_id)); */
            }

            $this->render('create', array(
                'model' => $model,
                'user' => $user,
                'userVendor' => $userVendor
            ));
        } else {
            $this->redirect(url('/vendor/register'));
            //app()->user->loginRequired();
        }
    }

    // Allow front-end vendor registration (chapter is hard coded to 1 for now)
    public function actionRegister($chapter_id = 2) {
        if (app()->user->isGuest()) {
            //$vendor = $chapter = $st = null;
            $user = new User('register');
            $model = new Vendor();
            $userVendor = new UserVendor();

            $this->performAjaxValidation($user);

            if (isset($_POST['User']) && isset($_POST['Vendor'])) {
                $model->attributes = $_POST['Vendor'];
                $user->attributes = $_POST['User'];
                $vendor = $model->attributes;
                $vendor['chapterId'] = $chapter_id;
                $vendor['logo'] = 'X6wRDVztMuWwq';
                $valid = $user->validate();
                $valid = $model->validate() && $valid;
                if ($valid) {
                    $output = Vendor::create(array('vendor' => $vendor, 'user' => $user->attributes, 'userVendor' => $userVendor->attributes));
                    if ($output['success']) {
                        $userArray = array();
                        $userArray['username'] = $user['email_address'];
                        $userArray['password'] = $_POST['User']['password'];
                        $userArray['rememberMe'] = 0;
                        $loginModel = new LoginForm;
                        $loginModel->attributes = $userArray;
                        if ($loginModel->validate() && $loginModel->login()) {
                            $this->redirect(url(app()->user->getHomeUrl()));
                            app()->end();
                        }
                    } else {
                        
                    }
                }
            }

            $this->render('register', array(
                'user' => $user,
                'vendor' => $model,
                'userVendor' => $userVendor,
            ));
        } else {
            if (app()->user->isVendor()) {
                //echo 'Send this user to some page that is not register and is not create... but what? vendor update?';
                throw new CHttpException(403, 'Access Denied.');
                app()->end();
            } else {
                $this->redirect(url('/vendor/create'));
            }
        }
    }

    /* public function actionSubscription($id) {
      $model = $this->loadModel($id);
      //first check if this user has access to this particular vendor
      if (Auth::hasVendorAccess($model)) {
      $this->render('subscription', array(
      'model' => $model,
      //'user' => $user,
      // 'userVendor' => $userVendor
      ));
      } else {
      Yii::app()->user->loginRequired();
      }
      } */

    /**
     * Display publishing screen and publish / unpublish vendor and all offers
     */
    public function actionGoLive() {
        $vendorId = app()->user->getActiveVendor();
        //Shared::debug($vendorId);

        if ($vendorId) {

            $model = Vendor::model()->findByPk($vendorId);

            if (Auth::hasVendorAccess($model)) {
                $subStatus = $model->getSubscriptionStatus();
                $readyToGo = false;
                if ($subStatus == Vendor::SUB_ACTIVE && $model->isVerified()) {
                    $readyToGo = true;
                }

                if (isset($_POST['publish'])) {
                    if (isset($_POST['agree']) && $readyToGo) {
                        // he is good to go!
                        $model->published = 1;
                        $model->save(false, array('published'));
                        app()->user->setFlash('success', $model->vendor_name . ' has been published.');
                        echo "1";
                        app()->end();
                    }
                    echo "0";
                    app()->end();
                }

                // we can go the other way as well
                if (isset($_POST['unpublish'])) {
                    $model->published = 0;
                    $model->save(false, array('published'));
                    app()->user->setFlash('success', $model->vendor_name . ' has been unpublished.');
                    echo "1";
                    app()->end();
                }

                $branchProvider = new CActiveDataProvider('Branch', array(
                            'criteria' => array(
                                'condition' => 'active = 1 AND vendor_id=' . $vendorId,
                            ),
                            'pagination' => array(
                                'pageSize' => 5,
                            ),
                        ));

                $this->render('go_live', array(
                    'model' => $model,
                    'branchData' => $branchProvider,
                    'readyToGo' => $readyToGo
                ));
            }
        } else {
            app()->user->redirectHome();
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        if (Auth::hasVendorAccess($model)) {
            $user = $model->getContactUser();
            if ($user == null)
                $user = User::create(array());
            $user->scenario = 'update';

            // there should be one already anyway
            $userVendor = UserVendor::findByUser($user, $model);
            if ($userVendor == null) {
                //Shared::debug("no contact information");
                $userVendor = new UserVendor;
            }
            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation($model, $user);

            if (isset($_POST['Vendor'])) {
                $model->attributes = $_POST['Vendor'];
                if (isset($_POST['User'])) {
                    //Shared::debug("got user");
                    $user->attributes = $_POST['User'];
                }
                if (isset($_POST['Vendor']['salesmenArr'])) {
                    $model->salesmenArr = $_POST['Vendor']['salesmenArr'];
                }

                // associate with chapter
                if (app()->user->isAdmin() && isset($_POST['Vendor']['chapterId'])) {
                    // check if it is already there
                    /* $found = false;
                      foreach ($model->chapters as $chapter){
                      if ($chapter->chapter_id == $_POST['Vendor']['chapterId']) $found = true;
                      } */
                    $membership = VendorMembership::model()->findByAttributes(array('vendor_id' => $model->vendor_id,
                        'chapter_id' => $_POST['Vendor']['chapterId']));
                    if ($membership == null) {
                        // create membership
                        $chapter = Chapter::model()->findByPk((int) $_POST['Vendor']['chapterId']);
                        if (is_object($chapter)) {
                            $vendorMembership = VendorMembership::create($model, $chapter);
                            $vendorMembership->save();
                        }
                    }
                }

                $model->validate();

                if (!$model->hasErrors() && !$user->hasErrors()) {
                    //Shared::debug("it is valid");
                    // disable validation when saving
                    $model->updated_on = date("Y-m-d H:i:s");
                    //Shared::debug($model);
                    $model->save(false);
                    $user->save(false);

                    // workaround for saving params
                    if (strlen($user->params)) {
                        $user->save(false, array('params'));
                    }

                    $model->saveSalesman();

                    if ($userVendor->isNewRecord) {
                        //Shared::debug("creating user vendor");
                        $userVendor = UserVendor::create($user, $model);
                    }
                    $userVendor->send_redemption_email = isset($_POST['UserVendor']['send_redemption_email']) ? $_POST['UserVendor']['send_redemption_email'] : false;
                    $userVendor->save();
                    if (isset($_POST['isAjaxRequest'])) {
                        app()->user->setFlash('success', 'Vendor settings have been saved.');
                        echo $model->vendor_id;
                        app()->end();
                    } else {
                        app()->user->setActiveVendor($model->vendor_id);
                        $this->redirect(array('dashboard'));
                    }
                } else {

                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        foreach ($user->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
            }

            $branchProvider = new CActiveDataProvider('Branch', array(
                        'criteria' => array(
                            'condition' => 'vendor_id=' . $id,
                        ),
                        'pagination' => array(
                            'pageSize' => 5,
                        ),
                    ));

            $this->render('update', array(
                'model' => $model,
                'user' => $user,
                'userVendor' => $userVendor,
                'branchProvider' => $branchProvider
            ));
        } else {
            Yii::app()->user->loginRequired();
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (app()->isAdmin()) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                $this->loadModel($id)->delete();

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
            else
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }else {
            throw new CHttpException(403, 'Access Denied. This operation is for Admins only.');
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        if (app()->user->isAdmin()) { //IF an administrator logs in, show all vendors.
            //$id = app()->user->getUser()->user_id;
            // setup a model for ajax grid filtering
            $criteria = new CDbCriteria(array(
                        'with' => array(
                            'chapters' => array(
                                //'condition' => 'chapter.chapter_id IN (SELECT chapter_id FROM chapter WHERE entity_id1=' . Entity::model()->getEntity() . ')',
                                'alias' => 'chapter',
                            ),
                        ),
                        //'condition' => 'chapter.entity_id=' . Entity::model()->getEntity(),
                        'order' => 'vendor_name'
                            )
            );
            /*$criteria = new CDbCriteria(array(
                        'order' => 'vendor_name',
                        'with' => 'chapters',
                        'condition' => 'chapters.entity_id = 3',
                    ));*/
            //Shared::debug($criteria);
            $model = new Vendor('search');

            if (isset($_GET['Vendor'])) {
                $model->attributes = $_GET['Vendor'];
                $criteria->mergeWith($model->search());
            }

            $dataProvider = new CActiveDataProvider('Vendor', array(
                        'pagination' => array('pageSize' => 15),
                        'criteria' => $criteria
                            /* 'criteria' => array(
                              'with' => array(
                              'chapters'
                              ),
                              'order' => 'vendor_name'
                              ), */
                    ));
            //Shared::debug($dataProvider->getData());


            $this->render('index', array(
                'user' => app()->user->getUser(),
                'model' => $model,
                'dataProvider' => $dataProvider,
                'offerRedirect' => (app()->user->hasFlash('setOfferVendor')?true:false),
                'bannerRedirect' => (app()->user->hasFlash('setBannerVendor')?true:false),
            ));
        } elseif (app()->user->isSalesman()) { // Show vendors specific to a sales team
            $stId = app()->user->getActiveSt();

            $criteria = new CDbCriteria(array(
                        'with' => array(
                            'vendor' => array(
                                'alias' => 'vendor'
                            ),
                        ),
                        'condition' => 'sales_team_id=' . $stId . ' AND active=1',
                        'order' => 'vendor_name'
                            )
            );
            $model = new Vendor('search');

            if (isset($_GET['Vendor'])) {
                $model->attributes = $_GET['Vendor'];
                $criteria->mergeWith($model->search());
            }

            $dataProvider = new CActiveDataProvider('StVendor', array(
                        'pagination' => array('pageSize' => 15),
                        'criteria' => $criteria
                            /* 'criteria' => array(
                              'with' => array(
                              'vendor' => array(
                              'alias' => 'vendor'
                              ),
                              ),
                              'condition' => 'sales_team_id=' . $stId . ' AND active=1',
                              'order' => 'vendor_name'
                              ), */
                    ));
            $st = SalesTeam::model()->findByPk(app()->user->getActiveSt());

            /* if ($stId) {
              $st = SalesTeam::model()->findByPk($stId);
              } */

            /* $this->redirect(url('/salesTeam/dashboard')); */
            $this->render('indexViews/salesteamIndex', array(
                'model' => $st,
                'dataProvider' => $dataProvider,
                'offerRedirect' => (app()->user->hasFlash('setOfferVendor')?true:false),
                'bannerRedirect' => (app()->user->hasFlash('setBannerVendor')?true:false)));
        } else if (app()->user->isChapterUser()) {//Show vendors from all sales teams but specific to a Chapter..
            // This is now taken care of in the chapter controller actionVendors - TS
            $this->redirect(url('chapter/vendors'));
        } else if (app()->user->isVendor()) {//Show vendor dashboard..
            $this->redirect(url('/vendor/dashboard'));
        } else {
            Yii::app()->user->loginRequired();
        }
    }

    /**
     * Switch to Vendor Dashboard
     * @param type $id
     */
    public function actionGo($id) {
        $vendor = Vendor::model()->findByPk($id);
        //Shared::debug($vendor);
        if (Auth::hasVendorAccess($vendor)) {
            app()->user->setActiveVendor($id);
            $this->redirect(url('/vendor/dashboard'));
        }
        $this->redirect(url('/site/index'));
    }

    /**
     * action functions for ajax dashboard loading
     * @author Travis
     */
    public function actionOffers() {
        $vendorId = app()->user->getActiveVendor();
        if ($vendorId) {
            $notice = false;
            $model = Vendor::model()->findByPk($vendorId);
            $offerProvider = new CActiveDataProvider('Offer', array(
                        'criteria' => array(
                            'condition' => 'vendor_id=' . $vendorId,
                        //'condition' => 'vendor_id=' . $vendorId . ' AND start_date<=NOW() AND end_date>=NOW()',
                        ),
                        'sort' => array(
                            'defaultOrder' => 'published DESC',
                        ),
                    ));
            $this->render('offers', array('model' => $model, 'offerProvider' => $offerProvider, 'notice' => $notice));
        }
    }

    public function actionBranches() {
        $vendorId = app()->user->getActiveVendor();
        if ($vendorId) {
            $model = Vendor::model()->findByPk($vendorId);
            $branchProvider = new CActiveDataProvider('Branch', array(
                        'criteria' => array(
                            'condition' => 'vendor_id=' . $vendorId,
                        ),
                        'pagination' => array(
                            'pageSize' => 5,
                        ),
                    ));
            $this->render('_branches', array('model' => $model, 'branches' => $model->branches, 'branchData' => $branchProvider));
        }
    }

    public function actionSupport() {
        $vendorId = app()->user->getActiveVendor();
        $chModel = null;

        if ($vendorId > 0) {
            $model = Vendor::model()->findByPk($vendorId);
            $vendorMembership = VendorMembership::model()->findByAttributes(array('vendor_id' => $vendorId));
            if ($vendorMembership) {
                $chModel = Chapter::model()->findbyPk($vendorMembership->chapter_id);
            }
            $stVendor = StVendor::model()->findByAttributes(array('vendor_id' => $vendorId, 'active' => 1));
            $salesmen = array();
            if ($stVendor) {
                $stModel = SalesTeam::model()->findbyPk($stVendor->sales_team_id);
                $salesmen = VendorSalesman::model()->with('user')->findAllByAttributes(array('vendor_id' => $vendorId));
            } else {
                $stModel = null;
            }
            $contactForm = new ContactForm('vendor');
            if (isset($_POST['ContactForm'])) {

                $contactForm->setAttributes($_POST['ContactForm']);
                $contactForm->email = app()->user->user->email_address;
                $contactForm->name = app()->user->user->getFullName();

                if ($contactForm->validate()) {
                    $mail = new AOEmail('siteContactUs');

                    $mail->addPlaceholders(array(
                        'name' => CHtml::encode($contactForm->name),
                        'subject' => CHtml::encode($contactForm->subject . " (" . $model->vendor_name . ")"),
                        'body' => CHtml::encode($contactForm->body),
                        'senderEmail' => $contactForm->email,
                    ));

                    $mail->addRecipient('admin@ausaoffers.com', 'AUSA Offers Admin');

                    $mail->send();
                    Yii::app()->user->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
                    $contactForm->unsetAttributes();
                }
            }

            $this->render('support', array(
                'model' => $model,
                //'branch' => $model->branches, 
                'contactForm' => $contactForm,
                'chapter' => $chModel,
                'stModel' => $stModel,
                'salesmen' => $salesmen));
        }
    }

    // End action functions for ajax dashboard loading

    /**
     * Render main vendor dasboard ... todo
     */
    public function actionDashboard() {
        $vendorId = app()->user->getActiveVendor();
        //Shared::debug($vendorId);

        if ($vendorId) {
            $model = Vendor::model()->findByPk($vendorId);

            $offerProvider = new CActiveDataProvider('Offer', array(
                        'criteria' => array(
                            'condition' => 'vendor_id=' . $vendorId,
                        ),
                        'sort' => array(
                            'defaultOrder' => 'published DESC',
                        ),
                    ));

            $this->render('dashboard', array(
                'model' => $model,
                'offerProvider' => $offerProvider,
            ));
        } else {
            if (app()->user->hasFlash('success')) {
                app()->user->setFlash('success', 'Vendor Settings have been saved.');
            }
            app()->user->redirectHome();
        }
    }

    /**
     * Send an activation email to AUSA admins, which will activate the account.
     */
    public function actionSendActivationEmail($id) {
        $vendor = Vendor::model()->findByPk((int) $id);
        if (is_object($vendor) && !$vendor->activated) {

            $mail = new AOEmail('vendorActivation');
            $mail->addRecipient(app()->params['adminEmail'], 'AUSA Admins');
            $mail->addPlaceholders(array(
                'vendor_url' => absUrl('/vendor/go/' . $vendor->vendor_id),
                'vendor_id' => $vendor->vendor_id,
                'vendor_name' => $vendor->vendor_name));
            if ($mail->send()) {
                echo "1";
                app()->end();
            }
        }
        echo "0";
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Vendor::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model, $user = null) {
        //Shared::debug($_POST);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'vendor-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if ($user != null && isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($user);
            Yii::app()->end();
        }
    }

}

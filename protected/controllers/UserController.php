<?php

class UserController extends Controller {

    public function filters() {
        return array(
            'https +login', // Force https, but only on login page
            'entity + forgotpassword, newPassword, verify',
        );
    }

    public function actionIndex() {
        if (app()->user->isAdmin()) {
            $id = app()->user->getUser()->user_id;
            $model = User::model()->findByPk($id);
            $dataProvider = new CActiveDataProvider('User');
            $this->render('index', array(
                'dataProvider' => $dataProvider,
                'model' => $model,
            ));
        } else {
            Yii::app()->user->loginRequired();
        }
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      } */

    public function actions() {
        // return external action classes, e.g.:
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {

        $model = new LoginForm;
        $alert = false;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];

            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                $user = Yii::app()->user->getUser();

                if (!isset($user->last_login) || !isset($user->email_verified) || (int) ($user->email_verified) < 1 || strlen($user->first_name) < 2 || strlen($user->last_name) < 2) {
                    User::model()->updateByPk($user->user_id, array('last_login' => new CDbExpression('NOW()')));
                    if (isset($_POST['ajax'])) {
                        echo url('/user/firstlogin');
                        app()->end();
                    }
                    $this->redirect(url('/user/firstlogin'));
                } else {
                    User::model()->updateByPk($user->user_id, array('last_login' => new CDbExpression('NOW()')));
                    if (isset($_POST['ajax'])) {
                        echo app()->user->getHomeUrl();
                        app()->end();
                    } else {
                        $this->redirect(app()->user->getHomeUrl());
                    }
                    app()->user->redirectHome();
                }
            } else {
                if (isset($_POST['ajax'])) {
                    echo "bad";
                    app()->end();
                } else {
                    $alert = true;
                }
            }
        }

        // display the login form
        $this->render('login', array('model' => $model, 'alert' => $alert));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    /**
     * Create new login. Only admin should be able to do it.
     */
    public function actionCreate($vendor_id = null, $chapter_id = null, $st_id = null) {
        // check that admin is creating another user
        //$this->authAdminOnly();
        if (app()->user->isAdmin() || app()->user->isSalesman()) {
            $model = new User('create');
            // Shared::debug($_POST);

            $vendor = $chapter = $st = null;

            if ($vendor_id) {
                $vendor = Vendor::model()->findByPk($vendor_id);
            } else if ($chapter_id) {
                $chapter = Chapter::model()->findByPk($chapter_id);
            } else if ($st_id) {
                $st = SalesTeam::model()->findByPk($st_id);
            }


            if (isset($_POST['User'])) {
                $model = User::create($_POST['User']);
                if ($model->save()) {
                    if (isset($_POST['vendor_id']) && is_numeric($_POST['vendor_id'])) {//check to see if vendor is being added.
                        $userVendor = UserVendor::create($model, $_POST['vendor_id']);
                        $userVendor->save(); //also save the user_vendor join
                    }

                    if (isset($_POST['chapter_id']) && is_numeric($_POST['chapter_id'])) {//check to see if vendor is being added.
                        $userChapter = UserChapter::create($model, $_POST['chapter_id']); //also save the user_chapter join
                        $userChapter->save();
                    }

                    if (isset($_POST['sales_team_id']) && is_numeric($_POST['sales_team_id'])) {//check to see if vendor is being added.
                        $userSt = UserSt::create($model, $_POST['sales_team_id']); //also save the user_st join..
                        $userSt->save();
                    }

                    $this->redirect(url('/user'));
                    Yii::app()->end();
                }
            }
            // render the create form
            $this->render('create', array(
                'model' => $model,
                'vendor' => $vendor,
                'chapter' => $chapter,
                'st' => $st,
            ));
        } else {
            $this->redirect(url('/vendor/register'));
            //app()->user->loginRequired();
        }
    }

    public function actionRegister() {
        $this->redirect(url('/vendor/register'));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {

        if (app()->user->id == $id || app()->user->IsAdmin()) {

            // some authentication is required

            $model = $this->loadModel($id);
            $model->scenario = 'update';

            $this->performAjaxValidation($model);

            //Now find out what Sales Team or Chapter or Vendor this user is assigned to.. 
            $vendor = $chapter = $st = null;
            $vArray = $model->getVendorIds(true, true); //reload=true; but get onlyVendors here = true
            if (isset($vArray[0])) {
                $vendor = Vendor::model()->findByPk($vArray[0]);
            }

            $cArray = $model->getChapterIds(); //reload=true; but get onlyVendors here = true
            if (isset($cArray[0])) {
                $chapter = Chapter::model()->findByPk($cArray[0]);
            }

            $stArray = $model->getStIds(true, true); //reload=true; but get onlyVendors here = true
            if (isset($stArray[0])) {
                $st = SalesTeam::model()->findByPk($stArray[0]);
            }

            if (isset($_POST['User'])) {
                $model->attributes = $_POST['User'];
                //$model->setPassword($_POST['User']['password']);
                if ($model->save()) {
                    app()->user->setFlash('success', 'Account setting have been saved.');

                    //Shared::debug($_POST);
                    //(isset($_POST['Vendors'])) {
                    // N squared is not good
                    //foreach ($_POST['Vendors']['user_vendor'] as $id => $vendorId) {
                    // add remove user from vendor list
                    //$found = false;
                    //foreach ($model->vendors as $vendor) {
                    //if ($vendor->vendor_id == $vendorId) {
                    //$found = true;
                    //}
                    //}
                    //if (!$found) {
                    //$userVendor = UserVendor::create($user, $vendorId);
                    //Shared::debug($userVendor);
                    //if ($userVendor != null)
                    //$userVendor->save();
                    //}
                    //}
                    //}
                    //}else {
                    //Shared::debug("errors are everywhere");
                }
            }

            // do not edit the hash
            $model->password = '';

            $this->render('update', array(
                'model' => $model,
                'vendor' => $vendor,
                'chapter' => $chapter,
                'st' => $st,
            ));
        } else {
            throw new CHttpException(403, 'Access Denied.');
        }
    }

    public function actionPassword($id) {
        $user = app()->user->getUser();
        if (isset($user->user_id) && $user->user_id === $id || app()->user->isAdmin()) {
            $model = $this->loadModel($id);
            $model->setScenario('changePassword');
            if (isset($_POST['User'])) {
                $model->attributes = $_POST['User'];
                if ($model->validate()) {
                    $model->password = sha1($model->salt . $_POST['User']['pass1']);
                    if ($model->save()) {
                        app()->user->setFlash('success', 'Your password has been updated.');
                        $this->redirect(array('update', 'id' => $model->user_id));
                    }
                }
            }

            $this->render('/user/password', array('model' => $model));
        } else {
            // access denied for this user
            throw new CHttpException(403, 'Access Denied.');
        }
    }

    public function actionFirstlogin() {
        if(!app()->user->isGuest()){
            $user = app()->user->getUser();
            $id = $user->id;
            $model = $this->loadModel($id);
            $model->setScenario('firstLogin');
            
            $vArray = $user->getVendorIds(true, true);
            
            if (isset($vArray[0]))
                $vendor = Vendor::model()->findByPk($vArray[0]);
            
            if (isset($_POST['User'])) {
                $model->attributes = $_POST['User'];
                if($model->validate()){
                    $model->email_verified = 1;
                    $model->password = sha1($model->salt.$_POST['User']['pass1']);
                    if ($model->save()) {
                        app()->user->setFlash('success', 'Your information has been updated.');
                        $this->redirect(app()->user->getHomeUrl());
                    }
                }
            }
            $this->render('firstlogin', array(
                'model' => $model,
                'role' => app()->user->getActiveRole(true),
            ));
        } else {
            app()->user->loginRequired();
        }
    }

    // for now just simple stupid email field
    public function actionForgotPassword() {
        $model = new User('passwordReset');

        $notFound = false;
        if (isset($_POST['User'])) {
            // find user first
            $model = User::model()->findByEmail($_POST['User']['email_address']);

            if ($model == null) {
                $notFound = true;
                if ($_POST['ajax']) {
                    echo 'bad request';
                    app()->end();
                }
                $model = new User('passwordReset');
            } else {
                $model->setScenario('passwordReset');
                // send the reset password email
                $timestamp = time();
                // we use hash to lookup password reset request from the user table
                $hash = md5($model->email_address . $model->password . $timestamp);
                $model->password_reset = $timestamp;
                $model->save();
                //Shared::debug($timestamp);
                //Shared::debug($hash);
                $mail = new AOEmail('resetPassword');
                $mail->addPlaceholders(array(
                    'email' => $model->email_address,
                    'full_name' => $model->getFullName(),
                    'reset_link' => absUrl('/user/newPassword', array('req' => $hash))));
                $mail->addRecipient($model->email_address, $model->getFullName());
                //$mail->addRecipient('ondrej.nebesky@gmail.com', 'Ondrej Nebesky');
                $mail->send();
            }
        }
        $this->render('forgot_password', array(
            'model' => $model,
            'emailSent' => $notFound
        ));
    }

    /**
     * Confirmation of the email. This action will allow to reset the password
     * asking for new one.
     * The link has to be 24hrs and newer
     */
    public function actionNewPassword($req) {
        // lookup users, who requested a password change
        $since = strtotime(Shared::toDatabase(time()) . " -1 day");
        $users = User::model()->findAllBySql("SELECT * FROM user WHERE password_reset > $since");
        $found = null;
        foreach ($users as $model) {
            if ($req == md5($model->email_address . $model->password . $model->password_reset)) {
                $found = $model;
                break;
            }
        }
        if ($found != null) {
            $model->setScenario('resetPass');
            if (isset($_POST['User'])) {
                $model->attributes = $_POST['User'];
                if ($model->validate()) {
                    $found->setPassword($_POST['User']['pass1']);
                    $found->save();

                    // log the user in i guess
                    $userArray = array();
                    $userArray['username'] = $found['email_address'];
                    $userArray['password'] = $_POST['User']['pass1'];
                    $userArray['rememberMe'] = 0;
                    $login = new LoginForm;
                    $login->attributes = $userArray;
                    if ($login->validate() && $login->login()) {
                        app()->user->setFlash('success', 'Your password has been changed.');
                        $this->redirect(app()->user->getHomeUrl());
                    }
                }
            }
            $this->render('new_password', array('model' => $found));
            app()->end();
        }
        // display not found screen
        throw new CHttpException(400, 'This password reset link is not valid. To reset your password, please <a href="' . url('user/forgotpassword') . '">initiate a new request</a>.');
    }

    /**
     * Send a verification email to the currently logged in user.
     */
    public function actionSendVerificationEmail() {
        $user = User::model()->findByPk((int) $_GET['id']);
        if (is_object($user) && !$user->email_verified) {
            if (app()->user->isAdmin()) {
                // admin verifies the acount directly without sending an email
                $user->email_verified = 1;
                //Shared::debug($user);
                $user->save(false, array('email_verified'));
                echo "1";
                app()->end();
            } else {
                $mail = new AOEmail('emailVerification');
                $mail->addRecipient($user->email_address, $user->getFullName());
                $mail->addPlaceholders(array('verification_url' => absUrl('/user/verify/' . $user->user_id . '?v=' . $user->salt)));
                if ($mail->send()) {
                    echo "1";
                    app()->end();
                }
            }
        }
        echo "0";
    }

    /**
     * Recive an email verification request, process and redirect home
     * @param type $id user id
     * @param type $v this is a salt from user's password
     */
    public function actionVerify($id, $v = null) {
        $user = User::model()->findByPk($id);

        if (is_object($user)) {
            if ($v == $user->salt) {
                $user->scenario = 'verify';
                // we use activation also for email change
                // new email might be stored in the parameter
                Yii::import('application.models.behaviors.ParamsBehavior');
                $user->attachBehavior('paramsBehavior', new ParamsBehavior());
                $newEmail = $user->getParameter('changeEmail');

                // there might be a security flaw if user can somehow change the params array
                if ($newEmail && strlen($newEmail) > 4) {
                    $user->email_address = $newEmail;

                    // TODO: this one does not work
                    $user->unsetParameter('changeEmail');
                    $user->saveParameters();
                }

                // success
                $user->email_verified = 1;
                $user->save();

                $this->render('verification_success', array('model' => $user));
                Yii::app()->end();
            }
        }

        $this->render('verification_error');
    }

    /**
     * Display login information for mobile access for users to see their
     * company offers
     */
    public function actionMobile() {
        $user = app()->user->getUser();
        $vendor = Vendor::model()->findByPk(app()->user->getActiveVendor());
        if ($user) {
            $this->render('_mobile', array('model' => $user, 'vendor' => $vendor));
        }
    }

    /**
     * Display login information for mobile access for users to see their
     * company offers
     */
    public function actionMobilePreview() {
        $user = app()->user->getUser();
        $vendor = Vendor::model()->findByPk(app()->user->getActiveVendor());
        // TODO: detect incompatible browsers and not logged in users

        if ($user) {
            if (app()->user->isChapterUser()) {
                $this->render('mobile_chapter');
            } else {
                $this->render('mobile', array('model' => $user, 'vendor' => $vendor));
            }
        } else {
            // user is not logged in
            app()->user->loginRequired();
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}

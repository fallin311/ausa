<?php

class SalesTeamController extends Controller {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    //public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    /* public function filters()
      {
      return array(
      'accessControl', // perform access control for CRUD operations
      );
      } */

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    /*
     * 	public function accessRules()
      {
      return array(
      array('allow',  // allow all users to perform 'index' and 'view' actions
      'actions'=>array('index','view'),
      'users'=>array('*'),
      ),
      array('allow', // allow authenticated user to perform 'create' and 'update' actions
      'actions'=>array('create','update'),
      'users'=>array('@'),
      ),
      array('allow', // allow admin user to perform 'admin' and 'delete' actions
      'actions'=>array('admin','delete'),
      'users'=>array('admin'),
      ),
      array('deny',  // deny all users
      'users'=>array('*'),
      ),
      );
      } */

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed

      public function actionView($id) {
      $this->render('view', array(
      'model' => $this->loadModel($id),
      ));
      } */

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        if (app()->user->isAdmin()) {
            $model = new SalesTeam;
            $user = User::create(array());
            $userSt = new UserSt;

            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation($model, $user);

            if (isset($_POST['SalesTeam']) && isset($_POST['User'])) {
                //Shared::debug($_POST);
                $model->attributes = $_POST['SalesTeam'];
                $user->attributes = $_POST['User'];

                $user->address = $model->address; // Set the users address to the st address as-per the july 2013 task doc - travis

                $model->validate();
                $user->validate();
                if (!$model->hasErrors() && !$user->hasErrors()) {
                    $model->save(false);
                    $user->save(false);
                    $userSt = UserSt::create($user, $model);
                    if ($userSt != null)
                        $userSt->save();
                    if (isset($_POST['isAjaxRequest'])) {
                        echo $model->sales_team_id;
                        app()->end();
                    }
                } else {
                    // validation errors
                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        foreach ($user->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
            }

            $this->render('create', array(
                'model' => $model,
                'user' => $user,
                'userSt' => $userSt
            ));
        } else {
            Yii::app()->user->loginRequired();
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        if (Auth::hasStAccess($model)) {
            app()->user->setActiveSt($id);
            $user = $model->getContactUser();
            if ($user == null)
                $user = User::create(array());

            // there should be one already anyway
            $userSt = UserSt::findByUser($user, $model);
            if ($userSt == null) {
                //Shared::debug("no contact information");
                $userSt = new UserSt;
            }

            $this->performAjaxValidation($model, $user);

            if (isset($_POST['SalesTeam'])) {
                $model->attributes = $_POST['SalesTeam'];

                if (isset($_POST['User'])) {
                    //Shared::debug("got user");
                    $user->attributes = $_POST['User'];
                    $user->address = $model->address; // Set the users address to the st address as-per the july 2013 task doc - travis
                }

                $model->validate();
                $user->validate();

                if (!$model->hasErrors() && !$user->hasErrors()) {

                    $model->save(false);
                    $user->save(false);

                    // just to make sure ... when it wasn't created with the model
                    if ($userSt->isNewRecord) {
                        $userSt = UserSt::create($user, $model);
                        $userSt->save();
                    }

                    if (isset($_POST['isAjaxRequest'])) {
                        app()->user->setFlash('success', 'Sales Team settings have been saved.');
                        app()->end();
                    } else {
                        $this->redirect(array('dashboard'));
                    }
                } else {
                    // valdiation failed
                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        foreach ($user->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }
                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
            }
            $this->render('update', array(
                'model' => $model,
                'user' => $user,
                'userSt' => $userSt
            ));
        } else {
            Yii::app()->user->loginRequired();
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (app()->user->isAdmin()) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                $this->loadModel($id)->delete();

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
            else
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }else {
            throw new CHttpException(403, 'Access Denied. This operation is for Admins only.');
        }
    }

    /**
     * Lists all models for administrator; redirects to dashboard for a salesman
     * @author Vk
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('SalesTeam');
        $salesTeam = SalesTeam::model()->findByPk(app()->user->getActiveSt());
        if (app()->user->isAdmin()) { //if admin, redirect to Index view to show all sales teams
            $id = app()->user->getUser()->user_id;
            $model = User::model()->findByPk($id);
            $this->render('index', array(
                'stProvider' => $dataProvider,
                'model' => $model,
            ));
        } else if (Auth::hasStAccess($salesTeam)) { //else if salesman, redirect to his dashboard
            $this->redirect(url('/salesTeam/dashboard'));
        } else { // else redirect to Login required
            Yii::app()->user->loginRequired();
        }
    }

    /**
     * Manages all models.

      public function actionAdmin() {
      $model = new SalesTeam('search');
      $model->unsetAttributes();  // clear any default values
      if (isset($_GET['SalesTeam']))
      $model->attributes = $_GET['SalesTeam'];

      $this->render('admin', array(
      'model' => $model,
      ));
      } */

    /**
     * Switch to Sales Team Dashboard
     * @param type $id
     * @author Vk
     */
    public function actionGo($id) {
        $salesTeam = SalesTeam::model()->findByPk($id);
        if (Auth::hasStAccess($salesTeam)) {
            app()->user->setActiveSt($id);
            $this->redirect(url('/salesTeam/dashboard'));
        }
        $this->redirect(url('/site/index'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = SalesTeam::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model, $user) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'sales-team-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if ($user != null && isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($user);
            Yii::app()->end();
        }
    }

    public function actionVendors() {
        $stId = app()->user->getActiveSt();
        if ($stId) {
            $dataProvider = new CActiveDataProvider('StVendor', array(
                'pagination' => array('pageSize' => 15),
                'criteria' => array(
                    'with' => array(
                        'vendor' => array(
                            'alias' => 'vendor'
                        ),
                    ),
                    'condition' => 'sales_team_id=' . $stId . ' AND active=1',
                    'order' => 'vendor_name'
                ),
                    ));
            $model = SalesTeam::model()->findByPk($stId);
            $this->render('_vendors', array('model' => $model, 'dataProvider' => $dataProvider));
        }
    }

    /**
     * Display ST dashboard and reset active roles from previously visited vendors
     */
    public function actionDashboard() {
        $stId = app()->user->getActiveSt();
        if ($stId) {
            app()->user->setHomeRole();
            $vendorProvider = new CActiveDataProvider('StVendor', array(
                'pagination' => array('pageSize' => 5),
                'criteria' => array(
                    'with' => array('vendor' => array('alias' => 'vendor')),
                    'condition' => 'sales_team_id=' . $stId,
                ),
                    ));

            $model = SalesTeam::model()->with('chapter')->findByPk($stId);
            $this->render('dashboard', array('model' => $model,
                'vendorProvider' => $vendorProvider,
            ));
        } else {
            app()->user->redirectHome();
        }
    }

}

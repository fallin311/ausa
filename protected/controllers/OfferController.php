<?php

class OfferController extends Controller {

    public $layout = '//layouts/full';

    /**
     * List of active / expired offers visible for the vendor
     */
    public function actionIndex() {
        $vendor = app()->user->getActiveVendor();
        $setVendor = app()->request->getParam('postOfferVendor');
        if ($vendor) {
            $notice = false;
            $dataProvider = new CActiveDataProvider('Offer', array(
                'criteria' => array(
                    'condition' => 'vendor_id=' . $vendor,
                ),
            ));
            if(app()->user->getFlash('success')){
                $notice = true;
            }
            //$this->render('index', array('vendor' => Vendor::model()->findByPk($vendor), 'dataProvider' => $dataProvider));
            $this->render('//vendor/offers', array('model' => Vendor::model()->findByPk($vendor), 'offerProvider' => $dataProvider, 'notice' => $notice));
        } elseif($setVendor) {
            $vendor = Vendor::model()->findByPk($setVendor);
            if (Auth::hasVendorAccess($vendor)) {
                app()->user->setActiveVendor($setVendor);
                $this->redirect(url('/offer'));
            }
        } else {
            app()->user->loginRequired(true,true,false);
        }
    }

    public function actionCreate() {
        Yii::import('application.models.behaviors.UpdateOfferBehavior');
        $model = new Offer;
        $model->attachBehavior('updateOffer', new UpdateOfferBehavior());

        $model->loadDefaults();

        //Shared::debug($_POST);
        $vendor = Vendor::model()->findByPk(app()->user->getActiveVendor());
        if ($vendor != null) {

            $this->performAjaxValidation($model);
            Shared::debug($_POST);
            if (isset($_POST['Offer'])) {
                //$model->attributes = $_POST['Offer'];
                //$model->updated_on = Shared::timeNow();

                $model = Offer::create($_POST['Offer'], $model);

                //$model->beforeValidate();

                if ($model->save()) {
                    $model->saveCategories();
                    if (isset($_POST['isAjaxRequest'])) {
                        echo $model->offer_id;
                        app()->end();
                    } else {
                        $this->redirect(array('/offer/update' . $model->offer_id));
                    }
                } else {
                    // validation issue
                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }

                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
            }

            $dataProvider = new CActiveDataProvider('OfferTemplate', array(
                'criteria' => array(
                    'condition' => ('category_id=' . $vendor->industry . ' OR category_id = 0'),
                ),
            ));

            $this->render('create', array('model' => $model,
                'vendor' => $vendor,
                'templateDataProvider' => $dataProvider));
        } else {
            app()->user->loginRequired();
        }
    }

    public function actionUpdate($id) {
        Yii::import('application.models.behaviors.UpdateOfferBehavior');
        $model = Offer::model()->findByPk($id);
        $model->attachBehavior('updateOffer', new UpdateOfferBehavior());
        $vendor = Vendor::model()->findByPk(app()->user->getActiveVendor());
        $model->beforeFormLoad();

        //Shared::debug($_POST);
        if (Auth::hasVendorAccess($model)) {
            if ($vendor == null || $model->vendor_id != $vendor->vendor_id) {
                // it is wrong vendor, lets try to switch to role
                if (!app()->user->setActiveVendor($model->vendor_id)) {
                    app()->user->loginRequired();
                } else {
                   
                    $vendor = Vendor::model()->findByPk(app()->user->getActiveVendor());
                }
            }

            if (isset($_POST['Offer'])) {
                $model = Offer::create($_POST['Offer'], $model);
                if ($model->published){
                    $model->setScenario('publish');
                }
                if ($model->save()) {
                    $model->saveCategories();
                    if (isset($_POST['isAjaxRequest'])) {
                        app()->user->setFlash('success', 'Offer changes have been saved.');
                        echo $model->offer_id;
                        app()->end();
                    } else {
                        $this->redirect(array('/offer/update' . $model->offer_id));
                    }
                } else {
                    // validation issue
                    if (isset($_POST['isAjaxRequest'])) {
                        $errors = '<ul>';
                        foreach ($model->getErrors() as $error) {
                            $errors .= '<li>' . $error[0] . '</li>';
                        }

                        echo $errors . '</ul>';
                        app()->end();
                    }
                }
            }
            $dataProvider = new CActiveDataProvider('OfferTemplate');

            $this->render('update', array('model' => $model,
                'vendor' => $vendor,
                'templateDataProvider' => $dataProvider));
        } else {
            app()->user->loginRequired();
        }
    }

    public function actionGetPublishingInfo($offerId, $startDate, $endDate){
        $model = Offer::model()->findByPk($offerId);
        if (Auth::hasVendorAccess($model)){
            echo json_encode($model->getPublishingInfo(CHtml::decode($startDate), CHtml::decode($endDate)));
        } else{
            throw new CHttpException(403, 'Access Denied.');
        }
    }
    
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        Shared::debug($_POST);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'offer-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
<?php

class AdminController extends Controller {

    public function actionIndex() {
        app()->user->redirectHome();
    }

    public function actionDashboard() {
        //if (app()->user->isAdmin() || app()->user->isSalesman()) {
        //app()->user->setActiveVendor(null);
        // }
        if (app()->user->getUser()) {
            if (app()->user->IsAdmin()) {
                app()->user->setHomeRole();
                $id = app()->user->getUser()->user_id;
                $model = User::model()->findByPk($id);

                $this->render('dashboard', array('model' => $model));
            } else {
                app()->user->redirectHome();
            }
        } else {
            app()->user->loginRequired();
        }
    }

    public function actionLogs($logName = "application.log", $lines = 50) {
        if (app()->user->getUser()) {
            if (app()->user->IsAdmin()) {
                // default model, which can be changed
                switch ($logName) {
                    case "debug.txt":
                    case "email.txt":
                    case "application.log":
                        $model = new SystemLog($logName);
                        break;
                    default:
                        $model = new SystemLog("application.log");
                }
                
                $model->setLineCount($lines);

                $this->render('logs', array('model' => $model));
            } else {
                app()->user->redirectHome();
            }
        } else {
            app()->user->loginRequired();
        }
    }
    
    /**
     * Perform system update from subversion
     */
    public function actionSystemUpdate(){
        if (app()->user->getUser()) {
            if (app()->user->IsAdmin()) {
                $model = new SvnUpdate();
                $model->updated_on = Shared::timeNow();
                
                if (isset($_POST['SvnUpdate'])){
                    // peform the update
                    //$model->attributes = $_POST['SvnUpdate'];
                    $res = $model->runUpdate($_POST['SvnUpdate']['note'], $_POST['SvnUpdate']['revision'], $_POST['SvnUpdate']['updated_file']);
                    if ($res){
                        app()->user->setFlash('success','The application has been updated.');
                    } else {
                        app()->user->setFlash('success','The application is already at the latest revision.');
                    }
                    $model = new SvnUpdate;
                }
                
                $dataProvider = new CActiveDataProvider('SvnUpdate', array(
                'criteria' => array(
                    'order' => 'revision DESC'
                ),
                    ));
                
                $this->render('system_update', array('model' => $model, 'dataProvider' => $dataProvider));
            } else {
                app()->user->redirectHome();
            }
        } else {
            app()->user->loginRequired();
        }
    }

}
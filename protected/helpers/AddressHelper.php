<?php

/**
 * Helps to parse the address and translate into GPS coordinates
 */
class AddressHelper {

    static $googleApiUrl = 'http://maps.googleapis.com/maps/api/geocode/json?address=';

    public static function toLatLon($address) {
        $output = array();
        if (strlen($address) > 8) {
            // construct the full URL first
            $url = self::$googleApiUrl . urlencode($address) . "&sensor=false";
            $source = file_get_contents($url);
            $obj = json_decode($source);

            if (count($obj->results)) {
                // found at least one
                if (strlen($obj->results[0]->formatted_address)) {
                    $output['formatted'] = $obj->results[0]->formatted_address;
                } else {
                    $output['formatted'] = $address;
                    //Shared::debug($output);
                }
                $output['lat'] = $obj->results[0]->geometry->location->lat;
                $output['lon'] = $obj->results[0]->geometry->location->lng;

                // get the zipcode
                foreach ($obj->results[0]->address_components as $component) {
                    if (in_array('postal_code', $component->types)) {
                        $output['zipcode'] = $component->short_name;
                    }
                }
            }
        }
        return $output;
    }

    public static function getAddressFields($address) {
        $output = array();
        $output['street']=$output['street2']=$output['city']=$output['state']=$output['zip']=$output['country']="";
        if (strlen($address) > 8) {
            // construct the full URL first
            $url = self::$googleApiUrl . urlencode($address) . "&sensor=false";
            $source = file_get_contents($url);
            $obj = json_decode($source);

            if (count($obj->results)) {
                // found at least one
                //if (strlen($obj->results[0]->formatted_address)) {
                    //$output['formatted'] = $obj->results[0]->formatted_address;
               // }
                
                //Shared::debug($obj->results[0]->address_components);

                // get the address
                foreach ($obj->results[0]->address_components as $component) {
                    
                    if (in_array('street_number', $component->types)) {
                        $output['street'] = $component->short_name;
                    }
                    
                    if (in_array('route', $component->types)) {
                        $output['street'] .= ' '. $component->short_name;
                    }
                    
                    if (in_array('administrative_area_level_2', $component->types)) {
                        $output['city'] = $component->short_name;
                    }
                    
                    if (in_array('administrative_area_level_1', $component->types)) {
                        $output['state'] = $component->short_name;
                    }
                    
                    if (in_array('postal_code', $component->types)) {
                        $output['zip'] = $component->short_name;
                    }
                    
                    if (in_array('country', $component->types)) {
                        $output['country'] = $component->short_name;
                    }
                }
            }
        }
        return $output;
    }

}

?>

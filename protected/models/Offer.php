<?php

/**
 * This is the model class for table "offer".
 *
 * The followings are the available columns in table 'offer':
 * @property string $offer_id
 * @property string $vendor_id
 * @property string $user_id
 * @property integer $start_date
 * @property integer $end_date
 * @property string $participating_branches
 * @property string $filters
 * @property string $offer_title
 * @property string $offer_description
 * @property integer $offer_icon
 * @property string $redemption_instructions
 * @property string $redemption_code
 * @property string $custom_barcode
 * @property string $redemption_types
 * @property integer $render_barcode
 * @property integer $global_limit
 * @property integer $per_capita_limit_number
 * @property string $per_capita_limit_unit
 * @property integer $impressions
 * @property integer $clicks
 * @property integer $redemptions
 * @property integer $active
 * @property integer $published
 * @property string $updated_on
 * @property string $status
 * @property double $offer_value
 *
 * The followings are the available model relations:
 * @property Category[] $categories
 * @property MemberOffer[] $memberOffers
 */
class Offer extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Offer the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'offer';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vendor_id, user_id, offer_title', 'required'),
            array('render_barcode, global_limit, per_capita_limit_number, impressions, clicks, redemptions, published', 'numerical', 'integerOnly' => true),
            array('offer_value', 'numerical'),
            array('offer_description, redemption_instructions', 'application.components.validators.AOPhoneTextValidator'),
            array('vendor_id, user_id, custom_barcode', 'length', 'max' => 10),
            array('participating_branches', 'length', 'max' => 511),
            array('offer_title, redemption_types', 'length', 'max' => 63),
            array('redemption_code', 'length', 'max' => 12),
            array('offer_icon', 'length', 'max' => 31),
            array('per_capita_limit_unit, status', 'length', 'max' => 8),
            array('start_date, end_date', 'required', 'on' => 'publish'),
            array('start_date', 'validateConcurency', 'on' => 'publish', 'publishedOnly' => true), // when the banner is being published, we need to make sure there is only one
            array('start_date, end_date, filters, updated_on', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('offer_id, vendor_id, user_id, start_date, end_date, participating_branches, filters, offer_title, offer_description, offer_icon, redemption_instructions, redemption_code, custom_barcode, redemption_types, render_barcode, global_limit, per_capita_limit_number, per_capita_limit_unit, impressions, clicks, redemptions, published, updated_on, status, offer_value', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'categories' => array(self::MANY_MANY, 'Category', 'offer_category(offer_id, category_id)'),
            'memberOffers' => array(self::HAS_MANY, 'MemberOffer', 'offer_id'),
            'vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),
        );
    }

    public function scopes() {
        return array(
            // only those, which should be sent to cellphones
            'planPublished' => array(
                'condition' => 'end_date > NOW()',
            )
        );
    }

    public function behaviors() {
        return array(
            'offerBehavior' =>
            array(
                'class' => 'application.models.behaviors.OfferBehavior'
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'offer_id' => 'Offer',
            'vendor_id' => 'Vendor',
            'user_id' => 'User',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'participating_branches' => 'Participating Branches',
            'filters' => 'Filters',
            'offer_title' => 'Offer Title',
            'offer_description' => 'Description',
            'offer_icon' => 'Offer Image',
            'redemption_instructions' => 'Redemption Instructions',
            'redemption_code' => 'Redemption Code',
            'custom_barcode' => 'Custom Barcode',
            'redemption_types' => 'How to Redeem',
            'render_barcode' => 'Render Barcode',
            'global_limit' => 'Global Limit',
            'per_capita_limit_number' => 'Per Capita Limit Number',
            'per_capita_limit_unit' => 'Per Capita Limit Unit',
            'impressions' => 'Impressions',
            'clicks' => 'Clicks',
            'redemptions' => 'Redemptions',
            'published' => 'Published',
            'updated_on' => 'Updated On',
            'status' => 'Status',
            'offer_value' => 'Offer Value',
            'categoriesArr' => 'Categories'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('offer_id', $this->offer_id, true);
        $criteria->compare('vendor_id', $this->vendor_id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('start_date', $this->start_date);
        $criteria->compare('end_date', $this->end_date);
        $criteria->compare('participating_branches', $this->participating_branches, true);
        $criteria->compare('filters', $this->filters, true);
        $criteria->compare('offer_title', $this->offer_title, true);
        $criteria->compare('offer_description', $this->offer_description, true);
        $criteria->compare('offer_icon', $this->offer_icon);
        $criteria->compare('redemption_instructions', $this->redemption_instructions, true);
        $criteria->compare('redemption_code', $this->redemption_code, true);
        $criteria->compare('custom_barcode', $this->custom_barcode, true);
        $criteria->compare('redemption_types', $this->redemption_types, true);
        $criteria->compare('render_barcode', $this->render_barcode);
        $criteria->compare('global_limit', $this->global_limit);
        $criteria->compare('per_capita_limit_number', $this->per_capita_limit_number);
        $criteria->compare('per_capita_limit_unit', $this->per_capita_limit_unit, true);
        $criteria->compare('impressions', $this->impressions);
        $criteria->compare('clicks', $this->clicks);
        $criteria->compare('redemptions', $this->redemptions);
        $criteria->compare('published', $this->published);
        $criteria->compare('updated_on', $this->updated_on, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('offer_value', $this->offer_value);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
                ));
    }

    /**
     * We need to make sure that no other banners are in the same window
     * We want warning for unpublished banners and error for published
     * @param type $attribute
     * @param type $params
     */
    function validateConcurency($attribute, $params) {
        // the question is how to produce warning on ajax validate and error on
        // published resource
        $checkPublished = false;
        if (isset($params['publishedOnly'])) {
            $checkPublished = $params['publishedOnly'];
        }
        if ($this->published && $checkPublished && $this->vendor_id) {
            // find coliding offers using interval arithmetics
            $colliding = Offer::model()->findAllBySql("SELECT offer_id, offer_title, start_date, end_date, published FROM offer 
                WHERE vendor_id={$this->vendor_id} AND published > 0 " . ($this->isNewRecord ? '' : ' AND offer_id != ' . $this->offer_id) . " AND(
                (start_date<='{$this->start_date}' AND end_date >= '{$this->start_date}') OR
                (start_date<='{$this->end_date}' AND end_date >='{$this->end_date}') OR
                (start_date>='{$this->start_date}' AND end_date <='{$this->end_date}'))");
            $allowed = $this->vendor->max_offers;

            Shared::debug(count($colliding), $allowed);
            if ($allowed == 0) {
                $this->addError('offer_title', 'Your account is not enabled to publish a offer. Please contact your sales person.');
                return false;
            } else if (count($colliding)) {

                // we don't really care about expiration date right now
                if (count($colliding) >= $allowed) {
                    // allright, this would not work
                    // get list of offers, so we can display nice error message
                    $existingMsg = '';
                    foreach ($colliding as $offer) {
                        $existingMsg .= '' . $offer->offer_title . ' (from <i>' . Shared::formatDateLonger($offer->start_date) . '</i> to <i>' . Shared::formatDateLonger($offer->end_date) . '</i>)<br />';
                    }

                    $this->addError('start_date', 'Please select different publishing date for this offer. Published offers: <br />' . $existingMsg);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Create or update the model from form. This function does not save or
     * valiate the model.
     * @param type $input
     * @return boolean|Offer
     */
    public static function create($input, $model = null) {
        // suppose the model is alrady found in the database
        if ($model == null) {
            $model = self::createModel('Offer', $input);
        } else {
            $model->setAttributes($input);
        }

        // test scripts cannot use app()->user
        if (php_sapi_name() !== 'cli') {
            $model->user_id = app()->user->id;
            $vendor = Yii::app()->user->getActiveVendor();
        } else {
            $vendor = $input['vendor_id'];
        }

        if (!$vendor)
            return null;
        if (!$model->vendor_id)
            $model->vendor_id = $vendor;
        $model->updated_on = Shared::timeNow();

        // set defaults
        if (!$model->per_capita_limit_unit) {
            $model->per_capita_limit_unit = 'campaign';
        }

        if (!isset($input['campaign_duration']) && isset($input['end_date'])) {
            // use end date from the select
            $model->end_date = $input['end_date'];
            if (strtotime($model->end_date) < strtotime($model->start_date)) {
                // set default offer duration
                $model->end_date = Shared::toDatabase(strtotime($model->start_date . " +1 month"));
            }
        } else if (isset($input['campaign_duration'])) {
            // calculate the end date
            $duration = $input['campaign_duration'];
            if ($duration < 1 || $duration > 12) {
                $duration = 1;
            }
            if (isset($input['start_date']) && $model->isNewRecord) {
                $model->end_date = Shared::toDatabase(strtotime($model->start_date . " +" . $duration . " month"));
            } else {
                // update the model. There are special rules for published offers
                // no date changes once it is published
                // TODO

                if ($model->published) {
                    // make sure we can only extend the offer
                    $new = strtotime($model->start_date . " +" . $duration . " month");
                    if ($new > time()) {
                        $model->end_date = Shared::toDatabase($new);
                    }
                } else {
                    Shared::debug("not published");
                    $model->end_date = Shared::toDatabase(strtotime($model->start_date . " +" . $duration . " month"));
                }
            }
        }

        if (isset($input['participating_branches']) && is_array($input['participating_branches'])) {
            $model->participating_branches = implode(",", $input['participating_branches']);
        } else {
            $model->participating_branches = '';
        }

        // convert the array back to string
        if (is_array($input['redemption_types'])) {
            $model->redemption_types = implode(",", $input['redemption_types']);
        } else {
            if (is_numeric($input['redemption_types'])) {
                $model->redemption_types = $input['redemption_types'];
            } else {
                $model->redemption_types = '';
            }
        }

        // only unpublished offer can set categories
        if ($model->published) {
            // we still need to put there something, so the validator doesn't complain
            $model->categoriesArr = array("dummy");
        } else {
            if (isset($input['categoriesArr']) && is_array($input['categoriesArr'])) {
                $model->setCategories($input['categoriesArr']);
            } else {
                // when the offer is published and we want to unpublish, there
                // is no catetegoriesArr
                // why is it here ??? We need some tests
                //$model->setCategories(array());
            }
        }

        return $model;
    }

    /**
     * We need to load whole list of offers for given user (chapter), which are
     * active. Also, it loads only limited set of attributes (performance)
     * It uses DAO instad of AR for performance reasons
     * Returns multi-dimensional member array used in json api
     */
    public static function getApiList($chapterId, $timestamp = null, $user = null) {
        $db = app()->db;
        $timestampSql = '';
        if ($timestamp == null) {
            $timestampSql = ' AND c.published=1 AND c.end_date > DATE_SUB(NOW(), INTERVAL 2 DAY) AND v.published=1';
        } else {
            // also include all not expired offers from recently updated vendor
            $timestampSql = " AND (c.updated_on > '$timestamp' OR v.updated_on > '$timestamp')";
        }

        if ($user == null) {
            // find offers by chapter
            if ($timestamp == null) {
                $timestampSql = ' AND c.published=1 AND c.end_date > DATE_SUB(NOW(), INTERVAL 2 DAY) AND v.published=1';
            } else {
                // also include all not expired offers from recently updated vendor
                $timestampSql = " AND (c.updated_on > '$timestamp' OR v.updated_on > '$timestamp')";
            }

            $sql = "SELECT c.offer_id, offer_description, offer_icon, offer_title, custom_barcode, 
            UNIX_TIMESTAMP(DATE_ADD(end_date, INTERVAL 1 DAY)) AS 'timestamp_sec', 
            IF ((DATE_ADD(v.expiration, INTERVAL 1 DAY) < NOW() OR v.published=0), '1', '0') AS 'vendor_expired',
            participating_branches, per_capita_limit_number, global_limit,
            per_capita_limit_unit, redemption_code, redemption_instructions, 
            redemption_types, render_barcode, c.vendor_id, redemptions,
            IF ((c.start_date < NOW() AND DATE_ADD(c.end_date, INTERVAL 1 DAY) > NOW() AND c.published=1), '1', '0') AS 'published',
            GROUP_CONCAT(cc.category_id) AS 'categories'
            FROM offer c
            LEFT JOIN vendor v ON v.vendor_id = c.vendor_id 
            LEFT JOIN vendor_membership m ON v.vendor_id = m.vendor_id 
            LEFT JOIN offer_category cc ON c.offer_id = cc.offer_id
            WHERE m.chapter_id = " . $chapterId . "
                AND v.activated = 1 AND DATE_ADD(v.expiration, INTERVAL 1 DAY) > NOW() AND m.active>0 AND v.disabled=0
            $timestampSql
            GROUP BY c.offer_id";
        } else {
            // find offers by user
            if ($timestamp == null) {
                $timestampSql = '';
            } else {
                // also include all not expired offers from recently updated vendor
                $timestampSql = " AND (c.updated_on > '$timestamp' OR v.updated_on > '$timestamp')";
            }
            if ($user->isAdmin()){
                $vendorIds = "";
            } else {
                $vendorIds =  "AND v.vendor_id IN (" . implode(",", $user->getVendorIds()) . ")";
            }
            $sql = "SELECT c.offer_id, offer_description, offer_icon, offer_title, custom_barcode, 
            UNIX_TIMESTAMP(DATE_ADD(end_date, INTERVAL 1 DAY)) AS 'timestamp_sec', 
            IF ((DATE_ADD(v.expiration, INTERVAL 1 DAY) < NOW() OR v.published=0), '1', '0') AS 'vendor_expired',
            participating_branches, per_capita_limit_number, global_limit,
            per_capita_limit_unit, redemption_code, redemption_instructions, 
            redemption_types, render_barcode, c.vendor_id, redemptions,
            IF ((c.start_date < NOW() AND DATE_ADD(c.end_date, INTERVAL 1 DAY) > NOW() AND c.published=1), '1', '0') AS 'published',
            GROUP_CONCAT(cc.category_id) AS 'categories'
            FROM offer c
            LEFT JOIN vendor v ON v.vendor_id = c.vendor_id 
            LEFT JOIN vendor_membership m ON v.vendor_id = m.vendor_id 
            LEFT JOIN offer_category cc ON c.offer_id = cc.offer_id
            WHERE m.chapter_id =  $chapterId $vendorIds
                AND m.active>0 AND v.disabled=0
            $timestampSql
            GROUP BY c.offer_id";
        }

        // Still not sure this will cover all the cases
       /* $sql = "SELECT c.offer_id, offer_description, offer_icon, offer_title, custom_barcode, 
            UNIX_TIMESTAMP(DATE_ADD(end_date, INTERVAL 1 DAY)) AS 'timestamp_sec', 
            IF ((DATE_ADD(v.expiration, INTERVAL 1 DAY) < NOW() OR v.published=0), '1', '0') AS 'vendor_expired',
            participating_branches, per_capita_limit_number, global_limit,
            per_capita_limit_unit, redemption_code, redemption_instructions, 
            redemption_types, render_barcode, c.vendor_id, redemptions,
            IF ((c.start_date < NOW() AND DATE_ADD(c.end_date, INTERVAL 1 DAY) > NOW() AND c.published=1), '1', '0') AS 'published',
            GROUP_CONCAT(cc.category_id) AS 'categories'
            FROM offer c
            LEFT JOIN vendor v ON v.vendor_id = c.vendor_id 
            LEFT JOIN vendor_membership m ON v.vendor_id = m.vendor_id 
            LEFT JOIN offer_category cc ON c.offer_id = cc.offer_id
            WHERE $chWhere
                AND v.activated = 1 AND DATE_ADD(v.expiration, INTERVAL 1 DAY) > NOW() AND m.active>0 AND v.disabled=0
            $timestampSql
            GROUP BY c.offer_id";*/

        Shared::debug($sql);
        $command = $db->createCommand($sql);
        $dataReader = $command->query();
        $offers = array();
        while (($row = $dataReader->read()) !== false) {
            // prepare images
            // get list of branches
            // get barcode image / code
            // fix branches (integer instead of string)
            $_branches = explode(',', $row['participating_branches']);
            $branches = array();
            foreach ($_branches as $branch) {
                $branches[] = $branch + 0;
            }

            $offers[$row['offer_id']] = array(
                'id' => $row['offer_id'] + 0,
                'title' => CHtml::encode($row['offer_title']),
                'description' => $row['offer_description'],
                'redemptionCode' => $row['redemption_code'],
                'customBarcode' => $row['custom_barcode'],
                'endDate' => $row['timestamp_sec'],
                'published' => ($row['vendor_expired'] == 1 ? '0' : $row['published']) + 0,
                //'icon' => Image::getImageUrl($row['offer_icon'], 80),
                'icon' => Image::getCachedBase64Src($row['offer_icon']),
                'branches' => $branches,
                'perCapitaLimitNumber' => $row['per_capita_limit_number'] + 0,
                'perCapitaLimitUnit' => $row['per_capita_limit_unit'],
                // -1 means it is expired, 0 - limit is not applied, otherwise show how many offers are available
                'globalLimit' => $row['global_limit'] > 0 ? $row['global_limit'] - $row['redemptions'] > 0 ? $row['global_limit'] - $row['redemptions'] : -1  : 0,
                'redemptionInstructions' => $row['redemption_instructions'],
                'redemptionTypes' => explode(',', $row['redemption_types']),
                'vendorId' => $row['vendor_id'] + 0,
                'categories' => $row['categories']
            );
        }
        // update impressions in one query
        if (count($offers)) {
            $db->createCommand("UPDATE offer SET impressions = impressions + 1 WHERE offer_id IN(" . implode(",", array_keys($offers)) . ")")->execute();
        }

        return $offers;
    }

    public function getParticipatingBranches() {
        if (strlen($this->participating_branches) == 0) {
            return $this->vendor->getLimitedBranches();
        } else {
            return Branch::model()->findAllBySql("SELECT * FROM branch WHERE branch_id IN({$this->participating_branches})");
        }
    }

    public function getImageUrl($width = 80) {
        if (strlen($this->offer_icon) > 0) {
            return url('/image/display', array('img' => $this->offer_icon, 'w' => $width));
        } else {
            // default image
            return url('/images/no-category-' . $width . '.png');
        }
    }

    /**
     * Recieve data from a cellphone through API and create additional stats objects
     * related to this offer. This transaction should be executed with locked table to prevent
     * incorrect statistics.
     * 
     * @param type $member
     * @param type $offer array(
     * array('offer_id','clicks'=>array(array(date,lat,lon)),'redemptions'=>array(array(date,lat,lon, [ticket_value], [redemption_type], [branch_id])),'favorite'=>false)
     * )
     */
    public function syncData($member, $offer, $data) {
        $memberOffer = MemberOffer::model()->with(array('clicks', 'redemptions'))->findByAttributes(array('member_id' => $member->member_id, 'offer_id' => $offer->offer_id));

        if (!is_object($memberOffer)) {
            // create one
            $memberOffer = MemberOffer::createModel('MemberOffer', array(
                        'member_id' => $member->member_id,
                        'offer_id' => $offer->offer_id,
                        'vendor_id' => $offer->vendor_id,
                        'favorite' => $data['favorite']
                    ));
        } else {

            if ($data['favorite'] != $memberOffer->favorite) {
                $memberOffer->favorite = $data['favorite'];
            }
        }
        $memberOffer->save();
        Shared::debug($memberOffer->getErrors());

        // now save the clicks and redeems
        if (isset($data['clicks'])) {
            foreach ($data['clicks'] as $click) {
                $timestamp = floor(($click['date'] + 0) / 1000);
                $time = date('Y-m-d H:i:s', $timestamp);
                // create new click ... later on we can check for duplicity
                $model = OfferClick::createModel('OfferClick', array(
                            'member_offer_id' => $memberOffer->member_offer_id,
                            'click_date' => $time
                        ));
                if (isset($click['lat'])) {
                    $model->lat = $click['lat'];
                    $model->lon = $click['lon'];
                }
                Shared::debug("click saved");
                $model->save();
            }

            // update statistics on a offer
            $offer->clicks += count($data['clicks']);
        }

        if (isset($data['redemptions'])) {
            foreach ($data['redemptions'] as $redemption) {
                $timestamp = floor(($redemption['date'] + 0) / 1000);
                $time = date('Y-m-d H:i:s', $timestamp);

                // create new click ... later on we can check for duplicity
                $model = Redemption::createModel('Redemption', array(
                            'member_offer_id' => $memberOffer->member_offer_id,
                            'redemption_activity' => $time
                        ));

                if (isset($click['lat'])) {
                    $model->lat = $click['lat'];
                    $model->lon = $click['lon'];
                }

                if (isset($redemption['redemption_type'])) {
                    $model->redemption_type = $redemption['redemptionType'];
                }

                if (isset($redemption['ticket_value'])) {
                    $model->ticket_value = $redemption['ticketValue'];
                }

                // TODO: find different way (GPS, QR code) to do this
                if (isset($redemption['branch_id'])) {
                    $model->branch_id = $redemption['branch_id'];
                }
                Shared::debug("redemption saved");
                // TODO - if lat lon match the branch, use it

                $model->save();
                $offer->sendRedemptionEmail($model);
            }
            $offer->redemptions += count($data['redemptions']);

            // if there is global limit set, we need to mark it as updated, so
            // api will advertise new "remaining offers" number
            if ($offer->global_limit > 0) {
                $offer->updated_on = Shared::toDatabase(time());
            }
        }
        $offer->save();
    }

    /**
     * Sends an email to all the users subscribed for redemption emails.
     * @param type $redemption
     */
    public function sendRedemptionEmail($redemption) {
        $member = $redemption->campaignMember->member;
        $mail = new AOEmail('offerRedemption');
        $mail->addPlaceholders(array(
            'offer_name' => CHtml::encode($this->offer_title),
            'member_name' => CHtml::encode($member->getFullName()),
            'start_date' => Shared::formatShortUSDate($this->start_date),
            'end_date' => Shared::formatShortUSDate($this->end_date),
            'redemption_date' => Shared::formatShortUSDate($redemption->redemption_activity),
            'redemption_count' => $this->redemptions,
            'click_count' => $this->clicks,
            'impression_count' => $this->impressions,
            'offer_description' => $this->offer_description
        ));

        // collect all interested users
        $users = UserVendor::Model()->findAllByAttributes(array('vendor_id' => $this->vendor_id));
        foreach ($users as $_user) {
            if ($_user->active && $_user->send_redemption_email) {
                $mail->addRecipient($_user->user->email_address, $_user->user->getFullName());
            }
        }

        // temporary monitoring
        $mail->addRecipient('admin@ausaoffers.com', 'AUSA Offers Admin');
        $mail->send();
    }

    /**
     * Get information about publishing dates and limits displayed in the edit
     * form. This can be displayed directly or through ajax callback after
     * date change in create /update form
     * @param type $startDate
     * @param type $endDate
     * @return array Array of the following keys:
     *  limits: textual representation how many offers is vendor allowed to publish
     *  colisions: array of coliding offfers in the selected date range. Contains title, offerId, startDate, endDate
     *  canPublish: boolean, can the vendor publish this offer right now in the selected date range?
     */
    public function getPublishingInfo($startDate = null, $endDate = null) {
        if ($startDate != null)
            $this->start_date = $startDate;
        if ($endDate != null)
            $this->end_date = $endDate;

        // check limits
        $output = array();
        if (!$this->vendor_id) {
            $this->vendor = Vendor::model()->findByPk(app()->user->getActiveVendor());
            $this->vendor_id = $this->vendor->vendor_id;
        }

        if ($this->vendor->max_offers == 0) {
            $output['limits'] = "You cannot publish the offer yet. Please activate your account first.";
        } else if ($this->vendor->max_offers == 1) {
            $output['limits'] = "You can publish one offer at time.";
        } else {
            $output['limits'] = "You can publish up to " . $this->vendor->max_offers . " offers at time.";
        }

        $output['canPublish'] = true;

        // check vendor expiration
        if ($this->vendor->max_offers > 0) {
            if (strtotime($this->end_date) > strtotime($this->vendor->expiration)) {
                $output['limits'] .= " Your subscription expires on " . Shared::formatDateShort($this->vendor->expiration) . ". If you don't extend your subscription by this day, your offer might be unpublished. ";
            } else if (strtotime($this->end_date) < strtotime($this->vendor->expiration)) {
                if (strtotime($this->vendor->expiration) > time()) {
                    $output['limits'] .= " Your subscription will expire on " . Shared::formatDateShort($this->vendor->expiration) . '.';
                } else {
                    $output['canPublish'] = false;
                    $output['limits'] .= " Your subscription has expired. Please activate your account first.";
                }
            }
        }

        // check coliding offers
        $colliding = Offer::model()->findAllBySql("SELECT offer_id, offer_title, start_date, end_date, published FROM offer 
                WHERE vendor_id={$this->vendor_id} AND published > 0 " . ($this->isNewRecord ? '' : ' AND offer_id != ' . $this->offer_id) . " AND(
                (start_date<='{$this->start_date}' AND end_date >= '{$this->start_date}') OR
                (start_date<='{$this->end_date}' AND end_date >='{$this->end_date}') OR
                (start_date>='{$this->start_date}' AND end_date <='{$this->end_date}'))");
        $output['colisions'] = array();
        foreach ($colliding as $offer) {
            $output['colisions'][] = array(
                'title' => $offer->offer_title,
                'offerId' => $offer->offer_id,
                'start_date' => Shared::formatShortUSDate($offer->start_date),
                'end_date' => Shared::formatShortUSDate($offer->end_date),
            );
        }

        if (count($colliding) >= $this->vendor->max_offers && count($colliding) > 0) {
            $output['canPublish'] = false;
            $output['limits'] .= " <b>Please select different publishing date. It has not to colide with the dates below.</b>";
        }

        return $output;
    }

}
<?php

/**
 * This is the model class for table "vendor_salesman".
 *
 * The followings are the available columns in table 'vendor_salesman':
 * @property string $vendor_salesman_id
 * @property string $vendor_id
 * @property string $user_id
 * @property string $created_on
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Vendor $vendor
 */
class VendorSalesman extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return VendorSalesman the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'vendor_salesman';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vendor_id, user_id', 'length', 'max' => 10),
            array('created_on', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('vendor_salesman_id, vendor_id, user_id, created_on', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'vendor_salesman_id' => 'Vendor Salesman',
            'vendor_id' => 'Vendor',
            'user_id' => 'User',
            'created_on' => 'Created On',
        );
    }

    /**
     * Link a person from sales team to the vendor
     * @param type $user
     * @param type $vendor
     * @return \UserVendor|null
     */
    public static function create($user, $vendor){
        if (is_numeric($user)) $user = User::model()->findByPk($user);
        if (is_numeric($vendor)) $vendor = Vendor::model()->findByPk($vendor);
        if ($user != null && $vendor != null){
            $userVendor = self::model()->findByAttributes(array('user_id' => $user->user_id, 'vendor_id' => $vendor->vendor_id));
            if ($userVendor == null){
                $userVendor = new VendorSalesman;
                $userVendor->user_id = $user->user_id;
                $userVendor->vendor_id = $vendor->vendor_id;
                $userVendor->created_on = Shared::timeNow();
            }
            return $userVendor;
        }
        return null;
    }
}
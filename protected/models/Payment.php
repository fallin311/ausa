<?php

/**
 * This is the model class for table "payment".
 *
 * The followings are the available columns in table 'payment':
 * @property string $payment_id
 * @property string $vendor_id
 * @property string $ps_charge_id
 * @property string $created
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property string $transaction_id
 * @property double $amount
 * @property string $plan_id
 * @property integer $payment_completed
 * @property string $customer_ip
 * @property string $response
 *
 * The followings are the available model relations:
 * @property Plan $plan
 * @property Vendor $vendor
 */
class Payment extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Payment the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'payment';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('created', 'required'),
            array('payment_completed', 'numerical', 'integerOnly' => true),
            array('amount', 'numerical'),
            array('vendor_id, plan_id', 'length', 'max' => 10),
            array('ps_charge_id', 'length', 'max' => 63),
            array('description, response', 'length', 'max' => 255),
            array('transaction_id', 'length', 'max' => 127),
            array('customer_ip', 'length', 'max' => 16),
            array('start_date, end_date', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('payment_id, vendor_id, ps_charge_id, created, description, start_date, end_date, transaction_id, amount, plan_id, payment_completed, customer_ip, response', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'plan' => array(self::BELONGS_TO, 'Plan', 'plan_id'),
            'vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'payment_id' => 'Payment',
            'vendor_id' => 'Vendor',
            'ps_charge_id' => 'Ps Charge',
            'created' => 'Created',
            'description' => 'Description',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'transaction_id' => 'Transaction',
            'amount' => 'Amount',
            'plan_id' => 'Plan',
            'payment_completed' => 'Payment Completed',
            'customer_ip' => 'Customer Ip',
            'response' => 'Response',
        );
    }
    
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    /* public function search()
      {
      // Warning: Please modify the following code to remove attributes that
      // should not be searched.

      $criteria=new CDbCriteria;

      $criteria->compare('payment_id',$this->payment_id,true);
      $criteria->compare('vendor_id',$this->vendor_id,true);
      $criteria->compare('ps_charge_id',$this->ps_charge_id,true);
      $criteria->compare('created',$this->created,true);
      $criteria->compare('description',$this->description,true);
      $criteria->compare('start_date',$this->start_date,true);
      $criteria->compare('end_date',$this->end_date,true);
      $criteria->compare('transaction_id',$this->transaction_id,true);
      $criteria->compare('amount',$this->amount);
      $criteria->compare('plan_id',$this->plan_id,true);
      $criteria->compare('payment_completed',$this->payment_completed);
      $criteria->compare('customer_ip',$this->customer_ip,true);
      $criteria->compare('response',$this->response,true);

      return new CActiveDataProvider($this, array(
      'criteria'=>$criteria,
      ));
      } */
}
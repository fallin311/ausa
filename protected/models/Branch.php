<?php

/**
 * This is the model class for table "branch".
 *
 * The followings are the available columns in table 'branch':
 * @property string $branch_id
 * @property string $vendor_id
 * @property integer $active
 * @property string $branch_name
 * @property string $branch_address
 * @property string $phone_number
 * @property string $zipcode
 * @property string $website
 * @property string $hours
 * @property double $lat
 * @property double $lon
 *
 * The followings are the available model relations:
 * @property Vendor $vendor
 */
class Branch extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Branch the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'branch';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vendor_id, branch_name, branch_address', 'required'),
            array('active', 'numerical', 'integerOnly' => true),
            array('lat, lon', 'numerical'),
            array('hours', 'application.components.validators.AOPhoneTextValidator'),
            array('active', 'maxBranchesValidator'),
            array('vendor_id', 'length', 'max' => 10),
            array('branch_name, website', 'length', 'max' => 127),
            array('branch_address, hours', 'length', 'max' => 255),
            array('phone_number, zipcode', 'length', 'max' => 12),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('branch_id, vendor_id, active, branch_name, branch_address, phone_number, zipcode, website, hours, lat, lon', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),
        );
    }

    /* public function scopes() {
      return array(
      'active'=>array(
      // load only allowed number of branches (vendor paid for)
      'limit'=>'published=' . $this->max_branches,
      )
      );
      } */

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'branch_id' => 'Branch',
            'vendor_id' => 'Vendor',
            'active' => 'Active',
            'branch_name' => 'Branch Name',
            'branch_address' => 'Branch Address',
            'phone_number' => 'Phone Number',
            'zipcode' => 'Zipcode',
            'website' => 'Website',
            'hours' => 'Hours',
            'lat' => 'Lat',
            'lon' => 'Lon',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    /* public function search()
      {
      // Warning: Please modify the following code to remove attributes that
      // should not be searched.

      $criteria=new CDbCriteria;

      $criteria->compare('branch_id',$this->branch_id,true);
      $criteria->compare('vendor_id',$this->vendor_id,true);
      $criteria->compare('active',$this->active);
      $criteria->compare('branch_name',$this->branch_name,true);
      $criteria->compare('branch_address',$this->branch_address,true);
      $criteria->compare('phone_number',$this->phone_number,true);
      $criteria->compare('zipcode',$this->zipcode,true);
      $criteria->compare('website',$this->website,true);
      $criteria->compare('hours',$this->hours,true);
      $criteria->compare('lat',$this->lat);
      $criteria->compare('lon',$this->lon);

      return new CActiveDataProvider($this, array(
      'criteria'=>$criteria,
      ));
      } */

    /**
     * Branch can be default (created with vendor) or from the form. In general,
     * there is a limit on branches and we want to check it before branch is
     * created.
     * @param type $input
     */
    public static function create($input) {
        // validation rule or should we check the limit here? TODO
        $branch = new Branch;
        $branch->attributes = $input;
        return $branch;
    }

    /**
     * Check that we have branches in limit
     * @param type $attribute
     * @param type $params
     */
    public function maxBranchesValidator($attribute, $params) {
        if ($this->active && count($this->vendor->activeBranches) >= $this->vendor->max_branches) {
            // is it one of them?
            if (count($this->vendor->activeBranches) == $this->vendor->max_branches) {
                foreach ($this->vendor->activeBranches as $branch) {
                    if ($branch->branch_id == $this->branch_id) {
                        return true;
                    }
                }
            }
            $this->addError('active', 'Cannot activate this branch. Your account has maximum limit of ' . $this->vendor->max_branches . ' active branches.');
            return false;
        }
        return true;
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->getOriginalAttribute('branch_address') != $this->branch_address) {
            Yii::import('application.helpers.AddressHelper');
            $address = AddressHelper::toLatLon($this->branch_address);
            if (isset($address['formatted'])) {
                $this->branch_address = $address['formatted'];
            }
            if (isset($address['lat'])){
                $this->lat = $address['lat'];
                $this->lon = $address['lon'];
            }
        }
        
        // update the vendor, so the API shows the current data
        if ($this->isDirty()){
            $this->vendor->updated_on = Shared::toDatabase(time());
            $this->vendor->save();
        }
        return true;
    }

}
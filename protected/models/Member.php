<?php

/**
 * This is the model class for table "member". The model might user 
 * UploadMemberBehavior and ParamsBehavior
 *
 * The followings are the available columns in table 'member':
 * @property string $member_id
 * @property integer $ausa_id
 * @property string $prefix
 * @property string $first_name
 * @property string $last_name
 * @property string $email_address
 * @property string $phone_number
 * @property string $address1
 * @property string $address2
 * @property string $city
 * @property string $state
 * @property string $zipcode
 * @property string $country
 * @property string $military_unit
 * @property string $army_status
 * @property date $birthday
 * @property string $lat
 * @property string $lon
 * @property string $access_pin
 * @property string $access_token
 * @property string $source
 * @property string $updated_on
 * @property string $phone_identifier
 * @property string $last_phone_activation
 * @property string $last_login
 *
 * The followings are the available model relations:
 * @property Vendor[] $vendors
 * @property MemberOffer[] $memberOffers
 * @property Membership[] $memberships
 */
class Member extends AOActiveRecord {

    /**
     * Army statuses - taken from the AUSA registration form
     * @var type 
     */
    public static $statuses = array(
        'AD' => 'Active Component',
        'AG' => 'Active Guard and Reserve',
        'AR' => 'Army Reserve',
        'CIV' => 'Civilian',
        'FC' => 'Foreign National',
        'GC' => 'Government Civilian',
        'KR' => 'Cadet',
        'MC' => 'Other US Armed Services',
        'NG' => 'National Guard',
        'RA' => 'Retired Army',
        'VC' => 'Veteran'
    );

    /**
     * Name prefixes taken from the official AUSA registration form
     * @var type 
     */
    public static $prefixes = array(
        "" => "",
        "1LT" => "1LT",
        "1SG" => "1SG",
        "2LT" => "2LT",
        "A1C" => "A1C",
        "ADM" => "ADM",
        "BG" => "BG",
        "CAPT" => "CAPT",
        "CDR" => "CDR",
        "CDT" => "CDT",
        "CMSG" => "CMSG",
        "COL" => "COL",
        "CPL" => "CPL",
        "CPO" => "CPO",
        "CPT" => "CPT",
        "CSGT" => "CSGT",
        "CSM" => "CSM",
        "CW2" => "CW2",
        "CW3" => "CW3",
        "CW4" => "CW4",
        "CW5" => "CW5",
        "CWO" => "CWO",
        "Dr." => "Dr.",
        "DSC" => "DSC",
        "ENS" => "ENS",
        "ENS1" => "ENS1",
        "GA" => "GA",
        "GEN" => "GEN",
        "HON" => "Hon.",
        "LCDR" => "LCDR",
        "LTC" => "LTC",
        "LTG" => "LTG",
        "LTJG" => "LTJG",
        "MAJ" => "MAJ",
        "MG" => "MG",
        "MISS" => "Miss",
        "Mr." => "Mr.",
        "Mrs." => "Mrs.",
        "Ms." => "Ms.",
        "MSG" => "MSG",
        "OC" => "OC",
        "PFC" => "PFC",
        "PSG" => "PSG",
        "PV2" => "PV2",
        "PVT" => "PVT",
        "RAD" => "RAD",
        "SA" => "SA",
        "SFC" => "SFC",
        "SGM" => "SGM",
        "SGT" => "SGT",
        "SMA" => "SMA",
        "SPC" => "SPC",
        "SSG" => "SSG",
        "TSGT" => "TSGT",
        "VADM" => "VADM",
        "WO1" => "WO1",
    );

    /**
     * Virtual attribute used during upload and registration to get full name in 
     * one field in order to eliminate one text field
     * @var string 
     */
    public $fullName;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Member the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'member';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('first_name, last_name', 'required'),
            array('email_address, ausa_id', 'unique', 'except' => 'register,upload'),
            array('phone_number', 'unique'),
            array('email_address, phone_number, ausa_id', 'default', 'setOnEmpty' => true),
            array('ausa_id', 'numerical', 'integerOnly' => true),
            array('prefix', 'length', 'max' => 10),
            array('lat, lon, activated', 'numerical'),
            array('fullName, email_address, phone_number, address1, city, zipcode, state', 'required', 'on' => 'register'),
            array('email_address, phone_number', 'idRequired'),
            array('first_name, last_name, access_pin, access_token, phone_identifier, army_status', 'length', 'max' => 63),
            array('email_address, address1, address2, military_unit, fullName', 'length', 'max' => 127),
            array('phone_number, zipcode', 'length', 'max' => 12),
            array('city, state', 'length', 'max' => 45),
            array('source', 'length', 'max' => 31),
            array('updated_on, last_phone_activation, last_login, birthday, params', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('member_id, ausa_id, prefix, first_name, last_name, email_address, phone_number, address1, address2, city, state, zipcode, access_pin, access_token, source, updated_on, phone_identifier, last_phone_activation, last_login', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'favoriteStores' => array(self::MANY_MANY, 'Vendor', 'favorite_store(member_id, vendor_id)'),
            'offers' => array(self::HAS_MANY, 'MemberOffer', 'member_id'),
            'memberships' => array(self::HAS_MANY, 'Membership', 'member_id'),
            'membership' => array(self::HAS_ONE, 'Membership', 'member_id'),
        );
    }

    /**
     * Used to get expiration date and chapter.
     * @return null
     */
    public function getMembership() {
        foreach ($this->memberships as $membership) {
            if ($membership->active)
                return $membership;
        }
        return null;
    }

    public function getFullName() {
        $name = strlen($this->prefix) ? ($this->prefix . ' ') : '';
        $name = $name . $this->first_name . ' ' . $this->last_name;
        if (strlen($name) < 3)
            $name = 'member';
        return $name;
    }

    public function getFullAddress() {
        if (strlen($this->address2)) {
            return $this->address1 . ', ' . $this->address2;
        }
        return $this->address1;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'member_id' => 'Member',
            'ausa_id' => 'Ausa Member ID',
            'prefix' => 'Rank/Prefix',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email_address' => 'Email Address',
            'phone_number' => 'Mobile Number',
            'address1' => 'Address1',
            'address2' => 'Address2',
            'city' => 'City',
            'state' => 'State',
            'zipcode' => 'Zipcode',
            //'country' => 'Country',
            'access_pin' => 'Access Pin',
            'access_token' => 'Access Token',
            'source' => 'Source',
            'updated_on' => 'Updated On',
            'phone_identifier' => 'Phone Identifier',
            'last_phone_activation' => 'Last Phone Activation',
            'last_login' => 'Last Login',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->with = 'membership';
        if (app()->user->getActiveChapter()) {
            $criteria->condition = 'membership.chapter_id=' . app()->user->getActiveChapter();
        }

        $criteria->compare('member_id', $this->member_id, true);
        $criteria->compare('ausa_id', $this->ausa_id);
        $criteria->compare('prefix', $this->prefix, true);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('email_address', $this->email_address, true);
        $criteria->compare('phone_number', $this->phone_number, true);
        $criteria->compare('address1', $this->address1, true);
        $criteria->compare('address2', $this->address2, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('state', $this->state, true);
        $criteria->compare('zipcode', $this->zipcode, true);
        //$criteria->compare('country', $this->country, true);



        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => array(
                    'updated_on' => array(
                        'default' => 'desc',
                ))),
            'pagination' => array(
                'pageSize' => 20,
            ),
                ));
    }

    /**
     * Parametrizied scope. We want to find authenticated user
     * in the database with each request
     */
    public function byToken($token) {
        $token = preg_replace("/[^a-zA-Z0-9]/", '', $token);
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "access_token='$token'",
        ));
        return $this;
    }

    /**
     * Validation rule
     * We need to be able to find a member somehow, eiterh email, phone or ausa
     * @param type $attribute_name
     * @param type $params
     * @return boolean
     */
    public function idRequired($attribute_name, $params) {
        if (empty($this->email_address) && empty($this->phone_number) && empty($this->ausa_id)
        ) {
            $this->addError($attribute_name, Yii::t('user', 'At least 1 of the following has to be set up: email, phone, ausa id'));
            return false;
        }
        return true;
    }

    /**
     * Find the member by either email or phone number
     * @param type $email
     * @param type $phone
     * @return null|Member
     */
    public static function findByWhatever($email = null, $phone = null, $ausa_id = null) {
        $model = null;
        if ($ausa_id != null) {
            //Shared::debug("looking up by ausa_id " . $ausa_id);
            $model = self::model()->findByAttributes(array('ausa_id' => $ausa_id));
        }
        if ($model == null && $email != null) {
            //Shared::debug("looking up by email " . $email);
            $model = self::model()->findByAttributes(array('email_address' => strtolower($email)));
        }
        if ($model == null && $phone != null) {
            // TODO: sanitize phone number and try to add / remove country code
            $formats = array('###-###-####', '####-###-###', '#########',
                '(###)###-###', '####-####-####', '(###)###-####', '#(###)###-####',
                '##-###-####-####', '####-####', '###-###-###',
                '#####-###-###', '##########', '+############', '00############');
            $format = preg_replace("/[0-9]/", "#", $phone);
            $format = str_replace(' ', '-', $format);
            if (in_array($format, $formats)) {

                $phone = str_replace("-", '', $phone);
                $phone = str_replace("(", '', $phone);
                $phone = str_replace(")", '', $phone);
                Shared::debug("looking up by phone " . $phone);
                $model = self::model()->findByAttributes(array('phone_number' => $phone));
                Shared::debug($model);
                if ($model == null) {
                    Shared::debug("still not found, last try with area code");
                    // try it with the US country code
                    $model = self::model()->findByAttributes(array('phone_number' => '1' . $phone));
                }
            }
        }
        return $model;
    }

    /**
     * Returns comma separated mailing address on one line
     * @return string
     */
    public function getMailingAddress() {
        if (strlen($this->address1)) {
            $address = $this->address1;
            if (strlen($this->address2)) {
                $address .= ', ' . $this->address2;
            }
            if (strlen($this->city)) {
                $address .= ', ' . $this->city;
            }
            if (strlen($this->state)) {
                $address .= ', ' . $this->state;
            }
            if (strlen($this->zipcode)) {
                $address .= ', ' . $this->zipcode;
            }
            if (strlen($this->country)) {
                $address .= ', ' . $this->country;
            }
            return $address;
        } else {
            return '';
        }
    }

    /**
     * We want to update GPS coordinates in case there is a change in the address
     */
    public function beforeSave() {
        if (parent::beforeSave()) {
            if (strlen($this->address1) > 3 && ($this->getOriginalAttribute('address1') != $this->address1 || $this->getOriginalAttribute('address2') != $this->address2 || $this->getOriginalAttribute('city') != $this->city || $this->getOriginalAttribute('zipcode') != $this->zipcode || $this->lat == null)) {
                Yii::import('application.helpers.AddressHelper');
                $address = AddressHelper::toLatLon($this->getMailingAddress());

                if (isset($address['lat'])) {
                    $this->lat = $address['lat'];
                    $this->lon = $address['lon'];
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Find a user and authenticate. Returns user or null if
     * user is not found.
     * @param type $login
     * @param type $pin
     * @return false|Member returns member if the authentication is successful
     */
    public static function apiAuthenticate($login, $pin) {
        // no email or phone problem (need to know ausa_id)
        $member = Member::findByWhatever($login, $login, $login);
        if (is_object($member)) {
            //Shared::debug($member->attributes);
            // lets see if the password matches
            if ($pin == $member->access_pin) {
                // success, lets handle it
                $member->createAccessToken();
                $member->save();
                return $member;
            } else {
                Shared::debug("password does not match: $pin");
            }
        }
        return null;
    }

    public static function forgotPin($login, $newEmail = null) {
        $email = Shared::isEmailValid(urldecode($login)) ? urldecode($login) : null;

        // we can send email directly when member inputs email
        if ($email != null) {
            $member = Member::findByWhatever($email);
        } else {
            // it should lookup with and without country code
            $member = Member::findByWhatever(null, $login, (int) $login);
            //Shared::debug("Lookup member by $login");
        }

        // prepare the response array
        $response = array(
            'success' => 0,
            'emailSent' => 0,
            'emailFound' => 0,
            'errorMessage' => ''
        );
        if ($member == null) {
            Shared::debug("new pin cannot find the login - " . $login);
            $response['errorMessage'] = 'We cannot recognize your login information.';
        } else {
            // the member has to be active to use reset pin feature
            if ($member->membership->isActive()) {
                $response['success'] = 1;

                // There is no email address associated for this member
                // Phone has to send an extra request, which will set an email address
                if ($member->email_address == null || strlen($member->email_address) < 3) {
                    Shared::debug("creating new email address");
                    if ($newEmail == null) {
                        // tell the phone it has to ask for email
                        Shared::debug("ask the mofo for the email, we don't have it");
                        $response['member'] = $member->getInfoForApi(false);
                    } else {
                        Shared::debug("we got new email $newEmail");
                        Yii::import('application.models.behaviors.UploadMemberBehavior');
                        $member->attachBehavior('uploadMember', new UploadMemberBehavior());
                        // update member with this email
                        $member->setEmail($newEmail);
                        if (strlen($member->email_address) > 3) {
                            // The email is valid. Update member and send him reset password email.
                            $member->updated_on = Shared::toDatabase(time());
                            $member->createPin();
                            $member->save();

                            $member->sendPinEmail();
                            
                            $response['emailSent'] = 1;
                            $response['emailFound'] = 1;
                            $response['member'] = $member->getInfoForApi();
                        } else {
                            // the email is not valid
                            $response['member'] = $member->getInfoForApi(false);
                        }
                    }
                } else if ($email == null) {
                    // This member was found using a phone number, however, we have the email address as well
                    $response['member'] = $member->getInfoForApi(false);
                    $response['emailFound'] = 1;
                } else {
                    Shared::debug("sending him new pin");
                    // did he log in using email? Great! we can just use it
                    $member->createPin();
                    $member->save();
                    
                    $member->sendPinEmail();
                    
                    $response['emailSent'] = 1;
                    $response['emailFound'] = 1;
                    $response['member'] = $member->getInfoForApi();
                }
            } else {
                $response['errorMessage'] = 'Your AUSA membership has expired. Please contact your chapter.';
            }
        }
        Shared::debug($response);
        return $response;
    }

    private function sendPinEmail() {

        // send the email here ... TODO
        $mail = new AOEmail('newPin');

        // information about membership in all chapters and their expirations
        $expiration = '';
        foreach ($this->memberships as $thisship) {
            $expiration .= ", " . $thisship->chapter->chapter_name . ' ' . Shared::formatDateLonger($thisship->expiration_date);
        }
        $mail->addPlaceholders(array(
            'email_address' => $this->email_address ? $this->email_address : 'N/A',
            'full_name' => $this->getFullName(),
            'pin' => $this->access_pin,
            'ausa_id'  => $this->ausa_id ? $this->ausa_id : 'N/A',
            'phone_number' => $this->phone_number ? $this->phone_number : 'N/A',
            'membership_expiration' => substr($expiration, 2),
                )
        );
        $mail->addRecipient($this->email_address, $this->getFullName());
        $mail->send();
    }

    /**
     * Access token used for API through cell phone
     */
    public function createAccessToken() {
        $this->access_token = Shared::generateRandomPassword(32);
        $this->last_login = Shared::timeNow();
        $this->updated_on = Shared::timeNow();
    }

    /**
     * Pin / password used for login
     */
    public function createPin() {
        $this->access_pin = Shared::generateCode(5);
    }

    /**
     * Returns multi-dimensional member array used in json api
     * @param $fullInfo Return information used after login including api key and gps
     */
    public function getInfoForApi($fullInfo = true, $membership = null) {
        if ($membership == null) {
            $membership = $this->membership;
        }

        $member = array(
            'id' => $this->member_id,
            'ausaId' => $this->ausa_id,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'prefix' => $this->prefix,
            'emailAddress' => $this->email_address,
            // since member cannot update it, displaye it formatted
            'phoneNumber' => Shared::formatPhone($this->phone_number),
            'joinDate' => Shared::formatDateShort($this->membership->join_date),
            'demoUser' => 0,
            'chapter' => array(
                'id' => $membership->chapter->chapter_id,
                'name' => $membership->chapter->chapter_name,
                'city' => $membership->chapter->city,
            )
        );
        if ($fullInfo) {
            $member['lat'] = $this->lat; // we can use these as a default position before GPS signal is valid
            $member['lon'] = $this->lon;
            $member['authToken'] = $this->access_token; // the most important record
            $member['expiration'] = Shared::formatDateShort($membership->expiration_date);
        }
        return $member;
    }

}
<?php

/**
 * This is the model class for table "chapter".
 *
 * The followings are the available columns in table 'chapter':
 * @property string $chapter_id
 * @property integer $entity_id
 * @property integer $chapter_name
 * @property string $ausa_id
 * @property string $address
 * @property boolean $disabled
 * @property string $city
 * @property string $zipcode
 * @property string $country
 * @property string $offer_app_url
 * @property string $chapter_url
 * @property integer $timezone
 * @property string $logo
 *
 * The followings are the available model relations:
 * @property Membership[] $memberships
 * @property SalesTeam[] $salesTeams
 * @property User[] $users
 * @property Vendor[] $vendors
 */
class Chapter extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Chapter the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'chapter';
    }

    public function behaviors() {
        return array(
            'companyBehavior' => array(
                'class' => 'application.models.behaviors.CompanyBehavior'
            )
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('chapter_name, city, state, entity_id', 'required'),
            array('timezone, disabled', 'numerical', 'integerOnly' => true),
            array('ausa_id', 'length', 'max' => 16),
            array('chapter_name', 'length', 'max' => 31),
            array('city, state, zipcode, country', 'length', 'max' => 45),
            array('offer_app_url, address, chapter_url', 'length', 'max' => 127),
            array('logo', 'length', 'max' => 31),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('chapter_id, chapter_name, ausa_id, address, city, state, zipcode, country, offer_app_url, chapter_url, timezone, logo', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'memberships' => array(self::HAS_MANY, 'Membership', 'chapter_id'),
            'salesTeams' => array(self::HAS_MANY, 'SalesTeam', 'chapter_id'),
            'users' => array(self::MANY_MANY, 'User', 'user_chapter(chapter_id, user_id)'),
            'vendors' => array(self::MANY_MANY, 'Vendor', 'vendor_membership(chapter_id, vendor_id)'),
            'entity' => array(self::BELONGS_TO, 'Entity', 'entity_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'chapter_id' => 'Chapter',
            'chapter_name' => 'Chapter Name',
            'entity_id' => 'Entity Name',
            'ausa_id' => 'Ausa Chapter ID',
            'address' => 'Address',
            'state' => 'State',
            'city' => 'City',
            'zipcode' => 'Zipcode',
            'country' => 'Country',
            'offer_app_url' => 'Offer App Url',
            'chapter_url' => 'Website',
            'timezone' => 'Timezone',
            'logo' => 'Logo',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('chapter_id', $this->chapter_id, true);
        $criteria->compare('chapter_name', $this->chapter_name);
        $criteria->compare('ausa_id', $this->ausa_id, true);
        $criteria->compare('address', $this->address);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('zipcode', $this->zipcode, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('offer_app_url', $this->offer_app_url, true);
        $criteria->compare('chapter_url', $this->chapter_url, true);
        $criteria->compare('timezone', $this->timezone);
        $criteria->compare('logo', $this->logo, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
                ));
    }

    /**
     * TODO: this really should not be here. Maybe vendor->getIndustryName() or something like that
     * @param type $industry
     * @return type
     */
    /*public function getIndustryName($industry) {
        $connection = app()->db;
        $query = "SELECT category_name FROM category WHERE category_id='" . $industry . "';";
        $run = $connection->createCommand($query);
        $industryName = $run->queryScalar();
        return $industryName;
    }*/

    /**
     * @return total number of vendors in a chapter
     * @author Vinayak Melarkod <vk@inradiussytems.com>
     */
    public function getTotalVendorCount() {

        $connection = Yii::app()->db;
        $sql = "SELECT COUNT(m.vendor_id) FROM vendor_membership m
            LEFT JOIN vendor v ON m.vendor_id = v.vendor_id 
            WHERE chapter_id=" . $this->chapter_id . " AND m.active=1 AND v.disabled=0 AND v.expiration>= NOW() AND v.published=1";
        $command = $connection->createCommand($sql);
        $value = $command->queryScalar();
        return $value;
    }

    /**
     * @return total number of vendors in a chapter
     * @author Vinayak Melarkod <vk@inradiussystems.com>
     */
    public function getTotalMemberCount() {

        $connection = Yii::app()->db;
        $sql = "SELECT COUNT(membership_id) FROM membership WHERE chapter_id=" . $this->chapter_id . " AND active=1 AND expiration_date>= NOW()";
        $command = $connection->createCommand($sql);
        $value = $command->queryScalar();
        return $value;
    }

    /**
     * @return total number of active offers by a Sales Team within a chapter
     * @author Vinayak Melarkod <vk@inradiussystems.com>
     */
    public function getTotalActiveOffers() {

        $connection = Yii::app()->db;
        $sql = "SELECT COUNT(offer_id) FROM offer c
                    LEFT JOIN vendor_membership vm
                    ON c.vendor_id = vm.vendor_id
                    WHERE vm.chapter_id=" . $this->chapter_id . " AND vm.active=1 AND
                    c.start_date<=DATE(NOW()) AND c.end_date>=DATE(NOW()) AND c.published=1;";
        $command = $connection->createCommand($sql);
        $value = $command->queryScalar();
        return $value;
    }

    /**
     * Return array of relavent branch data for a chapters vendors. This array
     * is being returned in JSON format to the view and rendered in a map.
     * @param $vendor single vendor id (vendor view) or array of vendor ids (ST view) or keep empty to display all vendors from this chapter
     */
    public function getVendorBranches($vendor = null) {
        $connection = app()->db;
        $branches = array();

        $vendors = "";
        if (is_array($vendor)) {
            $vendors = "AND b.vendor_id IN(" . implode(',', $vendor) . ")";
        } else if ($vendor > 0) {
            $vendors = "AND b.vendor_id = " . (int) $vendor;
        }

        $sql = "SELECT b.branch_id, b.vendor_id, b.lat, b.lon, c.marker
            FROM vendor v
            JOIN branch b ON v.vendor_id = b.vendor_id
            JOIN category c ON c.category_id = v.industry
            WHERE v.published=1 $vendors
            AND v.disabled = 0 AND DATE_ADD(expiration, INTERVAL 1 DAY)>NOW()";
        //Shared::debug($sql);
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();

        $iconUrl = url('/images/mapMarkers');

        foreach($dataReader as $row) {
            $branches[] = array('latLng' => array($row['lat'], $row['lon']),
                'data' => "<div data-vendor_id='" . $row['vendor_id'] . "' data-branch_id='" .$row['branch_id'] . "'></div>",
                'options' => array('icon' => $iconUrl . '/' . $row['marker'] . '.png', 'shadow' => $iconUrl . '/icon-shadow.png'));
        }
        
        return $branches;
    }

}
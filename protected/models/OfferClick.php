<?php

/**
 * This is the model class for table "offer_click".
 *
 * The followings are the available columns in table 'offer_click':
 * @property string $offer_click_id
 * @property string $member_offer_id
 * @property string $click_date
 * @property double $lat
 * @property double $lon
 *
 * The followings are the available model relations:
 * @property MemberOffer $memberOffer
 */
class OfferClick extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return OfferClick the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'offer_click';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('member_offer_id, click_date', 'required'),
            array('lat, lon', 'numerical'),
            array('member_offer_id', 'length', 'max' => 10),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('offer_click_id, member_offer_id, click_date, lat, lon', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'memberOffer' => array(self::BELONGS_TO, 'MemberOffer', 'member_offer_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'offer_click_id' => 'Offer Click',
            'member_offer_id' => 'Member Offer',
            'click_date' => 'Click Date',
            'lat' => 'Lat',
            'lon' => 'Lon',
        );
    }


}
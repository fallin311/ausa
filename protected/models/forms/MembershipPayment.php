<?php

/**
 * Collect data for payment for regular membership. This form is sent
 * to USA Epay
 */
class MembershipPayment extends CFormModel {

    public $useBillingAddress;
    public $firstName;
    public $lastName;
    public $address1;
    public $address2;
    public $city;
    public $zipcode;
    public $state;
    public $country;
    public $phone;
    public $email;
    public $cardNumber;
    public $cardExpMonth;
    public $cardExpYear;
    public $cardCode;
    public $paymentAmount;

    /** Member model is used to get additional information for payment */
    public $member;
    public $chapter;
    public static $levels = array(
        1 => array(
            'title' => 'JUNIOR, W/O ARMY MAGAZINE, E1-E4, CADET/OCS',
            'price' => 14),
        2 => array(
            'title' => 'REGULAR, WITH ARMY MAGAZINE, E1-E4, CADET/OCS GS1-GS4',
            'price' => 21),
        3 => array(
            'title' => 'E5-E7, GS5-GS6',
            'price' => 26),
        4 => array(
            'title' => 'O1-O3,W1-W3,E8-E9,GS7-GS11,VETERAN',
            'price' => 31),
        5 => array(
            'title' => 'O4-O6,W4-W5,GS12-GS15,CIVILIAN',
            'price' => 34),
        6 => array(
            'title' => 'O7-O10,SES,ES',
            'price' => 39)
    );
    // we should implement checking of rank based on the level
    public $level = 1;

    public function loadDefaults($member) {
        $this->useBillingAddress = true;
    }

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        Yii::import('application.components.validators.ECCValidator');

        return array(
            array('firstName, lastName, address1, zipcode, paymentAmount, cardNumber, cardExpMonth, cardExpYear, cardCode', 'required'),
            array('paymentAmount, cardNumber, cardCode, cardExpMonth, cardExpYear, useBillingAddress, level', 'numerical'),
            array('cardNumber', 'ext.validators.ECCValidator',
                'format' => array(ECCValidator::MASTERCARD, ECCValidator::VISA, ECCValidator::AMERICAN_EXPRESS),
                'message' => 'Please check your credit card number. Note that Visa, Master Card and American Express are the only supported cards.'),
            array('city, company, contractExpiration, country, firstName, lastName, phone, recurringSchedule, state, address1, address2, zipcode, email', 'safe')
                //array('firstPaymentDate, paymentStartDate, contractExpiration', 'date'),
        );
    }

    public function attributeLabels() {
        return array(
            'useBillingAddress' => 'Reuse concat address',
            'cardNumber' => 'Credit Card Number',
            'cardExpMonth' => 'Expiration Month',
            'cardExpYear' => 'Expiration Year',
        );
    }

    /**
     * Validate and send the payment to the gateway
     */
    public function prepare($member = null) {

        if ($member != null) {
            $this->member = $member;
        } else {
            $member = $this->member;
        }

        // copy address from the member
        if ($this->useBillingAddress) {
            $this->firstName = $member->first_name;
            $this->lastName = $member->last_name;
            $this->address1 = $member->address1;
            $this->address2 = $member->address2;
            $this->city = $member->city;
            $this->zipcode = $member->zipcode;
            $this->state = $member->state;
            $this->country = $member->country;
            $this->email = $member->email_address;
            $this->phone = $member->phone_number;
        }

        if ($this->level > 0) {
            $this->paymentAmount = self::$levels[$this->level]['price'];
        }

        //$this->addError('cardNumber', 'Not yet...');
    }

    public function process() {
        $this->prepare();
        $result = array('success' => true);
        $result['amount'] = $this->paymentAmount;
        // send it out ... we need to capture how much did we recieve, substract
        // taxes and possibly display an error
        $transaction = app()->usaepaytransaction->getInstance($this->chapter);
        if (is_object($transaction)) {
            $transaction->card = $this->cardNumber;
            $transaction->exp = str_pad($this->cardExpMonth, 2, '0', STR_PAD_LEFT) . $this->cardExpYear;
            $transaction->amount = $this->paymentAmount;
            $transaction->invoice="member-" . $this->member->member_id . '-' . date('y');
            $transaction->cardholder = $this->firstName . ' ' . $this->lastName;
            $transaction->street = $this->address1;
            $transaction->zip = $this->zipcode;
            $transaction->description = 'One year membership in AUSA chapter ' . $this->chapter->chapter_name;
            $transaction->cvv2 = $this->cardCode;
            
            if ($transaction->Process()){
                $result['success'] = true;
            } else {
                $result['success'] = false;
                $result['error'] = $transaction->error;
            }
        }
        Shared::debug($transaction);
        return $result;
    }

    /**
     * Returns list data displayed in a payment form. The final price is calcualed based on these values.
     * @return string
     */
    public static function getLevelList() {
        $output = array();
        foreach (self::$levels as $lvl => $data) {
            $output[$lvl] = $data['title'] . ' - $' . $data['price'];
        }
        return $output;
    }

    /**
     * Return credit card number in xxxx format
     */
    public function obscureNumber() {
        if (strlen($this->cardNumber) > 10) {
            $last = substr($this->cardNumber, -4);
            return "xxxx xxxx xxxx " . $last;
        } else {
            return "invalid";
        }
    }

}

?>

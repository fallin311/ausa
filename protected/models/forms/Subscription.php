<?php

/**
 * Subscription class.
 * Subscription form collects information on how many offers and banners a vendor 
 * is subscribed to. It also collects the billing information and sends it to the 
 * payment gateway. It is used by the 'subscription' action of 'VendorController'.
 */
class Subscription extends CFormModel {

    public $paymentMode;
    public $maxOffers;
    public $maxBranches;
    public $maxBanners;
    public $maxCategories;
    //public $firstPaymentAmt;
    public $firstPaymentDate;
    public $paymentAmount;
    public $singlePaymentPrice; //single payment amount
    public $monthlyRecurringPrice;
    public $recurringSchedule;
    //public $nextPaymentDate;
    public $subscriptionNotes;
    public $contractExpiration;
    public $paymentType;
    //public $cardType;
    public $cardNumber;
    public $cardExpMonth;
    public $cardExpYear;
    //public $cardExpiration;
    public $cardCode;
    public $achAccount;
    public $achRouting;
    public $firstName;
    public $lastName;
    public $company;
    public $street;
    public $street2;
    public $city;
    public $state;
    public $zip;
    public $country;
    public $phone;
    public $email;
    public $currentlyRecurring;
    public $numLeft;
    public $discountPercentage;
    public $tax;
    public $setupFee;

    const recurringDescription = 'AUSA Offers Recurring Subscription';
    const singleDescription = 'AUSA Offers Single Payment';
    const CARD_ACTIVE = "Active";
    const CARD_EXPIRED = "Expired";
    const CARD_EXPIRE_SOON = "Expiring Soon";
    const CARD_NOT_AVAILABLE = "Not Available";

    private $currentCCNumber;
    private $currentCardExp;
    private $currentAchAcc;
    private $currentAchRouting;
    private $currentPaymentType;
    private $currentPaymentMethodID;
    private $transactionCommand;
    private $totalAmount;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            array('paymentMode, maxOffers, recurringSchedule, firstPaymentDate, paymentAmount, maxBranches, maxBanners', 'required'),
            array('maxOffers, maxBranches, maxBanners, numLeft', 'numerical', 'integerOnly' => true),
            array('paymentAmount', 'numerical'),
            array('subscriptionNotes', 'length', 'max' => 1000),
            array('achAccount, achRouting, cardNumber, cardCode, cardExpMonth, cardExpYear, city, company, contractExpiration, country, firstName, lastName, paymentMode, paymentType, phone, recurringSchedule, numLeft, state, street1, street2, subscriptionNotes, zip', 'safe')
                //array('firstPaymentDate, paymentStartDate, contractExpiration', 'date'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'paymentMode' => 'Payment Mode',
            'maxOffers' => 'Max Offer Slots',
            'maxBranches' => 'Total Branches Slots',
            'maxBanners' => 'Max Banners',
            //'firstPaymentAmt' => 'First Payment Amt',
            'firstPaymentDate' => 'Start Payments on',
            'paymentAmount' => 'Payment Amt (USD)',
            'recurringSchedule' => 'Payment Schedule',
            //'nextPaymentDate' => 'Next Recurring Payment On',
            'subscriptionNotes' => 'Subscription Notes',
            'contractExpiration' => 'Contract Expiration',
            'paymentType' => 'Payment Type',
            //cardType' => 'Credit Card Type',
            'cardNumber' => 'Credit Card Number',
            'cardExpMonth' => 'Expiration Month',
            'cardExpYear' => 'Expiration Year',
            //'cardExpiration' => 'Expiration Date',
            'cardCode' => 'CVV2/CID',
            'achAccount' => 'Bank Account Number',
            'achRouting' => 'Bank Routing Number',
            'discountPercentage' => 'Discount Percentage',
            'numLeft' => 'Duration (months)',
            'setupFee' => 'Setup Fee',
        );
    }

    /**
     * Sets values to local form variables from Vendor Model and payment gateway
     */
    public function initSubscription($vendor) {

        if ($vendor->pg_id) { //If payment gateway id is set, then get the customer object from payment gateway and fill the form
            Shared::debug("==========initSubscription - Customer already in PG===========");

            $cacheKey = 'pg-customer-' . $vendor->vendor_id;
            $customer = Yii::app()->cache->get($cacheKey);
            if ($customer == false) {
                // not found in cache
                $customer = $this->getPGCustomer($vendor->pg_id);

                // cache it for one day
                Yii::app()->cache->set($cacheKey, $customer, 86400);
            }

            if ($customer) {
                Shared::debug("customer found");
                $this->paymentAmount = round(($customer->Amount - $customer->Tax), 2);
                $this->tax = $customer->Tax;
                $this->city = $customer->BillingAddress->City;
                $this->company = $customer->BillingAddress->Company;
                $this->country = $customer->BillingAddress->Country;
                $this->email = $customer->BillingAddress->Email;
                $this->firstName = $customer->BillingAddress->FirstName;
                $this->lastName = $customer->BillingAddress->LastName;
                $this->phone = $customer->BillingAddress->Phone;
                $this->state = $customer->BillingAddress->State;
                $this->street = $customer->BillingAddress->Street;
                $this->street2 = $customer->BillingAddress->Street2;
                $this->zip = $customer->BillingAddress->Zip;
                $nextDate = explode('T', $customer->Next);
                $this->firstPaymentDate = $nextDate[0]; //$customer->Next;
                $this->subscriptionNotes = $customer->Notes;
                if ($customer->Enabled) {
                    $this->paymentMode = "recurring";
                } else {
                    $this->paymentMode = "single";
                }

                if ($customer->PaymentMethods) {
                    Shared::debug("found customer payment methods in payment gateway");
                    $this->paymentType = $customer->PaymentMethods[0]->MethodType;
                    $this->currentPaymentMethodID = $customer->PaymentMethods[0]->MethodID;

                    if ($this->paymentType == "cc") {
                        $this->cardNumber = $customer->PaymentMethods[0]->CardNumber;
                        //$tempVar = date($customer->PaymentMethods[0]->CardExpiration);
                        $dateReturn = explode('-', date($customer->PaymentMethods[0]->CardExpiration));
                        $this->cardExpMonth = $dateReturn[1];
                        $this->cardExpYear = substr($dateReturn[0], 2, 4);
                        //$this->cardExpiration = $customer->PaymentMethods[0]->CardExpiration;
                        $this->cardCode = "XXXX";

                        $this->currentPaymentType = "cc";
                        Shared::debug("currentpaymenttype: " . $this->currentPaymentType);
                        $this->currentCCNumber = $this->cardNumber;
                        $this->currentCardExp = $this->cardExpMonth . $this->cardExpYear;
                    } else if ($this->paymentType == "check") {
                        $this->achAccount = $customer->PaymentMethods[0]->Account;
                        $this->achRouting = $customer->PaymentMethods[0]->Routing;

                        $this->currentPaymentType = "check";
                        $this->currentAchAcc = $this->achAccount;
                        $this->currentAchRouting = $this->achRouting;
                    }
                }

                $this->numLeft = $customer->NumLeft;

                if ($customer->NumLeft == 0 || !$customer->Enabled) {
                    $this->currentlyRecurring = false;
                } else {
                    $this->currentlyRecurring = true;
                }
                //Shared::debug($this->currentlyRecurring);

                $this->recurringSchedule = $customer->Schedule;

                $this->maxOffers = $vendor->max_offers;
                $this->maxBanners = $vendor->max_banners;
                $this->maxBranches = $vendor->max_branches;
                Shared::debug($this);
            } else { //if customer not found in gateway, reset pg_id and run Subscription Initiation again.
                Shared::debug("Customer not found in PG");
                $vendor->pg_id = null;
                $vendor->plan_id = null;
                $vendor->updated_on = date("Y-m-d H:i:s");
                $vendor->save(false);
                $this->initSubscription($vendor);
            }
        } else { // If payment gateway id is not in our local database, then fill form values with data from selected plan
            Shared::debug("======initSubscription - Customer NOT in PG===========");
            //Shared::debug("======Todo: Integrate with Plans here===========");
            //if (!$vendor->plan_id) {
            //$vendor->plan_id = 2; //Todo: This is default plan; change it to database later.
            //}

            $selectedPlan = Plan::model()->findByPk($vendor->plan_id);

            if ($selectedPlan) {
                $this->paymentMode = "recurring";
                $this->maxOffers = $selectedPlan->max_offers;
                $this->maxBranches = $selectedPlan->max_branches;
                $this->maxBanners = $selectedPlan->max_banners;
                $this->maxCategories = $selectedPlan->max_categories;
                //$this->firstPaymentAmt = 500; //Yii::app()->numberFormatter->formatCurrency(500, 'USD');
                $this->tax = round(0.0825 * $selectedPlan->monthly_recurring_price, 2);
                $this->setupFee = $selectedPlan->setup_fee;
                if ($selectedPlan->free_duration > 0) {
                    $this->firstPaymentDate = date('M d, Y', strtotime("+" . $selectedPlan->free_duration . " day", strtotime(date("Y-m-d"))));
                    //$this->paymentAmount = '0.00'; //authonly charge
                    //$this->setupFee = '0.00';
                    $this->transactionCommand = 'authonly';
                    $this->totalAmount = '0.00';
                } else {
                    $this->firstPaymentDate = date("Y-m-d");
                    $this->paymentAmount = $selectedPlan->monthly_recurring_price;
                    $this->transactionCommand = 'sale';
                    $this->totalAmount = $this->paymentAmount + $this->setupFee + $this->tax;
                }
                //Shared::debug("============$$$$$$$$$$$$$$$$============" . $this->firstPaymentDate);
                //Yii::app()->numberFormatter->formatCurrency(500, 'USD');
                $this->singlePaymentPrice = $selectedPlan->single_payment_price;
                $this->monthlyRecurringPrice = $selectedPlan->monthly_recurring_price;
                if ((int) $selectedPlan->offer_duration >= (int) $selectedPlan->banner_duration) {
                    $this->numLeft = $selectedPlan->offer_duration;
                } else {
                    $this->numLeft = $selectedPlan->banner_duration;
                }
                $this->recurringSchedule = "monthly"; //Todo: change to monthly
                //$this->nextPaymentDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . " +1 month"));
                if ($this->paymentMode == "recurring") {
                    if ($this->maxBanners > 0) {
                        $this->subscriptionNotes = "Monthly automatic withdrawal of $" . $this->paymentAmount . " + Tax for " . $this->maxBanners . " banner slot(s) and " . $this->maxOffers . " offer slot(s).";
                    } else {
                        $this->subscriptionNotes = "Monthly automatic withdrawal of $" . $this->paymentAmount . " + Tax for " . $this->maxOffers . " offer slot(s).";
                    }
                } elseif ($this->paymentMode == "single") {
                    if ($this->maxBanners > 0) {
                        $this->subscriptionNotes = "Single Payment of $" . $this->paymentAmount . " + Tax for " . $this->maxBanners . " banner slot(s) and " . $this->maxOffers . " offer slot(s).";
                    } else {
                        $this->subscriptionNotes = "Single Payment of $" . $this->paymentAmount . " + Tax for " . $this->maxOffers . " offer slot(s).";
                    }
                }
                //$this->contractExpiration = date("Y-m-d", strtotime(date("Y-m-d", strtotime($this->firstPaymentDate)) . " +" . $this->numLeft . " month"));
                //Shared::debug("firstPaymentDate: " . $this->firstPaymentDate);
                // Shared::debug("numLeft: " . $this->numLeft);
                //Shared::debug("contractExpiration: " . $this->contractExpiration);
                //$date = strtotime(date("Y-m-d", strtotime($date)) . " +1 month");
                //Billing Form
                $this->paymentType = 'cc';
                //$this->cardType = 'Visa';


                $vendorUser = $vendor->getContactUser();
                if ($vendorUser) {
                    $this->firstName = $vendorUser->first_name;
                    $this->lastName = $vendorUser->last_name;
                    $this->phone = $vendorUser->phone_number;
                    $this->email = $vendorUser->email_address;
                }

                $this->company = $vendor->vendor_name;

                Yii::import('application.helpers.AddressHelper');
                $address = AddressHelper::getAddressFields($vendor->address);
                //Shared::debug($address);
                $this->street = $address['street'];
                $this->street2 = $address['street2'];
                $this->city = $address['city'];
                $this->state = $address['state'];
                $this->zip = $address['zip'];
                $this->country = $address['country'];


                //Todo: Delete these - 
                $this->cardCode = '123';
                $this->cardExpMonth = '09';
                $this->cardExpYear = '14';
                //$this->cardExpiration = '0914';
                $this->cardNumber = '4000100011112224';

                $this->achAccount = '5688296876';
                $this->achRouting = '987654322';
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////


    public function getPGCustomer($pg_id) {
        $client = Yii::app()->usaepay->getSoapClient();
        $token = Yii::app()->usaepay->getToken();

        try {
            $result = $client->getCustomer($token, $pg_id);
            //Shared::debug($result);
            return $result;
        } catch (Exception $exc) {
            //Shared::debug($exc);
            //echo $exc->getMessage();
            Yii::log($exc->getMessage(), 'warning');
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public function deletePGCustomer($pg_id) {
        $client = Yii::app()->usaepay->getSoapClient();
        $token = Yii::app()->usaepay->getToken();

        try {
            $result = $client->deleteCustomer($token, $pg_id);
            //Shared::debug($result);
            return $result;
        } catch (Exception $exc) {
            //Shared::debug($exc);
            //echo $exc->getMessage();
            Yii::log($exc->getMessage(), 'error');
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////


    public function getCCStatus() {

        //$this->cardExpMonth = 5;
        //$this->cardExpYear = 13;
        //Shared::debug($this);
        if (isset($this->cardExpYear) && isset($this->cardExpMonth)) {
            $cardExpiration = strtotime($this->cardExpYear . "-" . $this->cardExpMonth . "-01"); //strtotime("yy-mm-dd")
            //Shared::debug($cardExpiration);
            if (($cardExpiration < (strtotime(Shared::timeNow() . ' +60 day'))) && ($cardExpiration > time())) {
                return self::CARD_EXPIRE_SOON;
            } else if ($cardExpiration < time()) {
                return self::CARD_EXPIRED;
            } else {
                return self::CARD_ACTIVE;
            }
        } else {
            return self::CARD_NOT_AVAILABLE;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Use the status field in a table and give it a color
     */
    public function getColoredCCStatus() {
        $status = $this->getCCStatus();
        $color = 'inverse';

        switch ($status) {
            case self::CARD_NOT_AVAILABLE:
                $color = null;
                break;
            case self::CARD_EXPIRE_SOON:
                $color = 'warning';
                break;
            case self::CARD_ACTIVE:
                $color = 'success';
                break;
            case self::CARD_EXPIRED:
                $color = 'important';
            default:
                break;
        }
        return '<span class="label' . (($color ? ' label-' . $color : '')) . '">' . $status . '</span>';
    }

    /////////////////////////////////////////////////////////////////////////////////////////


    public function getPgIdFromVendor($vendor_id) {
        $client = Yii::app()->usaepay->getSoapClient();
        $token = Yii::app()->usaepay->getToken();

        try {
            $result = $client->searchCustomerID($token, $vendor_id);
            //Shared::debug($result);
            return $result;
        } catch (Exception $exc) {
            //Shared::debug($exc);
            //echo $exc->getMessage();
            Yii::log($exc->getMessage(), 'warning');
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public function getRecentTransactions($startDate = null) {
        $client = Yii::app()->usaepay->getSoapClient();
        $token = Yii::app()->usaepay->getToken();

        if ($startDate == null) {
            // get last 14 days of transactions. If server is down more than that,
            // we have to update subscription table manually.
            $startDate = date('Y/m/d', strtotime(Shared::timeNow() . " -14 day"));
        } else {
            // format the start date
            $startDate = date('Y/m/d', Shared::toTimestamp($startDate));
        }
        $endDate = date('Y/m/d');

        try {
            return base64_decode($client->getTransactionReport($token, $startDate, $endDate, 'custom:recenttransactions', 'csv'));
        } catch (Exception $exc) {
            echo $exc->getMessage();
            Yii::log($exc->getMessage(), 'warning');
        }
    }

    public function getBillingHistory($pgId) {
        $client = Yii::app()->usaepay->getSoapClient();
        $token = Yii::app()->usaepay->getToken();

        try {
            return $client->getCustomerHistory($token, $pgId);
        } catch (Exception $exc) {
            //echo $exc->getMessage();
            Yii::log($exc->getMessage(), 'warning');
        }
    }

    public function getTransactions($pgId) {
        $history = $this->getBillingHistory($pgId);
        $payCount = 0;
        $payments = array();
        if (is_object($history) && $history->Transactions != null) {
            foreach ($history->Transactions as $transaction) {

                $payment = new Payment();
                $payment->transaction_id = $transaction->Details->Invoice;
                $payment->created = $transaction->AccountHolder;
                $payment->ps_charge_id = $transaction->Details->Description;
                $payment->start_date = date("F j, Y", strtotime($transaction->DateTime));
                if ($transaction->CheckData->Account) {
                    $payment->description = 'Payment Type: E-Check / ACH <br />Account: ' . $transaction->CheckData->Account . '<br />';
                } else if ($transaction->CreditCardData->CardNumber) {
                    $payment->description = 'Payment Type: Credit Card <br />CC Number: ' . $transaction->CreditCardData->CardNumber . '<br />';
                }
                $payments[] = $payment;
            }
        }
        return $payments;
    }

    /**
     * Unsubscribe vendor from recurring payments
     * @param type $vendor Vendor object
     */
    public function cancelSubscription($vendor) {
        if ($vendor->pg_id) {
            $client = Yii::app()->usaepay->getSoapClient();
            $token = Yii::app()->usaepay->getToken();
            try {
                $client->disableCustomer($token, $vendor->pg_id);

                $cacheKey = 'pg-customer-' . $vendor->vendor_id;
                Yii::app()->cache->delete($cacheKey);
                return true;
            } catch (Exception $exc) {
                Yii::log($exc->getMessage(), 'error');
            }
        }
        return false;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public function save($vendor) {
        //if new vendor, create a customer in payment gateway and set payment structure
        if ($vendor->pg_id) {
            //Shared::debug("vendor is already in payment gateway");
            //$vendor->updated_on = date("Y-m-d H:i:s");
            // $vendor->save(false);
            $updatedCustomerInPG = $this->updateCustomer($vendor);

            if ($updatedCustomerInPG) {
                $this->quickUpdatePaymentMethod($vendor);
            }
        } else {
            //Shared::debug("Vendor is NOT in payment gateway");

            $this->addCustomer($vendor);
        }
        $cacheKey = 'pg-customer-' . $vendor->vendor_id;
        Yii::app()->cache->delete($cacheKey);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////

    public function addCustomer($vendor) {
        //Shared::debug("Method - addCustomer");

        $CustomerObject = $this->buildCustomerObject($vendor);

        $client = Yii::app()->usaepay->getSoapClient();
        $token = Yii::app()->usaepay->getToken();
        try {
            $transactionResult = $client->runTransaction($token, $CustomerObject); //Check Result and if appvoed, save pg_id to vendor;
            //Shared::debug($pg_id);
            $vendor->pg_id = $pg_id;
            //$vendor->published = 1;
            $vendor->updated_on = date("Y-m-d H:i:s");
            $vendor->save(false);
            return $pg_id;
        } catch (Exception $exc) {
            //Shared::debug($exc->getMessage());
            //Shared::debug($pg_id);
            //echo $exc->getMessage();
            Yii::log($exc->getMessage(), 'error');
            if (!$vendor->pg_id) { //Search and get the inserted customer from PG and associate it with local vendor
                $pg_id = $this->getPgIdFromVendor($vendor->vendor_id);
                if ($pg_id) {
                    $vendor->pg_id = null;
                    $vendor->updated_on = date("Y-m-d H:i:s");
                    $vendor->save(false);
                    //return $pg_id;
                    $this->deletePGCustomer($pg_id);
                }
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////


    public function updateCustomer($vendor) {
        //Shared::debug("Method - updateCustomer");

        $updateCustomerObject = $this->buildQuickCustomerObject($vendor);
        $client = Yii::app()->usaepay->getSoapClient();
        $token = Yii::app()->usaepay->getToken();
        try {
            $result = $client->quickUpdateCustomer($token, $vendor->pg_id, $updateCustomerObject);
            //Shared::debug("Quick Updated Customer?: " . $result);

            return $result;
        } catch (Exception $exc) {

            //echo $exc->getMessage();
            Yii::log($exc->getMessage(), 'error');
            //Todo: how to handle update failure??
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////


    public function quickUpdatePaymentMethod($vendor) {
        Shared::debug("Method - quickUpdatePaymentMethod");
        //Shared::debug($this);
        Shared::debug($this->paymentType);
        Shared::debug($this->currentPaymentType);
        Shared::debug(($this->cardExpMonth . $this->cardExpYear));
        Shared::debug($this->currentCardExp);
        Shared::debug($this->cardNumber);
        Shared::debug($this->currentCCNumber);
        Shared::debug($this->achAccount);
        Shared::debug($this->currentAchAcc);
        Shared::debug($this->achRouting);
        Shared::debug($this->currentAchRouting);

        if (($this->paymentType != $this->currentPaymentType) || (($this->cardExpMonth . $this->cardExpYear) != $this->currentCardExp) || ($this->cardNumber != $this->currentCCNumber) || ($this->achAccount != $this->currentAchAcc) || ($this->achRouting != $this->currentAchRouting)) {
            Shared::debug("The payment details changed.. ");
            $this->updatePaymentMethodNow();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public function updatePaymentMethodNow() {
        if ($this->paymentType == 'cc') {

            $paymentMethod = array(
                'MethodID' => $this->currentPaymentMethodID,
                'MethodType' => 'cc',
                'MethodName' => $this->firstName . ' - ' . $this->paymentType,
                'CardCode' => $this->cardCode, //'999', //123
                'CardNumber' => $this->cardNumber, //'4000100011112224',
                'CardExpiration' => $this->cardExpMonth . $this->cardExpYear,
                //'CardExpiration' => $this->cardExpiration, //'0914',
                'CardType' => '',
                'AvsStreet' => $this->street,
                'AvsZip' => $this->zip,
                'SecondarySort' => '',
            );
        } elseif ($this->paymentType == 'check') {

            $paymentMethod = array(
                'MethodID' => $this->currentPaymentMethodID,
                'MethodType' => 'check', //$this->paymentType,
                'MethodName' => $this->firstName . ' - ' . $this->paymentType,
                'Routing' => $this->achRouting,
                'Account' => $this->achAccount,
                'SecondarySort' => '',
            );
        }
        Shared::debug($paymentMethod);

        $client = Yii::app()->usaepay->getSoapClient();
        $token = Yii::app()->usaepay->getToken();
        try {
            if ($this->paymentType == "cc") {
                $result = $client->updateCustomerPaymentMethod($token, $paymentMethod, true);
            } else {
                $result = $client->updateCustomerPaymentMethod($token, $paymentMethod);
            }
            //Shared::debug("Quick Updated Payment Method?: " . $result);

            return $result;
        } catch (Exception $exc) {

            //echo $exc->getMessage();
            Yii::log($exc->getMessage(), 'error');
            //Todo: how to handle update failure??
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    public function buildQuickCustomerObject($vendor) {
        $updateCustomerObject = array(
            array('Field' => 'FirstName', 'Value' => $this->firstName),
            array('Field' => 'LastName', 'Value' => $this->lastName),
            array('Field' => 'Company', 'Value' => $this->company),
            array('Field' => 'Address', 'Value' => $this->street),
            array('Field' => 'Address2', 'Value' => $this->street2),
            array('Field' => 'City', 'Value' => $this->city),
            array('Field' => 'State', 'Value' => $this->state),
            array('Field' => 'Zip', 'Value' => $this->zip),
            array('Field' => 'Country', 'Value' => $this->country),
            array('Field' => 'Phone', 'Value' => $this->phone),
            array('Field' => 'Email', 'Value' => $this->email),
            array('Field' => 'URL', 'Value' => $vendor->website),
            array('Field' => 'Notes', 'Value' => $this->subscriptionNotes),
            array('Field' => 'NumLeft', 'Value' => $this->numLeft),
            array('Field' => 'Amount', 'Value' => $this->paymentAmount),
        );
        //Shared::debug($updateCustomerObject);
        return $updateCustomerObject;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    public function buildCustomerObject($vendor) {

        $salesmen = array();
        foreach ($vendor->salesmen as $man) {
            $salesmen[] = $man->user_id;
        }
        
        $chapters = array();
        foreach ($vendor->vendorMembership as $chapter) {
            $chapters[] = $chapter->chapter_id;
        }
        //Build Transaction Object.. 
        $transactionObject = array(
            'Command' => $this->transactionCommand,
            'AccountHolder' => $vendor->vendor_name,
            'Details' => array(
                'Clerk' => implode(',', $salesmen),
                'Terminal' => is_object($vendor->stVendor) ? $vendor->stVendor->st->st_name : '',
                'Table' => implode(',', $chapters),
                'Description' => 'AUSA Offers Subscription',
                'Amount' => floatval($this->totalAmount),
                'Tax' => $this->tax,
                'Shipping' => $this->setupFee,
                'Subtotal' => $this->paymentAmount,
            ),           
            'ClientIP' => $_SERVER['REMOTE_ADDR'],
            'CustomerID' => $vendor->vendor_id,
            'BillingAddress' => array(
                'FirstName' => $this->firstName,
                'LastName' => $this->lastName,
                'Company' => $vendor->vendor_name,
                'Street' => $this->street,
                'Street2' => $this->street2,
                'City' => $this->city,
                'State' => $this->state,
                'Zip' => $this->zip,
                'Country' => $this->country,
                'Email' => $this->email,
                'Phone' => $this->phone,
            ),
            'CustReceipt' => true,
            'Software' => 'AO-Web',
            'CustReceiptName' => 'Ausa Offers - Payment Receipt',
            'RecurringBilling' => array(
                'Schedule' => 'monthly',
                'Next' => $this->firstPaymentDate,
                'NumLeft' => '*',
                'Amount' => floatval($this->paymentAmount) + floatval($this->tax),
                'Enabled' => true,
            ),
        );

        //Billing Data        
        /* if ($this->paymentMode == 'recurring') { //Recurring Payments
          $CustomerObject['Description'] = self::recurringDescription;
          $CustomerObject['Enabled'] = true;
          $CustomerObject['Amount'] = $this->paymentAmount + 0.0825 * (float) $this->paymentAmount;
          $CustomerObject['Tax'] = 0.0825 * (float) $this->paymentAmount;
          //Shared::debug($vendor->plan);
          //if ($vendor->plan->monthly_recurring_price > ($this->paymentAmount - $CustomerObject['Tax'])) {
          //$CustomerObject['Discount'] = $vendor->plan->monthly_recurring_price - $this->paymentAmount - $CustomerObject['Tax'];
          // }
          $CustomerObject['Discount'] = 0; //Todo: Check if this value is being saved in the PG. If not, delete it.
          $CustomerObject['Next'] = $this->firstPaymentDate;
          //$CustomerObject['Next'] = $this->nextPaymentDate;
          $CustomerObject['Schedule'] = 'daily'; //todo: change to monthly
          $CustomerObject['NumLeft'] = $this->numLeft;
          } elseif (($this->paymentMode == 'single') && (!$this->currentlyRecurring)) { //cannot handle single payments in add or update customer if there is currently recurring payments going on.
          $CustomerObject['Description'] = self::singleDescription;
          $CustomerObject['Enabled'] = false;
          $CustomerObject['Amount'] = $this->paymentAmount + 0.0825 * (float) $this->paymentAmount;
          $CustomerObject['Tax'] = 0.0825 * (float) $this->paymentAmount;

          //$CustomerObject['Next'] = $this->firstPaymentDate;
          $CustomerObject['Next'] = date("Y-m-d");
          $CustomerObject['Schedule'] = 'daily'; //Todo: dropdown capture value

          $CustomerObject['NumLeft'] = '0'; //unlimited. customer has to call to cancel subscription
          } */

        if ($this->paymentType == 'cc') {

            $transactionObject['CreditCardData'] = array(
                'CardCode' => $this->cardCode,
                'CardNumber' => $this->cardNumber,
                'CardExpiration' => $this->cardExpMonth . $this->cardExpYear,
                'CardType' => '',
                'AvsStreet' => $this->street,
                'AvsZip' => $this->zip,
                'CardPresent' => false,
            );
        } elseif ($this->paymentType == 'check') {

            $transactionObject['CheckData'] = array(
                array(
                    'Routing' => $this->achRouting,
                    'Account' => $this->achAccount,
                ),
            );
        }

        Shared::debug($transactionObject);
        return $transactionObject;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
}
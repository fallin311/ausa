<?php

/**
 * System log loads log files stored in protected/runtime directory
 * and returns specified number of lines from the log.
 *
 * @author One
 */
class SystemLog extends CFormModel {

    public $logname;
    private $filename;
    public $filesize;
    public $lastModified;
    public $lineCount = 50;

    public function __construct($logName) {
        $this->logname = $logName;
        $this->filename = app()->getBasePath() . '/runtime/' . $logName;
        $this->getFileInfo();
    }

    /**
     * How many lines are we going to read from this file
     * @param type $count
     */
    public function setLineCount($count) {
        if (!$count || $count <= 0){
            $count = 50;
        }
        $this->lineCount = $count;
    }

    public function getLineCount($count) {
        return $this->lineCount;
    }

    /**
     * Reads last x lines from the system log
     * @param type $lines
     */
    public function read() {
        $lineCount = $this->lineCount;
        $lines = array();
        $blockSize = 4096;
        $filesize = $this->filesize;
        if ($filesize > 0) {
            $startOffset = $filesize;
            $fp = fopen($this->filename, "r");
            if ($fp) {
                while (count($lines) < $lineCount && $startOffset > 0) {
                    $startOffset -= $blockSize;
                    if ($startOffset < 0) {
                        $startOffset = 0;
                    }
                    fseek($fp, $startOffset);
                    $block = fread($fp, $blockSize);
                    $newBlock = explode("\n", $block);
                    if (count($newBlock) > $lineCount - count($lines)) {
                        $offset = $lineCount > count($lines) ? $lineCount - count($lines) : $lineCount;
                        $offset = ($offset > count($newBlock) ? $offset - count($newBlock) : $offset);
                        // not sure why it has to be negative number
                        array_splice($newBlock, 0, $offset * -1);
                    }
                    $lines = array_merge($newBlock, $lines);
                }
            }
        }
        return $lines;
    }

    /**
     * Return information about file name, size and last modification
     * @return null
     */
    public function getFileInfo() {
        if (is_file($this->filename)) {
            $this->filesize = filesize($this->filename);
            $this->lastModified = Shared::toDatabase(filemtime($this->filename));
            return array(
                'name' => $this->filename,
                'size' => $this->filesize,
                'modified' => $this->lastModified
            );
        }
        return null;
    }

}

?>

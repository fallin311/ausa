<?php

/**
 * This is the model class for table "image".
 *
 * The followings are the available columns in table 'image':
 * @property string $image_id
 * @property string $long_id
 * @property string $vendor_id
 * @property string $resolution
 * @property string $created_on
 * @property string $updated_on
 * @property string $file_type
 * @property integer $file_size
 * @property string $file_name
 * @property string $file_content
 * @property integer $cropped
 * @property string $category
 *
 * The followings are the available model relations:
 * @property Vendor $vendor
 */
class Image extends AOActiveRecord {
    // image sizes for logos and offers

    const SIZE_ICON_W = 25;
    const SIZE_ICON_H = 20;
    const SIZE_PHONE_W = 80;
    const SIZE_PHONE_H = 64;
    const SIZE_PREVIEW_W = 125;
    const SIZE_PREVIEW_H = 100;
    const SIZE_FULL_W = 180;
    const SIZE_FULL_H = 144;

    public static $categories = array(
        'logo',
        'logoTemplate',
        'offer',
        'offerTemplate',
        'category',
        'banner'
    );

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'image';
    }

    /**
     * Returns the static model of the specified AR class.
     * @return Image the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),
        );
    }

    public static function getNoImageUrl() {
        //$path = dirname(Yii::app()->request->scriptFile) . '/images/no-image.png';
        return url('/images/no_photo.jpg');
    }

    /**
     * Load everything but file content
     */
    public static function findByPkLight($pk) {
        return self::model()->findByPk($pk, '', array('file_id', 'file_name', 'file_size', 'file_type', 'long_id', 'vendor_id', 'updated_on', 'category'));
    }

    /**
     * This should move somewhere else, so we don't need to call it every time 
     */
    public function afterFind() {
        parent::afterFind();
        $this->file_content = stripslashes($this->file_content);
    }

    public function afterValidate() {
        parent::afterValidate();
        $this->file_content = addslashes($this->file_content);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('file_size, cropped', 'numerical', 'integerOnly' => true),
            array('long_id', 'length', 'max' => 31),
            array('vendor_id', 'length', 'max' => 10),
            array('resolution, category', 'length', 'max' => 16),
            array('file_type', 'length', 'max' => 63),
            array('file_name', 'length', 'max' => 255),
            array('vendor_id', 'default', 'setOnEmpty' => true), // make empty values stored as NULL
            array('created_on, updated_on, file_content', 'safe'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                //array('image_id, long_id, vendor_id, resolution, created_on, updated_on, file_type, file_size, file_name, file_content, cropped, category', 'safe', 'on'=>'search'),
        );
    }

    /**
     * This is ImageFactory, but we just saved one file
     * @return Image 
     */
    public static function create() {
        $model = new Image;
        $model->created_on = Shared::toDatabase(time());
        $model->long_id = Shared::generateRandomPassword(12);
        $model->cropped = 1;
        return $model;
    }

    /**
     * Load the data from this object, resize to given size and return image resource.
     * Always keep the aspect ratio
     */
    public function resize($width, $height, $originalImage = null) {
        if ($originalImage == null) {
            $originalImage = imagecreatefromstring($this->file_content);
        }
        $oldWidth = imagesx($originalImage);
        $oldHeight = imagesy($originalImage);
        $dontResize = false;

        // take the smaller one
        if ($width && $height) {
            $factorW = (float) $width / (float) $oldWidth;
            $factorH = (float) $height / (float) $oldHeight;
            if ($factorH > $factorW) {
                $height = null;
            } else {
                $width = null;
            }
        }

        // only width or height is set
        if ($width > 0) {
            if ($oldWidth < $width)
                $dontResize = true;
            $factor = (float) $width / (float) $oldWidth;
            $height = (int) ($factor * $oldHeight);
        }
        else if ($height > 0) {
            if ($oldHeight < $height)
                $dontResize = true;
            $factor = (float) $height / (float) $oldHeight;
            $width = (int) ($factor * $oldWidth);
        }else {
            // no change
            $dontResize = true;
        }

        // we need to create a thumb
        if ($dontResize) {
            Shared::debug("just returning the original image, no resize applied");
            $resampledImage = $originalImage;
        } else {
            //Shared::debug("resizing the image to $width and $height");
            // setup the transparency
            $resampledImage = imagecreatetruecolor($width, $height);
            imagealphablending($resampledImage, false);
            imagesavealpha($resampledImage, true);
            $transparent = imagecolorallocatealpha($resampledImage, 255, 255, 255, 127);
            imagefilledrectangle($resampledImage, 0, 0, $width, $height, $transparent);
            imagecopyresampled($resampledImage, $originalImage, 0, 0, 0, 0, $width, $height, $oldWidth, $oldHeight); //($image_c, $new_image, 0, 0, 0, 0, $this->new_width, $this->new_height, $width, $height);
        }
        return $resampledImage;
    }

    /**
     * Similar to resize, but does not check aspect ratio.
     * @param type $x
     * @param type $y
     * @param type $width
     * @param type $height 
     */
    public function resizeCrop($x, $y, $width, $height, $dstWidth, $dstHeight, $canvas = null) {
        // make sure there is a change
        if ($canvas == null) {
            $canvas = imagecreatefromstring($this->file_content);
        }
        Shared::debug("resize and crop");
        // apply transparency

        // we might change only selected area of the image
        $resampled = imagecreatetruecolor($dstWidth, $dstHeight);
        imagecopyresampled($resampled, $canvas, 0, 0, $x, $y, $dstWidth, $dstHeight, $width, $height);
        $canvas = $resampled;

        return $canvas;
    }

    /**
     * Save the canvas to temp file, read its content and save content and size to the database
     * @param type $canvas
     * @return boolean
     */
    public function saveCanvasToString($canvas) {
        $tempFile = 'protected/runtime/temp/' . $this->file_name;

        if (strstr($this->file_type, 'png')) {
            imagepng($canvas, $tempFile);
        } else if (strstr($this->file_type, 'gif')) {
            imagegif($canvas, $tempFile);
        } else {
            imagejpeg($canvas, $tempFile);
        }

        // then read it again
        /* $fp = fopen($tempFile, 'r');
          $this->file_content = fread($fp, $this->file_size);
          fclose($fp); */
        if (is_file($tempFile)){
            $this->file_size = filesize($tempFile);
            $this->file_content = file_get_contents($tempFile);
            unlink($tempFile);
            return true;
        }else{
            return false;
        }
    }

    /**
     * Render the data to the browsers. You can use GET parameter width and height to fix the size.
     * The image is cached on filesystem for better better performance.
     * @param integer $width - maximal width
     * @param integer $height - maximal height
     * @param boolean $display - display to page or just resize internally. Returns true if image was resized.
     */
    public function display($width = null, $height = null, $display = true) {

        if (isset($_GET['w']))
            $width = (int) $_GET['w'];
        if (isset($_GET['h']))
            $height = (int) $_GET['h'];

        // display from cache. Yii cache stores only path to the file on filesystem and
        // is using sql dependency checking file modification time.
        if ($width == null) {
            $cacheId = 'image-preview-' . $this->image_id;
        } else {
            $cacheId = 'image-preview-' . $this->image_id . '-' . $width;
        }
        $path = Yii::app()->cache->get($cacheId);
        if ($path !== false) {
            if (file_exists($path)) {
                //Shared::debug("found cached image at $path - {$this->file_type}, {$this->file_name}");
                header("Content-type: $this->file_type");
                header("Content-Disposition: inline; filename=" . $this->file_name);
                header("Pragma: public");
                header("Expires: 1200");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                readfile($path);
                return true;
            }
        }

        // continue loading from database
        // detect the type of image, so we can use the right function for the problem
        if (strstr($this->file_type, 'png')) {
            $type = 'png';
        } else if (strstr($this->file_type, 'gif')) {
            $type = 'gif';
        } else {
            $type = 'jpeg';
        }

        if (strlen($this->file_content)) {

            $resampledImage = $this->resize($width, $height);

            //Shared::debug("displaying image from database");
            if ($display) {
                // save it to the cache
                $path = YiiBase::getPathOfAlias('application.runtime.cache') . '/' . $cacheId . '.' . $type;
                call_user_func('image' . $type, $resampledImage, $path);

                // load the right file size
                /* Shared::debug("old file size is {$this->file_size}");
                  $this->file_size = filesize($path);
                  Shared::debug("new file size is {$this->file_size}"); */

                // output the image to screen
                header("Pragma: public");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Content-type: $this->file_type");
                header("Content-Disposition: inline; filename=" . $this->file_name);

                // store to cache
                $dependency = new CDbCacheDependency('SELECT updated_on FROM image WHERE image_id=' . $this->image_id);
                Yii::app()->cache->set($cacheId, $path, 0, $dependency);

                // and display
                //call_user_func('image' . $type, $resampledImage);
                readfile($path);
            } else {
                // just store it back resized to the database
                return $this->saveCanvasToString($resampledImage);
            }
        }

        return false;
    }

    /**
     * Returns an url where you can view the file (picture)
     * @return <type> 
     */
    public function getUrl($width = null, $height = null, $unique = false) {
        $conf = array(
            'img' => $this->long_id
        );
        if ($unique)
            $conf['v'] = time();
        if ($width > 0)
            $conf['w'] = $width;
        if ($height > 0)
            $conf['h'] = $height;

        $path = Yii::app()->createAbsoluteUrl(
                'image/display/', $conf);
        return $path;
    }

    public function getWidth() {
        $arr = explode('x', $this->resolution);
        if (count($arr) == 2) {
            return $arr[0];
        }
        return false;
    }

    public function getHeight() {
        $arr = explode('x', $this->resolution);
        if (count($arr) == 2) {
            return $arr[1];
        }
        return false;
    }

    /**
     * Get image url without loading the object from the database. Image long id
     * is required.
     * @param type $longId
     * @param type $width
     * @param type $height
     * @param type $unique
     * @return type
     */
    public static function getImageUrl($longId, $width = null, $height = null, $unique = false) {
        $conf = array(
            'img' => $longId
        );
        if ($unique)
            $conf['v'] = time();
        if ($width > 0)
            $conf['w'] = $width;
        if ($height > 0)
            $conf['h'] = $height;
        $path = Yii::app()->createAbsoluteUrl(
                'image/display/', $conf);
        return $path;
    }

    /**
     * Returns the image directly in base64 format, which can be displayed by client
     * There is no resize feature
     * @return type 
     */
    public function getBase64Src($width = null, $height = null) {
        if ($width != null || $height != null) {
            // resize first
            $canvas = $this->resize($width, $height);
            
            // temp
            //$tempFile = 'protected/runtime/temp/' . $this->file_name;
            //imagepng($canvas, $tempFile);
            
            $this->saveCanvasToString($canvas);
        }
        // this should be loaded from cache
        return "data:{$this->file_type};base64," . base64_encode($this->file_content);
    }

    public static function getCachedBase64Src($longId, $width = 80) {
        // TODO: cache it here
        $img = app()->cache->get('base64-' . $longId . '-' . $width);
        if ($img === false) {
            $image = Image::model()->findByAttributes(array('long_id' =>$longId));
            if ($image == null) {
                return self::getDefaultBase64Icon();
            } else {
                $img = $image->getBase64Src($width);
                app()->cache->set('base64-' . $longId . '-' . $width, $img, 300);
            }
        }
        return $img;
    }

    /**
     * Based on category
     * @return boolean
     */
    public function isTemplate() {
        if (strstr(strtolower($this->category), 'template'))
            return true;
        return false;
    }

    public static function getDefaultBase64Icon() {
        return "data:image/png;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAyAD8DASIAAhEBAxEB/8QAGgABAAMBAQEAAAAAAAAAAAAAAAQFBgcDAv/EADAQAAEDAwMCBAQGAwAAAAAAAAECAwQABREGEiEHQRMxUaEiYXGBFhcyQpHRYoOy/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AO+UqpvuoLfYzHFxdWgyCQ2EoKs4xny+oq2oFKUoFKUoFKUoOb9WFb7nYmFIQpK1K5OcjKkDsadSLndo2o7bCtMx1gyG0gIQcArKyBn2rz6qnOoNPgZzuP8A2mvLqKofj+wgHkBk4/2n+qDKo1TqIRUvC6P+G24EjJBJJBPPHI471pNe368tz4iWJL0GKqKl3ejKUrWUlRGR9hjNYkKxptafhwqWk+fPCD7c1f8AUlxarpbUOqV4SYDZQO3IPP8AI9qDU2jU1x/LSdcnXfEmx3PBQ6oZPJQAT6kbvas5ZdTahN6srMue4piU80dpCfiQXNpyQM9jXpZznpHfEjzEtJ92v6qnsz7UnUWmEoUQWVstryP3B4nHuKDR9RNRXNvUzsCFOXEZjNg/AvZuVs38nuTwAK2XTu7yLzpxD01wOSG3FNqVwCoDyJ/n2rm3UZKEa6nqkIOxSEKT/kfCAH23Cuh9L8HSTKkspZCnVkBJODzjPJJ7UGgn2qDPkR35kZDrsc7mlK80nIPH3ApKtUCXMalSojDsloAIcWgEpAORj71NpQUp0tYy0Wza42wq3YCMc1LmWe3TfBEyDHf8EYb8RsK2j0+lT6UFHd7Ay9pybbLSlqAZAyC0nYN2R547EDB+VYbTGgrrEv0KVOMZtiKsLyhWSrByB5evc9q6rSghXC02+4qQqfDjyFI/SXEAkfLPp8qlMtNsNJbZbQ22kYShAwAPQAV90oFKUoFKUoFKUoFKUoP/2Q==";
    }

    public static function getDefaultBase64IconLarge() {
        return "data:image/png;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAyAD8DASIAAhEBAxEB/8QAGgABAAMBAQEAAAAAAAAAAAAAAAQFBgcDAv/EADAQAAEDAwMCBAQGAwAAAAAAAAECAwQABREGEiEHQRMxUaEiYXGBFhcyQpHRYoOy/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AO+UqpvuoLfYzHFxdWgyCQ2EoKs4xny+oq2oFKUoFKUoFKUoOb9WFb7nYmFIQpK1K5OcjKkDsadSLndo2o7bCtMx1gyG0gIQcArKyBn2rz6qnOoNPgZzuP8A2mvLqKofj+wgHkBk4/2n+qDKo1TqIRUvC6P+G24EjJBJJBPPHI471pNe368tz4iWJL0GKqKl3ejKUrWUlRGR9hjNYkKxptafhwqWk+fPCD7c1f8AUlxarpbUOqV4SYDZQO3IPP8AI9qDU2jU1x/LSdcnXfEmx3PBQ6oZPJQAT6kbvas5ZdTahN6srMue4piU80dpCfiQXNpyQM9jXpZznpHfEjzEtJ92v6qnsz7UnUWmEoUQWVstryP3B4nHuKDR9RNRXNvUzsCFOXEZjNg/AvZuVs38nuTwAK2XTu7yLzpxD01wOSG3FNqVwCoDyJ/n2rm3UZKEa6nqkIOxSEKT/kfCAH23Cuh9L8HSTKkspZCnVkBJODzjPJJ7UGgn2qDPkR35kZDrsc7mlK80nIPH3ApKtUCXMalSojDsloAIcWgEpAORj71NpQUp0tYy0Wza42wq3YCMc1LmWe3TfBEyDHf8EYb8RsK2j0+lT6UFHd7Ay9pybbLSlqAZAyC0nYN2R547EDB+VYbTGgrrEv0KVOMZtiKsLyhWSrByB5evc9q6rSghXC02+4qQqfDjyFI/SXEAkfLPp8qlMtNsNJbZbQ22kYShAwAPQAV90oFKUoFKUoFKUoFKUoP/2Q==";
    }

}
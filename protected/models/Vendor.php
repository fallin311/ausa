<?php

/**
 * This is the model class for table "vendor".
 *
 * The followings are the available columns in table 'vendor':
 * @property string $vendor_id
 * @property string $pg_id
 * @property string $vendor_name
 * @property string $vendor_description
 * @property string $internal_notes
 * @property string $logo
 * @property string $vendor_banner_image
 * @property string $address
 * @property string $website
 * @property string $industry
 * @property integer $disabled
 * @property string $created_on
 * @property string $updated_on
 * @property string $expiration
 * @property integer $max_offers
 * @property integer $max_banners
 * @property string $banner_expiration
 * @property integer $max_branches
 * @property integer $max_categories
 * @property string $plan_id
 * @property integer $published This controls if the vendor shows up in the mobile app or not. alongside with expiration dates

 *
 * The followings are the available model relations:
 * @property Banner[] $banners
 * @property Branch[] $branches
 * @property Member[] $members
 * @property Image[] $images
 * @property MemberOffer[] $memberOffers
 * @property Payment[] $payments
 * @property SalesTeam[] $salesTeams
 * @property User[] $users
 * @property Plan $plan
 * @property Chapter[] $chapters
 * @property VendorSalesman[] $vendorSalesmen
 */
class Vendor extends AOActiveRecord {

    /**
     * Virtual attribute used to display list of salesman in update vendor form
     * @var type 
     */
    public $salesmenArr;
    public $salesTeamId;
    public $chapterId;

    const SUB_ACTIVE = "Active";
    const SUB_EXPIRED = "Expired";
    const SUB_EXPIRE_SOON = "Expiring Soon";
    const SUB_NOT_ACTIVE = "Not Active Yet";

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Vendor the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'vendor';
    }

    public function behaviors() {
        return array(
            'companyBehavior' => array(
                'class' => 'application.models.behaviors.CompanyBehavior'
            )
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            app()->user->isAdmin()?array('vendor_name, address, salesTeamId', 'required'):array('vendor_name, address', 'required'),
            array('disabled, max_offers, activated, max_banners, max_branches, max_categories, published', 'numerical', 'integerOnly' => true),
            array('vendor_name, address', 'length', 'max' => 255),
            array('vendor_banner_image, industry, plan_id', 'length', 'max' => 10),
            array('website', 'length', 'max' => 127),
            array('vendor_description', 'application.components.validators.AOPhoneTextValidator'),
            //array('payment_method', 'length', 'max' => 5),
            //array('billing_name, billing_state, billing_country', 'length', 'max' => 63),
            //array('billing_zipcode', 'length', 'max' => 12),
            //array('billing_phone', 'length', 'max' => 16),
            array('logo', 'length', 'max' => 31),
            array('internal_notes, created_on, updated_on, expiration, banner_expiration', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('vendor_id, vendor_name, vendor_description, internal_notes, logo, vendor_banner_image, address, website, industry, disabled, created_on, updated_on, expiration, max_offers, max_banners, banner_expiration, max_branches, plan_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'offers' => array(self::HAS_MANY, 'Offer', 'vendor_id'),
            'banners' => array(self::HAS_MANY, 'Banner', 'vendor_id'),
            'branches' => array(self::HAS_MANY, 'Branch', 'vendor_id'),
            'activeBranches' => array(self::HAS_MANY, 'Branch', 'vendor_id', 'condition' => 'activeBranches.active=1'),
            'members' => array(self::MANY_MANY, 'Member', 'favorite_store(vendor_id, member_id)'),
            'images' => array(self::HAS_MANY, 'Image', 'vendor_id'),
            'memberOffers' => array(self::HAS_MANY, 'MemberOffer', 'vendor_id'),
            'payments' => array(self::HAS_MANY, 'Payment', 'vendor_id'),
            'salesTeams' => array(self::MANY_MANY, 'SalesTeam', 'st_vendor(vendor_id, sales_team_id)'),
            'users' => array(self::MANY_MANY, 'User', 'user_vendor(vendor_id, user_id)'),
            'plan' => array(self::BELONGS_TO, 'Plan', 'plan_id'),
            'chapters' => array(self::MANY_MANY, 'Chapter', 'vendor_membership(vendor_id, chapter_id)'),
            'salesmen' => array(self::HAS_MANY, 'VendorSalesman', 'vendor_id'),
            'stVendors' => array(self::HAS_MANY, 'StVendor', 'vendor_id'),
            'stVendor' => array(self::HAS_ONE, 'StVendor', 'vendor_id'),
            'vendorMembership' => array(self::HAS_MANY, 'VendorMembership', 'vendor_id'),
            'activeOffers' => array(self::HAS_MANY, 'Offer', 'vendor_id', 'condition' => 'activeOffers.published=1 AND activeOffers.start_date<=NOW() AND activeOffers.end_date>=NOW()'),
            'almostActiveOffers' => array(self::HAS_MANY, 'Offer', 'vendor_id', 'condition' => 'almostActiveOffers.end_date>=NOW()'),
            'activeBanners' => array(self::HAS_MANY, 'Banner', 'vendor_id', 'condition' => 'activeBanners.published=1 AND activeBanners.start_date<=NOW() AND activeBanners.end_date>=NOW()'),
            'category' => array(self::BELONGS_TO, 'Category', 'industry'),
            //'chapterEntity' => array(self::BELONGS_TO, 'Chapter', 'vendor_id', 'condition' => 'almostActiveOffers.end_date>=NOW()'),
        );
    }

    public function scopes() {
        /* return array(
          // only those, which should be sent to cellphones
          'publishedOffersPlus' => array(
          'condition' => 'published=1 AND vendor.disabled=0 AND vendor.expiration > NOW()',
          'with' => 'vendor',
          )
          ); */
        return array(
            'active' => array(
                'condition' => 'active=1',
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'vendor_id' => 'Vendor',
            'pg_id' => 'Payment Gateway ID',
            'vendor_name' => 'Vendor Name',
            'vendor_description' => 'Vendor Description',
            'internal_notes' => 'Internal Notes',
            'logo' => 'Logo',
            'vendor_banner_image' => 'Vendor Banner Image',
            'address' => 'Address',
            'website' => 'Website',
            'industry' => 'Industry',
            'disabled' => 'Disabled',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'expiration' => 'Offer Expiration',
            'max_offers' => 'Total Offer Slots',
            'max_banners' => 'Total Banner Slots',
            'max_categories' => 'No. of Categories',
            'banner_expiration' => 'Banner Expiration',
            'max_branches' => 'Maximum no. of Branches',
            'plan_id' => 'Plan',
            /* 'customer_number' => 'Customer Number',
              'payment_method' => 'Payment Method',
              'billing_name' => 'Billing Name',
              'billing_address' => 'Billing Address',
              'billing_state' => 'Billing State',
              'billing_country' => 'Billing Country',
              'billing_zipcode' => 'Billing Zipcode',
              'billing_phone' => 'Billing Phone', */
            'salesmenArr' => 'Select at least one',
            'chapterId' => 'Chapter',
            'salesTeamId' => 'Sales Team',
            'published' => 'Vendor Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('vendor_id', $this->vendor_id, true);
        //$criteria->compare('pg_id', $this->pg_id, true);
        $criteria->compare('vendor_name', $this->vendor_name, true);
        $criteria->compare('vendor_description', $this->vendor_description, true);
        //$criteria->compare('internal_notes', $this->internal_notes, true);
        //$criteria->compare('logo', $this->logo, true);
        //$criteria->compare('vendor_banner_image', $this->vendor_banner_image, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('website', $this->website, true);
        $criteria->compare('industry', $this->industry, true);
        /* $criteria->compare('disabled', $this->disabled);
          $criteria->compare('created_on', $this->created_on, true);
          $criteria->compare('updated_on', $this->updated_on, true);
          $criteria->compare('expiration', $this->expiration, true);
          $criteria->compare('max_offers', $this->max_offers);
          $criteria->compare('max_banners', $this->max_banners);
          $criteria->compare('max_categories', $this->max_categories);
          $criteria->compare('banner_expiration', $this->banner_expiration, true);
          $criteria->compare('max_branches', $this->max_branches);
          $criteria->compare('plan_id', $this->plan_id, true);
          $criteria->compare('customer_number', $this->customer_number);
          $criteria->compare('payment_method', $this->payment_method, true);
          $criteria->compare('billing_name', $this->billing_name, true);
          $criteria->compare('billing_address', $this->billing_address, true);
          $criteria->compare('billing_state', $this->billing_state, true);
          $criteria->compare('billing_country', $this->billing_country, true);
          $criteria->compare('billing_zipcode', $this->billing_zipcode, true);
          $criteria->compare('billing_phone', $this->billing_phone, true); */

        return $criteria;

        /* return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          )); */
    }

    /**
     * Create new Vendor and all the related object. There are many things
     * happening here, so we need to enclose everything to transaction.
     * Output is assoc array with all the created object. Check for success
     * field to see if there were some errors
     * @param array $input(user,vendor)
     */
    public static function create($input) {

        Yii::import('application.helpers.AddressHelper');

        $output = array('success' => false);
        // two basic objects we need to create vendor
        if (isset($input['vendor']) && isset($input['user'])) {
            // make vendor first
            $vendor = new Vendor();
            $vendor->attributes = $input['vendor'];
            // set some default values
            $vendor->created_on = Shared::timeNow();
            $vendor->updated_on = Shared::timeNow();

            // make sure we have expiration set
            if (!$vendor->expiration) {
                $vendor->expiration = Shared::toDatabase(strtotime(Shared::timeNow()));
            }
            if (!$vendor->max_branches) {
                $vendor->max_branches = 3;
            }

            //Check to see if the logged in user has access to set the selected plan (this is to prevent hacks)
            if (is_object($vendor->plan)) {
                if (!$vendor->plan->hasAccess()) {
                    $vendor->plan_id = null;
                }
            }

            // crate user
            // TODO: active / inactive and how to send an email
            $user = User::create($input['user']);

            if ($vendor->validate() && $user->validate()) {
                // get the correct address from google
                $address = AddressHelper::toLatLon($vendor->address);
                if (isset($address['formatted'])) {
                    $vendor->address = $address['formatted'];
                }
                $connection = Yii::app()->db;
                $transaction = $connection->beginTransaction();

                $vendor->save();
                $user->address = $vendor->address;
                $user->save();

                $output['vendor'] = $vendor;
                $output['user'] = $user;

                $userVendor = UserVendor::create($user, $vendor);
                // update only this attribute
                if (isset($input['userVendor'])) {
                    $userVendor->send_redemption_email = $input['userVendor']['send_redemption_email'];
                }
                if (!$userVendor->save()) {
                    $output['userVendor'] = $userVendor;
                    $transaction->rollBack();
                    return $output;
                }
                $output['userVendor'] = $userVendor;

                // construct a branch from the information we have
                $branchData = array(
                    'vendor_id' => $vendor->vendor_id,
                    'branch_name' => $vendor->vendor_name,
                    'branch_address' => $vendor->address,
                    //'phone_number' => $user->phone_number, // commented out as-per the july 2013 task document - travis
                    'zipcode' => isset($address['zipcode']) ? $address['zipcode'] : null,
                    'lat' => isset($address['lat']) ? $address['lat'] : null,
                    'lon' => isset($address['lon']) ? $address['lon'] : null
                );
                $branch = new Branch;
                $branch->attributes = $branchData;
                if (!$branch->save()) {
                    $output['branch'] = $branch;
                    $transaction->rollBack();
                    return $output;
                }
                $output['branch'] = $branch;
                //Shared::debug($input['vendor']);
                // link vendor to chapter
                if (isset($input['vendor']['chapterId'])) {
                    $chapter = Chapter::model()->findByPk((int) $input['vendor']['chapterId']);
                    if (is_object($chapter)) {
                        $vendorMembership = VendorMembership::create($vendor, $chapter);
                        if (!$vendorMembership->save()) {
                            $output['vendorMembership'] = $vendorMembership;
                            $transaction->rollBack();
                            return $output;
                        }
                    }
                }

                // creating test data from cli does not allow using app()->user
                if (php_sapi_name() !== 'cli') {

                    // TODO: create a plan for vendor and logo
                    // link it with sales team automatically
                    $stId = app()->user->getActiveSt();
                    if ($stId) {
                        $salesTeam = SalesTeam::model()->findByPk($stId);

                        if (is_object($salesTeam)) {
                            Shared::debug($salesTeam->st_name, "linking to sales team");
                            $stVendor = StVendor::create($salesTeam, $vendor);
                            if (!$stVendor->save()) {
                                $output['stVendor'] = $stVendor;
                                $transaction->rollBack();
                                return $output;
                            }
                            $output['stVendor'] = $stVendor;

                            // we don't want to accidently assign an administrator
                            if (!app()->user->isAdmin()) {
                                // make this salesman a contact person for this vendor
                                $vendorSalesman = VendorSalesman::create(app()->user->getUser(), $vendor);
                                if (!$vendorSalesman->save()) {
                                    $output['vendorSalesman'] = $vendorSalesman;
                                    $transaction->rollBack();
                                    return $output;
                                }
                                $output['vendorSalesman'] = $vendorSalesman;
                            }
                        }
                    } else if (app()->user->isAdmin()) {
                        // set sales team from drop down select
                        if (isset($input['vendor']['salesTeamId'])) {
                            $salesTeam = SalesTeam::model()->findByPk($input['vendor']['salesTeamId']);
                            if ($salesTeam != null) {
                                $stVendor = StVendor::create($salesTeam, $vendor);
                                if (!$stVendor->save()) {
                                    $output['stVendor'] = $stVendor;
                                    $transaction->rollBack();
                                    return $output;
                                }
                            }
                        } else {
                            $transaction->rollBack();
                            return $output;
                        }
                    }
                }

                // everything is ok
                $transaction->commit();
                $output['success'] = true;
            } else {
                // to display errors inside the form
                $output['vendor'] = $vendor;
                $output['user'] = $user;
                Shared::debug($user->getErrors());
                Shared::debug($vendor->getErrors());
                Shared::debug("rollback");
            }
        }
        return $output;
    }

    public function getBeforeCreateModel() {
        if (!$this->expiration) {
            $this->expiration = Shared::toDatabase(strtotime(Shared::timeNow() . " +35 day"));
        }
    }

    /**
     * Has this vendor current subscription for banners? We want to display
     * additional link in the menu and more fields in the compnay preferences
     * screen
     */
    public function isBannerEnabled() {
        if ($this->max_banners) {
            return true;
        }
        if (count($this->banners)) {
            return true;
        }
        return false;
    }

    /**
     * Loads only active ones and cuts down to max_branches
     */
    public function getLimitedBranches() {
        if (count($this->activeBranches) <= $this->max_branches) {
            return $this->activeBranches;
        } else {
            return array_slice($this->activeBranches, 0, $this->max_branches);
        }
    }

    /**
     * Returns the first chapter this vendor belongs to. We might modify
     * this for national vendors.
     * @return null
     */
    public function getChapter() {
        if (isset($this->chapters[0])) {
            //Shared::debug($this->chapters[0]);
            return $this->chapters[0];
        }
        return null;
    }

    /**
     * We need to load whole list of vendors for given user (chapter), which are
     * active and we want to attach branches. Also, it loads only limited set of attributes (performance)
     * It uses DAO instead of AR for performance reasons.
     * Returns multi-dimensional member array used in json api
     */
    public static function getApiList($chapterId, $timestamp = null, $user = null) {
        Shared::debug($chapterId, 'chapter');
        // TODO: chapter, caching
        $db = app()->db;
        $vendors = array();

        if (is_object($user)) {
            // load vendors by user
            $_vendors = $user->getVendorIds();

            if ($user->isAdmin()) {
                $sql = "SELECT v.vendor_id, vendor_name, vendor_description, industry, website, logo, 
                vendor_banner_image, UNIX_TIMESTAMP(DATE_ADD(expiration, INTERVAL 1 DAY)) AS 'timestamp_sec', published
                FROM vendor v LEFT JOIN vendor_membership m ON v.vendor_id = m.vendor_id 
                WHERE m.chapter_id=$chapterId";
            } else {
                $sql = "SELECT v.vendor_id, vendor_name, vendor_description, industry, website, logo, 
                vendor_banner_image, UNIX_TIMESTAMP(DATE_ADD(expiration, INTERVAL 1 DAY)) AS 'timestamp_sec', published
                FROM vendor v LEFT JOIN vendor_membership m ON v.vendor_id = m.vendor_id 
                WHERE m.chapter_id=$chapterId AND v.vendor_id IN (" . implode(",", $_vendors) . ")";
            }
            // we want to display all vendors
            if ($timestamp != null) {
                // get updates only
                $sql .= " AND v.updated_on > '$timestamp' AND disabled=0";
            } else {
                // get everything
                $sql .= " AND disabled=0";
            }
        } else {
            // chapter member user
            $sql = "SELECT v.vendor_id, vendor_name, vendor_description, industry, website, logo, 
            vendor_banner_image, UNIX_TIMESTAMP(DATE_ADD(expiration, INTERVAL 1 DAY)) AS 'timestamp_sec', published
            FROM vendor v LEFT JOIN vendor_membership m ON v.vendor_id = m.vendor_id 
            WHERE m.chapter_id=$chapterId AND v.activated=1";
            if ($timestamp != null) {
                // get updates only
                $sql .= " AND v.updated_on > '$timestamp' AND disabled=0";
            } else {
                // get everything
                $sql .= " AND disabled=0 AND m.active>0 AND published=1 AND DATE_ADD(expiration, INTERVAL 1 DAY)>NOW()";
            }
        }



        Shared::debug($sql);

        $command = $db->createCommand($sql);
        $dataReader = $command->query();

        while (($row = $dataReader->read()) !== false) {
            $vendors[$row['vendor_id']] = array(
                'id' => $row['vendor_id'] + 0,
                'favorite' => 0, // we load this value later on in the controller
                'branches' => array(),
                'name' => $row['vendor_name'],
                'description' => Shared::textToPhone($row['vendor_description']),
                'website' => $row['website'],
                'categoryId' => $row['industry'] + 0,
                'expiration' => $row['timestamp_sec'],
                'published' => $row['published'] + 0,
                'logo' => Image::getCachedBase64Src($row['logo']),
                    //'logo' => Image::getImageUrl($row['logo'], 80),
            );
        }

        // lets load branches
        if (count($vendors)) {
            // TODO: lets do something with timestamp, so we don't have to load everything
            $sql = "SELECT b.* FROM branch b LEFT JOIN vendor_membership m ON b.vendor_id = m.vendor_id WHERE b.active > 0 AND m.active > 0";
            $command = $db->createCommand($sql);
            $dataReader = $command->query();
            while (($row = $dataReader->read()) !== false) {
                if (isset($vendors[$row['vendor_id']])) {
                    $vendors[$row['vendor_id']]['branches'][] = array(
                        'id' => $row['branch_id'],
                        'name' => $row['branch_name'],
                        'address' => $row['branch_address'],
                        'phone' => Shared::formatPhone($row['phone_number']),
                        'website' => $row['website'],
                        'hours' => Shared::textToPhone($row['hours']),
                        'vendorId' => $row['vendor_id'],
                        'lat' => $row['lat'],
                        'lon' => $row['lon']
                            //'distance' => '' // fake field, we just want it to be part of the model
                    );
                }
            }
        }

        return $vendors;
    }

    /*
     * @return total branches count for a specified vendor
     */

    public function getTotalBranches() {
        $connection = app()->db;
        $query = "SELECT count(branch_id) FROM branch WHERE vendor_id='" . $this->vendor_id . "' AND active = 1;";
        $run = $connection->createCommand($query);
        $count = $run->queryScalar();
        return $count;
    }

    /**
     * @return total number of active offers offered by a Vendor
     * @author Vinayak Melarkod <vk@inradiussystems.com>
     */
    public function getTotalActiveOffers() {

        $connection = Yii::app()->db;
        $sql = "SELECT COUNT(offer_id) FROM offer
                    WHERE vendor_id=" . $this->vendor_id . " AND
                    start_date<=DATE(NOW()) AND end_date>=DATE(NOW()) AND published>0;";
        $command = $connection->createCommand($sql);
        $value = $command->queryScalar();
        return $value;
    }

    /**
     * @return total number of offers offered by a Vendor
     */
    public function getTotalOffers() {

        $connection = Yii::app()->db;
        $sql = "SELECT COUNT(offer_id) FROM offer WHERE vendor_id=" . $this->vendor_id . ";";
        $command = $connection->createCommand($sql);
        $value = $command->queryScalar();
        return $value;
    }

    /**
     * @return total number of active offers offered by a Vendor
     * @author Vinayak Melarkod <vk@inradiussystems.com>
     */
    public function getTotalActiveBanners() {

        $connection = Yii::app()->db;
        $sql = "SELECT COUNT(banner_id) FROM banner
                    WHERE vendor_id=" . $this->vendor_id . " AND
                    start_date<=DATE(NOW()) AND end_date>=DATE(NOW()) AND published>0;";
        $command = $connection->createCommand($sql);
        $value = $command->queryScalar();
        return $value;
    }

    /**
     * Total number of offers, which are in planning or waiting state
     * @return type
     */
    public function getTotalNewOffers() {
        $connection = Yii::app()->db;
        $sql = "SELECT COUNT(offer_id) FROM offer
                    WHERE vendor_id=" . $this->vendor_id . " AND
                    start_date<=NOW();";
        $command = $connection->createCommand($sql);
        $value = $command->queryScalar();
        return $value;
    }

    /**
     * Total number of offers, which were published and not displayed anymore
     * @return type
     */
    public function getTotalExpiredOffers() {
        $connection = Yii::app()->db;
        $sql = "SELECT COUNT(offer_id) FROM offer
                    WHERE vendor_id=" . $this->vendor_id . " AND
                    end_date>=NOW() AND published>0";
        $command = $connection->createCommand($sql);
        $value = $command->queryScalar();
        return $value;
    }

    /**
     * Total number of offers, which were published and not displayed anymore
     * @return type
     */
    public function getTotalExpiredBanners() {
        $connection = Yii::app()->db;
        $sql = "SELECT COUNT(offer_id) FROM banner
                    WHERE vendor_id=" . $this->vendor_id . " AND
                    end_date>=NOW() AND published>0";
        $command = $connection->createCommand($sql);
        $value = $command->queryScalar();
        return $value;
    }

    /**
     * Load helper variables (salesman) to virtual attribute
     */
    public function beforeFormLoad() {
        $this->salesmenArr = array();
        foreach ($this->salesmen as $salesman) {
            $this->salesmenArr[] = $salesman->user_id;
        }
    }

    /**
     * Set the correct updated_on mark
     */
    public function afterValidate() {
        parent::afterValidate();
        if ($this->isDirty()) {
            $this->updated_on = Shared::toDatabase(time());
            // update last date on offers and banners in order to send
            // updates through API
            if ($this->isDirtyAttribute('published')) {
                foreach ($this->activeOffers as $offer) {
                    $offer->updated_on = Shared::toDatabase(time());
                    $offer->save();
                }
                foreach ($this->activeBanners as $banner) {
                    $banner->updated_on = Shared::toDatabase(time());
                    $banner->save();
                }
            }
        }
    }
    
    public function afterSave() {
        parent::afterSave();
        app()->cache->delete('user-' . app()->user->getUser()->user_id);
    }

    /**
     * Salesman (contact persons) are stored in M:N table
     */
    public function saveSalesman() {
        if (!is_array($this->salesmenArr)) {
            $this->beforeFormLoad();
        }

        // load existing ones first
        $existing = $this->salesmen;
        if (!is_array($existing)) {
            $existing = array();
        }
        $_existing = array();
        foreach ($existing as $user) {
            $_existing[$user->user_id] = true;
        }

        // create the connections
        foreach ($this->salesmenArr as $userId) {
            if (!isset($_existing[$userId])) {
                $salesman = new VendorSalesman;
                $salesman->vendor_id = $this->vendor_id;
                $salesman->user_id = $userId;
                $salesman->created_on = Shared::timeNow();
                $salesman->save();
            }
        }

        // delete old ones
        $new = array_flip($this->salesmenArr);

        foreach ($existing as $user) {
            if (!isset($new[$user->user_id])) {
                // Shared::debug("delete old");
                $salesman = VendorSalesman::model()->findByAttributes(array('vendor_id' => $this->vendor_id, 'user_id' => $user->user_id));
                $salesman->delete();
            }
        }
    }

    public function getSalesTeamNames() {
        $output = "";
        foreach ($this->salesTeams as $st) {
            $output .= $st->st_name . "<br />";
        }
        if (strlen($output)) {
            return substr($output, 0, strlen($output) - 6);
        }
        //Shared::debug($output);
        return $output;
    }

    /**
     * This function returns the current subscription status of a vendor. The possible outputs are "not active yet", "active", "expiring soon" and "expired". 
     */
    public function getSubscriptionStatus($subscription = null) {
        $expiration = strtotime($this->expiration);
        $created = strtotime($this->created_on);

        $status = "Unknown";
        if (abs($expiration - $created) < 172800 || $expiration == null) {//if expiration and created dates are two days apart, or expiration not set, then the subscription has not started yet.
            $status = self::SUB_NOT_ACTIVE;
        } else if ($expiration < time()) {
            $status = self::SUB_EXPIRED;
        } else if (($expiration - time()) < Shared::day * 3) {
            $status = self::SUB_EXPIRE_SOON;
        } else if ($expiration > time()) {
            $status = self::SUB_ACTIVE;
        }
        
        if ($subscription != null){
            if ($status == self::SUB_ACTIVE && !$subscription->currentlyRecurring){
                $status = self::SUB_EXPIRE_SOON;
            }
        }
        return $status;
    }

    /**
     * Use the status field in a table and give it a color
     */
    public function getColoredStatus($subscription = null) {
        $status = $this->getSubscriptionStatus($subscription);
        $color = 'inverse';

        switch ($status) {
            case self::SUB_NOT_ACTIVE:
                $color = null;
                break;
            case self::SUB_EXPIRE_SOON:
                $color = 'warning';
                break;
            case self::SUB_ACTIVE:
                $color = 'success';
                break;
            case self::SUB_EXPIRED:
                $color = 'important';
            default:
                break;
        }
        return '<span class="label' . (($color ? ' label-' . $color : '')) . '">' . $status . '</span>';
    }

    /**
     * Was the vendor completely approved by admin and everything is matching?
     * Returns either true or list of missing thigs for the verification.
     */
    public function isVerified() {
        if ($this->activated == 1) {
            return true;
        } else {
            $errors = array();
            // verify user email address
            $user = $this->getContactUser();
            if ($user == null) {
                $errors['user'] = 'Complete your contact information <a href="' . url('/vendor/update/' . $this->vendor_id) . '">here</a>.';
            } else {
                // is the email verified?
                if (!$user->email_verified) {
                    $admin = '';
                    if (app()->user->isAdmin()) {
                        $admin = 'As an admin you can verify this email address without sending an activation email.';
                    }
                    $errors['user'] = '<div id="email-verification-placeholder"><span class="ajax-link" id="verify-email" data-email-for="' .
                            $user->user_id . '">Send email</span> to verify my email address: ' .
                            $user->email_address . ". $admin</div>";
                }
            }
            
            if (strlen($this->vendor_description) < 50){
                $errors['vendor'] = 'Provide a brief <a href="' . url('/vendor/update/' . $this->vendor_id) . '">description</a> of your company. It is a good way how to promote your company.' ;
            }
            
            if ($this->logo == null){
                $errors['logo'] = 'Upload your brand <a href="' . url('/vendor/update/' . $this->vendor_id) . '">logo</a>.';
            }
            
            if ($this->getSubscriptionStatus() == self::SUB_NOT_ACTIVE && $this->published != 1) {
                $errors['subscription'] = 'Please visit <a href="' . url('/subscription/update/' . $this->vendor_id) . '">subscription screen</a> to activate.';
            }
            
            // verify company logo
            // TODO
            // load an offer
            /*
              $offers = $this->offers;
              if (count($offers) == 0) {
              $errors['offer'] = 'Create a great offer <a href="' . url('/offer/create') . '">here</a>.';
              } else {
              $published = false;
              foreach ($offers as $offer) {
              if ($offer->published) {
              $published = true;
              }
              }
              if (!$published) {
              $errors['offer'] = 'Publish your offer <a href="' . url('/offer/update/' . $offers[0]->offer_id) . '">here</a>';
              }
              }



              // is the activation request sent?
              if (count($errors) == 0) {
              // everything else is ready
              $errors['activate'] = '<div id="vendor-activation-placeholder">Let us know you are ready, so we can start your free trial. Send us an <span class="ajax-link" id="activate-vendor">activation request</span>.</div>';
              } else {
              $errors['activate'] = 'Send an activation request to AUSA Offers when my offer is ready.';
              }
             */

            if (count($errors) != 0) {
                return $errors;
            } else {
                return true;
            }
        }
    }

    /**
     * Apply features from selected plan and set expiration from 33 days from the given plan
     */
    public function extendSubscription($date, $amount) {
        if (is_object($this->plan)) {
            Shared::debug($this->plan->monthly_recurring_price, $amount);
            if ($this->plan->monthly_recurring_price <= ($amount + 0)) {
                // the payment level is correct, let's check the date
                // it is one month plus three days as a grace period
                $extendedDate = Shared::toDatabase(strtotime(Shared::toDatabase($date) . " +33 day"));
                Shared::debug($extendedDate);
                $change = false;

                // banners
                if ($this->plan->max_banners > 0) {
                    if (strtotime($this->banner_expiration) < time() + Shared::day * 14) {
                        $this->banner_expiration = $extendedDate;
                        $change = true;
                    } else {
                        Shared::debug("Vendor banner is valid until $this->banner_expiration");
                    }
                }

                // offers
                if ($this->plan->max_offers > 0) {
                    if (strtotime($this->expiration) < time() + Shared::day * 14) {
                        $this->expiration = $extendedDate;
                        $change = true;
                    } else {
                        Shared::debug("Vendor is valid until $this->expiration");
                    }
                }

                if ($change) {
                    Shared::debug("vendor subscription extended");
                    $this->save();
                    // TODO: send subscription extension email
                }
            } else {
                // TODO: send failed email if the price does not match
            }
        }
    }

}

//end class
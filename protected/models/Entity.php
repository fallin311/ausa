<?php

/**
 * This is the model class for table "entity".
 *
 * The followings are the available columns in table 'entity':
 * @property string $entity_id
 * @property string $entity_name
 * @property string $entity_company_name
 * @property string $entity_url
 */
class Entity extends CActiveRecord {
    //public $entity_id;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Entity the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'entity';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('entity_name, entity_url, ganalytics_id, landing_page', 'required'),
            array('entity_name, entity_company_name', 'length', 'max' => 45),
            array('entity_url', 'length', 'max' => 127),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('entity_id, entity_name, entity_company_name, entity_url', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'entity_id' => 'Entity',
            'entity_name' => 'Entity Name',
            'entity_company_name' => 'Entity Company Name',
            'entity_url' => 'Entity Url',
            'ganalytics_id' => 'Google Analytics ID',
            'landing_page' => 'Landing Page',
        );
    }

    /**
     * Set active entity we are working with. It allows us to load different CSS and 
     * check if user logging in belongs to that particular entity
     * @param type $id 

      public function setActiveEntity($host) {
      $host = "ausaoffers.com";
      //Shared::debug("host: " . $host);
      $model = self::model()->findByAttributes(array('entity_url' => $host));
      if (is_object($model)) {
      $this->entity_id = $model->entity_id;
      return $this->entity_id;
      } else {
      return false;
      }
      }

      public function getActiveEntity() {
      return $this->entity_id;
      } */
    public function getEntity() {
        if (!app()->user->getState('entity')) { // is the entity already set?
            $host = self::cleanEntity(app()->getBaseUrl(true)); // what is the domain they are currently at
            $model = self::model()->findByAttributes(array('entity_url' => $host)); // match the current domain to an entity
            if (is_object($model)) {
                $this->entity_id = $model->entity_id;
                app()->user->setState('entity', $this->entity_id); // store the domain/entity match in session
                return $this->entity_id;
            } else {
                if(app()->user->isAdmin()){
                    // admin can log in without an entity, so set something here
                    app()->user->setState('entity', '1');
                    return '1';
                } else {
                    Shared::debug("Attempted access from unknown entity: " . $host);
                    return null;
                }
            }
        } else {
            return app()->user->getState('entity');
        }
    }

    public function belongsToEntity($user) {
        $entity_id = app()->user->getState('entity'); // entity should be set in the session at this point
        if (!isset($entity_id)) // but if not
            $entity_id = $this->getEntity();
        if ($user->isAdmin()) { //admin can login under any entity... but will be able to perform only entity specific actions
            return true;
        } else if ($user->isChapterUser()) {
            $all = $user->getChapterIds();
            $chapter = Chapter::model()->findByPk($all[0]);
            if (is_object($chapter) && $chapter->entity_id == $entity_id) {
                return true;
            } else {
                return false;
            }
        } else if ($user->isSalesman()) {
            $all = $user->getStIds();
            $st = SalesTeam::model()->findByPk($all[0]);
            if (is_object($st) && $st->chapter->entity_id == $entity_id) {
                return true;
            } else {
                return false;
            }
        } else if ($user->isVendor()) {
            $all = $user->getVendorIds();
            $vendor = Vendor::model()->findByPk($all[0]);
            if (is_object($vendor) && $vendor->chapters[0]->entity_id == $entity_id) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Need to get rid of the http or https (and probably the www) to be safe.
     * @param type $url
     * @return type
     */
    public function cleanEntity($url) {
        $clean = array('http://', 'https://', 'http://www.', 'https://www.');
        foreach ($clean as $c) {
            if (strpos($url, $c) === 0) {
                return str_replace($c, '', $url);
            }
        }
        return $url;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('entity_id', $this->entity_id, true);
        $criteria->compare('entity_name', $this->entity_name, true);
        $criteria->compare('entity_company_name', $this->entity_company_name, true);
        $criteria->compare('entity_url', $this->entity_url, true);
        $criteria->compare('ganalytics_id', $this->ganalytics_id, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}
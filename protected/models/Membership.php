<?php

/**
 * This is the model class for table "membership".
 *
 * The followings are the available columns in table 'membership':
 * @property string $membership_id
 * @property string $member_id
 * @property string $chapter_id
 * @property integer $subchapter_id
 * @property string $ausa_product_code
 * @property string $join_date
 * @property string $expiration_date
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Chapter $chapter
 * @property Member $member
 */
class Membership extends AOActiveRecord {
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Membership the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'membership';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('chapter_id', 'required'),
            array('member_id', 'required', 'on' => 'afterMemberCreate'), // this object is created together with member
            array('subchapter_id, active', 'numerical', 'integerOnly' => true),
            array('member_id, chapter_id', 'length', 'max' => 10),
            array('ausa_product_code', 'length', 'max' => 8),
            array('subchapter_id, ausa_product_code', 'default', 'setOnEmpty' => true), // make empty values stored as NULL
            array('join_date, expiration_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'chapter' => array(self::BELONGS_TO, 'Chapter', 'chapter_id'),
            'member' => array(self::BELONGS_TO, 'Member', 'member_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'membership_id' => 'Membership',
            'member_id' => 'Member',
            'chapter_id' => 'Chapter',
            'subchapter_id' => 'Subchapter',
            'ausa_product_code' => 'Ausa Product Code',
            'join_date' => 'Join Date',
            'expiration_date' => 'Expiration Date',
            'active' => 'Active',
        );
    }

    public static function create($member, $chapter) {
        if (is_numeric($member))
            $member = Member::model()->findByPk($member);
        if (is_numeric($chapter))
            $chapter = Chapter::model()->findByPk($chapter);
        if ($member != null && $chapter != null) {
            $membership = self::model()->findByAttributes(array('chapter_id' => $chapter->chapter_id, 'member_id' => $member->member_id));
            if ($membership == null) {
                $membership = new Membership;
                $membership->member_id = $member->member_id;
                $membership->chapter_id = $chapter->chapter_id;
                $membership->join_date = Shared::timeNow();
                $membership->expiration_date = Shared::timeNow();
                $membership->active = 1;
            }
            return $membership;
        }
        return null;
    }
    
    /**
     * When registering or renewing this date reflects current expiration
     * date or the current date.
     * @return int start date of this subscription
     */
    public function getNewStartDate(){
        $exp = strtotime($this->expiration_date);
        $now = time();
        if ($exp > $now){
            return $exp;
        } else {
            return $now;
        }
    }

    /**
     * Check for account expiration and chapter
     * @return type
     */
    public function isActive(){
        return strtotime($this->expiration_date) > time() && $this->expiration_date && $this->chapter->disabled == 0;
    }
}
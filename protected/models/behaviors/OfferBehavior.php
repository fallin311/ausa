<?php

/**
 * Common functions for banner and offer
 */
class OfferBehavior extends CBehavior {

    public function isPublished() {
        if ($this->owner->published > 0) {
            return true;
        }
        return false;
    }

    /**
     * Returns one of the stages of the offer: planning, waiting, active, expired
     * The offer banner is valid starting midnight and ending midnight
     * This value is calculated on the fly
     */
    public function getStatus() {
        $start = strtotime($this->owner->start_date . ' 0:0');
        $end = strtotime($this->owner->end_date . ' 23:59');
        if (!$start || (!$this->isPublished() && $end > time())) {
            return "planning";
        }
        if ($this->isPublished() && $start > time()) {
            return "waiting";
        }

        if ($this->isPublished() && $start < time() && $end > time()) {
            return "active";
        }
        if ($end < time()) {
            return "expired";
        }
        return "unknown";
    }

    /**
     * Use the status field in a table and give it a color
     */
    public function getColoredStatus() {
        $status = $this->getStatus();
        $color = 'inverse';

        switch ($status) {
            case 'planning':
                $color = null;
                break;
            case 'waiting':
                $color = 'warning';
                break;
            case 'active':
                $color = 'success';
                break;
            case 'expired':
                $color = 'important';
            default:
                break;
        }
        return '<span class="label'.(($color?' label-'.$color:'')).'">'.ucfirst($status).'</span>';
    }

    /**
     * Displays dates information inside offer admin table
     */
    public function getDatesForGrid() {
        $status = $this->getStatus();
        $dates = '';
        if ($status != 'expired' && $status != 'unknown') {
            $dates .= "Starts on " . Shared::formatDateLonger($this->owner->start_date);
            if ($status == 'active') {
                // running already
                $days = floor((time() - strtotime($this->owner->start_date)) / Shared::day) - 1;
                if ($days == 0)
                    $dates .= ' - today';
                if ($days == 1)
                    $dates .= ' - 1 day ago';
                if ($days > 1)
                    $dates .= " - $days days ago";
            }else {
                // waiting
                $days = floor((strtotime($this->owner->start_date) - time()) / Shared::day) - 1;
                if ($days == 0)
                    $dates .= ' - today';
                if ($days == 1)
                    $dates .= ' - in 1 day';
                if ($days > 1)
                    $dates .= " - in $days days";
            }
            $dates .= "<br />";
            $dates .= "Expires on " . Shared::formatDateLonger($this->owner->end_date);

            if ($status == 'active') {
                $days = floor((strtotime($this->owner->end_date) - time()) / Shared::day) - 1;
                if ($days == 0)
                    $dates .= ' - today';
                if ($days == 1)
                    $dates .= ' - in 1 day';
                if ($days > 1)
                    $dates .= " - in $days days";
            }
        }else {
            // just regular start / end date
            $dates .= "Starts on " . Shared::formatDateLonger($this->owner->start_date) . "<br />";
            $dates .= "Expires on " . Shared::formatDateLonger($this->owner->end_date);
        }
        return $dates;
    }

    public function getStatsForGrid() {
        if ($this->owner->impressions == 0 && $this->owner->clicks == 0) {
            $html = 'No Activity Yet';
            return $html;
        }

        $stats = $this->owner->impressions . " impressions<br />";
        $stats .= $this->owner->clicks . ' clicks<br />';

        // banners don't have redemptions
        if (isset($this->owner->redemptions)) {
            $stats .= $this->owner->redemptions . " redemptions";
        }
        return $stats;
    }

}

?>

<?php

/**
 * Implement all special setters used during member upload.
 * This is different from validators in a sense that if something
 * is not valid, we just ignore it.
 */
class UploadMemberBehavior extends CBehavior {

    private $membership;

    /**
     * Send a regisration email after the member is created
     * @var type 
     */
    public $sendRegistrationEmail;

    /**
     * Set this when you dont know the chapter id yet
     * @var string 
     */
    public $chapterCode;
    
    /**
     * Payment for information coming from member registration form
     * @var type 
     */
    public $payment;

    public $repeatEmail;
    
    function setFirstName($string) {
        $name = Shared::removeDiacritics($string);
        $this->owner->first_name = CHtml::encode(ucfirst(strtolower($name)));
    }

    function setLastName($string) {
        $name = Shared::removeDiacritics($string);
        $this->owner->last_name = CHtml::encode(ucfirst(strtolower($name)));
    }
    
    /**
     * Parse full name into first name and last name
     * @param type $fullName
     */
    function setFullName($fullName) {
        $nameParts = explode(" ", $fullName);
        if (count($nameParts) > 0) {
            // for required validation rule
            $this->owner->fullName = $fullName;
            
            //$this->first_name = CHtml::encode(trim(Shared::ucfirst($nameParts[0])));
            //Shared::debug($this->first_name);
            $this->owner->first_name = CHtml::encode(trim(ucfirst(strtolower($nameParts[0]))));
            if (count($nameParts) > 1) {
                $this->owner->last_name = '';
                for ($i = 1; $i < count($nameParts); $i++) {
                    $this->owner->last_name .= " " . CHtml::encode(trim(ucfirst(strtolower($nameParts[$i]))));
                }
                $this->owner->last_name = trim($this->owner->last_name);
            }
        }
    }

    function setEmail($string) {
        if (Shared::isEmailValid($string)) {
            $this->owner->email_address = strtolower($string);
        } else {
            Shared::debug($string, "email is not valid");
        }
    }

    function setZipcode($zipcode) {
        // we have only two simple validation rules. It wont work outside USA
        if (strlen($zipcode) == 5 && is_numeric($zipcode)) {
            // basic
            $this->owner->zipcode = $zipcode;
        } else {
            // extended
            $parts = explode('-', $zipcode);
            if (count($parts) == 2 && is_numeric($parts[0]) && is_numeric($parts[1])) {
                $this->owner->zipcode = $zipcode;
            }
        }
    }

    /**
     * Many phone numbers from the default file are not valid, eliminate them
     * here. Also ignore extensions
     */
    function setPhoneNumber($string) {
        $value = str_replace(" ", '', $string);

        // allow null values
        if (strlen($value) == 0)
            return false;

        $formats = array('###-###-####', '####-###-###', '#########',
            '(###)###-###', '####-####-####', '(###)###-####', '#(###)###-####',
            '##-###-####-####', '####-####', '###-###-###',
            '#####-###-###', '##########', '+############', '00############');
        $format = preg_replace("/[0-9]/", "#", $value);
        $format = str_replace(' ', '-', $format);
        if (in_array($format, $formats)) {

            $value = str_replace("-", '', $value);
            $value = str_replace("(", '', $value);
            $value = str_replace(")", '', $value);
            // update the value
            $this->owner->phone_number = $value;
        }
    }

    public function setMembership($membership) {
        $this->membership = $membership;
    }

    public function getTempMembership() {
        if ($this->membership == null) {
            $this->membership = new Membership;
        }
        return $this->membership;
    }

    /**
     * Search for existing membership in given chapter and create empty object
     * if there is no membership
     * @param type $chapterId
     * @return type
     */
    public function findMembership($chapterId) {
        $this->membership = Membership::model()->findByAttributes(array('member_id' => $this->owner->member_id, 'chapter_id' => $chapterId));
        if ($this->membership == null) {
            $this->membership = new Membership;
        }
        return $this->membership;
    }

    /**
     * Membership function
     */
    function setJoinDate($string) {
        $date = str_replace('.', '-', $string);
        $time = strtotime($date);
        if ($time) {
            $this->membership->join_date = Shared::toDatabase($time);
        }
    }

    /**
     * Membership function
     */
    function setExpirationDate($string) {
        $date = str_replace('.', '-', $string);
        $time = strtotime($date);
        if ($time) {
            $this->membership->expiration_date = Shared::toDatabase($time);
        }
    }

    /**
     * Membership function
     */
    function setSubchapter($string) {
        $this->membership->subchapter_id = (int) $string;
    }

    function setChapterCode($string) {
        $this->chapterCode = $string;
    }

    function getChapterCode() {
        return $this->chapterCode;
    }

    function setProductCode($string) {
        $allowed = array(
            'IND', 'DESCORP', 'DESUS', 'LIFEFULL', 'LIFE55', 'LIFE59', 'LIFE64', 'HONOR'
        );
        if (in_array($string, $allowed)) {
            $this->membership->ausa_product_code = $string;
        }
    }

    /**
     * Use this class to process the values submitted through POST form
     * @param type $input
     */
    function setAttributesFromForm($input) {

        // Member
        if (isset($input['Member']['fullName'])){
            Shared::debug("set full name");
            $this->setFullName($input['Member']['fullName']);
        } else {
            $this->setFirstName($input['Member']['first_name']);
            $this->setLastName($input['Member']['last_name']);
        }
        
        $this->owner->prefix = CHtml::encode($input['Member']['prefix']);
        $this->setEmail($input['Member']['email_address']);
        $this->setPhoneNumber($input['Member']['phone_number']);
        $this->owner->address1 = CHtml::encode($input['Member']['address1']);
        $this->owner->address2 = CHtml::encode($input['Member']['address2']);
        $this->setZipcode($input['Member']['zipcode']);
        $this->owner->city = CHtml::encode($input['Member']['city']);
        $this->owner->state = CHtml::encode($input['Member']['state']);
        //$this->owner->country = CHtml::encode($input['Member']['country']);
        //$this->owner->army_status = CHtml::encode($input['Member']['army_status']);
        //$this->owner->military_unit = CHtml::encode($input['Member']['military_unit']);
        $this->owner->birthday = CHtml::encode($input['Member']['birthday']);
        
        if (isset($input['Member']['ausa_id'])) {
            $this->owner->ausa_id = CHtml::encode($input['Member']['ausa_id']);
        }

        // quick and dirty email repeat validator
        if (isset($input['Member']['repeatEmail'])){
            $this->repeatEmail = $input['Member']['repeatEmail'];
        }
        
        if ($this->owner->email_address != $this->repeatEmail){
            $this->owner->addError('email_address', 'Please make sure your email address is correct. Your login information will be sent there.');
        }
        
        // Membership
        if (app()->user->isGuest || !isset($input['Membership']['ausa_product_code'])) {
            // register user from the public site
            if (!$input['Membership']['chapter_id']) {
                $this->membership->addError('chapter_id', 'Please select a chapter.');
            } else {
                $this->membership->chapter_id = $input['Membership']['chapter_id'];
                $this->membership->member_id = $this->owner->member_id; // this one will not work before save
                $this->owner->source = 'registration';
                $this->setProductCode('IND');
                $this->setJoinDate(time());
                // TODO: might not work when updating a member
                $this->setExpirationDate(strtotime(Shared::toDatabase(time()) . " +12 month"));
                // default state is expired. We will extend this after the payment is made
                $this->membership->active = 0;
            }
        } else if (isset($input['Membership']['chapter_id']) && (app()->user->isAdmin() || in_array($input['Membership']['chapter_id'], app()->user->getUser()->getChapterIds()))) {
            // registration from logged in user
            $this->membership->chapter_id = $input['Membership']['chapter_id'];
            $this->membership->member_id = $this->owner->member_id; // this one will not work before save
            $this->setSubchapter($input['Membership']['subchapter_id']);
            $this->setProductCode($input['Membership']['ausa_product_code']);
            $this->setJoinDate($input['Membership']['join_date']);
            $this->setExpirationDate($input['Membership']['expiration_date']);
            $this->membership->active = $input['Membership']['active'];
            Shared::debug($this->membership->attributes);
        } else {
            $this->membership->addError('chapter_id', 'You don\'t have access to this chapter.');
        }
    }

    /**
     * Save both membership and member. It is useful when we create the objects.
     * 
     */
    public function saveMembership() {
        $connection = Yii::app()->db;
        $transaction = $connection->beginTransaction();
        if (!$this->owner->hasErrors() && !$this->membership->hasErrors() && $this->owner->save()) {
            $this->membership->member_id = $this->owner->member_id;
            $this->membership->setScenario('afterMemberCreate');
            if ($this->membership->save()) {
                $transaction->commit();
                return true;
            }
        }
        $transaction->rollback();
        return false;
    }

    /**
     * Make sure we have all the information before we send this out to
     * the payment gateway
     */
    public function validateMembership() {
        Shared::debug($this->owner->fullName . ' ' . $this->owner->first_name);
        if (!$this->owner->hasErrors() && !$this->membership->hasErrors() && $this->owner->validate()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Once the payment is recieved, the membership is activated. This function
     * is used only in a connection with registration form. Uploaded members
     * are activated automatically.
     *  - extends the lenght of membership for one year
     *  - sets activated to 1
     *  - create pin (if there is no pin)
     *  - sends activation / extension email
     *  - sends email to chapter about activation
     */
    public function activateMembership($registration = false) {
        Shared::debug("activate membership");
        $this->owner->activated = 1;
        $this->membership->active = 1;
        $activated = false;
        if ($registration){
            $this->membership->expiration_date = Shared::toDatabase(strtotime(Shared::toDatabase($this->membership->getNewStartDate()) . " +12 month"));
        }
        
        // new members don't have a pin
        if ($this->owner->access_pin == null) {
            Shared::debug("creating new pin for this sucka");
            $this->owner->createPin();
            $activated = true;
        }

        if ($this->saveMembership()) {
            Shared::debug("save membership");
            Shared::debug($this->owner->scenario, "scenario");
            if (!$activated){
                Shared::debug("not activated");
            }
            if ($registration && $activated) {
                Shared::debug("sending registration email");
                // send the email out only if the user didn't have PIN before
                $member = $this->owner;
                $mail = new AOEmail('memberRegistration');
                $mail->addPlaceholders(array(
                    'pin' => $member->access_pin,
                    'phone_number' => strlen($member->phone_number) > 4 ? CHtml::encode($member->phone_number) : 'N/A',
                    'email' => CHtml::encode($member->email_address),
                    'full_name' => CHtml::encode($member->getFullName()),
                    'join_date' => Shared::formatShortUSDate($this->membership->join_date),
                    'expiration_date' => Shared::formatShortUSDate($this->membership->expiration_date),
                ));
                $mail->addRecipient($member->email_address, CHtml::encode($member->getFullName()));
                //$mail->setTestRecipient('ondrej.nebesky@gmail.com');
                $mail->send();

                // send chapter email
                if (!$member->ausa_id){
                    Shared::debug("send chapter email");
                    $contact = $this->membership->chapter->getContactUser();
                    $chmail = new AOEmail('memberRegistrationChapter');
                    $chmail->addPlaceholders(array(
                        'prefix' => CHtml::encode($member->prefix),
                        'first_name' => CHtml::encode($member->first_name),
                        'last_name' => CHtml::encode($member->last_name),
                        'full_address' => CHtml::encode($member->getFullAddress()),
                        'city' => CHtml::encode($member->city),
                        'state' => CHtml::encode($member->state),
                        'zipcode' => CHtml::encode($member->zipcode),
                        'phone_number' => strlen($member->phone_number) > 4 ? CHtml::encode($member->phone_number) : 'N/A',
                        'email_address' => CHtml::encode($member->email_address),
                        'birthday' => Shared::formatShortUSDate($member->birthday),

                        'chapter_id'=>$this->membership->chapter->ausa_id,
                        'chapter_name'=>$this->membership->chapter->chapter_name,
                        'army_status' => CHtml::encode($member->army_status),
                        'military_unit' => CHtml::encode($member->military_unit),
                        'price_level' => CHtml::encode(MembershipPayment::$levels[$this->payment->level]['title']),
                        'price' => CHtml::encode("$".MembershipPayment::$levels[$this->payment->level]['price']),
                        'payment_reference_number' => "Coming Soon...",

                        'join_date' => Shared::formatShortUSDate($this->membership->join_date),
                        'expiration_date' => Shared::formatShortUSDate($this->membership->expiration_date),
                    ));
                    $chmail->addRecipient($contact->email_address, CHtml::encode($contact->getFullName()));

                    // temporary monitoring
                    $chmail->addRecipient('admin@ausaoffers.com', 'AUSA Offers Admin');
                    //$chmail->setTestRecipient('ondrej.nebesky@gmail.com');
                    $chmail->send();
                }
            } else if ($this->sendRegistrationEmail && $activated && strlen($this->owner->email_address)) {
                Shared::debug("sending added member email");
                // create / update form activation
                $member = $this->owner;
                $mail = new AOEmail('memberRegistration');
                $mail->addPlaceholders(array(
                    'pin' => $member->access_pin,
                    'phone_number' => strlen($member->phone_number) > 4 ? CHtml::encode($member->phone_number) : 'N/A',
                    'email' => CHtml::encode($member->email_address),
                    'full_name' => CHtml::encode($member->getFullName()),
                    'join_date' => Shared::formatShortUSDate($this->membership->join_date),
                    'expiration_date' => Shared::formatShortUSDate($this->membership->expiration_date),
                ));
                $mail->addRecipient($member->email_address, CHtml::encode($member->getFullName()));
            }
            return true;
        }
        return false;
    }

    /**
     * Preload default values for the form
     */
    public function loadDefaults() {
        $this->getTempMembership();
        if ($this->membership->active == null || $this->membership->isNewRecord)
            $this->membership->active = true;
        if ($this->membership->join_date == null) {
            $this->membership->join_date = date("Y-m-d", time());
        }
        if ($this->membership->expiration_date == null) {
            //$this->membership->expiration_date = date("Y-m-d", strtotime(date("Y-m-d", time()) . " +1 year"));
            // set correct expiration date after they pay
            $this->membership->expiration_date = $this->membership->join_date;
        }
    }

}

?>

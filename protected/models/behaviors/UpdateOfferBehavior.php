<?php

class UpdateOfferBehavior extends CBehavior {
    
    const EMAIL_REDEMPTION = '1';
    
    const PRINTED_REDEMPTION = '2';
    
    const QR_SCAN_REDEMPTION = '3';
    
    const SWIPE_REDEMPTION = '4';
    
    /**
     * Dummy variable used to render categories inside the form
     * @var type array
     */
    public $categoriesArr;
    
    public $campaign_duration;
    
    /**
     * Constructs default values before the model is displayed in the create form
     */
    public function loadDefaults(){
        $this->owner->start_date = date('Y-m-d', time());
        $this->owner->end_date = date('Y-m-d', strtotime($this->owner->start_date . " +1 month"));
        if (strlen($this->owner->redemption_code) == 0){
            $this->owner->redemption_code = Shared::generateRandomPassword(6);
        }
        
        $this->beforeFormLoad();
        //Shared::debug($this->owner->redemption_types);
    }
    
    public function beforeFormLoad(){
        if (!is_array($this->owner->participating_branches)){
            if (strlen($this->owner->participating_branches) == 0){
                $this->owner->participating_branches = array();
            }else{
                $this->owner->participating_branches = explode(',', $this->owner->participating_branches);
            }
        }
        
        if (!is_array($this->owner->redemption_types)){
            if (strlen($this->owner->redemption_types) == 0){
                if ($this->owner->isNewRecord){
                    $this->owner->redemption_types = array(self::SWIPE_REDEMPTION);
                }
            }else{
                $this->owner->redemption_types = explode(',', $this->owner->redemption_types);
            }
        }
        
        // preload categories to array
        $this->categoriesArr = array();
        //Shared::debug($this->owner->categories);
        foreach ($this->owner->categories as $category){
            $this->categoriesArr[] = $category->category_id;
        }
    }
    
    /**
     * not used
     * @return boolean
     */
    /*public function beforeValidate($on){
        if (is_array($this->owner->participating_branches)){
            $this->owner->participating_branches = implode(",", $this->owner->participating_branches);
        }
        
        if (is_array($this->owner->redemption_types)){
            $this->owner->redemption_types = implode(",", $this->owner->redemption_types);
        }
        
        $this->owner->updated_on = Shared::timeNow();
        $this->owner->vendor_id = app()->user->getActiveVendor();
        $this->owner->user_id = app()->user->id;
        
        // set the correct end date (based on month select)
        if (!$this->campaign_duration || $this->campaign_duration < 1 || $this->campaign_duration > 12){
            $this->campaign_duration = 1;
        }
        // create => just add number of months to it
        if ($this->owner->isNewRecord){
            $this->owner->end_date = '';
            $model->owner->end_date = Shared::toDatabase(strtotime($model->start_date . " +" . $duration . " month"));
        }
        
        return true;
    }*/
    
    public function setCategories($categories){
        $this->categoriesArr = $categories;
    }
    
    /**
     * Categories are stored in external table, so we need to handle
     * MANY to MANY data here.
     */
    public function saveCategories(){
        // published offers cannot change the category
        if ($this->owner->published){
            return true;
        }
        
        // load existing ones first
        $existing = $this->owner->categories;
        if (!is_array($existing)){
            $existing = array();
        }
        $_existing = array();
        foreach ($existing as $cat){
            Shared::debug($cat);
            $_existing[$cat->category_id] = true;
        }
        Shared::debug($_existing);
        
        // create the categories
        foreach ($this->categoriesArr as $cat){
            if (!isset($_existing[$cat])){
                Shared::debug("create new");
                $category = new OfferCategory;
                $category->offer_id = $this->owner->offer_id;
                $category->category_id = $cat;
                $category->save();
            }
        }
        
        // delete old ones
        $new = array_flip($this->categoriesArr);
        Shared::debug($new);
        foreach ($existing as $cat){
            if (!isset($new[$cat->category_id])){
                Shared::debug("delete old");
                $category = OfferCategory::model()->findByAttributes(array('offer_id' => $this->owner->offer_id, 'category_id' => $cat->category_id));
                $category->delete();
            }
        }
    }
    
    /**
     * This is basically a constructor of this behavior
     * @param CActiveRecord $owner
     * @throws Exception
     */
    public function attach($owner) {
                parent::attach($owner);
                $validators = $owner->getValidatorList();
                $validators->add(CValidator::createValidator('application.components.validators.AOCategoryValidator', $owner, 'categoriesArr'));
                $validators->add(CValidator::createValidator('required', $owner, 'categoriesArr'));

        }
}
?>

<?php

class ApiMemberBehavior extends CBehavior {
    
    /**
     * Find a user and authenticate. Returns user or null if
     * user is not found.
     * @param type $login
     * @param type $pin
     * @return false|Member returns member if the authentication is successful
     */
    public static function apiAuthenticate($login, $pin){
        // no email or phone problem (need to know ausa_id)
        $member = Member::findByWhatever($login, $login);
        if (is_object($member)){
            // lets see if the password matches
            if (strlen($pin) > 3 && $pin === $member->access_pin){
                return $member;
            }
        }
        return false;
    }
}
?>

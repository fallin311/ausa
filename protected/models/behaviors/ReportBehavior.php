<?php

class ReportBehavior extends CBehavior {
    
    /**
     * Get total number of impressions, clicks and redemptions by executing SQL query
     */
    public function getOfferSums(){
        $query = 'SELECT SUM(impressions) AS "impressions", SUM(clicks) AS "clicks", SUM(redemptions) AS "redemptions" FROM offer
WHERE vendor_id = :vendor_id;';
        $command = app()->db->createCommand($query);
        $command->bindValue(":vendor_id", $this->owner->vendor_id);
        Shared::debug($query, $this->owner->vendor_id);
        return $command->queryRow();
    }
}
?>

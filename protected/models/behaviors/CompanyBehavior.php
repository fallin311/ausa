<?php

/**
 * This behavior contains functions common for all the company type models
 * Vendor, SalesTeam and Chapter
 */
class CompanyBehavior extends CBehavior {

    /**
     * Get the first user created with the company. This guy is the one
     * we want to contact if something goes wrong or we feel lonely.
     * @return null
     */
    public function getContactUser() {
        $users = $this->owner->users;
        if (count($users)) {
            return $users[0];
        }
        return null;
    }

    public function getLogoUrl() {
        
    }

    /**
     * Returns BASE64 encoded logo displayed in login menu. The icon has 25x20 resolution
     */
    public function getLoginLogo($large = false) {
        $logo = null;
        if ($this->owner->logo) {
            $logo = Image::model()->findByAttributes(array('long_id' => $this->owner->logo));
            if ($logo != null) {
                if ($large){
                    return $logo->getBase64Src(Image::SIZE_PHONE_W, Image::SIZE_PHONE_H);
                }else{
                    return $logo->getBase64Src(Image::SIZE_ICON_W, Image::SIZE_ICON_H);
                }
                
            }
        }
        if ($logo == null) {
            // nothing found, lets display default one
            return Image::getDefaultBase64Icon();
        }
    }

    /**
     * Get either vendor, ST or chapter name
     * @return type
     */
    public function getCompanyName(){
        if (isset($this->owner->chapter_name)){
            return $this->owner->chapter_name;
        }
        if (isset($this->owner->st_name)){
            return $this->owner->st_name;
        }
        if (isset($this->owner->vendor_name)){
            return $this->owner->vendor_name;
        }
    }
}

?>

<?php

class ParamsBehavior extends CBehavior {
    
    public $parameters = null;
    public $modified = false;
    
    /**
     * Save the parametres only on change and when the parent is being saved
     */
    public function beforeSave($event){
        //Shared::debug("saving parameters");
        if ($this->modified){
            Shared::debug("modified");
            $this->saveParameters();
        }
        return parent::beforeSave($event);
    }
    
    public function getParameters() {
        if ($this->parameters == null) {
            if (strlen($this->owner->params)) {
                $this->parameters = unserialize(base64_decode($this->owner->params));
            } else {
                // crate empty array
                $this->parameters = array();
            }
        }
        //Shared::debug($this->parameters);
        return $this->parameters;
    }

    public function saveParameters() {
        // we have loaded the parameters
        if ($this->parameters != null) {
            // criteria is special case
            $this->owner->params = base64_encode(serialize($this->parameters));
            // Shared::debug($this->parameters, 'saved');
        }
    }

    public function setParameter($key, $value) {
        $this->getParameters();
        $this->parameters[$key] = $value;
        $this->modified = true;
    }

    public function getParameter($key) {
        $parameters = $this->getParameters();
        if (isset($parameters[$key]))
            return $parameters[$key];
        return null;
    }
    
    public function unsetParameter($key){
        $parameters = $this->getParameters();
        if (isset($parameters[$key])){
            unset($this->parameters[$key]);
        }
        $this->modified = true;
    }
}
?>
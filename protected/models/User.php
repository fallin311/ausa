<?php

/*
 * ALTER TABLE `ausa_dev`.`user` ADD COLUMN `mobile_username` INT NULL COMMENT 'Login ID to mobile username'  AFTER `password_reset` , ADD COLUMN `mobile_password` INT NULL  AFTER `mobile_username` ;
  ALTER TABLE `ausa_dev`.`user` ADD COLUMN `access_token` VARCHAR(63) NULL COMMENT 'mobile access token'  AFTER `mobile_password` ;

 */

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $user_id
 * @property string $email_address
 * @property integer $first_name
 * @property integer $last_name
 * @property string $address
 * @property string $phone_number
 * @property string $password
 * @property string $salt
 * @property integer $super_admin
 * @property string $params
 * @property string $last_login
 * @property boolean $email_verified
 * @property integer $password_reset
 *
 * The followings are the available model relations:
 * @property Chapter[] $chapters
 * @property SalesTeam[] $salesTeams
 * @property Vendor[] $vendors
 * @property VendorSalesman[] $vendorSalesmen
 */
class User extends AOActiveRecord {

    /**
     * When user object is cached in the WebUser, this field is used
     * to render the whole user menu without touching the database.
     * @var array 
     */
    public $userMenu;
    private $_vendors;
    private $_chapters;
    private $_sts;
    // user password changes
    public $pass1;
    public $pass2;
    public $old_password;
    public $verify;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('email_address', 'required'),
            array('password, pass2', 'required', 'on' => 'register'),
            array('first_name, last_name, pass1, pass2', 'required', 'on' => 'firstLogin'),
            array('old_password', 'application.components.validators.AOCurrentPassword', 'on' => 'changePassword'),
            array('pass1, pass2', 'required', 'on' => 'resetPass, changePassword'),
            array('pass2', 'compare', 'compareAttribute' => 'pass1', 'on' => 'firstLogin, resetPass, changePassword'),
            array('pass1, pass2', 'application.components.validators.EPasswordStrength', 'on' => 'firstLogin, resetPass, changePassword'),
            array('user_id, password_reset, super_admin, email_verified, mobile_username, mobile_password', 'numerical', 'integerOnly' => true),
            array('email_address, password, salt', 'length', 'max' => 63),
            array('address', 'length', 'max' => 511),
            array('phone_number', 'length', 'max' => 12),
            array('first_name, last_name', 'length', 'max' => 45),
            array('access_token', 'length', 'max' => 63),
            array('password_reset', 'numerical', 'on' => 'passwordReset', 'integerOnly' => true),
            array('password, pass1, pass2', 'application.components.validators.EPasswordStrength', 'on' => 'create'),
            array('pass2', 'compare', 'compareAttribute' => 'password', 'on' => 'register'),
            array('password, pass2', 'application.components.validators.EPasswordStrength', 'on' => 'register'),
            array('email_address', 'application.components.validators.AOEmailValidator', 'on' => 'update, create, register'),
            array('first_name, last_name', 'application.components.validators.AONameValidator'),
            array('email_address', 'unique'),
            array('email_address, phone_number', 'default', 'setOnEmpty' => true), // make empty values stored as NULL
            array('super_admin, login_disabled', 'application.components.validators.AOAdminStValidator'),
            array('params, last_login', 'safe'),
            array('verify', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements(), 'on' => 'register'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                //array('user_id, email_address, first_name, last_name, address, phone_number, password, salt, super_admin, params, last_login', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'chapters' => array(self::MANY_MANY, 'Chapter', 'user_chapter(user_id, chapter_id)'),
            'salesTeams' => array(self::MANY_MANY, 'SalesTeam', 'user_st(user_id, sales_team_id)'),
            // load only active stuff
            'vendors' => array(self::MANY_MANY, 'Vendor', 'user_vendor(user_id, vendor_id)', 'condition' => 'vendors_vendors.active=1 AND vendors.disabled!=1'),
            'vendorSalesmen' => array(self::HAS_MANY, 'VendorSalesman', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'user_id' => 'User',
            'email_address' => 'Email Address',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'address' => 'Address',
            'phone_number' => 'Phone Number',
            'password' => 'Password',
            'salt' => 'Salt',
            'super_admin' => 'Super Admin',
            'params' => 'Params',
            'last_login' => 'Last Login',
            'pass1' => 'New Password',
            'pass2' => 'Confirm Password',
            'verify' => 'Validate',
        );
    }

    /**
     * Load only active roles (role is not expired and company is not disabled)
     * TODO: verify active recrods only
     * TODO: this query will be super expensive
     * @obsolete moved to WebUser
     */
    /* public function getRoles($reload = false) {
      if (!$this->_vendors || $reload) {
      $this->getVendorIds($reload);
      }
      return $this->_vendors;
      } */

    /**
     * Returns an array of vendors this user can administer
     */
    public function getVendorIds($reload = false, $onlyVendors = false) {
        if (!isset($this->_vendors) || $reload) {
            $db = Yii::app()->db;

            // TODO: consider SalesTeam object
            $command = $db->createCommand("SELECT v.vendor_id FROM user_vendor u
INNER JOIN vendor v ON u.vendor_id = v.vendor_id
WHERE u.user_id = {$this->user_id} AND v.disabled = 0 AND u.active = 1");


            $result = $command->queryAll(false);
            $this->_vendors = array();
            foreach ($result as $row) {
                $this->_vendors[] = $row[0];
            }
            //Shared::debug($this->_vendors);
            if (!$onlyVendors) {
                // is it sales team user?
                //Shared::debug("NOT onlyVendors");
                $stIds = $this->getStIds();
                if (count($stIds)) {

                    $command = $db->createCommand("SELECT v.vendor_id FROM st_vendor sv
INNER JOIN vendor v ON sv.vendor_id = v.vendor_id
WHERE sv.sales_team_id IN (" . implode(",", $stIds) . ") AND v.disabled = 0 AND sv.active = 1;");
                    $result = $command->queryAll(false);
                    foreach ($result as $row) {
                        $this->_vendors[] = $row[0];
                    }
                }
            }
            //Shared::debug($this->_vendors);
        }
        //Shared::debug($this->_vendors);
        return $this->_vendors;
    }

    /**
     * Get an array of chapters this user has access to through vendors.
     * For example this user can control 5 vendors in two chapters and this will
     * return infor from those two chapters.
     * Assoc array contains chapter id, chapter name and city
     * @return type
     */
    public function getAccessibleChapterIds($user = null) {
        if ($user != null && $user->isAdmin()) {
            // admin can see all chapters and vendors
            $query = "SELECT chapter_id, chapter_name, city FROM chapter WHERE disabled = 0";
        } else {
            $vendors = $this->getVendorIds();
            $query = "SELECT DISTINCT c.chapter_id, chapter_name, c.city FROM chapter c
                RIGHT JOIN vendor_membership v ON c.chapter_id = v.chapter_id
                WHERE c.disabled = 0 AND v.active = 1 AND v.vendor_id IN(" . implode(",", $vendors) . ")";
        }
        // get all vendors (for white testing
        /* $query = "SELECT DISTINCT c.chapter_id, chapter_name, c.city FROM chapter c
          RIGHT JOIN vendor_membership v ON c.chapter_id = v.chapter_id
          WHERE c.disabled = 0 AND v.active = 1"; */
        Shared::debug($query);
        $command = app()->db->createCommand($query);

        // we need to force chapter_id to be an integer for javascript
        $result = $command->queryAll();
        foreach ($result as &$row) {
            $row['chapter_id'] = $row['chapter_id'] + 0;
        }
        return $result;
    }

    /**
     * Get ids of chapters user is member of. It should be faster this way
     * @param type $reload
     * @return type
     */
    public function getChapterIds($reload = false) {
        if (!isset($this->_chapters) || $reload) {
            $db = Yii::app()->db;

            // TODO: consider SalesTeam object
            $command = $db->createCommand("SELECT c.chapter_id FROM user_chapter u
INNER JOIN chapter c ON u.chapter_id = c.chapter_id
WHERE u.user_id = {$this->user_id} AND c.disabled = 0 AND u.active = 1");

            $result = $command->queryAll(false);
            $this->_chapters = array();
            foreach ($result as $row) {
                $this->_chapters[] = $row[0];
            }
            //Shared::debug($this->_chapters);
        }
        return $this->_chapters;
    }

    /**
     * Get ids of chapters user is member of. It should be faster this way
     * @param type $reload
     * @return type
     */
    public function getStIds($reload = false) {
        if (!isset($this->_sts) || $reload) {
            $db = Yii::app()->db;

            // TODO: consider SalesTeam object
            $command = $db->createCommand("SELECT s.sales_team_id FROM user_st u
INNER JOIN sales_team s ON u.sales_team_id = s.sales_team_id
WHERE u.user_id = {$this->user_id} AND s.disabled = 0 AND u.active = 1");

            $result = $command->queryAll(false);
            $this->_sts = array();
            foreach ($result as $row) {
                $this->_sts[] = $row[0];
            }
            //Shared::debug($this->_sts);
        }
        return $this->_sts;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    /* public function search()
      {
      // Warning: Please modify the following code to remove attributes that
      // should not be searched.

      $criteria=new CDbCriteria;

      $criteria->compare('user_id',$this->user_id,true);
      $criteria->compare('email_address',$this->email_address,true);
      $criteria->compare('first_name',$this->first_name);
      $criteria->compare('last_name',$this->last_name);
      $criteria->compare('address',$this->address,true);
      $criteria->compare('phone_number',$this->phone_number,true);
      $criteria->compare('password',$this->password,true);
      $criteria->compare('salt',$this->salt,true);
      $criteria->compare('super_admin',$this->super_admin);
      $criteria->compare('params',$this->params,true);
      $criteria->compare('last_login',$this->last_login,true);

      return new CActiveDataProvider($this, array(
      'criteria'=>$criteria,
      ));
      } */

    public function isAdmin() {
        return $this->super_admin;
    }

    /**
     * Does this person belong to sales team
     * @return boolean
     */
    public function isSalesman() {
        if (count($this->getStIds()))
            return true;
        return false;
    }

    public function isChapterUser() {
        if (count($this->getChapterIds()))
            return true;
        return false;
    }

    public function isVendor() {
        if (count($this->getVendorIds()))
            return true;
        return false;
    }

    public static function findByEmail($email) {
        return self::model()->findByAttributes(array('email_address' => $email));
    }

    /**
     * Create new user object, do not save it to database yet
     * all permission should be valid now
     * @param type $input
     */
    public static function create($input) {

        if (isset($input['email_address'])) {
            $model = self::findByEmail($input['email_address']);
        } else {
            $model = new User;
        }
        $password = isset($input['password']) ? $input['password'] : '';
        if (null == $model) {
            $model = new User;
            $model->attributes = $input;

            // create new one if nothing is provided
            if (strlen($model->password) == 0) {
                $password = Shared::generateMnemonicPassword(8);
            }
            $model->salt = md5(rand());
            $model->password = sha1($model->salt . $password);
            $model->email_address = strtolower($model->email_address);
            // we should probably email the person their account info... no way to know the password - t.s.
        }
        return $model;
    }

    public function getFullName() {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getFullNameCompany() {
        $sts = $this->salesTeams;
        return $this->first_name . ' ' . $this->last_name . ' (' . $sts[0]->st_name . ')';
    }

    /**
     * Function called before update. 
     * @param type $password
     */
    public function setPassword($password) {
        if (strlen($password) > 0) {
            // change password
            $this->salt = md5(rand());
            $this->password = sha1($this->salt . $password);
        } else {
            // keep the original one
            $this->restoreAttribute('password');
        }
    }

    /**
     * No matter what active id is set, always get the role this user started with.
     * @return String
     */
    public function getDefaultRole() {
        if ($this->isAdmin()) {
            return 'admin';
        } else if ($this->isChapterUser()) {
            return 'chapter';
        } else if ($this->isSalesman()) {
            return 'salesman';
        } else if ($this->isVendor()) {
            return 'vendor';
        }
        return 'guest';
    }

    /**
     * Information displayed in the main menu ... such as icon
     * This function is data intensive, so make sure you cache it
     */
    public function getUserMenu() {

        if ($this->userMenu == null) {
            //Shared::debug("load user menu");
            $menu = array('url' => '#', 'items' => array());
            $role = app()->user->getActiveRole();
            $logo = null;

            if (isset($role['class'])) {
                $company = $role['class']::model()->findByPk($role['id']);
                if ($company != null) {
                    //Shared::debug("Got company");
                    $logo = $company->getLoginLogo();
                }
            }

            if ($logo == null) {
                // admin, or company does not have one
                $logo = Image::getDefaultBase64Icon();
            }
            $info = '<i class="icon-admin" style="background-image: url(&quot;' . preg_replace('/^\s+|\n|\r|\s+$/m', '', $logo) . '&quot;)"></i> ';
            $info .= $this->email_address;
            // just remember to reset the state when the role changes
            //app()->user->setState('menuInfo', $info);
            //}
            $menu['label'] = $info;

            // for admins, st and chapters we have active role
            /* if ($this->isAdmin() || $this->isSalesman() || $this->isChapterUser()) {
              $isHigherUser = true;
              } else {
              $isHigherUser = false;
              } */
            $default = $this->getDefaultRole();

            // st or admins have also default role
            if ($default == 'admin') {
                $logo = Image::getDefaultBase64IconLarge();
                $companyName = 'System Administrator';
                $homeUrl = app()->user->getHomeUrl();
            } else {

                if ($default == 'salesman') {
                    $ids = $this->getStIds();
                    $company = SalesTeam::model()->findByPk($ids[0]);
                    $homeUrl = url('/salesTeam/dashboard');
                } elseif ($default == 'chapter') {
                    $ids = $this->getChapterIds();
                    $company = Chapter::model()->findByPk($ids[0]);
                    $homeUrl = url('/chapter/dashboard');
                } elseif ($default == 'vendor') {
                    $ids = $this->getVendorIds();
                    $company = Vendor::model()->findByPk($ids[0]);
                    $homeUrl = url('/vendor/dashboard');
                }
                $logo = $company->getLoginLogo(true);
                $companyName = $company->getCompanyName();
            }
            $companyName = str_replace(" ", "&nbsp;", $companyName);

            $hasLastVendors = Yii::app()->user->getState('lastVendors');
            $menu['items'][] = array('label' => '<div class="ausa-userInfoDrop">'
                . '<div class="ausa-userAvatar">'
                . '<a style="padding: 0px;" href="' . $homeUrl . '"><img width="80" height="64" src="' . preg_replace('/^\s+|\n|\r|\s+$/m', '', $logo) . '" alt="" /></a>'
                . '</div>'
                . '<div class="ausa-userInfoDropData">'
                . '<h4 class="ausa-userInfoDropDataName"><a style="padding: 0px;" href="' . $homeUrl . '">' . $companyName . '</a></h4>'
                . '<button class="btn btn-primary btn-small pull-left" onClick="window.location.href=\'' . url('user/update') . '/' . app()->user->id . '\'"><i class="icon-cogs icon-white"></i> My Account</button>'
                . '<button class="btn btn-small pull-left" style="margin-left:10px;" onClick="window.location.href=\'' . url('user/logout') . '\'"><i class="icon-off"></i> Sign Out</button>'
                . '</div>'
                . ((count($hasLastVendors) && ($this->getDefaultRole() != 'vendor')) ? '<ul><li class="divider"></li></ul>' : '')
                . '</div>');
            //Shared::debug("submenu loaded");
            // for st and admins there is also a list of vendors. The list is stored in a session
            if ($this->isAdmin() || $this->isSalesman()) {
                $lastVendors = Yii::app()->user->getState('lastVendors');
                if (count($lastVendors)) {
                    $lastVendors = $lastVendors; // Most recent vendor at the top of the list
                    $menu['items'][] = array('label' => '<span>Recently used vendors:</span>');
                    $activeVendor = 0;
                    if ($role['type'] == 'vendor') {
                        $activeVendor = $role['id'];
                    }
                    foreach ($lastVendors as $vendor) {
                        if ($vendor['id'] == $activeVendor) {
                            $active = true;
                        } else {
                            $active = false;
                        }
                        $menu['items'][] = array(
                            'label' => '<i class="icon-admin" style="background-image: url(&quot;' . preg_replace('/^\s+|\n|\r|\s+$/m', '', $vendor['icon']) . '&quot;)"></i> ' . ($active ? "<b>" . $vendor['name'] . "</b>" : $vendor['name']),
                            'url' => url('/vendor/go/' . $vendor['id']),
                        );
                    }
                }
            }
            //$menu['items'][] = array('label' => 'Account Settings', 'url' => url('/user/update').'/'.app()->user->user->id);
            //$menu['items'][] = array('label' => 'Sign Out', 'url' => array('/user/logout'));

            $this->userMenu = $menu;
        }
        return $this->userMenu;
    }

    /**
     * Cache generic user object used from WebUser. We want to cache the menus and
     * roles.
     * @param type $pk
     */
    public static function findInCache($pk) {
        $user = app()->cache->get('user-' . $pk);
        if ($user === false) {
            $user = self::model()->findByPk($pk);
            if ($user == null) {
                return null;
            }

            // set user early to web user object, so role permission checking
            // works during menu registration process. This value will be overwritten
            // once the $user variable is returned
            app()->user->_user = $user;

            // preload
            $user->getUserMenu();

            // and save
            $user->saveToCache();

            return $user;
        } else {
            // found, but we still have to construct the object
            $model = new User;
            $model->loadFromCache($user);
            $model->user_id = $pk; // PK is not part of attributes
            return $model;
        }
    }

    /**
     * Interface setter, which will grab the values saved in cache and load
     * it to this object
     */
    public function loadFromCache($attributes) {
        if (isset($attributes['reset_password'])) {
            unset($attributes['reset_password']);
        }

        $this->attributes = $attributes['attributes'];
        $this->userMenu = $attributes['userMenu'];
        $this->_vendors = $attributes['vendors'];
        $this->_chapters = $attributes['chapters'];
        $this->_sts = $attributes['sts'];
    }

    /**
     * Save User menu and attributes to cache as an associative array
     */
    public function saveToCache() {
        $attributes = array(
            'attributes' => $this->attributes,
            'userMenu' => $this->userMenu,
            'vendors' => $this->_vendors,
            'chapters' => $this->_chapters,
            'sts' => $this->_sts
        );
        app()->cache->set('user-' . $this->id, $attributes, 1200);
    }

    public function afterSave() {
        parent::afterSave();
        $this->saveToCache();
        return true;
    }

    /**
     * Send email verification email to user. Email address is changed by
     * clicking on email verification field.
     */
    public function beforeSave() {
        // && !app()->user->isAdmin()
        if ($this->scenario == 'update') {
            $orig = $this->getOriginalAttribute('email_address');

            if ($orig != $this->email_address) {
                Shared::debug("it has changed - save it");
                app()->user->setFlash('emailChange', 'This email is different from our stored email address. Please check your email and confirm the change.');
                // set the value back as it was, and create activation link
                Yii::import('application.models.behaviors.ParamsBehavior');
                $this->attachBehavior('paramsBehavior', new ParamsBehavior());
                $this->setParameter('changeEmail', $this->email_address);

                // enforce params saving
                $this->saveParameters();

                $mail = new AOEmail('emailVerification');
                $mail->addRecipient($this->email_address, $this->getFullName());
                $mail->addPlaceholders(array('verification_url' => absUrl('/user/verify/' . $this->user_id . '?v=' . $this->salt)));
                $mail->send();

                $this->email_address = $orig;
            }
        }

        return true;
    }

    /**
     * Creates mobile access username and pin plus access token
     * @return string access token
     */
    public function getAccessToken() {
        $update = false;
        if (!$this->access_token) {
            $this->access_token = Shared::generateRandomPassword(32);
            $update = true;
        }
        if (!$this->mobile_username) {
            $this->mobile_username = Shared::generateCode(5);
            $this->mobile_password = Shared::generateCode(5);
            $update = true;
        }
        if ($update) {
            // user might be loaded from cache and it would be presented as a new record
            $user = User::model()->findByPk($this->user_id);
            $user->mobile_username = $this->mobile_username;
            $user->mobile_password = $this->mobile_password;
            $user->access_token = $this->access_token;

            $user->save();
        }
        return $this->access_token;
    }

    /**
     * Returns multi-dimensional member array used in json api
     * @param $fullInfo Return information used after login including api key
     */
    public function getInfoForApi($chapter) {
        $member = array(
            'id' => $this->user_id,
            'ausaId' => 42,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'prefix' => '',
            'emailAddress' => $this->email_address,
            // since member cannot update it, displaye it formatted
            'phoneNumber' => Shared::formatPhone($this->phone_number),
            'joinDate' => null,
            'demoUser' => 1,
            'vendorUser' => 1,
            'authToken' => $this->getAccessToken(),
            'chapter' => array(
                'id' => $chapter->chapter_id,
                'name' => $chapter->chapter_name,
                'city' => $chapter->city,
            )
        );
        return $member;
    }

    /**
     * Renders the users dashboard links based on their currently active role.
     * @return array
     */
    public function getUserDashboard() {
        $active = app()->user->getActiveRole();
        $array = array();
        if (!app()->user->isGuest()) {
            $array = array(
                'items' => array(
                    'vendors' => array('icon' => 'dashboard-vendors', 'text' => 'Vendors', 'url' => url('vendor/index'), 'visible' => ($active['action'] == 'vendor' ? 0 : 1)),
                    'chapters' => array('icon' => 'dashboard-vendors', 'text' => 'Chapters', 'url' => url('chapter'), 'visible' => app()->user->isAdmin()),
                    'members' => array('icon' => 'dashboard-members', 'text' => 'Members', 'url' => url($active['action'] . '/members'), 'visible' => ($active['action'] == 'chapter' ? 1 : 0)),
                    'branches' => array('icon' => 'dashboard-branches', 'text' => 'View<br/>Branches', 'url' => url('vendor/branches'), 'visible' => ($active['action'] == 'vendor' ? 1 : 0)),
                    'offers' => array('icon' => 'dashboard-offers', 'text' => 'Vendors Offers', 'url' => url('/offer'), 'visible' => ($active['action'] == 'vendor' ? 1 : 0)),
                    'banners' => array('icon' => 'dashboard-banners', 'text' => 'View<br/>Banners', 'url' => url('banner/index'), 'visible' => ($active['action'] == 'vendor' ? (Vendor::model()->findByPk($active['id'])->max_banners > 0 ? 1 : 0) : 0)),
                    'mobile' => array('icon' => 'dashboard-mobile mobile-preview', 'text' => 'Mobile Preview', 'url' => url('user/mobile'), 'visible' => ($active['action'] == 'vendor' ? 1 : 0)),
                    'settings' => array('icon' => 'dashboard-settings', 'text' => 'Settings', 'url' => url($active['action'] . '/update/' . $active['id']), 'visible' => ($active['action'] == 'chapter' ? 1 : ($active['action'] == 'salesTeam' ? 1 : ($active['action'] == 'vendor' ? 1 : 0)))),
                    'subscription' => array('icon' => 'dashboard-settings', 'text' => 'My<br />Subscription', 'url' => url('subscription/index/' . $active['id']), 'visible' => ($active['action'] == 'vendor' ? 1 : 0)),
                    'support' => array('icon' => 'dashboard-help', 'text' => 'Support<br />Information', 'url' => url('vendor/support'), 'visible' => ($active['action'] == 'vendor' ? 1 : 0)),
                ), 'multiCol' => 0);
        }
        return $array;
    }

}

<?php

/**
 * This is the model class for table "svn_update".
 * It performs updates using SVN and stores revision information into database
 *
 * The followings are the available columns in table 'svn_update':
 * @property string $svn_update_id
 * @property string $updated_on
 * @property string $user
 * @property integer $revision
 * @property string $note
 */
class SvnUpdate extends CActiveRecord {

    // svn executable file
    private $svnBin = 'svn';
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SvnUpdate the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function afterConstruct() {
        parent::afterConstruct();
        if (AO_URI == '/ausa/'){
            // windows
            $this->svnBin = '"C:\\Program Files\\TortoiseSVN\\bin\\svn.exe"';
        } else {
            // linux
            $this->svnBin = 'svn';
        }
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'svn_update';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('updated_on', 'required'),
            array('revision', 'numerical', 'integerOnly' => true),
            array('user', 'length', 'max' => 63),
            array('note, output, updated_file', 'safe'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'svn_update_id' => 'Svn Update',
            'updated_on' => 'Date',
            'user' => 'User',
            'updated_file', 'Updated File',
            'revision' => 'Revision',
            'note' => 'Note',
        );
    }

    /**
     * Run SVN command to update root or selected files.
     * @param type $note
     * @param type $revision
     * @param type $file
     */
    public function runUpdate($note, $revision = null, $file = null) {
        $root = dirname(Yii::getPathOfAlias('application'));
        if ($file != null){
            if (file_exists($root . '/' . $file)){
                $root .= '/' . $file;
            }
        }

        $params = '';
        if ($revision != null){
            $params .= ' -r' . $revision;
        }

        $output = `$this->svnBin update$params $root 2>&1`;
        //Shared::debug($output);
        $revision = $this->getRevisionFromUpdate($output);
        //Shared::debug($revision);
        $latest = self::getLastRevisionNumber();
        
        // insert the record only if there is something new
        if ($revision != $latest){
            $user = 'unknown';
            if (is_object(app()->user->getUser())){
                $user = app()->user->getUser()->getFullName();
            }
            $this->note = $note;
            $this->revision = $revision;
            $this->output = $output;
            $this->user = $user;
            $this->updated_on = Shared::timeNow();
            $this->save();
            return true;
        }
        return false;
    }
    
    public static function getLastRevisionNumber(){
        $query = "SELECT MAX(revision) FROM svn_update";
        return app()->db->createCommand($query)->queryScalar();
    }
    
    public function getHeadRepoStatus(){
        $root = dirname(Yii::getPathOfAlias('application'));
        $output = `$this->svnBin info $root 2>&1`;
        return $output;
    }
    
    /**
     * Reads output returned by svn command and returns revision version
     */
    public function getRevisionFromUpdate($input){
        $matches = array();
        preg_match('/ revision ([\d]+)./', $input, $matches);
        //Shared::debug($matches);
        if (isset($matches[1])){
            return $matches[1];
        }
        return 0;
    }

}
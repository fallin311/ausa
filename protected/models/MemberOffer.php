<?php

/**
 * This is the model class for table "member_offer".
 *
 * The followings are the available columns in table 'member_offer':
 * @property string $member_offer_id
 * @property string $offer_id
 * @property string $member_id
 * @property string $vendor_id
 * @property integer $favorite
 *
 * The followings are the available model relations:
 * @property OfferClick[] $offerClicks
 * @property Offer $offer
 * @property Vendor $vendor
 * @property Member $member
 * @property Redemption[] $redemptions
 */
class MemberOffer extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MemberOffer the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'member_offer';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('offer_id, member_id, vendor_id', 'required'),
            array('favorite', 'numerical', 'integerOnly' => true),
            array('offer_id, member_id, vendor_id', 'length', 'max' => 10),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('member_offer_id, offer_id, member_id, vendor_id, favorite', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'clicks' => array(self::HAS_MANY, 'OfferClick', 'member_offer_id'),
            'offer' => array(self::BELONGS_TO, 'Offer', 'offer_id'),
            'vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),
            'member' => array(self::BELONGS_TO, 'Member', 'member_id'),
            'redemptions' => array(self::HAS_MANY, 'Redemption', 'member_offer_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'member_offer_id' => 'Member Offer',
            'offer_id' => 'Offer',
            'member_id' => 'Member',
            'vendor_id' => 'Vendor',
            'favorite' => 'Favorite',
        );
    }

    /**
     * Find all recent offers belonging to this member
     * only active offers this user already clicked / faved, or redeemed
     * @param type $member
     */
    public static function getMemberOffers($member){
       return self::model()->with(array(
           'clicks' => array('together' => true), 'redemptions' => array('together' => true), 'offer'))->findAll(array(
            'condition' => 't.member_id=' . $member->member_id . ' AND offer.published=1 AND offer.end_date > NOW() AND offer.start_date < NOW()'));
    }
}
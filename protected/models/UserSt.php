<?php

/**
 * This is the model class for table "user_st".
 *
 * The followings are the available columns in table 'user_st':
 * @property string $sales_team_id
 * @property string $user_id
 * @property string $since
 * @property integer $active
 */
class UserSt extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserSt the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_st';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('sales_team_id, user_id', 'required'),
            array('active', 'numerical', 'integerOnly' => true),
            array('sales_team_id, user_id', 'length', 'max' => 10),
            array('since', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('sales_team_id, user_id, since, active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'salesTeam' => array(self::BELONGS_TO, 'SalesTeam', 'sales_team_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'sales_team_id' => 'Sales Team',
            'user_id' => 'User',
            'since' => 'Since',
            'active' => 'Active',
        );
    }

    public static function findByUser($user, $st) {
        return self::model()->findByAttributes(array('user_id' => $user->user_id, 'sales_team_id' => $st->sales_team_id));
    }

    public static function create($user, $st) {
        if (is_numeric($user))
            $user = User::model()->findByPk($user);
        if (is_numeric($st))
            $st = SalesTeam::model()->findByPk($st);
        if ($user != null && $st != null) {
            $userSt = self::findByUser($user, $st);
            if ($userSt == null) {
                $userSt = new UserSt;
                $userSt->user_id = $user->user_id;
                $userSt->sales_team_id = $st->sales_team_id;
                $userSt->since = Shared::timeNow();
                $userSt->active = 1;
            }
            return $userSt;
        }
        return null;
    }

    /**
     * Search criteria to get all the users from the current ST
     * @return \User
     */
    public function fromActiveSt() {
        if (app()->user->getActiveSt()) {
            $this->getDbCriteria()->mergeWith(array(
                'with' => 'user',
                'condition' => 'login_disabled = 0 AND sales_team_id=' . app()->user->getActiveSt()
            ));
        }
        return $this;
    }

    /**
     * Search criteria to get all the users from selected ST
     * @return \User
     */
    public function fromSt($stId) {
        $this->getDbCriteria()->mergeWith(array(
            'with' => 'user',
            'order' => 'last_name'
        ));
        if (is_array($stId)) {
            if (count($stId)){
            $this->getDbCriteria()->mergeWith(array(
                'condition' => 'login_disabled = 0 AND sales_team_id IN(' . implode(",", $stId) . ')',
            ));
            }
        } else {
            $this->getDbCriteria()->mergeWith(array(
                'condition' => 'login_disabled = 0 AND sales_team_id=' . $stId,
            ));
        }


        return $this;
    }

}
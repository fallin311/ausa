<?php

/**
 * This is the model class for table "user_chapter".
 *
 * The followings are the available columns in table 'user_chapter':
 * @property string $chapter_id
 * @property string $user_id
 * @property string $since
 * @property integer $active
 */
class UserChapter extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserChapter the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_chapter';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('chapter_id, user_id', 'required'),
            array('active', 'numerical', 'integerOnly' => true),
            array('chapter_id, user_id', 'length', 'max' => 10),
            array('since', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('chapter_id, user_id, since, active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'chapter_id' => 'Chapter',
            'user_id' => 'User',
            'since' => 'Since',
            'active' => 'Active',
        );
    }
    
    public static function findByUser($user, $chapter){
        return self::model()->findByAttributes(array('user_id' => $user->user_id, 'chapter_id' => $chapter->chapter_id));
    }
    
    public static function create($user, $chapter){
        if (is_numeric($user)) $user = User::model()->findByPk($user);
        if (is_numeric($chapter)) $chapter = Chapter::model()->findByPk($chapter);
        if ($user != null && $chapter != null){
            $userChapter = self::findByUser($user, $chapter);
            if ($userChapter == null){
                $userChapter = new UserChapter;
                $userChapter->user_id = $user->user_id;
                $userChapter->chapter_id = $chapter->chapter_id;
                $userChapter->since = Shared::timeNow();
                $userChapter->active = 1;
            }
            return $userChapter;
        }
        return null;
    }

}
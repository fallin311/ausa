<?php

/**
 * This is the model class for table "cron_job".
 *
 * The followings are the available columns in table 'cron_job':
 * @property string $cron_job_id
 * @property string $date_created
 * @property string $last_update
 * @property integer $data_size
 * @property integer $progress
 * @property string $params
 * @property integer $initiated_by_user
 * @property string $results
 */
class CronJob extends CActiveRecord {

    // rather variable than const, so we can change it for testing purposes
    public static $chunkSize = 2000;

    /**
     * Unserialized params
     * @var type 
     */
    public $data = array();

    /**
     * Returns the static model of the specified AR class.
     * @return CronJob the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cron_job';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('date_created', 'required'),
            array('data_size, progress, initiated_by_user', 'numerical', 'integerOnly' => true),
            array('last_update, params', 'safe'),
            array('results', 'length', 'max' => 255),
        );
    }

    /**
     * Process one single job
     */
    public function process() {
        $this->data = unserialize(stripslashes($this->params));

        // used to load lat/lon from member address
        Yii::import('application.helpers.AddressHelper');
        
        // data upload
        $results = $this->uploadMembers();
        $this->progress += self::$chunkSize;
        $stored = unserialize(stripslashes($this->results));
        if (!is_array($stored)) {
            $stored = array('total' => 0, 'new' => 0, 'updated' => 0, 'invalid' => 0);
        }
        $stored['total'] += $results['total'];
        $stored['updated'] += $results['updated'];
        $stored['new'] += $results['new'];
        $stored['invalid'] += $results['invalid'];
        $this->results = addslashes(serialize($stored));

        //Shared::debug($stored);

        $this->save();

        // save new data ... what is it?
        if (isset($this->data['users'])) {
            $this->data['users'] = array_slice($this->data['users'], self::$chunkSize);
            $this->saveData();
            $this->save();
            //Shared::debug("saved");
        }

        if ($this->progress >= $this->data_size) {
            // job complete
            if (isset($stored)) {
                // it was file upload and it is done now. Lets notify the uploader
                $email = new AOEmail('csvUploadComplete');
                $email->addPlaceholders($stored);
                $this->data['path'] = str_replace('\\', '/', $this->data['path']);
                $email->addPlaceholders(array('file_name' => substr($this->data['path'], strpos($this->data['path'], '/'))));
                $user = User::model()->findByPk($this->initiated_by_user);
                $email->addPlaceholders(array('first_name' => $user->first_name, 'last_name' => $user->last_name));
                //$email->addRecipient('stroud.travis@gmail.com', CHtml::encode($user->getFullName()));
                $email->addRecipient($user->email_address, CHtml::encode($user->getFullName()));
                $email->send();
            }
            $this->delete();
            // we might want to delete the file as well, but it is not going to work for testing
            //$filePath = $this->data['path'];
            //unlink($filePath);
        }
        return $results;
    }

    /**
     * Call only when the object is created. serialize big arrays is an expensive operation
     */
    public function saveData() {
        if (is_array($this->data)) {
            $this->params = addslashes(serialize($this->data));
        }
    }

    /**
     * Create the job and save it to the database
     * @param type $fileInfo
     * @param type $filePath
     * @return \CronJob
     */
    public static function createUploadJob($fileInfo, $filePath) {
        $job = new CronJob;
        $job->date_created = Shared::toDatabase(time());
        $job->last_update = Shared::toDatabase(time());
        $job->initiated_by_user = app()->user->id;
        if (isset($fileInfo['linecount'])) {
            // we really need to know how big the file is
            $job->data_size = $fileInfo['linecount'];
            $job->data['info'] = $fileInfo;
            $job->data['path'] = $filePath;
            $job->saveData();
            $job->save();
        }
        return $job;
    }

    public function uploadMembers() {
        $fileInfo = $this->data['info'];
        $filePath = $this->data['path'];

        // chapter objects used during upload
        $chapters = array();

        if (!is_file($filePath)) {
            $this->delete();
            Shared::debug("File $filePath not found.");
            return false;
        }

        srand((double) microtime() * 1000000);
        $result = array(
            'total' => 0,
            'new' => 0,
            'updated' => 0,
            'invalid' => 0,
        );

        if (($handle = fopen($filePath, "r")) !== false) {
            // header
            $data = fgetcsv($handle, 10000);

            // skip to the lines we need to process
            for ($i = 0; $i < $this->progress; $i++) {
                fgetcsv($handle, 10000);
            }

            $uploader = new MemberUpload($fileInfo['headers']);

            // read the real data
            while (($data = fgetcsv($handle, 10000, $fileInfo['separator'])) !== false) {
                $result['total']++;
                $num = count($data);

                // check the column number
                if ($num != count($fileInfo['headers'])) {
                    // something changed here
                    $this->delete();
                    return false;
                }

                // create model first and set all the values from upload to it
                $model = new Member('upload');

                $success = $uploader->setValuesFromUpload($model, $data);

                $model->updated_on = Shared::timeNow();
                $model->activated = 1;

                $updated = false;
                if ($success && $model->validate()) {
                    // find it in the database
                    $member = Member::findByWhatever($model->email_address, $model->phone_number, $model->ausa_id);
                    if (is_object($member)) {
                        // update
                        // some attributes are calculated on save when address is changed
                        // last update is set only if there is a change
                        $attributes = $model->attributes;
                        unset($attributes['source']);
                        unset($attributes['updated_on']);
                        unset($attributes['lat']);
                        unset($attributes['lon']);
                        unset($attributes['access_pin']);//access pin should not change and the excel export does not have this column.
                        unset($attributes['ausa_id']);//also should not change.. 
                        $member->setAttributes($attributes);

                        Shared::debug($member->getDirtyAttributes());
                        if ($member->isDirty()) {
                            Shared::debug("member {$model->email_address} is dirty");
                            Shared::debug($member->getDirtyAttributes());
                            $member->updated_on = Shared::timeNow();
                            $updated = true;
                        }
                        $member->save(); // saving member through AOActiveRecord might not run the query if the object stays the same
                        $member->attachBehavior('uploadMember', new UploadMemberBehavior());

                        $model->setAttributes($member->attributes);
                        $model->isNewRecord = false;
                        $model->member_id = $member->member_id;
                    } else {
                        // create new one
                        $model->source = 'ausa';
                        $model->access_pin = Shared::generateCode(5);
                        // send the new member an email with their access pin
                        $email = new AOEmail('memberRegistration');
                        $email->addRecipient($model->email_address, CHtml::encode($model->getFullName()));
                        $email->addPlaceholders(array(
                            'pin' => $model->access_pin,
                            'phone_number' => strlen($model->phone_number) > 4 ? CHtml::encode($model->phone_number) : 'N/A',
                            'email' => CHtml::encode($model->email_address),
                            'full_name' => CHtml::encode($model->getFullName()),
                        ));
                        $email->send();
                        // save member to db
                        $model->save();
                        $result['new']++;
                    }

                    // member should be saved, lets handle membership
                    $tempMembership = $model->getTempMembership();
                    $chapterCode = $model->getChapterCode();

                    // find the chapter first
                    if (!isset($chapters[$chapterCode])) {
                        // we might not have the chapter loaded yet, lets do it
                        $chapter = Chapter::model()->findByAttributes(array('ausa_id' => $model->getChapterCode()));
                        // it might be null, but it is ok
                        $chapters[$chapterCode] = $chapter;
                    }
                    if (is_object($chapters[$chapterCode])) {
                        // we got the chapter, lets create / update the membership object
                        $membership = Membership::model()->findByAttributes(array('chapter_id' => $chapters[$chapterCode]->chapter_id, 'member_id' => $model->member_id));
                        if ($membership == null) {
                            //Shared::debug("create new membership");
                            // create new one
                            $tempMembership->chapter_id = $chapters[$chapterCode]->chapter_id;
                            $tempMembership->member_id = $model->member_id;

                            if ($tempMembership->join_date == null) {
                                $tempMembership->join_date = Shared::timeNow();
                            }
                            if ($tempMembership->expiration_date == null) {
                                $tempMembership->expiration_date = Shared::timeNow();
                            }
                            $tempMembership->active = 1;
                            $tempMembership->save();
                        } else {
                            // update existing one, just make sure it does not overwrite non-empty values
                            // first remove some fields, so we can determine if the object is dirty
                            $attributes = $tempMembership->attributes;
                            unset($attributes['member_id']);
                            unset($attributes['chapter_id']);
                            unset($attributes['active']);
                            $membership->setAttributes($attributes);
                            if ($membership->isDirty()) {
                                Shared::debug("membership for {$model->email_address} is dirty");
                                Shared::debug($membership->getDirtyAttributes());
                                $updated = true;
                            }
                            $membership->save();
                        }

                        // there was a change
                        if ($updated) {
                            Shared::debug("there was an update");
                            $result['updated']++;
                        }
                    }
                } else {
                    $result['invalid']++;
                    Shared::debug($model->getErrors());
                }

                // process only part of it
                if ($result['total'] == self::$chunkSize) {
                    break;
                }
            }
            fclose($handle);
        }
        return $result;
    }

}
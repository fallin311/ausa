<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property string $category_id
 * @property string $category_name
 * @property string $category_icon
 * @property string $category_description
 * @property string $marker
 * @property integer $published
 *
 * The followings are the available model relations:
 * @property Offer[] $offers
 */
class Category extends AOActiveRecord {

    private $image;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Category the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'category';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('category_name', 'required'),
            array('published', 'numerical', 'integerOnly' => true),
            array('category_name', 'length', 'max' => 63),
            array('category_icon', 'length', 'max' => 31),
            array('marker', 'length', 'max' => 16),
            array('category_description', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('category_id, category_name, category_icon, category_description, published', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'offers' => array(self::MANY_MANY, 'Offer', 'offer_category(category_id, offer_id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'category_id' => 'Category',
            'category_name' => 'Category Name',
            'category_icon' => 'Category Icon',
            'marker' => 'Map Marker',
            'category_description' => 'Category Description',
            'published' => 'Published',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    /* public function search()
      {
      // Warning: Please modify the following code to remove attributes that
      // should not be searched.

      $criteria=new CDbCriteria;

      $criteria->compare('category_id',$this->category_id,true);
      $criteria->compare('category_name',$this->category_name,true);
      $criteria->compare('category_icon',$this->category_icon,true);
      $criteria->compare('category_description',$this->category_description,true);
      $criteria->compare('published',$this->published);

      return new CActiveDataProvider($this, array(
      'criteria'=>$criteria,
      ));
      } */
    
    public function scopes(){
        return array(
            'published'=>array(
                'condition'=>'published=1',
            )
        );
    }
    
    /**
     * We need to load whole list of banners for given user (chapter), which are
     * active. Also, it loads only limited set of attributes (performance)
     * It uses DAO instad of AR for performance reasons
     * Returns multi-dimensional member array used in json api
     */
    public static function getApiList($timestamp = null) {
        $db = app()->db;
        $sql = "SELECT * FROM category WHERE published = 1";

        if ($timestamp != null) {
            $sql .= " AND updated_on > '$timestamp'";
        }
        Shared::debug($sql);
        $categories = array();
        $command = $db->createCommand($sql);
        $dataReader = $command->query();
        while (($row = $dataReader->read()) !== false) {
            // we need to get images as well
            $categories[] = array(
                'id' => $row['category_id'],
                'name' => $row['category_name'],
                'marker' => $row['marker'],
                'icon' => Image::getCachedBase64Src($row['category_icon']),
                //'icon' => Image::getImageUrl($row['category_icon'], 80),
                'description' => $row['category_description']
            );
        }
        
        return $categories;
    }
    
    /**
     * Set the correct updated_on mark
     */
    public function afterValidate() {
        parent::afterValidate();
        if ($this->isDirty()){
            $this->updated_on = Shared::toDatabase(time());
        }
    }
    
    public function getImageUrl($width = 80){
        return Image::getImageUrl($this->category_icon, $width);
        /*if ($this->image == null){
            $this->image = Image::findByPkLight($this->category_icon);
        }
        
        if ($this->image != null){
            return $this->image->getUrl($width);
        }else{
            // default image
            return url('/images/no-category-' . $width . '.png');
        }*/
    }
    
}
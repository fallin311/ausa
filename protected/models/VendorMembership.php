<?php

/**
 * This is the model class for table "vendor_membership".
 *
 * The followings are the available columns in table 'vendor_membership':
 * @property string $vendor_id
 * @property string $chapter_id
 * @property string $since
 * @property string $valid_to
 * @property integer $active
 */
class VendorMembership extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return VendorMembership the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'vendor_membership';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vendor_id, chapter_id', 'required'),
            array('active', 'numerical', 'integerOnly' => true),
            array('vendor_id, chapter_id', 'length', 'max' => 10),
            array('since, valid_to', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('vendor_id, chapter_id, since, valid_to, active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'vendor_id' => 'Vendor',
            'chapter_id' => 'Chapter',
            'since' => 'Since',
            'valid_to' => 'Valid To',
            'active' => 'Active',
        );
    }

    public static function create($vendor, $chapter) {
        if (is_numeric($vendor))
            $vendor = Vendor::model()->findByPk($vendor);
        if (is_numeric($chapter))
            $chapter = Chapter::model()->findByPk($chapter);
        if ($vendor != null && $chapter != null) {
            $vendorChapter = self::model()->findByAttributes(array('chapter_id' => $chapter->chapter_id, 'vendor_id' => $vendor->vendor_id));
            if ($vendorChapter == null) {
                $vendorChapter = new VendorMembership;
                $vendorChapter->vendor_id = $vendor->vendor_id;
                $vendorChapter->chapter_id = $chapter->chapter_id;
                $vendorChapter->since = Shared::timeNow();
                $vendorChapter->active = 1;
            }
            return $vendorChapter;
        }
        return null;
    }

    public function getVendorsSalesteam($id) {
        $connection = app()->db;
        $queryStId = "SELECT sales_team_id FROM st_vendor WHERE vendor_id='$id';";
        $runStId = $connection->createCommand($queryStId);
        $getStId = $runStId->queryScalar();
        $queryStName = "SELECT st_name FROM sales_team WHERE sales_team_id='$getStId';";
        $getStName = $connection->createCommand($queryStName);
        $result = $getStName->queryScalar();
        return $result;
    }
}
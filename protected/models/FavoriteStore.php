<?php

/**
 * This is the model class for table "favorite_store".
 *
 * The followings are the available columns in table 'favorite_store':
 * @property string $vendor_id
 * @property string $member_id
 */
class FavoriteStore extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return FavoriteStore the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'favorite_store';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vendor_id, member_id', 'required'),
            array('vendor_id, member_id', 'length', 'max' => 10),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('vendor_id, member_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'vendor_id' => 'Vendor',
            'member_id' => 'Member',
        );
    }

    /**
     * List of favorites contains only changes (records with dirty bit) recorded
     * since the last visit
     * @param type $favorites array(array(vendor_id, favorite),...))
     */
    public static function syncData($member, $favorites) {
        foreach ($favorites as $favorite) {
            Shared::debug($favorite);
            // find the object
            $model = FavoriteStore::model()->findByAttributes(array('member_id' => $member->member_id, 'vendor_id' => $favorite['id']));
            if ($favorite['favorite'] == 0 && is_object($model)) {
                if (!$favorite['favorite']) {
                    $model->delete();
                }
            } elseif ($favorite['favorite'] == 1 && !is_object($model)) {
                $model = FavoriteStore::createModel('FavoriteStore', array(
                            'member_id' => $member->member_id,
                            'vendor_id' => $favorite['id']
                        ));
                $model->save();
            }
        }
    }

}
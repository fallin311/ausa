<?php

/**
 * This is the model class for table "plan".
 *
 * The followings are the available columns in table 'plan':
 * @property string $plan_id
 * @property string $plan_name
 * @property string $plan_description
 * @property integer $active
 * @property string $access_level
 * @property integer $max_offers
 * @property integer $offer_duration
 * @property integer $max_branches
 * @property integer $max_banners
 * @property integer $max_categories;
 * @property integer $banner_duration
 * @property double $single_payment_price
 * @property double $monthly_recurring_price
 * @property string $type
 * @property string $pg_product_id
 * @property integer $plan 
 *
 * The followings are the available model relations:
 * @property Payment[] $payments
 * @property Vendor[] $vendors
 */
class Plan extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Plan the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @var array Returns and array of the months formatted to suit date('y') 
     */
    public static $months = array(
        '01' => 'Jan - 01',
        '02' => 'Feb - 02',
        '03' => 'Mar - 03',
        '04' => 'Apr - 04',
        '05' => 'May - 05',
        '06' => 'June - 06',
        '07' => 'July - 07',
        '08' => 'Aug - 08',
        '09' => 'Sept - 09',
        '10' => 'Oct - 10',
        '11' => 'Nov - 11',
        '12' => 'Dec - 12'
    );

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'plan';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('max_offers, offer_duration, max_branches, max_banners, banner_duration, max_categories, active', 'numerical', 'integerOnly' => true),
            array('monthly_recurring_price, single_payment_price, setup_fee', 'numerical'),
            array('plan_name', 'length', 'max' => 31),
            array('plan_description', 'length', 'max' => 255),
            array('type, access_level', 'length', 'max' => 7),
            array('pg_product_id', 'length', 'max' => 16),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('plan_id, plan_name, plan_description, max_offers, offer_duration, max_branches, max_banners, banner_duration, type, pg_product_id, active, access_level', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'payments' => array(self::HAS_MANY, 'Payment', 'plan_id'),
            'vendors' => array(self::HAS_MANY, 'Vendor', 'plan_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'plan_id' => 'Plan',
            'plan_name' => 'Plan Name',
            'plan_description' => 'Plan Description',
            'active' => 'Active',
            'access_level' => 'Access Level',
            'max_offers' => 'Max Offers',
            'offer_duration' => 'Offer Duration',
            'max_branches' => 'Max Branches',
            'max_banners' => 'Max Banners',
            'max_categories' => 'Max Offer Categories',
            'banner_duration' => 'Banner Duration',
            'single_payment_price' => 'Single Payment Price',
            'setup_fee' => 'Setup Fee',
            'monthly_recurring_price' => 'Monthly Recurring Price',
            'type' => 'Type',
            'pg_product_id' => 'Product',
        );
    }

    /**
     * @return array Returns an array of years starting from the current year to the number specified (12 years default)
     * @author Travis Stroud <travis@inradiussystems.com>
     */
    public static function getExpYearArray($yrs = 12) {
        $years = array();
        $maxDates = $yrs;
        $currYearShort = date('y');
        $currYearLong = date('Y');
        for ($i = 0; $i < $maxDates; $i++) {
            $years[$currYearShort + $i] = $currYearLong + $i;
        }
        return $years;
    }

    /**
     * @return count Gets the most used plan by vendors
     * @author Travis Stroud <travis@inradiussystems.com>
     */
    public function getPopularPlan() {
        $db = app()->db;
        $query = 'SELECT plan_id, COUNT(plan_id) AS cnt FROM vendor GROUP BY plan_id ORDER BY cnt DESC LIMIT 1;';
        $run = $db->createCommand($query);
        $count = $run->queryScalar();
        return $count;
    }

    /**
     * @return true or false Checks access of a logged in user to available plans
     * @author Vinayak Melarkod <vk@ausaoffers.com>
     */
    public function hasAccess() {
        if (app()->user->isAdmin() && $this->active) {
            return true;
        } else if (app()->user->isSalesman() && $this->active && ($this->access_level == "st" || $this->access_level == "public")) {
            return true;
        } else {
            if ($this->active && $this->access_level == "public") {
                return true;
            }
        }
        return false;
    }

}
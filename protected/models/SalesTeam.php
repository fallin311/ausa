<?php

/**
 * This is the model class for table "sales_team".
 *
 * The followings are the available columns in table 'sales_team':
 * @property string $sales_team_id
 * @property string $chapter_id
 * @property boolean $disabled
 * @property string $st_name
 * @property string $st_description
 * @property string $address
 * @property string $logo
 *
 * The followings are the available model relations:
 * @property Chapter $chapter
 * @property Vendor[] $vendors
 * @property User[] $users
 */
class SalesTeam extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SalesTeam the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sales_team';
    }

    public function behaviors() {
        return array(
            'companyBehavior' => array(
                'class' => 'application.models.behaviors.CompanyBehavior'
            )
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('chapter_id, st_name', 'required'),
            array('disabled', 'numerical', 'integerOnly' => true),
            array('chapter_id', 'length', 'max' => 10),
            array('st_name', 'length', 'max' => 128),
            array('address', 'length', 'max' => 511),
            array('logo', 'length', 'max' => 31),
            array('st_description', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('sales_team_id, chapter_id, st_name, st_description, address, phone, disabled, email_address, logo', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'chapter' => array(self::BELONGS_TO, 'Chapter', 'chapter_id'),
            'vendors' => array(self::MANY_MANY, 'Vendor', 'st_vendor(sales_team_id, vendor_id)'),
            'users' => array(self::MANY_MANY, 'User', 'user_st(sales_team_id, user_id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'sales_team_id' => 'Sales Team',
            'chapter_id' => 'Associated Chapter',
            'st_name' => 'Sales Team Name',
            'st_description' => 'Description',
            'address' => 'Address',
            'disabled' => 'Disabled',
            'logo' => 'Logo',
        );
    }

    /**
     * @return total number of active offers by a Sales Team within a chapter
     * @author Vinayak Melarkod <vk@inradiussystems.com>
     */
    public function getTotalActiveOffers($chId) {

        $connection = Yii::app()->db;
        $sql = "SELECT COUNT(offer_id) FROM offer c
                    LEFT JOIN vendor_membership vm
                    ON c.vendor_id = vm.vendor_id
                    WHERE vm.chapter_id=" . $chId . " AND vm.active=1 AND
                    c.start_date<=DATE(NOW()) AND c.end_date>=DATE(NOW()) AND c.active=1 AND c.published=1 AND
                    c.vendor_id IN (SELECT vendor_id FROM st_vendor WHERE sales_team_id=" . $this->sales_team_id . " ;";
        $command = $connection->createCommand($sql);
        $value = $command->queryScalar();
        return $value;
    }

    /**
     * @return total number of vendors in a chapter by a SalesTeam
     * @author Vinayak Melarkod <vk@inradiussytems.com>
     */
    public function getTotalVendors() {

        $connection = Yii::app()->db;
        //$sql = "SELECT COUNT(vendor_id) FROM st_vendor WHERE sales_team_id=" . $this->sales_team_id . " AND active=1"; 
        $sql = "SELECT COUNT(stv.vendor_id) FROM st_vendor stv
                    LEFT JOIN vendor v ON stv.vendor_id = v.vendor_id
                    WHERE stv.sales_team_id=" . $this->sales_team_id . " AND stv.active=1 AND v.disabled=0 AND v.expiration>= NOW() AND v.published=1";
        $command = $connection->createCommand($sql);
        $value = $command->queryScalar();
        return $value;
    }

    /*
     * Return array of relavent branch data for a sales teams vendors
     * Travis - 10/23/2012
     */

    public function getVendorBranches($st) {
        $connection = app()->db;
        $branchArray = array();
        $branches = array();
        
        $query = "SELECT b.branch_id, v.vendor_id, b.lat, b.lon, c.marker
            FROM branch b, st_vendor sv, vendor v, category c
            WHERE b.vendor_id = sv.vendor_id
            AND sv.vendor_id = v.vendor_id AND sv.sales_team_id = $st
            AND c.category_id = v.industry;";
        $run = $connection->createCommand($query);
        $branchArray = $run->queryAll();
        
        $iconUrl = url('/images/mapMarkers');
        
        foreach($branchArray as $row) {
            $branches[] = array('latLng' => array($row['lat'], $row['lon']),
                'data' => "<div data-vendor_id='" . $row['vendor_id'] . "' data-branch_id='" .$row['branch_id'] . "'></div>",
                'options' => array('icon' => $iconUrl . '/' . $row['marker'] . '.png', 'shadow' => $iconUrl . '/icon-shadow.png'));
        }
        //Shared::debug($branches);
        
        return $branches;
    }

}
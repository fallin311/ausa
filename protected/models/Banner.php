<?php

/**
 * This is the model class for table "banner".
 *
 * The followings are the available columns in table 'banner':
 * @property string $banner_id
 * @property string $vendor_id
 * @property string $start_date
 * @property string $end_date
 * @property string $updated_on
 * @property string $banner_name
 * @property integer $active
 * @property integer $dashobard Whether or not the banner is displayed on the mobile dashboard
 * @property integer $published
 * @property string $layout
 * @property string $icon_image
 * @property string $panorama_image
 * @property string $message_text
 * @property string $target
 * @property string $external_url
 * @property integer $impressions
 * @property integer $clicks
 *
 * The followings are the available model relations:
 * @property Vendor $vendor
 */
class Banner extends AOActiveRecord {

    /**
     * Virtual attribute used in a form
     * @var type 
     */
    public $campaign_duration;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Banner the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'banner';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vendor_id, banner_name, layout, target, panorama_image', 'required'),
            array('active, published, impressions, clicks, dashboard', 'numerical', 'integerOnly' => true),
            array('vendor_id, offer_id', 'length', 'max' => 10),
            array('banner_name', 'length', 'max' => 63),
            array('layout', 'length', 'max' => 5),
            array('icon_image, panorama_image', 'length', 'max' => 31),
            array('start_date', 'validateConcurency'),
            array('start_date, end_date', 'required', 'on' => 'publish'),
            array('start_date', 'validateConcurency', 'on' => 'publish', 'publishedOnly' => true), // when the banner is being published, we need to make sure there is only one
            array('message_text, external_url', 'length', 'max' => 255),
            array('target', 'length', 'max' => 8),
            array('start_date, end_date, updated_on', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('banner_id, vendor_id, start_date, end_date, updated_on, banner_name, active, published, layout, icon_image, panorama_image, message_text, target, external_url, impressions, clicks', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),
        );
    }

    public function scopes() {
        return array(
            // only those, which should be sent to cellphones
            'published' => array(
                'condition' => 'published=1 AND vendor.disabled=0 AND vendor.banner_expiration > NOW()',
                'with' => 'vendor',
            )
        );
    }

    public function behaviors() {
        return array(
            'offerBehavior' =>
            array(
                'class' => 'application.models.behaviors.OfferBehavior'
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'banner_id' => 'Banner',
            'vendor_id' => 'Vendor',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'updated_on' => 'Updated On',
            'banner_name' => 'Banner Name',
            'active' => 'Active',
            'published' => 'Published',
            'layout' => 'Layout',
            'icon_image' => 'Icon Image',
            'panorama_image' => 'Panorama Image',
            'message_text' => 'Message Text',
            'target' => 'Target',
            'external_url' => 'External Url',
            'impressions' => 'Impressions',
            'clicks' => 'Clicks',
        );
    }

    /**
     * We need to make sure that no other banners are in the same window
     * We want warning for unpublished banners and error for published
     * @param type $attribute
     * @param type $params
     */
    function validateConcurency($attribute, $params) {
        // the question is how to produce warning on ajax validate and error on
        // published resource
        $checkPublished = false;
        if (isset($params['publishedOnly'])) {
            $checkPublished = $params['publishedOnly'];
        }
        if ($this->published && $checkPublished && $this->vendor_id) {
            // find coliding banners using interval arithmetics
            $colliding = Banner::model()->findAllBySql("SELECT banner_id, banner_name, start_date, end_date, published FROM banner 
                WHERE vendor_id={$this->vendor_id} AND published > 0 " . ($this->isNewRecord ? '' : ' AND banner_id != ' . $this->banner_id) . " AND(
                (start_date<='{$this->start_date}' AND end_date >= '{$this->start_date}') OR
                (start_date<='{$this->end_date}' AND end_date >='{$this->end_date}') OR
                (start_date>='{$this->start_date}' AND end_date <='{$this->end_date}'))");
            $allowed = $this->vendor->max_banners;
            if ($allowed == 0) {
                $this->addError('banner_name', 'Your account is not enabled to publish a banner. Please contact your sales person.');
                return false;
            } else if (count($colliding)) {
                Shared::debug("there are some colliding banners");
                // we don't really care about expiration date right now
                if (count($colliding) >= $allowed) {
                    // allright, this would not work
                    // get list of banners, so we can display nice error message
                    $existingMsg = '';
                    foreach ($colliding as $banner) {
                        $existingMsg .= ', ' . $banner->banner_name . ' (from ' . Shared::formatDateLonger($banner->start_date) . ' to ' . Shared::formatDateLonger($banner->end_date) . ')';
                    }

                    $this->addError('start_date', 'Please select different publishing date for this banner. Published banners: ' . substr($existingMsg, 2));
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Constructs default values before the model is displayed in the create form
     */
    public function loadDefaults() {
        if (!$this->start_date) {
            $this->start_date = date('Y-m-d', time());
        }

        if (!$this->end_date) {
            $this->end_date = Shared::toDatabase(strtotime($this->start_date . " +12 month"));
        }

        if (!$this->target) {
            $this->target = 'external';
        }

        if (!$this->layout) {
            $this->layout = 'image';
        }

        $vendor = Vendor::model()->findByPk(app()->user->getActiveVendor());

        if (!$this->external_url) {
            $this->external_url = $vendor->website;
        }

        if (!$this->banner_name) {

            $this->banner_name = "Banner #" . (count($vendor->banners) + 1);
            Shared::debug($this->banner_name);
        }
    }

    /**
     * Create or update the model from form. This function does not save or
     * valiate the model.
     * @param type $input
     * @return boolean|Offer
     */
    public static function create($input, $model = null) {
        // suppose the model is alrady found in the database
        Shared::debug("create");
        if ($model == null) {
            $model = self::createModel('Banner', $input);
        } else {
            $model->setAttributes($input);
        }

        // set basic attributes
        $vendor = Yii::app()->user->getActiveVendor();
        if (!$model->vendor_id && !$vendor) {
            return null;
        }

        if (!$model->vendor_id)
            $model->vendor_id = $vendor;
        $model->updated_on = Shared::timeNow();

        // calculate the end date
        if ($model->dashboard != 1) {

            if (!isset($input['campaign_duration']) && isset($input['end_date'])) {
                // use end date from the select
                $model->end_date = $input['end_date'];
                if (strtotime($model->end_date) < strtotime($model->start_date)) {
                    // set default duration
                    $model->end_date = Shared::toDatabase(strtotime($model->start_date . " +12 month"));
                }
            } else if (isset($input['campaign_duration'])) {
                // calculate the end date
                $duration = $input['campaign_duration'];
                if ($duration < 1 || $duration > 12) {
                    $duration = 1;
                }
                if (isset($input['start_date']) && $model->isNewRecord) {
                    $model->end_date = Shared::toDatabase(strtotime($model->start_date . " +" . $duration . " month"));
                } else {
                    // update the model. There are special rules for published banners
                    // no date changes once it is published
                    // TODO

                    if ($model->published) {
                        // make sure we can only extend the banner
                        $new = strtotime($model->start_date . " +" . $duration . " month");
                        if ($new > time()) {
                            $model->end_date = Shared::toDatabase($new);
                        }
                    } else {
                        Shared::debug("not published");
                        $model->end_date = Shared::toDatabase(strtotime($model->start_date . " +" . $duration . " month"));
                    }
                }
            }
        }

        return $model;
    }

    public function getImageUrl($width = 320) {
        if (strlen($this->panorama_image) > 0) {
            return url('/image/display', array('img' => $this->panorama_image, 'w' => $width));
        } else {
            // default image
            return url('/images/no-category-' . $width . '.png');
        }
    }

    /**
     * We need to load whole list of banners for given user (chapter), which are
     * active. Also, it loads only limited set of attributes (performance)
     * It uses DAO instad of AR for performance reasons
     * Returns multi-dimensional member array used in json api
     */
    public static function getApiList($chapterId, $timestamp = null, $width = 240, $user = null) {
        $db = app()->db;

        if ($width < 200)
            $width = 200;
        if ($width > 640)
            $width = 640;



        if ($user == null) {
            // find offers by chapter
            if ($timestamp == null) {
                $timestampSql = ' AND b.published=1 AND b.end_date > NOW() AND v.published=1';
            } else {
                // also include all not expired offers from recently updated vendor
                $timestampSql = " AND (b.updated_on > '$timestamp' OR v.updated_on > '$timestamp')";
            }

            $chWhere = "m.chapter_id = " . $chapterId;
            $sql = "SELECT b.banner_id, b.vendor_id, offer_id, panorama_image, target, external_url,
            IF ((DATE_ADD(v.expiration, INTERVAL 1 DAY) < NOW() OR v.published=0), '1', '0') AS 'vendor_expired',
            IF (b.start_date < NOW() AND (DATE_ADD(b.end_date, INTERVAL 1 DAY) > NOW() AND b.published=1), '1', '0') AS 'published'
            FROM banner b 
            LEFT JOIN vendor v ON v.vendor_id = b.vendor_id LEFT JOIN vendor_membership m ON v.vendor_id = m.vendor_id 
            WHERE v.disabled=0 AND v.activated=1 AND DATE_ADD(v.banner_expiration, INTERVAL 1 DAY) > NOW() AND m.active>0 AND $chWhere";
        } else {
            // find offers by user
            if ($timestamp == null) {
                $timestampSql = '';
            } else {
                // also include all not expired offers from recently updated vendor
                $timestampSql = " AND (b.updated_on > '$timestamp' OR v.updated_on > '$timestamp')";
            }
            if ($user->isAdmin()){
                $vendorIds = "";
            } else {
                $vendorIds =  "AND v.vendor_id IN (" . implode(",", $user->getVendorIds()) . ")";
            }
            //$chWhere = "v.vendor_id IN (" . implode(",", $user->getVendorIds()) . ")";
            $sql = "SELECT b.banner_id, b.vendor_id, offer_id, panorama_image, target, external_url,
            IF ((DATE_ADD(v.expiration, INTERVAL 1 DAY) < NOW() OR v.published=0), '1', '0') AS 'vendor_expired',
            IF (b.start_date < NOW() AND (DATE_ADD(b.end_date, INTERVAL 1 DAY) > NOW() AND b.published=1), '1', '0') AS 'published'
            FROM banner b 
            LEFT JOIN vendor v ON v.vendor_id = b.vendor_id LEFT JOIN vendor_membership m ON v.vendor_id = m.vendor_id 
            WHERE v.disabled=0 AND m.active>0 $vendorIds";
        }



        Shared::debug($sql);
        if ($timestamp != null) {
            $sql .= " AND b.updated_on > '$timestamp'";
        }
        $banners = array();
        $command = $db->createCommand($sql);
        $dataReader = $command->query();
        while (($row = $dataReader->read()) !== false) {
            // TODO: prepare images (BASE64)
            $banners[] = array(
                'id' => $row['banner_id'] + 0,
                'vendorId' => $row['vendor_id'] + 0,
                'offerId' => $row['offer_id'] + 0,
                'bannerImage' => Image::getCachedBase64Src($row['panorama_image'], $width),
                'target' => $row['target'],
                'published' => ($row['vendor_expired'] == 1 ? '0' : $row['published']) + 0,
                'externalUrl' => $row['external_url']
            );
        }
        return $banners;
    }

    /**
     * Recieve data from a cellphone through API and create additional stats objects
     * related to this offer. This transaction should be executed with locked table to prevent
     * incorrect statistics.
     * This function basically only updates access statistics on the banner object
     * 
     * @param type $member
     * @param type $banner array(
     * array('offer_id','clicks'=>integer))
     * )
     */
    public function syncData($data) {
        if (isset($data['clicks']))
            $this->clicks += $data['clicks'];
        if (isset($data['impressions']))
            $this->impressions += $data['impressions'];
    }

    /**
     * Get information about publishing dates and limits displayed in the edit
     * form. This can be displayed directly or through ajax callback after
     * date change in create /update form
     * @param type $startDate
     * @param type $endDate
     * @return array Array of the following keys:
     *  limits: textual representation how many offers is vendor allowed to publish
     *  colisions: array of coliding banners in the selected date range. Contains title, offerId, startDate, endDate
     *  canPublish: boolean, can the vendor publish this offer right now in the selected date range?
     */
    public function getPublishingInfo($startDate = null, $endDate = null) {
        if ($startDate != null)
            $this->start_date = $startDate;
        if ($endDate != null)
            $this->end_date = $endDate;

        // check limits
        $output = array();
        if (!$this->vendor_id) {
            $this->vendor = Vendor::model()->findByPk(app()->user->getActiveVendor());
            $this->vendor_id = $this->vendor->vendor_id;
        }

        if ($this->vendor->max_banners == 0) {
            $output['limits'] = "You cannot publish the banner yet. Please activate banners in your account first.";
        } else if ($this->vendor->max_banners == 1) {
            $output['limits'] = "You can publish one banner.";
        } else {
            $output['limits'] = "You can publish up to " . $this->vendor->max_banners . " banners at time.";
        }

        $output['canPublish'] = true;

        // check vendor expiration
        if ($this->vendor->max_banners > 0) {
            if (strtotime($this->end_date) > strtotime($this->vendor->banner_expiration)) {
                $output['limits'] .= " Your subscription expires on " . Shared::formatDateShort($this->vendor->banner_expiration) . ". If you don't extend your subscription by this day, your banner might be unpublished. ";
            } else if (strtotime($this->end_date) < strtotime($this->vendor->banner_expiration)) {
                if (strtotime($this->vendor->banner_expiration) > time()) {
                    $output['limits'] .= " Your banner subscription will expire on " . Shared::formatDateShort($this->vendor->banner_expiration) . '.';
                } else {
                    $output['canPublish'] = false;
                    $output['limits'] .= " Your banner subscription has expired. Please activate banners in your account first.";
                }
            }
        }

        // check coliding banners
        $colliding = Banner::model()->findAllBySql("SELECT banner_id, banner_name, start_date, end_date, published FROM banner 
                WHERE vendor_id={$this->vendor_id} AND published > 0 " . ($this->isNewRecord ? '' : ' AND banner_id != ' . $this->banner_id) . " AND(
                (start_date<='{$this->start_date}' AND end_date >= '{$this->start_date}') OR
                (start_date<='{$this->end_date}' AND end_date >='{$this->end_date}') OR
                (start_date>='{$this->start_date}' AND end_date <='{$this->end_date}'))");
        $output['colisions'] = array();
        foreach ($colliding as $banner) {
            $output['colisions'][] = array(
                'title' => $banner->banner_name,
                'bannerId' => $banner->banner_id,
                'start_date' => Shared::formatShortUSDate($banner->start_date),
                'end_date' => Shared::formatShortUSDate($banner->end_date),
            );
        }

        if (count($colliding) >= $this->vendor->max_banners) {
            $output['canPublish'] = false;
            $output['limits'] .= "<b>Please select different publishing date. It has not to colide with the dates below.</b>";
        }

        return $output;
    }

}

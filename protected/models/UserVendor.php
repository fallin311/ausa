<?php

/**
 * This is the model class for table "user_vendor".
 *
 * The followings are the available columns in table 'user_vendor':
 * @property string $vendor_id
 * @property string $user_id
 * @property string $since
 * @property integer $active
 * @property integer $send_redemption_email
 */
class UserVendor extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserVendor the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_vendor';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vendor_id, user_id', 'required'),
            array('active, send_redemption_email', 'numerical', 'integerOnly' => true),
            array('vendor_id, user_id', 'length', 'max' => 10),
            array('since', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('vendor_id, user_id, since, active, send_redemption_email', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'vendor_id' => 'Vendor',
            'user_id' => 'User',
            'since' => 'Since',
            'active' => 'Active',
            'send_redemption_email' => 'Send Redemption Email',
        );
    }
    
    public static function findByUser($user, $vendor){
        return self::model()->findByAttributes(array('user_id' => $user->user_id, 'vendor_id' => $vendor->vendor_id));
    }

    public static function create($user, $vendor){
        if (is_numeric($user)) $user = User::model()->findByPk($user);
        if (is_numeric($vendor)) $vendor = Vendor::model()->findByPk($vendor);
        if ($user != null && $vendor != null){
            $userVendor = self::findByUser($user, $vendor);
            if ($userVendor == null){
                $userVendor = new UserVendor;
                $userVendor->user_id = $user->user_id;
                $userVendor->vendor_id = $vendor->vendor_id;
                $userVendor->since = Shared::timeNow();
                $userVendor->active = 1;
                $userVendor->send_redemption_email = 1;
            }
            return $userVendor;
        }
        return null;
    }
}
<?php

/**
 * This is the model class for table "redemption".
 *
 * The followings are the available columns in table 'redemption':
 * @property string $redemption_id
 * @property string $member_offer_id
 * @property double $ticket_value
 * @property string $redemption_activity
 * @property string $redemption_type
 * @property string $branch_id
 * @property double $lat
 * @property double $lon
 *
 * The followings are the available model relations:
 * @property MemberOffer $campaignMember
 */
class Redemption extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Redemption the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'redemption';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('member_offer_id', 'required'),
            array('ticket_value, lat, lon', 'numerical'),
            array('member_offer_id, branch_id', 'length', 'max' => 10),
            array('redemption_type', 'length', 'max' => 7),
            array('redemption_activity', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('redemption_id, member_offer_id, ticket_value, redemption_activity, redemption_type, branch_id, lat, lon', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'campaignMember' => array(self::BELONGS_TO, 'MemberOffer', 'member_offer_id'),
            'branch' => array(self::BELONGS_TO, 'Branch', 'branch_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'redemption_id' => 'Redemption',
            'member_offer_id' => 'Campaign Member',
            'ticket_value' => 'Ticket Value',
            'redemption_activity' => 'Redemption Activity',
            'redemption_type' => 'Redemption Type',
            'branch_id' => 'Branch',
            'lat' => 'Lat',
            'lon' => 'Lon',
        );
    }

}
<?php

/**
 * This is the model class for table "st_vendor".
 *
 * The followings are the available columns in table 'st_vendor':
 * @property string $sales_team_id
 * @property string $vendor_id
 * @property string $since
 * @property integer $active
 */
class StVendor extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return StVendor the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'st_vendor';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('sales_team_id, vendor_id', 'required'),
            array('active', 'numerical', 'integerOnly' => true),
            array('sales_team_id, vendor_id', 'length', 'max' => 10),
            array('since', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('sales_team_id, vendor_id, since, active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),
            'st' => array(self::BELONGS_TO, 'SalesTeam', 'sales_team_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'sales_team_id' => 'Sales Team',
            'vendor_id' => 'Vendor',
            'since' => 'Since',
            'active' => 'Active',
        );
    }

    public static function create($salesTeam, $vendor){
        if (is_numeric($salesTeam)) $salesTeam = SalesTeam::model()->findByPk($salesTeam);
        if (is_numeric($vendor)) $vendor = Vendor::model()->findByPk($vendor);
        if ($salesTeam != null && $vendor != null){
            $stVendor = self::model()->findByAttributes(array('sales_team_id' => $salesTeam->sales_team_id, 'vendor_id' => $vendor->vendor_id));
            if ($stVendor == null){
                $stVendor = new StVendor;
                $stVendor->vendor_id = $vendor->vendor_id;
                $stVendor->sales_team_id = $salesTeam->sales_team_id;
                $stVendor->since = Shared::timeNow();
                $stVendor->active = 1;
            }
            return $stVendor;
        }
        return null;
    }
}
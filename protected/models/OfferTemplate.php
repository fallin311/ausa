<?php

/**
 * This is the model class for table "offer_template".
 *
 * The followings are the available columns in table 'offer_template':
 * @property string $offer_template_id
 * @property string $offer_name
 * @property string $offer_description
 * @property string $offer_image
 * @property string $type
 * @property string $vendor_id
 * @property string $category_id
 */
class OfferTemplate extends AOActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return OfferTemplate the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'offer_template';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('offer_name, offer_image', 'required'),
            array('offer_name', 'length', 'max' => 63),
            array('offer_image', 'length', 'max' => 31),
            array('offer_description', 'length', 'max' => 255),
            array('vendor_id, category_id', 'length', 'max' => 10),
            array('type', 'length', 'max' => 8),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('offer_template_id, offer_name, offer_description, offer_image, type, vendor_id, category_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'offer_template_id' => 'Offer Template',
            'offer_name' => 'Offer Name',
            'offer_description' => 'Offer Description',
            'offer_image' => 'Offer Image',
            'type' => 'Type',
            'vendor_id' => 'Vendor',
            'category_id' => 'Category / Industry',
        );
    }

    public function getImageUrl($width) {
        return Image::getImageUrl($this->offer_image, $width);
    }
}
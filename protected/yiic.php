<?php

// Set a fallback timezone if the current php.ini does not contain a default timezone setting.
// If the environment is setup correctly, we won't override the timezone.
date_default_timezone_set("America/Denver");

// change the following paths if necessary
$yiic=dirname(__FILE__).'/../yii/yiic.php';
$config=dirname(__FILE__).'/config/console.php';

require_once($yiic);

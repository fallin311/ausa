<?php

// change the following paths if necessary
$yiit=dirname(__FILE__).'/../../yii/yiit.php';
$config=dirname(__FILE__).'/../config/test.php';

require_once($yiit);
require_once(dirname(__FILE__).'/WebTestCase.php');

require_once(dirname(__FILE__) . '/../helpers/shortcuts.php');

// TODO: should match client timezone
if (!date_default_timezone_get()) {
    // Set a fallback timezone if the current php.ini does not contain a default timezone setting.
    // If the environment is setup correctly, we won't override the timezone.
    date_default_timezone_set("America/Denver");
}

// prevent login error
ob_start();

Yii::createWebApplication($config);

<?php

class SubscriptionTest extends CTestCase {

    public function _testGetTransactions() {
        $subscription = new Subscription;
        $transactions = $subscription->getRecentTransactions();
        Shared::debug($transactions);
    }

    public function testSOAPTransaction() {
        $transactionObject = array(
            'Command' => 'authonly',
            'AccountHolder' => 'Toro 12345',// . rand(2, 399),
            'Details' => array(
                'Clerk' => 'MaRSHALL',
                'Terminal' => 'Marshall Sales Company',
                'Table' => 'Omar Bradley Chapter',
                'Description' => 'AUSA Offers - Monthly Subscription',
                'Amount' => '1',
                'Tax' => '10.25',
                //'Shipping' => '20',
                //'Subtotal' => '90',
            ),
            'CreditCardData' => array(
                'CardCode' => '123', //'999', //123
                'CardNumber' => '4000100011112224', //'4000100011112224',
                'CardExpiration' => '0914',
                'CardType' => '',
                'AvsStreet' => '360 N. Festival Dr.',
                'AvsZip' => '79912',
                'CardPresent' => false,
            ),
            'ClientIP' => '129.108.78.154',
            'CustomerID' => '1203',//rand(400, 1000),
            'BillingAddress' => array(
                'FirstName' => 'vinayak',
                'LastName' => 'melarod',
                'Company' => 'Toro 12345',
                'Street' => '1127 E. Rio Grande Ave.',
                'Street2' => '',
                'City' => 'El Paso',
                'State' => 'Texas',
                'Zip' => '79902',
                'Country' => 'USA',
                'Email' => 'vsmela.rkod@gmail.com',
                'Phone' => '9152190230',
            ),
            'CustReceipt' => true,
            'Software' => 'Test Script - Ausa Offers',
            'CustReceiptName' => 'Test Customer Receipt Name 123',
            'RecurringBilling' => array(
                'Schedule' => 'monthly',
                'Next' => '2013-2-23',
                'NumLeft' => '*',
                'Amount' => '100.25',
                'Enabled' => true,
            ),
        );

        Shared::debug($transactionObject);

        $client = Yii::app()->usaepay->getSoapClient();
        $token = Yii::app()->usaepay->getToken();
        try {
            $result = $client->runTransaction($token, $transactionObject);
            Shared::debug($result);
            //return $result;
        } catch (Exception $exc) {

            //echo $exc->getMessage();
            Yii::log($exc->getMessage(), 'error');
            //Todo: how to handle update failure??
        }
    }

}
?>


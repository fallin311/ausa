<?php

class VendorTest extends CTestCase {

    private function getMember() {
        $member = new Member('register');
        $member->prefix = 'Mr.';
        $member->first_name = 'Unit';
        $member->last_name = 'Test';
        $member->email_address = 'vsmelarkod@gmail.com';
        $member->phone_number = '9152190230';
        $member->address1 = '1127 Rio Grande Ave.';
        //$member->full_address = '1127 Rio Grande Ave.';
        $member->city = 'El Paso';
        $member->zipcode = '79902';
        $member->state = 'TX';
        $member->country = 'USA';
        $member->access_pin = 12345;
        $member->birthday = '06/26/1982';
        
        //$member->membership->chapter->chapter_id = 1;
        $member->army_status = 'GC';
        $member->military_unit = 'Unit 1234';
        //$member->join_date = '01/29/2013';
        //$member->expiration_date = '01/28/2014';
        return $member;
    }

    
    /**
     * Send a registration email to Chapter Admin
     */
    function _testEmailRegistrationChapter() {
        $member = $this->getMember();
        $this->assertFalse($member->hasErrors());

        $chapter = Chapter::model()->findByPk(1);

        $mail = new AOEmail('memberRegistrationChapter');
        $mail->addPlaceholders(array(
                    'prefix' => CHtml::encode($member->prefix),
                    'first_name' => CHtml::encode($member->first_name),
                    'last_name' => CHtml::encode($member->last_name),
                    'full_address' => CHtml::encode($member->getFullAddress()),
                    'city' => CHtml::encode($member->city),
                    'state' => CHtml::encode($member->state),
                    'zipcode' => CHtml::encode($member->zipcode),
                    'phone_number' => strlen($member->phone_number) > 4 ? CHtml::encode($member->phone_number) : 'N/A',
                    'email_address' => CHtml::encode($member->email_address),
                    'birthday' => Shared::formatShortUSDate($member->birthday),
                    
                    'chapter_id'=>'c4405',
                    'chapter_name'=>'GA Omar N. Bradley',
                    'army_status' => CHtml::encode($member->army_status),
                    'military_unit' => CHtml::encode($member->military_unit),
                    'price_level' => 'EC4 EC5 etc.',
                    'price' => '$14.00',
                    'payment_reference_number' => "Coming Soon...",
                    
                    'join_date' => '01/29/2013',
                    'expiration_date' => '01/28/2014',
                ));
        $mail->addRecipient($member->email_address, CHtml::encode($member->getFullName()));

        // temporary monitoring
        //$mail->addRecipient('admin@ausaoffers.com', 'AUSA Offers Admin');
        $this->assertTrue($mail->send());
    }
    
    /**
     * Test comunication with the payment gateway.
     */
    function _testMemberPayment() {
        $member = $this->getMember();
        $this->assertFalse($member->hasErrors());

        $chapter = Chapter::model()->findByPk(1);

        $payment = new MembershipPayment;
        $payment->member = $member;
        $payment->chapter = $chapter;

        // set payment attributes
        $payment->useBillingAddress = 1;
        $payment->cardNumber = '4000101611112225';
        //$payment->cardNumber = '4000101611112226';
        $payment->cardExpMonth = 9;
        $payment->cardExpYear = 14;
        $payment->cardCode = 999;

        // pricing is based on this one
        $payment->level = 4;

        $response = $payment->process();
        Shared::debug($response);
    }

    /**
     * Send a registration email to member
     */
    function testEmailRegistration() {
        $member = $this->getMember();
        $this->assertFalse($member->hasErrors());

        $chapter = Chapter::model()->findByPk(1);

        $mail = new AOEmail('memberRegistration');
        $mail->addPlaceholders(array(
            'pin' => $member->access_pin,
            'phone_number' => strlen($member->phone_number) > 4 ? CHtml::encode($member->phone_number) : 'N/A',
            'email' => CHtml::encode($member->email_address),
            'full_name' => CHtml::encode($member->getFullName()),
                //'join_date' => Shared::formatShortUSDate($this->membership->join_date),
                //'expiration_date' => Shared::formatShortUSDate($this->membership->expiration_date),
        ));
        $mail->addRecipient($member->email_address, CHtml::encode($member->getFullName()));

        // temporary monitoring
        //$mail->addRecipient('admin@ausaoffers.com', 'AUSA Offers Admin');
        $this->assertTrue($mail->send());
    }


}

?>

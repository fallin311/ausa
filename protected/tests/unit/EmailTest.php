<?php

class EmailTest extends CTestCase {

    /**
     * Send simple stupid email through SES using Shared::sendEmail
     */
    public function _testAmazonEmail() {
        $subject = 'testEmail';
        $name = 'Ondrej';
        $body = 'sup, buddy?';
        $toEmail = 'ondrej.nebesky@gmail.com';
        $fromEmail = 'ondrej@inradiussystems.com';
    
        $response = Shared::sendEmail($subject, $body, $toEmail, $name, $fromEmail, 'test');
        $this->assertTrue($response);
    }
    
    public function _testAoEmail() {
        $mail = new AOEmail("newPin");
        $this->assertNotEquals(false, $mail);
        $mail->addPlaceholders(array('pin' => 'goHome', 'email_address' => 'test@test.com'));
        $mail->setSender('ondrej@inradiussystems.com');
        $mail->addRecipient('ondrej.nebesky@gmail.com', 'Ondrej');
        $mail->addRecipient('vk@inradiussystems.com', 'Victor');
        $mail->send();
    }
}

?>

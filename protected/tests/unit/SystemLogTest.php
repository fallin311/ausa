<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SystemLogTest
 *
 * @author One
 */
class SystemLogTest extends CTestCase {
    public function testErrorLog(){
        $log = new SystemLog('application.log');
        $logFile = app()->getBasePath() . '/runtime/application.log';
        $logSize = filesize($logFile);
        if ($logSize){
            // test only if we have data to test
            
            $lines = $log->read(10);
            $this->assertEquals(10, count($lines));
            
            $lines = $log->read(30);
            $this->assertEquals(30, count($lines));
        }
        echo "$logFile\n";
        echo $logSize;
    }
}

?>

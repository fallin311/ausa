<?php

/**
 * Test functions related to addresses and GPS location
 */
class AddressTest extends CTestCase {
    
    function testAddressTranslation(){
        Yii::import('application.helpers.AddressHelper');
        $address = '1127 Rio Grande, El Paso, TX';
        $latLon = AddressHelper::toLatLon($address);
        Shared::debug($latLon);
        
        $this->assertEquals('79902', $latLon['zipcode']);
        $this->assertEquals('31.770', $latLon['lat'], 'Latitude found', 5);
    }
}
?>

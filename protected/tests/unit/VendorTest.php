<?php

class VendorTest extends CTestCase {
    
    /*function testCreateByAdmin(){
        Fixtures::cleanup();
        Fixtures::getAdminUser();

        // lets login as a new user
        Fixtures::loginAs('testAdmin');
        
        // lets get some data we might get from the form
        // make sure those emails and names are defined in fixtures, so we can delete them later on
        $user = array(
            'first_name' => 'Vendor',
            'last_name' => 'User',
            'phone' => '915 333 5541',
            'email_address' => 'vendor_user_2@inradiussystems.com'
        );
        
        $vendor = array(
            'vendor_name' => 'AO Test Vendor 2',
            'address' => '1127 Rio Grande, El Paso, TX, 79902',
            'website' => 'http://inradiussystems.com',
            'description' => 'Non fixture test vendor',
            'allow_login' => false,
        );
        
        $output = Vendor::create(array('vendor' => $vendor, 'user' => $user));
        $this->assertTrue($output['success']);
        
        // lets see if all the objects were created as expected
        $userVendor = UserVendor::model()->findByAttributes(array('user_id' => $output['user']->user_id, 'vendor_id' => $output['vendor']->vendor_id));
        $this->assertNotNull($userVendor);
        
        $branch = Branch::model()->findByAttributes(array('vendor_id' => $output['vendor']->vendor_id));
        $this->assertNotNull($branch);
    }*/
    
    function _testCreateBySt(){
        Fixtures::cleanup();
        Fixtures::getUser('stUser1');

        // lets login as a new user
        Fixtures::loginAs('stUser1');
        
        $chapter = Fixtures::getChapter('chapter1');
        
        // lets get some data we might get from the form
        // make sure those emails and names are defined in fixtures, so we can delete them later on
        $user = array(
            'first_name' => 'Vendor',
            'last_name' => 'User',
            'phone' => '915 333 5541',
            'email_address' => 'vendor_user_2@inradiussystems.com'
        );
        
        $vendor = array(
            'vendor_name' => 'AO Test Vendor 2',
            'address' => '1127 Rio Grande, El Paso, TX, 79902',
            'website' => 'http://inradiussystems.com',
            'description' => 'Non fixture test vendor',
            'allow_login' => false,
            'chapter_id' => $chapter->chapter_id
        );
        
        $output = Vendor::create(array('vendor' => $vendor, 'user' => $user));

        $this->assertTrue($output['success']);
        
        // lets see if all the objects were created as expected
        $userVendor = UserVendor::model()->findByAttributes(array('user_id' => $output['user']->user_id, 'vendor_id' => $output['vendor']->vendor_id));
        $this->assertNotNull($userVendor);
        
        $vendorSalesman = VendorSalesman::model()->findByAttributes(array('user_id' => app()->user->id, 'vendor_id' => $output['vendor']->vendor_id));
        $this->assertNotNull($vendorSalesman);
        
        $vendorMembership = VendorMembership::model()->findByAttributes(array('vendor_id' => $output['vendor']->vendor_id, 'chapter_id' => $chapter->chapter_id));
        $this->assertNotNull($vendorMembership);
        
        $branch = Branch::model()->findByAttributes(array('vendor_id' => $output['vendor']->vendor_id));
        $this->assertNotNull($branch);
    }
    
    public function testGetChapterBranches(){
        // the ugly way
        $chapter = Chapter::model()->findByPk(1);
        $branches = $chapter->getVendorBranches();
        Shared::debug($branches);
    }

}
?>

<?php

/**
 * Helper class that creates objects used for testing
 */
class Fixtures {

    // this is replacement for fixtures
    static $users = array(
        'primaryAdmin' => array(
            // always there, but not used to test, just to create everything else
            'email_address' => 'admin@inradiussystems.com',
            'password' => 'projectZien',
        ),
        'testAdmin' => array(
            'first_name' => 'Admin',
            'last_name' => 'User',
            'email_address' => 'test_admin@inradiussystems.com',
            'super_admin' => 1,
            'password' => 'IamDaAdm1n',
            'email_verified' => 1,
            'phone_number' => '999 999 9001'
        ),
        'vendorUser1' => array(
            'first_name' => 'Vendor',
            'last_name' => 'User',
            'email_address' => 'vendor_user_1@inradiussystems.com',
            'super_admin' => 0,
            'password' => 'IamDaUs3r',
            'email_verified' => 1,
            'phone_number' => '999 999 9002',
            'vendors' => array('vendor1') // where does he have access
        ),
        'vendorUser2' => array(
            'first_name' => 'Vendor',
            'last_name' => 'User',
            'email_address' => 'vendor_user_2@inradiussystems.com',
            'super_admin' => 0,
            'password' => 'IamDaUs3r',
            'email_verified' => 1,
            'phone_number' => '999 999 9003',
            'vendors' => array('vendor2') // where does he have access
        ),
        'vendorUser3' => array(
            'first_name' => 'Vendor',
            'last_name' => 'User',
            'email_address' => 'vendor_user_3@inradiussystems.com',
            'super_admin' => 0,
            'password' => 'IamDaUs3r',
            'email_verified' => 1,
            'phone_number' => '999 999 9003',
            'mobile_login' => 95412,
            'mobile_pin' => 95412,
            'vendors' => array('vendor1', 'vendor2', 'vendor3') // where does he have access
        ),
        'stUser1' => array(
            'first_name' => 'Sales',
            'last_name' => 'User',
            'email_address' => 'sales_user_1@inradiussystems.com',
            'super_admin' => 0,
            'password' => 'IamDaUs3r',
            'email_verified' => 1,
            'phone_number' => '999 999 9004',
            'salesTeams' => array('st1') // where does he have access
        ),
        'chapterUser1' => array(
            'first_name' => 'Chapter',
            'last_name' => 'User',
            'email_address' => 'chapter_user_1@inradiussystems.com',
            'super_admin' => 0,
            'password' => 'IamDaUs3r',
            'email_verified' => 1,
            'phone_number' => '999 999 9005',
            'chapters' => array('chapter1') // where does he have access
        ),
        'chapterUser2' => array(
            'first_name' => 'Chapter',
            'last_name' => 'User',
            'email_address' => 'chapter_user_2@inradiussystems.com',
            'super_admin' => 0,
            'password' => 'IamDaUs3r',
            'email_verified' => 1,
            'phone_number' => '999 999 9006',
            'chapters' => array('chapter2') // where does he have access
        )
    );
    static $vendors = array(
        'vendor1' => array(
            'vendor_name' => 'AO Test Vendor 1',
            'vendor_description' => 'This is only a testing object. Delete it if you do not need it anymore.',
            'website' => 'http://ausaoffers.com',
            'address' => '1127 Rio Grande, El Paso, TX',
            'banner_expiration' => '2014-8-17',
            'expiration' => '2014-8-17',
            'max_banners' => 1,
            'max_offers' => 2,
            'chapter' => 'chapter1',
            'activated' => 1,
            'branches' => array('branch4'),
        ),
        'vendor2' => array(
            'vendor_name' => 'AO Test Vendor 2',
            'vendor_description' => 'This is only a testing object. Delete it if you do not need it anymore.',
            'website' => 'http://ausaoffers.com',
            'address' => '1127 Rio Grande, El Paso, TX',
            'max_branches' => 3, // reduce number of default branches
            'branches' => array('branch1', 'branch2', 'branch3'),
            'banner_expiration' => '2014-8-17',
            'expiration' => '2012-8-17',
            'chapter' => 'chapter1',
            'activated' => 1,
        ),
        'vendor3' => array(
            'vendor_name' => 'AO Test Vendor 3',
            'vendor_description' => 'This is only a testing object. Delete it if you do not need it anymore.',
            'website' => 'http://ausaoffers.com',
            'address' => '1127 Rio Grande, El Paso, TX',
            'chapter' => 'chapter2',
            'expiration' => '2014-8-17',
            'max_banners' => 0,
            'max_offers' => 1,
            'max_branches' => 2,
            'branches' => array('branch5', 'branch9'),
            'activated' => 1,
        ),
        'vendor4' => array(
            'vendor_name' => 'AO Test Vendor 4',
            'vendor_description' => 'This is only a testing object. Delete it if you do not need it anymore.',
            'website' => 'http://ausaoffers.com',
            'address' => '1127 Rio Grande, El Paso, TX',
            'chapter' => 'chapter1',
            'branches' => array('branch6'),
            'activated' => 1,
        ),
        // expired banners and offers
        'vendor5' => array(
            'vendor_name' => 'AO Test Vendor 5',
            'vendor_description' => 'This is only a testing object. Delete it if you do not need it anymore.',
            'website' => 'http://ausaoffers.com',
            'address' => '1127 Rio Grande, El Paso, TX',
            'banner_expiration' => '2012-8-17',
            'expiration' => '2012-8-17',
            'chapter' => 'chapter1',
            'branches' => array('branch7'),
            'activated' => 1,
        ),
        // this one should not be visible inside the system
        'vendor6' => array(
            'vendor_name' => 'AO Test Vendor 6',
            'vendor_description' => 'This is only a testing object. Delete it if you do not need it anymore.',
            'website' => 'http://ausaoffers.com',
            'address' => '1127 Rio Grande, El Paso, TX',
            'disabled' => 1,
            'chapter' => 'chapter1',
            'branches' => array('branch8'),
            'activated' => 1,
        ),
    );
    static $branches = array(
        'branch1' => array(
            'branch_name' => 'AO Test Branch #1',
            'branch_address' => '1127 Rio Grande, El Paso, TX',
            'active' => 0,
        ),
        'branch2' => array(
            'branch_name' => 'AO Test Branch #2',
            'branch_address' => '1127 Rio Grande, El Paso, TX',
        ),
        'branch3' => array(
            'branch_name' => 'AO Test Branch #3',
            'branch_address' => '1127 Rio Grande, El Paso, TX',
            'phone_number' => '19151234444',
            'zipcode' => '79902',
            'branch_address' => '2609 North Mesa Street, El Paso, TX'
        ),
        'branch4' => array(
            'branch_name' => 'AO Test Branch #4',
            'phone_number' => '19151234444',
            'zipcode' => '79902',
            'branch_address' => '2609 North Mesa Street, El Paso, TX'
        ),
        'branch5' => array(
            'branch_name' => 'AO Test Branch #5',
            'phone_number' => '19151234454',
            'zipcode' => '79902',
            'branch_address' => '2609 North Mesa Street, El Paso, TX'
        ),
        'branch6' => array(
            'branch_name' => 'AO Test Branch #6',
            'phone_number' => '19151234444',
            'zipcode' => '79902',
            'branch_address' => '3600 North Mesa Street, El Paso, TX'
        ),
        'branch7' => array(
            'branch_name' => 'AO Test Branch #7',
            'phone_number' => '19151234446',
            'zipcode' => '79902',
            'branch_address' => '2609 North Mesa Street, El Paso, TX'
        ),
        'branch8' => array(
            'branch_name' => 'AO Test Branch #8',
            'phone_number' => '19151234744',
            'zipcode' => '79902',
            'branch_address' => '2200 North Mesa Street, El Paso, TX'
        ),
        'branch9' => array(
            'branch_name' => 'AO Test Branch #9',
            'phone_number' => '19151234484',
            'zipcode' => '79902',
            'branch_address' => '2709 North Mesa Street, El Paso, TX'
        ),
    );
    static $chapters = array(
        'chapter1' => array(
            'chapter_name' => 'AO Test Chapter 1',
            'ausa_id' => '123321',
            'city' => 'Hell Paso',
            'country' => 'USA',
            'state' => 'TX',
            'ausa_id' => '4405TST'
        ),
        'chapter2' => array(
            'chapter_name' => 'AO Test Chapter 2',
            'ausa_id' => '123322',
            'city' => 'Heaven Paso',
            'country' => 'USA',
            'state' => 'TX',
            'ausa_id' => '4406TST'
        ),
    );
    static $salesTeams = array(
        'st1' => array(
            'st_name' => 'AO Test Sales 1',
            'st_description' => 'Testing Sales Team. You can delete it any time.',
            'chapter' => 'chapter1',
            'vendors' => array('vendor1', 'vendor4')
        ),
        'st2' => array(
            'st_name' => 'AO Test Sales 2',
            'st_description' => 'Testing Sales Team. You can delete it any time.',
            'chapter' => 'chapter1'
        ),
        'st3' => array(
            'st_name' => 'AO Test Sales 3',
            'st_description' => 'Testing Sales Team. You can delete it any time.',
            'chapter' => 'chapter2'
        ),
    );
    static $members = array(
        'member1' => array(
            'first_name' => 'Mackenzie',
            'last_name' => 'Hudkins',
            'prefix' => 'PFC',
            'ausa_id' => '2961722',
            'email_address' => 'hudkins@ausatest.com',
            'phone_number' => '29154445555',
            'join_date' => '2012-06-07',
            'expiration_date' => '2014-06-07',
            'chapter' => 'chapter1',
            'access_pin' => '54198'
        ),
        'member2' => array(
            'first_name' => 'Stephanie',
            'last_name' => 'Parras',
            'prefix' => 'SPC',
            'ausa_id' => '2926112',
            'phone_number' => '29159296241',
            'email_address' => 'parras@ausatest.com',
            'join_date' => '2012-04-02',
            'expiration_date' => '2012-06-07',
            'access_pin' => '45921',
            'chapter' => 'chapter1'
        ),
        'member3' => array(
            'first_name' => 'Jordan',
            'last_name' => 'Bombard',
            'prefix' => 'PFC',
            'ausa_id' => '2957663',
            'phone_number' => '23365436322',
            'email_address' => 'bombard@ausatest.com',
            'join_date' => '2012-06-03',
            'chapter' => 'chapter1'
        ),
        'member4' => array(
            'first_name' => 'Tania',
            'last_name' => 'Zeiler',
            'prefix' => 'MSG',
            'ausa_id' => '2921982',
            'phone_number' => '22704016450',
            'email_address' => 'zeiler@ausatest.com',
            'join_date' => '2012-07-03',
            'chapter' => 'chapter1'
        ),
        'member5' => array(
            'first_name' => 'Jacob',
            'last_name' => 'Nelson',
            'prefix' => 'LT',
            'ausa_id' => '2617983',
            'phone_number' => '27327571694',
            'email_address' => 'nelson@ausatest.com',
            'join_date' => '2012-03-29',
            'chapter' => 'chapter1'
        ),
        'member6' => array(
            'first_name' => 'Felix',
            'last_name' => 'Danz',
            'prefix' => 'SPC',
            'ausa_id' => '2981767',
            'phone_number' => '26783870042',
            'email_address' => 'danz@ausatest.com',
            'join_date' => '2012-07-19',
            'chapter' => 'chapter1'
        ),
        'member7' => array(
            'first_name' => 'Casey',
            'last_name' => 'Cupples',
            'prefix' => 'SPC',
            'ausa_id' => '2925989',
            'phone_number' => '29155043468',
            'email_address' => 'cupples@ausatest.com',
            'join_date' => '2012-04-02',
            'chapter' => 'chapter1'
        ),
        'member8' => array(
            'first_name' => 'Judith',
            'last_name' => 'Severns',
            'prefix' => 'SGM',
            'ausa_id' => '2133164',
            'email_address' => 'severns@ausatest.com',
            'join_date' => '1996-04-02',
            'chapter' => 'chapter1'
        ),
        'member9' => array(
            'first_name' => 'Veronica',
            'last_name' => 'Finklea',
            'prefix' => 'Ms.',
            'ausa_id' => '2642635',
            'phone_number' => '29155340600',
            'email_address' => 'finklea@ausatest.com',
            'join_date' => '2010-09-08',
            'chapter' => 'chapter1'
        ),
        'member10' => array(
            'first_name' => 'Glen',
            'last_name' => 'Cody',
            'prefix' => 'PFC',
            'ausa_id' => '2958241',
            'phone_number' => '28186366597',
            'join_date' => '2012-09-08',
            'expiration_date' => '2014-09-08',
            'chapter' => 'chapter1'
        ),
        'member11' => array(
            'first_name' => 'Michael',
            'last_name' => 'Neumeister',
            'prefix' => 'SGT',
            'ausa_id' => '2981797',
            'join_date' => '2010-07-19',
            'chapter' => 'chapter1'
        ),
        'member12' => array(
            'first_name' => 'Patricia',
            'last_name' => 'Sneller',
            'prefix' => 'SGT',
            'ausa_id' => '2911239',
            'phone_number' => '29562958781',
            'email_address' => 'finklea@ausatest.com',
            'join_date' => '2010-03-16',
            'chapter' => 'chapter1'
        ),
    );
    static $banners = array(
        'banner1' => array(
            'vendor' => 'vendor1',
            'start_date' => '2012-08-17',
            'end_date' => '2014-08-17',
            'banner_name' => 'AO Test Banner #1
                with line breaks, "yay"',
            'active' => 1,
            'published' => 1,
            'layout' => 'image',
            'message_text' => 'Testing Banner',
            'panorama_image' => 'VF49puFsqeD3D',
            'target' => 'internal'
        ),
        // active banner, inactive vendor
        'banner2' => array(
            'vendor' => 'vendor5',
            'start_date' => '2012-08-17',
            'end_date' => '2014-08-17',
            'banner_name' => 'AO Test Banner #2',
            'active' => 1,
            'published' => 1,
            'layout' => 'image',
            'message_text' => 'Testing Banner',
            'panorama_image' => 'VF49puFsqeD3D',
            'target' => 'internal'
        ),
        // active banner, disabled vendor
        'banner3' => array(
            'vendor' => 'vendor6',
            'start_date' => '2012-08-17',
            'end_date' => '2014-08-17',
            'banner_name' => 'AO Test Banner #3',
            'active' => 1,
            'published' => 1,
            'layout' => 'image',
            'message_text' => 'Testing Banner',
            'panorama_image' => 'VF49puFsqeD3D',
            'target' => 'internal'
        ),
        // inactive banner, active vendor
        'banner4' => array(
            'vendor' => 'vendor1',
            'start_date' => '2011-08-17',
            'end_date' => '2012-08-16',
            'banner_name' => 'AO Test Banner #4',
            'active' => 0,
            'published' => 1,
            'layout' => 'image',
            'message_text' => 'Testing Banner',
            'panorama_image' => 'VF49puFsqeD3D',
            'target' => 'internal'
        ),
            // additional banners used to test the validator
            /* 'banner5' => array(
              'vendor' => 'vendor1',
              'start_date' => '2012-08-15', // coliding with the beginning
              'end_date' => '2012-08-26',
              'banner_name' => 'AO Test Banner #5',
              'active' => 0,
              'published' => 1,
              'layout' => 'image',
              'message_text' => 'Testing Banner',
              'target' => 'internal'
              ),
              'banner5' => array(
              'vendor' => 'vendor1',
              'start_date' => '2014-07-17',
              'end_date' => '2015-09-17',// coliding with the end
              'banner_name' => 'AO Test Banner #6',
              'active' => 0,
              'published' => 1,
              'layout' => 'image',
              'message_text' => 'Testing Banner',
              'target' => 'internal'
              ),
              'banner6' => array(
              'vendor' => 'vendor1',
              'start_date' => '2011-07-17', // coliding with the beginning outside
              'end_date' => '2015-09-17',// coliding with the end
              'banner_name' => 'AO Test Banner #6',
              'active' => 0,
              'published' => 1,
              'layout' => 'image',
              'message_text' => 'Testing Banner',
              'target' => 'internal'
              ),
              'banner7' => array(
              'vendor' => 'vendor1',
              'start_date' => '2012-08-22', // coliding with the beginning inside
              'end_date' => '2012-09-17',// coliding with the end
              'banner_name' => 'AO Test Banner #7',
              'active' => 0,
              'published' => 1,
              'layout' => 'image',
              'message_text' => 'Testing Banner',
              'target' => 'internal'
              ), */
    );
    static $offers = array(
        // active offer
        'offer1' => array(
            'offer_title' => 'AO Test Offer #1',
            'start_date' => '2012-08-17',
            'end_date' => '2014-08-17',
            'user_id' => 1,
            'published' => 1,
            'offer_description' => 'Test offer for unit testing',
            'redemption_code' => '12311',
            'vendor' => 'vendor1',
            'categories' => array(2),
        ),
        'offer2' => array(
            'offer_title' => 'AO Test Offer #2',
            'start_date' => '2012-08-17',
            'end_date' => '2014-08-17',
            'user_id' => 1,
            'published' => 1,
            'offer_description' => 'Test offer for unit testing',
            'redemption_code' => '12311',
            'vendor' => 'vendor1',
            'categories' => array(2,3),
        ),
        // expired offer
        'offer3' => array(
            'offer_title' => 'AO Test Offer #3',
            'start_date' => '2011-08-01',
            'end_date' => '2012-08-01',
            'user_id' => 1,
            'published' => 1,
            'offer_description' => 'Test offer for unit testing',
            'redemption_code' => '12311',
            'vendor' => 'vendor1',
            'categories' => array(1,3),
        ),
        'offer4' => array(
            'offer_title' => 'AO Test Offer #4',
            'start_date' => '2012-08-17',
            'end_date' => '2014-08-17',
            'user_id' => 1,
            'published' => 1,
            'offer_description' => 'Test offer for unit testing',
            'redemption_code' => '12311',
            'vendor' => 'vendor3',
            'categories' => array(1,3),
        ),
        'offer5' => array(
            'offer_title' => 'AO Test Offer #5',
            'start_date' => '2012-08-17',
            'end_date' => '2014-08-17',
            'user_id' => 1,
            'published' => 1,
            'offer_description' => 'Test offer for unit testing',
            'redemption_code' => '12311',
            'vendor' => 'vendor2',
            'categories' => array(1),
        ),
    );

    /**
     * Login as a user defined above.
     * @param type $login
     * @param type $password
     */
    public static function loginAs($login, $password = null) {
        if ($password == null) {
            // find the password in fixture itself
            $password = self::$users[$login]['password'];
            $login = self::$users[$login]['email_address'];
        }
        self::logout();
        $_identity = new UserIdentity($login, $password);
        $_identity->authenticate();
        Yii::app()->user->login($_identity);
    }

    public static function loginAsSuperAdmin() {
        if (!app()->user->isAdmin())
            self::loginAs(self::$users['primaryAdmin']['email_address'], self::$users['primaryAdmin']['password']);
    }

    public static function logout() {
        if (!app()->user->isGuest) {
            app()->user->logout();
            // make sure he is really logged out!
            if (isset($_SESSION)) {
                app()->user->clearStates();
                $_SESSION = array();
            }
        }
    }

    /**
     * Delete all the test objects from the database. If they are not created
     * this function will create them and delete them.
     * All the objects are looked up by name or email address
     */
    public static function cleanup() {
        self::loginAsSuperAdmin();

        foreach (self::$vendors as $_vendor) {
            $vendor = Vendor::model()->findByAttributes(array('vendor_name' => $_vendor['vendor_name']));
            if (is_object($vendor)) {
                $vendor->delete();
            }
        }

        foreach (self::$chapters as $_chapter) {
            $chapter = Chapter::model()->findByAttributes(array('chapter_name' => $_chapter['chapter_name']));
            if (is_object($chapter)) {
                $chapter->delete();
            }
        }

        foreach (self::$salesTeams as $_st) {
            $st = SalesTeam::model()->findByAttributes(array('st_name' => $_st['st_name']));
            if (is_object($st)) {
                $st->delete();
            }
        }

        foreach (self::$users as $_user) {
            if ($_user['email_address'] != 'admin@inradiussystems.com') {
                $user = User::model()->findByAttributes(array('email_address' => $_user['email_address']));
                if (is_object($user)) {
                    $user->delete();
                }
            }
        }

        foreach (self::$members as $_member) {
            $member = Member::findByWhatever(isset($_member['email_address']) ? $_member['email_address'] : null, isset($_member['phone_number']) ? $_member['phone_number'] : null, isset($_member['ausa_id']) ? $_member['ausa_id'] : null);
            if (is_object($member)) {
                $member->delete();
            }
        }

        foreach (self::$banners as $_banner) {
            $banner = Banner::model()->findByAttributes(array('banner_name' => $_banner['banner_name']));
            if (is_object($banner)) {
                $banner->delete();
            }
        }

        foreach (self::$offers as $_offer) {
            $offer = Offer::model()->findByAttributes(array('offer_title' => $_offer['offer_title']));
            if (is_object($offer)) {
                $offer->delete();
            }
        }
    }

    //
    // Users

    //
    
    /**
     * Create super admin user for tests. There is always only one admin user.
     * This user is used more often, so he deserves his own function.
     * @return type
     */
    static function getAdminUser() {
        // we need admin user to create new user acccount
        self::loginAsSuperAdmin();
        $user = User::findByEmail('test_admin@inradiussystems.com');
        if ($user == null) {
            // this should create password hash
            $user = User::create(self::$users['testAdmin']);
            if (!$user->save())
                throw new CException(print_r($user->getErrors(), 1));
        }
        return $user;
    }

    /**
     * Returns new user or finds it in a database. Use super admin login to
     * create the user
     * @param type $userId
     */
    static function getUser($userId) {
        if (isset(self::$users[$userId])) {
            // look for it
            $user = User::model()->findByAttributes(array('email_address' => self::$users[$userId]['email_address']));
            if ($user == null) {
                self::loginAsSuperAdmin();

                // this should create password hash
                $user = User::create(self::$users[$userId]);
                if (!$user->save())
                    throw new CException(print_r($user->getErrors(), 1));

                // create roles ... note that we don't assert roles membership here
                if (isset(self::$users[$userId]['vendors'])) {
                    foreach (self::$users[$userId]['vendors'] as $vendorId) {
                        $vendor = self::getVendor($vendorId);
                        if ($vendor != null) {
                            $userVendor = UserVendor::create($user, $vendor);
                            if (!$userVendor->save()) {
                                throw new CException(print_r($userVendor->getErrors(), 1));
                            }
                        }
                    }
                }

                if (isset(self::$users[$userId]['chapters'])) {
                    foreach (self::$users[$userId]['chapters'] as $chapterId) {
                        $chapter = self::getChapter($chapterId);
                        if ($chapter != null) {
                            $userChapter = UserChapter::create($user, $chapter);
                            if (!$userChapter->save()) {
                                throw new CException(print_r($userChapter->getErrors(), 1));
                            }
                        }
                    }
                }

                if (isset(self::$users[$userId]['salesTeams'])) {
                    foreach (self::$users[$userId]['salesTeams'] as $stId) {
                        $st = self::getSalesTeam($stId);
                        if ($st != null) {
                            $userSt = UserSt::create($user, $st);
                            if (!$userSt->save()) {
                                throw new CException(print_r($userSt->getErrors(), 1));
                            }
                        }
                    }
                }
            }
            return $user;
        }
        return null;
    }

    static function getChapter($chapterId) {
        if (is_numeric($chapterId)) {
            $chapterId = 'chapter' . $chapterId;
        }
        if (isset(self::$chapters[$chapterId])) {
            // find it in database
            $chapter = Chapter::model()->findByAttributes(array('chapter_name' => self::$chapters[$chapterId]['chapter_name']));
            if ($chapter == null) {
                $chapter = Chapter::createModel('Chapter', self::$chapters[$chapterId]);
                if (!$chapter->save()) {
                    throw new CException(print_r($chapter->getErrors(), 1));
                }
            }
            return $chapter;
        }
        return null;
    }

    static function getVendor($vendorId) {
        if (is_numeric($vendorId)) {
            $vendorId = 'vendor' . $vendorId;
        }
        if (isset(self::$vendors[$vendorId])) {
            // find it in database
            $vendor = Vendor::model()->findByAttributes(array('vendor_name' => self::$vendors[$vendorId]['vendor_name']));
            if ($vendor == null) {
                $vendor = Vendor::createModel('Vendor', self::$vendors[$vendorId]);
                if (!$vendor->save()) {
                    throw new CException(print_r($vendor->getErrors(), 1));
                }
            }
            if (isset(self::$vendors[$vendorId]['branches'])) {
                foreach (self::$vendors[$vendorId]['branches'] as $_branch) {
                    $branch = Branch::model()->findByAttributes(array('branch_name' => self::$branches[$_branch]['branch_name']));
                    if ($branch == null) {
                        self::$branches[$_branch]['vendor_id'] = $vendor->vendor_id;
                        $branch = Branch::createModel('Branch', self::$branches[$_branch]);
                        if (!$branch->save()) {
                            throw new CException(print_r($branch->getErrors(), 1));
                        }
                    }
                }
            }
            if (isset(self::$vendors[$vendorId]['chapter'])) {
                $chapter = self::getChapter(self::$vendors[$vendorId]['chapter']);
                if ($chapter != null){
                    $membership = VendorMembership::create($vendor, $chapter);
                    $membership->save();
                }
            }
            return $vendor;
        }
        return null;
    }

    static function getMember($memberId) {
        if (is_numeric($memberId)) {
            $memberId = 'member' . $memberId;
        }
        if (isset(self::$members[$memberId])) {
            $_member = self::$members[$memberId];
            $member = Member::findByWhatever(isset($_member['email_address']) ? $_member['email_address'] : null, isset($_member['phone_number']) ? $_member['phone_number'] : null, isset($_member['ausa_id']) ? $_member['ausa_id'] : null);
            if ($member == null) {
                $member = Member::createModel('Member', self::$members[$memberId]);
                if (!$member->save()) {
                    throw new CException(print_r($member->getErrors(), 1));
                }
                if (isset(self::$members[$memberId]['chapter'])) {
                    // find a chapter and create membership
                    $chapter = self::getChapter(self::$members[$memberId]['chapter']);
                    if ($chapter != null) {
                        $membership = Membership::create($member, $chapter);
                        if (isset($_member['expiration_date'])){
                            $membership->expiration_date = $_member['expiration_date'];
                        }
                        if (isset($_member['join_date'])){
                            $membership->join_date = $_member['join_date'];
                        }
                        if (!$membership->save()) {
                            throw new CException(print_r($membership->getErrors(), 1));
                        }
                    }
                }
            }
            return $member;
        }
    }

    static function getBanner($bannerId) {
        if (is_numeric($bannerId)) {
            $bannerId = 'banner' . $bannerId;
        }
        if (isset(self::$banners[$bannerId])) {
            $banner = Banner::model()->findByAttributes(array('banner_name' => self::$banners[$bannerId]['banner_name']));
            if ($banner == null) {
                $banner = Banner::createModel('Banner', self::$banners[$bannerId]);
                // find a vendor for it
                $vendor = self::getVendor(self::$banners[$bannerId]['vendor']);
                if ($vendor != null) {
                    $banner->vendor_id = $vendor->vendor_id;
                    if (!$banner->save()) {
                        throw new CException(print_r($banner->getErrors(), 1));
                    }
                }
            }
            return $banner;
        }
    }

    static function getOffer($offerId) {
        if (is_numeric($offerId)) {
            $offerId = 'offer' . $offerId;
        }
        if (isset(self::$offers[$offerId])) {
            $offer = Offer::model()->findByAttributes(array('offer_title' => self::$offers[$offerId]['offer_title']));
            if ($offer == null) {
                $offer = Offer::createModel('Offer', self::$offers[$offerId]);
                // find a vendor for it
                $vendor = self::getVendor(self::$offers[$offerId]['vendor']);
                if ($vendor != null) {
                    $offer->vendor_id = $vendor->vendor_id;
                    if (!$offer->save()) {
                        throw new CException(print_r($offer->getErrors(), 1));
                    }
                }
                if (isset(self::$offers[$offerId]['categories'])){
                    foreach (self::$offers[$offerId]['categories'] as $_category){
                        $category = Category::model()->findByPk($_category);
                        if (is_object($category)){
                            $cc =OfferCategory::create($offer, $category);
                            $cc->save();
                        }
                    }
                }
            }
            return $offer;
        }
    }

    static function getSalesTeam($stId) {
        if (is_numeric($stId)) {
            $stId = 'st' . $stId;
        }
        if (isset(self::$salesTeams[$stId])) {
            // find it in database
            $st = SalesTeam::model()->findByAttributes(array('st_name' => self::$salesTeams[$stId]['st_name']));
            if ($st == null) {
                $st = SalesTeam::createModel('SalesTeam', self::$salesTeams[$stId]);
                // set a chapter
                $chapter = self::getChapter(self::$salesTeams[$stId]['chapter']);
                if ($chapter != null) {
                    $st->chapter_id = $chapter->chapter_id;
                    if (!$st->save()) {
                        throw new CException(print_r($st->getErrors(), 1));
                    }
                    if (isset(self::$salesTeams[$stId]['vendors'])) {
                        // create vendor association
                        foreach (self::$salesTeams[$stId]['vendors'] as $vendorId) {
                            $vendor = self::getVendor($vendorId);
                            if ($vendor != null) {
                                $stVendor = StVendor::create($st, $vendor);
                                if (!$stVendor->save()) {
                                    throw new CException(print_r($stVendor->getErrors(), 1));
                                }
                            }
                        }
                    }
                }
            }
            return $st;
        }
        return null;
    }

    /**
     * Get max id from the table, so we can delete it after the test
     * @param type $table
     * @return type
     */
    public static function getMaxId($table) {
        $db = app()->db;
        $command = $db->createCommand("SELECT MAX({$table}_id) FROM $table");
        return $command->queryScalar();
    }

}

?>
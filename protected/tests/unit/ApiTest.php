<?php

/**
 * Testing API v1
 */
class ApiTest extends CTestCase {
    
    /**
     * Just login as one of the members and create an access token
     */
    function _testLogin(){
        Fixtures::cleanup();
        Fixtures::getMember(1);
        //$member->attachBehavior('apiBehavior', new ApiMemberBehavior());
        
        // lets try wrong pin
        $member = Member::apiAuthenticate('hudkins@ausatest.com', '11254');
        $this->assertNull($member);
        
        // wrong email
        $member = Member::apiAuthenticate('hudkinsssss@ausatest.com', '11254');
        $this->assertNull($member);
        
        // success
        $member = Member::apiAuthenticate('hudkins@ausatest.com', '54198');
        $this->assertNotNull($member);
        $token = $member->access_token;
        $this->assertTrue(strlen($token) > 20, 'access token was created');
        
        // login using phone number
        $member = Member::apiAuthenticate('29154445555', '54198');
        $this->assertNotNull($member);
        // check that new access token was created
        $this->assertNotEquals($token, $member->access_token);
    }
    
    /**
     * this function should send emails, which is not implemented function yet (August 2012)
     * however, we can check if the authentication and new pin generator works
     */
    function _testForgotPassword(){
        Fixtures::cleanup();
        // find a user with email
        Fixtures::getMember(1);
        $response = Member::forgotPin('hudkins@ausatest.com');
        Shared::debug($response);
        $this->assertEquals(1, $response['success']);
        $this->assertNotNull($response['member']);
        
        // find a user without email
        Fixtures::getMember(10); // find him by ausa id
        $response = Member::forgotPin('2958241');
        Shared::debug($response);
        $this->assertEquals(1, $response['success']);
        $this->assertEquals(0, $response['email_sent']);
        $this->assertNotNull($response['member']);
        
        // look for bullshit
        Fixtures::getMember(10); // find him by ausa id
        $response = Member::forgotPin('295824131');
        $this->assertEquals(0, $response['success']);
        $this->assertEquals(0, $response['email_sent']);
    }
    
    public function _testApiLogin(){
        Fixtures::cleanup();
        $member = Fixtures::getMember(1);
        $response = $this->getApiResponse('login', array('login' => '29154445555', 'pin' => '54198'));
        Shared::debug($response);
        $this->assertEquals(1, $response['success']);
        $this->assertEquals($response['member']['email_address'], $member->email_address);
        
        $member2 = Member::model()->findByPk($member->member_id);
        $this->assertEquals($response['member']['access_token'], $member2->access_token);
        
        // lets do it again with wrong password
        $response = $this->getApiResponse('login', array('login' => '29154445555', 'pin' => '54199'));
        $this->assertEquals(0, $response['success']);
        $this->assertTrue(strlen($response['success']) > 0);
        
        // lets try expired member
        $member = Fixtures::getMember(2);
        $response = $this->getApiResponse('login', array('login' => $member->email_address, 'pin' => $member->access_pin));
        $this->assertEquals(0, $response['success']);
        $this->assertTrue(strlen($response['success']) > 0);
    }
    
    /**
     * Get all the objects from the database
     */
    public function _testApiGetDataWithVersion(){
        
    }
    
    /**
     * Load only updated records
     */
    public function _testApiGetData(){
        Fixtures::cleanup();
        // load some data first
        Fixtures::getVendor(1);
        Fixtures::getVendor(2);
        Fixtures::getVendor(3);
        Fixtures::getVendor(4);
        Fixtures::getVendor(5);
        Fixtures::getVendor(6);
        
        Fixtures::getBanner(1);
        Fixtures::getBanner(2);
        Fixtures::getBanner(3);
        Fixtures::getBanner(4);
        
        Fixtures::getOffer(1);
        Fixtures::getOffer(2);
        Fixtures::getOffer(3);

        $member = Fixtures::getMember(1);
        if (strlen($member->access_token) == 0){
            $member = Member::apiAuthenticate($member->email_address, $member->access_pin);
        }
        $this->assertNotNull($member->access_token);
        $response = $this->getApiResponse('get-data', 'a=' . $member->access_token);
        Shared::debug($response);
        
        // just a simple check that something was returned
        // later on we have to check that the structure match the documentation
        $this->assertTrue(isset($response['categories'][0]));
        $this->assertTrue(isset($response['banners'][0]));
        $this->assertTrue(isset($response['offers'][0]));
        $this->assertTrue(isset($response['vendors'][0]));
    }
    
    public function _testApiSyncData(){
        Fixtures::cleanup();
        $offer1 = Fixtures::getOffer(1);
        $offer2 = Fixtures::getOffer(2);
        
        $banner = Fixtures::getBanner(1);
        
        $vendor = Fixtures::getVendor(1);
        $vendor2 = Fixtures::getVendor(2);
        
        $member = Fixtures::getMember(1);
        if (strlen($member->access_token) == 0){
            $member = Member::apiAuthenticate($member->email_address, $member->access_pin);
        }
        $this->assertNotNull($member->access_token);
        
        $usage = array(
            'offers' => array(
                array(
                    'offer_id' => $offer1->offer_id,
                    'favorite' => 0,
                    'clicks' => array(
                        array(
                            'date' => Shared::timeNow()
                        )
                    ),
                    'redemptions' => array(
                        array(
                            'date' => Shared::timeNow()
                        )
                    ),
                ),
                array(
                    'offer_id' => $offer2->offer_id,
                    'favorite' => 1,
                    'clicks' => array(
                        array(
                            'date' => Shared::timeNow()
                        )
                    ),
                    
                ),
            ),
            'banners' => array(
                array(
                    'banner_id' => $banner->banner_id,
                    'clicks' => 2
                )
            ),
            'stores' => array(
                array('vendor_id' => $vendor->vendor_id, 'favorite' => 1),
                array('vendor_id' => $vendor2->vendor_id, 'favorite' => 0),
            )
        );

        
        // this has to be different since we are sending data out
        $response = $this->getApiResponse('sync-data', 'a=' . $member->access_token, $usage);
        
        $this->assertEquals('1', $response['success']);
        
        // is everything created
        $memberOffer = MemberOffer::model()->findByAttributes(array('member_id' => $member->member_id, 'offer_id' => $offer1->offer_id));
        $this->assertTrue(is_object($memberOffer));
        
        $this->assertTrue(count($memberOffer->redemptions) > 0);
        $this->assertTrue(count($memberOffer->clicks) > 0);
        
        $favStore = FavoriteStore::model()->findByAttributes(array('member_id' => $member->member_id, 'vendor_id' => $vendor->vendor_id));
        $this->assertTrue(is_object($favStore));
        
        $favStore2 = FavoriteStore::model()->findByAttributes(array('member_id' => $member->member_id, 'vendor_id' => $vendor2->vendor_id));
        $this->assertTrue(!is_object($favStore2));
        
        $banner2 = Banner::model()->findByPk($banner->banner_id);
        $this->assertEquals(2, $banner2->clicks);
        
        // send one more request
        $usage = array(
            'offers' => array(
                array(
                    'offer_id' => $offer1->offer_id,
                    'favorite' => 0,
                    'clicks' => array(
                        array(
                            'date' => Shared::timeNow()
                        )
                    ),
                    'redemptions' => array(
                        array(
                            'date' => Shared::timeNow()
                        )
                    ),
                ),
                array(
                    'offer_id' => $offer2->offer_id,
                    'favorite' => 1,
                    'clicks' => array(
                        array(
                            'date' => Shared::timeNow()
                        )
                    ),
                    
                ),
            ),
            'banners' => array(
                array(
                    'banner_id' => $banner->banner_id,
                    'clicks' => 3
                )
            ),
            'stores' => array(
                array('vendor_id' => $vendor->vendor_id, 'favorite' => 0),
                array('vendor_id' => $vendor2->vendor_id, 'favorite' => 1),
            )
        );
        
        $response = $this->getApiResponse('sync-data', 'a=' . $member->access_token, $usage);
        
        $this->assertEquals('1', $response['success']);
        
        $memberOffer = MemberOffer::model()->findByAttributes(array('member_id' => $member->member_id, 'offer_id' => $offer1->offer_id));
        $this->assertTrue(is_object($memberOffer));
        
        $this->assertEquals(2, count($memberOffer->redemptions));
        $this->assertEquals(2, count($memberOffer->clicks));
        
        $favStore = FavoriteStore::model()->findByAttributes(array('member_id' => $member->member_id, 'vendor_id' => $vendor->vendor_id));
        $this->assertTrue(!is_object($favStore));
        
        $favStore2 = FavoriteStore::model()->findByAttributes(array('member_id' => $member->member_id, 'vendor_id' => $vendor2->vendor_id));
        $this->assertTrue(is_object($favStore2));
    }
    
    private function getApiResponse($action, $params, $data = null){
        
        $url = '/apiv2/' . $action;
        if (is_array($params)){
            $url = Yii::app()->createUrl($url, $params);
        }else{
            $url .= '?' . $params;
        }
        
        // Warning: this is hardcoded url and has to be modified on linux
        $url = str_replace('C:\wnmp\php', '', $url);
        $url = 'localhost:8000/ausa/index-test.php' . $url;
        Shared::debug($url);
        $request = curl_init($url);
        
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            
        );
        
        if ($data != null){
            $options[CURLOPT_POST]=1;
            $options[CURLOPT_POSTFIELDS]='json=' . json_encode($data);
            //$options[CURLOPT_HTTPHEADER]='Content-Type: application/json';
        }
        
        curl_setopt_array( $request, $options );
        $result =  curl_exec($request); 
        Shared::debug($result);
        return json_decode($result, true);
    }
    
    /**
     * Just test that we can get list of all chapters user has vendors in
     */
    public function testGetUserChapters() {
        Fixtures::getVendor(2);
        Fixtures::getVendor(3);
        Fixtures::getVendor(4);
        
        $user = Fixtures::getUser('vendorUser3');
        $chapters = $user->getAccessibleChapterIds();
        Shared::debug($chapters);
        $this->assertTrue(count($chapters) > 1, 'could not find enough chapters');
    }
    
    /**
     * Load only updated records
     */
    public function testApiGetDataByUser(){
        Fixtures::cleanup();
        // load some data first
        Fixtures::getVendor(1);
        Fixtures::getVendor(2);
        Fixtures::getVendor(3);
        Fixtures::getVendor(4);
        Fixtures::getVendor(5);
        Fixtures::getVendor(6);
        
        Fixtures::getBanner(1);
        Fixtures::getBanner(2);
        Fixtures::getBanner(3);
        Fixtures::getBanner(4);
        
        Fixtures::getOffer(1);
        Fixtures::getOffer(2);
        Fixtures::getOffer(3);
        Fixtures::getOffer(4);
        Fixtures::getOffer(5);
        
        $chapter = Fixtures::getChapter(1);
        $user = Fixtures::getUser('vendorUser3');
        $user->getInfoForApi($chapter);
        
        $this->assertNotNull($user->access_token);
        $response = $this->getApiResponse('get-data', 'a=' . $user->access_token . '&ch=' . $chapter->chapter_id);
        Shared::debug($response);
        
        // just a simple check that something was returned
        // later on we have to check that the structure match the documentation
       /* $this->assertTrue(isset($response['categories'][0]));
        $this->assertTrue(isset($response['banners'][0]));
        $this->assertTrue(isset($response['offers'][0]));
        $this->assertTrue(isset($response['vendors'][0]));*/
    }
}
?>

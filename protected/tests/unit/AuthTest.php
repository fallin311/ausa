<?php

class AuthTest extends CTestCase {
    
    
    function testAdminAccess(){
        // create admin first, this function logs in as an admin user
        Fixtures::getAdminUser();

        // lets login as a new user
        Fixtures::loginAs('testAdmin');
        $this->assertTrue(app()->user->isAdmin(), 'Admin was created and logged in');
        $this->assertEquals(Fixtures::$users['testAdmin']['email_address'], Yii::app()->user->getUser()->email_address);
        
        // test that non primary admin can create stuff
        $chapter = Fixtures::getChapter(1);
        $this->assertNotNull($chapter, 'chapter was found or created');
        
        //$chapterId = $chapter->chapter_id;
        //$chapter->delete();
        //$chapter2 = Chapter::model()->findByPk($chapterId);
        //$this->assertNull($chapter2, 'chapter was deleted');
        
        // create it again
        //$chapter3 = Fixtures::getChapter(1);
        //$this->assertNotNull($chapter3);
        
        Fixtures::logout();
        $this->assertTrue(app()->user->isGuest(), 'Admin was logged out');
    }
    
    function testVendorAccess(){
        Fixtures::cleanup();
        // admin creates vendor
        Fixtures::getAdminUser();
        Fixtures::loginAs('testAdmin');
        $vendor = Fixtures::getVendor(1);
        $this->assertNotNull($vendor);
        
        // was the user object created
        $user = Fixtures::getUser('vendorUser1');
        $this->assertNotNull($user);
        
        // was the access object created
        $userVendor = UserVendor::model()->findByAttributes(array('user_id' => $user->user_id, 'vendor_id' => $vendor->vendor_id));
        $this->assertNotNull($userVendor);
        
        // lets switch the user
        Fixtures::loginAs('vendorUser1');
        
        // does the user have access?
        $this->assertTrue($vendor->hasVendorAccess());
        
        // lets load different user and check access as well
        Fixtures::getUser('vendorUser2');
        Fixtures::loginAs('vendorUser2');
        $this->assertFalse($vendor->hasVendorAccess());
    }
    
    function testChapterAccess(){
        //Fixtures::cleanup();
        Fixtures::getAdminUser();
        Fixtures::loginAs('testAdmin');
        $chapter = Fixtures::getChapter(1);
        $this->assertNotNull($chapter);
        
        $user = Fixtures::getUser('chapterUser1');
        $this->assertNotNull($user);
        
        // was the access object created
        $userChapter = UserChapter::model()->findByAttributes(array('user_id' => $user->user_id, 'chapter_id' => $chapter->chapter_id));
        $this->assertNotNull($userChapter);
        
        // get the chapter user
        Fixtures::loginAs('chapterUser1');
        $this->assertTrue(Auth::hasChapterAccess($chapter));
        
        Fixtures::getUser('chapterUser2');
        Fixtures::loginAs('chapterUser2');
        $this->assertFalse(Auth::hasChapterAccess($chapter));
        
        // try vendor user
        Fixtures::getUser('vendorUser2');
        Fixtures::loginAs('vendorUser2');
        $this->assertFalse(Auth::hasChapterAccess($chapter));
        
        // try admin
        Fixtures::getAdminUser();
        Fixtures::loginAs('testAdmin');
        $this->assertTrue(Auth::hasChapterAccess($chapter));
    }
    
    function testStAccess(){
        //Fixtures::cleanup(); // not cleanup means a problem

        Fixtures::getAdminUser();
        Fixtures::loginAs('testAdmin');
        $st = Fixtures::getSalesTeam(1);
        $this->assertNotNull($st);
        
        $user = Fixtures::getUser('stUser1');
        $this->assertNotNull($user);
        
        // was the access object created
        $userSt = UserSt::model()->findByAttributes(array('user_id' => $user->user_id, 'sales_team_id' => $st->sales_team_id));
        $this->assertNotNull($userSt);
        
        // get the chapter user
        Fixtures::loginAs('stUser1');
        $this->assertTrue(Auth::hasStAccess($st));
        
        Fixtures::getUser('chapterUser2');
        Fixtures::loginAs('chapterUser2');
        $this->assertFalse(Auth::hasStAccess($st));
  
        // try admin
        Fixtures::getAdminUser();
        Fixtures::loginAs('testAdmin');
        $this->assertTrue(Auth::hasStAccess($st));
    }
    
    /**
     * Test that users from ST have access to Vendor
     */
    function testStVendorAccess(){
        Fixtures::cleanup();
        Fixtures::getAdminUser();
        Fixtures::loginAs('testAdmin');
        $st = Fixtures::getSalesTeam(1);
        $this->assertNotNull($st);
        
        // this user should have access to vendor1 and vendor4
        $user = Fixtures::getUser('stUser1');
        $this->assertNotNull($user);
        
        $vendor1 = Fixtures::getVendor(1);
        $vendor2 = Fixtures::getVendor(2);
        $vendor3 = Fixtures::getVendor(3);
        $vendor4 = Fixtures::getVendor(4);
        
        // was the access object created
        $userSt = StVendor::model()->findByAttributes(array('vendor_id' => $vendor4->vendor_id, 'sales_team_id' => $st->sales_team_id));
        $this->assertNotNull($userSt);

        Fixtures::loginAs('stUser1');
        
        $this->assertTrue($vendor1->hasVendorAccess());
        $this->assertFalse($vendor2->hasVendorAccess());
        $this->assertFalse($vendor3->hasVendorAccess());
        $this->assertTrue($vendor4->hasVendorAccess());
    }
}
?>

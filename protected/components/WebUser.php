<?php

/**
 * User object accessible through Yii::app()->user
 */
class WebUser extends CWebUser {

    // this is pretty ugly way to do it (the value might be set directly from here
    // and from user object while initializing cache
    public $_user;
    private $_active = array();

    /**
     * Is it inradius administrator? Should this guy have full rights?
     * @return type
     */
    function isAdmin() {
        return (!$this->isGuest() && $this->getUser()->super_admin );
    }

    /**
     * Is it part of sales team
     * @return boolean
     */
    public function isSalesman() {
        if ($this->getActiveSt()) {
            return true;
        }
        return (!$this->isGuest() && $this->user->isSalesman() );
    }

    /**
     * Is the current logged in user Vendor? This might return yes if the
     * user is salesman
     * @return type
     */
    public function isVendor($notSalesman = false) {
        if ($this->getActiveVendor()) {
            if ($notSalesman) {
                if ($this->getActiveSt() || $this->isAdmin()) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return (!$this->isGuest() && $this->user->isVendor() );
    }

    public function isChapterUser() {
        if ($this->getActiveChapter()) {
            return true;
        }
        return (!$this->isGuest() && $this->user->isChapterUser() );
    }

    /**
     * In other words, do we have anonymous user?
     */
    public function isGuest() {
        return $this->isGuest;
    }

    /**
     * Returns user object from database
     * @return type 
     */
    function getUser() {
        if ($this->isGuest)
            return false;
        if ($this->_user === null) {
            // check the cache first
            $this->_user = User::findInCache($this->id);
        }
        return $this->_user;
    }
    
    /**
     * All users should have an entity set once logged in. Return that entity number.
     * @return type
     */
    function getEntity() {
        return Entity::model()->getEntity();
    }

    /**
     * Returns an array with active role and company id
     * @return array('type', 'class', 'id')
     */
    public function getActiveRole($loadModel = false) {
        $role = array(
            'type' => 'guest'
        );
        if ($this->getActiveVendor()) {
            $role['type'] = 'vendor';
            $role['class'] = 'Vendor';
            $role['action'] = 'vendor';
            $role['id'] = $this->getActiveVendor();
            if ($loadModel) {
                $role['model'] = Vendor::model()->findByPk($role['id']);
            }
        } else if ($this->getActiveSt()) {
            $role['type'] = 'st';
            $role['class'] = 'SalesTeam';
            $role['action'] = 'salesTeam';
            $role['id'] = $this->getActiveSt();
            if ($loadModel) {
                $role['model'] = SalesTeam::model()->findByPk($role['id']);
            }
        } else if ($this->getActiveChapter()) {
            $role['type'] = 'chapter';
            $role['class'] = 'Chapter';
            $role['action'] = 'chapter';
            $role['id'] = $this->getActiveChapter();
            if ($loadModel) {
                $role['model'] = Chapter::model()->findByPk($role['id']);
            }
        } else if ($this->isAdmin()) {
            $role['type'] = 'admin';
            $role['action'] = 'user';
            $role['id'] = null;
        }
        return $role;
    }

    public function getActiveVendor() {
        if (!isset($this->_active['vendor'])) {
            // load from session
            // not sure if it is a security hole, can we forge session state?
            $active = Yii::app()->user->getState('activeVendor');
            if ($active) {
                $this->_active['vendor'] = $active;
            } else {
                $this->_active['vendor'] = false;
            }
        }
        return $this->_active['vendor'];
    }

    public function getActiveChapter() {
        if (!isset($this->_active['chapter'])) {
            // load from session
            // not sure if it is a security hole, can we forge session state?
            $active = Yii::app()->user->getState('activeChapter');
            if ($active) {
                $this->_active['chapter'] = $active;
            } else {
                $this->_active['chapter'] = false;
            }
        }
        return $this->_active['chapter'];
    }

    public function getActiveSt() {
        if (!isset($this->_active['st'])) {
            // load from session
            // not sure if it is a security hole, can we forge session state?
            $active = Yii::app()->user->getState('activeSt');
            if ($active) {
                $this->_active['st'] = $active;
            } else {
                $this->_active['st'] = false;
            }
        }
        return $this->_active['st'];
    }

    
    
    
    /**
     * Set active vendor we are working with. It allows us faster permission check
     * and display vendor name in main menu
     * @param type $id 
     */
    public function setActiveVendor($id) {
        // verify access first
        $all = $this->getUser()->getVendorIds();
        if (in_array($id, $all) || app()->user->isAdmin()) {
            Yii::app()->user->setState('activeVendor', $id);
            $this->switchRole('vendor', $id);
            return true;
        }
        return false;
    }

    public function setActiveSt($id) {
        // verify access first
        $all = $this->getUser()->getStIds();
        //Shared::debug($all);
        if (in_array($id, $all) || app()->user->isAdmin()) {
            Yii::app()->user->setState('activeSt', $id);
            $this->switchRole('st', $id);
            return true;
        }
        return false;
    }

    public function setActiveChapter($id) {
        // verify access first
        $all = $this->getUser()->getChapterIds();
        if (in_array($id, $all) || app()->user->isAdmin()) {
            Yii::app()->user->setState('activeChapter', $id);
            $this->switchRole('chapter', $id);
            return true;
        }
        return false;
    }

    /**
     * Unset all extra active roles and keep only the default role.
     */
    public function setHomeRole() {
        $change = false;
        if ($this->isAdmin()) {
            if (app()->user->getState('activeChapter') != null) {
                app()->user->setState('activeChapter', null);
                $change = true;
            }
            if (app()->user->getState('activeVendor') != null) {
                app()->user->setState('activeVendor', null);
                $change = true;
            }
            if (app()->user->getState('activeSt') != null) {
                app()->user->setState('activeSt', null);
                $change = true;
            }
        } else if ($this->isSalesman()) {
            if (Yii::app()->user->getState('activeVendor') != null) {
                Yii::app()->user->setState('activeVendor', null);
                $change = true;
            }
        }
        
        // force user menu to change on the next request
        if ($change){
            $this->user->userMenu = null;
            $this->user->saveToCache();
        }
    }

    /**
     * Call this when the role is switched. It will reset menu info state
     */
    private function switchRole($type, $id = null) {
        // reset the menu icon and email
        //app()->user->setState('menuInfo', null);
        $this->user->userMenu = null;

        // log the history of roles
        if ($type == 'vendor') {
            $lastVendors = Yii::app()->user->getState('lastVendors');
            if ($lastVendors == null) {
                $lastVendors = array();
            }

            // check if the vendor is alrady there
            if (!isset($lastVendors[$id])) {
                $vendor = Vendor::model()->findByPk((int) $id);
                // add this one
                $newVendor = array(
                    'id' => $id,
                    'name' => $vendor->vendor_name,
                    'icon' => $vendor->getLoginLogo()
                );
                // first, limit it to max 5 vendors
                if (count($lastVendors) > 4) {
                    $lastVendors = array_slice($lastVendors, 0, 4, true);
                }
                $newVendors = array($id => $newVendor);
                foreach ($lastVendors as $vendorId => $vendor) {
                    $newVendors[$vendorId] = $vendor;
                }
                app()->user->setState('lastVendors', $newVendors);
            }
        }

        $this->user->saveToCache();
    }

    /**
     * Cache user menu object, which is data intensive. This function is called
     * every page reload, so we want to cache it.
     */
    public function getUserMenu() {
        Shared::debug("loading user menu");
        $menu = app()->cache->get('user-menu-' . $this->id);
        if ($menu === false) {
            Shared::debug("loading user menu from cache");
            $menu = $this->user->getUserMenu();
            app()->cache->set('user-menu-' . $this->id, $menu);
        } else {
            Shared::debug("found the user in cache");
        }
        return $menu;
    }

    /**
     * Destroy all the records stored in the session. This is mainly useful
     * for testing, where we need to switch a couple of users inside one session.
     * @param type $destroySession
     */
    public function logout($destroySession = true) {
        app()->cache->set('user-' . $this->id, false);
        parent::logout($destroySession);
        $this->_user = null;
        $this->_active = array();
    }

    public function login($identity, $duration = 0) {
        parent::login($identity, $duration);

        $user = $this->getUser();
        // switch the role, lookup in this order:
        // admin
        // sales team
        // vendor
        // chapter
        if ($user->isAdmin()) {
            // no active role
            //Shared::debug("it is admin!");
        } else if ($user->isSalesman()) {
            $ids = $user->getStIds();
            app()->user->setActiveSt($ids[0]);
            //Shared::debug("it is salesman!");
        } else if ($user->isVendor()) {
            $ids = $user->getVendorIds();
            app()->user->setActiveVendor($ids[0]);
            // Shared::debug("it is vendor!");
        } else if ($user->isChapterUser()) {
            $ids = $user->getChapterIds();
            app()->user->setActiveChapter($ids[0]);
            //Shared::debug("it is Chapter User!");
        }
    }

    /**
     * Redirect to dashboard based on a role
     */
    public function redirectHome() {
        app()->controller->redirect($this->getHomeUrl());
    }

    public function getHomeUrl() {
        if ($this->IsAdmin()) {
            return url('/admin/dashboard');
        } elseif ($this->IsSalesman()) {
            return url('/salesTeam/dashboard');
        } elseif ($this->IsChapterUser()) {
            return url('/chapter/dashboard');
        } elseif ($this->IsVendor()) {
            return url('/vendor/dashboard');
        } else {
            return url('/site/index');
        }
    }

    /**
     * Redirect to login screen, when user is not logged in.
     * Redirect to vendor select screen when user is admin or salesteam
     */
    public function loginRequired($flashVendor = false, $offer = false, $banner = false) {
        if ($this->isGuest()) {
            parent::loginRequired();
        } elseif ($this->isAdmin() || $this->isSalesman()) {
            if ($flashVendor) {
                // this helps to determine where to redirect the user later
                if($offer) {
                    app()->user->setFlash('setOfferVendor', 'Please select a vendor first.');
                } elseif($banner) {
                    app()->user->setFlash('setBannerVendor', 'Please select a vendor first.');
                } else {
                    app()->user->setFlash('setVendor', 'Please select a vendor first.');
                }
                app()->controller->redirect(url('/vendor/index'));
            }
        }
        // permission denied
        throw new CHttpException(403, 'You don\'t have permission to access this page.');
    }

}

?>

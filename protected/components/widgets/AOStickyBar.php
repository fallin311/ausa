<?php

/*
 * 
 * AUSA Offers Sticky Menu WIdget
 * This widget creates a wrapper for any content that fixes the css position when a user scrolls below the wrapper.
 * Javascript and non javascript version availible.
 * 
 * 11/10/2012 - Added panelWrap options so that the sticky menu can be wrapped with the AOPanel widget and AOPanelHead widget.
 * 
 * @author travis@inradiussystems.com
 * 
 */

class AOStickyBar extends CWidget {

    public $sticky = true;
    public $panelWrap = false;
    public $panelWrapHead = false;
    public $panelWrapHeadTitle = 'Title';

    public function init() {
        $baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AOStickyBar'), false, 1);
        if ($this->sticky) {
            if (!cs()->isCssFileRegistered($baseUrl . '/AOStickyBar.css') && !cs()->isScriptFileRegistered($baseUrl . '/AOStickyBar.js')) {
                $cs = cs();
                $cs->registerScriptFile($baseUrl . '/AOStickyBar.js', CClientScript::POS_HEAD);
                $cs->registerCssFile($baseUrl . '/AOStickyBar.css');
            }
        } else {
            if (!cs()->isCssFileRegistered($baseUrl . '/AOStickyBar.css')) {
                $cs = cs();
                $cs->registerCssFile($baseUrl . '/AOStickyBar.css');
            }
        }
        if($this->panelWrap){
            cs()->registerCss('panelWrapOverwrite',
                    '#ausa-stickyBarWrap {'
                        .'height: 40px;'
                        .'background-color: #FFF;'
                    .'}'
                    .'.ausa-contentMainInner {'
                        .'min-height: 40px;'
                    .'}');
            $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
            if($this->panelWrapHead){
                $this->widget('application.components.widgets.AOPanelHead', array('panelTitle'=>$this->panelWrapHeadTitle));
            }
        }
        echo '<div id="ausa-stickyBarWrap"' . (($this->panelWrap) ? ' class="no-head-foot"' : '') . ((!$this->sticky) ? ' class="no-js"' : '') . '>'
        . '<div id="ausa-stickyBar"' . ((!$this->sticky) ? ' style="position: fixed;" class="ausa-stickyBarVisible"' : '') . '>'
        . '<div id="ausa-stickyBarInner">';
    }

    public function run() {
        echo '</div>'
        . '</div>'
        . '</div>';
        if($this->panelWrap){
            $this->endWidget();
        }
    }

}

?>

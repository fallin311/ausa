<?php

/**
 * This is a work in progress. One header for all role types. - Travis
 */
class AOUserHeaderBar extends CWidget {

    public $breadcrumbs;
    public $map = false;
    public $dataHolder = ''; // dataHolder cannot be used if map is used
    private $_user;
    private $_currentRole;
    private $_defaultRole;
    private $_company;
    private $_chapter;
    private $_title;
    private $_logo;

    public function init() {
        if (app()->user->getUser()) {
            $this->_user = app()->user->getUser();
            $this->_defaultRole = $this->_user->getDefaultRole();
            $this->_currentRole = app()->user->getActiveRole();
            if (isset($this->_currentRole['class'])) {
                $class = $this->_currentRole['class'];
                $this->_company = $class::model()->findByPk($this->_currentRole['id']);
                $this->_title = $this->_company->getCompanyName();
                if ($this->_company != null) {
                    $this->_logo = $this->_company->getLoginLogo(true);
                }
            }
            if ($this->_logo == null) {
                $this->_logo = Image::getDefaultBase64IconLarge();
            }
            if(app()->user->isSalesman() && $this->_currentRole['class'] == 'SalesTeam'){
                // if st user, show their chapter (should cache this .. and other stuff in this widget)
                $this->_chapter = SalesTeam::model()->findByAttributes(array('sales_team_id' => $this->_currentRole['id']))->chapter_id;
                $this->_chapter = Chapter::model()->findByAttributes(array('chapter_id' => $this->_chapter))->chapter_name;
                $this->_title = $this->_title.' of the '.$this->_chapter.' Chapter';
            }
        }
    }

    public function run() {
        if (!app()->user->isGuest()) {
            $htmlSep = '</div>'
                . ($this->map ? '<div id="vendor-mainDashboard"><div id="map_holder" class="dashboardHighlight" style="padding: 0px; height: 307px; clear: both; overflow: hidden;"><div id="map_canvas" style="height: 100%;"></div></div></div>' : ($this->dataHolder != '' ? $this->dataHolder : '')) . '<div class="widget-content dashboard">';

            $this->render('_header', array('defaultRole' => $this->_defaultRole, 'currentRole' => $this->_currentRole, 'logo' => $this->_logo, 'user' => $this->_user, 'title' => $this->_title));
            $this->widget('bootstrap.widgets.TbBreadcrumbs', array('homeLink' => CHtml::link('Home', app()->homeUrl), 'links' => $this->breadcrumbs,));
            echo $htmlSep;
            $this->widget('application.components.widgets.AODashboard', $this->_user->getUserDashboard());
            $this->render('_footer', array());
        }
    }

}
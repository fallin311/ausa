<?php

class AOPanelHead extends CWidget {
    
    public $panelHeader = true;
    public $panelIcon = '';
    public $panelTitle = '';
    public $panelSettings = false;
    
    public function init() {
        $baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.ausaPanel.ausaPanelHead'), false, 1);
        $cs = cs();
        if (!cs()->isCssFileRegistered($baseUrl . '/ausaPanelHead.css')) {
            $cs->registerCssFile($baseUrl . '/ausaPanelHead.css');
        }
        if($this->panelIcon != ''){
            // Only load the icons if one is set. (The css file is large)
            $cs->registerCssFile($baseUrl . '/ausaIconsSprite.css');
        }
        echo '<div class="user-control">';
        if($this->panelTitle != ''){
            if($this->panelIcon){
                echo '<h5><i class="'.$this->panelIcon.'"></i> '.$this->panelTitle.'</h5>';
            } else {
                echo '<h5>'.$this->panelTitle.'</h5>';
            }
        }
    }
    
    public function run() {
        echo '</div>';
    }
    
}
?>

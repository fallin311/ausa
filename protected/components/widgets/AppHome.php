<?php

class AppHome extends CWidget {
    
    private $_entity;
    private $_assetsUrl;
    
    public function init() {
        $this->_entity = Entity::model()->findByPk(app()->user->getEntity());
        if(!is_null($this->_entity)) {
            $baseUrl = app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AppHome.'.$this->_entity->identifier.'AppHome'), false, 1);
            if (!cs()->isCssFileRegistered($baseUrl . '/appHome.css')) {
                cs()->registerCssFile($baseUrl . '/appHome.css', 'screen, projection');
            }
        } else {
            throw new CHttpException(401, 'Unauthorized Access &mdash; This host does not match any entity.');
        }
    }
    
    public function getAssetsUrl() {
        if ($this->_assetsUrl === null)
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AppHome.'.$this->_entity->identifier.'AppHome') );
        return $this->_assetsUrl;
    }
}
?>

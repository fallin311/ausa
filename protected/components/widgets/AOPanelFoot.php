<?php

class AOPanelFoot extends CWidget {
    
    public function init() {
        $baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.ausaPanel.ausaPanelFoot'), false, 1);
        if (!cs()->isCssFileRegistered($baseUrl . '/ausaPanelFoot.css')) {
            $cs = cs();
            $cs->registerCssFile($baseUrl . '/ausaPanelFoot.css');
        }
        echo '<div class="widget-bottom">';
    }
    
    public function run() {
        echo '</div>';
    }
    
}
?>

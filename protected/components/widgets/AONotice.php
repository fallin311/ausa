<?php

/*
 * 
 * To use, insert $this->widget('application.components.widgets.ausaNotice', array('notice' => 'Notice text','noticeType' => 'errors','noticeClosable' => ''));
 * 
 * noticeType: 'successfull', 'errors', 'info'
 * noticeClosable: true (default), false
 * 
 */

class AONotice extends CWidget {
    
    public $notice = '';
    public $noticeType = 'successfull';
    public $noticeClosable = true;
    public $noticeStyle = '';
    
    public function init() {
        $baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AONotice'), false, 1);
        if (!cs()->isCssFileRegistered($baseUrl . '/AONotice.css')&&!cs()->isScriptFileRegistered($baseUrl . '/AONotice.js')) {
            $cs = cs();
            $cs->registerScriptFile($baseUrl . '/AONotice.js', CClientScript::POS_HEAD);
            $cs->registerCssFile($baseUrl . '/AONotice.css');
        }
        echo '<div class="notification '. $this->noticeType . (($this->noticeClosable) ? " closable" : "") .'"'.(($this->noticeStyle)?' style="'.$this->noticeStyle.'"':'').'>' . $this->notice . '</div>';
    }
    
}
?>

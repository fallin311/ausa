<?php

/*
 * 
 * AUSA Offers Homepage Javascript Slider
 * This widget Builds the homepage slider.
 * 
 * @author travis@inradiussystems.com
 * 
 */

class AOHomepageSlider extends CWidget {
    
    public function init() {
        $baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AOHomepageSlider'), false, 1);
        if (!cs()->isCssFileRegistered($baseUrl . '/AOHomepageSlider.css')&&!cs()->isScriptFileRegistered($baseUrl . '/AOHomepageSlider.js')&&!cs()->isScriptFileRegistered($baseUrl . 'jquery.easing.1.3.js')) {
            $cs = cs();
            $cs->registerScriptFile(app()->assetManager->publish(Yii::getPathOfAlias('ausa.assets'), false, -1, YII_DEBUG) . '/js/jquery.easing.1.3.js');
            $cs->registerCssFile($baseUrl . '/AOHomepageSlider.css');
            $cs->registerScriptFile($baseUrl . '/AOHomepageSlider.js');
        }
    }
    
    public function run() {
        
    }
}
?>

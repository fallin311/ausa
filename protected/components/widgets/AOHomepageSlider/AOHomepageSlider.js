AOffers = function() {
    
    this.defaults = {
        'slider': {
            'selector': $('#ausa_slider'),
            'mainSelector': $('.slider', this.selector),
            'width': 1086,
            'height': 265,
            'delay': 3000,
            'openEasing': 'easeInOutBack',
            'closeEasing': 'easeInOutCubic',
            'easingDelay': 550,
            'callfn': function() {}
        }
    }
}

AOffers.prototype = {
    _init: function(o) {
        this._fn_sldr(o);
    },
    _fn_sldr: function(options) {
        var _self = this,
                op = $.extend({}, _self.defaults.slider, options),
                $t = op.selector,
                $tS = op.mainSelector,
                $SS = $('ul', $tS),
                $Sl = $('li', $SS),
                $sl = $Sl.length;
        _self._op = op;
        _self.img = $($t).children('img').css('opacity', 0);
        _self.Slider = $t;
        $tS.css({'height': op.height});
        _self.S_l = $Sl;
        _self.S_t = $sl;
        _self.S_w = $t.width();
        _self.S_h = $t.height();
        _self.S_o = true;
        _self.S_s = 0;
        _self.S_r = 0;
        _self.S_a = true;
        _self.TimeOut = '';

        if (_self.S_t > 0) {
            _self._prevnext();
        }



        $Sl.each(function(i) {

            var _t = $(this);

            _t.css({
                'display': 'block',
                'width': 1086
            });

            var _row = _t.children('.rows'),
                    _row_el = _row.children();

            _row.each(function(r) {
                var _re = $(this);

                _re.css({
                    'left': '0px',
                    'width': $.browser.safari ? _re[0].clientWidth : _re[0].clientWidth - 1 + 'px',
                    'height': _re[0].clientHeight + 'px',
                    'position': 'relative',
                    'overflow': 'hidden'
                });

            });

            _row_el.each(function(i) {

                var __t = $(this),
                        _tp = __t.position();

                __t.data('anim_position', {

                    'stop': _tp.top,
                    'sleft': _tp.left

                }).css({
                    'opacity': '0.0',
                    'position': 'relative',
                    'visibility': 'hidden'

                });

                if (i == _row_el.length - 1) {

                    _self._anim_open();

                }
                __t.css({'top': _tp.top + 'px', 'left': (i % 2 == 0) ? '-100px' : '100px'});
            });

        });

    },
    _anim_open: function() {

        var _self = this;
        action = _self.S_l.eq(_self.S_s),
                action_child = $('.rows', action),
                a_c = action_child.children(),
                a_l = a_c.length,
                a_s = 0;

        _self.S_a = false;
        _self.S_r = _self.S_s;

        action.show().siblings('li').hide();

        a_c.each(function(x) {

            var _tc = $(this);

            _tc
                    .css({'visibility': 'visible', 'position': 'absolute'})
                    .delay(x * 200)
                    .animate({
                left: _tc.data('anim_position').sleft,
                top: _tc.data('anim_position').stop,
                opacity: '1.0'
            },
                    {
                        duration: _self._op.easingDelay,
                        easing: _self._op.openEasing,
                        complete: function() {

                            if (++a_s == a_l) {

                                _self.S_s = (_self.S_s == _self.S_t - 1) ? 0 : _self.S_s + 1;
                                _self.S_a = true;
                                clearTimeout(_self.TimeOut);

                                _self.TimeOut = setTimeout(function() {

                                    _self._anim_close();

                                }, _self._op.delay);

                            }

                        }
                    }
            );

        });

    },
    _anim_close: function() {

        var _self = this,
                action = _self.S_l.eq(_self.S_r),
                action_child = $('.rows', action),
                a_c = action_child.children(),
                a_l = a_c.length,
                a_s = 0,
                a_r = 'margin-right margin-left opacity'.split(' ');

        _self.S_a = false;

        a_c.each(function(x) {

            var _tc = $(this),
                    a_t = a_r[ Math.floor(Math.random() * a_r.length) ],
                    a_t_n = {};

            switch (a_t) {

                case 'margin-left' :
                    a_t_n = {

                        left: '-100px',
                        "opacity": '0.0'

                    }
                    break;
                case 'margin-right' :
                    a_t_n = {

                        left: '100px',
                        "opacity": '0.0'

                    }
                    break;
                case 'opacity' :
                    a_t_n = {

                        "opacity": '0.0'

                    }

            }

            _tc
                    .delay(x * 200)
                    .animate(a_t_n,
                    {
                        duration: _self._op.easingDelay,
                        easing: _self._op.closeEasing,
                        complete: function() {

                            if (++a_s == a_l) {

                                _self._anim_open();

                            }

                        }
                    }
            );

        });

    },
    _prevnext: function() {

        var _self = this,
                p_n = $('<div />', {

        'class' : 'sn',
                'html' : '<span class="prev"></span><span class="next"></span>'

        });

        $('span', p_n).bind('click', function() {

            var _t = $(this);

            if (_self.S_a === true) {

                clearTimeout(_self.TimeOut);
                _self._anim_close();

                if (_t.is('.prev')) {

                    _self.S_s = _self.S_r == 0 ? _self.S_t - 1 : _self.S_r - 1;

                }

            }

        });

        // mouseover stop animation
        _self.Slider.bind({
            'mousemove': function() {
                clearTimeout(_self.TimeOut);
            },
            'mouseleave': function() {
                _self.TimeOut = setTimeout(function() {
                    _self._anim_close();
                }, _self._op.delay);
            }
        });

    },
    _toggle: function() {

        var _self = this,
                _ta = $('.toggle');

        _ta.each(function(i, v) {

            var _toggle = $(this),
                    _toggleTitle = _toggle.children('h3'),
                    _toggleContent = _toggle.children('div');

            _toggleTitle.addClass('toggle-title').append('<span />');
            _toggleContent.addClass('toggle-content');

            _toggleTitle.each(function() {

                var _toggleLink = $(this);

                _toggleLink.bind('click', function() {

                    var _toggleContentActive = $(this).next('div.toggle-content');

                    $(this).toggleClass('toggle-title-active');
                    $('span', this).toggleClass('negative');
                    _toggleContentActive.slideToggle(500);

                });

                if (_toggleLink.data('value') == 'active') {

                    _toggleLink.trigger('click').removeAttr('data-value');

                }

            });

        });


    },
    _c: function() {
        if (!$.browser.msie) {

            console.log(arguments);

        }

    }
}

$(window).load(function() {
    var n = new AOffers();
    n._init();
});

(function($) {
    $.fn.sorted = function(customOptions) {
        var options = {
            reversed: false,
            list: 4,
            by: function(a) {
                return a.text();
            }
        };
        $.extend(options, customOptions);
        $data = $(this);
        arr = $data.get();
        arr.sort(function(a, b) {
            var valA = options.by($(a));
            var valB = options.by($(b));
            if (options.reversed) {
                return (valA < valB) ? 1 : (valA > valB) ? -1 : 0;
            }
            else {
                return (valA < valB) ? -1 : (valA > valB) ? 1 : 0;
            }

        });
        if (options.list != 1) {
            var x = 1;
            $.each(arr, function(i, v) {
                if (x == options.list) {
                    $(v).addClass('last');
                    x = 0;
                }
                else {
                    $(v).removeClass('last')
                }
                x++;
            });
        }
        return $(arr);
    };
})(jQuery);
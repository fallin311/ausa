$(function () {
    var bgnum = 9;
    var buttons = [];
    var window_w = 0;
    function Guy(){
        this.w = 64;
        this.h = 96;
        this.x = 200;
        this.y = 4000;
        this.vy = 0;
        this.frame = 0;
        this.state = 0;
        this.delay = 0;
        this.flip = 0;
        this.drag = 0;
        this.div = $('#guy');
        this.target_x = 100;
        this.scroll_x = 0;

        this.mouse_x = 0;
        this.mouse_y = 0;

        this.STAND  = 0;
        this.FALL  = 1;
        this.SPLAT  = 2;
        this.WALK  = 3;
        this.DRAG  = 4;

        this.update = function(){
            if (this.y>0) {
                this.vy+=1;
                if (this.vy>32) this.vy=32;
                this.state = this.FALL;
            }
            if (this.drag) this.state=this.DRAG;
            if (this.state == this.FALL) {
                this.y-=this.vy;
                this.delay++;
                this.frame = ((this.delay&2)>>1)+13;
                if (this.y<=0) {
                    this.state = this.SPLAT;
                    this.delay = 50;
                    this.vy = 0;
                    this.y = 0;
                }
            } else if (this.state == this.STAND) {
                this.vy = 0;
                this.frame = 0;
                if (Math.abs(this.x-this.target_x)>=4) {
                    this.state=this.WALK;
                    this.delay=0;
                }
            } else if (this.state == this.SPLAT) {
                this.vy = 0;
                this.target_x = this.x;
                this.delay--;
                if (this.delay<=0) {
                    this.state = this.STAND;
                    this.delay=0;
                } else if (this.delay>45) {
                    this.frame = 15;
                } else {
                    this.frame = 16;
                }
            } else if (this.state == this.WALK) {
                this.vy = 0;
                this.delay+=2;
                if (this.delay>=40) this.delay=0;
                this.frame = Math.floor(this.delay/5)+1;
                var dir = 1;
                if (this.target_x<this.x) {
                    dir=-1;
                    this.flip=1;
                } else this.flip=0;
                this.x+=dir*6;
                if (Math.abs(this.x-this.target_x)<8) {
                    this.state=this.STAND;
                    this.delay=0;
                }
            } else if (this.state == this.DRAG) {
                this.vy = 0;
                this.delay++;
                this.frame = ((this.delay&2)>>1)+11;
                if (this.y<1) this.y=1;
            }
            this.div.css('background-position',(-this.frame*64).toString() + 'px '+(-this.flip*96).toString()+'px');
            this.div.css('bottom',(this.y+20).toString() + 'px');
            this.div.css('left',(this.x-32+this.scroll_x*bgnum).toString() + 'px');
        };
    };
    var guy = new Guy();
    $(document).click(function(e){
        guy.target_x = e.pageX-guy.scroll_x*bgnum;
    });
    $(document).mousedown(function(e){
        if( e.pageX > guy.x+guy.scroll_x*bgnum-20 &&
            e.pageX < guy.x+guy.scroll_x*bgnum+20 &&
            $(window).height() - e.pageY - 20 > guy.y &&
            $(window).height() - e.pageY - 80 < guy.y) guy.drag=true;
    });
    $(document).mouseup(function(e){
        guy.drag=false;
    });
    window.setInterval(function(){
        guy.update();
        if (window_w != $(window).width()) {
            window_w = $(window).width();
            var bw = 0;
            for (var c=0;c<buttons.length;c++) {
                buttons[c].css('left',((window_w-button_w)/2+bw).toString() + 'px');
                bw += buttons[c].width() + button_margin;
            }
        }
    },30);
});
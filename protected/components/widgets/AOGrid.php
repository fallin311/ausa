<?php

/*
 * This widget simply loads the required javascript necessary to implement the Wookmark style grid system.
 * You, however, must still surround elements with the appropriate html.
 * Travis Stroud
 */

class AOGrid extends CWidget {
    
    public function init() {
        $baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AOGrid'), false, 1);
        if (!cs()->isScriptFileRegistered($baseUrl . '/jquery.wookmark.min.js')) {
            $cs = cs();
            $cs->registerScriptFile($baseUrl . '/jquery.wookmark.min.js', CClientScript::POS_HEAD);
        }
		$cs->registerScript('ausa-wookmarkGrid',
		'
                function refreshWookmark(){
                var options = {
                    autoResize: true,
                    container: $("#ausa-contentMainTiles"),
                    offset: 0,
                    itemWidth: 585
                };
                var handler = $("#tiles li.grid");
                handler.wookmark(options);
                }
                refreshWookmark();');
    }
    
}
?>

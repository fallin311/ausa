<?php

class AOGMap extends CWidget {
    
    public $assets;
    public $markers;
    public $mapDivId = "map_canvas";
    
    public function init() {
        $this->assets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AOGMap'), false, 1);
        if (!cs()->isCssFileRegistered($this->assets . '/gmap.css')&&!cs()->isScriptFileRegistered($this->assets . '/gmap3.js')&&!cs()->isScriptFileRegistered($this->assets . '/infobox.min.js')) {
            cs()->registerScriptFile('https://maps-api-ssl.google.com/maps/api/js?v=3&sensor=false', CClientScript::POS_HEAD);
            cs()->registerScriptFile($this->assets . '/infobox.min.js', CClientScript::POS_HEAD);
            cs()->registerScriptFile($this->assets . '/gmap3.js', CClientScript::POS_HEAD);
            cs()->registerCssFile($this->assets . '/gmap.css');
        }
    }
    
    public function run() {
        $this->renderJavascript();
    }
    
    private function renderJavascript() {
        $script = <<<EOL
	var mapDiv,
        map,
        infobox,
        assets = "{$this->assets}";

        jQuery(document).ready(function($) {
            mapDiv = $("#{$this->mapDivId}");
            mapDiv.height(310).gmap3({
                map: {
                    options: {
                        "draggable": true
                        ,"mapTypeControl": true
                        ,"mapTypeControlOptions": {
                            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                        }
                        ,"mapTypeId": google.maps.MapTypeId.ROADMAP
                        ,"scrollwheel": false
                        ,"panControl": false
                        ,"rotateControl": false
                        ,"scaleControl": true
                        ,"streetViewControl": true
                        ,"zoomControlOptions": {
                            style: google.maps.ZoomControlStyle.LARGE,
                            position: google.maps.ControlPosition.LEFT_TOP
                        }
                    }
                }
                ,marker: {
                    values: {$this->markers},
                    options:{ draggable: false },
                    cluster:{
                        radius: 20,
                        0: {
                            content: "<div class='cluster cluster-1'>CLUSTER_COUNT</div>",
                            width: 90,
                            height: 80
                        },
                        20: {
                            content: "<div class='cluster cluster-2'>CLUSTER_COUNT</div>",
                            width: 90,
                            height: 80
                        },
                        50: {
                            content: "<div class='cluster cluster-3'>CLUSTER_COUNT</div>",
                            width: 90,
                            height: 80
                        },
                        events: {
                            click: function(cluster) {
                                map.panTo(cluster.main.getPosition());
                                map.setZoom(map.getZoom() + 2);
                            }
                        }
                    },
                    events: {
                        click: function(marker, event, context){
                            map.panTo(marker.getPosition());

                            infobox.setContent(context.data);
                            infobox.open(map,marker);

                            var iWidth = 260;
                            var iHeight = 300;
                            if((mapDiv.width() / 2) < iWidth ){
                                var offsetX = iWidth - (mapDiv.width() / 2);
                                map.panBy(offsetX,0);
                            }
                            if((mapDiv.height() / 2) < iHeight ){
                                var offsetY = -(iHeight - (mapDiv.height() / 2));
                                map.panBy(0,offsetY);
                            }
                        }
                    }
                }
            },"autofit");

            map = mapDiv.gmap3("get");
            infobox = new InfoBox({
                pixelOffset: new google.maps.Size(-50, -65),
                closeBoxURL: '',
                enableEventPropagation: true
            });
            mapDiv.delegate('.infoBox .infobox-close','click',function () {
                infobox.close();
            });
        });

EOL;
        cs()->registerScript('gmap3', $script, CClientScript::POS_BEGIN);
    }
    
}

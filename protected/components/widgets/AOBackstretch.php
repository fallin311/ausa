<?php

class AOBackstretch extends CWidget {
    
    public function init() {
        $baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AOBackstretch'), false, 1);
        if (!cs()->isScriptFileRegistered($baseUrl . '/jquery.backstretch.min.js')) {
            $cs = cs();
            $cs->registerScriptFile($baseUrl . '/jquery.backstretch.min.js', CClientScript::POS_END);
        }
		$cs->registerScript('ausa-background', '$.backstretch("'.$baseUrl.'/full.jpg");');
    }
    
}
?>

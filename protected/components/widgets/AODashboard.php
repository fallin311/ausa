<?php

/*
 * 
 * AUSA Offers Dashboard Icon Widget
 * This widget will build a clean set of icons in a typical dashboard format, customized to fit the AUSA Offers theme.
 * 
 * 25/9/2012 - Added ajax link compatability. Uses ajax, ajaxAction and ajaxUpdate within the items parameters to function.
 * 26/9/2012 - Added option for setting an items visibility
 * 26/9/2012 - Added json option for double div id updates from one ajax link (controller functions must be configured properly)
 * 
 * @author travis@inradiussystems.com
 * 
 */

class AODashboard extends CWidget {
    
    public $items = array();
    public $multiCol = true;
    
    
    public function init() {
        $baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AODashboard'), false, 1);
        if (!cs()->isCssFileRegistered($baseUrl . '/AODashboard.css')) {
            $cs = cs();
            $cs->registerCssFile($baseUrl . '/AODashboard.css');
        }
        $itemsCount = 0;
        echo '<ul id="ausa-dashboardMain" class="ausa-dashboard'.(($this->multiCol)?' multi':'').'">';
        foreach($this->items as $i => $item){
            if(isset($item['visible']) && !$item['visible']){
                unset($this->items[$i]);
                continue;
            }
            $itemsCount++;
            if(isset($item['ajax'])){
                echo '<li'.(($this->multiCol)?(($itemsCount %4 == 0)?' class="last"':''):(($itemsCount %8 == 0)?' class="last"':'')).'>'.CHtml::ajaxLink('<span>'.$item['text'].'</span>', array($item['ajaxAction']), array('update' => $item['ajaxUpdate']), array('class' => $item['icon'], 'id' => uniqid())).'</li>';
            } elseif(isset($item['json'])) {
                // CHtml::ajaxLink('link', array('controllerAction'), array('type' => 'POST', 'dataType' => 'json', 'success' => 'function(data){ $("#id1").html(data.id1); $("#id2").html(data.id2); }'), array(htmlOptions))
                echo '<li'.(($this->multiCol)?(($itemsCount %4 == 0)?' class="last"':''):(($itemsCount %8 == 0)?' class="last"':'')).'>'.CHtml::ajaxLink('<span>'.$item['text'].'</span>', array($item['jsonAction']), array('type'=>'POST', 'dataType'=>'json', 'success'=>'function(data) { $("#vendor-mainDashboard").html(data.dashboard); $("#vendor-contentDashboard").html(data.content); }'), array('class' => $item['icon'], 'id' => uniqid())).'</li>';
            } else {
                echo '<li'.(($this->multiCol)?(($itemsCount %4 == 0)?' class="last"':''):(($itemsCount %8 == 0)?' class="last"':'')).'><a href="'.$item['url'].'" class="'.$item['icon'].'"><span>'.$item['text'].'</span></a></li>';
            }
        }
    }
    
    public function run() {
        echo '</ul>';
    }
    
}
?>

<?php

class AO400 extends CWidget {
    
    public function init() {
        $baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AO400'), false, 1);
        if (!cs()->isCssFileRegistered($baseUrl . '/AO400.css')&&!cs()->isScriptFileRegistered($baseUrl . '/buffer-loader.js')) {
            $cs = cs();
            $cs->registerScriptFile($baseUrl . '/buffer-loader.js', CClientScript::POS_HEAD);
            $cs->registerCssFile($baseUrl . '/AO400.css');
        }
		$cs->registerScript('ausa-400Error', '$(".ausa-body").after("<div class=\"ausa-contentAboveFooter\"></div>");');
		$cs->registerScript('ausa-404ErrorAudio',
		'var context;
		 var bufferLoader;
		 
		 function init() {
			context = new webkitAudioContext();
			
			bufferLoader = new BufferLoader(
				context,
				["'.$baseUrl.'/music-loop-1.ogg",],
				finishedLoading
			);
			console.log(bufferLoader);
			bufferLoader.load();
		}
		
		function finishedLoading(bufferList) {
			var source = context.createBufferSource();
			source.buffer = bufferList[0];
			source.loop = true;
			source.connect(context.destination);
			source.noteOn(0);
		}
		init();');
    }
    
}
?>

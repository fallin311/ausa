var animationSpeed = 200;
$(function () {
    $('.closable').append('<span class="closelink" title="Close"></span>');
    $('.closelink').click(function() {
            $(this).parent().delay(animationSpeed).fadeTo("slow", 0.00, function(){
                    $(this).slideUp("slow", function() {
                            $(this).css({'display': 'none'});
                    });
            });
    });
});
<?php

class AppLandingPage extends CWidget {
    
    private $_entity;
    private $_assetsUrl;
    
    public function init() {
        $this->_entity = Entity::model()->findByPk(app()->user->getEntity());
        $baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AppLandingPage.'.$this->_entity->identifier.'LandingPage'), false, 1);
        if (!cs()->isCssFileRegistered($baseUrl . '/landingPage.css')&&!cs()->isScriptFileRegistered($baseUrl . '/landingPage.js')&&!cs()->isScriptFileRegistered($baseUrl . 'jquery.easing.1.3.js')) {
            cs()->registerScriptFile(app()->assetManager->publish(Yii::getPathOfAlias('ausa.assets'), false, -1, YII_DEBUG) . '/js/jquery.easing.1.3.js');
            cs()->registerCssFile($baseUrl . '/landingPage.css');
            cs()->registerScriptFile($baseUrl . '/landingPage.js');
        }
    }
    
    public function getAssetsUrl() {
        if ($this->_assetsUrl === null)
            $this->_assetsUrl = app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AppLandingPage.'.$this->_entity->identifier.'LandingPage') );
        return $this->_assetsUrl;
    }
}
?>

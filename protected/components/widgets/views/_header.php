<?php
cs()->registerScript('mobile-preview', '$(".mobile-preview").click(function(event){
    event.preventDefault();
    newwindow=window.open("' . url('/user/mobilePreview') . '","mobile preview","height=850,width=720");
    if (window.focus) {
        newwindow.focus();
    }
    this.hide();
    return false;
});');
?>
<div class="singleCol">
    <div class="ausa-contentMainInner">
        <div class="widget-content no-head-foot" style="padding: 0px;">
            <div class="ausa-salesTeamDashboard">
                <div class="header">
                    <img class="img-rounded dashboard" src="<?php echo $logo; ?>" alt="" />
                    <h1>
                        <span style="display: block; line-height: 35px;"><?php echo $user->first_name . ' ' . $user->last_name; ?> <small class="hidden-tablet hidden-phone" style="font-size: 17.5px; letter-spacing: 1px;"><?php echo $user->email_address; ?></small></span>
                        <span style="display: block; margin-top: -2px; line-height: 17px; font-size: 17px; font-weight: normal; color: #646464; letter-spacing: 5px; text-indent: 4px;"><?php echo $title; ?></span>
                    </h1>
                    <?php ($defaultRole == 'salesman' && $currentRole['type'] == 'vendor' ? '<a data-title="Vendor Dashboard" data-content="This is the dashboard for your vendor (vendor name).<br/><br/>
                           From here, you can configure settings and monitor data unique to this specific vendor." data-placement="left" rel="clickover" class="btn btn-large disabled st-vendor-helpPopover" data-original-title=""><i class="icon-question-sign"></i></a>' : '') ?>
                </div>
                <div class="breadcrumbs" style="height: 25px; line-height: 25px; border-top: #D2D2D2 1px solid; -webkit-box-shadow: inset 0 0 5px #DDD; -moz-box-shadow: inset 0 0 5px #dddddd; box-shadow: inset 0 0 5px #DDD;">
<?php

/*
 * Generates the price table html and css
 */

class AOPriceTable extends CWidget {
    
    public $planArray = array(null);
    
    // If planName and planPrice are not passed, they will be fetched from the planArray
    public $planName = '';
    public $planPrice = '';
    public $planPriceTerm = 'mo';
    public $planTag = false;
    public $planLink = '';
    
    public function init() {
        $baseUrl = app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AOPriceTable'), false, 1);
        if (!cs()->isCssFileRegistered($baseUrl . '/AOPriceTable.css')) {
            cs()->registerCssFile($baseUrl . '/AOPriceTable.css');
        }

        $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
        $html = ''
            .'<div class="widget-content no-head-foot" style="padding: 0px;">'
                .'<div class="ausa-salesTeamDashboard">'
                    .'<div id="vendor-mainDashboard">'
                        .'<div class="dashboardHighlight priceTable">'
                            .'<h5 class="plan-name">'.($this->planName?$this->planName:$this->planArray['plan_name']).'</h5>'
                            .'<div class="price-amount"><strong>$'.($this->planPrice?$this->planPrice:$this->planArray['monthly_recurring_price']).'</strong>'
                            .'<div class="duration">'.$this->planPriceTerm.'</div></div>';
                            if($this->planTag){
                                $html .= '<div class="popular"><span>Popular</span></div>';
                            }
                        $html .= '</div>'
                    .'</div>'
                    .'<div class="widget-content dashboard priceTable">';
                        $html .= '<table class="priceTable detail-view table table-striped table-condensed" id="yw1">'
                            .'<tbody>'
                            .'<tr class="odd"><td><span>'.$this->planArray['max_offers'].'</span> Active Offer'.($this->planArray['max_offers']==1?'':'s').'</td></tr>'
                            .($this->planArray['max_banners']>0?'<tr class="even"><td><span>'.$this->planArray['max_banners'].'</span> Active Banner'.($this->planArray['max_banners']==1?'':'s').'</td></tr>':'')
                            .($this->planArray['max_banners']>0?'<tr class="odd"><td><span>'.$this->planArray['max_branches'].'</span> Active Branch Locations</td></tr>':'<tr class="even"><td><span>'.$this->planArray['max_branches'].'</span> Active Branch Locations</td></tr>')
                            .($this->planArray['max_banners']>0?'<tr class="even"><td>Visibility within <span>'.$this->planArray['max_categories'].'</span> Categories</td></tr>':'<tr class="odd"><td>Visibility within <span>'.$this->planArray['max_categories'].'</span> Categories</td></tr>')
                            .($this->planArray['max_banners']>0?'<tr class="odd"><td><span>'.$this->planArray['offer_duration'].'</span> Month Offer Duration</td></tr>':'<tr class="even"><td><span>'.$this->planArray['offer_duration'].'</span> Month Offer Duration</td></tr>')
                            .($this->planArray['max_banners']>0&&$this->planArray['banner_duration']?'<tr class="even"><td><span>'.$this->planArray['banner_duration'].'</span> Month Banner Duration</td></tr>':'')
                            .($this->planArray['setup_fee']>0?'<tr class="even"><td><span>$'.$this->planArray['setup_fee'].'</span> One Time Setup Fee</td></tr>':'')
                            .'</tbody>'
                            .'</table>';
                    $html .= '</div>'
                    .'<div class="widget-content dashboard">';
                        $html .= $this->widget('bootstrap.widgets.TbButton', array('label'=>'Choose This Plan', 'type'=>'success', 'size'=>'large', 'block'=>true, 'url'=>$this->planLink), true);
                    $html .= '</div>'
                .'</div>'
            .'</div>';
        echo $html;
    }
    
    public function run() {
        $this->endWidget();
    }
    
}
?>

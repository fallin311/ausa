$(function() {
    var sticky_navigation_offset_top = $('#ausa-stickyBar').offset().top;
    var sticky_navigation = function(){
	var scroll_top = $(window).scrollTop();
	if (scroll_top > sticky_navigation_offset_top - 40) { 
		$('#ausa-stickyBar').addClass("ausa-stickyBarVisible").css({'position': 'fixed'});
	} else {
		$('#ausa-stickyBar').removeClass("ausa-stickyBarVisible").css({'position': 'relative'}); 
	}
    };
    sticky_navigation();
    $(window).scroll(function() {
	 sticky_navigation();
    });
});
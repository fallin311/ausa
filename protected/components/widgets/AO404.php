<?php

class AO404 extends CWidget {
    
    public function init() {
        $baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AO404'), false, 1);
        if (!cs()->isCssFileRegistered($baseUrl . '/AO404.css')&&!cs()->isScriptFileRegistered($baseUrl . '/AO404.js')) {
            $cs = cs();
            $cs->registerScriptFile($baseUrl . '/AO404.js', CClientScript::POS_HEAD);
            $cs->registerCssFile($baseUrl . '/AO404.css');
        }
        echo '<div id="guy" style="bottom: 2000px; left: -1000px;"></div>';
    }
    
}
?>

<?php

/*
 * 
 * To use, insert $this->beginWidget('application.components.widgets.ausaPanel', array('multiCol' => false));
 * 
 * Must close off with: $this->endWidget();
 * 
 * Parameters
 * multiCol: true (default), false
 * centerPanel: true, false (default)
 * 
 */

class AOPanel extends CWidget {
    
    public $multiCol = true;
    public $centerPanel = false;
    
    public function init() {
        $baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.ausaPanel.ausaPanel'), false, 1);
        if (!cs()->isCssFileRegistered($baseUrl . '/ausaPanel.css')) {
            $cs = cs();
            $cs->registerCssFile($baseUrl . '/ausaPanel.css');
        }
        if($this->multiCol){
            echo '<div class="ausa-contentMain multiCol'. (($this->centerPanel) ? " center" : "") .'">';
        } else {
            echo '<div class="ausa-contentMain">';
        }
        echo '<div class="ausa-contentMainInner">';
    }
    
    public function run() {
        echo '</div>'
        . '</div>';
    }
    
}
?>

<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
        // find user by email
        $user = User::model()->findByAttributes(array('email_address' => $this->username));

        // check email
        if (!is_object($user)) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            //Shared::debug("user is disabled - " . $this->username);
        }
        // user can be deleted
        else if ($user->login_disabled == 1) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }

        // Check the password
        else if ($user->password != sha1($user->salt . $this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else if (!Entity::model()->belongsToEntity($user)) {//does not belong to entity
            //Shared::debug("Active Entity: " . Entity::model()->getActiveEntity());
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {
            $this->errorCode = self::ERROR_NONE;
            if ($this->errorCode == self::ERROR_NONE) {
                $user->last_login = Shared::timeNow();
                $user->save();

                $this->_id = $user->user_id;
            }
        }

        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }

}
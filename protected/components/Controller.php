<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/full';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    
    /**
     * Used for rendering the header and footer in a view without adding a mess of separation html tags
     * @var type string
     * @author Travis Stroud <travis@inradiussystems.com>
     */
    public $heading = '';
    public $footer = true;

    /**
     * Redirect to no access apge
     */
    protected function accessDenied() {
        if (Yii::app()->user->isGuest()) {
            Yii::app()->user->loginRequired();
        } else {
            throw new CHttpException(403, 'Access Denied.');
        }
    }

    /**
     * Authenticate Admin users only.
     * @return type
     */
    protected function authAdminOnly() {
        if (app()->user->isGuest()) {
            Yii::app()->user->loginRequired();
        } else
        if (!app()->user->isAdmin()) {
            throw new CHttpException(403, 'Access Denied.');
        }
        return true;
    }
    
    /**
     * Force certain urls to go to HTTPS
     * @param type $filterChain
     */
    public function filterHttps( $filterChain ) {
        $filter = new HttpsFilter;
        $filter->filter( $filterChain );
    }
    
    public function filterEntity($filterChain) {
        $filter = new EntityFilter;
        $filter->filter( $filterChain );
    }

}
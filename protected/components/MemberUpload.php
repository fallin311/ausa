<?php

class MemberUpload {
    
    /** columns from ausa file are lowercased and spaces are removed
    /* key is database column name, value is file column name
     *
     * @var assocArray
     */
    static $ausaMapping = array(
        'lastname' => 'setLastName',
        'firstname' => 'setFirstName',
        'prefix' => 'prefix',
        'id' => 'ausa_id',
        'address1' => 'address1',
        'address2' => 'address2',
        'city' => 'city',
        'state' => 'state',
        'postalcode' => 'setZipcode',
        'countrycode' => 'country',
        'primaryphone' => 'setPhoneNumber',
        'primaryemail' => 'setEmail',
        // membership table
        'subchapter' => 'setSubchapter',
        'productcode' => 'setProductCode',
        'chapter' => 'setChapterCode',
        'joindate' => 'setJoinDate',
        'expires' => 'setExpirationDate'
    );
    
    /**
     * These columns are necessary for upload from ausa export
     * @var type 
     */
    static $ausaRequredHeaders = array(
        'firstname',
        'lastname',
        'primaryemail',
        'joindate',
        'expires',
        'chapter'
        
    );
    
    // set in a moment the upload is initiated
    public $headers;
    
    
    public function __construct($headers) {
        $this->headers = $headers;
        Yii::import('application.models.behaviors.UploadMemberBehavior');
    }
    
    /**
     * Reads headers from the uploaded CSV file and return following info
     * in associative array:
     * headers - lowercase without spaces
     * separator - comma or semicolumn
     * enclosure - special quotes around field values
     * valid - all the necessary columns are present
     * linecount - how many data lines are in the file (without header line)
     * @param type $filepath
     * @return boolean
     */
    public static function getCsvFileInfo($filepath) {
        $separator = ",";
        if (($handle = fopen($filepath, "r")) !== false) {
            $line = fgets($handle, 8000);
            // we need to find the right separator and value enclosure
            $pos = strpos($line, "\n");
            if ($pos) {
                $line = trim(substr($line, 0, $pos));
            }
            $countComma = count(explode(",", $line));
            $countSemi = count(explode(";", $line));
            if ($countSemi > $countComma)
                $separator = ";";
            $enclosure = null;
            if (substr($line, 0, 1) == '"')
                $enclosure = '"';
            if (substr($line, 0, 1) == "'")
                $enclosure = "'";

            $headers = explode($separator, $line);
            // the final headers array ($pos=>$header)
            foreach ($headers as &$header){
                $header = strtolower(trim(preg_replace('/\s+/', '',$header), '"\' '));
            }
            // check the matching columns, so we know if we need to call designate
            $valid = true;

            foreach (self::$ausaRequredHeaders as $required) {
                if (!in_array($required, $headers)) {
                    //Shared::debug($required . ' not found');
                    $valid = false;
                }
            }
            fclose($handle);

            // open it again without fixed line width to count how many lines are there
            $linecount = 0;
            $handle = fopen($filepath, "r");
            while (!feof($handle)) {
                $line = fgets($handle);
                $linecount++;
            }
            // header
            $linecount --;
            
            // last empty line
            if (strlen($line) < 2) $linecount --;
            
            return array(
                'headers' => $headers,
                'separator' => $separator,
                'enclosure' => $enclosure,
                'valid' => $valid,
                'linecount' => $linecount
            );
        }
        return false;
    }
    
    /**
     * update values in the model from CSV
     * @param Member $model
     * @param array $data CSV row
     * @return boolean
     */
    function setValuesFromUpload(&$model, $data){
        
        if (count($this->headers) != count($data)){
            Shared::debug(count($this->headers) . ' vs provided ' . count($data), 'match does not count');
            Shared::debug($data);
            return false;
        }

        $model->attachBehavior('uploadMember', new UploadMemberBehavior());

        // temporary object we will save later
        $model->getTempMembership();
        
        // lets go through the both arrays together to save some time
        foreach ($this->headers as $i => $header){
            //Shared::debug("processing $header");
            if (isset(self::$ausaMapping[$header])){
                $to = self::$ausaMapping[$header];
                if (substr($to, 0,3) == 'set'){
                    // call setter
                    $model->$to($data[$i]);
                    //call_user_func(array($member, $to));
                }else{
                    // no checking
                    $model->$to = $data[$i];
                }
            }else{
                //Shared::debug("cannot find mapping for $header");
            }
        }
        return true;
    }
}
?>

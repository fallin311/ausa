<?php

/**
 * Force the loading of the proper entity attributes.
 */
class EntityFilter extends CFilter {

    protected function preFilter($filterChain) {
        $entity_id = app()->user->getEntity();
        if (isset($entity_id)) {  //Entity::model()->getEntity() found no matching domain
            return true;
        } else {
            throw new CHttpException(401, 'Unauthorized Access &mdash; This host does not match any entity.');
        }
    }

}

?>
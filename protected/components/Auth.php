<?php

/**
 * Helpers to assign access for users to vendors, chapters and sales teams.
 * The Auth class is separated from User model because these functions are not
 * used that often and it reduces loaded code size.
 * Use WebUser (app()->user) to check the access permission. 
 */
class Auth {

    public static function addVendorAccess($vendor, $user) {
        // this might be also initiated from ST user ... TODO
        if (app()->user->isAdmin()) {
            $userVendor = UserVendor::create($user, $vendor);
            return $userVendor;
        }
        return null;
    }

    /**
     * Check user access to this resoure. Function compares vendor_id
     * in this object (if found) to member vendor id
     * @return boolean 
     */
    public static function hasVendorAccess($object) {
        if ($object != null && isset($object->vendor_id)) {
            //Shared::debug($object);
            if (app()->user->isAdmin()) {
                //Shared::debug("It is Admin. so returning true");
                return true;
            }
            $user = app()->user->getUser();
            // we don't want to authorize guests (TODO: in some cases guests might have read permission)
            if ($user === false) {
                //Shared::debug("NO user logged in.. so, returning false");
                return false;
            }
            if ($object->vendor_id === app()->user->getActiveVendor()) {
                //Shared::debug("Found ActiveVendor.. so, returning true");
                return true;
            }
            // ok, it is not the active one, so let us check the whole list
            $all = $user->getVendorIds();
            //Shared::debug($all);
            if (in_array($object->vendor_id, $all)) {
                //Shared::debug("Found user has access to vendor from the list of vendors available. retyring trye.");
                return true;
            }
        }
        Shared::debug("Nothing worked.. WTF?");

        return false;
    }

    public static function hasChapterAccess($object) {
        if ($object != null && isset($object->chapter_id)) {
            if (app()->user->isAdmin())
                return true;
            $user = app()->user->getUser();
            // we don't want to authorize guests (TODO: in some cases guests might have read permission)
            if ($user === false)
                return false;
            if ($object->chapter_id === app()->user->getActiveChapter()) {
                return true;
            }
            // ok, it is not the active one, so let us check the whole list
            $all = $user->getChapterIds();
            if (in_array($object->chapter_id, $all)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Does the current user have access to the object through sales team?
     * @param type $object any object with sales_team_id attribute
     * @return boolean
     */
    public static function hasStAccess($object) {
        if ($object != null && isset($object->sales_team_id)) {
            if (app()->user->isAdmin())
                return true;
            $user = app()->user->getUser();
            // we don't want to authorize guests (TODO: in some cases guests might have read permission)
            if ($user === false)
                return false;
            if ($object->sales_team_id === app()->user->getActiveSt()) {
                return true;
            }
            // ok, it is not the active one, so let us check the whole list
            $all = $user->getStIds();
            if (in_array($object->sales_team_id, $all)) {
                return true;
            }
        }
        return false;
    }

}

?>

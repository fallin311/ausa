<?php

/**
 * This class is supposed to send the system emails
 * 
 * we need:
 * send predefined email with placeholders
 * send custom email with placeholders
 * 
 */
class AOEmail {

    private static $templateCss = '
        body { background-color: #F3F4F4; padding: 5px; }
        .holder { margin-top: 20px; margin-bottom: 50px; border-color: gray; font-size: 13px; line-height: 18px; font-family: \'Lucida Grande\', Tahoma, Verdana, Arial, sans-serif; color: #333333; width: 90%; border-width: 1px; border-style: solid; padding: 5px; }
        .content-holder { width: 100%; }
        .main-content-holder { width: 100%; border: solid 1px #999999; }
        .center {text-align: center; margin-left: auto; margin-right: auto;}
        ul { padding-left: 0; }
        a:link { color:#046380; text-decoration:underline; }
        a:visited { color:#A7A37E; text-decoration:none; }
        a:hover { color:#002F2F; text-decoration:underline; }
        a:active { color:#046380; text-decoration:none; }
        p { font-family: arial, helvetica, sans-serif; font-size: 0.9em; color: #666666; }
        p.title { font-size: 2.2em; color: #351a05; margin: 0px; }
        p.subtitle { font-size: 1.4em; color: #351a05; margin: 0px; }
        td.data { padding-top: 5px; padding-bottom: 5px; padding-left: 10px; padding-right: 10px; }
        p.footer { text-align: center; font-size: 0.7em; line-height: 1.5em; }';
    private static $template = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<body>
		<table class="holder" align="center" bgcolor="#ecd9b1">
			<tbody><tr>
				<td colspan="2">
					<table class="content-holder">
						<tbody><tr>
							<td align="left" valign="top">
								<div style="font-family:Arial, Helvetica, sans-serif;">
									<table class="main-content-holder" cellpadding="0" cellspacing="10" bgcolor="#ffffff">
										<tbody><tr>
											<td align="left" valign="top">
												<table border="0" cellpadding="0" cellspacing="0" width="600" align="center">
													<tbody>
													[content]
													<tr>
														<td align="center" valign="top" style="padding-top:40px;border-collapse:collapse">
															<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-top:1px solid #cccccc">
																<tbody><tr>
																	<td valign="top" style="padding-top:40px;border-collapse:collapse">
																		<table border="0" cellpadding="0" cellspacing="0">
																			<tbody><tr>
																				<td colspan="2" valign="middle" style="padding-bottom:20px;border-collapse:collapse;color:#707070;font-family:Helvetica;font-size:11px;line-height:125%;text-align:left">
																					Limitations of Liability: Under no circumstances shall the company, <span class="il">AUSA</span> chapters, advertisers, vendors, product, offer, or service providers, and/or its licensors be liable for any direct, indirect, punitive, incidental, special, or consequential damages that result from, or arise out of the use of, or inability to use, this service, the information contained on or received through use of this app, the products or services listed on this app, or any services or products received through this service. Click here to read full <a href="https://ausaoffers.com/site/terms" style="color:#505050;font-weight:normal;text-decoration:underline" target="_blank">terms of service</a>.
																				</td>
																			</tr>
																			<tr>
																				<td valign="top" style="padding-right:20px;border-collapse:collapse;color:#707070;font-family:Helvetica;font-size:11px;line-height:125%;text-align:left">
																					<em>Copyright &copy; 2013 <span class="il">AUSA</span> Offers, All rights reserved.</em>
																					<br>
																					You are receiving this email because you are an <span class="il">AUSA</span> member and entitled to receive special offers from the local chapter.
																					<br>
																					<br>
																					<strong>Our mailing address is:</strong>
																					<br>
																					<div><span><span class="il">AUSA</span> Offers</span><div><div>Post Office Box 6062</div><span>Ft. Bliss</span>, <span>TX</span>  <span>79906</span></div><br><a href="http://ausaoffers.net/docs/Omar_Bradley_Chapter_Member_List.vcf" target="_blank">Add us to your address book</a></div>
																				</td>
																				<td valign="top" width="200" style="border-collapse:collapse;color:#707070;font-family:Helvetica;font-size:11px;line-height:125%;text-align:left"></td>
																			</tr>
																			<tr>
																				<td colspan="2" valign="middle" style="padding-top:20px;border-collapse:collapse;color:#707070;font-family:Helvetica;font-size:11px;line-height:125%;text-align:left"></td>
																			</tr>
																		</tbody></table>
																	</td>
																</tr>
															</tbody></table>
														</td>
													</tr>
												</tbody></table>
											</td>
										</tr></tbody>
									</table>
								</div>
							</td>
						</tr></tbody>
					</table>
				</td>
			</tr></tbody>
		</table>
	</body>
</html>';

    /**
     * Define all the possible messages here. Use subject, title and content strings
     * @var array
     */
    public static $messages = array(
        'siteContactUs' => array(
            'subject' => 'Communication via AUSA Offers Website',
            'title' => '[subject]',
            'sendToAll' => false,
            'content' => '<tr>
	<td align="center" valign="top" style="padding-top:20px;border-collapse:collapse">
		<table border="0" cellpadding="0" cellspacing="0" width="600">
			<tbody><tr>
				<td colspan="3" style="padding-bottom:40px;border-collapse:collapse;color:#303030;font-family:Helvetica;font-size:40px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle">
					<h3 style="text-align:center;color:#303030;display:block;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0">
						An Email from AUSA Offers Contact Form
					</h3>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" style="border-collapse:collapse">
					<p>Name: [name]</p>
					<p>Email: [senderEmail]</p>
					<p>Message: [body]</p>
				</td>
			</tr>
		</tbody></table>
	</td>
</tr>'
        ),
        'resetPassword' => array(
            'subject' => 'Password Reset - AUSA Offers',
            'title' => 'Password Reset',
            'content' => '<tr>
	<td align="center" valign="top" style="padding-top:20px;border-collapse:collapse">
		<table border="0" cellpadding="0" cellspacing="0" width="600">
			<tbody><tr>
				<td colspan="3" style="padding-bottom:40px;border-collapse:collapse;color:#303030;font-family:Helvetica;font-size:40px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle">
					<h3 style="text-align:center;color:#303030;display:block;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0">
						Reset your AUSA Offers Password
					</h3>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" style="border-collapse:collapse">
					<p>Dear [full_name],</p>
					<p>This email was sent automatically by AUSA Offers in response to your request to recover your password. Please click the link below to reset the password. If you didn\'t initiate a password reset, please disregard this email.</p>
					<p>[reset_link]</p>
				</td>
			</tr>
		</tbody></table>
	</td>
</tr>'
        ),
        'newPin' => array(
            'subject' => 'Your New PIN - AUSA Offers',
            'title' => 'New Login PIN',
            'content' => '<tr>
	<td align="center" valign="top" style="padding-top:20px;border-collapse:collapse">
		<table border="0" cellpadding="0" cellspacing="0" width="600">
			<tbody><tr>
				<td colspan="3" style="padding-bottom:40px;border-collapse:collapse;color:#303030;font-family:Helvetica;font-size:40px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle">
					<h3 style="text-align:center;color:#303030;display:block;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0">
						Your New AUSA Offers PIN
					</h3>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" style="border-collapse:collapse">
					<p> Dear [full_name], </p>
					<p>This email was sent automatically by AUSA Offers in response to your request to recover mobile application access PIN. Please see your login information below.</p>
					<table align="center" border="1" cellspacing="0" cellpadding="0" bordercolor="#999999">
						<tbody><tr>
							<td class="data">Email Address</td>
							<td class="data">[email_address]</td>
						</tr>
						<tr>
							<td class="data">AUSA Member ID</td>
							<td class="data">[ausa_id]</td>
						</tr>
						<tr>
							<td class="data">Phone Number</td>
							<td class="data">[phone_number]</td>
						</tr>
						<tr>
							<td class="data">Membership Expiration</td>
							<td class="data">[membership_expiration]</td>
						</tr>
						<tr>
							<td class="data">Access PIN</td>
							<td class="data">[pin]</td>
						</tr></tbody>
					</table>
					<p>Please use either email address, phone number or ausa member id to login into AUSA Offers application using pin [pin].</p>
				</td>
			</tr>
		</tbody></table>
	</td>
</tr>'
        ),
        // triggered when someone redeems an offer
        // sent to everybody, who is subscribed for it (vendor users)
        'offerRedemption' => array(
            'subject' => 'Offer Claimed - AUSA Offers',
            'title' => 'Offer Claimed',
            'content' => '<tr>
	<td align="center" valign="top" style="padding-top:20px;border-collapse:collapse">
		<table border="0" cellpadding="0" cellspacing="0" width="600">
			<tbody><tr>
				<td colspan="3" style="padding-bottom:40px;border-collapse:collapse;color:#303030;font-family:Helvetica;font-size:40px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle">
					<h3 style="text-align:center;color:#303030;display:block;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0">
						Your AUSA Offer Was Claimed
					</h3>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" style="border-collapse:collapse">
					<p>The offer <b>[offer_name]</b> has just been claimed by AUSA member <b>[member_name]</b> on <b>[redemption_date]</b>.</p>
					<p>Offer Details</p>
					<table align="center" border="1" cellspacing="1" cellpadding="0">
						<tbody><tr>
							<td>Publishing dates</td>
							<td>[start_date] - [end_date]</td>
						</tr>
						<tr>
							<td>Number of claims</td>
							<td>[redemption_count]</td>
						</tr>
						<tr>
							<td>Number of clicks</td>
							<td>[click_count]</td>
						</tr>
						<tr>
							<td>Number of views / downloads</td>
							<td>[impression_count]</td>
						</tr>
						<tr>
							<td>Offer Description</td>
							<td>[offer_description]</td>
						</tr></tbody>
					</table>
				</td>
			</tr>
		</tbody></table>
	</td>
</tr>'
        ),
        'emailVerification' => array(
            'subject' => 'Email Verification - AUSA Offers',
            'title' => 'Email Verification',
            'content' => '<tr>
	<td align="center" valign="top" style="padding-top:20px;border-collapse:collapse">
		<table border="0" cellpadding="0" cellspacing="0" width="600">
			<tbody><tr>
				<td colspan="3" style="padding-bottom:40px;border-collapse:collapse;color:#303030;font-family:Helvetica;font-size:40px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle">
					<h3 style="text-align:center;color:#303030;display:block;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0">
						AUSA Offers Account Verification
					</h3>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" style="border-collapse:collapse">
					<p>Thank you registering with AUSA Offers. Please click <a href="[verification_url]">here</a> or the link below to complete the email verification process.</p>
					<p><a href="[verification_url]">[verification_url]</a></p>
				</td>
			</tr>
		</tbody></table>
	</td>
</tr>'
        ),
        'vendorActivation' => array(
            'subject' => 'Activation Request',
            'title' => 'Vendor Activation Request',
            'content' => '<tr>
	<td align="center" valign="top" style="padding-top:20px;border-collapse:collapse">
		<table border="0" cellpadding="0" cellspacing="0" width="600">
			<tbody><tr>
				<td colspan="3" style="padding-bottom:40px;border-collapse:collapse;color:#303030;font-family:Helvetica;font-size:40px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle">
					<h3 style="text-align:center;color:#303030;display:block;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0">
						AUSA Offers Vendor Activation Request
					</h3>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" style="border-collapse:collapse">
					<p>Vendor [vendor_name] is waiting for your approval of his account.</p>
					<p>Visit his dashboard on <a href="[vendor_url]">[vendor_url]</a> and check his offer make sense and contact information.</p>
					<p>If everything is correct, go to account settings and enable trial subscription plan.</p>
				</td>
			</tr>
		</tbody></table>
	</td>
</tr>'
        ),
        'memberRegistration' => array(
            'subject' => 'Welcome to AUSA Offers',
            'title' => 'Member Registration',
            'content' => '<tr>
		<td align="center" valign="top" style="padding-top:20px;border-collapse:collapse">
			<table border="0" cellpadding="0" cellspacing="0" width="600">
				<tbody><tr>
					<td colspan="3" style="padding-bottom:40px;border-collapse:collapse;color:#303030;font-family:Helvetica;font-size:40px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle">
						<h3 style="text-align:center;color:#303030;display:block;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0">
							Welcome to <span class="il">AUSA</span> - Omar N. Bradley Chapter in Ft. Bliss, TX
						</h3>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" style="border-collapse:collapse">
						<table border="0" cellpadding="0" cellspacing="0">
							<tbody><tr>
								<td align="center" height="460" valign="top" width="235" style="border-collapse:collapse">
									<img src="http://ausaoffers.net/images/email_phone.png" height="460" width="235" style="display:block!important;border:0;min-height:auto;line-height:100%;outline:none;text-decoration:none">
								</td>
							</tr>
						</tbody></table>
					</td>
					<td width="40" style="border-collapse:collapse">
						<br>
					</td>
					<td valign="top" width="325" style="border-collapse:collapse">
						<table border="0" cellpadding="0" cellspacing="0">
							<tbody><tr>
								<td valign="top" width="72" style="padding-right:10px;border-collapse:collapse">
									<img src="http://ausaoffers.net/images/512_Android_72.png" alt="AUSA Offers Icon" border="0" width="72" height="72">
								</td>
								<td valign="bottom" style="border-collapse:collapse;color:#505050;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">
									<h2 style="color:#303030;display:block;font-family:Helvetica;font-size:30px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left">
										<span class="il">AUSA</span> Offers
									</h2>
									<h4 style="color:#303030;display:block;font-family:Helvetica;font-size:14px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left">
										Free Mobile App
									</h4>
								</td>
							</tr>
							<tr>
								<td colspan="2" valign="top" style="padding-top:20px;border-collapse:collapse;color:#505050;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">
									Hello [full_name],<br>
									We\'re excited to announce that <span class="il">AUSA</span> Offers,&nbsp;our super-simple app which lists offers (coupons) from local vendors in El Paso, is now available <strong>exclusively </strong>for <span class="il">AUSA</span> members on the <a href="https://itunes.apple.com/us/app/ausa-offers/id589070238?mt=8" style="color:#ea5d41;font-weight:normal;text-decoration:none" target="_blank">Apple App Store</a> and <a href="https://play.google.com/store/apps/details?id=com.ausaoffers.offers" style="color:#ea5d41;font-weight:normal;text-decoration:none" target="_blank">Google Play Android Market</a>. <span class="il">AUSA</span> Offers app lets you redeem offers at a vendor\'s location with ease and you can mark your favorite vendors and / or the offers they provide.<br>
									Download the app on your platform below and use the following credentials to login -&nbsp;<br>
									Email: <strong><a href="mailto:[email]" target="_blank">[email]</a></strong><br>
									Pin: <strong>[pin]</strong>
								</td>
							</tr>
							<tr>
								<td align="left" colspan="2" valign="top" style="padding-top:20px;border-collapse:collapse">
									<table border="0" cellpadding="0" cellspacing="0">
										<tbody><tr>
											<td align="left" valign="middle" style="border-collapse:collapse">
												<a href="https://itunes.apple.com/us/app/ausa-offers/id589070238?mt=8" target="_blank"><img align="none" alt="iOS Download" height="41" src="http://ausaoffers.net/images/apple_email_badge.png" style="width:120px;min-height:41px;border:0;line-height:100%;outline:none;text-decoration:none" width="120"></a>&nbsp;<a href="https://play.google.com/store/apps/details?id=com.ausaoffers.offers" target="_blank"><img align="none" alt="Android Download" height="41" src="http://ausaoffers.net/images/android_email_badge.png" style="width:120px;min-height:41px;border:0;line-height:100%;outline:none;text-decoration:none" width="120"></a><br>
											</td>
										</tr>
									</tbody></table>
								</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
		</td>
	</tr>
	<tr>
		<td align="center" valign="top" style="padding-top:40px;border-collapse:collapse">
			<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-top:1px solid #cccccc">
				<tbody><tr>
					<td valign="top" style="padding-top:40px;border-collapse:collapse">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody><tr>
								<td valign="top" width="180" style="border-collapse:collapse;color:#505050;font-family:Helvetica;font-size:13px;line-height:150%;text-align:left">
									<h3 style="color:#303030;display:block;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left">
										Can anyone use <span class="il">AUSA</span> Offers?
									</h3>
									<span class="il">AUSA</span> Offers is a free phone app available only for <span class="il">AUSA</span> Members of the Omar N. Bradley Chapter @ Ft. Bliss. Please share this information with your friends. Please visit <a href="http://ausaoffers.us7.list-manage.com/track/click?u=d330d81087eaa5b95435bc741&amp;id=c91cf77ae0&amp;e=53c6753537" style="color:#ea5d41;font-weight:normal;text-decoration:none" target="_blank"><span class="il">AUSA</span>.org</a>&nbsp;and join today to start enjoying amazing deals and offers from local vendors.
								</td>
								<td width="30" style="border-collapse:collapse">
									<br>
								</td>
								<td valign="top" width="180" style="border-collapse:collapse;color:#505050;font-family:Helvetica;font-size:13px;line-height:150%;text-align:left">
									<h3 style="color:#303030;display:block;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left">
										Features
									</h3>
									<ul>
                                        <li>Browse Offers in various categories</li>
                                        <li>Browse Vendor list</li>
                                        <li>Search for Offers</li>
                                        <li>Locate Offers on Map</li>
                                        <li>Mark Vendors and Offers as \'Favorite\'</li>
                                        <li>New Vendors and Offers are added frequently</li>
									</ul>
								</td>
								<td width="30" style="border-collapse:collapse">
									<br>
								</td>
								<td valign="top" width="180" style="border-collapse:collapse;color:#505050;font-family:Helvetica;font-size:13px;line-height:150%;text-align:left">
									<h3 style="color:#303030;display:block;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left">
										Vendors
									</h3>
									Enjoy deals from:
									<ul>
                                        <li>Cabo Joe\'s</li>
                                        <li>Westend Hair Co. &amp; Spa</li>
                                        <li>Tire Connection Tire Pros</li>
                                        <li>Sam\'s Club</li>
                                        <li>The Mattress Firm</li>
                                        <li>Italians Restaurant</li>
                                        <li>Bake Me Happy</li>
                                        <li>Accurate Collision Center</li>
                                        <li>Block Table and Tap</li>
									</ul>
								</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
				<tr>
					<td valign="top" style="border-collapse:collapse">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody><tr>
								<td colspan="5" valign="top" style="padding-top:40px;padding-bottom:20px;border-collapse:collapse;color:#505050;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">
									<table border="0" cellpadding="4" cellspacing="0" width="100%">
										<tbody><tr>
											<td style="border-collapse:collapse">
												<h3 style="color:#303030;display:block;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left">
													<span style="font-size:12px"><span><img align="none" alt="Refresh Button" height="30" src="http://ausaoffers.net/images/button1.png" style="width:40px;min-height:30px;line-height:100%;outline:none;text-decoration:none;display:inline;border:0" width="40">Click on the Refresh button on top right corner to load new offers</span></span><br>
													<br>
													App Screenshots:
												</h3>
											</td>
											<td style="border-collapse:collapse">
												<img alt="AUSA Logo" height="99" src="http://ausaoffers.net/images/AUSA_Logo.jpeg" width="99" style="border:0;min-height:auto;line-height:100%;outline:none;text-decoration:none;display:inline">
											</td>
										</tr>
									</tbody></table>
								</td>
							</tr>
							<tr>
								<td valign="top" width="180" style="border-collapse:collapse">
									<img src="http://ausaoffers.net/images/2013_07_26_18_35_58.png" alt="Categories View" border="0" width="180" height="320">
								</td>
								<td width="30" style="border-collapse:collapse">
									<br>
								</td>
								<td valign="top" width="180" style="border-collapse:collapse">
									<img src="http://ausaoffers.net/images/2013_07_26_18_36_10.png" alt="Vendor View" border="0" width="180" height="320">
								</td>
								<td width="30" style="border-collapse:collapse">
									<br>
								</td>
								<td valign="top" width="180" style="border-collapse:collapse">
									<img src="http://ausaoffers.net/images/2013_07_26_18_36_51.png" alt="Redeem Offer Screen" border="0" width="180" height="320">
								</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
		</td>
	</tr>'
        ),
        'memberRegistrationChapter' => array(
            'subject' => 'New Member Registration',
            'title' => 'Member Registration',
            'content' => '<tr>
	<td align="center" valign="top" style="padding-top:20px;border-collapse:collapse">
		<table border="0" cellpadding="0" cellspacing="0" width="600">
			<tbody><tr>
				<td colspan="3" style="padding-bottom:40px;border-collapse:collapse;color:#303030;font-family:Helvetica;font-size:40px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle">
					<h3 style="text-align:center;color:#303030;display:block;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0">
						Welcome to AUSA Offers
					</h3>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" style="border-collapse:collapse">
					<table width="800px" border="0" cellspacing="1" cellpadding="1">
						<tr>
							<td><img src="http://ausaoffers.net/images/ausa_logo.jpg" width="100" height="100" alt="AUSA Logo" /></td>
							<td>
								<h1>INDIVIDUAL MEMBERSHIP APPLICATION</h1>
								<p>2425 Wilson Blvd., Arlington, Virginia 22201</p>
								<p>855-246-6269 | 703-841-4300 | Fax: 703-841-7570</p>
							</td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table width="800px" border="1" cellspacing="0" cellpadding="4" bordercolor="#cccccc">
						<tr>
							<td>Rank/Title: <strong>[prefix]</strong></td>
							<td>First Name: <strong>[first_name]</strong></td>
							<td>Last Name: <strong>[last_name]</strong></td>
						</tr>
						<tr>
							<td colspan="3">Mailing Address: <strong>[full_address]</strong></td>
						</tr>
						<tr>
							<td>City: <strong>[city]</strong></td>
							<td>State: <strong>[state]</strong></td>
							<td>Zipcode: <strong>[zipcode]</strong></td>
						</tr>
						<tr>
							<td>Phone Number: <strong>[phone_number]</strong></td>
							<td colspan="2">Email: <strong>[email_address]</strong></td>
						</tr>
						<tr>
							<td>Birth Date: <strong>[birthday]</strong></td>
							<td colspan="2">&nbsp;</td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table width="800" border="1" cellspacing="0" cellpadding="4" bordercolor="#cccccc">
						<tr>
							<td>Chapter ID: <strong>[chapter_id]</strong></td>
							<td>Chapter Name: <strong>[chapter_name]</strong></td>
						</tr>
						<tr>
							<td>Army Status: <strong>[army_status]</strong></td>
							<td>Military Unit: <strong>[military_unit]</strong></td>
						</tr>
						<tr>
							<td colspan="2">Level: <strong>[price_level]</strong></td>
						</tr>
						<tr>
							<td>Term: <strong>1 Year</strong></td>
							<td>Price: <strong>[price]</strong></td>
						</tr>
						<tr>
							<td>Join Date: <strong>[join_date]</strong></td>
							<td>Expiration Date: <strong>[expiration_date]</strong></td>
						</tr>
						<tr>
							<td colspan="2">Payment Reference # <strong>[payment_reference_number]</strong></td>
						</tr>
					</table>
					<p>- Individual Membership Application created dynamically by AUSA OFFERS - </p>
				</td>
			</tr>
		</tbody></table>
	</td>
</tr>'
        ),
        'csvUploadComplete' => array(
            'subject' => 'AUSA Members Added',
            'title' => 'CSV Upload Has Been Processed',
            'content' => '<tr>
	<td align="center" valign="top" style="padding-top:20px;border-collapse:collapse">
		<table border="0" cellpadding="0" cellspacing="0" width="600">
			<tbody><tr>
				<td colspan="3" style="padding-bottom:40px;border-collapse:collapse;color:#303030;font-family:Helvetica;font-size:40px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle">
					<h3 style="text-align:center;color:#303030;display:block;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0">
						AUSA Offers Member CSV Processed
					</h3>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" style="border-collapse:collapse">
					<p>A recently uploaded CSV of AUSA member data to AUSA Offers has been processed. The members have either been added or updated. New members have been emailed instructions on how to use the app.</p>
				</td>
			</tr>
		</tbody></table>
	</td>
</tr>'
        ),
    );
    private $placeholders = array();
    public $extraPlaceholders;
    public $sender;
    public $senderName;
    public $subject;
    public $sendTo;
    public $sendToName;
    public $client;
    public $campaign;
    public $messageType;

    /** Send to all the people in the company */
    public $sendToAll = false;
    private $recipients = array();
    private $testRecipient = null;

    public function AOEmail($type) {
        //Shared::debug("new email type $type");
        if (!isset(self::$messages[$type]))
            return false;
        $this->messageType = $type;
        //Shared::debug($this->messageType);

        $this->sender = Yii::app()->params['email']['noReplySender'];
        $this->senderName = Yii::app()->params['email']['senderName'];
    }

    /**
     * Use in a case you need to overwrite the default sender
     * @param type $sender
     */
    public function setSender($sender, $senderName) {
        $this->sender = $sender;
        $this->senderName = $senderName;
    }

    public function setSubject($subject) {
        $this->subject = $subject;
    }

    /**
     * Overwrite test recipient settings from the configuration. Use this
     * option to ensure the email will not be sent to the original recipient
     * in a case you want to perfrom a test with your own email.
     * @param type $recipient
     */
    public function setTestRecipient($recipient) {
        $this->testRecipient = $recipient;
    }

    public function addRecipient($email, $name) {
        if (Shared::isEmailValid($email)) {
            $this->recipients[$email] = $name;
            return true;
        } else {
            return false;
        }
    }

    private function loadPlaceholders() {
        // we can preload extra placeholders about the vendore here

        if (is_array($this->extraPlaceholders)) {
            foreach ($this->extraPlaceholders as $key => $val) {
                $this->placeholders[$key] = $val;
            }
        }
    }

    /**
     * Add additional placeholders to the standart ones. This might be used
     * by certain types of emails 
     */
    public function addPlaceholders($newPlaceholders) {
        foreach ($newPlaceholders as $key => $value) {
            $this->placeholders[$key] = $value;
        }
    }

    public function send() {

        if (count($this->recipients)) {
            // some recipients were added through addRecipient function
            $this->sendTo = true;
            //$this->sendToAll = true;
        }

        $settings = self::$messages[$this->messageType];
        //Shared::debug($settings);
        if (isset($settings['sendToAdmin']) && $settings['sendToAdmin'] == true) {

            $this->addRecipient(Yii::app()->params['adminEmail'], 'AO Admin');
            $this->sendTo = true;
            //$this->sendToAll = true;
        }

        if ($this->sendTo) {
            $this->loadPlaceholders();

            foreach ($this->recipients as $email => $name) {
                //Shared::debug("sending to $email");
                $this->placeholders['email'] = $email;
                $this->placeholders['full_name'] = $name;

                // replace placeholders in a title placeholder
                $title = Shared::replacePlaceholders($settings['title'], $this->placeholders);
                $subject = Shared::replacePlaceholders($settings['subject'], $this->placeholders);

                // content of the email
                $message = Shared::replacePlaceholders($settings['content'], $this->placeholders);

                // construct the final message
                $body = Shared::replacePlaceholders(self::$template, array('title' => $title, 'content' => $message));
                $convertor = new CSSToInlineStyles($body, self::$templateCss);
                $htmlBody = $convertor->convert();

                // and send it out
                //Shared::debug($subject);
                //Shared::debug($htmlBody);
                //Shared::debug($email);
                //Shared::debug($name);
                //Shared::debug($this->sender);
                //Shared::debug($this->senderName);
                if (isset($settings['test']) || $this->testRecipient != null) {
                    // set only one recipient of this email during a testing stage
                    $test = $this->testRecipient == null ? $settings['test'] : $this->testRecipient;
                    Shared::sendEmail($subject, $htmlBody, $test, $test, $this->sender, $this->senderName);
                    return true;
                } else {
                    // send regular email
                    Shared::sendEmail($subject, $htmlBody, $email, $name, $this->sender, $this->senderName);
                }
            }
            return true;
        } else {
            Shared::debug("nobody to send this email to...");
            return false;
        }
    }

}

?>

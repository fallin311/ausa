<?php

/**
 * VAlidates that message, which is possibly displayed in phone is safe. That
 * means that the message will not break HTML layout of the page, it will not
 * execute any javascript and it will have only safe tags.
 */
class AOPhoneTextValidator extends CValidator {

    /**
     * Validates the attribute of the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object, $attribute) {
        $purifier = new CHtmlPurifier();
        $purifier->options = array(
            'CSS.Trusted' => false,
            'HTML.AllowedElements' => array(
                'p',
                'span',
                'a',
                'ul',
                'ol',
                'li',
                'img',
                'div',
                'b',
                'i',
                'strong',
                'br',
                'table',
                'tr',
                'td'
            ),
            'Attr.AllowedFrameTargets' => array("_blank")
        );
        $object->$attribute = $purifier->purify($object->$attribute);
    }

}


<?php

/**
 * Validates that offer has at least one category selected and no more than two.
 */
class AOCategoryValidator extends CValidator {

    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object, $attribute) {
        $value = $object->$attribute;

        if (!$object->published){
            Shared::debug($value, $attribute);
            if (!is_array($value) || count($value) == 0) {
                $message = $this->message !== null ? $this->message : Yii::t('yii', 'Select at least one category to make your offer visible in the cellphone app.');
                $this->addError($object, $attribute, $message);
                return false;
            }
            if (!is_array($value) || count($value) >2) {
                $message = $this->message !== null ? $this->message : Yii::t('yii', 'Please select TWO categories only.');
                $this->addError($object, $attribute, $message);
                return false;
            }
        }
        
        return true;
    }

}


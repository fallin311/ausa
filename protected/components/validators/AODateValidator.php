<?php

class AODateValidator extends CValidator {

    public $date_first;
    public $date_second;

    public function validateAttribute($model, $attribute) {

        $value = $model->$attribute;

        if ($attribute == 'start_date') {
            $this->date_first = Shared::toTimestamp($value);
        }

        if ($attribute == 'end_date') {
            $this->date_second = Shared::toTimestamp($value);
        }

        if ($this->date_first !== NULL && $this->date_second !== NULL) {
            
            if ($this->date_first > $this->date_second) {

                $model->addError($attribute, 'End Date needs to be after Start Date.');
            }else{
                // max lenght of the campaign is one year
                if ($this->date_second > ($this->date_first + Shared::month * 12)){
                    $model->addError($attribute, 'Maximal lenght of campaign is three months.');
                }
            }
        }
    }

}

?>

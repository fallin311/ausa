<?php
class AOClientScript extends CClientScript {

    public function renderOnRequest($loadFiles = true, $loadCss = true, $loadScripts = true) {

        $html = '';
        if ($loadCss) {
            if (count($this->cssFiles)) {
                $css = "";
                $i = 1;
                foreach ($this->cssFiles as $file => $params) {
                    $css .= 'var fileref' . $i . '=document.createElement("link");fileref' . $i . '.setAttribute("rel", "stylesheet");fileref' . $i . '.setAttribute("type", "text/css");fileref' . $i . '.setAttribute("href", "' . $file . '");';
                    $i++;
                }
                $html.=CHtml::script($css);
            }
        }

        if ($loadFiles) {
            foreach ($this->scriptFiles as $scriptFiles) {
                foreach ($scriptFiles as $scriptFile)
                    $html.=CHtml::scriptFile($scriptFile) . "\n";
            }
        }

        if ($loadScripts) {
            foreach ($this->scripts as $script)
                $html.=CHtml::script(implode("\n", $script)) . "\n";
        }

        if ($html !== '')
            return $html;
    }

}
?>

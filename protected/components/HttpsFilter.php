<?php

/**
 * Force certain actions to redirect to HTTPS schema.
 * example:
 * public function filters() {
 *       return array(
 *           'https +login',
 *       );
 *   }
 */
class HttpsFilter extends CFilter {

    protected function preFilter($filterChain) {
        if (!Yii::app()->params['useHttps']) {
            return true;
        }
        if (!Yii::app()->getRequest()->isSecureConnection) {
            # Redirect to the secure version of the page.
            $url = 'https://' .
                    Yii::app()->getRequest()->serverName .
                    Yii::app()->getRequest()->requestUri;
            Yii::app()->request->redirect($url);
            return false;
        }
        return true;
    }

}

?>

<?php

Yii::import('zii.widgets.CListView');

class AOListView extends CListView {

    public $templatesView = false; // AO Offer Templates List View

    public function registerClientScript() {
        $id = $this->getId();

        if ($this->ajaxUpdate === false)
            $ajaxUpdate = array();
        else
            $ajaxUpdate = array_unique(preg_split('/\s*,\s*/', $this->ajaxUpdate . ',' . $id, -1, PREG_SPLIT_NO_EMPTY));
        $options = array(
            'ajaxUpdate' => $ajaxUpdate,
            'ajaxVar' => $this->ajaxVar,
            'pagerClass' => $this->pagerCssClass,
            'loadingClass' => $this->loadingCssClass,
            'sorterClass' => $this->sorterCssClass,
            'enableHistory' => $this->enableHistory
        );
        if ($this->ajaxUrl !== null)
            $options['url'] = CHtml::normalizeUrl($this->ajaxUrl);
        if ($this->updateSelector !== null)
            $options['updateSelector'] = $this->updateSelector;
        if ($this->beforeAjaxUpdate !== null) {

            if (!($this->beforeAjaxUpdate instanceof CJavaScriptExpression) && strpos($this->beforeAjaxUpdate, 'js:') !== 0) {
                $options['beforeAjaxUpdate'] = new CJavaScriptExpression($this->beforeAjaxUpdate);
            } else {
                $options['beforeAjaxUpdate'] = $this->beforeAjaxUpdate;
            }
        }
        if ($this->afterAjaxUpdate !== null) {
            if (!($this->afterAjaxUpdate instanceof CJavaScriptExpression) && strpos($this->afterAjaxUpdate, 'js:') !== 0) {
                $options['beforeAjaxUpdate'] = new CJavaScriptExpression($this->afterAjaxUpdate);
            } else {
                $options['beforeAjaxUpdate'] = $this->afterAjaxUpdate;
            }
        }

        $options = CJavaScript::encode($options);
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerCoreScript('bbq');
        if ($this->enableHistory)
            $cs->registerCoreScript('history');
        $cs->registerScriptFile($this->baseScriptUrl . '/jquery.yiilistview.js', CClientScript::POS_END);
        $cs->registerScript(__CLASS__ . '#' . $id, "jQuery('#$id').yiiListView($options);");
        if ($this->templatesView) {
            $baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.AODashboard'), false, 1);
            $cs->registerCssFile($baseUrl . '/AODashboard.css');
        }
    }

    public function renderItems() {
        echo CHtml::openTag($this->itemsTagName,array('class'=>$this->itemsCssClass))."\n";
        $data=$this->dataProvider->getData();
        if(($n=count($data))>0) {
            $owner=$this->getOwner();
            $viewFile=$owner->getViewFile($this->itemView);
            echo '<div class="widget-content dashboard no-head-foot">';
            echo '<ul class="ausa-dashboard">';
            foreach($data as $i=>$item) {
                $data=$this->viewData;
                $data['index']=$i;
                $data['data']=$item;
                $data['widget']=$this;
                $owner->renderFile($viewFile,$data);
            }
            echo '</ul>';
            echo '</div>';
        } else
            $this->renderEmptyText();
        echo CHtml::closeTag($this->itemsTagName);
    }

}

?>

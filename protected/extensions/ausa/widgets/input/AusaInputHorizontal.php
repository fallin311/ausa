<?php

/**
 * TbInputHorizontal class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets.input
 */
Yii::import('bootstrap.widgets.input.BootInput');

/**
 * Bootstrap horizontal form input widget.
 * @since 0.9.8
 */
class AusaInputHorizontal extends AusaInput {
    /**
     * Runs the widget.
     */
    /* public function run() {
      echo CHtml::openTag('div', array('class' => 'control-group ' . $this->getContainerCssClass()));
      parent::run();
      echo '</div>';
      } */

    /**
     * Returns the label for this block.
     * @return string the label
     */
    /* protected function getLabel() {
      if (isset($this->labelOptions['class']))
      $this->labelOptions['class'] .= ' control-label';
      else
      $this->labelOptions['class'] = 'control-label';

      return parent::getLabel();
      } */

    /**
     * Renders a checkbox.
     * @return string the rendered content
     */
    protected function checkBox() {
        echo '<div class="form-row">';
        $attribute = $this->attribute;
        echo '<div class="form-element">';
        echo '<label class="checkbox" for="' . $this->getAttributeId($attribute) . '">';
        echo $this->form->checkBox($this->model, $attribute, $this->htmlOptions) . PHP_EOL;
        echo $this->model->getAttributeLabel($attribute);
        echo $this->getError() . $this->getHint();
        echo '</label></div></div>';
    }

    /**
     * Renders a list of checkboxes.
     * @return string the rendered content
     */
    protected function checkBoxList() {
        echo '<div class="form-row">';
        echo $this->getLabel();
        echo '<div class="form-element">';
        echo $this->form->checkBoxList($this->model, $this->attribute, $this->data, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo '</div></div>';
    }

    /**
     * Renders a list of inline checkboxes.
     * @return string the rendered content
     */
    protected function checkBoxListInline() {
        echo '<div class="form-row">';
        $this->htmlOptions['inline'] = true;
        $this->checkBoxList();
        echo '</div>';
    }

    /**
     * Renders a drop down list (select).
     * @return string the rendered content
     */
    protected function dropDownList() {
        echo '<div class="form-row">';
        echo $this->getLabel();
        echo '<div class="form-element">';
        echo $this->form->dropDownList($this->model, $this->attribute, $this->data, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo '</div></div>';
    }

    /**
     * Renders a file field.
     * @return string the rendered content
     */
    protected function fileField() {
        echo '<div class="form-row">';
        echo $this->getLabel();
        echo '<div class="form-element">';
        echo $this->form->fileField($this->model, $this->attribute, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo '</div></div>';
    }

    /**
     * Renders a password field.
     * @return string the rendered content
     */
    protected function passwordField() {
        echo '<div class="form-row">';
        echo $this->getLabel();
        echo '<div class="form-element">';
        echo $this->getPrepend();
        echo $this->form->passwordField($this->model, $this->attribute, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo $this->getAppend();
        echo '</div></div>';
    }

    /**
     * Renders a radio button.
     * @return string the rendered content
     */
    protected function radioButton() {
        echo '<div class="form-row">';
        $attribute = $this->attribute;
        echo '<div class="form-element">';
        echo '<label class="radio" for="' . $this->getAttributeId($attribute) . '">';
        echo $this->form->radioButton($this->model, $attribute, $this->htmlOptions) . PHP_EOL;
        echo $this->model->getAttributeLabel($attribute);
        echo $this->getError() . $this->getHint();
        echo '</label></div></div>';
    }

    /**
     * Renders a list of radio buttons.
     * @return string the rendered content
     */
    protected function radioButtonList() {
        echo '<div class="form-row">';
        echo $this->getLabel();
        echo '<div class="form-element">';
        echo $this->form->radioButtonList($this->model, $this->attribute, $this->data, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo '</div></div>';
    }

    /**
     * Renders a list of inline radio buttons.
     * @return string the rendered content
     */
    protected function radioButtonListInline() {
        echo '<div class="form-row">';
        $this->htmlOptions['inline'] = true;
        $this->radioButtonList();
        echo '</div>';
    }

    /**
     * Renders a textarea.
     * @return string the rendered content
     */
    protected function textArea() {
        echo '<div class="form-row">';
        echo $this->getLabel();
        echo '<div class="form-element">';
        echo $this->getPrepend();
        echo $this->form->textArea($this->model, $this->attribute, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo $this->getAppend();
        echo '</div>';
        echo '</div>';
    }
    
    /**
     * Renders a text field.
     * @return string the rendered content
     */
    protected function textField() {
        echo '<div class="form-row">';
        echo $this->getLabel() . $this->getHint();
        echo '<div class="form-element">';
        echo $this->getPrepend();
        echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
        echo $this->getError();
        echo $this->getAppend();
        echo '</div>';
        echo '</div>';
    }

    /**
     * Renders a CAPTCHA.
     * @return string the rendered content
     */
    protected function captcha() {
        echo '<div class="form-row">';
        echo $this->getLabel();
        echo '<div class="form-element"><div class="captcha">';
        echo '<div class="widget">' . $this->widget('CCaptcha', $this->captchaOptions, true) . '</div>';
        echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo '</div></div></div>';
    }

    protected function textAOField() {
         echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
    }

    protected function passwordAOField() {
         echo $this->form->passwordField($this->model, $this->attribute, $this->htmlOptions);
    }
    
    protected function checkBoxAO() {
        $attribute = $this->attribute;
        echo '<label class="checkbox" for="' . $this->getAttributeId($attribute) . '">';
        echo $this->form->checkBox($this->model, $attribute, $this->htmlOptions) . PHP_EOL;
        echo 'Keep me signed in';
        echo '</label>';
    }

    /**
     * Renders an uneditable field.
     * @return string the rendered content
     */
    protected function uneditableField() {
        echo '<div class="form-row">';
        echo $this->getLabel();
        echo '<div class="form-element">';
        echo CHtml::tag('span', $this->htmlOptions, $this->model->{$this->attribute});
        echo $this->getError() . $this->getHint();
        echo '</div></div>';
    }

    /**
     * Render date picker calendar text box field
     */
        protected function datepickerField() {
        echo '<div class="form-row">';
        // register extra scripts
        $assetsPath = Yii::getPathOfAlias('ausa.assets');
        $assetsUrl = Yii::app()->assetManager->publish($assetsPath, false, -1, YII_DEBUG);
        if (!cs()->isScriptFileRegistered($assetsUrl . '/js/bootstrap-datepicker.js')) {
            cs()->registerScriptFile($assetsUrl . '/js/bootstrap-datepicker.js');
            cs()->registerCssFile($assetsUrl . '/css/bootstrap-datepicker.css');
        }
        $extraOptions = array(
            'data-tbdatepicker' => 'datepicker',
            'class' => 'span'
        );
        echo $this->getLabel();
        echo '<div class="form-element">';
        echo $this->getPrepend();
        echo $this->form->textField($this->model, $this->attribute, CMap::mergeArray($this->htmlOptions, $extraOptions));
        echo $this->getError() . $this->getHint();
        echo $this->getAppend();
        echo '</div></div>';
    }

    /**
     * Render two buttons (select from template and upload file) fields with
     * image preview
     */
    protected function imageField() {
        echo '<div class="form-row">';
        /* $labels = $this->model->attributeLabels();
          if (isset($labels[$this->attribute])){
          echo '<div style="color: #333333; font-size: 13px; font-weight: 700; padding: 15px 20px 10px;">';
          echo $labels[$this->attribute];
          echo '</div>';
          } */
        //echo $this->getLabel();
        $image = null;

        if (strlen($this->model->{$this->attribute}) > 4) {
            $image = Image::model()->findByAttributes(array('long_id' => $this->model->{$this->attribute}));
        } else if ($this->model->{$this->attribute} > 0) {
            $image = Image::model()->findByPk($this->model->{$this->attribute});
        }

        if ($image == null) {
            $image = new Image;
        }
        echo '<div class="form-element">';
        Yii::app()->controller->widget('ext.imageupload.ImageUploadWidget', array(
            'image' => $image,
            'model' => $this->model,
            'attribute' => $this->attribute,
            'layout' => isset($this->imageOptions['layout']) ? $this->imageOptions['layout'] : null,
            'noPhoto' => isset($this->imageOptions['noPhoto']) ? $this->imageOptions['noPhoto'] : null,
            // javascript executed after upload and resize is done
            'afterFinish' => isset($this->imageOptions['afterFinish']) ? $this->imageOptions['afterFinish'] : null,
            // javascript executed after first upload is done
            'afterUpload' => isset($this->imageOptions['afterUpload']) ? $this->imageOptions['afterUpload'] : null,
            // what images are loaded as template ones
            //'template' => isset($this->imageOptions['template']) ? $this->imageOptions['template'] : null,
            'configuration' => isset($this->imageOptions['configuration']) ? $this->imageOptions['configuration'] : 'default'));
        echo '</div></div>';
    }
    
    /**
     * Render month, day, year drop down fields in one row
     */
    public function dateDropDownField(){
        
        $monthMap = array(
            1 => 'Jan',
            2 => 'Feb',
            3 => 'Mar',
            4 => 'Apr',
            5 => 'May',
            6 => 'Jun',
            7 => 'Jul',
            8 => 'Aug',
            9 => 'Sep',
            10 => 'Oct',
            11 => 'Nov',
            12 => 'Dec'
        );

        $years = array('' => 'year');
        $months = array('' => 'month');
        $days = array('' => 'day');
        
        $yearNow = date('Y') + 0;

        // allow the minimum age to be set by the widget call, or set to 17 otherwise - travis
        if(isset($this->htmlOptions['minAge'])) {
            $minAge = $this->htmlOptions['minAge'];
        } else {
            $minAge = 17;
        }

        // allow the manimum age to be set by the widget call, or set to 100 otherwise
        if(isset($this->htmlOptions['maxAge'])) {
            $maxAge = $this->htmlOptions['maxAge'];
        } else {
            $maxAge = 100;
        }
        
        for ($i = $yearNow - $minAge; $i > $yearNow - $maxAge; $i--){
            $years[$i] = $i;
        }
        
        for ($i = 1; $i <= 31; $i ++){
            $days[$i] = $i;
        }
        
        for ($i = 1; $i <=12; $i++){
            $months[$i] = $i . ' - ' . $monthMap[$i];
        }
        
        $prefix = get_class($this->model) . '_' . $this->attribute;
        $value = Shared::toTimestamp($this->model->{$this->attribute});
        
        // update hidden field when value of any drop down changes
        cs()->registerScript($prefix . '_dateselect', '
            function ' . $prefix . '_aoDateDropdown(){
                var year = $("#' . $prefix . '_year").val();
                var month = $("#' . $prefix . '_month").val() || 1;
                var day = $("#' . $prefix . '_day").val() || 1;
                console.log(year, month, day);
                if (year) {
                    $("#' . $prefix . '").val(year + "-" + month + "-" + day);
                }
            }
            
            $("#' . $prefix . '_year").change(function(e){' . $prefix . '_aoDateDropdown();});
            $("#' . $prefix . '_month").change(function(e){' . $prefix . '_aoDateDropdown();});
            $("#' . $prefix . '_day").change(function(e){' . $prefix . '_aoDateDropdown();});
        ');
        
        // render the element
        $this->htmlOptions['class'] = 'form-row';
        $content = $this->getLabel();
        $content .= '<div class="form-element">';
        
        $content .= CHtml::dropDownList($prefix . '_year', $value ? date('Y', $value) : '', $years, array('style' => 'width: 30%;'));
        $content .= CHtml::dropDownList($prefix . '_month', $value ? date('m', $value) : '', $months, array('style' => 'width: 30%; margin-left: 5%;'));
        $content .= CHtml::dropDownList($prefix . '_day', $value ? date('d', $value) : '', $days, array('style' => 'width: 30%; margin-left: 5%;'));
        $content .= CHtml::activeHiddenField($this->model, $this->attribute);
        
        $content .= $this->getError() . $this->getHint();
        $content .= '</div>';
        echo CHtml::tag('div', $this->htmlOptions, $content);
    }

    /**
     * Render custom html element together with label for this field. Use this
     * option if you need to customize the input row.
     */
    public function customField() {
        $this->htmlOptions['class'] = 'form-row';
        $content = '<label>' . $this->data['label'] . '</label>';
        $content .= '<div class="form-element">';
        $content .= $this->data['element'];
        $content .= $this->getError() . $this->getHint();
        $content .= '</div>';
        echo CHtml::tag('div', $this->htmlOptions, $content);
    }

}

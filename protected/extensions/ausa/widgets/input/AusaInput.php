<?php

/**
 * TbInput class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets.input
 */
Yii::import('bootstrap.widgets.input.TbInput');

/**
 * Bootstrap input widget.
 * Used for rendering inputs according to Bootstrap standards.
 */
abstract class AusaInput extends TbInput {
    // The different input types.
    // these could be replaced by regular CHtml elements

    const TYPE_TEXT_AO = 'textao';
    const TYPE_PASSWORD_AO = 'passao';
    const TYPE_CHECKBOX_AO = 'checkao';
    const TYPE_DATEPICKER = 'datepicker';
    const TYPE_IMAGE = 'imagefield';
    const TYPE_CUSTOM = 'custom';
    const TYPE_DATEDROPDOWN = 'datedropdown';

    public $hintInline;

    /**
     * Runs the widget.
     * @throws CException if the widget type is invalid.
     */
    public function run() {
        switch ($this->type) {
            case self::TYPE_CHECKBOX:
                $this->checkBox();
                break;

            case self::TYPE_CHECKBOXLIST:
                $this->checkBoxList();
                break;

            case self::TYPE_CHECKBOXLIST_INLINE:
                $this->checkBoxListInline();
                break;

            case self::TYPE_DROPDOWN:
                $this->dropDownList();
                break;

            case self::TYPE_FILE:
                $this->fileField();
                break;

            case self::TYPE_PASSWORD:
                $this->passwordField();
                break;

            case self::TYPE_RADIO:
                $this->radioButton();
                break;

            case self::TYPE_RADIOLIST:
                $this->radioButtonList();
                break;

            case self::TYPE_RADIOLIST_INLINE:
                $this->radioButtonListInline();
                break;

            case self::TYPE_TEXTAREA:
                $this->textArea();
                break;

            case self::TYPE_TEXT:
                $this->textField();
                break;

            case self::TYPE_CAPTCHA:
                $this->captcha();
                break;

            case self::TYPE_TEXT_AO:
                $this->textAOField();
                break;

            case self::TYPE_PASSWORD_AO:
                $this->passwordAOField();
                break;

            case self::TYPE_CHECKBOX_AO:
                $this->checkBoxAO();
                break;

            case self::TYPE_UNEDITABLE:
                $this->uneditableField();
                break;

            case self::TYPE_DATEPICKER:
                $this->datepickerField();
                break;

            case self::TYPE_IMAGE:
                $this->imageField();
                break;

            case self::TYPE_CUSTOM:
                $this->customField();
                break;

            case self::TYPE_DATEDROPDOWN:
                $this->dateDropDownField();
                break;

            default:
                throw new CException(__CLASS__ . ': Failed to run widget! Type is invalid.');
        }
    }

    protected function getLabel() {
        if ($this->label !== false && !in_array($this->type, array('checkbox', 'radio')) && $this->hasModel())
            return $this->form->labelEx($this->model, $this->attribute, $this->labelOptions);
        else if ($this->label !== null)
            return $this->label;
        else
            return '';
    }

    /**
     * Returns the prepend element for the input.
     * @return string the element
     */
    protected function getPrepend() {
        if ($this->hasAddOn()) {
            $htmlOptions = $this->prependOptions;

            if (isset($htmlOptions['class']))
                $htmlOptions['class'] .= ' add-on';
            else
                $htmlOptions['class'] = 'add-on';

            ob_start();
            echo '<div class="' . $this->getAddonCssClass() . '">';
            if (isset($this->prependText))
                echo CHtml::tag('span', $htmlOptions, $this->prependText);

            return ob_get_clean();
        }
        else
            return '<div class="clearfix">';
    }

    /**
     * Returns the append element for the input.
     * @return string the element
     */
    protected function getAppend() {
        $hint = $this->getInlineHint();
        if ($this->hasAddOn()) {
            $htmlOptions = $this->appendOptions;

            if (isset($htmlOptions['class']))
                $htmlOptions['class'] .= ' add-on';
            else
                $htmlOptions['class'] = 'add-on';

            ob_start();
            if (isset($this->appendText))
                echo CHtml::tag('span', $htmlOptions, $this->appendText);

            echo $hint . '</div>';
            return ob_get_clean();
        } else {
            return $hint . '</div>';
        }
    }

    /**
     * Rerutrns hint for a popup question mark
     * @return string
     */
    protected function getHint() {
        if (isset($this->hintText)) {
            $htmlOptions = $this->hintOptions;
            if (isset($htmlOptions['class']))
                $htmlOptions['class'] .= ' help-block';
            else
                $htmlOptions['class'] = 'help-block';
            return '<a class="ausa-toolTip" rel="tooltip" title="' . $this->hintText . '"></a>';
        } else {
            return '';
        }
    }
    
    /**
     * Returns hint displayed below the form field
     * @return string
     */
    protected function getInlineHint() {
        if (isset($this->hintInline)) {
            return CHtml::tag('p', array('class' => 'help-block'), $this->hintInline);
            ;
        } else {
            return '';
        }
    }
    
    /*protected function getAppend() {
        $append = parent::getAppend();
        return $this->getInlineHint() . $append;
    }*/

    protected function processHtmlOptions() {
        parent::processHtmlOptions();
        if (isset($this->htmlOptions['hint-inline'])) {
            $this->hintInline = $this->htmlOptions['hint-inline'];
            unset($this->htmlOptions['hint-inline']);
        }
    }

    abstract protected function textAOField();

    abstract protected function passwordAOField();

    abstract protected function checkBoxAO();

    /**
     * Renders an bootstap datepicker downloaded from some example page
     * @return string the rendered content
     * @abstract
     */
    abstract protected function datepickerField();

    /**
     * Renders ajax image upload field
     * @return string the rendered content
     * @abstract
     */
    abstract protected function imageField();

    /**
     * Renders any html field, which is not a part of model.
     * Use CHtml class to render the content of this field
     * @return string the rendered content
     * @abstract
     */
    abstract protected function customField();

    /**
     * Renders three dropdowns to select a date.
     * @abstract
     */
    abstract protected function dateDropDownField();
}

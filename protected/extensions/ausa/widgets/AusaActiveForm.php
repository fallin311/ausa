<?php

/**
 * TbActiveForm class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 */
Yii::import('ausa.widgets.input.AusaInput');
Yii::import('bootstrap.widgets.TbActiveForm');

/**
 * Bootstrap active form widget.
 */
class AusaActiveForm extends TbActiveForm {
    // Form types.

    const TYPE_VERTICAL = 'vertical';
    const TYPE_INLINE = 'inline';
    const TYPE_HORIZONTAL = 'horizontal';
    const TYPE_SEARCH = 'search';

    // Input classes.
    const INPUT_HORIZONTAL = 'ausa.widgets.input.AusaInputHorizontal';
    const INPUT_INLINE = 'bootstrap.widgets.input.TbInputInline';
    const INPUT_SEARCH = 'bootstrap.widgets.input.TbInputSearch';
    const INPUT_VERTICAL = 'ausa.widgets.input.AusaInputVertical';

    /**
     * @var string the form type. See class constants.
     */
    public $type = self::TYPE_HORIZONTAL;

    /**
     * Renders a checkbox input row.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes
     * @return string the generated row
     */
    public function checkBoxRow($model, $attribute, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_CHECKBOX, $model, $attribute, null, $htmlOptions);
    }

    /**
     * Renders a checkbox list input row.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $data the list data
     * @param array $htmlOptions additional HTML attributes
     * @return string the generated row
     */
    public function checkBoxListRow($model, $attribute, $data = array(), $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_CHECKBOXLIST, $model, $attribute, $data, $htmlOptions);
    }

    /**
     * Renders a checkbox list inline input row.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $data the list data
     * @param array $htmlOptions additional HTML attributes
     * @return string the generated row
     */
    public function checkBoxListInlineRow($model, $attribute, $data = array(), $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_CHECKBOXLIST_INLINE, $model, $attribute, $data, $htmlOptions);
    }

    /**
     * Renders a drop-down list input row.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $data the list data
     * @param array $htmlOptions additional HTML attributes
     * @return string the generated row
     */
    public function dropDownListRow($model, $attribute, $data = array(), $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_DROPDOWN, $model, $attribute, $data, $htmlOptions);
    }

    /**
     * Renders a file field input row.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes
     * @return string the generated row
     */
    public function fileFieldRow($model, $attribute, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_FILE, $model, $attribute, null, $htmlOptions);
    }

    /**
     * Renders a password field input row.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes
     * @return string the generated row
     */
    public function passwordFieldRow($model, $attribute, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_PASSWORD, $model, $attribute, null, $htmlOptions);
    }

    /**
     * Renders a radio button input row.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes
     * @return string the generated row
     */
    public function radioButtonRow($model, $attribute, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_RADIO, $model, $attribute, null, $htmlOptions);
    }

    /**
     * Renders a radio button list input row.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $data the list data
     * @param array $htmlOptions additional HTML attributes
     * @return string the generated row
     */
    public function radioButtonListRow($model, $attribute, $data = array(), $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_RADIOLIST, $model, $attribute, $data, $htmlOptions);
    }

    /**
     * Renders a radio button list inline input row.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $data the list data
     * @param array $htmlOptions additional HTML attributes
     * @return string the generated row
     */
    public function radioButtonListInlineRow($model, $attribute, $data = array(), $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_RADIOLIST_INLINE, $model, $attribute, $data, $htmlOptions);
    }

    /**
     * Renders a text field input row.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes
     * @return string the generated row
     */
    public function textFieldRow($model, $attribute, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_TEXT, $model, $attribute, null, $htmlOptions);
    }
        
    /**
     * Customized image upload field
     * @param type $model
     * @param type $attribute
     * @param type $htmlOptions
     * @return type
     */
    public function imageFieldRow($model, $attribute, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_IMAGE, $model, $attribute, null, $htmlOptions);
    }
    
    public function dateDropDownFieldRow($model, $attribute, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_DATEDROPDOWN, $model, $attribute, null, $htmlOptions);
    }

    /**
     * Customization for AO login bootstrap dropdown
     * @param type $model
     * @param type $attribute
     * @param type $htmlOptions
     * @return type
     */
    public function textAOFieldRow($model, $attribute, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_TEXT_AO, $model, $attribute, null, $htmlOptions);
    }
    public function passwordAOFieldRow($model, $attribute, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_PASSWORD_AO, $model, $attribute, null, $htmlOptions);
    }
    public function checkBoxAORow($model, $attribute, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_CHECKBOX_AO, $model, $attribute, null, $htmlOptions);
    }

    /**
     * Customized date picker field
     * @param type $model
     * @param type $attribute
     * @param type $htmlOptions
     * @return type
     */
    public function datepickerRow($model, $attribute, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_DATEPICKER, $model, $attribute, null, $htmlOptions);
    }

    public function customRow($model, $attribute, $label, $element, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_CUSTOM, $model, $attribute, array(
                    'element' => $element,
                    'label' => $label
                        ), $htmlOptions);
    }

    /**
     * Renders a text area input row.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes
     * @return string the generated row
     */
    public function textAreaRow($model, $attribute, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_TEXTAREA, $model, $attribute, null, $htmlOptions);
    }

    /**
     * Renders a captcha row.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes
     * @return string the generated row
     * @since 0.9.3
     */
    public function captchaRow($model, $attribute, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_CAPTCHA, $model, $attribute, null, $htmlOptions);
    }

    /**
     * Renders an uneditable text field row.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes
     * @return string the generated row
     * @since 0.9.5
     */
    public function uneditableRow($model, $attribute, $htmlOptions = array()) {
        return $this->inputRow(AusaInput::TYPE_UNEDITABLE, $model, $attribute, null, $htmlOptions);
    }
    
    public function errorSummary($models, $header = null, $footer = null, $htmlOptions = array()) {
        if (!isset($htmlOptions['class'])){
            $htmlOptions['class'] = 'alert alert-block alert-error'; // Bootstrap error class as default
        }
        return parent::errorSummary($models, $header, $footer, $htmlOptions);
    }

    /**
     * Returns the input widget class name suitable for the form.
     * @return string the class name
     */
    protected function getInputClassName() {
        if (isset($this->input))
            return $this->input;
        else {
            switch ($this->type) {
                case self::TYPE_HORIZONTAL:
                    return self::INPUT_HORIZONTAL;
                    break;

                case self::TYPE_INLINE:
                    return self::INPUT_INLINE;
                    break;

                case self::TYPE_SEARCH:
                    return self::INPUT_SEARCH;
                    break;

                case self::TYPE_VERTICAL:
                default:
                    return self::INPUT_VERTICAL;
                    break;
            }
        }
    }

}

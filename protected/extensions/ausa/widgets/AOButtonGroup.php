<?php

Yii::import('bootstrap.widgets.TbButtonGroup');

class AOButtonGroup extends TbButtonGroup{
    
    public function init() {
        $classes = array('widget-control pull-right');
        
        if ($this->stacked === true)
            $classes[] = 'btn-group-vertical';
        
        if ($this->dropup === true)
            $classes[] = 'dropup';
        
        if (!empty($classes)) {
            $classes = implode(' ', $classes);
            if (isset($this->htmlOptions['class']))
                $this->htmlOptions['class'] .= ' '.$classes;
        else
            $this->htmlOptions['class'] = $classes;
        }
            
            $validToggles = array(self::TOGGLE_CHECKBOX, self::TOGGLE_RADIO);
            
            if (isset($this->toggle) && in_array($this->toggle, $validToggles))
                    $this->htmlOptions['data-toggle'] = 'buttons-'.$this->toggle;
    }
    
    public function run()
	{
		echo CHtml::openTag('div', $this->htmlOptions);

		foreach ($this->buttons as $button)
		{
			if (isset($button['visible']) && $button['visible'] === false)
				continue;

			$this->controller->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>isset($button['buttonType']) ? $button['buttonType'] : $this->buttonType,
				'type'=>isset($button['type']) ? $button['type'] : $this->type,
				'size'=>$this->size, // all buttons in a group cannot vary in size
				'icon'=>isset($button['icon']) ? $button['icon'] : null,
				'label'=>isset($button['label']) ? $button['label'] : null,
				'url'=>isset($button['url']) ? $button['url'] : null,
				'active'=>isset($button['active']) ? $button['active'] : false,
				'items'=>isset($button['items']) ? $button['items'] : array(),
				'ajaxOptions'=>isset($button['ajaxOptions']) ? $button['ajaxOptions'] : array(),
				'htmlOptions'=>isset($button['htmlOptions']) ? $button['htmlOptions'] : array(),
				'encodeLabel'=>isset($button['encodeLabel']) ? $button['encodeLabel'] : $this->encodeLabel,
			));
		}

		echo '</div>';
	}
    
}
?>

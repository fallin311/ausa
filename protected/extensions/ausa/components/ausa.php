<?php
/**
 * AUSA application component (inserts AUSA js).
 */
class Ausa extends CApplicationComponent
{
	public $enableJS = true;

        protected $_assetsUrl;

	public function init()
	{
		if (Yii::getPathOfAlias('ausa') === false)
			Yii::setPathOfAlias('ausa', realpath(dirname(__FILE__).'/..'));

		// Prevents the extension from registering scripts and publishing assets when ran from the command line.
		if (Yii::app() instanceof CConsoleApplication)
			return;

		if ($this->enableJS !== false)
			$this->registerCoreScripts();

        parent::init();
	}

	public function registerCoreScripts()
	{
		$this->registerJS(Yii::app()->clientScript->coreScriptPosition);
	}


	public function registerJS($position = CClientScript::POS_HEAD)
	{
		/** @var CClientScript $cs */
		$cs = Yii::app()->getClientScript();
                $cs->registerScriptFile($this->getAssetsUrl().'/js/ausa.js', $position);
                $cs->registerScriptFile($this->getAssetsUrl().'/js/jquery.notifyBar.js', $position);
                $cs->registerScriptFile($this->getAssetsUrl().'/js/bootstrap-clickover.js', $position);
	}


	protected function registerPlugin($name, $selector = null, $options = array(), $defaultSelector = null)
	{
		if (!isset($selector) && empty($options))
		{
			// Initialization from extension configuration.
			$config = isset($this->plugins[$name]) ? $this->plugins[$name] : array();

			if (isset($config['selector']))
				$selector = $config['selector'];

			if (isset($config['options']))
				$options = $config['options'];

			if (!isset($selector))
				$selector = $defaultSelector;
		}

		if (isset($selector))
		{
			$key = __CLASS__.'.'.md5($name.$selector.serialize($options).$defaultSelector);
			$options = !empty($options) ? CJavaScript::encode($options) : '';
			Yii::app()->clientScript->registerScript($key, "jQuery('{$selector}').{$name}({$options});");
		}
	}


	protected function getAssetsUrl()
	{
		if (isset($this->_assetsUrl))
			return $this->_assetsUrl;
		else
		{
			$assetsPath = Yii::getPathOfAlias('ausa.assets');
			$assetsUrl = Yii::app()->assetManager->publish($assetsPath, false, -1, YII_DEBUG);
			return $this->_assetsUrl = $assetsUrl;
		}
	}
}

var animationSpeed = 400;
$(function () {
    $("a.backToTop").click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, animationSpeed);
        return false
    });
    $('[rel="clickover"]').clickover();
});
$(document).ready(function () {
    var userDropMenu = $(".ausa-userInfoDropData").width();
    var windowWidth = $(window).width();
    var padded = 120;
    if(userDropMenu <= 190){ userDropMenu = 191; }
    $(".ausa-footerCopy").css({'width':windowWidth-20});
    $(".ausa-userInfoDrop").css({'min-width':userDropMenu+padded});
    //console.log(userDropMenu+padded);
    if ($(".ausa-body").height() > $("body").height()) { $(".toTop").css({'display':'block'}); }
});
$(window).resize(function() {
    var windowWidth = $(window).width();
    $(".ausa-footerCopy").css({'width':windowWidth-20});
});
function showSuccess(msg){
	$.notifyBar({
		html: msg,
		cls: "success",
		delay: 3000,
		animationSpeed: "normal"
	});
};
function showError(msg){
	$.notifyBar({
		html: msg,
		cls: "error",
		delay: 3000,
		animationSpeed: "normal"
	});  
};
function showDefault(msg){
        $.notifyBar({
            html: msg,
            delay: 3000,
            animationSpeed: "normal"
        });
};
function serializeAll(){
    var output = '';
    $(".ausa-contentMainInner form").each(function(index, value){
        output += "&" + $(value).serialize()
    });
    return output.substring(1);
}
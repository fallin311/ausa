<?php

/**
 * Bootstrap wysiwyg widget.
 */
class TbWysiwyg extends CWidget {

    public $model;
    public $attribute;

    /**
     * @var array which buttons to show above the editor. Defaults to null 
     * meaning all buttons are shown.
     */
    public $buttons;

    /**
     * @var array list of events that should be passed to the editor.
     */
    public $events;

    /**
     * @var array the HTML attributes for the widget container.
     */
    public $htmlOptions = array();

    /**
     * @var array the default list of buttons to be shown.
     */
    private $_defaultButtons = array(
        'font-styles',
        'emphasis',
        'lists',
        'html',
        'link',
        'color',
        'image'
    );

    /**
     * Initializes the widget
     */
    public function init() {
        if (!isset($this->htmlOptions['id']))
            $this->htmlOptions['id'] = $this->getId();

        // Publish and register necessary files
        $libPath = Yii::getPathOfAlias('bootstrap') . '/lib/bootstrap-wysihtml5';

        $assetManager = Yii::app()->getAssetManager();
        $cssPath = $assetManager->publish($libPath . '/css');
        $jsPath = $assetManager->publish($libPath . '/js');

        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerCssFile($cssPath . '/bootstrap-wysihtml5-0.0.2.css');
        //$cs->registerScriptFile($jsPath . '/wysihtml5-0.3.0_rc2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($jsPath . '/wysihtml5-0.4.0pre.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($jsPath . '/bootstrap-wysihtml5-0.0.2.min.js', CClientScript::POS_END);
    }

    /**
     * Runs the widget 
     */
    public function run() {
        $id = $this->htmlOptions['id'];

        // Determine what options should be passed
        $options = array();
        foreach ($this->_defaultButtons as $button)
            $options[$button] = $this->buttons === null || array_search($button, $this->buttons) !== false;

        if (isset($this->events))
            $options['events'] = $this->events;

        $options = CJavascript::encode($options);

        app()->getClientScript()->registerScript(__CLASS__ . '#' . $id, "jQuery('#{$id}').wysihtml5({$options});");
        echo CHtml::openTag('textarea', $this->htmlOptions);
        echo $this->model[$this->attribute];
        echo '</textarea>';
    }

}
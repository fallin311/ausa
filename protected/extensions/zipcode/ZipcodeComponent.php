<?php

Yii::import('application.extensions.zipcode.ZipCode');

class ZipcodeComponent extends CApplicationComponent {

    public $tableName = 'zip_code';

    /**
     * Returns new zipcode instance
     */
    public function get($location) {
        //Shared::debug($location);
        $zip = new ZipCode($location);
        $zip->mysql_table = $this->tableName;
        return $zip;
    }

    /**
     * Installs the database schem if it doesn't exist. Might cause first slow load
     */
    public function init() {
        parent::init();
        Shared::debug("initialize");
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT zip_code_id FROM " . $this->tableName . " LIMIT 1,1");
        try {
            $command->query();
        } catch (CDbException $exc) {
            $this->installSchema();
        }
    }

    public function installSchema() {
        Shared::debug("The table doesn exist");

        // create the table first

        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->createTable($this->tableName, array(
            'zip_code_id' => 'int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT',
            'zip_code' => 'char(5) UNIQUE NOT NULL',
            'city' => 'varchar(50)',
            'county' => 'varchar(50)',
            'state_name' => 'varchar(50)',
            'state_prefix' => 'char(2)',
            'area_code' => 'char(3)',
            'time_zone' => 'varchar(50)',
            'lat' => 'float NOT NULL',
            'lon' => 'float NOT NULL'
        ));

        $command->createIndex('city_state', $this->tableName, 'city,state_prefix');

        // TODO: extend to multiple insert
        $command = $connection->createCommand("INSERT INTO " . $this->tableName . " VALUES (:p0,:p1,:p2,:p3,:p4,:p5,:p6,:p7,:p8,:p9)");
        
        // now load the data and insert them into the database
        $url = dirname(__FILE__) . '/data/zipcode.csv';
        if (file_exists($url)) {
            $row = 1;
            if (($handle = fopen($url, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    $num = count($data);
                    $row++;
                    for ($c = 0; $c < $num; $c++) {
                        $command->bindParam(":p$c", $data[$c]);
                    }
                    $command->execute();
                }
                fclose($handle);
            }
        }
    }

}

?>

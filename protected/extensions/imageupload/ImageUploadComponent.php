<?php

/**
 * Class responsible for processing uploaded image 
 */
class ImageUploadComponent {

    /**
     * Various configurations for image resizing. Use it when you need to
     * resize image after upload with confirmation dialog
     * @var type 
     */
    public static $configuration = array(
        // default config is inherited by the others
        'default' => array(
            'forceCrop' => false,
            'base64Response' => false,
            // how much will be image resized after upload
            // it helps to fit the original image to the crop screen
            'maxDimensions' => array('width' => 1024, 'height' => 768),
            // this is how big is the preview image
            // additionaly, we can crop it even more
            'previewDimensions' => array('width' => 125, 'height' => 100),
            'cropDimensions' => array('width' => 250, 'height' => 200),
            'requireMinSize' => true,
            'extensions' => array("jpeg", "jpg", "png"),
            'sizeLimit' => 5242880 // 5MB
        ),
        'little-square' => array(
            'forceCrop' => true,
            'maxDimensions' => array('width' => 500, 'height' => 400),
            'previewDimensions' => array('width' => 125, 'height' => 100)
        ),
        // default for offers and company logos
        'logo' => array(
            'forceCrop' => true,
            'maxDimensions' => array('width' => 500, 'height' => 400),
            'previewDimensions' => array('width' => 125, 'height' => 100),
            'template' => 'SELECT image_id, long_id, vendor_id, file_name FROM image WHERE category = "logoTemplate" ORDER BY updated_on'
        ),
        'banner' => array(
            'forceCrop' => true,
            'maxDimensions' => array('width' => 1024, 'height' => 768),
            'previewDimensions' => array('width' => 640, 'height' => 100),
            'cropDimensions' => array('width' => 640, 'height' => 100),
            //'template' => 'SELECT image_id, long_id, vendor_id, file_name FROM image WHERE category = "logoTemplate" ORDER BY updated_on'
        ),
        'big-banner' => array(
            'forceCrop' => true,
            'maxDimensions' => array('width' => 1024, 'height' => 768),
            'previewDimensions' => array('width' => 640, 'height' => 200),
            'cropDimensions' => array('width' => 640, 'height' => 200),
            //'template' => 'SELECT image_id, long_id, vendor_id, file_name FROM image WHERE category = "logoTemplate" ORDER BY updated_on'
        ),
    );
    
    private $config;

    /** we want to set a configuration with object */
    function __construct($config = 'default') {
        $this->config = self::getConfig($config);
        //$this->allowedExtensions = $this->config['extensions'];
        //$this->sizeLimit = $this->config['sizeLimit'];

        // $this->checkServerSettings();

        if (isset($_GET['qqfile'])) {
            $this->file = new qqUploadedFileXhr();
        } elseif (isset($_FILES['qqfile'])) {
            $this->file = new qqUploadedFileForm();
        } else {
            $this->file = false;
        }
    }

    /**
     * Returns complete configuration arrray based on 'default' config.
     * Each screen can have different resolution of uploaded pictures
     * @param type $config 
     */
    public static function getConfig($config){
        $default = self::$configuration['default'];
        if (isset(self::$configuration[$config]) && $config != 'default'){
            foreach (self::$configuration[$config] as $attr => $val){
                $default[$attr] = $val;
            }
        }
        return $default;
    }

    /**
     * Convert the file size to KB, MB or GB
     * @param type $str
     * @return int
     */
    private function toBytes($str) {
        $val = trim($str);
        $last = strtolower($str[strlen($str) - 1]);
        switch ($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;
        }
        return $val;
    }

    /**
     * Returns array('success'=>true) or array('error'=>'error message')
     */
    public function handleUpload(&$model) {

        if (!$this->file) {
            return array('error' => 'No files were uploaded.');
        }

        $size = $this->file->getSize();

        if ($size == 0) {
            return array('error' => 'File is empty');
        }

        if ($size > $this->config['sizeLimit']) {
            return array('error' => 'File is too large');
        }

        $pathinfo = pathinfo($this->file->getName());

        $ext = $pathinfo['extension'];

        if ($this->config['extensions'] && !in_array(strtolower($ext), $this->config['extensions'])) {
            $these = implode(', ', $this->config['extensions']);
            return array('error' => 'File has an invalid extension, it should be one of ' . $these . '.');
        }
        // see the class qqUploadedFileForm below
        if ($this->file->save($model, $pathinfo, $this->config)) {
            
            $response = array(
                'success' => true
            );
            // make sure we have at least required preview dimensions
            if ($this->config['requireMinSize']){
                if ($model->getWidth() < $this->config['previewDimensions']['width'] || $model->getHeight() < $this->config['previewDimensions']['height']){
                    
                    $response['error'] = 'Uploaded image is too small. Please get one in higher resolution (at least ' . $this->config['previewDimensions']['width'] . 'x' . $this->config['previewDimensions']['height'] . ')';
                    $response['success'] = false;
                }
            }
            
            // saved, lets see if it needs additional processing
            if ($response['success'] && $this->config['forceCrop'] && ($model->getWidth() > $this->config['cropDimensions']['width'] || $model->getHeight() > $this->config['cropDimensions']['height'])){
                $response = array( 
                    'success' => true,
                    'url' => $model->getUrl($this->config['maxDimensions']['width'], $this->config['maxDimensions']['height'], true), 
                    'previewUrl' => $model->getUrl($this->config['previewDimensions']['width'], $this->config['previewDimensions']['height'], true),
                    'crop' => 1 // make it crop
                    );
                    Shared::debug("the image has to be resized");
                    Shared::debug("width: " . $model->getWidth());
                    Shared::debug("configured width: " . $this->config['cropDimensions']['width']);
                    
            }else if ($response['success']){
                $response = array( 
                    'success' => true,
                    'url' => $model->getUrl($this->config['previewDimensions']['width'], $this->config['previewDimensions']['height']),
                    'previewUrl' => $model->getUrl($this->config['previewDimensions']['width'], $this->config['previewDimensions']['height'], true),
                    );
                    Shared::debug("the image does not have to be resized");
            }
            
            $response['fullUrl'] = $model->getUrl();
            $response['imageId'] = $model->long_id;
            
            if ($this->config['base64Response']){
                $response['base64img'] = $model->getBase64Src();
            }
            // dimensions are important for default crop area preselect
            $response['width'] = $model->getWidth();
            $response['height'] = $model->getHeight();
            
            // notice that there might be no image id yet, only after upload
            return $response;
        } else {
            return array('error' => 'Could not save uploaded file.' .
                'The upload was cancelled, or server error encountered');
        }
    }

}

/**
 * Handle file uploads via XMLHttpRequest
 */
class qqUploadedFileXhr {

    /**
     * Save the file to the specified path
     * attribute file_content of media will be updated
     * @return boolean TRUE on success
     */
    function save(&$model, $fileinfo, $config) {
        Shared::debug("xhr upload");
        $input = fopen("php://input", "r");
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);
        if ($realSize != $this->getSize()) {
            return false;
        }

        $tempFile = 'protected/runtime/temp/' . $this->getName();
        $target = fopen($tempFile, "w");
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);
        if (is_object($model)) {
            $model->file_size = $this->getSize();
            $model->file_name = preg_replace("/[^A-Za-z0-9-_\.]/", '', str_replace(" ", "-", strtolower($this->getName())));
            $model->updated_on = Shared::toDatabase(time());
            
            Shared::debug("new file size is" . $model->file_size);
            switch ($fileinfo['extension']) {
                case 'png':
                    $model->file_type = 'image/png';
                    break;
                case 'jpg':
                case 'jpeg':
                    $model->file_type = 'image/jpeg';
                    break;
                case 'css':
                    $model->file_type = 'text/css';
            }

            // load the file
            $model->file_content = file_get_contents('protected/runtime/temp/' . $this->getName());
            
            $originalImage = imagecreatefromstring($model->file_content);
            imagealphablending($originalImage, false);
            imagesavealpha($originalImage, true);
            
            $canvas = $model->resize($config['maxDimensions']['width'], $config['maxDimensions']['height'], $originalImage);

            $model->saveCanvasToString($canvas);
            
            // resolution and file size might change after resize
            $model->file_size = strlen($model->file_content);
            $model->resolution = imagesx($canvas) . 'x' . imagesy($canvas);

            // why it worked before ...
            //$model->save();
            
            //Shared::debug("new resolution is " . $model->resolution);
            //fclose($fp);
            if (file_exists($tempFile)){
                unlink($tempFile);
            }
        }

        return true;
    }

    function getName() {
        return $_GET['qqfile'];
    }

    function getSize() {
        if (isset($_SERVER["CONTENT_LENGTH"])) {
            return (int) $_SERVER["CONTENT_LENGTH"];
        } else {
            throw new Exception('Getting content length is not supported.');
        }
    }

}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 * Used by opera and IE. Handles only one file at the time
 */
class qqUploadedFileForm {

    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save(&$model, $fileInfo, $config) {
        Shared::debug("traditional upload");
        if (is_object($model)) {
            Shared::debug("save the shit");
            $file = $_FILES['qqfile']['tmp_name'];
            $model->file_size = $this->getSize();
            $model->file_name = preg_replace("/[^A-Za-z0-9-_\.]/", '', str_replace(" ", "-", strtolower($this->getName())));
            $model->updated_on = Shared::toDatabase(time());

            if (strstr(strtolower($model->file_name), 'png')) {
                $model->file_type = 'image/png';
            }
            if (strstr(strtolower($model->file_name), 'jpg')) {
                $model->file_type = 'image/jpeg';
            }
            if (strstr(strtolower($model->file_name), 'jpeg')) {
                $model->file_type = 'image/jpeg';
            }
            // load the file
            /*$fp = fopen($file, 'r');
            if (!$fp) {
                return false;
            }*/
            $model->file_content = file_get_contents($file);
            //$model->file_content = fread($fp, $model->file_size);
            $originalImage = imagecreatefromstring($model->file_content);
            //Shared::debug("original length is " . strlen($model->file_content));
            
            $canvas = $model->resize($config['maxDimensions']['width'], $config['maxDimensions']['height'], $originalImage);
            $model->saveCanvasToString($canvas);
            $model->file_size = strlen($model->file_content);
            //Shared::debug("resized has {$model->file_size}B");
            
            //$model->file_content = addslashes($model->file_content);
            $model->resolution = imagesx($canvas) . 'x' . imagesy($canvas);

            // why it worked before ...
            //$model->save();
            
            //fclose($fp);
            unlink($file);
        } else {
            return false;
        }
        /* if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){
          return false;
          } */
        return true;
    }

    function getName() {
        return $_FILES['qqfile']['name'];
    }

    function getSize() {
        return $_FILES['qqfile']['size'];
    }

}

?>

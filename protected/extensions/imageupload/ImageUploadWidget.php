<?php

/**
 * This widget displays upload button and active drag and drop area for image upload
 * It is similar to IMageSelectWidget (it should merge) 
 */
class ImageUploadWidget extends CWidget {

    /** what is going to process the uploaded data */
    public $serverCallback;

    /** customizable layouts for displaying the widget. You can choose from
     * previewLayout previewTemplateLayou simpleUploadLayout
     */
    public $layout = 'preview-layout';

    /** default value. If you set this one, there is already going to be an image in the preview field */
    public $selectedMediaId;
    private $buttonLabel = '<div style="qq-button-title">Upload a file</div>';
    // executed the callback directly
    public $onComplete = false;
    public $buttonId;
    // upload multiple files at once
    public $multiple = false;
    // see ImageUploadComponent::$configuration
    public $configuration = 'default';
    private $config; // loaded configuration array based on configuration attribute
    public $label;
    public $noPhoto;
    // updating existing image
    public $model;
    public $attribute;
    // or just set the image object directly
    public $image;
    // javascript action executed after successful upload
    public $afterUpload;
    // javascript executed after upload and resize is done
    public $afterFinish;
    // select an image from pre-uploaded template
    // array containing query, limit
    public $template = null;
    public $useLongId = true;
    // path to the assets folder of imageupload extension
    private $assets;

    public function loadAssets() {
        // debug always replaces the files
        $url = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.extensions.imageupload.assets'), false, -1, false);

        Yii::app()->clientScript->registerScriptFile($url . "/fileuploader.js");
        Yii::app()->clientScript->registerCSSFile($url . '/fileuploader.css');

        Yii::app()->clientScript->registerScriptFile($url . "/jquery.color.js");
        Yii::app()->clientScript->registerScriptFile($url . "/jquery.Jcrop.min.js");
        Yii::app()->clientScript->registerCSSFile($url . '/jquery.Jcrop.min.css');

        $this->assets = $url;

        Yii::import('ext.imageupload.ImageUploadComponent');
        Yii::import('ext.simpleModal.ModalWidget');

        ModalWidget::loadAssets();
    }

    public function init() {
        $this->loadAssets();

        $browser = new EWebBrowser();
        if ($browser->browser == EWebBrowser::BROWSER_CHROME || $browser->browser == EWebBrowser::BROWSER_FIREFOX) {
            $this->buttonLabel = '<div class="qq-button-title-2">Upload a file</div><div class="qq-button-subtitle">or drag and drop here</div>';
        } else {
            $this->buttonLabel = '<div class="qq-button-title-1">Upload a file</div>';
        }

        if (!$this->buttonId) {
            $this->buttonId = "upload-button-" . $this->getId();
        }

        // default server callback, the part, which is going to process the data
        if ($this->serverCallback == null) {
            $this->serverCallback = url('image/process');
        }

        $this->config = ImageUploadComponent::getConfig($this->configuration);
    }

    public function run() {

        $id = $this->getId();

        $params = array('config' => $this->configuration);
        if ($this->image != null) {
            if ($this->image->image_id) {
                $params['image_id'] = $this->image->image_id;
            }
        } else if ($this->model != null) {
            // load the image from model
            $attr = $this->attribute;
            if ($this->useLongId) {
                $this->image = Image::model()->findByAttributes(array('long_id' => $this->model->$attr));
                Shared::debug($this->image);
            } else {
                $this->image = Image::model()->findByPk($this->model->$attr);
            }
        }

        // set the correct vendor id for uploaded images
        // TODO: extend for chapter and sales team
        if ($this->model != null && isset($this->model->vendor_id)) {
            $params['vendor_id'] = $this->model->vendor_id;
        }

        // set no photo preview image
        if ($this->noPhoto != null) {
            $imagePreview = $this->noPhoto;
        } else {
            $imagePreview = $this->assets . '/no_photo.jpg';
        }
        if ($this->image != null && !$this->image->isNewRecord) {
            $imagePreview = $this->image->getUrl($this->config['previewDimensions']['width'], $this->config['previewDimensions']['height']);
        }

        // lets make default callback, which will refresh preview thumbnail
        if (!$this->onComplete) {
            // refresh preview and possibly display crop window
            $this->onComplete = 'function(id, filename, response){
                var $img = $(".upload-' . $id . ' .preview-image");

                var imageId = 0;
                var longId = "";
console.log(response);
                // do we have the right size?
                if (!response["success"]) {
                    // the uploaded image is too small
                    showError(response["error"]);
                }else if (1 == response["crop"]) {
                 var url = response["url"].replace(/&amp;/g, "&");
                    var $cropImg = $(".upload-' . $id . ' .crop-image");
                        
                    // set the resize feautre
                    widget = widget || new ImageUploadWidget("' . $id . '");
                    widget.setupJcrop(url, response["imageId"], response["longId"], response["width"], response["height"]);
                    
                    // display the popup window ... we have to always create new one, possible memory leak
                    var $modal = $.modal($(".upload-' . $id . ' .upload-resize-popup"), {
                        escClose: false,
                        overlayClose: false,
                        persist: true,
                        modal: true,
                        minWidth: ' . ($this->config['maxDimensions']['width'] + 30) . ',
                        minHeight: ' . ($this->config['maxDimensions']['height'] + $this->config['previewDimensions']['height'] + 40) . ',
                        close: false,
                        containerCss:{padding:0},
                    });
                    widget.setModalDialog($modal);
                    // update the temporary uploaded image id
                    ' . ($this->model == null ? '' : 'imageId = response["imageId"]; longId = response["longId"]') . '
                }else{
                    var url = response["previewUrl"].replace(/&amp;/g,"&");
                    $img.attr("src", url).removeClass("loading-overlay").attr("src", url);
                    ' . ($this->model == null ? '' : '$("#' . get_class($this->model) . '_' . $this->attribute . '").val(response["' . ($this->useLongId ? 'longId' : 'imageId') . '"]);') . ' 
                    ' . ($this->afterFinish == null ? '' : $this->afterFinish) . '
                }
                ' . ($this->afterUpload == null ? '' : $this->afterUpload) . '
            }';
        }

        if ($this->config['forceCrop']) {
            // Function to update preview and submit final crop to the server
            // EScriptBoost::registerScript
            cs()->registerScript('crop-the-image', '
function ImageUploadWidget(widgetId) {  

    var $croppedArea = $(".upload-" + widgetId +" .crop-image")
    var $croppedPreview = $(".upload-" + widgetId +" .preview-image-crop");
    var $dimensionValues = $("#crop-dimensions-" + widgetId +"");

    // Create variables (in this scope) to hold the API and image size
    var jcrop_api, boundx, boundy, defaultArea, imageId, longId;
    var modalDialog;
    
    /**
    * Update Preview image when cropped area is moved. This does not update
    * the real preview image of the ImageWidget element
    */
    this.updatePreview = function(c) {

            if (parseInt(c.w) > 0) {
            var rx = ' . $this->config['previewDimensions']['width'] . ' / c.w;
            var ry = ' . $this->config['previewDimensions']['height'] . ' / c.h;
            // resize preview
            $croppedPreview.css({
                width: Math.round(rx * boundx) + "px",
                height: Math.round(ry * boundy) + "px",
                marginLeft: "-" + Math.round(rx * c.x) + "px",
                marginTop: "-" + Math.round(ry * c.y) + "px"
            });

            // set values, which can submitted back to server
            $dimensionValues.val( Math.round(c.x) + "," + Math.round(c.y) + "," + Math.round(c.w) + "," + Math.round(c.h) );
            }
        };

    /**
    * Update Image Widget preview image, set the correct image_id for the model
    * and close the popup window.
    */
    this.finishUpload = function() {
        $.get("' . url('image/finishUpload') . '", "image_id=" + imageId + "&dimensions=" + $dimensionValues.val() + "&configuration=' . $this->configuration . '", function(responseJson){
            var response = jQuery.parseJSON(responseJson);
            if (response["success"] == 1){
                $(".upload-' . $id . ' .preview-image").attr("src", response["url"]);

                // and close the window
                if (modalDialog != undefined){
                    modalDialog.close();
                }
                
                // simple changeImage has problems
                jcrop_api.destroy();
                jcrop_api = undefined;
                
                // workaround for images, which stays the same size
                var $parent = $croppedArea.parent();
                $croppedArea.remove();
                $croppedArea = $("<img />");
                $croppedArea.addClass("crop-image");
                $parent.append($croppedArea);
                
                // update the form field
                ' . ($this->model == null ? '' : 'if(imageId > 0){$("#' . get_class($this->model) . '_' . $this->attribute . '").val(' . ($this->useLongId ? 'longId' : 'imageId') . ');}') . '
                //console.log(response["imageId"]);
                ' . ($this->afterFinish == null ? '' : $this->afterFinish) . '
            }
        });
    }
    
    this.setModalDialog = function(dialog){
        modalDialog = dialog;
    }
    
    this.getModalDialog = function(){
        return modalDialog;
    }
    
    /** Invoke every time the image to be resized is changed */
    this.setupJcrop = function(imageUrl, _imageId, _longId, width, height){
        // preselected crop area
        if (width == undefined) width = $croppable.width();
        if (height == undefined) height = $croppable.height();
        boundx = width;
        boundy = height;
        imageId = _imageId;
        longId = _longId;
        
        defaultArea = [0, 0, width, height];
        // check if some variables are already initialized

        // setup the server callback for the resize button

        me = this;

        $croppedArea.attr("src", imageUrl);
        $croppedPreview.attr("src", imageUrl);

        $croppedArea.Jcrop({
            onChange: this.updatePreview,
            onSelect: this.updatePreview,
            setSelect: defaultArea,
            minSize: [' . $this->config['cropDimensions']['width'] . ',' . $this->config['cropDimensions']['height'] . '],
            aspectRatio: ' . ($this->config['previewDimensions']['width'] / $this->config['previewDimensions']['height']) . '}, 
            function(){
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                jcrop_api = this;
        });

        // set the corect viewport at preview image
        this.updatePreview({
            w: parseInt(width),
            h: parseInt(height),
            x: 0,
            y: 0,
            x2: parseInt(width),
            y2: parseInt(height)
        });
    }
    

    // control actions
    $(".upload-" + widgetId + " .submit-crop-button").bind("click", this.finishUpload);

}
');
        }

        cs()->registerScript('upload-file-' . $id, "
            var widget; // upload widget defined above
            var uploader = new qq.FileUploader({
                element: document.getElementById('{$this->buttonId}'),
                action: '" . $this->serverCallback . "',
                onSubmit: function(){
                    $('.upload-" . $id . " img.preview-image').addClass('loading-overlay');
                },
                template: '<div class=\"qq-uploader\">' + 
                '<div class=\"qq-upload-drop-area\"><span>Drop files here to upload</span></div>' +
                '<div class=\"qq-upload-button btn btn-primary\">" . $this->buttonLabel . "</div>' +
                '<div class=\"qq-result\"><ul class=\"qq-upload-list\"></ul><div class=\"qq-uploaded\"></div></div>' + 
                '</div>',
                params: " . CJavaScript::jsonEncode($params) . ",
                " . ($this->onComplete ? ('onComplete: ' . $this->onComplete . ',') : '') . "
                multiple: " . ($this->multiple ? 'true' : 'false') . "
            });
            
", CClientScript::POS_READY);

        // prepare the template images
        if ($this->template == null) {
            if (isset($this->config['template']))
                $this->template = $this->config['template'];
        }
        if ($this->template == null) {
            $templateImages = null;
        } else {
            //Shared::debug("loading template images");
            // this should rather return list of urls for those images
            $templateImages = Image::model()->findAllBySql($this->template);
            $command = Yii::app()->db->createCommand($this->template);
            $dataReader = $command->query();
            //$templateImages = array();
            foreach ($dataReader as $row) {
                $templateImages[] = array(
                    'name' => $row['file_name'],
                    'image_id' => $row['image_id'],
                    'long_id' => $row['long_id'],
                    // display it without resizing
                    'src' => Yii::app()->createAbsoluteUrl('image/display', array('img' => $row['long_id'], 'w' => $this->config['previewDimensions']['width']))
                );
            }

            // now register callback, so when user click the image, popup window close and preview is set to the new image
            cs()->registerScript('template-select-popup', '
var $templateModal;                        
$(".upload-' . $id . ' .select-from-template-button").click(function(){
    $templateModal = $.modal($(".upload-' . $id . ' .template-popup"), {
        escClose: true,
        overlayClose: true,
        persist: true,
        modal: true,
        opacity: 50,
        minWidth: ' . ($this->config['previewDimensions']['width'] * 2 + 30) . ',
        minHeight: ' . ($this->config['previewDimensions']['height'] * 3 + 40) . ',
        containerCss:{padding:0},
    });
});
                    
$(".upload-' . $id . ' .template-popup .template-image-holder").bind("click", function(){

    // update the preview image first
    $(".upload-' . $id . ' .preview-image").attr("src", "' . url('/image/display') . '?img=" + $(this).data("long_id"));
        
    // then model ... TODO - model field name
    ' . ($this->model != null ? '$("#' . get_class($this->model) . '_' . $this->attribute . '").val($(this).data("' . ($this->useLongId ? 'long_id' : 'image_id') . '"))' : '') . '
        
    // then close the window
    if ($templateModal != undefined){
        $templateModal.close();
    }
});                        
');
        }

        // and render
        if (strlen($this->layout)) {
            $this->renderFile('protected/extensions/imageupload/views/' . $this->layout . '.php', array(
                'id' => $id,
                'buttonId' => $this->buttonId,
                'previewSrc' => $imagePreview,
                'label' => $this->label,
                'config' => $this->config,
                'model' => $this->model,
                'attribute' => $this->attribute,
                'templateImages' => $templateImages,
                'image' => $this->image));
        }
    }

}

?>

<?php
Yii::app()->clientScript->registerCss('layout-css', '
.image-upload-widget .first-column {
    float: left; 
    border: 1px solid transparent;
    position: relative;
    width: 660px;
    
}
.image-upload-widget .second-column {
    float: left; border: 1px solid transparent;
    position: relative;
    width: 180px;
    padding-left: 10px;
}

.preview-image-mask {
    overflow: hidden;
    margin: 5px;
    margin-top: 0px;
}

.upload-resize-popup {
    display: none;
}

.upload-resize-popup .left-column, .upload-resize-popup .right-column {
    position: relative;
    float: left;
    width: 45%;
    margin-left: 10px;
}
.upload-resize-popup .right-column .preview-image-mask {
    position: relative;
    float: right;
}
.upload-resize-popup .jcrop-holder {
    margin-left: auto;
    margin-right: auto;
    margin-top: 5x;
}
.upload-resize-popup .popup-header {
    border-bottom: 2px solid grey;
    margin-bottom: 10px;
    position: relative;
    float: left;
    width: 100%;
}
.template-popup {
    padding: 5px;
    position: relative;
    clear: both;
    display: none;
}
.template-popup .template-image-holder {
    position: relative;
    float: left;
    margin: 5px;
    cursor: pointer;
}
.select-from-template-button {
    width: 152px;
}
.img-preview-loading-mask {
    width: ' . $config['previewDimensions']['width'] . 'px;
    height: ' . $config['previewDimensions']['height'] . 'px;
    background-color: grey;
    background-image:url(\'' . url('/images/spinner2.gif') . '\');
    background-position: 50% 50%;
    background-repeat: no-repeat;
}

.preview-image {
    width: ' . $config['previewDimensions']['width'] . 'px;
    height: ' . $config['previewDimensions']['height'] . 'px;
}

.qq-result {
    height: 58px !important;
}
');

// display the image id, so it can be directly used in a form
if ($model != null) {
    echo CHtml::activeHiddenField($model, $attribute);
}
?>
<div class="image-upload-widget upload-<?php echo $id ?>">
    <div class="first-column">
        <div class="img-preview-loading-mask">
            <img class="preview-image" src="<?php echo $previewSrc ?>" alt="preview" />
        </div>
    </div>
    <div class="second-column">
        <div style="margin-bottom: 5px; height: 100px;">
            <div class="upload-button" id="<?php echo $buttonId ?>">Upload File</div>
        </div>
        
    </div>
    <div style="clear: both; padding-top: 10px; margin-left: 20px; padding-bottom: 10px;">
        <p class="help-block">Please keep the image size under 5MB and use jpg or png extensions.</p>
    </div>

    <div class="upload-resize-popup">
        <div class="popup-header">
            <div class="left-column">

                <?php
                // <div class="qq-upload-button submit-crop-button"><div class="qq-button-title-1">Finish Upload</div></div>
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Finish Upload',
                    'type' => 'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                    'htmlOptions' => array(
                        'class' => 'submit-crop-button'
                    )
                ));
                ?>
                <p style="margin-top: 15px;">
                    Please select an area you want to display and click the <i>Finish Upload</i> button above.
                </p>
            </div>
            <div class="right-column">
                <div class="preview-image-mask" style="<?php echo $config['forceCrop'] ? ('width: ' . $config['previewDimensions']['width'] . 'px; height: ' . $config['previewDimensions']['height'] . 'px;') : '' ?>">
                    <img class="preview-image-crop" src="" alt="preview" style="max-width: none !important;"/>
                </div>
            </div>
        </div>
        <div class="clear"> </div>
        <input type="hidden" id="crop-dimensions-<?php echo $id ?>" value="" />
        <div style="text-align: center; margin-left: 0px; margin-right: 0px; position: relative; top: 130px;">
            <img src="" class="crop-image" alt="crop" />
        </div>
    </div>
  
</div>
<div class="clear"> </div>

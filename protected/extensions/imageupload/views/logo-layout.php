<?php
Yii::app()->clientScript->registerCss('layout-css', '
#simplemodal-container {width: 540px;}
.image-upload-widget .first-column {
    float: left; 
    border: 1px solid transparent;
    position: relative;
    width: 162px;
    
}
.image-upload-widget .second-column {
    float: left; border: 1px solid transparent;
    position: relative;
    width: 140px;
}

.preview-image-mask {
    overflow: hidden;
    margin: 5px;
    margin-top: 0px;
}

.upload-resize-popup {
    display: none;
}

.upload-resize-popup .left-column, .upload-resize-popup .right-column {
    position: relative;
    float: left;
    width: 45%;
    margin-left: 10px;
}
.upload-resize-popup .right-column .preview-image-mask {
    position: relative;
    float: right;
}
.upload-resize-popup .jcrop-holder {
    margin-left: auto;
    margin-right: auto;
    margin-top: 5x;
}
.upload-resize-popup .popup-header {
    margin-top: 10px;
    position: relative;
    float: left;
    width: 100%;
}
.template-popup {
    width: 100% !important;
    border: 0px solid black !important;
    margin: 0px !important;
    display: none;
}
.template-popup .template-image-holder {
    position: relative;
    float: left;
    margin: 5px;
    cursor: pointer;
}
.crop-panel {
    max-height:285px;
    width:100%;
    position:relative;
    overflow-y:scroll;
    border-top: #D2D2D2 1px solid;
    -webkit-box-shadow: inset 0 0 5px #DDD;
    -moz-box-shadow: inset 0 0 5px #dddddd;
    box-shadow: inset 0 0 5px #DDD;
    padding: 20px 0;
}
.select-from-template-button {
    width: 152px;
}
.img-preview-loading-mask {
    width: ' . $config['previewDimensions']['width'] . 'px;
    height: ' . $config['previewDimensions']['height'] . 'px;
    background-color: grey;
    background-image:url(\'' . url('/images/spinner2.gif') . '\');
    background-position: 50% 50%;
    background-repeat: no-repeat;
}

.preview-image {
    width: ' . $config['previewDimensions']['width'] . 'px;
    height: ' . $config['previewDimensions']['height'] . 'px;
}
');

echo '<div class="clear">';
// display the image id, so it can be directly used in a form
if ($model != null) {
    echo CHtml::activeHiddenField($model, $attribute);
}
?>
    <div class="image-upload-widget upload-<?php echo $id ?>">
        <div class="first-column">
            <div class="img-rounded img-preview-loading-mask">
                <img class="img-rounded preview-image" src="<?php echo $previewSrc ?>" alt="preview" />
            </div>
        </div>
        <div class="second-column">
            <div style="margin-bottom: 5px; height: 60px;">
                <div class="upload-button" id="<?php echo $buttonId ?>">Upload File</div>
            </div>
            <?php
            if ($templateImages != null) {
                // display the button for popup for template images
                //<div class="select-from-template-button qq-upload-button" ><div class="qq-button-title-1">Select from Template</div></div>
                $this->widget('bootstrap.widgets.TbButton', array(
                        'label' => 'Select from Template',
                        'type' => 'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                        'htmlOptions' => array(
                            'class' => 'select-from-template-button',
                        )
                    ));
            }
            ?>
        </div>
        <div style="clear: both; padding-top: 10px;">
        </div>

        <div class="upload-resize-popup">
            <?php $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons camera', 'panelTitle' => 'Crop Your Logo')); ?>
            <div class="popup-header">
                <div class="left-column">

                    <?php
                    // <div class="qq-upload-button submit-crop-button"><div class="qq-button-title-1">Finish Upload</div></div>
                    $this->widget('bootstrap.widgets.TbButton', array(
                        'label' => 'Finish Upload',
                        'type' => 'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                        'htmlOptions' => array(
                            'class' => 'submit-crop-button'
                        )
                    ));
                    ?>
                    <p style="margin-top: 15px;">
                        Please select an area you want to display and click the <i>Finish Upload</i> button above.
                    </p>
                </div>
                <div class="right-column">
                    <div class="preview-image-mask" style="<?php echo $config['forceCrop'] ? ('width: ' . $config['previewDimensions']['width'] . 'px; height: ' . $config['previewDimensions']['height'] . 'px;') : '' ?>">
                        <img class="preview-image-crop" src="" alt="preview" style="max-width: none !important;"/>
                    </div>
                </div>
            </div>
            <input type="hidden" id="crop-dimensions-<?php echo $id ?>" value="" />
            <div class="crop-panel">
                <img src="" class="crop-image" alt="crop" />
            </div>
            <?php $this->widget('application.components.widgets.AOPanelFoot'); ?>
        </div>
        <?php
        if ($templateImages != null) {
            // display the popup for template images
            ?>
            <div class="template-popup">
                <?php $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons camera', 'panelTitle' => 'Select a Default Image')); ?>
                <div style="display:inline-block;">
                    <?php
                    foreach ($templateImages as $image) {
                        if (isset($image['src'])) {
                            ?>
                            <div class="template-image-holder" data-image_id="<?php echo $image['image_id'] ?>" data-long_id="<?php echo $image['long_id']; ?>">
                                <img class="img-rounded" src="<?php echo $image['src'] ?>" alt="<?php echo $image['name'] ?>" />
                            </div>
                        <?php }
                    }
                    ?>
                </div>
                <?php $this->widget('application.components.widgets.AOPanelFoot'); ?>
            </div>
            <?php
        }
        ?>
    </div>
    <a class="ausa-toolTip" rel="tooltip" data-original-title="Image must be under 1MB and be&nbsp;.jpg or&nbsp;.png extensions. Dimensions must be 125px by 100px minimum."></a>
</div>

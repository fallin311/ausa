<?php
Yii::app()->clientScript->registerCss('layout-css', '
.image-upload-widget .first-column {
    float: left; 
    border: 1px solid transparent;
    position: relative;
    width: 180px;
    width: 30%;
}
.image-upload-widget .second-column {
    float: left; border: 1px solid transparent;
    position: relative;
    min-width: 120px;
    width: 39%;
}
.image-upload-widget .third-column {
    float: left; 
    border: 1px solid transparent;
    position: relative;
    min-width: 160px;
    width: 30%;
}

.preview-image-mask {
    overflow: hidden;
    margin: 5px;
    margin-top: 0px;
}

.upload-resize-popup {
    display: none;
}

.upload-resize-popup .left-column, .upload-resize-popup .right-column {
    position: relative;
    float: left;
    width: 45%;
    margin-left: 10px;
}
.upload-resize-popup .right-column .preview-image-mask {
    position: relative;
    float: right;
}
.upload-resize-popup .jcrop-holder {
    margin-left: auto;
    margin-right: auto;
    margin-top: 5x;
}
.upload-resize-popup .popup-header {
    border-bottom: 2px solid grey;
    margin-bottom: 10px;
    position: relative;
    float: left;
    width: 100%;
}
.template-popup {
    padding: 5px;
    position: relative;
    clear: both;
    display: none;
}
.template-popup .template-image-holder {
    position: relative;
    float: left;
    margin: 5px;
    cursor: pointer;
}
');

// display the image id, so it can be directly used in a form
if ($model != null) {
    echo CHtml::activeHiddenField($model, $attribute);
}
?>
<div class="image-upload-widget upload-<?php echo $id ?>">
    <div class="first-column">

        <p>
            Only jpeg and png formats are allowed. Keep your image size under 1MB,
            you can resize the image once it is uploaded.
        </p>
    </div>
    <div class="second-column">
        <img class="preview-image" src="<?php echo $previewSrc ?>" alt="preview" />
    </div>
    <div class="third-column">
        <div style="margin-bottom: 5px; height: 60px;">
            <div class="upload-button" id="<?php echo $buttonId ?>">Upload File</div>
        </div>
        <?php
        if ($templateImages != null) {
            // display the button for popup for template images
            Shared::debug("display template images");
            ?>
            <div class="select-from-template-button qq-upload-button" ><div class="qq-button-title-1">Select from Template</div></div>
            <?php
        }
        ?>
    </div>

    <div class="upload-resize-popup">
        <div class="popup-header">
            <div class="left-column">

                <?php
                // <div class="qq-upload-button submit-crop-button"><div class="qq-button-title-1">Finish Upload</div></div>
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Finish Upload',
                    'type' => 'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                    'htmlOptions' => array(
                        'class' => 'submit-crop-button'
                    )
                ));
                ?>
                <p style="margin-top: 5px;">
                    Please select an area you want to display and click the <i>Finish Upload</i> button above.
                </p>
            </div>
            <div class="right-column">
                <div class="preview-image-mask" style="<?php echo $config['forceCrop'] ? ('width: ' . $config['previewDimensions']['width'] . 'px; height: ' . $config['previewDimensions']['height'] . 'px;') : '' ?>">
                    <img class="preview-image-crop" src="" alt="preview" style="max-width: none !important;"/>
                </div>
            </div>
        </div>
        <div class="clear"> </div>
        <input type="hidden" id="crop-dimensions-<?php echo $id ?>" value="" />
        <div style="text-align: center; margin-left: 0px; margin-right: 0px; position: relative; top: 130px;">
            <img src="" class="crop-image" alt="crop" />
        </div>
    </div>
    <?php
    if ($templateImages != null) {
        // display the popup for template images
        ?>
        <div class="clear"> </div>
        <div class="template-popup">
            <h2>Select an image from template</h2>
            <div>
                <?php
                foreach ($templateImages as $image) {
                    if (isset($image['src'])) {
                        ?>
                        <div class="template-image-holder" data-image_id="<?php echo $image['image_id'] ?>" data-long_id="<?php echo $image['long_id']; ?>">
                            <img src="<?php echo $image['src'] ?>" alt="<?php echo $image['name'] ?>" />
                        </div>
                    <?php }
                }
                ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<div class="clear"> </div>

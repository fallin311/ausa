<?php

/**
 * Initializes amazon SDK for PHP classes to be used inside YII 
 */
class AmazonComponent extends CApplicationComponent {

    public function init() {
        parent::init();
        Yii::import('ext.amazon.*');
        //$pathToSES = dirname(__FILE__) . '/protected/extensions/amazon/';
        $pathToSES = Yii::getPathOfAlias('ext.amazon') . '/';
        
        Yii::$classMap=array(
            'AmazonSES'=>$pathToSES.'services/ses.class.php',
            'CFRuntime'=>$pathToSES.'sdk.class.php',
            'CFUtilities'=>$pathToSES.'utilities/utilities.class.php',
            'CFComplexType'=>$pathToSES.'utilities/complextype.class.php',
            'RequestCore'=>$pathToSES.'lib/requestcore/requestcore.class.php',
            'CFRequest'=>$pathToSES.'utilities/request.class.php',
            'CFResponse'=>$pathToSES.'utilities/response.class.php',
            'CFSimpleXML'=>$pathToSES.'utilities/simplexml.class.php',
            'CFCredential'=>$pathToSES.'utilities/credential.class.php',
            'CFCredentials'=>$pathToSES.'utilities/credentials.class.php',
            'AmazonEC2'=>$pathToSES.'services/ec2.class.php',
            'AuthV2Query'=>$pathToSES.'authentication/signature_v2query.class.php',
            'AuthV3Query'=>$pathToSES.'authentication/signature_v3query.class.php',
            'Signer'=>$pathToSES.'authentication/signer.abstract.php',
            'Signable'=>$pathToSES.'authentication/signable.interface.php'
        );
    }

}

?>
<?php

class UsaepayComponent extends CApplicationComponent {

    public $wsdl;
    public $sourceKey;
    public $pin;
    
    private $soapClient;
    private $token;

    public function init() {
        $seed = time() . rand();
        
        // make hash value using sha1 function
        $clear = $this->sourceKey . $seed . $this->pin;
        $hash = sha1($clear);

        // assembly ueSecurityToken as an array
        $this->token = array(
            'SourceKey' => $this->sourceKey,
            'PinHash' => array(
                'Type' => 'sha1',
                'Seed' => $seed,
                'HashValue' => $hash
            ),
            'ClientIP' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1',
        );
        
        $this->soapClient = new SoapClient($this->wsdl);
    }

    public function getSoapClient() {
        return $this->soapClient;
    }
    
    public function getToken() {
        return $this->token;
    }

}

?>

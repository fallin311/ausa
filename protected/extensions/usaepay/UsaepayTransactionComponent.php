<?php

class UsaepayTransactionComponent extends CApplicationComponent {

    public function init() {
        Yii::import('ext.usaepay.usaepay');

        Yii::$classMap['umTransaction'] = Yii::getPathOfAlias('ext.usaepay') . '/usaepay.php';
    }

    /**
     * Get an instance of umTransaction class. Follow example on
     * http://wiki.usaepay.com/developer/phplibrary#examplequicksale
     * to see more details about the parameters.
     * @param type $chapter
     * @return \umTransaction
     */
    public function getInstance($chapter) {
        if (is_object($chapter) && isset($chapter->usaepay_key)) {
            $tran = new umTransaction;
            $tran->key = $chapter->usaepay_key;
            $tran->pin = $chapter->usaepay_pin;
            $tran->cabundle = Yii::getPathOfAlias('ext.usaepay') . '/cacert.pem';
            //$tran->ignoresslcerterrors = true;
            //$tran->ip = $_SERVER['REMOTE_ADDR'];
            $tran->usesandbox = true;
            return $tran;
        }
    }

}

?>

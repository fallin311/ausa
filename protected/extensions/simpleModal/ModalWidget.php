<?php

/**
 * This widget displays upload button and active drag and drop area for image upload
 * It is similar to IMageSelectWidget (it should merge) 
 */
class ModalWidget extends CWidget {

    public $dialog; // jquery selector for applied element
    public $trigger;
    
    // path to the assets folder of imageupload extension
    private $assets;

    /**
     * We might want to use the widget independently on Yii and php 
     */
    public static function loadAssets() {
        // debug always replaces the files
        $url = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('ext.simpleModal.assets'), false, -1, false);

        Yii::app()->clientScript->registerScriptFile($url . "/jquery.simplemodal.1.4.2.min.js");
        Yii::app()->clientScript->registerCSSFile($url . '/simpleModal.css');

        return $url;
    }

    public function init() {
        $this->assets = self::loadAssets();
        // we might start the dialog here and then just cache content and display it on run
    }

    public function run() {

        /*$id = $this->getId();
        cs()->registerScript('modal-dialog-' . $id, '
        var $modal_' . $id . ' = $("' . $this->element . '");
        $modal_' . $id . '.jqm({modal: false});
');*/
    }

}

?>

<?php

/**
 * Extension based on jquery expander plugin
 * http://plugins.learningjquery.com/expander/
 */
class Expander extends CInputWidget {

    private $defaults = array('userCollapseText' => ' [^]');
    public $options = array();
    public $scriptPosition = null;
    public $selector;

    public function run() {
        
        $cs = Yii::app()->clientScript;

        if ($this->scriptPosition === null)
            $this->scriptPosition = $cs->coreScriptPosition;
        if (YII_DEBUG){
            $bu = Yii::app()->assetManager->publish(dirname(__FILE__) . '/jquery.expander.js');
            $cs->registerScriptFile($bu, $this->scriptPosition);
        } else {
            $bu = Yii::app()->assetManager->publish(dirname(__FILE__) . '/jquery.expander.min.js');
            $cs->registerScriptFile($bu, $this->scriptPosition);
        }

        $options = CJavaScript::encode(CMap::mergeArray($this->defaults, $this->options));
        //$options = $this->options ? CJavaScript::encode($this->options) : '';
        $cs->registerScript(__CLASS__ . '#' . $this->id, "jQuery('{$this->selector}').expander({$options});");
    }

}

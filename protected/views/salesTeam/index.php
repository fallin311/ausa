<?php

$this->layout = '//layouts/full';
$breadcrumbs = array(
    'My Dashboard' => app()->user->getHomeUrl(),
    'Sales Teams'
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
?>

    <?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false)); ?>
    <?php $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Sales Teams')); ?>
    <?php
    $this->widget('ausa.widgets.AOButtonGroup', array(
        'type' => '',
        'buttons' => array(
            array('icon' => 'cog', 'items' => array(
                    array('label' => 'Add Sales Team', 'icon' => 'plus', 'url' => url('salesTeam/create')),
                    '---',
                    array('label' => 'Get Help', 'icon' => 'question-sign', 'url' => '#'),
            )),
        ),
    ));
    ?>
    <?php $this->endWidget(); ?>
    <?php
    $dataArray = array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $stProvider,
        'columns' => array(
            array('name' => 'st_name', 'header' => 'Name', 'type' => 'raw', 'value' => 'CHtml::link($data->st_name,url("/salesTeam/update/" . $data->sales_team_id))'),
            ));

    if (app()->user->isAdmin()) {
        $dataArray["columns"][] = array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
        );
    }
    $this->widget('bootstrap.widgets.TbGridView', $dataArray);
    ?>

    <?php $this->endWidget(); ?>
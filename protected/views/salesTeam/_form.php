<?php
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'sales-team-form',
    //'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => true,
        ));

echo $form->errorSummary($model);

if (app()->user->isAdmin()) {
    cs()->registerCss('select2-css',
        '#s2id1 {
            width: 100%;
            margin-bottom: 10px;
        }'
        .'.select2-container .select2-choice div {
            -webkit-border-radius: 0 3px 3px 0;
            -moz-border-radius: 0 3px 3px 0;
            border-radius: 0 3px 3px 0;
            border-left: 1px solid #ccc;
        }');
    echo '<div class="form-row" style="width: 100%;">'
        .'<label for="SalesTeam_chapter_id" class="required">Associated Chapter <span class="required">*</span></label>'
        .'<div class="form-element"><div class="clearfix">';
    $this->widget('ext.select2.ESelect2', array(
        'model' => $model,
        'attribute' => 'chapter_id',
        'data' => CHtml::listData(Chapter::model()->findAll(), 'id', 'chapter_name'),
        'options' => array('placeholder' => 'Select the Associated Chapter')
    ));
    echo '</div></div></div>';
} 

echo $form->textFieldRow($model, 'st_name', array('class' => 'span', 'maxlength' => 128));

//echo $form->textAreaRow($model, 'st_description', array('rows' => 6, 'cols' => 50, 'class' => 'span', 'hint' => 'Dashboard description'));

echo $form->textFieldRow($model, 'address', array('class' => 'span', 'maxlength' => 511));

//echo $form->textFieldRow($model, 'phone', array('class' => 'span', 'maxlength' => 12)); 

echo $form->imageFieldRow($model, 'logo', array('class' => 'span', 'maxlength' => 10, 'imageOptions' => array(
    'configuration' => 'logo',
    'layout' => 'logo-layout'
)));
    
if (app()->user->isAdmin()) {
    echo $form->checkBoxRow($model, 'disabled', array('hint' => 'Select to Disable'));
}

$this->widget('application.components.widgets.AOPanelFoot');

$this->endWidget();

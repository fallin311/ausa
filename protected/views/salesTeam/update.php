<?php
$this->pageTitle = Yii::app()->name . ' - Sales Team Update';
$this->layout = '//layouts/full';

if (app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => url('admin/dashboard'),
        'Sales Teams' => url('salesTeam/index'),
        $model->st_name . ' Dashboard' => url('salesTeam/go/' . $model->sales_team_id),
        'Settings',
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('salesTeam/dashboard'),
        'Settings',
    );
}

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));

$this->widget('application.components.widgets.AOGrid');

$this->beginWidget('application.components.widgets.AOStickyBar', array('sticky' => true, 'panelWrap' => true));
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'icon' => 'remove white',
    'type' => 'danger',
    'url' => url('/salesTeam/dashboard'),
    'label' => 'Cancel',
    'htmlOptions' => array('style' => 'margin: 5px 5px 0px 8px;', 'class' => 'pull-right')
));
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'ajaxSubmit',
    'type' => 'success',
    'url' => url('/salesTeam/update/' . $model->sales_team_id),
    'htmlOptions' => array(
        'style' => 'margin: 5px 8px;',
        'class' => 'pull-right',
        'id' => 'save-st-button',
    ),
    'loadingText' => 'processing...',
    'ajaxOptions' => array(
        'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
        'beforeSend' => 'function(){$("#save-st-button").button("loading")}',
        'success' => 'function(response){

                    if (response.length < 5){
                        // saved, lets go one step further
                        window.location.href="' . url('/salesTeam/dashboard') . '";
                    }else{
                        console.log(response);
                        $("#save-st-button").button("reset");

                        // we are going to display error message
                        $("#ajax-errors-holder").show();
                        $("#ajax-errors").html(response);
                        showError(\'Could not save the form. See error fields below.\');
                    }
                }',
    ),
    'icon' => 'ok white',
    'label' => 'Save',
));
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'ajaxSubmit',
    'type' => '',
    'url' => url('/salesTeam/update/' . $model->sales_team_id),
    'htmlOptions' => array(
        'style' => 'margin: 5px 8px;',
        'class' => 'pull-right',
        'id' => 'apply-st-button',
    ),
    'loadingText' => 'processing...',
    'ajaxOptions' => array(
        'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
        'beforeSend' => 'function(){$("#apply-st-button").button("loading")}',
        'success' => 'function(response){
                    $("#apply-st-button").button("reset");
                    if (response.length < 5){
                        showSuccess(\'Changes have been applied.\');
                        $("#ajax-errors-holder").hide();
                    }else{
                        showError(\'Could not apply changes to the sales team. See error fields below.\');
                        $("#ajax-errors-holder").show();
                        $("#ajax-errors").html(response);
                        $(".notification").css({\'display\':\'block\', \'opacity\':\'1\'});
                    }
                }',
    ),
    'icon' => 'ok',
    'label' => 'Apply',
));
$this->endWidget();
?>

<div style="width: 100%; margin-bottom: 25px; position: relative; float: left; display: none;" id="ajax-errors-holder">
    <?php $this->widget('application.components.widgets.AONotice', array('notice' => '<div id="ajax-errors"></div>', 'noticeType' => 'errors')); ?>
</div>

<div class="ausa-contentMain row-fluid">
    <div id="ausa-contentMainTiles">
        <ul id="tiles">
            <li class="grid">
                <?php
                // Basic company information
                $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Sales Team'));
                echo $this->renderPartial('_form', array('model' => $model));
                $this->endWidget();
                ?>
            </li>
            <li class="grid">
                <?php
                // User contact form
                $this->beginWidget('application.components.widgets.AOPanel');
                $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons download_to_computer', 'panelTitle' => 'Contact Person'));
                echo $this->renderPartial('//user/_contact_form', array('model' => $user, 'userSt' => $userSt));
                $this->endWidget();
                ?>
            </li>
        </ul>
    </div>
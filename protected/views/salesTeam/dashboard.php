<?php
$this->pageTitle = Yii::app()->name . ' - Sales Team Dashboard';
$this->layout = '//layouts/full';
$breadcrumbs = array(
    'My Dashboard',
);

if (app()->user->hasFlash('success')) {
    cs()->registerScript('updates', 'setTimeout(function() { showSuccess(\'' . app()->user->getFlash('success') . '\'); }, 500);');
}

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));

$this->widget('application.components.widgets.AOGrid');
?>

<!--
<div class="ausa-contentMain row-fluid">
    <?php
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelTitle' => 'Vendor List'));
    $this->widget('ausa.widgets.AOButtonGroup', array(
        'type' => '',
        'buttons' => array(
            array('icon' => 'cog', 'items' => array(
                    array('label' => 'Add a Vendor', 'icon' => 'plus', 'url' => url('/vendor/create')),
            )),
        ),
    ));
    $this->endWidget();
    $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $vendorProvider,
        'columns' => array(
            array('name' => 'vendor.vendor_name', 'header' => 'Name', 'type' => 'raw', 'value' => 'CHtml::link($data->vendor->vendor_name,url("/vendor/go/" . $data->vendor_id))'),
            array('name' => 'vendor.category.category_name', 'header' => 'Industry'),
            array('name' => 'vendor.branches', 'value' => '$data->vendor->getTotalBranches()'),
            array('name' => 'vendor.update', 'header' => '', 'type' => 'raw', 'value' => 'CHtml::link("<button class=\"btn btn-small\"><i class=\"icon-cog\"></i></button>",url("/vendor/update/" . $data->vendor_id))', 'htmlOptions' => array('width' => '20', 'class' => 'status-center'))
        ),
    ));
    $this->endWidget();
    ?>

</div>
-->

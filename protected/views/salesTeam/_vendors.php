<?php

// Is this view even being used anymore?

$this->pageTitle = Yii::app()->name . ' - Vendors';
$this->layout = '//layouts/full';
$breadcrumbs = array(
    'My Dashboard' => url('salesTeam/dashboard'),
    'My Vendors',
);

/*
$branchArray = SalesTeam::model()->getVendorBranches($model->sales_team_id);
$mapCenter = 'center: new google.maps.LatLng(31.771375,-106.487148),'; //Todo: Center map on Chapter's city
$mapZoom = null;
$branches = array();
foreach ($branchArray as $branch) {
    if ($branch['lat'] && $branch['lon']) {
        $branches[] = '{ lat: ' . $branch['lat'] . ', lng: ' . $branch['lon'] . ', name: "' . $branch['branch_name'] . '", icon: "../images/mapMarkers/' . ($branch['industry'] ? Category::model()->findByPk($branch['industry'])->marker : 'standard') . '.png" },';
    }
}
//if ($branches) {
if (count($branches) != 1 && count($branches) > 2) {
    $mapZoom = 'map.fitBounds(bounds);';
} else {
    // this does something that is not important right now
}
//}

cs()->registerScriptFile('https://maps-api-ssl.google.com/maps/api/js?v=3&sensor=false');
cs()->registerScript('chapterVendorMap', 'function initialize() {
            var branchOptions = {' .
        $mapCenter
        . 'zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                overviewMapControl: true,
                overviewMapControlOptions: {
                    opened: false,
                },
                scaleControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.LEFT_TOP
                },
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                },
            };
            var map = new google.maps.Map(document.getElementById("map_canvas"), branchOptions);
            var content = document.createElement("DIV");
            var title = document.createElement("DIV");
            var markerShadow = new google.maps.MarkerImage(
                "../images/mapMarkers/shadow.png",
                new google.maps.Size(54,37),
                new google.maps.Point(0,0),
                new google.maps.Point(16,37)
            );
            var markerShape = {
                coord: [29,0,30,1,31,2,31,3,31,4,31,5,31,6,31,7,31,8,31,9,31,10,31,11,31,12,31,13,31,14,31,15,31,16,31,17,31,18,31,19,31,20,31,21,31,22,31,23,31,24,31,25,31,26,31,27,31,28,31,29,30,30,29,31,23,32,22,33,21,34,20,35,19,36,12,36,11,35,10,34,9,33,8,32,2,31,1,30,0,29,0,28,0,27,0,26,0,25,0,24,0,23,0,22,0,21,0,20,0,19,0,18,0,17,0,16,0,15,0,14,0,13,0,12,0,11,0,10,0,9,0,8,0,7,0,6,0,5,0,4,0,3,0,2,1,1,2,0,29,0],
                type: "poly"
            };
            content.appendChild(title);
            var streetview = document.createElement("DIV");
            streetview.style.width = "213px";
            streetview.style.height = "102px";
            content.appendChild(streetview);
            var infowindow = new google.maps.InfoWindow({
                content: content
            });
            var markers = ['
        . implode($branches) .
        '];
            for(index in markers){
                addMarker(markers[index]);
            }
            function addMarker(data) {
                var marker = new google.maps.Marker({
                    animation: google.maps.Animation.DROP,
                    position: new google.maps.LatLng(data.lat, data.lng),
                    icon: data.icon,
                    shadow: markerShadow,
                    shape: markerShape,
                    size: (32,37),
                    point: (0,0),
                    point: (16,37),
                    map: map,
                    title: data.name,
                });
                google.maps.event.addListener(marker, "click", function() {
                    openInfoWindow(marker);
                });
            }
            var bounds = new google.maps.LatLngBounds();
            for (index in markers) {
                var data = markers[index];
                bounds.extend(new google.maps.LatLng(data.lat, data.lng));
            }'
        . $mapZoom .
        'var panorama = null;
            var pin = new google.maps.MVCObject();
            google.maps.event.addListenerOnce(infowindow, "domready", function() {
                 panorama = new google.maps.StreetViewPanorama(streetview, {
                    navigationControl: false,
                    enableCloseButton: false,
                    addressControl: false,
                    linksControl: false,
                    visible: true
                });
                panorama.bindTo("position", pin);
            });
            function openInfoWindow(marker) {
                title.innerHTML = marker.getTitle();
                pin.set("position", marker.getPosition());
                infowindow.open(map, marker);
            }
        }
        initialize()');
*/

$columns = array(
    //array('name' => 'vendor.vendor_id'),
    array('name' => 'vendor.vendor_name', 'header' => 'Name', 'type' => 'raw', 'value' => 'CHtml::link($data->vendor->vendor_name . (($data->vendor->published) ? "" : " <span style=\'color: red;\'>(unpublished)</span>"),url("/vendor/go/" . $data->vendor_id))'),
    array('name' => 'vendor.address', 'header' => 'Address'),
    array('name' => 'vendor.category.category_name', 'header' => 'Industry'),
    array('name' => 'vendor.branches', 'value' => '$data->getTotalBranches()'),
    array('name' => 'vendor.since', 'header' => 'Date Joined', 'type' => 'raw', 'value' => 'Shared::formatShortUSDate($data->since)'),
    array('name' => 'vendor.expiration', 'header' => 'Expires', 'type' => 'raw', 'value' => '"<span " . ((date($data->vendor->expiration) > Shared::timeNow()) ? "" : "class=\"ausa-unpublished\"") . ">" . Shared::formatShortUSDate($data->vendor->expiration) . "</span>"'),
);




/*if(date($vendorProvider->vendor->expiration)>date.now()){
    $expDateColumn = array('name' => 'vendor.expiration', 'header' => 'Expires', 'type' => 'raw', 'value' => 'Shared::formatShortUSDate($data->vendor->expiration)');    
}else{
    $expDateColumn = array('name' => 'vendor.expiration', 'header' => 'Expires', 'type' => 'raw', 'value' => '"<p " . ($data->vendor->published ? "" : "class=\"ausa-unpublished\"") . ">" . Shared::formatShortUSDate($data->vendor->expiration) . "</p>"');
}

array_push($columns, $expDateColumn);*/

$dataArray = array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $dataProvider,
    'columns' => $columns,
);

$this->renderPartial('//layouts/userHeader', array('model' => $model, 'breadcrumbs' => $breadcrumbs));
?>

<div class="ausa-contentMainInner">
    <div class="widget-content no-head-foot" style="padding: 0px;">
        <div class="ausa-vendorDashboard">
            <div id="vendor-mainDashboard" style="height: 307px;">
                <div id="map_holder" class="dashboardHighlight" style="padding: 0px; height: 100%; clear: both; overflow: hidden;">
                    <div id="map_canvas" style="height: 100%;"></div>
                </div>
            </div>
            <div class="widget-content dashboard no-foot">
                <?php $this->renderPartial('dashActions/_dashboardLinks', array('model' => $model), false, false); ?>
            </div>
        </div>
    </div>
</div>
</div>

<div class="ausa-contentMain row-fluid">
    <?php
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Vendors'));
    $this->widget('ausa.widgets.AOButtonGroup', array(
        'type' => '',
        'buttons' => array(
            array('icon' => 'cog', 'items' => array(
                    array('label' => 'Add a Vendor', 'icon' => 'plus', 'url' => url('/vendor/create')),
                    '---',
                    array('label' => 'Get Help', 'icon' => 'question-sign', 'url' => '#'),
            )),
        ),
    ));
    $this->endWidget();
    $this->widget('bootstrap.widgets.TbGridView', $dataArray);
    $this->endWidget();
    ?>
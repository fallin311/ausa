<?php

/**
 * This is just here so I don't lose the code for rendering the google charts (and so I dont have to scan through the repository for it).
 * Disregard, it is not used live anywhere.
 */

cs()->registerScriptFile(CGoogleApi::$bootstrapUrl, CClientScript::POS_HEAD);

echo CHtml::script("
var chartInit = false,
drawVisitorsChart = function() {
    var data = new google.visualization.DataTable();
    var raw_data = [
        ['Views', 50, 73, 104, 129, 146, 176, 139, 149, 218, 194, 96, 53],
        ['Redemption', 82, 77, 98, 94, 105, 81, 104, 104, 92, 83, 107, 91],
        ['Something', 50, 39, 39, 41, 47, 49, 59, 59, 52, 64, 59, 51],
        ['Something Else', 45, 35, 35, 39, 53, 76, 56, 59, 48, 40, 48, 21]
    ];
        
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    
    data.addColumn('string', 'Month');
    for (var i = 0; i < raw_data.length; ++i) {
        data.addColumn('number', raw_data[i][0]);
    }
    data.addRows(months.length);
    for (var j = 0; j < months.length; ++j) {
        data.setValue(j, 0, months[j]);
    }
    for (var i = 0; i < raw_data.length; ++i) {
        for (var j = 1; j < raw_data[i].length; ++j) {
            data.setValue(j-1, i+1, raw_data[i][j]);
        }
    }
    var div = $('#stat-chart'),
    divWidth = div.width();
    new google.visualization.LineChart(div.get(0)).draw(data, {
        title: 'Vendor Influx Graph (under construction)',
        width: divWidth,
        height: 269,
        legend: 'right',
        yAxis: {title: '(thousands)'},
        backgroundColor: 'transparent',
        legendTextStyle: { color: 'white' },
        titleTextStyle: { color: 'white' },
        hAxis: {
            textStyle: { color: 'white' }
        },
        vAxis: {
                textStyle: { color: 'white' },
                baselineColor: '#666666'
        },
        chartArea: {
                top: 35,
                left: 30,
                width: divWidth-40
        },
        legend: 'bottom'
    });
    if (chartInit) {
        notify('Chart resized', 'The width change event has been triggered.', {
            icon: 'img/demo/icon.png'
        });
    }
    chartInit = true;
};
google.load('visualization', '1', {
    'packages': ['corechart']
});
google.setOnLoadCallback(drawVisitorsChart);
//$('#demo-chart').widthchange(drawVisitorsChart);
$(document).on('respond-ready', drawVisitorsChart);

	
");
?>

<div class="ausa-contentMainInner">
    <div class="widget-content no-head-foot" style="padding: 0px;">
        <div class="ausa-vendorDashboard">
            <div class="dashboardHighlight" style="clear: both; overflow: hidden;">
                <div class="columns">
                    <div id="stat-chart" style="float: left; width: 70%; min-width: 760px;"></div>
                    <div style="float: left; width: 30%;">
                        <ul class="stats">
                            <?php
                            $vendorCount = $model->chapter->getTotalVendorCount();
                            ?>
                            <li><strong><a href="#" class="stats-popover" rel="clickover" data-placement="top" data-content="This is the total number of vendors brought in by <?php echo $model->st_name; ?>." data-original-title="What is this?"><?php echo $model->getTotalVendors(); ?></a>/<a href="#" class="stats-popover" rel="clickover" data-placement="top" data-content="This is the total number of vendors within <?php echo Chapter::model()->findByPk($model->chapter_id)->chapter_name; ?>." data-original-title="What is this?"><?php echo $vendorCount; ?></a></strong> total <br><?php echo $vendorCount == 1 ? 'vendor' : 'vendors'; ?></li>
                            <li><strong><?php echo $model->chapter->getTotalMemberCount(); ?></strong> active <br>chapter members</li>
                            <li><strong><?php echo $model->chapter->getTotalActiveOffers(); ?></strong> active <br>offers</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

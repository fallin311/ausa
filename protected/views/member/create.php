<?php
$this->pageTitle = Yii::app()->name . ' - Chapter Update';
$this->layout = '//layouts/full';
$breadcrumbs = array(
    'breadcrumbs not set'
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));

$this->beginWidget('application.components.widgets.AOStickyBar', array('sticky' => true, 'panelWrap' => true));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'icon' => 'remove white',
    'type' => 'danger',
    'url' => url('/member'),
    'label' => 'Cancel',
    'htmlOptions' => array('style' => 'margin: 5px 25px 0px 8px; float: right',)
));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'ajaxSubmit',
    'type' => 'success',
    'url' => url('/member/create'),
    'htmlOptions' => array(
        'style' => 'margin: 5px 8px; float: right',
        'id' => 'save-member-button',
    ),
    'loadingText' => 'processing...',
    'ajaxOptions' => array(
        'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
        'beforeSend' => 'function(){$("#save-member-button").button("loading")}',
        'success' => 'function(response){
                
                if (response.length < 5){
                    // saved, lets go one step further
                    window.location.href="' . url('/member/update') . '/" + response;
                }else{
                    console.log(response);
                    $("#save-member-button").button("reset");

                    // we are going to display error message
                    $("#ajax-errors-holder").show();
                    $("#ajax-errors").html(response);
                    showError(\'Could not save the member. Please check the error fields below.\');
                }
            }',
    ),
    'icon' => 'ok white',
    'label' => 'Save',
));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'ajaxSubmit',
    'type' => '',
    'url' => url('/member/create'),
    'htmlOptions' => array(
        'style' => 'margin: 5px 8px; float: right',
        'id' => 'apply-member-button',
    ),
    'loadingText' => 'processing...',
    'ajaxOptions' => array(
        'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
        'beforeSend' => 'function(){$("#apply-member-button").button("loading")}',
        'success' => 'function(response){
                
                if (response.length < 5){
                    // saved, lets reset the forms
                    $("#member-form").find(\'input:text\').val("");;
                    showSuccess(\'The member has been created. Feel free to add another one.\');
                    $("#apply-member-button").button("reset");
                }else{
                    console.log(response);
                    $("#apply-member-button").button("reset");

                    // we are going to display error message
                    $("#ajax-errors-holder").show();
                    $("#ajax-errors").html(response);
                    showError(\'Could not save the member. Please check the error fields below.\');
                }
            }',
    ),
    'icon' => 'ok',
    'label' => 'Apply',
));
$this->endWidget();
?>

<div class="ausa-contentMain row-fluid">

    <div style="width: 100%; position: relative; float: left; display: none;" id="ajax-errors-holder">
<?php
cs()->registerCss('notice-overwrite', '.notification {
    margin-top: 0px;
    margin-bottom: 40px;
    }');
$this->widget('application.components.widgets.AONotice', array('notice' => '<div id="ajax-errors"></div>', 'noticeType' => 'errors'));
?>
    </div>

        <?php
// Member form
        $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
        $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'New Member'));
        $this->endWidget();
        echo $this->renderPartial('_member_form', array('model' => $model));
        $this->endWidget();

// Membership form
        $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
        $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Chapter Membership'));
        $this->endWidget();
        echo $this->renderPartial('_membership_form', array('model' => $membership, 'member' => $model));
        $this->endWidget();
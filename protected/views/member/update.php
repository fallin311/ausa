<?php

$this->layout = '//layouts/full';
$breadcrumbs = array(
    'breadcrumbs not set'
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
    // make control buttons fixed at the top of screen
    $this->beginWidget('application.components.widgets.AOStickyBar', array('sticky' => true, 'panelWrap' => true));
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'link',
        'icon' => 'remove white',
        'type' => 'danger',
        'url' => url('/member'),
        'label' => 'Cancel',
        'htmlOptions' => array('class' => 'pull-right', 'style' => 'margin: 5px 25px 0px 8px',)
    ));

    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'ajaxSubmit',
        'type' => 'success',
        'url' => url('/member/update/' . $model->member_id),
        'htmlOptions' => array(
            'class' => 'pull-right',
            'style' => 'margin: 5px 8px',
            'id' => 'save-member-button',
        ),
        'loadingText' => 'processing...',
        'ajaxOptions' => array(
            'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
            'beforeSend' => 'function(){$("#save-member-button").button("loading")}',
            'success' => 'function(response){
                
                if (response.length < 5){
                    window.location.href="' . url('/member') . '/";
                }else{
                    $("#save-member-button").button("reset");

                    // we are going to display the error message
                    $("#ajax-errors-holder").show();
                    $("#ajax-errors").html(response);
                    showError(\'Could not save the member. Please check the error fields below.\');
                }
            }',
        ),
        'icon' => 'ok white',
        'label' => 'Save',
    ));
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'ajaxSubmit',
        'type' => '',
        'url' => url('/member/update/' . $model->member_id),
        'htmlOptions' => array(
            'class' => 'pull-right',
            'style' => 'margin: 5px 8px',
            'id' => 'apply-member-button',
        ),
        'loadingText' => 'processing...',
        'ajaxOptions' => array(
            'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
            'beforeSend' => 'function(){$("#apply-member-button").button("loading")}',
            'success' => 'function(response){
                
                if (response.length < 5){
                    showSuccess(\'Member changes have been updated.\');
                    $("#apply-member-button").button("reset");
                    $("#ajax-errors-holder").hide();
                }else{
                    $("#apply-member-button").button("reset");

                    // we are going to display the error message
                    $("#ajax-errors-holder").show();
                    $("#ajax-errors").html(response);
                    showError(\'Could not save the member. Please check the error fields below.\');
                }
            }',
        ),
        'icon' => 'ok',
        'label' => 'Apply',
    ));
    $this->endWidget();
?>

<div style="width: 100%; margin-bottom: 25px; position: relative; float: left; display: none;" id="ajax-errors-holder">
    <?php
    // display validation errors here
    $this->widget('application.components.widgets.AONotice', array('notice' => '<div id="ajax-errors"></div>', 'noticeType' => 'errors'));
    ?>
</div>

<?php
// render Member form
$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
$this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Update Member'));
$this->endWidget();
echo $this->renderPartial('_member_form', array('model' => $model));
$this->endWidget();

// render Membership form
$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
$this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Chapter Membership'));
$this->endWidget();
echo $this->renderPartial('_membership_form', array('model' => $membership, 'member' => $model));
$this->endWidget();
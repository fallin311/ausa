<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'payment-form',
    //'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => false,
        ));

echo $form->errorSummary($model);

$chbox = '<label class="checkbox" for="MembershipPayment_useBillingAddress">' . $form->checkBox($payment, 'useBillingAddress') . 'same as my mailing address</label>';
echo $form->customRow($payment, 'useBillingAddress', 'Billing Address', $chbox);

// render optional custom billing address
echo "<div style='" . ($payment->useBillingAddress ? 'display: none;' : '') . "' id='billing_info'>";
echo $form->textFieldRow($payment, 'firstName', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($payment, 'lastName', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($payment, 'phone', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($payment, 'email', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($payment, 'address1', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($payment, 'address2', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($payment, 'city', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($payment, 'state', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($payment, 'zipcode', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($payment, 'country', array('class' => 'span', 'maxlength' => 128));
echo "</div>";

echo $form->textFieldRow($payment, 'cardNumber', array('class' => 'span', 'maxlength' => 16));

echo $form->textFieldRow($payment, 'cardCode', array('class' => 'span', 'maxlength' => 4));

$expMonthInput = $form->dropDownList($payment, 'cardExpMonth', Plan::$months, array('style' => 'width:40%; margin-right:20px;'));
$expYearInput = $form->dropDownList($payment, 'cardExpYear', Plan::getExpYearArray(11), array('style' => 'width:25%;'));

echo $form->customRow($payment, 'exp_date', 'Expiration Date', $expMonthInput . $expYearInput);

echo $form->customRow($payment, 'cardNumber', 'Supported Cards', '<img src="' . url('/images/payment_types.png') . '" alt="Visa, Master Card, American Express" />', array('style' => 'height: 60px;'));

cs()->registerScript('toggle-billing-address', '
$("#MembershipPayment_useBillingAddress").change(function(e){
if ($("#MembershipPayment_useBillingAddress").is(":checked")) {
    $("#billing_info").hide("fast");
} else {
    $("#billing_info").show("fast");
}
});
');

$this->endWidget();

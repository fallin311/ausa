<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'membership-form',
    //'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => false,
));

cs()->registerCss('select2-css',
    '#s2id1 {
        width: 100%;
        margin-bottom: 10px;
    }');

echo $form->errorSummary($model);

// chapter id
if (app()->user->isAdmin()){
    echo '<div class="form-row" style="width: 100%;">'
        .'<label for="Vendor_chapterId" class="required">Chapter </label>'
        .'<div class="form-element"><div class="clearfix">';
    $this->widget('ext.select2.ESelect2', array(
        'model' => $model,
        'attribute' => 'chapter_id',
        'data' => CHtml::listData(Chapter::model()->findAll(), 'chapter_id', 'chapter_name'),
        'options' => array('placeholder' => 'Select Chapter'),
        'htmlOptions' => array('id' => 'chapter_id', 'name' => 'Membership[chapter_id]')
    ));
    echo '</div></div></div>';

} else {
    echo CHtml::hiddenField('Membership[chapter_id]', app()->user->getActiveChapter());
}
echo $form->textFieldRow($model, 'subchapter_id', array('class' => 'span', 'maxlength' => 128));
/*echo $form->dropDownListRow($model, 'ausa_product_code', array(
    'IND' => 'IND',
    'DESCORP' => 'DESCORP',
    'DESUS' => 'DESUS',
    'LIFEFULL' => 'LIFEFULL',
    'LIFE55' => 'LIFE55'
));*/
//echo $form->dropDownListRow($member, 'army_status', Member::$statuses, array('class' => 'span'));
//echo $form->textFieldRow($member, 'military_unit', array('class' => 'span', 'maxlength' => 128));

echo $form->datepickerRow($model, 'join_date', array('class' => 'span', 'maxlength' => 128));
echo $form->datepickerRow($model, 'expiration_date', array('class' => 'span', 'data-date-format' => 'mm/dd/yy', 'maxlength' => 128));
echo $form->checkBoxRow($model, 'active', array('hint' => 'Uncheck to disable access for this member.'));
if ($member->isNewRecord){
    echo $form->checkBoxRow($member, 'sendRegistrationEmail', array('hint' => 'Send login information to membmer.'));
}
$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->endWidget();

$this->endWidget();
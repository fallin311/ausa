<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'member-form',
    //'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => false,
        ));

echo $form->errorSummary($model);

echo $form->dropDownListRow($model, 'prefix', Member::$prefixes, array('class' => 'span'));
echo $form->textFieldRow($model, 'fullName', array('class' => 'span', 'maxlength' => 128));
//echo $form->textFieldRow($model, 'first_name', array('class' => 'span', 'maxlength' => 128));
//echo $form->textFieldRow($model, 'last_name', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'email_address', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'repeatEmail', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'phone_number', array('class' => 'span', 'maxlength' => 128));
echo $form->dateDropDownFieldRow($model, 'birthday', array('class' => 'span', 'data-validate' => 'phone, maxLength(14)'));

if (app()->user->isChapterUser()){
    echo $form->textFieldRow($model, 'ausa_id', array('class' => 'span', 'maxlength' => 128));
}

$this->endWidget();

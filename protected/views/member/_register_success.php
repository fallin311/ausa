<?php
/*
 * Registration was finished and payment recieved. Let's inform the member
 */
?>
<div class="row-fluid">
    <div id="column-holder" class="span12 wrapper">
        <?php $this->beginWidget('bootstrap.widgets.TbHeroUnit', array('heading' => 'Congratualtions!')); ?>
        <p>Please check your email <strong><?php echo $model->email_address ?></strong> for login credentials.</p>
        <p>Download the mobile AUSA Offers app from the app store and start enjoying great benefits and services today!</p>
        <div class="mobios-badge-container">
            <a class="mobios-badge" href="https://itunes.apple.com/us/app/ausa-offers/id589070238?mt=8" target="_blank"><img src="<?php echo $this->widget('application.components.widgets.AOAppHome')->assetsUrl; ?>/badge_ios.png" alt="Available on the App Store"></a>
            <a class="mobios-badge second" href="https://play.google.com/store/apps/details?id=com.ausaoffers.offers" target="_blank"><img src="<?php echo $this->widget('application.components.widgets.AOAppHome')->assetsUrl; ?>/badge_android.png" alt="Get it on Android"></a>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
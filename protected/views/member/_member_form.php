<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'member-form',
    //'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => false,
        ));

echo $form->errorSummary($model);

echo $form->textFieldRow($model, 'first_name', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'last_name', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'prefix', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'email_address', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'repeatEmail', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'phone_number', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'address1', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'address2', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'city', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'state', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'zipcode', array('class' => 'span', 'maxlength' => 128));
//echo $form->textFieldRow($model, 'country', array('class' => 'span', 'maxlength' => 128));
echo $form->dateDropDownFieldRow($model, 'birthday', array('class' => 'span', 'minAge' => 17, 'maxAge' => 100)); // birthday select with min/max age years set - travis
if (app()->user->isChapterUser()){
    echo $form->textFieldRow($model, 'ausa_id', array('class' => 'span', 'maxlength' => 128));
}

$this->beginWidget('application.components.widgets.AOPanelFoot');
/*
$this->widget('bootstrap.widgets.TbButton', array(
  'buttonType' => 'submit',
  'type' => 'success',
  'htmlOptions' => array('style' => 'margin: 7px 5px; float: right'),
  'size' => 'small',
  'icon' => 'ok white',
  'label' => $model->isNewRecord ? 'Create' : 'Update',
  ));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'type' => '',
    'url' => url('/category'),
    'htmlOptions' => array('style' => 'margin: 7px 5px; float: left'),
    'size' => 'small',
    'icon' => 'remove',
    'label' => 'Cancel',
));*/

$this->endWidget();

$this->endWidget();
<?php
$this->layout = '//layouts/aoHomeHeader';
$this->pageTitle = Yii::app()->name . ' - Become a Member';
$this->heading =
        '<div class="container">'
        . '<h1>Become an AUSA Member</h1>'
        . '<p class="lead">Sign up today and receive exclusive offers and discounts from area vendors.</p>'
        . '</div>';

cs()->registerScript('control-buttons', '
    
function displayRegistrationForm(){
    $("#registration-form-1 :input").removeAttr("disabled");
    $("#registration-confirm-form").hide();
    $("#registration-form-1").show();
    $("#ajax-errors-holder").hide();
}

function saveRegistration(btn, finalSubmission){
$.ajax({
    data: serializeAll()+"&isAjaxRequest=1"+(finalSubmission ? "&final=1" : ""),
    beforeSend: function(){
        btn.button("loading");
        $("#registration-form-1 :input").attr("disabled", true);
        //console.log(serializeAll());
    },
    dataType: "json",
    type: "post",
    url: "' . url('/member/register') . '",
    success: function(response){
        console.log(response);
        if (btn){
            btn.button("reset");
        }
        if (response.action == "confirm"){
            $("#registration-form-1").hide();
            $("#registration-confirm-form").html(response.confirm).show();
        } else if (response.action == "success"){
            $("#registration-confirm-form").html(response.confirm);
        } else if(response.action == "register") {
            displayRegistrationForm();
            $("#ajax-errors-holder").show();
            $("#ajax-errors").html(response.errors);
            showError(\'Could not finish the registration. Please check the error fields below.\');
        }
    },
    error: function(){
       if (btn){
            btn.button("reset");
       }
       showError(\'Something went wrong, please try this page later.\');
    }
});
}

$(".register-button").click(function(){saveRegistration($(this));});

// step back from confirmation screen
$("#reg-back-button").live("click", function(){
    displayRegistrationForm();
});

// send it out to the payment gateway
$("#reg-confirm-button").live("click", function(){

    // disable buttons
    $("#reg-confirm-button").attr("disabled", true);
    $("#reg-back-button").attr("disabled", true);
    
    // send data to the server, make sure the fields are not disabled, so the
    // value can be read
    $("#registration-form-1 :input").removeAttr("disabled");
    finalSubmission = true;
    console.log(serializeAll()+"&isAjaxRequest=1"+(finalSubmission ? "&final=1" : ""));
    saveRegistration($(this), true);
});
');
?>

<div id="registration-form-1">
    <div class="row-fluid">
        <div id="column-holder" class="span12 wrapper cols">
            <?php $this->beginWidget('bootstrap.widgets.TbHeroUnit', array('heading' => 'AUSA members enjoy great benefits and services!')); ?>
            <p>Great deals from AUSA Offers is one of the many benefits you will be entitled to access immediately. Visit <a href="http://www.ausa.org" target="_blank">http://www.ausa.org</a> to see
                more information about membership. Start saving today!</p>
            <?php $this->endWidget(); ?>
        </div>
    </div>

    <div style="width: 100%; position: relative; float: left; display: none;" id="ajax-errors-holder">
        <?php
        cs()->registerCss('notice-overwrite', '#ausa-stickyBarWrap.no-js { height: 0px; }', '.notification {'
                . 'margin-top: 0px;'
                . 'margin-bottom: 40px;'
                . '}');
        $this->widget('application.components.widgets.AONotice', array('notice' => '<div id="ajax-errors"></div>', 'noticeType' => 'errors', 'noticeClosable' => false));
        ?>
    </div>
    <div class="row-fluid">
        <div id="column-holder" class="span6 wrapper cols">
            <div class="heading-bar">
                <h3 class="pull-left">Contact Information</h3>
            </div>
            <?php
            $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
            echo $this->renderPartial('_contact_form', array('model' => $model));
            $this->endWidget();
            ?>
        </div>

        <div id="column-holder" class="span6 wrapper cols">
            <div class="heading-bar">
                <h3 class="pull-left">Mailing Address</h3>
            </div>
            <?php
            $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
            echo $this->renderPartial('_address_form', array('model' => $model));
            $this->endWidget();
            ?>
        </div>
    </div>

    <div class="row-fluid">
        <div id="column-holder" class="span6 wrapper cols">
            <div class="heading-bar">
                <h3 class="pull-left">AUSA Chapter and Status</h3>
            </div>
            <?php
            $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
            echo $this->renderPartial('_chapter_join_form', array('model' => $model, 'membership' => $membership, 'payment' => $payment));
            $this->endWidget();
            ?>
        </div>

        <div id="column-holder" class="span6 wrapper cols">
            <div class="heading-bar">
                <h3 class="pull-left">Payment Information</h3>
            </div>
            <?php
            $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
            echo $this->renderPartial('_payment_form', array('model' => $model, 'membership' => $membership, 'payment' => $payment));
            $this->endWidget();
            ?>
        </div>
    </div>

    <div class="row-fluid">
        <div id="column-holder" class="span12 wrapper">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'action',
                'type' => 'success',
                'htmlOptions' => array('style' => 'margin: 7px', 'class' => 'register-button pull-right'),
                'size' => 'large',
                'icon' => 'icon-arrow-right icon-white',
                'label' => 'Proceed',
                'loadingText' => '<i class="icon-spinner icon-spin"></i> Processing...',
            ));
            ?>
        </div>
    </div>

</div>

<?php // this is where the confirm information will be rendered  ?>
<div id="registration-confirm-form"></div>
<?php


$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'address-form',
    //'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => false,
        ));

echo $form->errorSummary($model);

echo $form->textFieldRow($model, 'address1', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'address2', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'city', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'state', array('class' => 'span', 'maxlength' => 128));
echo $form->textFieldRow($model, 'zipcode', array('class' => 'span', 'maxlength' => 128));
//echo $form->textFieldRow($model, 'country', array('class' => 'span', 'maxlength' => 128));
echo $form->customRow($model, 'country', 'Country', CHtml::textField('Member_country', 'United States', array('disabled' => 'disabled', 'class' => 'span')));

if (app()->user->isChapterUser()){
    echo $form->textFieldRow($model, 'ausa_id', array('class' => 'span', 'maxlength' => 128));
}

$this->endWidget();

<?php
$this->pageTitle = Yii::app()->name . ' - Upload Members';
$this->layout = '//layouts/full';
$breadcrumbs = array(
    'breadcrumbs not set'
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));

$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));

$this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons box_outgoing', 'panelTitle' => 'Mass Upload'));
?>
<div class="widget-content">
    <h1>AUSA Member Upload</h1>
    <p>
        Use this form to synchronize members downloaded form the official AUSA webpage and AUSA offers. Please follow these recommendations:
    </p>
    <ol>
        <li>Use <b>CSV</b> format only</li>
        <li>File has to be smaller than <b>10MB</b></li>
        <li>Upload the <b>newest version</b> only. Every change (subscription expiration, email address ...) will be updated in the database.</li>
        <li>Make sure these columns are present in the file: First Name, Last Name, Primary Email, Join Date, Expires, Chapter</li>
    </ol>
</p>
<p>
    It takes up to 10 minutes to process the file once it is uploaded.
</p>
<?php
// load the assets, we are reusing imageupload widget
$url = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.extensions.imageupload.assets'), false, -1, false);
cs()->registerScriptFile($url . "/fileuploader.js");
cs()->registerCSSFile($url . '/fileuploader.css');

$browser = new EWebBrowser();
if ($browser->browser == EWebBrowser::BROWSER_CHROME || $browser->browser == EWebBrowser::BROWSER_FIREFOX) {
    $buttonLabel = '<div class="qq-button-title-2">Upload a file</div><div class="qq-button-subtitle">or drag and drop here</div>';
} else {
    $buttonLabel = '<div class="qq-button-title-1">Upload a file</div>';
}
cs()->registerScript('upload-csv-file', "
            var uploader = new qq.FileUploader({
                element: document.getElementById('csv-upload-button'),
                action: '" . url('/member/uploadCsv') . "',
                onComplete: function(){
                    showSuccess('Your file has been uploaded. It will be processed shortly.');
                },
                template: '<div class=\"qq-uploader\">' + 
                '<div class=\"qq-upload-drop-area\"><span>Drop files here to upload</span></div>' +
                '<div class=\"btn btn-primary btn-small qq-upload-button\">" . $buttonLabel . "</div>' +
                '<div class=\"qq-result\"><ul class=\"qq-upload-list\"></ul><div class=\"qq-uploaded\"></div></div>' + 
                '</div>',
                
                multiple: 'false'
            });
            
", CClientScript::POS_READY);
//" . ($this->onComplete ? ('onComplete: ' . $this->onComplete . ',') : '') . "
?>
</div>
<?php $this->beginWidget('application.components.widgets.AOPanelFoot'); ?>
<div class="ausa-massUpload">
    <div class="upload-button" id="csv-upload-button">Upload CSV File</div>
</div>

<?php
$this->endWidget();
$this->endWidget();
?>

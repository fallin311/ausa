<?php

$this->layout = '//layouts/full';
$breadcrumbs = array(
    'breadcrumbs not set'
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
?>

<div class="ausa-contentMain row-fluid">

<?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false)); ?>
    <?php $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck' ,'panelTitle' => 'Members')); ?>
        <?php
            $this->widget('ausa.widgets.AOButtonGroup', array(
                'type'=>'',
                'buttons'=>array(
                    array('icon'=>'cog', 'items'=>array(
                        array('label'=>'Add a Member', 'icon' => 'plus', 'url'=>url('/member/create')),
                        '---',
                        array('label'=>'Get Help', 'icon' => 'question-sign', 'url' => '#'),
                    )),
                ),
            ));
        ?>
    <?php $this->endWidget(); ?>
    <?php
    $columns = array(
            //array('name' => 'vendor_id', 'header' => '#'),
            //array('name' => 'ausa_id'),
            array('name' => 'last_name', 'header' => 'Name', 'value' => '$data->getFullName()'),
            array('name' => 'email_address', 'header' => 'Email'),
            array('name' => 'phone_number', 'value' => 'Shared::formatPhone($data->phone_number)'),
            //array('name' => 'membership.ausa_product_code'),
            array('name' => 'membership.expiration_date')
            //array('name' => 'email_address', 'header' => 'Email'),
            );
    if (app()->user->isAdmin()){
        $columns[] = array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{update}{delete}',
            );
    }else{
        $columns[] = array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{update}',
            );
    }

    $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => 'striped bordered condensed',
        'filter' => $model,
        'dataProvider' => $model->search(),
        //'template'=>"{items}",
        'columns' => $columns,
        
    ));
    ?>

<?php $this->endWidget(); ?>
<?php
/**
 * Validate all information before the form is passed to the payment gateway
 */
?>

<div class="row-fluid">
    <div id="column-holder" class="span12 wrapper cols">
        <?php $this->beginWidget('bootstrap.widgets.TbHeroUnit', array('heading' => 'Please confirm your information')); ?>
        <p>Your login credentials will be sent to <strong><?php echo $model->email_address?></strong>.</p>
        <p>Once the payment goes through, we will send you an email with your login credentials and forward 
            your membership information to your chapter.</p>

        <?php
        $this->endWidget();
        ?>
    </div>
</div>
    
<div class="row-fluid">
    <div id="column-holder" class="span6 wrapper cols">
        <div class="heading-bar">
            <h3 class="pull-left">Contact Information</h3>
        </div>
        <?php
        // Personal
        $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
        $this->widget('bootstrap.widgets.TbDetailView', array(
            'data' => $model,
            'attributes' => array(
                array('label' => 'Rank/Prefix', 'value' => $model->prefix),
                array('label' => 'Name', 'value' => $model->first_name . " " . $model->last_name),
                array('label' => 'Address', 'type' => 'raw', 'value' => Shared::formatAddress($model)),
                array('name' => 'email_address'),
                array('name' => 'phone_number'),
                array('name' => 'army_status'),
                array('name' => 'military_unit'),
                array('label' => 'Level', 'value' => MembershipPayment::$levels[$payment->level]['title']),
                array('label' => 'birthday', 'value' => $model->birthday ? Shared::formatShortUSDate($model->birthday) : 'N/A'),
            ),
        ));
        $this->endWidget();
        ?>
    </div>

    <div id="column-holder" class="span6 wrapper cols">
        <div class="heading-bar">
            <h3 class="pull-left">Chapter Membership</h3>
        </div>
        <?php
        // Chapter
        $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
        // predicted expiration date if the transaction goes through
        $expiration = strtotime(Shared::toDatabase($membership->getNewStartDate()) . " +12 month");

        $this->widget('bootstrap.widgets.TbDetailView', array(
            'data' => $membership,
            'attributes' => array(
                array('label' => 'Chapter', 'value' => $membership->chapter->chapter_name),
                array('label' => 'City', 'value' => $membership->chapter->city),
                array('label' => 'Starts on', 'value' => Shared::formatShortUSDate($membership->getNewStartDate())),
                array('label' => 'Expires on', 'value' => Shared::formatShortUSDate($expiration)),
            ),
        ));
        $this->endWidget();
        ?>
    </div>
    
    <div id="column-holder" class="span6 wrapper cols">
        <div class="heading-bar">
            <h3 class="pull-left">Payment Information</h3>
        </div>
        <?php
        // Payment
        $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
        $this->widget('bootstrap.widgets.TbDetailView', array(
            'data' => $payment,
            'attributes' => array(
                //array('label' => 'Amount', 'value' => MembershipPayment::$levels[$payment->level]['price'] . ' USD / year'),
                array('label' => 'Amount', 'value' => $payment->paymentAmount . ' USD / year'),
                array('label' => 'Card Number', 'value' => $payment->obscureNumber()),
                array('label' => 'Expiration', 'value' => $payment->cardExpMonth . '/' . $payment->cardExpYear),
                array('label' => 'Billing Name', 'value' => $payment->firstName . ' ' . $payment->lastName),
                array('label' => 'Billing Address', 'type' => 'raw', 'value' => Shared::formatAddress($payment)),
            ),
        ));
        $this->endWidget();
        ?>
    </div>
</div>

<div class="row-fluid">
    <div id="column-holder" class="span12 wrapper">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            //'buttonType' => 'ajaxSubmit',
            'type' => 'action',
            'size' => 'large',
            //'url' => url('/branch/update'),
            'htmlOptions' => array('id' => 'reg-back-button',
                'class' => 'pull-left',
                'style' => 'margin: 7px'),
            'icon' => '  icon-chevron-left',
            'label' => 'Make Changes',
        ));

        $this->widget('bootstrap.widgets.TbButton', array(
            //'buttonType' => 'ajaxSubmit',
            'type' => 'success',
            'size' => 'large',
            //'url' => url('/branch/update'),
            'htmlOptions' => array('id' => 'reg-confirm-button', 
                'class' => 'pull-right',
                'style' => 'margin: 7px'),
            'icon' => 'icon-ok icon-white',
            'label' => 'Process Registration',
            'loadingText' => '<i class="icon-spinner icon-spin"></i> Processing...',
        ));
        ?>
    </div>
</div>

<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'chapter-join-form',
    //'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => false,
        ));

echo $form->errorSummary($model);

$chapterSelect = $this->widget('ext.select2.ESelect2', array(
        'model' => $membership,
        'attribute' => 'chapter_id',
        'data' => CHtml::listData(Chapter::model()->findAllBySql("SELECT * FROM chapter WHERE usaepay_key IS NOT NULL AND usaepay_pin IS NOT NULL AND disabled = 0"), 'chapter_id', 'chapter_name'),
        'options' => array('placeholder' => 'Select Chapter'),
        'htmlOptions' => array('id' => 'chapter_id', 'name' => 'Membership[chapter_id]', 'class' => 'span')
    ), true);

echo $form->customRow($membership, 'chapter_id', 'Chapter', $chapterSelect, array('style' => 'height: 60px', 'hint' => 'Select your local chapter - this membership is valid for one chapter only.', 'class' => 'span'));


if (app()->user->isChapterUser()){
    echo $form->textFieldRow($model, 'ausa_id', array('class' => 'span', 'maxlength' => 128));
}

echo $form->dropDownListRow($model, 'army_status', Member::$statuses, array('class' => 'span'));
echo $form->textFieldRow($model, 'military_unit', array('class' => 'span', 'maxlength' => 128));
echo $form->dropDownListRow($payment, 'level', MembershipPayment::getLevelList(), array('class' => 'span'));

$this->endWidget();

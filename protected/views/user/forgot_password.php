<?php
$this->layout = '//layouts/aoHomeNarrow';
cs()->registerScript('autofocus', '
    $("#User_email_address").focus();
');
?>
<h1 class="title">Forgot Password</h1>
    <?php if ($model->isNewRecord) { ?>                    
        <div class="mid-header">Please provide your email address in the field below to reset your password.</div>
        <?php if ($emailSent) { ?>
            <?php cs()->registerScript('alert','(function(){ showError(\'We cannot find your user account. Please try again.\'); })();'); ?>
        <?php } ?>
        <?php
        $form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
            'id' => 'user-form',
            'htmlOptions' => array('class' => 'reset'),
            'enableAjaxValidation' => true,
        )); ?>
        <ul class="fields">
            <li>
                <i class="icon-envelope-alt"></i>    
                <?php echo $form->textField($model, 'email_address', array('autocomplete' => 'off', 'placeholder' => 'email address')); ?>
            </li>
        </ul>
        <div class="mid-form">
            <span class="login-checkbox pull-left">
                <?php echo CHtml::hiddenField('ajax', '0'); ?>
            </span>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'label' => 'Reset Password',
                'type' => 'info',
                'htmlOptions' => array('class' => 'pull-right'),
                'block' => true,
                'size' => 'large',
            ));
            ?>
        </div>
        <?php
        $this->endWidget();
    } else {
        ?>
        <div class="inset-margin">
            <div class="mid-footer">
                Please check your email for instructions on resetting your account password. The email was sent to <code><?php echo $model->email_address; ?></code>.
            </div>
        </div>
<?php } ?>
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$this->layout = 'empty';
cs()->registerCss('mobile-preview', '
.mobile-left {
  position: relative;
  float: left;
  height: 850px;
  width: 430px;
  background-image: url(' . url('/images/iphone5-small.png') . ');
  background-repeat: norepeat;
}

.mobile-right {
  position: relative;
  float: left;
  height: 850px;
  width: 250px;
  
}

.mobile-info {
  //background-color: white;
  color: black;
  margin-top: 30px;
  background-repeat:no-repeat;
  background-position:10px 10px;
  background-color:#d8e7fa;
  border-color:#9dbfea;
  color: #00357b;
  padding: 15px;
  border-radius: 5px;
}

.white-box {
  background-color: white;
}

.mobile-info ul li {
  list-style: disc;
}

.mobile-title {
  margin-top: 30px;
}

.mobile-title .line1, .mobile-title .line2 {
  color: white;
  font-weight: bold;
}

.mobile-title .line1 {
  font-size: 75px;
  line-height: 60px;
}

.mobile-title .line2 {
  font-size: 60px;
  line-height: 60px;
  margin-bottom: 10px;
}

.login-info {
  border-radius: 5px;
  background-color: white;
  color: #555555;
  padding: 15px;
  margin-top: 10px;
}

.login-line {
  font-size: 22px;
}

.login-line .cred1 {
    color: #aaaaaa;
    width: 80px;
    display: inline-block;
}

.login-line .cred {
  color: black;
}

');

// create login credentials and access token
$model->getAccessToken();

?>
<div style="width: 680px">
    <div class="mobile-left">
        <iframe style="position: absolute; top: 140px; left: 52px; width: 320px; height: 570px; border: 0;" src="<?php echo app()->params['mobileUrl']; ?>"></iframe>
    </div>
    <div class="mobile-right">
        <div class="mobile-title">
            <div class="line1">PHONE</div>
            <div class="line2">PREVIEW</div>
        </div>
        <div class="login-info">
            <div style="line-height: 30px;">Use the following credentials to login:</div>
            <div class="login-line"><div class="cred1">LOGIN:</div><span class="cred"> <?php echo $model->mobile_username?></span></div>
            <div class="login-line"><div class="cred1">PIN:</div><span class="cred"> <?php echo $model->mobile_password?></span></div>
        </div>
        <div class="mobile-info">
            Preview your offer in a sand box before it is published to members.
            This test phone app is almost identical to the one members use with
            the following exceptions:
            <ul style="margin-top: 10px;">
                <li>You can see all your offers - published, unpublished or expired</li>
                <li>There are no other vendors</li>
                <li>There is no redemption button</li>
                <li>Favorites are not permanent</li>
            </ul>
        </div>
        <div class="mobile-info">
            To refresh changes press the top right button on your dashboard:
            <div style="text-align: center; margin-top: 10px;">
                <img src="<?php echo url('/images/refresh-offer.png') ?>" alt="refresh offer" />
            </div>
        </div>
        <div class="mobile-info white-box" style="text-align: center;">
            Supported on
            <a href="http://google.com/chrome"><img style="margin-left: 8px; margin-right: 8px;" src="<?php echo url('images/chrome.png')?>" /></a>
            and
            <a href="http://www.apple.com/safari/"><img style="margin-left: 8px; margin-right: 8px;" src="<?php echo url('images/safari.png')?>" /></a>
        </div>
    </div>
</div>
<?php
if(isset($new)){
    $title = 'Create User';
} else {
    $title = 'Account Settings';
} ?>

<h1 class="title"><?php echo $title; ?></h1>

<?php
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'user-form',
    'enableAjaxValidation' => false,
        ));
?>

<ul class="fields">

    <?php //echo $form->errorSummary($model); ?>

    <?php if ($vendor): ?>
        <li>
            <i class="icon-truck" title="Vendor"></i>
            <?php echo CHtml::tag('span', array('class' => 'uneditable-input'), $vendor->{'vendor_name'}); ?>
            <?php echo CHtml::hiddenField('vendor_id', $vendor->vendor_id); ?>
        </li>
    <?php elseif ($chapter): ?>
        <li>
            <i class="icon-star" title="Chapter"></i>
            <?php echo CHtml::tag('span', array('class' => 'uneditable-input'), $chapter->{'chapter_name'}); ?>
            <?php echo CHtml::hiddenField('chapter_id', $chapter->chapter_id); ?>
        </li>
    <?php elseif ($st): ?>
        <li>
            <i class="icon-group" title="Sales Team"></i>
            <?php echo CHtml::tag('span', array('class' => 'uneditable-input'), $st->{'st_name'}); ?>
            <?php echo CHtml::hiddenField('sales_team_id', $st->sales_team_id); ?>
        </li>
    <?php else: ?>
        <li>
            <i class="icon-truck" title="Vendor"></i>
            <?php echo $form->dropDownList(new Vendor, 'vendor_id', CHtml::listData(Vendor::model()->findAll(), 'vendor_id', 'vendor_name'), array('empty' => '', 'id' => "vendor_id", 'name' => 'vendor_id', 'class' => 'span3')); ?>
        </li>
        <li>
            <i class="icon-star" title="Chapter"></i>
            <?php echo $form->dropDownList(new Chapter, 'chapter_id', CHtml::listData(Chapter::model()->findAll(), 'chapter_id', 'chapter_name'), array('empty' => '', 'id' => "chapter_id", 'name' => 'chapter_id', 'class' => 'span3')); ?>
        </li>
        <li>
            <i class="icon-group" title="Sales Team"></i>
            <?php echo $form->dropDownList(new SalesTeam, 'sales_team_id', CHtml::listData(SalesTeam::model()->findAll(), 'sales_team_id', 'st_name'), array('empty' => '', 'id' => "sales_team_id", 'name' => 'sales_team_id', 'class' => 'span3')); ?>
        </li>
    <?php endif; ?>
    <li>
        <i class="icon-envelope-alt" title="Email Address"></i>
        <?php echo $form->textField($model, 'email_address', array('placeholder' => 'email address', 'maxlength' => 63,)); ?>
    </li>
    <li>
        <i class="icon-user" title="First Name"></i>
        <?php echo $form->textField($model, 'first_name', array('placeholder' => 'first name')); ?>
    </li>
    <li>
        <i class="icon-user" title="Last Name"></i>
        <?php echo $form->textField($model, 'last_name', array('placeholder' => 'last name')); ?>
    </li>
    <li>
        <i class="icon-globe" title="Address"></i>
        <?php echo $form->textField($model, 'address', array('placeholder' => 'address', 'maxlength' => 511)); ?>
    </li>
    <li>
        <i class="icon-phone" title="Phone Number"></i>
        <?php echo $form->textField($model, 'phone_number', array('placeholder' => 'phone number', 'maxlength' => 12)); ?>
    </li>
</ul>
<?php
// visible for admin only
if (app()->user->isAdmin()) {
    echo '<div class="mid-form" style="padding-bottom: 0;">';
    echo $form->checkBoxRow($model, 'super_admin');
    echo $form->checkBoxRow($model, 'login_disabled');
    echo '</div>';

    // company association .. this will be eventually a table
    /* echo $form->dropDownListRow(new Vendor, 'vendor_id', CHtml::listData(Vendor::model()->findAll(), 'vendor_id', 'vendor_name'), array('empty' => '', 'id' => "user_vendor_new",
      'name' => 'Vendors[user_vendor][]')
      ); */
    // edit
}
?>
<div class="mid-form"<?php if(isset($new)){?> style="padding-bottom: 0;"<?php } ?>>
    <span class="login-checkbox pull-left"></span>
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'label' => $model->isNewRecord ? 'Create' : 'Save Changes',
        'type' => 'primary',
        'htmlOptions' => array('class' => 'pull-right'),
        'block' => true,
        'size' => 'large',
    ));
    ?>
</div>

<?php if(!isset($new)){ ?>
<div class="mid-footer">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Change Password',
        'type' => 'link',
        'url' => array('/user/password/' . $model->user_id),
        'icon' => 'icon-key',
    ));
    ?>
</div>
<?php } ?>

<?php $this->endWidget(); ?>
<?php

$this->layout = '//layouts/aoHomeNarrow';

$breadcrumbs = array(
    'My Dashboard' => app()->user->getHomeUrl(),
    'My Account',
);

if (app()->user->hasFlash('success')) {
    if (app()->user->hasFlash('emailChange')) {
        cs()->registerScript('email-changed', 'setTimeout(function() { alert(\'' . app()->user->getFlash('emailChange') . '\'); }, 500);');
    }
    cs()->registerScript('updates', 'setTimeout(function() { showSuccess(\'' . app()->user->getFlash('success') . '\'); }, 500);');
}

if ($model->hasErrors()) {
    cs()->registerScript('alert', 'showError(\'There are errors in your submission.\');');
}

cs()->registerScript('errors', '$("input.error").parent().css("background-color", "#f4cccc");');

echo $this->renderPartial('_form', array('model' => $model, 'vendor' => $vendor, 'chapter' => $chapter, 'st' => $st));
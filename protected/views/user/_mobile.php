<?php
$this->pageTitle = Yii::app()->name . ' - Mobile Access';
$this->layout = '//layouts/full';

if (app()->user->IsSalesman() || app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        Vendor::model()->findByPk(app()->user->getActiveVendor())->vendor_name . ' Dashboard' => url('vendor/dashboard'),
        'Mobile Access'
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('vendor/dashboard'),
        'Mobile Access',
    );
}

$this->renderPartial('//layouts/userHeader', array('model' => $vendor, 'breadcrumbs' => $breadcrumbs));
$this->renderPartial('//layouts/vendorHeader', array('model' => $vendor));
?>
<div id="vendor-mainDashboard" style="height: 307px;">
    <div id="map_holder" class="dashboardHighlight" style="padding: 0px; height: 100%; clear: both; overflow: hidden;">
        <div id="map_canvas" style="height: 100%;"></div>
    </div>
</div>
<div class="widget-content dashboard">
    <?php $this->renderPartial('//vendor/dashActions/_dashboardLinks', array('model' => $vendor), false, false); ?>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="ausa-contentMain row-fluid">

    <?php
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons marker', 'panelTitle' => 'Mobile Access'));
    /* $this->widget('ausa.widgets.AOButtonGroup', array(
      'type' => '',
      'htmlOptions' => array('id' => 'branch-dropdown'),
      'buttons' => array(
      array('icon' => 'cog', 'items' => array(
      array('label' => 'Add a Branch', 'icon' => 'plus', 'url' => 'create', 'id' => 'create-branch-popup'),
      '---',
      array('label' => 'Get Help', 'icon' => 'question-sign', 'url' => '#'),
      )),
      ),
      )); */
    $this->endWidget();

    // genearate access token
    $model->getAccessToken();
    //Shared::debug($model);
    ?>
    <div>
        Download your mobile app at:
    </div>
    <div>
        Login: <?php echo $model->mobile_username ?><br />
        Pin: <?php echo $model->mobile_password ?><br />
    </div>
    <div class="info">
        Please note that this is a demo account and the app functionality is limited.
    </div>
    <?php
    $this->widget('application.components.widgets.AOPanelFoot');
    $this->endWidget();
    ?>

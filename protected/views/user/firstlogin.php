<?php
$this->layout = '//layouts/aoHomeNarrow';
$this->pageTitle = Yii::app()->name . ' - Verify Information';
$this->heading =
        '<div class="container">'
        . '<h1>Welcome To AUSA Offers</h1>'
        . '<p class="lead">It seems like this is your first visit. We\'d like to verify some information.</p>'
        . '</div>';

if ($model->hasErrors()) {
    cs()->registerScript('alert', 'showError(\'There are errors in your submission.\');');
}

cs()->registerScript('errors', '$("input.error").parent().css("background-color", "#f4cccc");');
?>

<h1 class="title">Verify Information</h1>
<div class="mid-header">We'd like you to check your contact information and verify your email address.</div>
<?php
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'user-form',
    'enableAjaxValidation' => false,
        ));
?>
<ul class="fields">
    <?php if (isset($role['model'])){ ?>
    <li>
        <i class="icon-truck" title="Vendor"></i>
        <?php echo CHtml::tag('span', array('class' => 'uneditable-input'), $role['model']->getCompanyName()); ?>
    </li>
    <?php } ?>
    <li>
        <i class="icon-envelope-alt" title="Email Address"></i>
        <?php echo CHtml::tag('span', array('class' => 'uneditable-input'), $model->{'email_address'}); ?>
        <?php echo CHtml::hiddenField('email_address', $model->email_address); ?>
    </li>
    <li>
        <i class="icon-user" title="First Name"></i>
        <?php echo $form->textField($model, 'first_name', array('placeholder' => 'First Name')); ?>
    </li>
    <li>
        <i class="icon-user" title="Last Name"></i>
        <?php echo $form->textField($model, 'last_name', array('placeholder' => 'Last Name')); ?>
    </li>
    <li>
        <i class="icon-globe" title="Address"></i>
        <?php echo $form->textField($model, 'address', array('placeholder' => 'Full Address', 'maxlength' => 511)); ?>
    </li>
    <li>
        <i class="icon-phone" title="Phone Number"></i>
        <?php echo $form->textField($model, 'phone_number', array('placeholder' => 'Phone Number', 'maxlength' => 12)); ?>
    </li>
    <li>
        <i class="icon-key"></i>
        <?php echo $form->passwordField($model, 'pass1', array('autocomplete' => 'off', 'placeholder' => 'Update Password', 'maxlength' => 63)); ?>
    </li>
    <li>
        <i class="icon-key"></i>
        <?php echo $form->passwordField($model, 'pass2', array('autocomplete' => 'off', 'placeholder' => 'Confirm Password', 'maxlength' => 63)); ?>
    </li>
</ul>
<div class="mid-form">
    <span class="login-checkbox pull-left"></span>
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'label' => 'Confirm Information',
        'type' => 'success',
        'icon' => 'icon-ok-sign',
        'htmlOptions' => array('class' => 'pull-right'),
        'block' => true,
        'size' => 'large',
    ));
    ?>
</div>
<div class="mid-footer">
    Passwords must contain at least 7 characters, at least one lower case letter, at least one upper case letter, and at least one number.
</div>
<?php $this->endWidget(); ?>


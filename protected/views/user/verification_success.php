<?php
$this->layout = '//layouts/aoHomeHeader';

$this->beginWidget('bootstrap.widgets.TBHeroUnit', array(
    'heading' => 'Thank you'
))
?>
<p>
    Your email address <?php echo $model->email_address?> has been verified.
</p>

<?php
$this->endWidget();
?>
<?php

// this form is used by admin only to change global user account configuration
// companies have custom forms
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'user-form',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => true,
        ));

echo $form->errorSummary($model);

echo $form->textFieldRow($model, 'email_address', array('class' => 'span', 'maxlength' => 63, 'hint' => 'Email address is used for login, make sure it is unique and correct.'));

echo $form->textFieldRow($model, 'first_name', array('class' => 'span'));

echo $form->textFieldRow($model, 'last_name', array('class' => 'span'));

echo $form->textFieldRow($model, 'phone_number', array('class' => 'span', 'maxlength' => 12));

// only admin and salesman can disable login for this user
if (app()->user->isAdmin()) {
    echo $form->checkBoxRow($model, 'login_disabled');
}
/*
else if (app()->user->isSalesman()) {
    echo $form->checkBoxRow($model, 'login_disabled', array('disabled' => 'disabled'));
}
*/

if (isset($userVendor)) {
    echo $form->checkBoxRow($userVendor, 'send_redemption_email');
}

$this->beginWidget('application.components.widgets.AOPanelFoot');

/* $this->widget('bootstrap.widgets.TbButton', array(
  'buttonType' => $model->isNewRecord ? 'submit' : 'ajaxSubmit',
  'type' => 'success',
  'htmlOptions' => array('style' => 'margin: 7px 5px; float: right', 'onclick' => 'showSuccess(\'Vendor changes updated.\')'),
  'size' => 'small',
  'icon' => 'ok white',
  'label' => $model->isNewRecord ? 'Create' : 'Update',
  ));
  $this->widget('bootstrap.widgets.TbButton', array(
  'buttonType' => 'reset',
  'type' => '',
  'htmlOptions' => array('style' => 'margin: 7px 5px; float: right', 'onclick' => 'showDefault(\'Vendor changes reset.\')'),
  'size' => 'small',
  'icon' => 'remove',
  'label' => $model->isNewRecord ? 'Clear' : 'Cancel',
  )); */

//if (!app()->user->isAdmin()){
    
// listen for email address change and bind event to global update / apply button
if (!$model->isNewRecord){
cs()->registerScript('email-changes', '
var origEmail = "' . $model->email_address . '";
var emailChanged = false;

$("#User_email_address").change(function(){
    if($("#User_email_address").val() != origEmail) {
        console.log("change detected");
        emailChanged = true;
    }
});

$("#apply-vendor-button,#save-vendor-button").click(function(){
    console.log("apply again, mofo");
    if (emailChanged){
        alert("This email is different from our stored email address. Please check your email and confirm the change.");
    }
});
');
}
//}

$this->endWidget();

$this->endWidget();
?>

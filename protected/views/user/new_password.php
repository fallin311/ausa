<?php
$this->layout = '//layouts/aoHomeNarrow';
cs()->registerScript('autofocus', '
    $("#User_email").focus();    
');

if ($model->hasErrors()) {
    cs()->registerScript('alert', 'showError(\'There are errors in your submission.\');');
}
?>
<h1 class="title">Set New Password</h1>
<div class="par">
    <?php if (!$model->isNewRecord): ?>
        <?php
        $form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
            'id' => 'user-form',
            'enableAjaxValidation' => true,
        ));
        if ($model->hasErrors()) {
            echo '<div class="mid-header" style="color: #B94A48;">The passwords didn\'t match (or) didn\'t meet specifications mentioned below.</div>';
        } ?>
        <ul class="fields">
            <li>
                <i class="icon-key"></i>
                <?php echo $form->passwordField($model, 'pass1', array('autocomplete' => 'off', 'placeholder' => 'new password', 'maxlength' => 63)); ?>
            </li>
            <li>
                <i class="icon-key"></i>
                <?php echo $form->passwordField($model, 'pass2', array('autocomplete' => 'off', 'placeholder' => 'confirm password', 'maxlength' => 63)); ?>
            </li>
        </ul>
        <div class="mid-form">
            <span class="login-checkbox pull-left"></span>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'label' => 'Update Password',
                'type' => 'info',
                'htmlOptions' => array('class' => 'pull-right'),
                'block' => true,
                'size' => 'large',
            ));
            ?>
        </div>
        <div class="mid-footer">
            Passwords must contain at least 7 characters, at least one lower case letter, at least one upper case letter, and at least one number.
        </div>
        <?php
        $this->endWidget();
    else:
        ?>
        <p>Please check your email for instructions on resetting your account password. The email was sent to <code><?php echo $model->email_address; ?></code>.</p>
    <?php endif; ?>
</div>

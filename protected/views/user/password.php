<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/aoHomeNarrow';
if ($model->hasErrors()) {
    cs()->registerScript('alert', 'showError(\'There are errors in your submission.\');');
}

cs()->registerScript('errors', '$("input.error").parent().css("background-color", "#f4cccc");');
?>

<h1 class="title">Change Password</h1>

<?php
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'user-form',
    'enableAjaxValidation' => false,
        ));
?>

<ul class="fields">
    <?php if (!app()->user->isAdmin()): // old password not required for admins  ?>
        <li>
            <i class="icon-key"></i>    
            <?php echo $form->passwordField($model, 'old_password', array('autocomplete' => 'off', 'placeholder' => 'current password', 'maxlength' => 63)); ?>
        </li>
<?php endif; ?>
    <li>
        <i class="icon-key"></i>    
        <?php echo $form->passwordField($model, 'pass1', array('autocomplete' => 'off', 'placeholder' => 'new password', 'maxlength' => 63)); ?>
    </li>
    <li>
        <i class="icon-key"></i>    
        <?php echo $form->passwordField($model, 'pass2', array('autocomplete' => 'off', 'placeholder' => 'confirm password', 'maxlength' => 63)); ?>
    </li>
</ul>
<div class="mid-form">
    <span class="login-checkbox pull-left">
    <?php echo CHtml::hiddenField('ajax', '0'); ?>
    </span>
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'label' => 'Change Password',
        'type' => 'primary',
        'htmlOptions' => array('class' => 'pull-right'),
        'block' => true,
        'size' => 'large',
    ));
    ?>
</div>
<div class="mid-footer">
    Passwords must contain at least 7 characters, at least one lower case letter, at least one upper case letter, and at least one number.
</div>

<?php $this->endWidget(); ?>
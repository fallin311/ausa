<?php
$this->layout = '//layouts/aoHomeHeader';
$this->beginWidget('bootstrap.widgets.TBHeroUnit', array(
    'heading' => 'Cannot verify your email address'
))
?>
<p>
    We could not find your email address or the verification code is wrong. Please make sure the url is exactly the same as the one we sent you.
</p>

<?php
$this->endWidget();
?>
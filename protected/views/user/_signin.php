<?php

// User sign in form which is found on any page in the main menu dropdown.
$model = new LoginForm;
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'login-form',
    'htmlOptions' => array('class' => 'ausa-userSignin'),
    'action' => url('/login'),
    'focus' => true,
        ));

echo $form->textAOFieldRow($model, 'username', array('class' => 'span3', 'tabindex' => '1', 'placeholder' => 'Email', 'maxlength' => 255));
echo $form->passwordAOFieldRow($model, 'password', array('class' => 'span3', 'tabindex' => '2', 'placeholder' => 'Password', 'maxlength' => 32));
echo CHtml::hiddenField('ajax', '1');

echo '<fieldset id="actions">';

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'button',
    'type' => 'primary',
    'icon' => 'user white',
    'htmlOptions' => array(
        'id' => 'submit-signin',
         'tabindex' => '4'
    ),
    'loadingText' => '<i class="icon-spinner icon-spin"></i> Sign In',
    'label' => 'Sign In',
));

echo $form->checkBoxAORow($model, 'rememberMe', array('tabindex' => '3'));

echo '</fieldset>';

cs()->registerScript('login-submit', "
function submitLoginForm(){
$.ajax({
    data: $('#login-form').serialize(0),
    type: 'post',
    url: '" . url('/login') . "',
    completedText: 'redirecting...',
    beforeSend: function(){ $('#submit-signin').button('loading') },
    success: function(response){
        if (response == 'bad'){
            showError('Sign in failed. Please try again.');
            $('#submit-signin').button('reset');
        }else{
            window.location.href=response;
        }
    }
});
}

$('#submit-signin').click(function(){submitLoginForm()});

$('#login-form input').keypress(function(e){
      if(e.which == 13){
       submitLoginForm()
       }
      });    
", CClientScript::POS_READY);
$this->endWidget();
?>

<ul class="signin-divider"><li class="divider"></li></ul>
<a style="margin: 0px -20px;" href="<?php echo url('user/forgotpassword'); ?>">Forgot password?</a>
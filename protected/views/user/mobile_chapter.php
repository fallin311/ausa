<?php
$this->layout = 'empty';
cs()->registerCss('mobile-preview', '
.mobile-info {
  color: black;
  margin: 30px;
  background-repeat:no-repeat;
  background-position:10px 10px;
  background-color:#d8e7fa;
  border-color:#9dbfea;
  color: #00357b;
  padding: 15px;
  border-radius: 5px;
  font-size: 14px;
}
');
?>
<div class="mobile-info">
    Mobile preview for chapter administrators is not available yet. Please use
    your regular member login to access the application. You can use your desktop
    chrome or safari browser and open <a href="<?php echo app()->params['mobileUrl']?>"><?php echo app()->params['mobileUrl']?></a>.
</div>
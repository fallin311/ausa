<?php
//This is INDEX page for Users View
$this->layout = '//layouts/full';
$breadcrumbs = array(
    'My Dashboard' => app()->user->getHomeUrl(),
    'Users',
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
?>

    <?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false)); ?>
    <?php $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'AUSA Users')); ?>
    <?php
    $this->widget('ausa.widgets.AOButtonGroup', array(
        'type' => '',
        'buttons' => array(
            array('icon' => 'cog', 'items' => array(
                    array('label' => 'Add a User', 'icon' => 'plus', 'url' => url('/user/create')),
                    '---',
                    array('label' => 'Get Help', 'icon' => 'question-sign', 'url' => '#'),
            )),
        ),
    ));
    ?>
    <?php $this->endWidget(); ?>
    <?php
    $dataArray = array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $dataProvider,
        'columns' => array(
            array('name' => 'user_id', 'header' => 'ID'),
            array('name' => 'first_name', 'header' => 'First name', 'type' => 'raw', 'value' => 'CHtml::link($data->first_name,url("/user/update/" . $data->user_id))'),
            array('name' => 'last_name', 'header' => 'Last name'),
            array('name' => 'email_address', 'header' => 'Email'),
            array('name' => 'last_login', 'header' => 'Last Login'),
            ));
    
    if (app()->user->isAdmin()) {
        $dataArray["columns"][] = array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
        );
    }
    
    
    $this->widget('bootstrap.widgets.TbGridView', $dataArray);
    ?>

<?php $this->endWidget(); ?>
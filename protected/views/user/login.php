<?php
$this->layout = '//layouts/aoHomeNarrow';
if($alert){ cs()->registerScript('alert','(function(){ showError(\'Sign in failed. Please try again.\'); })();'); }
?>
<h1 class="title">Sign In</h1>
<?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => true,
            )); ?>
    <ul class="fields">
        <li>
            <i class="icon-user" title="Email Address"></i>
            <?php echo $form->textField($model, 'username', array('autocomplete' => 'off', 'placeholder' => 'email address')); ?>
        </li>
        <li>
            <i class="icon-key" title="Password"></i>
            <?php echo $form->passwordField($model, 'password', array('autocomplete' => 'off', 'placeholder' => 'password')); ?>
        </li>
    </ul>
    <div class="mid-form">
        <span class="login-checkbox pull-left">
            <?php //echo $form->checkBoxRow($model, 'rememberMe'); ?>
        </span>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'success',
        'htmlOptions' => array('class' => 'pull-right'),
        'size' => 'large',
        'block' => true,
        'label' => 'Sign In',
        ));
        ?>
    </div>
    <div class="mid-footer">
        Forgot your password? Request a <a href="<?php echo url('/user/forgotpassword'); ?>">password reset</a>.
    </div>
<?php $this->endWidget(); ?>
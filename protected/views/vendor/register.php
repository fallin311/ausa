<?php $this->layout = '//layouts/plainCorp'; ?>
<h2>Register</h2>
<div class="par">
    <p>This is the vendors quick register form.</p>
    <p>Fields with <span class="required">*</span> are required.</p>
    <?php
    $form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
        'id' => 'register-form',
        'htmlOptions' => array('class' => 'no-foot'),
        'enableAjaxValidation' => true));

    echo $form->errorSummary(array($vendor, $user));
    ?>

    <div class="control-group">
        <?php echo $form->labelEx($user, 'first_name', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($user, 'first_name', array('class' => 'input-xlarge', 'maxlength' => 255)); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($user, 'last_name', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($user, 'last_name', array('class' => 'input-xlarge', 'maxlength' => 255)); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($vendor, 'vendor_name', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($vendor, 'vendor_name', array('class' => 'input-xlarge', 'maxlength' => 255)); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($vendor, 'address', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($vendor, 'address', array('class' => 'input-xlarge', 'maxlength' => 255)); ?>
            <p class="help-block">Example: 123 Some St., New York, NY, 10001</p>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($user, 'email_address', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($user, 'email_address', array('class' => 'input-xlarge', 'maxlength' => 63, 'hint' => 'Email address is used for login, make sure it is unique and correct.')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($user, 'password', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->passwordField($user, 'password', array('class' => 'input-xlarge', 'maxlength' => 63)); ?>
            <!--<p class="help-block">Password should be at least 7 characters.</p>-->
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($user, 'pass2', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->passwordField($user, 'pass2', array('class' => 'input-xlarge', 'maxlength' => 63)); ?>
            <!--<p class="help-block">Password should be at least 7 characters.</p>-->
        </div>
    </div>

    <?php if (CCaptcha::checkRequirements()): ?>
        <div class="control-group">
            <?php echo CHtml::activeLabel($user, 'verify', array('required' => true, 'class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($user, 'verify', array('class' => 'input-small')); ?>
                <?php $this->widget('CCaptcha', array('clickableImage' => true, 'showRefreshButton' => false, 'imageOptions' => array('style' => 'vertical-align: top; margin-top: -10px; cursor: pointer;'))); ?>
                <?php //echo $form->error($user,'verify'); ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="control-group">
        <div class="controls">
            <button class="btn btn-success">Register</button>
        </div>
    </div>
</div>

<?php $this->endWidget(); ?>

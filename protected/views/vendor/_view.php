<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('vendor_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->vendor_id),array('view','id'=>$data->vendor_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vendor_name')); ?>:</b>
	<?php echo CHtml::encode($data->vendor_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vendor_description')); ?>:</b>
	<?php echo CHtml::encode($data->vendor_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('internal_notes')); ?>:</b>
	<?php echo CHtml::encode($data->internal_notes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('logo')); ?>:</b>
	<?php echo CHtml::encode($data->logo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vendor_banner_image')); ?>:</b>
	<?php echo CHtml::encode($data->vendor_banner_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('website')); ?>:</b>
	<?php echo CHtml::encode($data->website); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('industry')); ?>:</b>
	<?php echo CHtml::encode($data->industry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled')); ?>:</b>
	<?php echo CHtml::encode($data->disabled); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_on')); ?>:</b>
	<?php echo CHtml::encode($data->created_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_on')); ?>:</b>
	<?php echo CHtml::encode($data->updated_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expiration')); ?>:</b>
	<?php echo CHtml::encode($data->expiration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('max_offers')); ?>:</b>
	<?php echo CHtml::encode($data->max_offers); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('max_banners')); ?>:</b>
	<?php echo CHtml::encode($data->max_banners); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('banner_expiration')); ?>:</b>
	<?php echo CHtml::encode($data->banner_expiration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('max_branches')); ?>:</b>
	<?php echo CHtml::encode($data->max_branches); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plan_id')); ?>:</b>
	<?php echo CHtml::encode($data->plan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_number')); ?>:</b>
	<?php echo CHtml::encode($data->customer_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_method')); ?>:</b>
	<?php echo CHtml::encode($data->payment_method); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_name')); ?>:</b>
	<?php echo CHtml::encode($data->billing_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_address')); ?>:</b>
	<?php echo CHtml::encode($data->billing_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_state')); ?>:</b>
	<?php echo CHtml::encode($data->billing_state); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_country')); ?>:</b>
	<?php echo CHtml::encode($data->billing_country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_zipcode')); ?>:</b>
	<?php echo CHtml::encode($data->billing_zipcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_phone')); ?>:</b>
	<?php echo CHtml::encode($data->billing_phone); ?>
	<br />

	*/ ?>

</div>
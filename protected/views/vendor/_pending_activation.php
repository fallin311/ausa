
<?php
$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
$this->widget('application.components.widgets.AOPanelHead', array('panelTitle' => 'Next Steps'));
?>
<div class="ao-panel-content" style="border-bottom: 1px solid #dddddd;">
<h2>Your account is not active yet...</h2>
<p>Please complete the following step(s) to activate your account.</p>
<ol style="list-style: decimal">
    <?php
    foreach ($missing as $error) {
        echo "<li>" . $error . "</li>";
    }
    ?>
</ol>
</div>
<?php
// register scripts to handle user email verification and activation request
$userActivation = '
        var sendBtn = $("#verify-email");
        var placeholder = $("#email-verification-placeholder");
        
        sendBtn.click(function(){
            $.ajax({
                url: "' . url('/user/sendVerificationEmail') . '",
                data: "id=" + sendBtn.data("email-for"),
                beforeSend: function(){
                    console.log("before-send");
                    sendBtn.removeClass("ajax-link");
                    placeholder.html("' . (app()->user->isAdmin() ? 'Updating Database...' : 'Sending verification email...') . '");
                },
                success: function(ret){
                    if (ret) {
                        placeholder.html("' . (app()->user->isAdmin() ? 'The email address verification is complete.' : 'Verification email successfully sent. Please check your email and click the verification link.') . '");
                            location.reload(true);
                    } else {
                        placeholder.html("<font color=\"red\">Could not send email</font>. Please contact our <a href=\"' . url('/vendor/support') . '\">user support</a>.");
                    }
                }
            });
        });
';
cs()->registerScript('account-activation', $userActivation);

$this->beginWidget('application.components.widgets.AOPanelFoot');


$this->endWidget();
$this->endWidget();
?>
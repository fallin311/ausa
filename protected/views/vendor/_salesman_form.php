<?php
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'vendor-form-salesmen',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => false,
));

if (app()->user->isAdmin()){
    // admin can see also the company name in the list
    $allSalesmen = CHtml::listData(UserSt::model()->fromSt($salesTeamId)->findAll(), 'user_id', 'user.fullNameCompany');
}else{
    $allSalesmen = CHtml::listData(UserSt::model()->fromSt($salesTeamId)->findAll(), 'user_id', 'user.fullName');
}

// TODO: preselect the logged in user
if ($model->isNewRecord){
    // preselect the current user
    $model->salesmenArr = array(app()->user->id);
}else{
    // select whatever is there
    $model->beforeFormLoad();
}

echo $form->checkBoxListRow($model, 'salesmenArr', $allSalesmen, array('hint' => 'Assign a contact person(s) inside your sales team.'));

$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->endWidget();

$this->endWidget();

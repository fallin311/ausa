<?php
cs()->registerCss('offer_css', '.head { display: block; clear: both; }
.head h2 { font-size: 16px; margin: 0 0 -5px 0; line-height: 1.5; }
.head h2 a { color: #444; display: block; }
.head .date, .head .tags { color: #999; font-size: 11px; }
.head .tags::before { content: \'— \00A0\'; }
.head .tags { font-style: italic; }
.head .tags a { color: #08C; text-decoration: none; }
.info { display: block; }');



$columns = array(
    array('name' => 'offer_title',
        'header' => 'Image',
        'type' => 'raw',
        'value' => '"<a href=\"" . app()->createUrl("/offer/update/",array("id" => $data->primaryKey)) . "\" class=\"thumbnail\"><img class=\"offer-preview-icon\" src=\"" . $data->getImageUrl(80) . "\" alt=\"preview\" /></a><div class=\"offer-description\"></div>"',
        'htmlOptions' => array('class' => 'image-center')),
    array('name' => 'offer_title', 'type' => 'raw', 'value' =>
        '"<div class=\"head\">
            <h2 class=\"title\"><a href=\"".app()->createUrl("/offer/update/",array("id" => $data->primaryKey))."\">".$data->offer_title."</a></h2>
            
          </div>
          <div class=\"info\">
            <p>".$data->offer_description."</p>
          </div>"'),
    //array('name' => 'offer_name', 'type' => 'raw', 'header' => 'Campaign Name', 'value' => 'CHtml::link("<h5>$data->offer_title</h5>",app()->createUrl("/offer/update/",array("id" => $data->primaryKey))).CHtml::link("<span>$data->offer_description</span>",app()->createUrl("/offer/update/",array("id" => $data->primaryKey)))'),
    array('name' => 'status', 'type' => 'raw', 'value' => '$data->getColoredStatus()', 'htmlOptions' => array('style' => 'width: 70px;', 'class' => 'status-center')),
    array('name' => 'start_date', 'type' => 'raw', 'header' => 'Dates', 'value' => '$data->getDatesForGrid()', 'htmlOptions' => array('style' => 'width: 200px;')),
    array('header' => 'Progress', 'type' => 'raw', 'value' => '$data->getStatsForGrid()', 'htmlOptions' => array('style' => 'width: 110px;')),
);

$dataArray = array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $offerProvider,
    'enablePagination' => false,
    'columns' => $columns
);
?>

<div class="ausa-contentMain row-fluid">
<?php
$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
$this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons iphone_4', 'panelTitle' => $model->vendor_name . ' Offers'));
$this->widget('ausa.widgets.AOButtonGroup', array(
    'type' => '',
    'buttons' => array(
        array('icon' => 'cog', 'items' => array(
                array('label' => 'Create Offer', 'icon' => 'plus', 'url' => url('/offer/create')),
                '---',
                array('label' => 'Get Help', 'icon' => 'question-sign', 'url' => '#'),
        )),
    ),
));
$this->endWidget();
$this->widget('bootstrap.widgets.TbGridView', $dataArray);
$this->endWidget();

$this->widget('ext.jqueryReadMore.Expander', array(
        'selector' => 'div.info p',
        'options' => array('slicePoint' => 80)
    ), true);
?>
<?php

$this->pageTitle = Yii::app()->name . ' - Vendor Dashboard';

if (app()->user->IsSalesman() || app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        Vendor::model()->findByPk(app()->user->getActiveVendor())->vendor_name . ' Dashboard',
    );
} else {
    $breadcrumbs = array(
        'My Dashboard'
    );
}

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));

//$this->renderPartial('//vendor/_account_status', array('model' => $model));

$this->renderPartial('//subscription/_subscription_status', array('model' => $model));

$subStatus = $model->getSubscriptionStatus();

$missingStuff = $model->isVerified();
if ($missingStuff !== true) {
    // there is something missing here completely
    $this->renderPartial('//vendor/_pending_activation', array('model' => $model, 'missing' => $missingStuff));
} else if ($model->published == 0) {
    // vendor is not published
    $this->renderPartial('//vendor/_go_live', array('model' => $model));
} else if($subStatus != Vendor::SUB_NOT_ACTIVE) {
    // vendor is active or nothing is pending (only if the payment was recieved)
    $this->renderPartial('//vendor/_quick_report', array('model' => $model));
}
$this->renderPartial('_offers', array('offerProvider' => $offerProvider, 'model' => $model));
?>
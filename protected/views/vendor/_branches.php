<?php

$this->pageTitle = Yii::app()->name . ' - Vendor Branches';

if(app()->user->IsSalesman()||app()->user->IsAdmin()){
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        Vendor::model()->findByPk(app()->user->getActiveVendor())->vendor_name.' Dashboard' => url('vendor/dashboard'),
        'Branches'
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('vendor/dashboard'),
        'Branches',
    );
}

$this->widget('application.components.widgets.AOGMap', array('markers' => str_replace("\\/", '/', json_encode(Chapter::model()->getVendorBranches($model->vendor_id)))));

Yii::import('ext.simpleModal.ModalWidget');
ModalWidget::loadAssets();

cs()->registerCSS('form-bgcolor','form {background-color: #FFF; } form#branch-form {background-color:transparent;}');

$dataArray = array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $branchData,
    'id' => 'branch-list',
    'enablePagination' => false,
    'columns' => array(
        array('name' => 'branch_name', 'header' => 'Name', 'type' => 'raw'),
        array('name' => 'branch_address', 'header' => 'Address'),
        array('name' => 'active', 'header' => 'Active?', 'type' => 'raw', 'value' => '($data->active?"<span class=\"label label-success\">Yes</span>":"<span class=\"label label-important\">No</span>")', 'htmlOptions' => array('class' => 'status-center')),
        array('class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{edit}',
            'buttons' => array(
                'edit' => array(
                    'icon' => 'pencil',
                    'options' => array(
                        'class' => 'update-popup',
                        'id' => '',
                    ),
                    'url' => '"' . url('/branch/update') . '/" . $data->branch_id'
                )
            )
        ),
    ),
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs, 'map' => true));
?>

<div class="ausa-contentMain row-fluid">

<?php
$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons marker', 'panelTitle' => 'Branches (' . $branchData->totalItemCount . '/' . $model->max_branches . ')'));
        $this->widget('ausa.widgets.AOButtonGroup', array(
            'type' => '',
            'htmlOptions' => array('id' => 'branch-dropdown'),
            'buttons' => array(
                array('icon' => 'cog', 'items' => array(
                    array('label' => 'Add a Branch', 'icon' => 'plus', 'url' => 'create', 'id' => 'create-branch-popup'),
                    '---',
                    array('label' => 'Get Help', 'icon' => 'question-sign', 'url' => '#'),
                )),
            ),
        ));
    $this->endWidget();

    $this->widget('bootstrap.widgets.TbGridView', $dataArray);

    $this->widget('application.components.widgets.AOPanelFoot');
$this->endWidget();
?>

<div id="branch-update-form" style="display: none;" class="ausa-contentMainInner">
    <?php $this->renderPartial('//branch/_form', array('model' => new Branch, 'vendor' => $model)); ?>
</div>
<?php
    cs()->registerCss('popup-form', '
    #branch-update-form {
        width: 100% !important;
        border: 0px solid black !important;
        margin: 0px !important;
    }
'
    );
    cs()->registerScript('branch-update-popup', '
    /** display the crate / update popup */
    function showUpdatePopup(){
        $("#branch-update-form").modal({
        containerCss:{
		padding:0, 
		width:500,
                height: 540
	},
        onClose: function(){
            $("a span.loading-icon").parent().html("<i class=\"icon-pencil\"></i>");
            $.modal.close();
        }
        });
    }
    
    /** Get data to be displayed in the popup */
    function prepareUpdatePopup(branchId){
        if (isNaN(branchId)){
            data = ({
                branch_id: "",
                active: 1,
                vendor_id: "' . $model->vendor_id . '",
                branch_name: "' . CHTml::decode($model->vendor_name) . ' #' . ((int) $branchData->totalItemCount + 1) . '",
                branch_address: "",
                phone_number: "",
                website: "",
                hours: ""
            });
            var $elmnt;
            $.each(data, function(i, val) {
                $elmnt = $("#Branch_" + i);
                if (($elmnt).length > 0){
                    $elmnt.val(val);
                }
            });
            showUpdatePopup();
        }else{
            $.ajax({
                url: "' . url('/branch/updateJson') . '/" + branchId,
                type: "post",
                dataType: "json",
                success: function(data){
                    if (data["success"]){
                        var $elmnt;
                        $.each(data["data"], function(i, val) {
                            $elmnt = $("#Branch_" + i);
                            if (($elmnt).length > 0){
                                $elmnt.val(val);
                            }
                        });
                        showUpdatePopup();
                    }else{
                        showError("You do not have a permission to do that.");
                    }
                },
                error: function(){
                    showError("Cannot update the branch. Please contact our technical support.");
                }
            });
        }
    }
    
    /** Bind update /create buttons */
    $("a.update-popup").live("click", function(e){
        e.preventDefault();
        var url = $(this).attr("href");
        var branchId = url.substring(url.lastIndexOf("/") + 1);
        $(this).html("<span class=\"loading-icon\"></span>");
        prepareUpdatePopup(branchId);
    });   
    $("#branch-dropdown a[href=\'create\']").click(function(e){
        e.preventDefault();
        prepareUpdatePopup("new");
    });
');
    ?>
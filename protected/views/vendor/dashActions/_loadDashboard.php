<?php
/*
 * This is the ajax loaded home dashboard for vendors
 * 
 */
cs()->registerScriptFile(CGoogleApi::$bootstrapUrl, CClientScript::POS_HEAD);

echo CHtml::script("
var chartInit = false,
drawVisitorsChart = function() {
    var data = new google.visualization.DataTable();
    var raw_data = [['Views', 50, 73, 104, 129, 146, 176, 139, 149, 218, 194, 96, 53],
        ['Redemption', 82, 77, 98, 94, 105, 81, 104, 104, 92, 83, 107, 91],
        ['Something', 50, 39, 39, 41, 47, 49, 59, 59, 52, 64, 59, 51],
        ['Something Else', 45, 35, 35, 39, 53, 76, 56, 59, 48, 40, 48, 21]];
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    data.addColumn('string', 'Month');
    for (var i = 0; i < raw_data.length; ++i) {
        data.addColumn('number', raw_data[i][0]);
    }
    data.addRows(months.length);
    for (var j = 0; j < months.length; ++j) {
        data.setValue(j, 0, months[j]);
    }
    for (var i = 0; i < raw_data.length; ++i) {
        for (var j = 1; j < raw_data[i].length; ++j) {
            data.setValue(j-1, i+1, raw_data[i][j]);
        }
    }
    var div = $('#stat-chart'),
    divWidth = div.width();
    new google.visualization.LineChart(div.get(0)).draw(data, {
        title: 'Vendor Influx Graph (under construction)',
        width: divWidth,
        height: 269,
        vAxis: {title: '(thousands)'},
        backgroundColor: 'transparent',
        legendTextStyle: { color: 'white' },
        titleTextStyle: { color: 'white' },
        hAxis: {
            textStyle: { color: 'white' }
        },
        vAxis: {
                textStyle: { color: 'white' },
                baselineColor: '#666666'
        },
        chartArea: {
                top: 35,
                left: 30,
                width: divWidth-40
        },
        legend: {position: 'bottom'}
    });
};
google.load('visualization', '1', { 'packages': ['corechart'] });
google.setOnLoadCallback(drawVisitorsChart);
$(document).on('respond-ready', drawVisitorsChart);	
");
?>
<div class="dashboardHighlight" style="clear: both; overflow: hidden; height: 265px;">
    <div class="columns">
        <div id="stat-chart" style="float: left; width: 70%; font-family: 'Cabin Sketch'; font-size: 60px;"></div>
        <div style="float: left; width: 30%;">
            <ul class="stats">
                <?php
                if ($model->max_banners > 0) {
                    ?>
                    <li><strong><?php echo CHtml::link($model->getTotalActiveBanners(), '', array('data-placement' => 'left', 'data-title' => 'Active Banners', 'data-content' => 'This is your current number of active banners', 'rel' => 'clickover', 'style' => 'cursor: pointer;')); ?>/<?php echo CHtml::link($model->max_banners, '', array('data-placement' => 'top', 'data-title' => 'Max Banners', 'data-content' => 'Total Number of Banners you can place on this system.', 'rel' => 'clickover', 'style' => 'cursor: pointer;')); ?></strong> active / max<br> banners</li>
                    <?php
                }
                ?>

                <li><strong><?php echo CHtml::link($model->getTotalActiveOffers(), '', array('data-placement' => 'left', 'data-title' => 'Active Offers', 'data-content' => 'This is your current number of active offers / service ads', 'rel' => 'clickover', 'style' => 'cursor: pointer;')); ?>/<?php echo CHtml::link($model->max_offers, '', array('data-placement' => 'top', 'data-title' => 'Max Offers', 'data-content' => 'Total Number of Offers / Service Ads  you can provide in this system.', 'rel' => 'clickover', 'style' => 'cursor: pointer;')); ?></strong> active / max<br> offers (service ads)</li>
                <?php
                if ($chModel != null) {
                    ?>
                    <li><strong><?php echo CHtml::link($chModel->getTotalMemberCount(), '', array('data-placement' => 'left', 'data-title' => 'Total Members', 'data-content' => 'Total Active Members in ' . $chModel->chapter_name, 'rel' => 'clickover', 'style' => 'cursor: pointer;')); ?></strong> active <br>chapter members</li>
                    <?php
                }
                ?>
                
            </ul>
        </div>
    </div>
</div>
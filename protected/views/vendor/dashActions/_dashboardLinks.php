<?php
// Vendor's dashboard links; relocated to one file for easier editing.
$this->widget('application.components.widgets.AODashboard', array(
    'items' => array(
        'sthome' => array('icon' => 'dashboard-home', 'text' => 'My<br/>Dashboard', 'url' => url('salesTeam/dashboard'), 'visible' => app()->user->isSalesman()),
        'home' => array('icon' => 'dashboard-home', 'text' => (app()->user->isSalesman()?'Vendors<br/>Dashboard':'My<br/>Dashboard'), 'url' => url('vendor/dashboard')),
        'branches' => array('icon' => 'dashboard-branches', 'text' => (app()->user->isSalesman()?'Vendors<br/>Branches':'View<br/>Branches'), 'url' => url('vendor/branches')),
        'offers' => array('icon' => 'dashboard-offers', 'text' => (app()->user->isSalesman()?'Vendors<br/>Offers':'Offers /<br/>Service Ads'), 'url' => url('vendor/offers')),
        'banners' => array('icon' => 'dashboard-banners', 'text' => (app()->user->isSalesman()?'Vendors<br/>Banners':'View<br/>Banners'), 'url' => url('banner/index'), 'visible'=>$model->max_banners),
        'mobile' => array('icon' => 'dashboard-mobile mobile-preview', 'text' => 'Mobile Preview', 'url' => url('user/mobile')),
        'settings' => array('icon' => 'dashboard-settings', 'text' => (app()->user->isSalesman()?'Vendors<br/>Profile':'My<br />Profile'), 'url' => url('vendor/update/' . $model->vendor_id)),
        'subscription' => array('icon' => 'dashboard-settings', 'text' => (app()->user->isSalesman()?'Vendors<br/>Subscription':'My<br />Subscription'), 'url' => url('subscription/index/' . $model->vendor_id)),
        'support' => array('icon' => 'dashboard-help', 'text' => 'Support<br />Information', 'url' => url('vendor/support'), 'visible' => !app()->user->isSalesman()),
    ), 'multiCol' => false,
));

cs()->registerScript('mobile-preview', '
$(".mobile-preview").click(function(event){
    event.preventDefault();
    newwindow=window.open("' . url('/user/mobilePreview') . '","mobile preview","height=850,width=720");
    if (window.focus) {
        newwindow.focus();
    }
    this.hide();
    return false;
});    
');
?>

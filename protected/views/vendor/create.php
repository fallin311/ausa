<?php
if (app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        'My Vendors' => url('vendor/index'),
        'Add a Vendor',
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        'My Vendors' => url('salesTeam/vendors'),
        'Add a Vendor',
    );
}

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
$this->widget('application.components.widgets.AOGrid');
?>

<?php
// Save/Cancel Buttons
$this->beginWidget('application.components.widgets.AOStickyBar', array('sticky' => true, 'panelWrap' => true));
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'icon' => 'remove white',
    'type' => 'danger',
    'url' => url('/vendor'),
    'label' => 'Cancel',
    'htmlOptions' => array('style' => 'margin: 5px 5px 0px 8px; float: right',)
));
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'ajaxSubmit',
    'type' => 'success',
    'url' => url('/vendor/create'),
    'htmlOptions' => array(
        'style' => 'margin: 5px 8px; float: right',
        'id' => 'save-vendor-button',
    ),
    'loadingText' => 'processing...',
    'ajaxOptions' => array(
        'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
        'beforeSend' => 'function(){$("#save-vendor-button").button("loading")}',
        'success' => 'function(response){

                    if (response.length < 5){
                        window.location.href="' . url('vendor/update') . '/" + response;
                    }else{
                        console.log(response);
                        $("#save-vendor-button").button("reset");

                        $("#ajax-errors-holder").show();
                        $("#ajax-errors").html(response);
                        showError(\'Could not save the vendor. Please check the error fields below.\');
                    }
                }',
    ),
    'icon' => 'ok white',
    'label' => 'Save',
));
$this->endWidget();
?>

<div style="width: 100%; margin-bottom: 30px; position: relative; float: left; display: none;" id="ajax-errors-holder">
    <?php
    cs()->registerCss('error-overwrite', '.notification { margin-top: 0px; }');
    $this->widget('application.components.widgets.AONotice', array('notice' => '<div id="ajax-errors"></div>', 'noticeType' => 'errors', 'noticeClosable' => false));
    ?>
</div>

<div class="ausa-contentMain row-fluid">
    <div id="ausa-contentMainTiles">
        <ul id="tiles">
            <li class="grid">
                <?php
                // Basic company information
                $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Company Information'));
                echo $this->renderPartial('_company_form', array('model' => $model));
                $this->endWidget();
                ?>
            </li>
            <li class="grid">
                <?php
                // User contact form
                $this->beginWidget('application.components.widgets.AOPanel');
                $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons download_to_computer', 'panelTitle' => 'Contact Person'));
                echo $this->renderPartial('//user/_contact_form', array('model' => $user, 'userVendor' => $userVendor));
                $this->endWidget();
                ?>
            </li>
            <?php
            //if (app()->user->isAdmin()) {
            ?>
            <li class="grid">
                <?php
                // extended expiration form
                $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Subscription Plan'));
                echo $this->renderPartial('_expiration_form', array('model' => $model));
                $this->endWidget();
                ?>
            </li>
            <?php
            //}
            ?>
            <?php
            // contact people inside salesteam
            if (app()->user->isSalesman()) {
                echo '<li class="grid">';
                $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Sales Team Contacts'));
                echo $this->renderPartial('_salesman_form', array('model' => $model, 'salesTeamId' => app()->user->getActiveSt()));
                $this->endWidget();
                echo '</li>';
            }
            ?>
        </ul>
    </div>
</div>
<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'vendor-form',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => true));


echo $form->errorSummary($model);

if (app()->user->isAdmin()) {
    echo $form->textFieldRow($model, 'pg_id', array('disabled' => 'disabled'));
}

echo $form->textFieldRow($model, 'vendor_name', array('class' => 'span', 'maxlength' => 255));


//echo $form->textAreaRow($model,'internal_notes',array('rows'=>6, 'cols'=>50, 'class'=>'span8'));

echo $form->imageFieldRow($model, 'logo', array('class' => 'span', 'maxlength' => 10, 'imageOptions' => array(
        'configuration' => 'logo',
        'layout' => 'logo-layout'
        )));

// Bootstrap WYSIWYG Textarea - Begin
echo '<div class="form-row" style="margin-top:-6px; min-height:157px;"><label for="yw1">Vendor Description</label><div class="form-element"><div class="clearfix">';
$this->widget('bootstrap.widgets.TbWysiwyg', array(
    'model'=>$model,
    'attribute'=>'vendor_description',
    'htmlOptions'=>array(
        'name'=>'Vendor[vendor_description]',
        'class'=>'span',
        'rows'=>'6',
        'cols'=>'50'),
    'buttons'=>array(
        'emphasis',
        'color',
        'html'),
    ));
echo '<a class="ausa-toolTip" rel="tooltip" data-original-title="The text displayed on cellphone describing your business. Minimum lenght is 50 characters, but optimal lenght should be somewhere between 200 - 500 charactes."></a>';
echo '</div><p class="help-block">This text is displayed as your business description in the mobile app. Make it appealing and keep it short.</p></div></div>';
// Bootstrap WYSIWYG Textarea - End
//echo $form->textAreaRow($model, 'vendor_description', array('rows' => 4, 'cols' => 50, 'class' => 'span', 'hint' => 'The text displayed on cellphone describing your business. Keep it short and use plain text for the best effect.'));

echo $form->textFieldRow($model, 'address', array('class' => 'span', 'maxlength' => 255, 'hint' => 'Recommended format:<br/>Street, City, State, Postal Code.'));

echo $form->textFieldRow($model, 'website', array('placeholder' => 'http://', 'class' => 'span', 'maxlength' => 127, 'hint' => "Your website url is visible in the phone app. Please provide full url with prefix http://"));

cs()->registerCss('select2-css',
    '#s2id1, #s2id2, #s2id3, #s2id4 {
        width: 100%;
        margin-bottom: 10px;
    }'
    .'.select2-drop-above {
        -webkit-box-shadow: none !important;
        -moz-box-shadow: none !important;
        -o-box-shadow: none !important;
        box-shadow: none !important;
    }'
    .'.select2-container .select2-choice div {
        -webkit-border-radius: 0 3px 3px 0;
        -moz-border-radius: 0 3px 3px 0;
        border-radius: 0 3px 3px 0;
        border-left: 1px solid #ccc;
    }');
echo '<div class="form-row" style="width: 100%;">'
    .'<label for="Vendor_industry" class="required">Industry </label>'
    .'<div class="form-element"><div class="clearfix">';
$this->widget('ext.select2.ESelect2', array(
    'model' => $model,
    'attribute' => 'industry',
    'data' => CHtml::listData(Category::model()->published()->findAll(array('order'=>'category_name ASC')), 'category_id', 'category_name'),
    'options' => array('placeholder' => 'Choose one')
));
echo '</div></div></div>';

if (app()->user->isAdmin()) {
    if (count($model->chapters) == 0){
        echo '<div class="form-row" style="width: 100%;">'
            .'<label for="Vendor_chapterId" class="required">Chapter </label>'
            .'<div class="form-element"><div class="clearfix">';
        $this->widget('ext.select2.ESelect2', array(
            'model' => $model,
            'attribute' => 'chapterId',
            'data' => CHtml::listData(Chapter::model()->findAll(), 'id', 'chapter_name'),
            'options' => array('placeholder' => 'Select Chapter')
        ));
        echo '</div></div></div>';
    }

    if (count($model->salesTeams) == 0){
        echo '<div class="form-row" style="width: 100%;">'
            .'<label for="Vendor_salesTeamId" class="required">Sales Team <span class="required">*</span></label>'
            .'<div class="form-element"><div class="clearfix">';
        $this->widget('ext.select2.ESelect2', array(
            'model' => $model,
            'attribute' => 'salesTeamId',
            'data' => CHtml::listData(SalesTeam::model()->findAll(), 'id', 'st_name'),
            'options' => array('placeholder' => 'Select Sales Team')
        ));
        echo '</div></div></div>';
    } else {
        echo $form->hiddenField($model,'salesTeamId',array('value'=>StVendor::model()->findByAttributes(array('vendor_id' => $model->vendor_id))->sales_team_id));
    }
} 

$this->beginWidget('application.components.widgets.AOPanelFoot');

/* $this->widget('bootstrap.widgets.TbButton', array(
  'buttonType' => $model->isNewRecord ? 'submit' : 'ajaxSubmit',
  'type' => 'success',
  'url' => url('/vendor/update/' . $model->vendor_id),
  'htmlOptions' => array('style' => 'margin: 7px 5px; float: right'),
  'ajaxOptions' => array(
  'data' => 'js:serializeAll()',
  'success' => 'showSuccess(\'Vendor changes updated.\')'
  ),
  'size' => 'small',
  'icon' => 'ok white',
  'label' => $model->isNewRecord ? 'Create' : 'Update',
  )); */

/*$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'reset',
    'type' => '',
    'htmlOptions' => array('style' => 'margin: 7px 5px; float: right', 'onclick' => 'showDefault(\'Vendor changes reset.\')'),
    'size' => 'small',
    'icon' => 'remove',
    'label' => $model->isNewRecord ? 'Clear' : 'Cancel',
));*/ //Vk Commented the Clear Buttons

$this->endWidget();

$this->endWidget();

<?php
$this->pageTitle = Yii::app()->name . ' - Vendor Support';
$this->layout = '//layouts/full';

if (app()->user->IsSalesman() || app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        Vendor::model()->findByPk(app()->user->getActiveVendor())->vendor_name . ' Dashboard' => url('vendor/dashboard'),
        'Support',
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('vendor/dashboard'),
        'Support',
    );
}

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
?>

<div class="ausa-contentMain row-fluid">

    <?php
    // contact form
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons email', 'panelTitle' => 'Contact AUSA Offers'));
    $this->endWidget();
    echo $this->renderPartial('_contact_form', array('vendor' => $model, 'model' => $contactForm));
    $this->endWidget();
    ?>

    <?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true)); ?>
    <?php $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons help', 'panelTitle' => 'Your Support Team')); ?>
    <div class="ao-panel-content">
        <p>
            Do you have a question about your offer or subscription? Please send an email to contact person from your support team.
        </p>
        <div style="display: inline-block; vertical-align: top;">
            <img src="<?php echo url('/image/display/' . $stModel->logo) . '?w=' . Image::SIZE_PREVIEW_W . '&h=' . Image::SIZE_PREVIEW_H; ?>" alt="<?php echo $stModel->st_name; ?>" />
        </div>
        <div style="display: inline-block; margin-left: 20px;">
            <?php if ($stModel) { ?>
                <b>Support Team: </b><?php echo $stModel->st_name; ?><br/>
                <?php
                $stUser = $stModel->getContactUser();
                if ($stUser != null) {
                    ?>
                    <p>
                        <b>Phone: </b> <?php echo Shared::formatPhone($stUser->phone_number); ?> <br />
                        <b>Email: </b> <?php echo $stUser->email_address; ?> <br />
                    </p>
                    <?php
                }
                foreach ($salesmen as $salesman) {
                    if ($salesman->user_id != $stUser) {
                        ?>
                        <p>
                            <b>Contact Person: </b><?php echo $salesman->user->getFullName() ?><br />
                            <b>Phone: </b> <?php echo Shared::formatPhone($salesman->user->phone_number); ?> <br />
                            <b>Email: </b> <?php echo $salesman->user->email_address; ?> <br />
                        </p>
                        <?php
                    }
                }
                ?>

            <?php } else { ?>
                Contact InRadius Systems for support.
            <?php } ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>

    <?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true)); ?>
    <?php $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons help', 'panelTitle' => 'Chapter')); ?>
    <?php
    $this->widget('bootstrap.widgets.TbDetailView', array(
        'data' => $chapter,
        'attributes' => array(
            'chapter_name',
            'city',
            array('name' => 'chapter_url', 'type' => 'raw', 'value' => CHtml::link($chapter->chapter_url)),
            //array('label' => 'Active Members', 'value' => $chapter->getTotalMemberCount() . ' (without family members)')
            ))
    );
    ?>
    <?php $this->endWidget(); ?>
    
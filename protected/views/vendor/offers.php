<?php
$this->pageTitle = Yii::app()->name . ' - Vendor Offers';
$this->layout = '//layouts/full';

if (app()->user->IsSalesman() || app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        Vendor::model()->findByPk(app()->user->getActiveVendor())->vendor_name . ' Dashboard' => url('vendor/dashboard'),
        'Offers'
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('vendor/dashboard'),
        'Offers',
    );
}

if ($notice) {
    cs()->registerScript('updates', 'setTimeout(function() { showSuccess(\'Offer changes have been saved.\'); }, 500);');
}

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
?>

<div class="ausa-contentMain row-fluid">
    <?php
    /*
      $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
      $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons iphone_4', 'panelTitle' => $model->vendor_name.' Offers'));
      $this->widget('ausa.widgets.AOButtonGroup', array(
      'type' => '',
      'buttons' => array(
      array('icon' => 'cog', 'items' => array(
      array('label' => 'Create Offer', 'icon' => 'plus', 'url' => url('/offer/create')),
      '---',
      array('label' => 'Get Help', 'icon' => 'question-sign', 'url' => '#'),
      )),
      ),
      ));
      $this->endWidget();
      $this->widget('bootstrap.widgets.TbGridView', $dataArray);
      $this->endWidget(); */


    $this->renderPartial('//vendor/_offers', array('offerProvider' => $offerProvider, 'model' => $model));
    ?>
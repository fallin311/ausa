<?php
$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
$this->widget('application.components.widgets.AOPanelHead', array('panelTitle' => 'Almost There'));
?>
<div class="ao-panel-content" style="border-bottom: 1px solid #dddddd;">
<p>Your offer is still not visible. Please go ahead and approve the content.</p>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'icon' => 'ok white',
    'type' => 'success',
    'url' => url('/vendor/goLive/'),
    'label' => 'Go Live!',
    //'htmlOptions' => array('style' => 'margin: 5px 25px 0px 8px; float: left',)
));
?>
</div>
<?php
$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->endWidget();
$this->endWidget();
?>
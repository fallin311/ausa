<div class="ao-panel-content" style="border-bottom: 1px solid #dddddd;">
    Would you like to suggest a new feature for this system or did you find a bug? Please use this form
    to contact AUSA Offers administrators.
</div>
<?php
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'contact-form',
    'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => false,
));

//echo $form->errorSummary($model);

echo $form->textFieldRow($model, 'subject', array('class' => 'span', 'maxlength' => 128));
echo $form->textAreaRow($model, 'body', array('class' => 'span', 'rows' => 9));

$this->beginWidget('application.components.widgets.AOPanelFoot');
$this->widget('bootstrap.widgets.TbButton', array(
  'buttonType' => 'submit',
  'type' => 'success',
  'htmlOptions' => array('style' => 'margin: 7px 5px; float: right'),
  'size' => 'small',
  'icon' => 'ok white',
  'label' => 'Submit',
  ));
$this->endWidget();

$this->endWidget();
?>

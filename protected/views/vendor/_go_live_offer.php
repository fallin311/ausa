<?php

$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
$this->widget('application.components.widgets.AOPanelHead', array('panelTitle' => 'Offer'));
?>
<div class="ao-panel-content">
    <img class="partial-left" src="<?php echo $model->getImageUrl(125) ?>" alt="<?php echo $model->offer_title ?>" />
    <div class="partial-right">
        <h2 style="margin-top: 0px;"><?php echo $model->offer_title ?></h2>
        <?php
        if (!$model->published){
            ?>
        <p>This offer is <span class="label label-warning">not published</span>. <a href="<?php echo url('offer/update/' . $model->offer_id)?>">Publish</a> this offer first.</p>
            <?php
        }
        ?>
        <p><b>Expiration: </b><?php echo Shared::formatShortUSDate($model->end_date) ?></p>
       <?php // TODO: add category and redemption options ?>
    </div>
    <div style="clear: both">
        <?php
        echo $model->offer_description;
        ?>
    </div>
</div>
<?php
$this->endWidget();
?>

<?php
$this->pageTitle = Yii::app()->name . ' - Go Live';
$this->layout = '//layouts/full';

if (app()->user->IsSalesman() || app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        Vendor::model()->findByPk(app()->user->getActiveVendor())->vendor_name . ' Dashboard' => url('vendor/dashboard'),
        'Go Live',
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('vendor/dashboard'),
        'Go Live',
    );
}

cs()->registerCss('publish-screen', '
.btn-row {
    margin-left: 1px; 
    margin-top: 20px; 
    margin-bottom: 10px;
}

.partial-left {
    position: relative; 
    float: left; 
    margin-right: 20px; 
    margin-bottom: 20px; 
    vertical-align: top;
}

.partial-right {
    position: relative; 
    float: left;
}
');

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));

// Submit Form
if ($model->published != 1) {
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
    $this->widget('application.components.widgets.AOPanelHead', array('panelTitle' => 'Go Live'));
    ?>
    <div class="ao-panel-content">
        <h2>Go Live Now!</h2>
        <p>All the information on this page will be pushed to AUSA Offers phone application. The data will be published as long as your subscription is active. 
            Please read carefully every part below before publishing.</p>
        <div>
            <?php
            $form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
                'id' => 'go-live-form',
                'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
                'enableAjaxValidation' => false,
                    ));
            echo CHtml::hiddenField('publish', 1);
            echo CHtml::checkBox('agree', false, array('style' => 'margin-top: 0px;'));
            ?>
            <span style="margin-left: 10px;">A agree to the <a href="<?php echo url('site/terms') ?>" target="_blank">terms of use</a> and I have reviewed the content below.</span>

            <div class="btn-row">
                <?php
                $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'ajaxButton',
                    'icon' => 'ok white',
                    'type' => 'success',
                    'size' => 'large',
                    //'url' => url('/vendor/goLive/'),
                    'label' => 'Publish',
                    'htmlOptions' => array(
                        'id' => 'go-live-btn'
                    ),
                    'ajaxOptions' => array(
                        'data' => 'js:serializeAll()',
                        'type' => 'POST',
                        'beforeSend' => 'function(){$("#go-live-btn").button("loading")}',
                        'success' => 'function(response){
                        if (response == 1){
                            window.location.href="' . url('/vendor/dashboard') . '";
                        }else{
                            $("#go-live-btn").button("reset")
                            showError(\'Please let us know that you agree to our terms first.\');
                        }
                    }',
                    )
                        //'htmlOptions' => array('style' => 'margin: 5px 25px 0px 8px; float: left',)
                ));
                ?>
            </div>
            <?php
            $this->endWidget();
            ?>
        </div>
    </div>
    <?php
    $this->endWidget();
} else {
    // display unpublish form
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
    $this->widget('application.components.widgets.AOPanelHead', array('panelTitle' => 'Go Live'));
    ?>
    <div class="ao-panel-content">
        <h2>You are already live!</h2>
        <p>All the information on this page is visible on AUSA Offers phone application. 
            Do you wish to un-publish it?</p>
        <div>
            <?php
            $form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
                'id' => 'go-live-form',
                'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
                'enableAjaxValidation' => false,
                    ));
            echo CHtml::hiddenField('unpublish', 1);
            ?>
            <div class="btn-row">
                <?php
                $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'ajaxButton',
                    'icon' => 'remove white',
                    'type' => 'danger',
                    'size' => 'large',
                    //'url' => url('/vendor/goLive/'),
                    'label' => 'Unpublish',
                    'htmlOptions' => array(
                        'id' => 'go-live-btn'
                    ),
                    'ajaxOptions' => array(
                        'data' => 'js:serializeAll()',
                        'type' => 'POST',
                        'beforeSend' => 'function(){$("#go-live-btn").button("loading")}',
                        'success' => 'function(response){
                        if (response == 1){
                            window.location.href="' . url('/vendor/dashboard') . '";
                        }else{
                            $("#go-live-btn").button("reset")
                            showError(\'Could not unpublish the data now. Please try again later.\');
                        }
                    }',
                    )
                ));
                ?>
            </div>
            <?php
            $this->endWidget();
            ?>
        </div>
    </div>
    <?php
    $this->endWidget();
}

// Company
$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
$this->beginWidget('application.components.widgets.AOPanelHead', array('panelTitle' => 'Company Information'));
$this->endWidget();
?>
<div class="ao-panel-content">
    <img class="partial-left" src="<?php echo Image::getImageUrl($model->logo, 125); ?>" alt="<?php echo $model->vendor_name ?>" />
    <div class="partial-right">
        <h2 style="margin-top: 0px;"><?php echo $model->vendor_name ?></h2>
        <p><b>Website: </b><?php echo $model->website ?></p>
        <p><b>Industry: </b><?php echo $model->category->category_name ?></p>
    </div>
    <div style="clear: both">
        <?php
        echo $model->vendor_description;
        ?>
    </div>
</div>
<?php
$this->endWidget();

// Branches
$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
$this->widget('application.components.widgets.AOPanelHead', array('panelTitle' => 'Locations'));
//$branches = $model->activeBranches;

$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $branchData,
    'id' => 'branch-list',
    'enablePagination' => false,
    'columns' => array(
        array('name' => 'branch_name', 'header' => 'Name', 'type' => 'raw'),
        array('name' => 'branch_address', 'header' => 'Address'),
        array('name' => 'hours', 'header' => 'Hours and Info')
    ),
));

$this->endWidget();

// Offers
$offers = $model->almostActiveOffers;

foreach ($offers as $offer){
    $this->renderPartial('_go_live_offer', array('model' => $offer));
}
?>

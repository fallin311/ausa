<?php

$breadcrumbs = array(
    'My Dashboard' => app()->user->getHomeUrl(),
    'Vendors'
);

cs()->registerScriptFile(CGoogleApi::$bootstrapUrl, CClientScript::POS_HEAD);

if(app()->user->hasFlash('setOfferVendor') || app()->user->hasFlash('setBannerVendor') || app()->user->hasFlash('setVendor')){
    cs()->registerScript('updates','setTimeout(function() { showError(\''.current(app()->user->getFlashes()).'\'); }, 500);');
}

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
?>

<?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false)); ?>
<?php $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Vendors')); ?>
<?php
$this->widget('ausa.widgets.AOButtonGroup', array(
    'type' => '',
    'buttons' => array(
        array('icon' => 'cog', 'items' => array(
                array('label' => 'Create Vendor', 'icon' => 'plus', 'url' => url('/vendor/create')),
                '---',
                array('label' => 'Get Help', 'icon' => 'question-sign', 'url' => '#'),
        )),
    ),
));

$this->endWidget();

// the link should do different things based on the circumstances that the user got here by
if($offerRedirect == false && $bannerRedirect == false){
    $vendorClick = array('name' => 'vendor_name', 'header' => 'Name', 'type' => 'raw', 'value' => 'CHtml::link($data->vendor_name . (($data->published) ? "" : " <span style=\'color: red;\'>(unpublished)</span>"),url("/vendor/go/" . $data->vendor_id))');
} elseif($offerRedirect) {
    $vendorClick = array('name' => 'vendor_name', 'header' => 'Name', 'type' => 'raw', 'value' => 'CHtml::link($data->vendor_name . (($data->published) ? "" : " <span style=\'color: red;\'>(unpublished)</span>"),"?",array(\'submit\' => url("/offer/"),\'params\' => array(\'postOfferVendor\' => $data->vendor_id)))');
} else {
    $vendorClick = array('name' => 'vendor_name', 'header' => 'Name', 'type' => 'raw', 'value' => 'CHtml::link($data->vendor_name . (($data->published) ? "" : " <span style=\'color: red;\'>(unpublished)</span>"),"?",array(\'submit\' => url("/banner/"),\'params\' => array(\'postBannerVendor\' => $data->vendor_id)))');
}

$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $dataProvider,
    'filter' => $model,
    'columns' => array(
        $vendorClick,
        array('header' => 'Sales Team', 'type' => 'raw', 'header' => 'Sales Team', 'value' => '$data->getSalesTeamNames()'),
        array('filter' => false, 'name' => 'chapter', 'value' => '$data->getChapter() == null ? "no chapter" : $data->getChapter()->chapter_name'),
        array('header' => 'Status', 'type' => 'raw', 'value' => '$data->getColoredStatus()'),
        array('header' => 'Branches', 'value' => '$data->getTotalBranches()'),
        array('filter' => false, 'name' => 'expiration', 'header' => 'Expiration', 'type' => 'raw', 'value'=>'"<span " . ((date($data->expiration) > Shared::timeNow()) ? "class=\"date\"" : "class=\"date ausa-unpublished\"") . ">" . Shared::formatShortUSDate($data->expiration) . "</span>"'),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
        ),
    ),
));
?>

<?php $this->endWidget(); ?>

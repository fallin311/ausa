<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'vendor_id',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'vendor_name',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'vendor_description',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'internal_notes',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'logo',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'vendor_banner_image',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'address',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'website',array('class'=>'span5','maxlength'=>127)); ?>

	<?php echo $form->textFieldRow($model,'industry',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'disabled',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'created_on',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'updated_on',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'expiration',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'max_offers',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'max_banners',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'banner_expiration',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'max_branches',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'plan_id',array('class'=>'span5','maxlength'=>10)); ?>

	

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
		    'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php
$this->layout = '//layouts/full';
$this->breadcrumbs=array(
	'Vendors'=>array('index'),
	$model->vendor_id,
);

$this->menu=array(
	array('label'=>'List Vendor','url'=>array('index')),
	array('label'=>'Create Vendor','url'=>array('create')),
	array('label'=>'Update Vendor','url'=>array('update','id'=>$model->vendor_id)),
	array('label'=>'Delete Vendor','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->vendor_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Vendor','url'=>array('admin')),
);
?>

<?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false)); ?>
<h1><?php echo $model->vendor_name; ?></h1>

<?php $this->widget('application.components.widgets.AONotice', array('notice' => 'This is the '.$model->vendor_name.' Dashboard.')); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'vendor_id',
		'vendor_name',
		'vendor_description',
		/*'internal_notes',
		'logo',
		'vendor_banner_image',
		'address',
		'website',
		'industry',
		'disabled',
		'created_on',
		'updated_on',
		'expiration',
		'max_offers',
		'max_banners',
		'banner_expiration',
		'max_branches',
		'plan_id',
		'customer_number',
		'payment_method',
		'billing_name',
		'billing_address',
		'billing_state',
		'billing_country',
		'billing_zipcode',
		'billing_phone',*/
	),
)); ?>
<?php $this->endWidget(); ?>

</div>
<div class="ausa-contentMain row-fluid">

<?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false)); ?>
    <?php $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons download_to_computer' ,'panelTitle' => 'Offers')); ?>
    <?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>

</div>
<div class="ausa-contentMain row-fluid">

<?php $this->beginWidget('application.components.widgets.AOPanel'); ?>
    <?php $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons download_to_computer' ,'panelTitle' => 'Brief Report')); ?>
    <?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>

<?php $this->beginWidget('application.components.widgets.AOPanel'); ?>
    <?php $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons download_to_computer' ,'panelTitle' => 'Subscription')); ?>
    <?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>

</div>
<div class="ausa-contentMain row-fluid">

<?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false)); ?>
    <?php $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons download_to_computer' ,'panelTitle' => 'Banners')); ?>
    <?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>
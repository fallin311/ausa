<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'vendor-form-expiration',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => true,
        ));

if (app()->user->isAdmin()) {
    $button = $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type' => 'default',
        'toggle' => 'radio',
        'buttons' => array(
            array('icon' => 'ok-sign', 'label' => 'Publish', 'active' => $model->published == '1' ? true : false, 'htmlOptions' => array('id' => 'publish-button')),
            array('icon' => 'remove-sign', 'label' => 'Unpublish', 'active' => $model->published == '0' ? true : false, 'htmlOptions' => array('id' => 'unpublish-button'))),
        'htmlOptions' => array('style' => 'text-align:center; margin-bottom:10px;')), true);
    echo $form->customRow($model, 'published', 'Published', $button);
    echo CHtml::activeHiddenField($model, 'published');

    cs()->registerScript('publish-action', '
                       
            $("#publish-button").click(function(){
                if (!$(this).hasClass("active")){
                    $("#Vendor_published").val("1");  
                    }
            });
            
            $("#unpublish-button").click(function(){
                if (!$(this).hasClass("active")){
                    $("#Vendor_published").val("0"); 
                    }
            });         
            
        ');

///Preselect the published button
    if ($model->published) {
        cs()->registerScript('publish-toggle', '            
            $("#publish-button").button("toggle");
            ');
    } else {
        cs()->registerScript('publish-toggle', '            
            $("#unpublish-button").button("toggle");            
        ');
    }
}

if (app()->user->isAdmin()) { //show all plans if user is admin
    echo '<div class="form-row" style="width: 100%;">'
    . '<label for="Vendor_plan" class="required">Plan </label>'
    . '<div class="form-element"><div class="clearfix">';
    $this->widget('ext.select2.ESelect2', array(
        'model' => $model,
        'attribute' => 'plan_id',
        'data' => CHtml::listData(Plan::model()->findAll(array('order' => 'plan_name ASC')), 'plan_id', 'plan_name'),
        'options' => array('placeholder' => 'Choose one')
    ));
} else { // Do not show "admin" access level plans if St is logged in.. 
    echo '<div class="form-row" style="width: 100%;">'
    . '<label for="Vendor_plan" class="required">Plan </label>'
    . '<div class="form-element"><div class="clearfix">';
    $this->widget('ext.select2.ESelect2', array(
        'model' => $model,
        'attribute' => 'plan_id',
        'data' => CHtml::listData(Plan::model()->findAll(array('order' => 'plan_name ASC', 'condition' => 'access_level!=:x', 'params' => array(':x' => 'admin'))), 'plan_id', 'plan_name'),
        'options' => array('placeholder' => 'Choose one')
    ));
}
echo '</div></div></div>';

if (app()->user->isAdmin()) {
//echo $form->textFieldRow($model,'disabled',array('class'=>'span5'));
    if ($model->expiration == 0) {
        $model->expiration = '';
    } else {
        $model->expiration = Shared::toDatabaseShort($model->expiration);
    }
    echo $form->datepickerRow($model, 'expiration', array('hint' => 'When subscription expires and offers stop showing in cellphone application.'));

    echo $form->textFieldRow($model, 'max_offers', array('class' => 'span'));


    echo $form->textFieldRow($model, 'max_banners', array('class' => 'span'));

    if ($model->banner_expiration == 0) {
        $model->banner_expiration = '';
    } else {
        $model->banner_expiration = Shared::toDatabaseShort($model->banner_expiration);
    }
    echo $form->datepickerRow($model, 'banner_expiration');


    echo $form->textFieldRow($model, 'max_branches', array('class' => 'span'));
}
$this->beginWidget('application.components.widgets.AOPanelFoot');

/* $this->widget('bootstrap.widgets.TbButton', array(
  'buttonType' => $model->isNewRecord ? 'submit' : 'ajaxSubmit',
  'type' => 'success',
  'htmlOptions' => array('style' => 'margin: 7px 5px; float: right', 'onclick' => 'showSuccess(\'Vendor changes updated.\')'),
  'size' => 'small',
  'icon' => 'ok white',
  'label' => $model->isNewRecord ? 'Create' : 'Update',
  )); */
/* $this->widget('bootstrap.widgets.TbButton', array(
  'buttonType' => 'reset',
  'type' => '',
  'htmlOptions' => array('style' => 'margin: 7px 5px; float: right', 'onclick' => 'showDefault(\'Vendor changes reset.\')'),
  'size' => 'small',
  'icon' => 'remove',
  'label' => $model->isNewRecord ? 'Clear' : 'Cancel',
  )); */ //Vk Commented the clear Button

$this->endWidget();

$this->endWidget();

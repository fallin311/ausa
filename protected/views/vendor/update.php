<?php
$this->pageTitle = Yii::app()->name . ' - Vendor Update';
$this->layout = '//layouts/full';

if (app()->user->IsSalesman() || app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        $model->vendor_name . ' Dashboard' => url('vendor/go/' . $model->vendor_id),
        'Settings',
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('vendor/dashboard'),
        'Settings',
    );
}

Yii::import('ext.simpleModal.ModalWidget');
ModalWidget::loadAssets();

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
//cs()->registerCSS('form-bgcolor','form {background-color: #FFF; } form#branch-form {background-color:transparent;}');
$this->widget('application.components.widgets.AOGrid');
?>

<?php
$this->beginWidget('application.components.widgets.AOStickyBar', array('sticky' => true, 'panelWrap' => true));
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'icon' => 'remove white',
    'type' => 'danger',
    'url' => url('/vendor/dashboard'),
    'label' => 'Cancel',
    'htmlOptions' => array(
        'class' => 'pull-right',
        'style' => 'margin: 5px 5px 0px 8px',
    ),
));
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'ajaxSubmit',
    'type' => 'success',
    'url' => url('/vendor/update/' . $model->vendor_id),
    'htmlOptions' => array(
        'class' => 'pull-right',
        'style' => 'margin: 5px 8px',
        'id' => 'save-vendor-button',
    ),
    'loadingText' => 'processing...',
    'ajaxOptions' => array(
        'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
        'beforeSend' => 'function(){$("#save-vendor-button").button("loading")}',
        'success' => 'function(response){
                        $("#save-vendor-button").button("reset");
                        if (response.length < 5){
                            window.location.href="' . url('vendor/dashboard') . '";
                        }else{
                            showError(\'Could not save the vendor. Please check the error fields below.\');
                            $("#ajax-errors-holder").show();
                            $("#ajax-errors").html(response);
                            $(".notification").css({\'display\':\'block\', \'opacity\':\'1\'});
                        }
                    }',
    ),
    'icon' => 'ok white',
    'label' => 'Save',
));
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'ajaxSubmit',
    'type' => '',
    'url' => url('/vendor/update/' . $model->vendor_id),
    'htmlOptions' => array(
        'class' => 'pull-right',
        'style' => 'margin: 5px 8px',
        'id' => 'apply-vendor-button',
    ),
    'loadingText' => 'processing...',
    'ajaxOptions' => array(
        'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
        'beforeSend' => 'function(){$("#apply-vendor-button").button("loading")}',
        'success' => 'function(response){
                            $("#apply-vendor-button").button("reset");
                            console.log(response);
                            if (response.length < 6){
                                showSuccess(\'Vendor settings have been updated.\');
                                $("#ajax-errors-holder").hide();
                            } else {
                                showError(\'Could not apply changes to the vendor. Please check the error fields below.\');
                                $("#ajax-errors-holder").show();
                                $("#ajax-errors").html(response);
                                $(".notification").css({\'display\':\'block\', \'opacity\':\'1\'});
                            }
                        }',
    ),
    'icon' => 'ok',
    'label' => 'Apply',
));
$this->endWidget();
?>

<div style="width: 100%; margin-bottom: 25px; position: relative; float: left; display: none;" id="ajax-errors-holder">
    <?php
    cs()->registerCss('error-overwrite', '.notification { margin-top: 0px; }');
    $this->widget('application.components.widgets.AONotice', array('notice' => '<div id="ajax-errors"></div>', 'noticeType' => 'errors', 'noticeClosable' => false));
    ?>
</div>

<div class="ausa-contentMain row-fluid">
    <div id="ausa-contentMainTiles" style="padding-bottom: 40px;">
        <ul id="tiles">
            <li class="grid">
                <?php
                // Basic company information
                $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Company Information'));
                echo $this->renderPartial('_company_form', array('model' => $model));
                $this->endWidget();
                ?>
            </li>
            <li class="grid">
                <?php
                // User contact form
                $this->beginWidget('application.components.widgets.AOPanel');
                $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons download_to_computer', 'panelTitle' => 'Contact Person'));
                echo $this->renderPartial('//user/_contact_form', array('model' => $user, 'userVendor' => $userVendor));
                $this->endWidget();
                ?>
            </li>
            <?php
            if (app()->user->isAdmin()) {
                ?>
                <li class="grid">
                    <?php
                    // extended expiration form
                    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                    $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Limits and expirations'));
                    echo $this->renderPartial('_expiration_form', array('model' => $model));
                    $this->endWidget();
                    ?>
                </li>
                <?php
            }
            // contact people inside salesteam
            if (app()->user->isSalesman()) {
                echo '<li class="grid">';
                $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Sales Team Contacts'));
                echo $this->renderPartial('_salesman_form', array('model' => $model, 'salesTeamId' => app()->user->getActiveSt()));
                $this->endWidget();
                echo '</li>';
            } else if (app()->user->isAdmin() && !$model->isNewRecord) {
                // admins might have possibly more sales teams available
                $stId = array();
                foreach ($model->salesTeams as $st) {
                    $stId[] = $st->sales_team_id;
                }
                ?>
                <li class="grid">
                    <?php
                    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                    $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Sales Team Contacts'));
                    echo $this->renderPartial('_salesman_form', array('model' => $model, 'salesTeamId' => $stId));
                    $this->endWidget(); ?>
                </li>
                <?php } ?>
        </ul>
    </div>

    <?php
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));

    // count should consider active branches only
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'color-icons application_get_co', 'panelTitle' => 'Branches (' . $branchProvider->totalItemCount . '/' . $model->max_branches . ')'));
    $this->widget('ausa.widgets.AOButtonGroup', array(
        'type' => '',
        'htmlOptions' => array('id' => 'branch-dropdown'),
        'buttons' => array(
            array('icon' => 'cog', 'items' => array(
                    array('label' => 'Add a Branch', 'icon' => 'plus', 'url' => 'create', 'id' => 'create-branch-popup'),
                    '---',
                    array('label' => 'Get Help', 'icon' => 'question-sign', 'url' => '#'),
            )),
        ),
    ));
    $this->endWidget();


    $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $branchProvider,
        'id' => 'branch-list',
        'columns' => array(
            array('name' => 'branch_name', 'header' => 'Name'),
            array('name' => 'branch_address', 'header' => 'Address'),
            array('name' => 'active', 'header' => 'Active?', 'type' => 'raw', 'value' => '($data->active?"<span class=\"label label-success\">Yes</span>":"<span class=\"label label-important\">No</span>")', 'htmlOptions' => array('class' => 'status-center')),
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{edit}',
                'buttons' => array(
                    'edit' => array(
                        'icon' => 'pencil',
                        'options' => array(
                            'class' => 'update-popup',
                        ),
                        'url' => '"' . url('/branch/update') . '/" . $data->branch_id'
                    )
                )
            ),
        ),
    ));

    $this->endWidget();

    // render branch update form
    ?>

    <div id="branch-update-form" style="display: none;" class="ausa-contentMainInner">
        <?php $this->renderPartial('//branch/_form', array('model' => new Branch, 'vendor' => $model)); ?>
    </div>
    <?php
    cs()->registerCss('popup-form', '
    #branch-update-form {
        width: 100% !important;
        border: 0px solid black !important;
        margin: 0px !important;
    }
'
    );
    cs()->registerScript('branch-update-popup', '
    /** display the crate / update popup */
    function showUpdatePopup(){
        $("#branch-update-form").modal({
        overlayClose: true,
        containerCss:{
		padding:0, 
		width:500,
                height: 540
	},
        onClose: function(){
            // restore edit icon
            $("a span.loading-icon").parent().html("<i class=\"icon-pencil\"></i>");
            // do not forget to close it manually
            $.modal.close();
        }
        });
    }
    
    /** Get data to be displayed in the popup */
    function prepareUpdatePopup(branchId){
        if (isNaN(branchId)){
            // prepare default data
            data = ({
                branch_id: "",
                active: 1,
                vendor_id: "' . $model->vendor_id . '",
                branch_name: "' . CHTml::encode($model->vendor_name) . ' #' . ((int) $branchProvider->totalItemCount + 1) . '",
                branch_address: "",
                phone_number: "",
                website: "",
                hours: ""
            });
            var $elmnt;
            // fill in the empty values
            $.each(data, function(i, val) {
                $elmnt = $("#Branch_" + i);
                if (($elmnt).length > 0){
                    // update the field value
                    $elmnt.val(val);
                }
            });
            showUpdatePopup();
        }else{
            $.ajax({
                url: "' . url('/branch/updateJson') . '/" + branchId,
                type: "post",
                dataType: "json",
                success: function(data){
                    if (data["success"]){
                        // update form fields
                        var $elmnt;
                        $.each(data["data"], function(i, val) {
                            $elmnt = $("#Branch_" + i);
                            if (($elmnt).length > 0){
                                // update the field value
                                $elmnt.val(val);
                            }
                        });
                        showUpdatePopup();
                    }else{
                        showError("You do not have a permission to do that.");
                    }
                },
                error: function(){
                    alert("Cannot update the branch. Please contact our technical support");
                }
            });
        }
    }
    
    /** Bind update /create buttons */
    $("a.update-popup").live("click", function(e){
        e.preventDefault();
        var url = $(this).attr("href");
        var branchId = url.substring(url.lastIndexOf("/") + 1);
        // change the icon
        $(this).html("<span class=\"loading-icon\"></span>");
        prepareUpdatePopup(branchId);
    });   
    $("#branch-dropdown a[href=\'create\']").click(function(e){
        e.preventDefault();
        prepareUpdatePopup("new");
    });
');
    
    /*if (!app()->user->isAdmin()){
    
// listen for email address change and bind event to global update / apply button
cs()->registerScript('email-changes', '
var origEmail = "' . $model->email_address . '";
var emailChanged = false;
$("#User_email_address").change(function(){
    if($("#User_email_address").value() != origEmail) {
        console.log("change detected");
        emailChanged = true;
    }
});

$("#apply-vendor-button").click(function(){
    console.log("apply again, mofo");
});
');
}*/
    ?>
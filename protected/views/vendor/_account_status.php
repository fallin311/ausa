<?php
$missing = $model->isVerified();
if ($missing !== true) {
    ?>
    <div class="ausa-info ausa-contentMainInner">
        <h2>Your account is not active yet...</h2>
        <p>Please complete the following step(s) to activate your account.</p>
        <ol style="list-style: decimal">
            <?php
            foreach ($missing as $error) {
                echo "<li>" . $error . "</li>";
            }
            ?>
        </ol>
    </div>
    <?php
    // register scripts to handle user email verification and activation request
    $userActivation = '
        var sendBtn = $("#verify-email");
        var placeholder = $("#email-verification-placeholder");
        
        sendBtn.click(function(){
            $.ajax({
                url: "' . url('/user/sendVerificationEmail') . '",
                data: "id=" + sendBtn.data("email-for"),
                beforeSend: function(){
                    console.log("before-send");
                    sendBtn.removeClass("ajax-link");
                    placeholder.html("' . (app()->user->isAdmin() ? 'Updating Database...' : 'Sending verification email...') . '");
                },
                success: function(ret){
                    if (ret) {
                        placeholder.html("' . (app()->user->isAdmin() ? 'The email address verification is complete.' : 'Verification email successfully sent. Please check your email and click the verification link.') . '");
                    } else {
                        placeholder.html("<font color=\"red\">Could not send email</font>. Please contact our <a href=\"'. url('/vendor/support') .'\">user support</a>.");
                    }
                }
            });
        });
';
    
   /* if (app()->user->isAdmin()){
        // admin has an option to verify the user directly without sending him email
        $userActivation .= '
            sendBtn.click(function(){
            $.ajax({
                url: "' . url('/user/sendVerificationEmail') . '",
                data: "id=" + sendBtn.data("email-for"),
                beforeSend: function(){
                    console.log("before-send");
                    sendBtn.removeClass("ajax-link");
                    placeholder.html("Sending verification email...");
                },
                success: function(ret){
                    if (ret) {
                        placeholder.html("The email was successfully sent. Please check your email.");
                    } else {
                        placeholder.html("Could not send the email. Please contact our user support.");
                    }
                }
            });
        });            
';
    }*/
    
    $vendorActivation = '
        var sendBtn = $("#activate-vendor");
        var placeholder = $("#vendor-activation-placeholder");
        
        sendBtn.click(function(){
            $.ajax({
                url: "' . url('/vendor/sendActivationEmail/' . $model->vendor_id) . '",
                beforeSend: function(){
                    sendBtn.removeClass("ajax-link");
                    placeholder.html("Sending activation request...");
                },
                success: function(ret){
                    if (ret) {
                        placeholder.html("Thank you for activating your account. Our team will let you know when the account is activated. It usually takes one to two business days.");
                    } else {
                        placeholder.html("Could not send the request. Please contact our user support.");
                    }
                }
            });
        });
';
    //cs()->registerScript('account-activation', isset($missing['activate']) ? $userActivation : $vendorActivation);
    cs()->registerScript('account-activation', $userActivation);
}
?>

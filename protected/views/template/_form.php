<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'template-form',
    //'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => false,
        ));

echo $form->errorSummary($model);

echo $form->textFieldRow($model, 'offer_name', array('class' => 'span', 'maxlength' => 63));
echo $form->textAreaRow($model, 'offer_description', array('class' => 'span', 'maxlength' => 63));

echo $form->dropDownListRow($model, 'category_id', CHtml::listData(Category::model()->findAllByAttributes(array('published'=>'1'), array('order'=>'category_name ASC')), 'category_id', 'category_name'), array('empty' => 'All Categories',
        'class' => 'span')
    );

echo $form->imageFieldRow($model, 'offer_image', array('class' => 'span', 'maxlength' => 10, 'imageOptions' => array(
        'configuration' => 'logo',
        'layout' => 'logo-layout'
        )));

echo $form->customRow($model, 'type', 'Type', CHtml::activeDropDownList($model, 'type', 
        array(
            'global' => 'Global',
            'private' => 'Private',
            'disabled' => 'Disabled',
            )), 
        array('class' => 'span'));

$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->widget('bootstrap.widgets.TbButton', array(
  'buttonType' => 'submit',
  'type' => 'success',
  'htmlOptions' => array('style' => 'margin: 7px 5px; float: right'),
  'size' => 'small',
  'icon' => 'ok white',
  'label' => $model->isNewRecord ? 'Create' : 'Update',
  ));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'type' => '',
    'url' => url('/template'),
    'htmlOptions' => array('style' => 'margin: 7px 5px; float: left'),
    'size' => 'small',
    'icon' => 'remove',
    'label' => 'Cancel',
));

$this->endWidget();

$this->endWidget();
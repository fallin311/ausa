<?php

cs()->registerCss('offer-template-items',
    'div#offer-template .summary {
        padding: 8px 15px;
        margin: 0px;
        list-style: none;
        background-color: whiteSmoke;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }'
    .'div#offer-template .items {
        display: inline-block;
        padding: 0px;
        margin-bottom: -3px;
        width: 100%;
        border-top: #D2D2D2 1px solid;
        -webkit-box-shadow: inset 0 0 5px #DDD;
        -moz-box-shadow: inset 0 0 5px #dddddd;
        box-shadow: inset 0 0 5px #DDD;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
    }'
    .'.select-offer-template {
        float: left;
        margin: 10px;
        padding: 10px;
        width: 227px;
        height: 100px;
        background: #EEE;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        border-radius: 4px;
        position: relative;
        -moz-box-shadow: inset 1px 1px 2px #ccc;
        -webkit-box-shadow: inset 1px 1px 2px #CCC;
        box-shadow: inset 1px 1px 2px #CCC;
    }'
    .'.offer-template-items {
        display:block;
        height: 100%;
        background-color: white;
        background-repeat: no-repeat;
        background-position: center 10px;
        overflow: hidden;
        border: 1px solid white;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        border-radius: 4px;
        -webkit-transition: all .2s linear;
        -moz-transition: all .2s linear;
        transition: all .2s linear;
    }'
    .'.offer-template-items:hover {
        color: #A6402C;
        text-decoration: none;
        cursor: pointer;
        -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
        -moz-box-shadow: 0 1px 4px rgba(0,0,0,0.3), 0 0 40px rgba(0,0,0,0.1) inset;
        box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
    }'
    .'.offer-template-items img {
        float: left;
        height: 100%;
        -moz-border-radius: 4px 0px 0px 4px;
        -webkit-border-radius: 4px 0px 0px 4px;
        border-radius: 4px 0px 0px 4px;
    }'
    .'.offer-template-items div.template-title{
        margin-left: 90px;
        line-height: 20px;
        font-size: 13px;
    }');
?>
<div style="<?php if($data->offer_template_id %3 == 0){ echo 'width: 226px;'; } ?>" class="select-offer-template">
    <div class="offer-template-items">
        <img data-id="<?php echo $data->offer_image; ?>" src="<?php echo $data->getImageUrl(Image::SIZE_FULL_W)?>" />
        <div class="template-title"><?php echo CHtml::encode($data->offer_name); ?></div>
        <div class="template-description" style="display: none;"><?php echo CHtml::encode($data->offer_description); ?></div>
    </div>
</div>
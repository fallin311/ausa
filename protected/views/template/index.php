<?php
$this->layout = '//layouts/full';
?>

<?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false)); ?>
    <?php $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck' ,'panelTitle' => 'Offer Templates')); ?>
        <?php
            $this->widget('ausa.widgets.AOButtonGroup', array(
                'type'=>'',
                'buttons'=>array(
                    array('icon'=>'cog', 'items'=>array(
                        array('label'=>'Create Template', 'icon' => 'plus', 'url'=>url('/template/create')),
                        '---',
                        array('label'=>'Get Help', 'icon' => 'question-sign', 'url' => '#'),
                    )),
                ),
            ));
        ?>
    <?php $this->endWidget(); ?>
    <?php
    $this->widget('ausa.widgets.AOListView', array(
        'dataProvider' => $dataProvider,
        'htmlOptions' => array('class' => 'grid-view'),
        'itemsCssClass' => 'items list table-bordered',
        'itemView' => '_view',
        'templatesView' => true,        
    ));
?>
 
<?php $this->endWidget(); ?>

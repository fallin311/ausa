<?php

$this->layout = '//layouts/full';
$breadcrumbs = array(
    'My Dashboard' => app()->user->getHomeUrl(),
    'Chapters'
);

if(app()->user->hasFlash('setChapter')) {
    cs()->registerScript('updates','setTimeout(function() { showError(\''.app()->user->getFlash('setChapter').'\'); }, 500);');
}

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
?>

    <?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false)); ?>
    <?php $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'AUSA Chapters')); ?>
    <?php
    $this->widget('ausa.widgets.AOButtonGroup', array(
        'type' => '',
        'buttons' => array(
            array('icon' => 'cog', 'items' => array(
                    array('label' => 'Add Chapter', 'icon' => 'plus', 'url' => url('chapter/create')),
                    '---',
                    array('label' => 'Get Help', 'icon' => 'question-sign', 'url' => '#'),
            )),
        ),
    ));

    $this->endWidget();

    if($dashBanRedirect == false){
        $chapterClick = array('name' => 'chapter_name', 'header' => 'Chapter Name', 'type' => 'raw', 'value' => 'CHtml::link($data->chapter_name,url("/chapter/go/" . $data->chapter_id))');
    } else {
        $chapterClick = array('name' => 'chapter_name', 'header' => 'Chapter Name', 'type' => 'raw', 'value' => 'CHtml::link($data->chapter_name,"?",array(\'submit\' => url("/dashBanner/"),\'params\' => array(\'postChapter\' => $data->chapter_id)))');
    }

    $dataArray=array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $chapterProvider,
        'columns' => array(
            $chapterClick,
            array('name' => 'ausa_id', 'header' => 'AUSA ID'),
            array('name' => 'city', 'header' => 'City'),
            array('name' => 'state', 'header' => 'State'),
        ),
    );
    
    
    if (app()->user->isAdmin()) {
        $dataArray['columns'][] = array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update} {delete}',
        );
    }
    
    $this->widget('bootstrap.widgets.TbGridView', $dataArray);
    ?>

    <?php $this->endWidget(); ?>
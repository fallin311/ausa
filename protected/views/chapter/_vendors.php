<?php
$this->pageTitle = Yii::app()->name . ' - Chapter Vendors';
$this->layout = '//layouts/full';
$breadcrumbs = array(
    'breadcrumbs not set'
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs, 'map' => true));

$branchArray = Chapter::model()->getVendorBranches($model->chapter_id);

/*cs()->registerScriptFile('https://maps-api-ssl.google.com/maps/api/js?v=3&sensor=false');
cs()->registerScript('chapterVendorMap',
        'function initialize() {'
            .'var mapOptions = {'
                .'center: new google.maps.LatLng(31.771375,-106.487148),'
                .'zoom: 13,'
                .'mapTypeId: google.maps.MapTypeId.ROADMAP,'
                .'overviewMapControl: true,'
                .'overviewMapControlOptions: { opened: false, },'
                .'scrollwheel: false,'
                .'scaleControl: true,'
                .'zoomControlOptions: {'
                    .'style: google.maps.ZoomControlStyle.LARGE,'
                    .'position: google.maps.ControlPosition.LEFT_TOP'
                .'},'
                .'mapTypeControl: true,'
                .'mapTypeControlOptions: {'
                    .'style: google.maps.MapTypeControlStyle.DROPDOWN_MENU'
                .'},'
            .'};'
            .'var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);'
            .'var geocoder = new google.maps.Geocoder();'
        .'}'
        .'initialize();');
*/

$columns = array(
    //array('name' => 'vendor.vendor_id'),
    array('name' => 'vendor.vendor_name', 'header' => 'Name'),
    array('name' => 'salesTeam', 'header' => 'Sales Team', 'value' => 'VendorMembership::model()->getVendorsSalesteam($data->vendor->vendor_id)'),
    //array('name' => 'vendor.address', 'header' => 'Address'),
    array('name' => 'vendor.industry', 'header' => 'Industry', 'value' => '$data->vendor->category->category_name'), //Chapter::model()->getIndustryName($data->vendor->industry)
    array('name' => 'since', 'header' => 'Date Joined', 'type' => 'raw', 'value' => 'Shared::formatShortUSDate($data->since)'),
    array('name' => 'vendor.expiration', 'header' => 'Expires', 'type' => 'raw', 'value' => 'Shared::formatShortUSDate($data->vendor->expiration)'),
        //array('name' => 'vendor.st_name', 'header' => 'Sales Team'),
        /*array(
          'class' => 'bootstrap.widgets.TbButtonColumn',
          'template' => '{update}',
          ), */
);
$dataArray = array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $dataProvider,
    //'template'=>"{items}",
    'columns' => $columns
);
?>

<div class="ausa-contentMain row-fluid">
    <?php
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
    $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Vendors in ' . $chapterName));
    $this->widget('bootstrap.widgets.TbGridView', $dataArray);
    $this->endWidget();
    ?>
<?php
$this->pageTitle = Yii::app()->name . ' - Chapter Dashboard';
$this->layout = '//layouts/full';
$breadcrumbs = array(
    'My Dashboard',
);

if(app()->user->hasFlash('success')) {
    cs()->registerScript('updates','setTimeout(function() { showSuccess(\''.app()->user->getFlash('success').'\'); }, 500);');
}

$chart = '<div id="vendor-mainDashboard">
                        <div class="dashboardHighlight" style="clear: both; overflow: hidden; min-height: 265px;">
                            <div class="columns">
                                <div style="float: left; width: 70%; font-family: \'Cabin Sketch\'; font-size: 60px;">
                                    <div id="chartArea">
                                        <svg width="760" height="265" style="overflow: hidden; ">
                                        <defs id="defs">
                                        <clipPath id="_ABSTRACT_RENDERER_ID_0">
                                            <rect x="30" y="35" width="720" height="164"></rect>
                                        </clipPath>
                                        </defs>
                                        <g>
                                        <text text-anchor="start" x="30" y="22.4" font-family="Arial" font-size="14" font-weight="bold" stroke="none" stroke-width="0" fill="#ffffff">Member Influx Graph (Under Construction)</text>
                                        </g>
                                        <g>
                                        <rect x="30" y="35" width="720" height="164" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect>
                                        <g clip-path="url(#_ABSTRACT_RENDERER_ID_0)">
                                        <g>
                                        <rect x="30" y="198" width="1100" height="1" stroke="none" stroke-width="0" fill="#cccccc"></rect>
                                        <rect x="30" y="157" width="1100" height="1" stroke="none" stroke-width="0" fill="#cccccc"></rect>
                                        <rect x="30" y="117" width="1100" height="1" stroke="none" stroke-width="0" fill="#cccccc"></rect>
                                        <rect x="30" y="76" width="1100" height="1" stroke="none" stroke-width="0" fill="#cccccc"></rect>
                                        <rect x="30" y="35" width="1100" height="1" stroke="none" stroke-width="0" fill="#cccccc"></rect>
                                        </g>
                                        <g>
                                        <rect x="30" y="198" width="720" height="1" stroke="none" stroke-width="0" fill="#666666"></rect>
                                        </g>
                                        <g>
                                        <path d="M76.29166666666666,164.54166666666666L167.875,148.92083333333335L259.4583333333333,127.86666666666666L351.04166666666663,110.8875L442.625,99.34166666666667L534.2083333333333,78.96666666666667L625.7916666666666,104.09583333333333L717.375,97.30416666666666L808.9583333333333,50.44166666666666L900.5416666666666,66.74166666666667L992.125,133.3L1083.7083333333333,162.50416666666666" stroke="#3366cc" stroke-width="2" fill-opacity="1" fill="none"></path>
                                        <path d="M76.29166666666666,142.80833333333334L167.875,146.20416666666665L259.4583333333333,131.94166666666666L351.04166666666663,134.65833333333333L442.625,127.1875L534.2083333333333,143.4875L625.7916666666666,127.86666666666666L717.375,127.86666666666666L808.9583333333333,136.01666666666665L900.5416666666666,142.12916666666666L992.125,125.82916666666667L1083.7083333333333,136.69583333333333" stroke="#dc3912" stroke-width="2" fill-opacity="1" fill="none"></path>
                                        <path d="M76.29166666666666,164.54166666666666L167.875,172.0125L259.4583333333333,172.0125L351.04166666666663,170.65416666666667L442.625,166.57916666666665L534.2083333333333,165.22083333333333L625.7916666666666,158.42916666666667L717.375,158.42916666666667L808.9583333333333,163.18333333333334L900.5416666666666,155.03333333333333L992.125,158.42916666666667L1083.7083333333333,163.8625" stroke="#ff9900" stroke-width="2" fill-opacity="1" fill="none"></path>
                                        <path d="M76.29166666666666,167.9375L167.875,174.72916666666666L259.4583333333333,174.72916666666666L351.04166666666663,172.0125L442.625,162.50416666666666L534.2083333333333,146.88333333333333L625.7916666666666,160.46666666666667L717.375,158.42916666666667L808.9583333333333,165.9L900.5416666666666,171.33333333333334L992.125,165.9L1083.7083333333333,184.2375" stroke="#109618" stroke-width="2" fill-opacity="1" fill="none"></path>
                                        </g>
                                        </g>
                                        <g></g>
                                        <g>
                                        <g><text text-anchor="middle" x="76.29166666666666" y="219.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">Jan</text></g>
                                        <g><text text-anchor="middle" x="167.875" y="219.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">Feb</text></g>
                                        <g><text text-anchor="middle" x="259.4583333333333" y="219.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">Mar</text></g>
                                        <g><text text-anchor="middle" x="351.04166666666663" y="219.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">Apr</text></g>
                                        <g><text text-anchor="middle" x="442.625" y="219.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">May</text></g>
                                        <g><text text-anchor="middle" x="534.2083333333333" y="219.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">Jun</text></g>
                                        <g><text text-anchor="middle" x="625.7916666666666" y="219.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">Jul</text></g>
                                        <g><text text-anchor="middle" x="717.375" y="219.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">Aug</text></g>
                                        <g><text text-anchor="middle" x="808.9583333333333" y="219.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">Sep</text></g>
                                        <g><text text-anchor="middle" x="900.5416666666666" y="219.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">Oct</text></g>
                                        <g><text text-anchor="middle" x="992.125" y="219.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">Nov</text></g>
                                        <g><text text-anchor="middle" x="1083.7083333333333" y="219.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">Dec</text></g>
                                        <g><text text-anchor="end" x="27" y="203.4" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">0</text></g>
                                        <g><text text-anchor="end" x="27" y="162.65" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">60</text></g>
                                        <g><text text-anchor="end" x="27" y="121.9" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">120</text></g>
                                        <g><text text-anchor="end" x="27" y="81.15" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">180</text></g>
                                        <g><text text-anchor="end" x="27" y="40.4" font-family="Arial" font-size="14" stroke="none" stroke-width="0" fill="#ffffff">240</text></g>
                                        </g>
                                        </g>
                                        <g></g>
                                        </svg>
                                    </div>
                                </div>
                                <div style="float: left; width: 30%;">
                                    <ul class="stats">
                                                                                <li><strong><a data-placement="left" data-title="Total Chapter Members" data-content="This is the total number of active AUSA members within your chapter." rel="clickover" style="cursor: pointer;" data-original-title="">3</a></strong> total active <br>chapter members</li>
                                        <li><strong><a data-placement="left" data-title="Chapter Sales Teams" data-content="This is the total number of sales teams representing your AUSA chapter." rel="clickover" style="cursor: pointer;" data-original-title="">1</a></strong> chapter <br>sales team</li>
                                        <li><strong><a data-placement="left" data-title="Total Chapter Vendors" data-content="This number is the total number of vendors who are a part of your AUSA chapter." rel="clickover" style="cursor: pointer;" data-original-title="">1</a></strong> total chapter<br> vendor</li>
                                        <li><strong><a data-placement="left" data-title="Total Active Offers" data-content="This is the total number of active running offers availible to your AUSA chapter members." rel="clickover" style="cursor: pointer;" data-original-title="">1</a></strong> total active<br> offer</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>';

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs, 'dataHolder' => $chart));
?>
    
<div class="ausa-contentMain row-fluid">
    <?php /* Chapter Dashboard Subcontent (ajax update area) */ ?>
    <div id="vendor-contentDashboard" style="display: inline-block;">
<?php
/*
$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
?>

<div class="widget-content no-head">

    <div>
        <h3>Your Sales Team<?php if ($noOfSts > 1) { ?>s<?php } ?></h3>
        <?php for ($i = 0; $i < $noOfSts; $i++) { ?>
            <img src="<?php echo url('/image/display/' . $stProvider[$i]->logo) . '?w=' . Image::SIZE_ICON_W . '&h=' . Image::SIZE_ICON_H ?>" alt="Logo" />
            <h4><?php echo $stProvider[$i]->st_name; ?> </h4>


        <?php } ?>


    </div>
</div>

<?php
$this->endWidget();

?>

    <?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false)); ?>
    <?php
    if ($model->chapter_name) {
        $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Vendors in ' . $model->chapter_name));
    } else {
        $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Vendors'));
    }
    ?>

    <?php $this->endWidget(); ?>
    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $vendorProvider,
        'columns' => array(
            array('name' => 'vendor.vendor_name', 'header' => 'Name'),
            array('name' => 'vendor.address', 'header' => 'Address'),
            array('name' => 'vendor.category.category_name', 'header' => 'Industry'),
            array('name' => 'since', 'header' => 'Since'),
            array('name' => 'vendor.expiration', 'header' => 'Expires'),
            array('header' => 'Support Team', 'type' => 'raw', 'value' => '$data->vendor->getSalesTeamNames();'),
            array('header' => 'Active Offers / Max Allowed', 'type' => 'raw', 'value' => 'count($data->vendor->activeOffers)." / ". $data->vendor->max_offers;'),
        ),
    ));
    ?>

    <?php $this->endWidget();
*/
?>
    </div>
<?php
$this->layout = '//layouts/full';
$breadcrumbs = array(
    'breadcrumbs not set'
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
?>

<?php
    $this->beginWidget('application.components.widgets.AOStickyBar', array('sticky' => true, 'panelWrap' => true));
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'link',
            'icon' => 'remove white',
            'type' => 'danger',
            'url' => url('/site/index'),
            'label' => 'Cancel',
            'htmlOptions' => array('style' => 'margin: 5px 25px 0px 8px; float: right',)
        ));
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'ajaxSubmit',
            'type' => 'success',
            'url' => url('/chapter/create'),
            'htmlOptions' => array(
                'style' => 'margin: 5px 8px; float: right',
                'id' => 'save-chapter-button',
            ),
            'loadingText' => 'processing...',
            'ajaxOptions' => array(
                'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
                'beforeSend' => 'function(){$("#save-chapter-button").button("loading")}',
                'success' => 'function(response){

                    if (response.length < 5){
                        // saved, lets go one step further
                        window.location.href="' . url('chapter/update') . '/" + response;
                    }else{
                        console.log(response);
                        $("#save-chapter-button").button("reset");

                        // we are going to display error message
                        $("#ajax-errors-holder").show();
                        $("#ajax-errors").html(response);
                        showError(\'Could not save the chapter. Please check the error fields below.\');
                    }
                }',
            ),
            'icon' => 'ok white',
            'label' => 'Save',
        ));
    $this->endWidget(); ?>

<div style="width: 100%; margin-bottom: 25px; position: relative; float: left; display: none;" id="ajax-errors-holder">
    <?php
    $this->widget('application.components.widgets.AONotice', array('notice' => '<div id="ajax-errors"></div>', 'noticeType' => 'errors'));
    ?>
</div>

    <?php
// Basic company information
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Chapter Information'));
    $this->endWidget();
    echo $this->renderPartial('_form', array('model' => $model));
    $this->endWidget();

// User contact form
    $this->beginWidget('application.components.widgets.AOPanel');
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons download_to_computer', 'panelTitle' => 'Contact Person'));
    $this->endWidget();
    echo $this->renderPartial('//user/_contact_form', array('model' => $user, 'userChapter' => $userChapter));
    $this->endWidget();
    ?>
<?php

$this->widget('application.components.widgets.AODashboard', array(
    'items' => array(
        'home' => array('icon' => 'dashboard-home', 'text' => 'Chapter Dashboard', 'url' => url('chapter/dashboard')),
        'members' => array('icon' => 'dashboard-members', 'text' => 'Chapter Members', 'url' => url('chapter/members')),
        'vendors' => array('icon' => 'dashboard-vendors', 'text' => 'Chapter Vendors', 'url' => url('chapter/vendors')),
        'salesteams' => array('icon' => 'dashboard-salesteams', 'text' => 'Chapter<br/>Sales Teams', 'url' => url('salesTeam/'), 'visible' => app()->user->isAdmin()),
        'settings' => array('icon' => 'dashboard-settings', 'text' => 'Chapter Settings', 'url' => url('chapter/update/' . $model->chapter_id)),
    ), 'multiCol' => false,
));
?>

<?php

cs()->registerCss('select2-css',
    '#s2id1 {
        width: 100%;
        margin-bottom: 10px;
    }'
    .'.select2-drop-above {
        -webkit-box-shadow: none !important;
        -moz-box-shadow: none !important;
        -o-box-shadow: none !important;
        box-shadow: none !important;
    }'
    .'.select2-container .select2-choice div {
        -webkit-border-radius: 0 3px 3px 0;
        -moz-border-radius: 0 3px 3px 0;
        border-radius: 0 3px 3px 0;
        border-left: 1px solid #ccc;
    }');

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'chapter-form',
    //'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => true,
        ));

echo $form->errorSummary($model);

echo $form->textFieldRow($model, 'chapter_name', array('class' => 'span', 'maxlength' => 128));

if (app()->user->isAdmin()) {
    //if (count($model->entity) == 0){
        echo '<div class="form-row" style="width: 100%;">'
            .'<label for="Chapter_entityId" class="required">Entity </label>'
            .'<div class="form-element"><div class="clearfix">';
        $this->widget('ext.select2.ESelect2', array(
            'model' => $model,
            'attribute' => 'entity_id',
            'data' => CHtml::listData(Entity::model()->findAll(), 'entity_id', 'entity_name'),
            'options' => array('placeholder' => 'Select Entity')
        ));
        echo '</div></div></div>';
    //}
}

echo $form->textFieldRow($model, 'ausa_id', array('class' => 'span', 'maxlength' => 16));

echo CHtml::tag('fieldset', array('class' => 'field-label'), $form->textFieldRow($model, 'address', array('class' => 'span')));

echo CHtml::tag('fieldset', array('class' => 'field-label'), $form->textFieldRow($model, 'city', array('class' => 'span', 'maxlength' => 45)));

echo CHtml::tag('fieldset', array('class' => 'field-label'), $form->textFieldRow($model, 'state', array('class' => 'span', 'maxlength' => 45)));

echo CHtml::tag('fieldset', array('class' => 'field-label'), $form->textFieldRow($model, 'zipcode', array('class' => 'span', 'maxlength' => 45)));

echo CHtml::tag('fieldset', array('class' => 'field-label'), $form->textFieldRow($model, 'country', array('class' => 'span', 'maxlength' => 45)));

echo $form->imageFieldRow($model, 'logo', array('class' => 'span', 'maxlength' => 10, 'imageOptions' => array(
    'configuration' => 'logo',
    'layout' => 'logo-layout'
)));

if (app()->user->isAdmin()) {
    echo CHtml::tag('fieldset', array('class' => 'field-label'), $form->textFieldRow($model, 'offer_app_url', array('class' => 'span', 'maxlength' => 127)));
}

echo CHtml::tag('fieldset', array('class' => 'field-label'), $form->textFieldRow($model, 'chapter_url', array('class' => 'span', 'maxlength' => 127)));


echo CHtml::tag('fieldset', array('class' => 'field-label'), $form->dropDownListRow($model, 'timezone', Shared::$timezones, array('hint' => 'Your offers will have expiration based on this timezone related to GMT')));

// echo $form->textFieldRow($model,'logo',array('class'=>'span5','maxlength'=>10)); 

$this->beginWidget('application.components.widgets.AOPanelFoot');


$this->endWidget();

$this->endWidget();


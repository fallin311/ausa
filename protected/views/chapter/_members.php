<?php

$this->pageTitle = Yii::app()->name . ' - Chapter Members';
$this->layout = '//layouts/full';
$breadcrumbs = array(
    'breadcrumbs not set'
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));

$columns = array(
    //array('name' => 'vendor_id', 'header' => '#'),
    array('name' => 'ausa_id'),
    array('name' => 'last_name', 'header' => 'Name', 'value' => '$data->getFullName()'),
    array('name' => 'email_address', 'header' => 'Email'),
    array('name' => 'phone_number', 'value' => 'Shared::formatPhone($data->phone_number)'),
    array('name' => 'membership.ausa_product_code'),
    array('name' => 'membership.expiration_date', 'value' => 'Shared::formatShortUSDate($data->membership->expiration_date)')
    //array('name' => 'email_address', 'header' => 'Email'),
);
if (app()->user->isAdmin()) {
    /*$columns[] = array(
        'class' => 'bootstrap.widgets.TbButtonColumn',
        'template' => '{update}{delete}',
    );
     */
} else {
    $columns[] = array('name' => 'member.update', 'header' => '', 'type' => 'raw', 'value' => 'CHtml::link("<button class=\"btn btn-small\"><i class=\"icon-pencil\"></i></button>",url("/member/update/" . $data->member_id))', 'htmlOptions' => array('width' => '20', 'class' => 'status-center'));
}

$dataArray = array(
        'type' => 'striped bordered condensed',
        'filter' => $member,
        'dataProvider' => $member->search(),
        //'template'=>"{items}",
        'columns' => $columns,
    );

if(app()->user->hasFlash('success')) {
    cs()->registerScript('updates','setTimeout(function() { showSuccess(\''.app()->user->getFlash('success').'\'); }, 500);');
}
?>

<div class="ausa-contentMain row-fluid">
    <?php
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons marker', 'panelTitle' => 'Chapter Members'));
        $this->widget('ausa.widgets.AOButtonGroup', array(
            'type'=>'',
            'htmlOptions' => array('style' => 'color: red'),
            'buttons'=>array(
                array('icon'=>'cog', 'items'=>array(
                    array('label'=>'Add a Member', 'icon' => 'plus', 'url'=>url('/member/create')),
                    array('label'=>'Upload Members', 'icon' => 'circle-arrow-up', 'url'=>url('/member/upload')),
                    '---',
                    array('label'=>'Get Help', 'icon' => 'question-sign', 'url' => '#'),
                )),
            ),
        ));
    $this->endWidget();

    $this->widget('bootstrap.widgets.TbGridView', $dataArray);

$this->endWidget();
?>
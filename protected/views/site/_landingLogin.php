<?php
cs()->registerCSS('modal-css', 'form { margin-bottom: 0px; } .modal-body { padding: 0px; } .ausa-userSignin { margin-top: 15px; }');
?>
<div class="togglable-tabs tabs-below" id="yw46">
    <div class="tab-content">
        <div id="yw46_tab_1" class="tab-pane fade active in">
            <?php
            $model = new LoginForm;
            $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id' => 'login-form',
                'type' => 'horizontal',
                'htmlOptions' => array('class' => 'ausa-userSignin'),
                'action' => url('/login'),
                'focus' => true,
                    ));

            cs()->registerScript('clickfocus', '$("#vendLogin").click(function() {
                    $("#LoginForm_username").focus();
                });');

            echo $form->textFieldRow($model, 'username', array('class' => 'span3', 'tabindex' => '1', 'placeholder' => 'Email', 'maxlength' => 255));
            echo $form->passwordFieldRow($model, 'password', array('class' => 'span3', 'tabindex' => '2', 'placeholder' => 'Password', 'maxlength' => 32));
            echo CHtml::hiddenField('ajax', '1');

            echo $form->checkBoxRow($model, 'rememberMe', array('tabindex' => '3'));

            echo '<div class="modal-footer">';

            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'button',
                'type' => 'primary',
                'icon' => 'user white',
                'htmlOptions' => array(
                    'id' => 'submit-signin',
                    'tabindex' => '4'
                ),
                'loadingText' => '<i class="icon-spinner icon-spin"></i> Processing...',
                'label' => 'Sign In',
            ));

            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'button',
                'type' => 'warning',
                'icon' => 'question-sign white',
                'htmlOptions' => array(
                    'class' => 'pull-left',
                    'data-toggle' => 'tab',
                    'href' => '#yw46_tab_2',
                    'tabindex' => '5'
                ),
                'label' => 'Forgot Password?',
            ));

            cs()->registerScript('login-submit', "
                    function submitLoginForm(){
                        $.ajax({
                            data: $('#login-form').serialize(0),
                            type: 'post',
                            url: '" . urlSec('/login') . "',
                            completedText: 'redirecting...',
                            beforeSend: function(){ $('#submit-signin').button('loading') },
                            success: function(response){
                                if (response == 'bad'){
                                    showError('Sign in failed. Please try again.');
                                    $('#submit-signin').button('reset');
                                } else {
                                    $(location).attr('href', response);
                                }
                            }
                        });
                    }
                    $('#submit-signin').click(function(){submitLoginForm()});
                    $('#login-form input').keypress(function(e){
                        if(e.which == 13){
                            submitLoginForm()
                        }
                    });", CClientScript::POS_READY);
            cs()->registerCSS('reset-paragraph', '.par { font-size: 12px; text-align: center; }');
            $this->endWidget();

            echo '</div>';
            ?>
        </div>
        <div id="yw46_tab_2" class="tab-pane fade">
            <div class="alert alert-block">
                <div class="par">If you've forgotten your password, please enter your email address below. We'll send you an email with instructions for resetting your password.</div>
            </div>
            <?php
            $resetModel = new User('passwordReset');
            $form = $this->beginWidget('ausa.widgets.TbActiveForm', array(
                'id' => 'user-form',
                'type' => 'horizontal',
                'htmlOptions' => array('class' => 'ausa-userSignin'),
                'action' => url('user/forgotpassword'),
                'focus' => true,
                    ));

            echo $form->textFieldRow($resetModel, 'email_address', array('class' => 'span3', 'tabindex' => '1', 'placeholder' => 'Email', 'maxlength' => 255));
            if (CCaptcha::checkRequirements()):
                ?>

            <!--
                <div class="control-group ">
                    <label class="control-label required" for="User_verify">Validate <span class="required">*</span></label>
                    <div class="controls">
                        <?php
                        echo $form->textField($resetModel, 'verify', array('class' => 'span2', 'tabindex' => '2', 'placeholder' => 'Validation Code'));
                        $this->widget('CCaptcha', array('clickableImage' => true, 'showRefreshButton' => false, 'imageOptions' => array('style' => 'vertical-align: top; margin-top: -10px; cursor: pointer;')));
                        ?>
                    </div>
                </div>
            -->
                <?php
            endif;
            echo CHtml::hiddenField('ajax', '1');

            echo '<div class="modal-footer">';

            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'button',
                'type' => 'primary',
                'icon' => 'envelope white',
                'htmlOptions' => array(
                    'id' => 'submit-reset',
                    'tabindex' => '4',
                    'data-complete-text' => 'Complete',
                ),
                'loadingText' => '<i class="icon-spinner icon-spin"></i> Processing...',
                'label' => 'Request Password Reset',
            ));

            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'button',
                'icon' => 'chevron-left',
                'htmlOptions' => array(
                    'id' => 'gotoSignin',
                    'class' => 'pull-left',
                    'data-toggle' => 'tab',
                    'href' => '#yw46_tab_1',
                    'tabindex' => '5'
                ),
                'label' => 'Back to Sign In',
            ));

            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'button',
                'type' => 'danger',
                'icon' => 'remove white',
                'htmlOptions' => array(
                    'id' => 'close-reset',
                    'style' => 'display: none;',
                    'data-dismiss' => 'modal',
                    'data-toggle' => 'tab',
                    'href' => '#yw46_tab_1',
                ),
                'label' => 'Close',
            ));

            cs()->registerScript('reset-submit', "
                    function submitResetForm(){
                        $.ajax({
                            data: $('#user-form').serialize(0),
                            type: 'post',
                            url: '" . urlSec('user/forgotpassword') . "',
                            completedText: 'redirecting...',
                            beforeSend: function(){ $('#submit-reset').button('loading') },
                            success: function(response){
                                if (response == 'bad request'){
                                    showError('That email address was not found.');
                                    $('#submit-reset').button('reset');
                                } else {
                                    showSuccess('Password reset instructions sent. Please check your email.');
                                    $('#submit-reset').css({'display': 'none'});
                                    $('#close-reset').css({'display': 'initial'});
                                }
                            }
                        });
                    }
                    $('#submit-reset').click(function(){submitResetForm()});
                    $('#user-form input').keypress(function(e){
                        if(e.which == 13){
                            submitResetForm()
                        }
                    });", CClientScript::POS_READY);

            $this->endWidget();

            echo '</div>';
            ?>
        </div>
    </div>
</div>
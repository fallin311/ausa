<?php $this->layout = '//layouts/aoHomeSidebar'; ?>
<div class="heading-bar">
    <h3 class="pull-left">Become a Vendor</h3>
</div>
<div class="padded">
    <div class="slate">
        <div class="slate-content">
            <div class="box-left">
                <img alt="Clipboard" src="<?php echo $this->widget('application.components.widgets.AppHome')->assetsUrl; ?>/clipboard.png">
            </div>

            <div class="box-right">
                <h3>Our Mission:</h3>
                <div class="text">
                    Increase your company’s visibility in the military market. For information on how to be a vendor in this app and market to
                    our soldiers and members, please email us at <a href="mailto:support@ausaoffers.com">support@ausaoffers.com</a> or call us at
                    (915) 400-4090.
                </div>
            </div>
        </div>
    </div>
    <!--
    <p>Our Mission:</p>
    <p>Increase your company’s visibility in the military market. For information on how to be a vendor in this app and market to our soldiers and members, please email us below.</p>
    -->
</div>

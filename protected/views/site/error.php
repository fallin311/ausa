<?php

$this->layout = '//layouts/aoHomeHeader';
$this->pageTitle = $code . ' Error';
$this->heading =
        '<div class="container">'
        . '<h1>Error ' . $code . '</h1>'
        . '<p class="lead">' . nl2br($message) . '</p>'
        . '</div>';
$this->footer = false;

/*if ($code == '500') {
    $this->renderPartial('_500');
} elseif ($code == '404') {
    $this->renderPartial('_404');
} elseif ($code == '403') {
    $this->renderPartial('_403');
} elseif ($code == '400') {
    $this->renderPartial('_400');
}*/

?>
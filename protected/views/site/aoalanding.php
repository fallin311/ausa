<?php
/**
 * This is the custom landing page for AOA Offers
 * Widgets used: AppLandingPage, AppHome, TbModal
 */
$this->pageTitle = app()->name;
$this->layout = '//layouts/appHome';
?>
<div class="base">
    <div class="page">
        <div class="main">
            <div id="home-bg"></div>
            <div class="wrap">
                <div class="phone" id="ausa_slider">
                    <div class="slider">
                        <ul>
                            <li>
                                <div id="slideshow">
                                    <img src="<?php echo $this->widget('application.components.widgets.AppLandingPage')->assetsUrl; ?>/homeScreen.png" class="active" />
                                    <img src="<?php echo $this->widget('application.components.widgets.AppLandingPage')->assetsUrl; ?>/homeScreen.png" />
                                </div>
                                <div class="rows" style="left: -2000px;">
                                    <img src="<?php echo $this->widget('application.components.widgets.AppLandingPage')->assetsUrl; ?>/categories.png">
                                    <img src="<?php echo $this->widget('application.components.widgets.AppLandingPage')->assetsUrl; ?>/phone.png" style="position: absolute; left: 45px; top: 18px;">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="content">
                    <img src="<?php echo url('images/logoAOA.png'); ?>" class="img-title" alt="Appreciation of Army Offers">
                    <div class="tab">
                        <div id="load-content" class="load-tab-content">
                            <h5 style="display: none;">The best deals on your phone when you need it most</h5>
                            <p>
                                <span>AOA</span> <span>Offers</span> is a <span>free phone app</span> primarily catering to our 
                                <span class="for">Active</span>, <span class="for">National Guard</span>, <span class="for">Reserve</span>, 
                                <span class="for">Civilians</span>, <span class="for">Retirees</span> and <span class="for">Family Members</span> 
                                at Ft. Bliss, TX. 
                            </p>
                            <p>
                                <span>Join Today</span> at <span><a href="http://www.ausa.org/membership/Pages/default.aspx" target="_blank">AUSA.org</a></span> and start enjoying <span>amazing</span> <span>deals</span> from <span>vendors like these</span>:
                            </p>
                            <p style="color: red; display: none;">
                                [small icons of few prominent vendors go here. and a "see more" link to redirect to another page and show a list of all vendors in Omar Bradley chapter for now.]
                            </p>
                            <p>
                                <img src="<?php echo $this->widget('application.components.widgets.AppHome')->assetsUrl; ?>/sams_vendor.png" />
                                <img src="<?php echo $this->widget('application.components.widgets.AppHome')->assetsUrl; ?>/tireconnection_vendor.png" />
                                <img src="<?php echo $this->widget('application.components.widgets.AppHome')->assetsUrl; ?>/toro_vendor.png" />
                                <img src="<?php echo $this->widget('application.components.widgets.AppHome')->assetsUrl; ?>/caboj_vendor.png" />
                            </p>
                            <div class="mobios-badge-container">
                                <a class="mobios-badge" href="#" target="_blank"><img src="<?php echo $this->widget('application.components.widgets.AppHome')->assetsUrl; ?>/badge_ios.png" alt="Available on the App Store"></a>
                                <a class="mobios-badge second" href="#" target="_blank"><img src="<?php echo $this->widget('application.components.widgets.AppHome')->assetsUrl; ?>/badge_android.png" alt="Get it on Android"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="foot">
        <ul class="footer">
            <li><a href="<?php echo url('site/contact'); ?>">Contact Us</a><span class="dot divider"> ·</span></li>
            <li><a href="<?php echo url('site/terms'); ?>">Terms</a><span class="dot divider"> ·</span></li>
            <li><a href="<?php echo url('site/privacy'); ?>">Privacy</a><span class="dot divider"> ·</span></li>
            <li><a href="<?php echo url('site/vendors'); ?>">Become a Vendor</a><span class="dot divider"> ·</span></li>
            <?php if(!app()->user->isGuest){ ?>
                <li><a href="<?php echo url(app()->user->getHomeUrl()); ?>">My Dashboard</a><span class="dot divider"> ·</span></li>
            <?php } else { ?>
                <li><a href="#" id="vendLogin" data-toggle="modal" data-target="#vendorLogin">Vendor Login</a><span class="dot divider"> ·</span></li>
            <?php } ?>
            <li><span class="copyright">&copy; <?php echo date('Y'); ?> AOA Offers</span></li>
        </ul>
    </div>
</div>
<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'vendorLogin')); ?>

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Vendor Login</h3>
        </div>

        <div class="modal-body">
            <?php $this->renderPartial('_landingLogin', array(null), false); ?>
        </div>
<?php $this->endWidget(); ?>
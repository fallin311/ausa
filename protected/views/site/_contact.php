<?php $this->layout = '//layouts/aoHomeSidebar'; ?>
<div class="heading-bar">
    <h3 class="pull-left">Contact Us</h3>
</div>
<div class="padded">
    <p>Need help, have a suggestion or a comment?</p>
    <p>Please know that while we do everything we can to answer you in the shortest possible delay, you might not always receive our response as quickly as you would like to. We appreciate your patience.</p>
    <?php if (Yii::app()->user->hasFlash('contact')): ?>
        <div class="flash-success"><?php echo Yii::app()->user->getFlash('contact'); ?></div>
    <?php else: ?>
        <p>Fields with <span class="required">*</span> are required.</p>
        <?php $form = $this->beginWidget('ausa.widgets.AusaActiveForm', array('id' => 'contact-form')); ?>
        <?php echo $form->errorSummary($model); ?>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'name', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model, 'name', array('class' => 'input-xlarge')); ?>
                <?php //echo $form->error($model,'name');  ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'email', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model, 'email', array('class' => 'input-xlarge')); ?>
                <?php //echo $form->error($model,'email');  ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'subject', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model, 'subject', array('class' => 'input-xlarge')); ?>
                <?php //echo $form->error($model,'subject'); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'body', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textArea($model, 'body', array('class' => 'input-xlarge')); ?>
                <?php //echo $form->error($model,'body'); ?>
            </div>
        </div>

        <?php if (CCaptcha::checkRequirements()): ?>
            <div class="control-group">
                <?php echo CHtml::activeLabel($model, 'verifyCode', array('required' => true, 'class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textField($model, 'verifyCode', array('class' => 'input-small')); ?>
                    <?php $this->widget('CCaptcha', array('clickableImage' => true, 'showRefreshButton' => false, 'imageOptions' => array('style' => 'vertical-align: top; margin-top: -10px; cursor: pointer;'))); ?>
                    <?php //echo $form->error($model,'verifyCode'); ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="control-group">
            <div class="controls">
                <?php
                $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'label' => 'Submit',
                    'type' => 'normal',
                    'size' => 'normal',
                ))
                ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    <?php endif; ?>
</div>
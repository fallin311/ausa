<?php
$this->pageTitle = '403 - No Access';
$this->beginWidget('bootstrap.widgets.TbHeroUnit', array('heading' => '403')); ?>
<p>You don't have a permission to access this page. Maybe <a href="<?php echo url('/login') ?>">login</a> can help.</p>

<?php
$this->endWidget();
?>

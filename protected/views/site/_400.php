<?php
$this->pageTitle = '400 - Bad Request';
$this->widget('application.components.widgets.AO400');
?>

    <div class="singleCol">
        <div class="ausa-contentMainInner">
            <div class="widget-content no-head-foot" style="padding: 0px;">
                <div class="ausa-salesTeamDashboard">
                    <div id="vendor-mainDashboard" style="height: 306px;">
                        <div class="dashboardHighlight" style="clear: both; overflow: hidden; height: 265px;">
                            400 - Bad Request
                        </div>
                    </div>
                    <div class="widget-content dashboard">
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->layout = '//layouts/aoHomeSidebar'; ?>
<div class="heading-bar">
    <h3 class="pull-left">Privacy Policy</h3>
</div>
<div class="padded">
<?php
cs()->registerCss('tos',
    '.par {
        padding: 10px;
        padding-left: 12px;
        font-family: Tahoma;
        font-size: 12px;
    }
    .title-this-page {
        font-family: Tahoma;
        font-size: 18px;
        font-weight: bold;
        margin-left: 12px;
        margin-top: 20px;
        margin-bottom: 16px;
    }
    .sub-title {
        font-weight: bold;
        margin-top: 12px;
        margin-bottom: 6px;
        padding-left: 12px;
        font-family: Tahoma;
        font-size: 14px;
    }');
?>
    <div class="par">
        <i>(Revised March 2012)</i><br />
    </div>
    <div class="title-this-page">Our Privacy Policy</div>
    <div class="par">
        We recognize the importance of protecting the privacy of the personally identifiable information you provide to us and to our clients, advertisers, and affiliates (collectively &quot;Affiliates&quot;) in connection with your use of the websites, services, products, and software applications owned, managed or provided to you by us (the &quot;Sites&quot;). We created this Privacy Policy to help you understand what information we collect (whether personally identifiable or not), how we collect it, how we use it, how we share it, how we protect it, and how you can control it.
    </div>

    <div class="par">
        Our Sites may contain links to third-party websites, products and services. This privacy policy only applies to our Sites and our collection and use of your information. The linked websites, products and services of third parties, and their collection and use of your information, are governed by the privacy policies of such third parties. We encourage you to learn about the privacy practices of those third parties.
    </div>

    <div class="title-this-page">Information We Collect</div>

    <div class="par">
        AUSA Offers collaborates closely with The Association of the United States Army (<a href="http://www.ausa.org" target="_blank">http://www.ausa.org</a>) and share some of the member information between each other. We store name, military rank, mobile phone number, mailing address, AUSA ID and email address in our database. As a member, you will be asked to provide us with your email address in order to use our services. There may be other occasions not specifically identified in this Privacy Policy when we may ask you to provide us with personally identifiable information. However, it will always be your decision whether to provide us with your personally identifiable information. In some cases, we can also collect location information. This happens for example during an offer claim process and other cases not specified in this privacy policy.
    </div>
    <div class="par">
        As our member, vendor, client or advertiser we will ask you for name, email address and phone number. Email address is used as a main user identifier. In some cases, if you decline to share such information with us, then we will not be able to provide you with certain services.
    </div>
    <div class="par">
        Additionally, when you download and install our AUSA Offers phone application, we might collect information about your mobile device. It helps us to manage our products and services and to prevent coupon fraud.
    </div>


    <div class="sub-title">Cookie Information</div>
    <div class="par">
        When you visit the Sites, we may send one or more cookies – a small text file containing a string of alphanumeric characters to your computer. We use both session cookies and persistent cookies. A persistent cookie remains after you close your browser and may be used by your browser on subsequent visits to the Sites. These persistent cookies can be removed, but each web browser is a little different. Please review your browser's Help menu to learn how to modify your cookies setup. Our Sites also use web beacons (also known as clear gifs or pixel tags), which are tiny graphics with a unique identifier that are placed in the code of a webpage.
    </div>

    <div class="sub-title">Phone Storage</div>
    <div class="par">
        When you install our AUSA Offers phone application or visit AUSA Offers mobile website, we will store some information on your device. It includes your personal identification, all available active vendors, offers and their locations.
    </div>

    <div class="sub-title">Analytics Tools</div>
    <div class="par">
        We may use tools such as Google Analytics to better understand who is using our Sites and how they are using them. These tools use cookies to collect information such as time of visit, pages visited, time spent on each page of the website, IP address, and type of operating system used. We use this information to manage and improve our Sites.
    </div>

    <div class="title-this-page">How We Use Your Information</div>
    <div class="par">
        We use all of the information that we collect from your use of the Sites, any information you provide when you register with us, and information provided to us by third parties as described below.  The following are the ways we use information we collect:
        <ul style="list-style: disc">
            <li>Operate, maintain, deliver, and communicate with you about the offers, advertisements, content, and promotional offerings found on the Sites.</li>
            <li>Process and record offer requesting and redemption activity.</li>
            <li>Manage and store shopping lists.</li>
            <li>Monitor the effectiveness of marketing campaigns.</li>
            <li>Aggregate usage metrics (such as total number of visitors to the Sites and pages viewed, application usage and downloads, and interaction with advertisements).</li>
            <li>Understand usage trends and preferences.</li>
            <li>Improve our marketing and promotional efforts.</li>
            <li>Provide location targeted offers.</li>
            <li>Provide you with a personalized experience on the Sites (such as recognizing your system so that you will not have to re-enter information during your current or future visit to the Sites).</li>
            <li>Provide you with custom, personalized offer promotions and advertisements on and off our Sites.</li>
            <li>Create new features and functionality.</li>
            <li>For other non-marketing or administrative purposes (such as customer service purposes).</li>
            <li>We may also use the device information to provide targeted ads to you based on your viewing and redemption activity. </li>
        </ul>
    </div>

    <div class="title-this-page">When We Share Information</div>
    <div class="par">
        We can share all our information (personally identifiable and non-personally identifiable) with the Association of the United States Army and its chapters. We have to do this in order to allow every member of participating chapter to access our system and services.
    </div>

    <div class="par">
        We may share information about you if required to do so by law or in the good-faith belief that such action is necessary to comply with local, state, or federal laws or to respond to a court order, judicial or other governmental subpoena or warrant, or in the event of bankruptcy proceedings. We also reserve the right to share information that we believe, in good faith, is appropriate or necessary to take precautions against liability, protect us or our Affiliates from fraudulent, abusive, or unlawful uses, investigate and defend ourselves against any third-party claims or allegations, assist government enforcement agencies, protect the security or integrity of the Sites, and to protect our rights, property, or safety and those of our Affiliates and others. 
    </div>

    <div class="par">
        <b>Personally Identifiable Information:</b> We do not share personally identifiable information with our Affiliates or other third-parties for their marketing or promotional uses except as part of a specific program or feature that you have chosen to participate in. For example, we offer coupons on our Sites that require you to fill out an advertiser survey in order to receive the coupon. If you choose to participate in the survey, then we will share the information you provide with the advertiser who provided the survey. You may be given the option to register with the website of one of our Affiliates, or sign up for an Affiliate’s newsletter. If we manage the website for the Affiliate and you choose to register or sign up with that Affiliate, then we will share the information you provide with that Affiliate. In addition, we may use your personally identifiable information for the purpose of obtaining associated non-personally identifiable information from third parties, but we do not permit those third parties to use your personally identifiable information for any purpose other than providing us with associated non-personally identifiable information.
    </div>

    <div class="par">
        <b>Non-Personally Identifiable Information:</b> We aggregate non-personally identifiable information (such as age, gender, zip code, state, offer print and redemption data, and other automatically collected information that does not personally identify you) and share such aggregated information with our Affiliates in order to help them improve the marketing of their products and services.
    </div>

    <div class="title-this-page">Data Security</div>
    <div class="par">
        The Sites incorporate physical, electronic, and administrative procedures to safeguard the confidentiality of your personal information. Additional security measures, such as Secure Sockets Layer ("SSL") or other technology, will be used if you engage in financial transactions on our Sites. However, as no security measures are perfect or impenetrable, we cannot guarantee the security of any information you transmit to us.
    </div>
    <br />
    <br />
</div>
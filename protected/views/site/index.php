<?php

$this->pageTitle = Yii::app()->name;
$this->layout = '//layouts/full';

cs()->registerScript('live-width-update',
        '$("#pixelWidth").text($(window).width() + "px");
        $(window).resize(function() {
            var show_width = $(window).width();
            $("#pixelWidth").text(show_width + "px");
         });');
?>
<div style="color:#fff; font-size:16px; text-align: center;"><span><code>travis testing: <span id="pixelWidth"></span></code></span></div>

     <div class="ausa-contentMain hidden-phone">
        <div class="ausa-contentMainInner">
            <div class="widget-content no-head-foot" style="padding: 0px;">
                <div class="ausa-salesTeamDashboard">
                    <div id="vendor-mainDashboard" style="height: 306px;">
                        <div class="dashboardHighlight" style="clear: both; overflow: hidden; height: 265px;">
                            <?php $this->widget('application.components.widgets.AOHomepageSlider'); ?>
                            <!-- begin slider -->
                            <div id="ausa_slider">
                                <div class="slider hidden-tablet">
                                    <ul>
                                        <li>
                                            <?php $this->renderPartial('//site/slides/slideOne', array(),false); ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- end slider -->
                        </div>
                    </div>
                    <div class="widget-content dashboard no-foot">
                        <?php $this->widget('application.components.widgets.AONotice', array('notice' => 'AUSA Offers is currently under development.','noticeType' => 'info')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

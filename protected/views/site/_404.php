<?php
$this->pageTitle = '404 - File Not Found';
//$this->widget('application.components.widgets.AO404');
$this->beginWidget('bootstrap.widgets.TbHeroUnit', array('heading' => '404? Oh, well...')); ?>
<p>You have time traveled to a page that doesn't exist now. Adjust your settings to another date and try again.</p>
<div style="text-align: center; margin-top: 20px;">
    <img style ="width: 600px; border: 4px solid black;" src="<?php echo url('images/back-to-the-future.jpg')?>" alt="time machine"/>
</div>

<?php
$this->endWidget();
?>

<?php
$this->pageTitle = Yii::app()->name . ' - System Logs';
$this->layout = '//layouts/full';

$breadcrumbs = array(
    'My Dashboard' => url(app()->user->getHomeUrl()),
    'System Logs'
);


$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));

$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
?>
    <div class="ao-panel-content">
    <?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'logs-form',
    'type' => 'inline',
    'htmlOptions' => array('class' => 'no-foot')));
echo "<span class='form-inline-text'>display </span>";
echo $form->textField($model, 'lineCount', array('class' => 'input-small', 'maxlength' => 6));
echo "<span class='form-inline-text'> lines of </span>";
$this->widget('bootstrap.widgets.TbButtonGroup', array(
    'type' => 'primary',
    'toggle' => 'radio', // 'checkbox' or 'radio'
    'buttons' => array(
        array('label' => 'application.log', 'active' => $model->logname == "application.log" ? true : false),
        array('label' => 'debug.txt', 'active' => $model->logname == "debug.txt" ? true : false),
        array('label' => 'email.txt', 'active' => $model->logname == "email.txt" ? true : false),
    ),
));
$this->endWidget();

$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'logname',
        'filesize',
        'lastModified'
    ),
));
?>    
    <div>
        <?php $lines = $model->read();
        echo nl2br(Chtml::encode(implode("\n", $lines)))?>
    </div>
</div>
<?php 
$this->endWidget();

cs()->registerCss('inline-form', '
.form-inline-text {
padding: 5px 10px 2px 10px;
}
.btn-group {
top: -4px;
}
');

cs()->registerScript('select-log', '
$(".btn-group a.btn").click(function(e){
    e.preventDefault();
    var log = $(this).text();
    var lines = $("#SystemLog_lineCount").val();
    if (isNaN(lines) || lines <= 0){
        lines = 50;
    }
    window.location.href="' . url('admin/logs') . '?logName=" + log + "&lines=" + lines;
});    
');
?>
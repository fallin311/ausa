<?php
$this->pageTitle = Yii::app()->name . ' - System Update';
$this->layout = '//layouts/full';

$breadcrumbs = array(
    'My Dashboard' => url(app()->user->getHomeUrl()),
    'System Update'
);


$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));

$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
$this->widget('application.components.widgets.AOPanelHead', array('panelTitle' => 'Repository Status'));
$status = $model->getHeadRepoStatus();
?>
<div class="ao-panel-content">
    <?php echo nl2br($status); ?>
</div> 
<?php
$this->endWidget();
//</div><div class="ausa-contentmain row-fluid">
?>

<?php
$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
$this->widget('application.components.widgets.AOPanelHead', array('panelTitle' => 'Run Update'));
?>
<div class="ao-panel-content">
    Use this form to update to the latest version from SVN. Make sure this version works on the
    development and staging server first.
</div>    
<?php
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'svn-update-form',
    'htmlOptions' => array('class' => 'no-foot')));

echo $form->errorSummary($model);

echo $form->textAreaRow($model, 'note', array('class' => 'span', 'hint-inline' => 'What did change since the last update?'));

echo $form->textFieldRow($model, 'revision', array('class' => 'input-small', 'hint-inline' => 'Revert / update to this reveision. Keep empty to update to HEAD revision.'));

echo $form->textFieldRow($model, 'updated_file', array('class' => 'span', 'hint-inline' => 'Use this field to update only one file or directory. Use absolute path - for example, <i>protected/controllers/AdminController.php</i>'));
?>

<div class="row">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'htmlOptions' => array('style' => 'margin: 17px 50px; float: left'),
        'size' => 'medium',
        'icon' => 'ok white',
        'label' => 'Beam me up, Scotty!',
    ));
    $this->endWidget();
    ?>

    <?php
    $this->endWidget();

    /* $this->widget('bootstrap.widgets.TbDetailView', array(
      'data' => $model,
      'attributes' => array(
      'logname',
      'filesize',
      'lastModified'
      ),
      )); */
    ?>
</div><div class="ausa-contentmain row-fluid">
    <?php
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
    $this->widget('application.components.widgets.AOPanelHead', array('panelTitle' => 'Update History'));
    $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $dataProvider,
        'columns' => array(
            array('name' => 'updated_on'),
            array('name' => 'revision'),
            array('name' => 'user'),
            array('name' => 'note'),
        //array('name' => 'vendor.update', 'header' => '', 'type' => 'raw', 'value' => 'CHtml::link("<button class=\"btn btn-small\"><i class=\"icon-cog\"></i></button>",url("/vendor/update/" . $data->vendor_id))', 'htmlOptions' => array('width' => '20', 'class' => 'status-center'))
        ),
    ));
    $this->endWidget();
    ?>    

</div>
<?php ?>
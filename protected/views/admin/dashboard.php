<?php

$this->layout = '//layouts/full';
$this->pageTitle = Yii::app()->name;

$breadcrumbs = array(
    'My Dashboard',
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs, 'map' => true));

$this->widget('application.components.widgets.AOGMap', array('markers' => str_replace("\\/", '/', json_encode(Chapter::model()->getVendorBranches()))));
?>   
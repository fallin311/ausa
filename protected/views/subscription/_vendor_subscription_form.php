<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'subscription-form',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => false));


echo $form->errorSummary($model);

$this->endWidget();
?>


<?php

//display these attributes..
$attributes = array('plan_name');
$attributes[] = array('name' => 'plan_description', 'label' => 'Description', 'type' => 'raw', 'value' => $plan->plan_description);
if ($plan->max_offers > 0) {
    $attributes[] = array('name' => 'max_offers', 'label' => 'No. of Offer Slots', 'type' => 'raw', 'value' => $plan->max_offers);
}
if ($plan->max_banners > 0) {
    $attributes[] = array('name' => 'max_banners', 'label' => 'No. of Banners', 'type' => 'raw', 'value' => $plan->max_banners);
}
//$attributes[] = 'max_branches';
$attributes[] = array('name' => 'max_categories', 'label' => 'No. of Categories', 'type' => 'raw', 'value' => $plan->max_categories);

if ($plan->free_duration > 0) {
    $attributes[] = array('name' => 'free_duration', 'label' => 'Free Trial Period', 'type' => 'raw', 'value' => $plan->free_duration . " days");
}

$attributes[] = array('name' => 'monthly_recurring_price', 'label' => 'Monthly Recurring Price', 'type' => 'raw', 'value' => Yii::app()->numberFormatter->formatCurrency($plan->monthly_recurring_price, 'USD'));

if($vendor->getSubscriptionStatus() == Vendor::SUB_NOT_ACTIVE){
    $attributes[] = array('name' => 'setup_fee', 'label' => 'One-time Setup Fee', 'type' => 'raw', 'value' => Yii::app()->numberFormatter->formatCurrency($plan->setup_fee, 'USD'));
}



$attributes[] = array('name' => 'tax', 'label' => 'Tax (8.25%)', 'type' => 'raw', 'value' => Yii::app()->numberFormatter->formatCurrency($model->tax, 'USD'));

$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $plan,
    'attributes' => $attributes,)
);
?>



<?php

$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
$this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons help', 'panelTitle' => 'Subscription Status'));

$subscription = new Subscription();
$subscription->initSubscription($model);

$subStatus = $model->getSubscriptionStatus($subscription);
//display these attributes..
$attributes = array(array('label' => 'Subcsription Status', 'type' => 'raw', 'value' => $model->getColoredStatus($subscription)));

if ($subStatus != Vendor::SUB_NOT_ACTIVE) {
    if ($subStatus != Vendor::SUB_EXPIRED) {
        $attributes[] = array('name' => 'expiration', 'label' => 'Expires', 'type' => 'raw', 'value' => Shared::formatShortUSDate($model->expiration));
    } else {
        $attributes[] = array('name' => 'expiration', 'label' => 'Expired On', 'type' => 'raw', 'value' => Shared::formatShortUSDate($model->expiration));
    }
}

$labelVar = "Pay Now And Activate";
$labelIcon = "icon-shopping-cart white";
$labelType = "success";
$labelURL = '/subscription/update/' . $model->vendor_id;
if ($subStatus == Vendor::SUB_ACTIVE) {
    $labelVar = "My Payment Information";
} else if ($subStatus == Vendor::SUB_EXPIRE_SOON) {
    $labelVar = "Pay Now and Extend";
}

$ccStatus = $subscription->getCCStatus();

if ($ccStatus != Subscription::CARD_ACTIVE && $subscription->paymentType == "cc") {
    if ($ccStatus == Subscription::CARD_EXPIRED) {
        $attributes[] = array('name' => 'paymentDetails', 'label' => 'Payment Details', 'type' => 'raw', 'value' => $subscription->getColoredCCStatus($subscription));
    } else if ($ccStatus == Subscription::CARD_EXPIRE_SOON) {
        $attributes[] = array('name' => 'paymentDetails', 'label' => 'Payment Details', 'type' => 'raw', 'value' => $subscription->getColoredCCStatus($subscription));
    } else {
        $attributes[] = array('name' => 'paymentDetails', 'label' => 'Payment Details', 'type' => 'raw', 'value' => $subscription->getColoredCCStatus($subscription));
    }

    $labelVar = "Update Payment Information";
    $labelIcon = "legal white";
    $labelType = "danger";
    $labelURL = '/subscription/updatebilling/' . $model->vendor_id;
}

$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => $attributes)
);


$this->beginWidget('application.components.widgets.AOPanelFoot');

// make it not too obvious and display it only if the customer is subscribed.
if ($subscription->currentlyRecurring){
    echo CHtml::link("Unsubscribe", url('subscription/unsubscribe'), array('style' => 'float: left; margin: 10px 15px;'));
}

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'icon' => $labelIcon,
    'type' => $labelType,
    'size' => 'small',
    'url' => url($labelURL),
    'label' => $labelVar,
    'htmlOptions' => array('style' => 'margin: 7px 10px 0px 8px; float: right',)
));
$this->endWidget();
$this->endWidget();
?>

<?php

$this->layout = '//layouts/full';

if (app()->user->IsSalesman() || app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => url(app()->user->getHomeUrl()),
        $vendor['vendor_name'] . ' Dashboard' => url('vendor/go/' . $vendor['vendor_id']),
        'Subscription',
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('vendor/dashboard'),
        'Subscription',
    );
}
/*
  if(app()->user->hasFlash('success')) {
  cs()->registerScript('updates','setTimeout(function() { showSuccess(\''.app()->user->getFlash('success').'\'); }, 500);');
  } */


$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
$this->widget('application.components.widgets.AOGrid');
?>

<?php

// vendor didn't pay or choose plan yet
// what if he selected plan, but didn't pay yet?
if (!$vendor->pg_id) {
    $this->renderPartial('_select_plan', array(
        'availablePlans' => $availablePlans,
    ));
} else {
    $this->renderPartial('_subscription_info', array(
        'vendor' => $vendor,
        'model' => $model
    ));
}
?>

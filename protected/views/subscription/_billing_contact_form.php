<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'billing-contact',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => false));


echo $form->errorSummary($model);
//Shared::debug($model);


echo $form->textFieldRow($model, 'firstName', array('class' => 'span'));

echo $form->textFieldRow($model, 'lastName', array('class' => 'span'));

echo $form->textFieldRow($model, 'company', array('class' => 'span'));

echo $form->textFieldRow($model, 'street', array('class' => 'span'));

echo $form->textFieldRow($model, 'street2', array('class' => 'span'));

echo $form->textFieldRow($model, 'city', array('class' => 'span'));

echo $form->textFieldRow($model, 'state', array('class' => 'span'));

echo $form->textFieldRow($model, 'zip', array('class' => 'span'));

echo $form->textFieldRow($model, 'country', array('class' => 'span'));

echo $form->textFieldRow($model, 'phone', array('class' => 'span'));

echo $form->textFieldRow($model, 'email', array('class' => 'span'));


$this->endWidget();
?>

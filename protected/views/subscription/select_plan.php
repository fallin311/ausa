<?php

$this->layout = '//layouts/full';

if (app()->user->IsSalesman() || app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        $vendor['vendor_name'] . ' Dashboard' => url('vendor/go/' . $vendor['vendor_id']),
        'Select A Plan',
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('vendor/dashboard'),
        'Select A Plan',
    );
}
/*
  if(app()->user->hasFlash('success')) {
  cs()->registerScript('updates','setTimeout(function() { showSuccess(\''.app()->user->getFlash('success').'\'); }, 500);');
  } */


$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
$this->widget('application.components.widgets.AOGrid');
?>

<div id="ausa-contentMainTiles">
    <ul id="tiles">
        <?php
        if ($availablePlans) {
            foreach ($availablePlans as &$planObj) {
                ?>
                <li class="grid">
                    <?php
                    if (Plan::model()->getPopularPlan() == $planObj->plan_id) {
                        $popularPlan = true;
                    } else {
                        $popularPlan = false;
                    }
                    $this->widget('application.components.widgets.AOPriceTable', array(
                        'planArray' => $planObj,
                        'planName' => $planObj->plan_name,
                        'planPrice' => $planObj->monthly_recurring_price,
                        'planTag' => $popularPlan,
                        'planLink' => url('/subscription/plan/?id=' . $vendor->vendor_id . '&p=' . $planObj->plan_id),
                    ));
                    ?>
                </li>
                <?php
            }
        }
        ?>
    </ul>
</div>
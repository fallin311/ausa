<?php
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'contact-info',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => false));

if ($model->firstName && $model->lastName)
    echo $form->customRow($model, 'name', 'Name', '<div style="margin-bottom:10px;">' . $model->firstName . ' ' . $model->lastName . '</div>');
if ($model->company)
    echo $form->customRow($model, 'company', 'Company', '<div style="margin-bottom:10px;">' . $model->company . '</div>');
echo $form->customRow($model, 'bill_add', 'Billing Address', '<div>' . $model->street . ($model->street2 ? ', ' . $model->street2 : '') . '</div>'
        . '<div' . ($model->country ? '' : ' style="margin-bottom:10px;"') . '>' . $model->city . ($model->state ? ', ' . $model->state : '') . ($model->zip ? ' &nbsp;' . $model->zip : '') . '</div>'
        . ($model->country ? '<div style="margin-bottom:10px;">' . $model->country . '</div>' : '')
);
if ($model->phone)
    echo $form->customRow($model, 'phone', 'Phone', '<div style="margin-bottom:10px;">' . $model->phone . '</div>');
if ($model->email)
    echo $form->customRow($model, 'email', 'Email', '<div style="margin-bottom:10px;">' . $model->email . '</div>', array('style'=>'border:0px;'));

$this->endWidget();
?>
<?php
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'billing-form',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => false));

echo $form->errorSummary($model);
//Shared::debug($model);


$button = $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'type' => 'default',
    'toggle' => 'radio',
    'buttons' => array(
        array('label' => 'Credit Card', 'active' => $model->paymentType == 'cc' ? true : false, 'htmlOptions' => array('id' => 'cc-button')),
        array('label' => 'ACH / E-Check', 'active' => $model->paymentType == 'check' ? true : false, 'htmlOptions' => array('id' => 'ach-button')),),
    'htmlOptions'=>array('style'=>'text-align:center; margin-bottom:10px;'),
), true);
echo $form->customRow($model, 'paymentType', 'Payment Type', $button, array('hint' => 'Please choose your method of payment.'));
echo CHtml::activeHiddenField($model, 'paymentType');

cs()->registerScript('payment-type', '
            
            $("#cc-button").click(function(){
                if (!$(this).hasClass("active")){
                    $("#Subscription_paymentType").val("cc");
                    $("#ach-fields").hide();
                    $("#cc-fields").show();
                }
            });
            $("#ach-button").click(function(){
                if (!$(this).hasClass("active")){
                    $("#Subscription_paymentType").val("check");
                    $("#cc-fields").hide();
                    $("#ach-fields").show();
                }
            });
            $("form#remove").remove();
        ');

 //preselect the payment type button on page load
if (($model->paymentType == "cc") || (!$model->paymentType)) {
    cs()->registerScript('payment-type-toggle', '
            
            $("#cc-button").button("toggle");
            $("#ach-fields").hide();
            $("#cc-fields").show();
            
        ');
} elseif ($model->paymentType == "check") {
    cs()->registerScript('payment-type-toggle', '
            
            $("#ach-button").button("toggle");
            $("#cc-fields").hide();
            $("#ach-fields").show();
            
        ');
}
?>

<div id="cc-fields">
    <?php
    //echo $form->dropDownListRow($model, 'cardType', $model->getPaymentTypes());

    echo $form->textFieldRow($model, 'cardNumber', array('class' => 'span', 'maxlength' => 16));

    echo $form->textFieldRow($model, 'cardCode', array('class' => 'span', 'maxlength' => 4));
    
    $expMonthInput = $form->dropDownList($model, 'cardExpMonth', Plan::$months, array('style' => 'width:40%; margin-right:20px;'));
    $expYearInput = $form->dropDownList($model, 'cardExpYear', Plan::getExpYearArray(11), array('style' => 'width:25%;'));

    //$expMonthInput = $this->widget('CActiveForm',array('id'=>'remove'))->dropDownList($model,'cardExpMonth',Plan::$months,array('style'=>'width:40%; margin-right:20px;'));
    //$expYearInput = $this->widget('CActiveForm',array('id'=>'remove'))->dropDownList($model,'cardExpYear',Plan::getExpYearArray(11),array('style'=>'width:25%;'));
    
    echo $form->customRow($model,'exp_date','Expiration Date',$expMonthInput.$expYearInput, array('style'=>'border:0px;'));
    
    ?>

</div>

<div id="ach-fields" class="no-foot" style="background-color: #FFF;">

    <?php
    echo $form->textFieldRow($model, 'achAccount', array('class' => 'span', 'maxlength' => 10));

    echo $form->textFieldRow($model, 'achRouting', array('class' => 'span', 'maxlength' => 10));

    ?>
</div>
<?php $this->endWidget(); ?>

<?php
$this->layout = '//layouts/full';

Yii::import('ext.simpleModal.ModalWidget');
ModalWidget::loadAssets();

//cs()->registerCSS('form-bgcolor','form {background-color: #FFF; }');
cs()->registerScript('billing-form-scripts', '
    $("#billing_contact_form").hide();
    $("#billing_contact").show();
    $("#edit-billing").click(function(e){
        e.preventDefault();
        $("#billing_contact").hide();
        $("#billing_contact_form").show();
    });
            
    $("#edit-payment").click(function(){
        $("#payment_info").hide();
        $("#payment_form").show();
    });');
?>
<?php
if (app()->user->IsSalesman() || app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        $vendor['vendor_name'] . ' Dashboard' => url('vendor/go/' . $vendor['vendor_id']),
        'Update Subscription',
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('vendor/dashboard'),
        'Update Subscription',
    );
}

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
?>
<!--<div class="ausa-contentMainInner">
    <div class="widget-content no-head-foot" style="padding: 0px;">
        <div class="ausa-salesTeamDashboard">
            <div class="header">
                <img class="img-rounded dashboard" src="<?php //echo url('/image/display/' . $vendor->logo) . '?w=' . Image::SIZE_FULL_W . '&h=' . Image::SIZE_FULL_H  ?>" alt="<?php //echo $vendor->vendor_name;  ?>" />
                <h1>
                    <span style="display: block; line-height: 35px;"><?php //echo $vendor->vendor_name;  ?></span>
                    <span style="display: block; margin-top: -2px; line-height: 17px; font-size: 17px; font-weight: normal; color: #646464; letter-spacing: 5px; text-indent: 4px;">
<?php //echo strip_tags($vendor->vendor_description); ?>
                    </span>
                </h1>
            </div>
        </div>
    </div>
</div>-->


<?php
if ($model->currentlyRecurring) {
    cs()->registerScript('payment-form-scripts', '
         $("#payment_form").hide(); 
         $("#payment_info").show();
    ');
} else {
    cs()->registerScript('payment-form-scripts', '
         $("#payment_form").show(); 
         $("#payment_info").hide();
    ');
}
/*
  $this->beginWidget('application.components.widgets.AOStickyBar', array('sticky' => true, 'panelWrap' => true));
  $this->widget('bootstrap.widgets.TbButton', array(
  'buttonType' => 'link',
  'icon' => 'remove white',
  'type' => 'danger',
  'url' => url('/vendor/dashboard'),
  'label' => 'Cancel',
  'htmlOptions' => array(
  'class' => 'pull-right',
  'style' => 'margin: 5px 25px 0px 8px',
  ),
  ));

  $this->widget('bootstrap.widgets.TbButton', array(
  'buttonType' => 'ajaxSubmit',
  'type' => 'success',
  'url' => url('/subscription/update/' . $vendor->vendor_id),
  'htmlOptions' => array(
  'class' => 'pull-right',
  'style' => 'margin: 5px 8px',
  'id' => 'save-vendor-button',
  ),
  'loadingText' => 'processing...',
  'ajaxOptions' => array(
  'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
  'beforeSend' => 'function(){$("#save-vendor-button").button("loading")}',
  'success' => 'function(response){
  $("#save-vendor-button").button("reset");
  if (response.length < 5){
  window.location.href="' . url('subscription/index/' . $vendor->vendor_id) . '";
  }else{
  showError(\'Could not save the subscription settings. Please check the error fields below.\');
  $("#ajax-errors-holder").show();
  $("#ajax-errors").html(response);
  $(".notification").css({\'display\':\'block\', \'opacity\':\'1\'});
  }
  }',
  ),
  'icon' => 'ok white',
  'label' => 'Save',
  ));
  $this->endWidget(); */
?>

<div style="width: 100%; margin-bottom: 25px; position: relative; float: left; display: none;" id="ajax-errors-holder">
    <?php $this->widget('application.components.widgets.AONotice', array('notice' => '<div id="ajax-errors"></div>', 'noticeType' => 'errors')); ?>
</div>


<?php
// Subscription information
$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
$this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Subscription Details'));
if (app()->user->isAdmin() || app()->user->isSalesman()) {
    $this->renderPartial('_subscription_form', array('model' => $model, 'vendor' => $vendor));
} else {
    $this->renderPartial('_vendor_subscription_form', array('model' => $model, 'vendor' => $vendor, 'plan' => $plan));
}

$this->widget('application.components.widgets.AOPanelFoot');
$this->endWidget();
?>

<div id="payment_form">
    <?php
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
    $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Payment Information'));
    echo $this->renderPartial('_billing_form', array('model' => $model));

    $this->beginWidget('application.components.widgets.AOPanelFoot');
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'ajaxButton',
        'url' => url('/subscription/update/' . $vendor->vendor_id),
        'icon' => 'icon-shopping-cart white',
        'type' => 'success',
        'size' => 'small',
        'label' => ' Pay Now',
        'htmlOptions' => array(
            'style' => 'margin: 7px 15px 0px 8px; float: right',
            'id' => 'pay-now-button',),
        'loadingText' => 'processing...',
        'ajaxOptions' => array(
            'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
            'beforeSend' => 'function(){$("#pay-now-button").button("loading")}',
            'type' => 'post',
            'success' => 'function(response){
                $("#pay-now-button").button("reset");
                if (response.length < 5){
                    window.location.href="' . url('subscription/index/' . $vendor->vendor_id) . '";
                }else{
                    showError(\'Could not process payment. Please check the error fields below.\');
                    $("#ajax-errors-holder").show();
                    $("#ajax-errors").html(response);
                    $(".notification").css({\'display\':\'block\', \'opacity\':\'1\'});
                }
            }',
        ),
    ));
    $this->endWidget();
    $this->endWidget();
    ?>
</div>

<div id="payment_info">
    <?php
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Edit Payment Information'));
    ?>
    <div class="widget-control pull-right">
        <a id="edit-payment" class="btn dropdown-toggle" href="#">
            <i class="icon-pencil"></i> Edit 
        </a>
    </div>
    <?php
    $this->endWidget();
    echo $this->renderPartial('_billing_info', array('model' => $model));
    $this->widget('application.components.widgets.AOPanelFoot');
    $this->endWidget();
    ?>
</div>

<div id="billing_contact">
    <?php
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Billing Contact Information'));
    ?>
    <div class="widget-control pull-right">
        <a id="edit-billing" class="btn dropdown-toggle" href="#">
            <i class="icon-pencil"></i> Edit
        </a>
    </div>
    <?php
    $this->endWidget();
    echo $this->renderPartial('_billing_contact', array('model' => $model));
    $this->widget('application.components.widgets.AOPanelFoot');
    $this->endWidget();
    ?>
</div>

<div id="billing_contact_form">
    <?php
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
    $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Edit Billing Contact Information'));
    echo $this->renderPartial('_billing_contact_form', array('model' => $model));
    $this->widget('application.components.widgets.AOPanelFoot');
    $this->endWidget();
    ?>
</div>

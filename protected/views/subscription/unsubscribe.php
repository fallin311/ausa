<?php
$this->layout = '//layouts/full';

if (app()->user->IsSalesman() || app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        $model->vendor_name . ' Dashboard' => url('vendor/go/' . $model->vendor_id),
        'Cancel Recurring Payment',
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('vendor/dashboard'),
        'Cancel Recurring Payment',
    );
}
$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));

$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
$this->widget('application.components.widgets.AOPanelHead', array('panelTitle' => 'Cancel Recurring Payment'));
?>
<div class="ao-panel-content">
    <h1>Why do you leave us?</h1>
    <p>
        Are you sure you want to cancel the monthly payment for AUSA offers service? 
        Is there something wrong with the service? Please <a href="<?php echo url('vendor/support') ?>">let us know</a>.
    </p>
    <p>
        If you are still determined to cancel the service, click the button below. Your account will remain active until expiration
        date, but your credit card / bank account will not be charged again.
    </p>
    <div class="row">
        <div class="span10 wrapper offset1">
            <?php
            $form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
                'id' => 'cancel-subscription'
                    ));
            echo CHtml::hiddenField('unsubscribe', 1);

            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'link',
                'icon' => 'icon-arrow-left white',
                'type' => 'success',
                'url' => url('/vendor/dashboard/'),
                'label' => 'Not Yet!',
                'size' => 'large',
                'htmlOptions' => array('style' => 'margin: 8px', 'class' => 'pull-left')
            ));

            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'icon' => 'icon-remove white',
                'type' => 'danger',
                'size' => 'large',
                'label' => 'Cancel my Montlhly Payment Now.',
                'htmlOptions' => array('style' => 'margin: 8px', 'class' => 'pull-right')
            ));
            $this->endWidget();
            ?>
        </div>
    </div>
    <div style="margin-left: auto; margin-right: auto; border: 8px solid black; margin-top: 20px; margin-bottom: 20px; width: 400px; height:356px;">
        <img src="<?php echo url('images/sad_cat.jpg') ?>" alt="stay" />
    </div>
</div>


<?php $this->endWidget(); ?>

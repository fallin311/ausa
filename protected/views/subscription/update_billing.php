<?php
$this->layout = '//layouts/full';

Yii::import('ext.simpleModal.ModalWidget');
ModalWidget::loadAssets();

cs()->registerScript('billing-form-scripts', '
    $("#billing_contact_form").hide();
    $("#billing_contact").show();
    $("#edit-billing").click(function(e){
        e.preventDefault();
        $("#billing_contact").hide();
        $("#billing_contact_form").show();
    });
            
    $("#payment_form").hide(); 
         $("#payment_info").show();
    $("#edit-payment").click(function(){    
        $("#payment_info").hide();
        $("#payment_form").show();
    });');
?>

<?php
if (app()->user->IsSalesman() || app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        $vendor['vendor_name'] . ' Dashboard' => url('vendor/go/' . $vendor['vendor_id']),
        'Update Subscription',
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('vendor/dashboard'),
        'Update Subscription',
    );
}

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
?>

<?php
$this->beginWidget('application.components.widgets.AOStickyBar', array('sticky' => true, 'panelWrap' => true));
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'icon' => 'remove white',
    'type' => 'danger',
    'url' => url('/vendor/dashboard'),
    'label' => 'Cancel',
    'htmlOptions' => array(
        'class' => 'pull-right',
        'style' => 'margin: 5px 25px 0px 8px',
    ),
));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'ajaxSubmit',
    'type' => 'success',
    'url' => url('/subscription/updatebilling/' . $vendor->vendor_id),
    'htmlOptions' => array(
        'class' => 'pull-right',
        'style' => 'margin: 5px 8px',
        'id' => 'save-vendor-button',
    ),
    'loadingText' => 'processing...',
    'ajaxOptions' => array(
        'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
        'beforeSend' => 'function(){$("#save-vendor-button").button("loading")}',
        'success' => 'function(response){
                    $("#save-vendor-button").button("reset");
                    if (response.length < 5){
                        window.location.href="' . url('vendor/dashboard') . '";
                    }else{
                        showError(\'Could not save payment information. Please check the error fields below.\');
                        $("#ajax-errors-holder").show();
                        $("#ajax-errors").html(response);
                        $(".notification").css({\'display\':\'block\', \'opacity\':\'1\'});
                    }
                }',
    ),
    'icon' => 'ok white',
    'label' => 'Save',
));
$this->endWidget();
?>

<!--<div style="width: 100%; margin-bottom: 25px; position: relative; float: left; display: none;" id="ajax-errors-holder">
    <?php //$this->widget('application.components.widgets.AONotice', array('notice' => '<div id="ajax-errors"></div>', 'noticeType' => 'errors')); ?>
</div>-->

<div class="ausa-contentMain row-fluid">
    <div id="ausa-contentMainTiles">
        <ul id="tiles">

            <li class="grid">
                <div id="payment_form">
                    <?php
                    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                    $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Payment Information'));
                    echo $this->renderPartial('_billing_form', array('model' => $model));
                    $this->widget('application.components.widgets.AOPanelFoot');
                    $this->endWidget();
                    ?>
                </div>
            </li>
            <li class="grid">
                <div id="payment_info">
                    <?php
                    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Edit Payment Information'));
                    ?>
                    <div class="widget-control pull-right">
                        <a id="edit-payment" class="btn dropdown-toggle" href="#">
                            <i class="icon-pencil"></i> Edit 
                        </a>
                    </div>
                    <?php
                    $this->endWidget();
                    echo $this->renderPartial('_billing_info', array('model' => $model));
                    $this->widget('application.components.widgets.AOPanelFoot');
                    $this->endWidget();
                    ?>
                </div>
            </li>
            <li class="grid">
                <div id="billing_contact">
                    <?php
                    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Billing Contact Information'));
                    ?>
                    <div class="widget-control pull-right">
                        <a id="edit-billing" class="btn dropdown-toggle" href="#">
                            <i class="icon-pencil"></i> Edit
                        </a>
                    </div>
                    <?php
                    $this->endWidget();
                    echo $this->renderPartial('_billing_contact', array('model' => $model));
                    $this->widget('application.components.widgets.AOPanelFoot');
                    $this->endWidget();
                    ?>
                </div>
            </li>
            <li class="grid">
                <div id="billing_contact_form">
                    <?php
                    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                    $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Edit Billing Contact Information'));
                    echo $this->renderPartial('_billing_contact_form', array('model' => $model));
                    $this->widget('application.components.widgets.AOPanelFoot');
                    $this->endWidget();
                    ?>
                </div>
            </li>
        </ul>
    </div>
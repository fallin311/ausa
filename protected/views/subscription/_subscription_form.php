<?php
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'subscription-form',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => false));


echo $form->errorSummary($model);

if (!$model->currentlyRecurring) {
    $button = $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type' => 'default',
        'toggle' => 'radio',
        'buttons' => array(
            array('label' => 'Recurring Payments', 'active' => $model->paymentMode == 'recurring' ? true : false, 'htmlOptions' => array('id' => 'recurring-button')),
            array('label' => 'Single Payment', 'active' => $model->paymentMode == 'single' ? true : false, 'htmlOptions' => array('id' => 'single-button')),),
        'htmlOptions' => array('style' => 'text-align:center; margin-bottom:10px;'),
            ), true);
    echo $form->customRow($model, 'paymentMode', 'Payment Mode', $button, array('hint' => 'Please choose your mode of payment.'));
    echo CHtml::activeHiddenField($model, 'paymentMode');

    cs()->registerScript('payment-mode',
            '$("#recurring-button").click(function(){
                if (!$(this).hasClass("active")){
                    $("#Subscription_paymentMode").val("recurring");
                    $("#recurring-fields").show();
                    $("#Subscription_paymentAmount").val($("#Subscription_monthlyRecurringPrice").val());
                    $("#Subscription_tax").val(($("#Subscription_monthlyRecurringPrice").val() * 0.0825).toFixed(2));
                    

                    var noOfBanners = $("#Subscription_maxBanners").val();
                if(noOfBanners > 0){
                    $("#Subscription_subscriptionNotes").val("Monthly automatic withdrawal of $"
                    + $("#Subscription_paymentAmount").val() +
                    " + Tax for "
                    + $("#Subscription_maxBanners").val() +
                    " banner slot(s) and "
                    + $("#Subscription_maxOffers").val() +
                    " offer slot(s)."
                     );
                }else{
                    $("#Subscription_subscriptionNotes").val("Monthly automatic withdrawal of $"
                    + $("#Subscription_paymentAmount").val() +
                    " for "
                    + $("#Subscription_maxOffers").val() +
                    " offer slot(s)."
                     );
                }
            }});
            
            $("#single-button").click(function(){
                if (!$(this).hasClass("active")){
                    $("#Subscription_paymentMode").val("single");
                    $("#recurring-fields").hide();
                    $("#Subscription_paymentAmount").val($("#Subscription_singlePaymentPrice").val());
                    $("#Subscription_tax").val(($("#Subscription_singlePaymentPrice").val() * 0.0825).toFixed(2));
                    


                    var noOfBanners = $("#Subscription_maxBanners").val();
                if(noOfBanners > 0){
                    $("#Subscription_subscriptionNotes").val("Single Payment of $"
                    + $("#Subscription_paymentAmount").val() +
                    " + Tax for "
                    + $("#Subscription_maxBanners").val() +
                    " banner slot(s) and "
                    + $("#Subscription_maxOffers").val() +
                    " offer slot(s)."
                     );
                }else{
                    $("#Subscription_subscriptionNotes").val("Single Payment of $"
                    + $("#Subscription_paymentAmount").val() +
                    " for "
                    + $("#Subscription_maxOffers").val() +
                    " offer slot(s)."
                     );
                }
                

                }
            });
            
            
        ');

///Preselect the payment Mode button
    if (($model->paymentMode == "recurring") || (!$model->paymentMode)) {
        cs()->registerScript('payment-mode-toggle', '$("#recurring-button").button("toggle");
                $("#recurring-fields").show();');
    } elseif ($model->paymentMode == "single") {
        cs()->registerScript('payment-mode-toggle', '$("#single-button").button("toggle");
            $("#recurring-fields").hide();');
    }
} else {
    echo $form->textFieldRow($model, 'paymentMode', array('class' => 'span', 'disabled' => 'disabled'));
}


cs()->registerScript('subscription-notes', '
            
            $("#Subscription_maxOffers, #Subscription_maxBanners, #Subscription_paymentAmount").keyup(function(){
                
                var noOfBanners = $("#Subscription_maxBanners").val();
                if((noOfBanners > 0) && ($("#Subscription_paymentMode").val() == "recurring")){
                    $("#Subscription_subscriptionNotes").val("Monthly automatic withdrawal of $"
                    + $("#Subscription_paymentAmount").val() +
                    " + Tax for "
                    + $("#Subscription_maxBanners").val() +
                    " banner slot(s) and "
                    + $("#Subscription_maxOffers").val() +
                    " offer slot(s)."
                     );
                }else if((noOfBanners > 0) && ($("#Subscription_paymentMode").val() == "single")){
                    $("#Subscription_subscriptionNotes").val("Single Payment of $"
                    + $("#Subscription_paymentAmount").val() +
                    " + Tax for "
                    + $("#Subscription_maxBanners").val() +
                    " banner slot(s) and "
                    + $("#Subscription_maxOffers").val() +
                    " offer slot(s)."
                     );
                }else if($("#Subscription_paymentMode").val() == "single"){
                    $("#Subscription_subscriptionNotes").val("Single Payment of $"
                    + $("#Subscription_paymentAmount").val() +
                    " for "
                    + $("#Subscription_maxOffers").val() +
                    " offer slot(s)."
                     );
                }else{
                    $("#Subscription_subscriptionNotes").val("Monthly automatic withdrawal of $"
                    + $("#Subscription_paymentAmount").val() +
                    " for "
                    + $("#Subscription_maxOffers").val() +
                    " offer slot(s)."
                     );
                }
            });
            
        ');

if ($vendor->plan->max_banners > 0) {
    echo $form->textFieldRow($model, 'maxBanners', array('class' => 'span', 'maxlength' => 1));
}
echo $form->textFieldRow($model, 'maxOffers', array('class' => 'span', 'maxlength' => 1));

cs()->registerScript('tax-calculation', '
            $("#Subscription_paymentAmount").keyup(function(){
                $("#Subscription_tax").text(($("#Subscription_paymentAmount").val() * 0.0825).toFixed(2));
            });
            
        ');

if ((app()->user->isAdmin()) || (app()->user->isSalesman())) {
    echo $form->textFieldRow($model, 'paymentAmount', array('class' => 'span', 'style' => 'width:30%;'));
} else {
    echo $form->textFieldRow($model, 'paymentAmount', array('class' => 'span', 'disabled' => 'disabled', 'style' => 'width:30%;'));
}
//echo $form->textFieldRow($model, 'tax', array('class' => 'span', 'disabled' => 'disabled', 'style' => 'width:30%;'));
echo $form->customRow($model, 'tax', "Tax", '<div id="Subscription_tax" style="margin-bottom:10px;">' . $model->tax . '</div>');
echo CHtml::activeHiddenField($model, 'singlePaymentPrice');
echo CHtml::activeHiddenField($model, 'monthlyRecurringPrice');
?>
<div id="recurring-fields">
    <?php
//echo $form->dropDownListRow($model, 'recurringSchedule', $model->recurringSchedule);
    //echo $form->textFieldRow($model, 'recurringSchedule', array('class' => 'span', 'disabled' => 'disabled'));
    echo $form->customRow($model, 'recurringSchedule', "Payment Schedule", '<div style="margin-bottom:10px;">Monthly</div>');
    if (!$model->currentlyRecurring) {
        echo $form->datepickerRow($model, 'firstPaymentDate', array('class' => 'span'));
    }
    ?>
</div>
<?php
if (!$model->currentlyRecurring) {
//    if ((int) $vendor->plan->offer_duration >= (int) $vendor->plan->banner_duration) {
//        echo $form->customRow($model, 'numLeft', "Duration", '<div style="margin-bottom:10px;">' . $vendor->plan->offer_duration . ' Months</div>');
//        //$model->numLeft = $vendor->plan->offer_duration;
//    } else {
//        echo $form->customRow($model, 'numLeft', "Duration", '<div style="margin-bottom:10px;">' . $vendor->plan->banner_duration . ' Months</div>');
//        //$model->numLeft = $vendor->plan->banner_duration;
//    }
    //echo $form->customRow($model, 'numLeft', 'Duration (months)', $model->numLeft);

    echo $form->customRow($model, 'numLeft', "Duration", '<div style="margin-bottom:10px;">' . $model->numLeft . ' Months</div>');
    echo CHtml::activeHiddenField($model, 'numLeft');
} else {
    if ($model->numLeft != -1) {
        echo $form->customRow($model, 'numLeft', "Months Left", '<div style="margin-bottom:10px;">' . $model->numLeft . ' Months</div>');
    }
}



//if ((app()->user->isAdmin()) || (app()->user->isSalesman())) {
//echo $form->textFieldRow($model, 'discountPercentage', array('class' => 'span', 'maxlength' => 2));
/*
 $discSlider = $this->widget('zii.widgets.jui.CJuiSlider', array(
     'value' => 0,
     'id' => 'discountSlider',
     'options' => array(
         'min' => 0,
         'max' => 30,
         'range' => 'min',
         'slide' => 'js:function(event, ui) {
             $("#percent-display").text(ui.value + "%");
             $("#Subscription_discountPercentage").val(ui.value);
         }',
     ),
     'htmlOptions' => array(
         'class' => 'progress progress-warning ausaProgress',
     ),
 ), true);
 $discSlider .= '<div id="percent-display" style="float:right; margin-top:-30px; margin-right:20px; font-size:14px;"></div>';
 
 echo $form->customRow($model, 'discount', 'Apply a Discount', $discSlider, array('hint' => 'Drag the slider to apply a discount percentage of up to 30%.'));
 
 echo '<input type="hidden" id="Subscription_discountPercentage" />';
//}
//echo $form->datepickerRow($model, 'nextPaymentDate', array('class' => 'span'));
//echo $form->datepickerRow($model, 'contractExpiration', array('class' => 'span'));
*/
echo $form->textAreaRow($model, 'subscriptionNotes', array('class' => 'span', 'rows' => 4, 'maxlength' => 1000));

$this->endWidget();
?>

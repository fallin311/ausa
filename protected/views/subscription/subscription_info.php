<?php
$this->layout = '//layouts/full';

if (app()->user->IsSalesman() || app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        $vendor['vendor_name'] . ' Dashboard' => url('vendor/go/' . $vendor['vendor_id']),
        'Subscription',
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('vendor/dashboard'),
        'Subscription',
    );
}
/*
  if(app()->user->hasFlash('success')) {
  cs()->registerScript('updates','setTimeout(function() { showSuccess(\''.app()->user->getFlash('success').'\'); }, 500);');
  } */


$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
$this->widget('application.components.widgets.AOGrid');
?>




<div id="ausa-contentMainTiles">
    <ul id="tiles">
        <li class="grid">
            <?php
            $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
            $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Your Plan'));
            ?>
            
            <?php
            //display these attributes..
            $attributes = array('plan_name', 'plan_description');
            if ($plan->max_offers > 0) {
                $attributes[] = 'max_offers';
            }
            if ($plan->max_banners > 0) {
                $attributes[] = 'max_banners';
            }
            $attributes[] = 'max_branches';
            $attributes[] = 'max_categories';

            $this->widget('bootstrap.widgets.TbDetailView', array(
                'data' => $plan,
                'attributes' => $attributes,)
            );
            ?>

            <?php $this->endWidget(); ?>
        </li>

        <li class="grid">
            <?php
            $this->renderPartial('_subscription_status', array('model'=>$vendor));
            ?>
        </li>
    </ul>
</div>

<?php
//$history = $model->getBillingHistory($vendor->pg_id);
$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
$this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons price_tags', 'panelTitle' => 'Billing History'));

$transactions = $model->getTransactions($vendor->pg_id);

if (isset($transactions[0])) {

    $gridColumns = array(
        array('name' => 'transaction_id', 'header' => 'Invoice #', 'htmlOptions' => array('class' => 'status-center')),
        array('name' => 'created', 'header' => 'Account Holder', 'htmlOptions' => array('class' => 'status-center')),
        array('name' => 'ps_charge_id', 'header' => 'Account Payment Type', 'htmlOptions' => array('class' => 'status-center')),
        array('name' => 'start_date', 'header' => 'Process Date', 'htmlOptions' => array('class' => 'status-center')),
        array('name' => 'description', 'type' => 'raw', 'header' => 'Payment Details', 'htmlOptions' => array('class' => 'status-center')),
    );

    $gridDataProvider = new CArrayDataProvider(array_reverse($transactions));
    $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $gridDataProvider,
        'columns' => $gridColumns,
        'enablePagination' => true,
    ));
    //if (count($history->Transactions) <= 10) {
    //$this->widget('application.components.widgets.AOPanelFoot');
    // }
} else {
    echo '<div class="items list table-bordered">';
    echo '<div class="widget-content dashboard no-head-foot">';
    echo "There is currently no billing history available.";
    echo '</div></div>';
}
/* if(count($history->Transactions)<=10){
  $this->widget('application.components.widgets.AOPanelFoot');
  } */
$this->endWidget();
?>
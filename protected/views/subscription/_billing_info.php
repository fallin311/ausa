<?php
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'billing-info',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => false));

if ($model->paymentType == "cc") {
    echo $form->customRow($model,'credit_card','Payment Type','<div class="cc-payment-type" style="margin-bottom:10px;"><span>Credit Card</span></div>');
    echo $form->customRow($model,'credit_card_num','Credit Card Number','<div style="margin-bottom:10px;">'.$model->cardNumber.'</div>');
    echo $form->customRow($model,'credit_card_exp','Expiration Date','<div style="margin-bottom:10px;">'.$model->cardExpMonth.'-'.$model->cardExpYear.'</div>', array('style'=>'border:0px;'));
} elseif($model->paymentType == "check") {
    echo $form->customRow($model,'check','Payment Type','<div style="margin-bottom:10px;">ACH / E-Check</div>');
    echo $form->customRow($model,'check_acc_num','Bank Account Number','<div style="margin-bottom:10px;">'.$model->achAccount.'</div>');
    echo $form->customRow($model,'check_rt_num','Routing Number','<div style="margin-bottom:10px;">'.$model->achRouting.'</div>', array('style'=>'border:0px;'));
}
$this->endWidget();
?>

<?php

$this->layout = '//layouts/full';
$breadcrumbs = array(
    'My Dashboard' => app()->user->getHomeUrl(),
    'Categories',
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
?>

    <?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false)); ?>
    <?php $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Offer Categories')); ?>
    <?php
    $this->widget('ausa.widgets.AOButtonGroup', array(
        'type' => '',
        'buttons' => array(
            array('icon' => 'cog', 'items' => array(
                    array('label' => 'Add Category', 'icon' => 'plus', 'url' => url('/category/create')),
                    '---',
                    array('label' => 'Get Help', 'icon' => 'question-sign', 'url' => '#'),
            )),
        ),
    ));

    cs()->registerCss('category-table', '
.category-preview-icon {
    display: inline-block;
}
.category-description {
    display: inline-block;
    padding-left: 15px;
}
');

    $this->endWidget();

    $dataArray = array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $dataProvider,
        'columns' => array(
            array('name' => 'category_name',
                'header' => 'Name',
                'type' => 'raw',
                'value' => '"<img class=\"category-preview-icon\" src=\"" . $data->getImageUrl(80) . "\" alt=\"preview\" />
<div class=\"category-description\">
<h5 " . ($data->published ? "" : "class=\"ausa-unpublished\"") . ">" . $data->category_name . "</h5>
" . $data->category_description . "
</div>
"'),
            //array('name' => 'published', 'type' => 'raw', 'value' => '$data->published ? "yes" : "<span class=\"ausa-unpublished\">no</span>"'),
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{update}',
            ),
        ),
    );
    //Shared::debug($dataArray);
    $this->widget('bootstrap.widgets.TbGridView', $dataArray);
    ?>

    <?php $this->endWidget(); ?>
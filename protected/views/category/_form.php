<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'category-form',
    //'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => false,
        ));

cs()->registerScript('clear-loader',
    '$(".img-preview-loading-mask").find("img").load(function(){
         $(".img-preview-loading-mask").css("background-image", "none");
     });');

echo $form->errorSummary($model);

echo $form->textFieldRow($model, 'category_name', array('class' => 'span', 'maxlength' => 128));

echo $form->textFieldRow($model, 'category_description', array('class' => 'span', 'maxlength' => 255));

echo $form->textFieldRow($model, 'marker', array('class' => 'span', 'maxlength' => 16, 'hint' => 'Name of the icon stored inside the phone application (ie. restaurant.png)'));

echo $form->checkBoxRow($model, 'published', array('hint' => 'Only published categories are visible.'));

echo $form->imageFieldRow($model, 'category_icon', array('class' => 'span', 'imageOptions' => array(
        'configuration' => 'logo',
        'layout' => 'logo-layout'
        )));

$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->widget('bootstrap.widgets.TbButton', array(
  'buttonType' => 'submit',
  'type' => 'success',
  'htmlOptions' => array('style' => 'margin: 7px 5px; float: right'),
  'size' => 'small',
  'icon' => 'ok white',
  'label' => $model->isNewRecord ? 'Create' : 'Update',
  ));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'type' => '',
    'url' => url('/category'),
    'htmlOptions' => array('style' => 'margin: 7px 5px; float: left'),
    'size' => 'small',
    'icon' => 'remove',
    'label' => 'Cancel',
));

$this->endWidget();

$this->endWidget();
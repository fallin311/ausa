<?php

$this->layout = '//layouts/full';
$breadcrumbs = array(
    'My Dashboard' => app()->user->getHomeUrl(),
    'Categories' => url('category'),
    'Update a Category',
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));

$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
    $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Category'));
    echo $this->renderPartial('_form', array('model' => $model));
$this->endWidget();
<?php
$this->pageTitle = 'Banners are not activated';
$this->beginWidget('bootstrap.widgets.TbHeroUnit', array('heading' => 'Banners are not activated')); ?>
<p>Your account subscription does not have banners feature activated yet. If you would like to publish a banner on the AUSA Offers App, please contact your support team to activate this feature.</p>

<?php
$this->endWidget();
?>

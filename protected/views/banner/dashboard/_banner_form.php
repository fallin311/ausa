<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'offer-form',
    //'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => false,
        ));

echo $form->errorSummary($model);

// the banner should be slightly smaller
// and the background image has to show dashboard
echo $form->imageFieldRow($model, 'panorama_image', array(
    'class' => 'span',
    'imageOptions' => array(
        'configuration' => 'big-banner',
        'layout' => 'big-banner-layout',
        'noPhoto' => url('/images/big-banner.png'),
        'afterFinish' => '

var timestamp = new Date().getTime();            
$("#phone-preview").css("background-image", "url(" + response["fullUrl"] + "?w=320&h=100&v=" + timestamp + ")");'
        )));

$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->endWidget();

$this->endWidget();
?>

<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'offer-form',
    //'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => false,
        ));

echo $form->errorSummary($model);

// TODO: extend to search for vendors from given chapter using scopes
if ($model->isNewRecord){
echo $form->dropDownListRow($model, 'vendor_id', CHtml::listData(Vendor::model()->findAll(), 'vendor_id', 'vendor_name'), array('empty' => 'Please, select a vendor', 'class' => 'span', 'hint' => 'These vendors are from all the chapters. Please select one belonging to the selected chapter')
    );
}

echo $form->textFieldRow($model, 'banner_name', array('class' => 'span', 'maxlength' => 63, 'hint' => 'How it is going to be presented to a member.'));

echo $form->datepickerRow($model, 'start_date', array('class' => 'span', 'hint' => 'First day your offer is going to be published.'));

echo $form->datepickerRow($model, 'end_date', array('class' => 'span', 'hint' => 'Last day of publishing. It is usually the same day'));


if (!$model->isNewRecord) {
if ($model->published == 0) {

        $button = $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'type' => 'primary',
            'toggle' => 'radio', // 'checkbox' or 'radio'
            'buttons' => array(
                array('label'=>'Not yet', 'active' => true),
                array('label'=>'Publish the banner', 'type' => 'success', 'htmlOptions' => array('id' => 'publish-button')),
                
            ),
        ), true);
        echo $form->customRow($model, 'published', 'Publishing', $button, array('hint' => 'Publish the banner so it can be displayed in the cellphone app.'));
        echo CHtml::activeHiddenField($model, 'published');
        
        cs()->registerScript('publish-button', '
            $("#publish-button").click(function(){
                if ($(this).hasClass("active")){
                    $("#Banner_published").val(0);
                }else{
                    $("#Banner_published").val(1);
                }
            });
        ');
    } else {
        echo $form->customRow($model, 'published', 'Published', 'The banner is <b>published</b>.', array('hint' => 'Publish the banner so it can be displayed in the cellphone app.'));
    }
}

$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->endWidget();

$this->endWidget();
?>

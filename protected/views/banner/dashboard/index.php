<?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true)); ?>
<div class="widget-content">
    <h2>Banners</h2>
    <div>
        Display a publishing calendar here
    </div>
</div>
<?php $this->endWidget(); ?>

<?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true)); ?>
<div class="widget-content dashboard no-head-foot">
    <?php
    $this->widget('application.components.widgets.AODashboard', array('items' => array(
            'createOffer' => array('text' => 'Add a banner', 'icon' => 'dashboard-add', 'url' => url('/dashBanner/create')),
            //'offerReport' => array('text' => 'View Reports', 'icon' => 'dashboard-add', 'url' => url('/banner/reports')),
            //'subscription' => array('text' => 'Extend my subscription', 'icon' => 'dashboard-add', 'url' => url('/vendor/subscription')),
            'helpMember' => array('text' => 'Need Help?', 'icon' => 'dashboard-help', 'url' => '#')), 'multiCol' => false));
    ?>
</div>

<?php $this->endWidget(); ?>

</div>
<div class="ausa-contentMain row-fluid">

    <?php
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Your Banners'));
    $this->widget('ausa.widgets.AOButtonGroup', array(
        'type' => '',
        'buttons' => array(
            array('icon' => 'cog', 'items' => array(
                    array('label' => 'Create Banner', 'icon' => 'plus', 'url' => url('/dashBanner/create')),
                    '---',
                    array('label' => 'Get Help', 'icon' => 'question-sign', 'url' => '#'),
            )),
        ),
    ));
    $this->endWidget();

    $dataArray = array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $dataProvider,
        'columns' => array(
            'banner_name',
            array(
                'header' => 'Banner',
                'type' => 'raw',
                'value' => '"<img class=\"offer-preview-icon\" src=\"" . $data->getImageUrl(240) . "\" alt=\"preview\" />"'),
            array('name' => 'status', 'type' => 'raw', 'value' => '$data->getColoredStatus()'),
            array('name' => 'vendor_id', 'header' => 'Vendor', 'value' => '$data->vendor->vendor_name'),
            array('name' => 'start_date', 'type' => 'raw', 'header' => 'Dates', 'value' => '$data->getDatesForGrid()'),
            array('header' => 'Progress', 'type' => 'raw', 'value' => '$data->getStatsForGrid()'),
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{update}',
            ),
        ),
    );

    $this->widget('bootstrap.widgets.TbGridView', $dataArray);


    $this->endWidget();
    ?>  

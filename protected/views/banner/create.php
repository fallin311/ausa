<?php

$this->layout = '//layouts/full';

if (app()->user->IsSalesman() || app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        Vendor::model()->findByPk(app()->user->getActiveVendor())->vendor_name . ' Dashboard' => url('vendor/dashboard'),
        'Banners' => url('banner'),
        'Create a Banner',
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('vendor/dashboard'),
        'Banners' => url('banner'),
        'Create a Banner',
    );
}

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
$this->widget('application.components.widgets.AOGrid');

$this->beginWidget('application.components.widgets.AOStickyBar', array('sticky' => true, 'panelWrap' => true));
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'icon' => 'remove white',
    'type' => 'danger',
    'url' => url('/banner'),
    'label' => 'Cancel',
    'htmlOptions' => array('style' => 'margin: 5px 25px 0px 8px; float: right',)
));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'ajaxSubmit',
    'type' => 'success',
    'url' => url('/banner/create'),
    'htmlOptions' => array(
        'style' => 'margin: 5px 8px; float: right',
        'id' => 'save-banner-button',
    ),
    'loadingText' => 'processing...',
    'ajaxOptions' => array(
        'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
        'beforeSend' => 'function(){$("#save-banner-button").button("loading")}',
        'success' => 'function(response){
                
                if (response.length < 5){
                    // saved, lets go one step further
                    window.location.href="' . url('/banner/update') . '/" + response;
                }else{
                    console.log(response);
                    $("#save-banner-button").button("reset");

                    // we are going to display error message
                    $("#ajax-errors-holder").show();
                    $("#ajax-errors").html(response);
                    showError(\'Could not save the banner. Please check the error fields below.\');
                }
            }',
    ),
    'icon' => 'ok white',
    'label' => 'Save',
));

$this->endWidget();
?>

<div style="width: 100%; margin-bottom: 25px; position: relative; float: left; display: none;" id="ajax-errors-holder">
    <?php
    $this->widget('application.components.widgets.AONotice', array('notice' => '<div id="ajax-errors"></div>', 'noticeType' => 'errors'));
    ?>
</div>
<?php
$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
$this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Banner'));
$this->endWidget();
echo $this->renderPartial('_banner_form', array('model' => $model));
$this->endWidget();
?>

<div style="height: 40px; width: 100%; position: relative; float: left;"></div>

<div style="position: relative; float: left; width: 585px;">
    <?php
// Campaign
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Campaign'));
    $this->endWidget();
    echo $this->renderPartial('_campaign_form', array('model' => $model, 'vendor' => $vendor));
    $this->endWidget();
    ?>
    <div style="height: 40px; width: 100px; position: relative; float: left;"></div>
    <?php
// Link Target
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Target'));
    $this->endWidget();
    echo $this->renderPartial('_target_form', array('model' => $model, 'vendor' => $vendor));
    $this->endWidget();
    ?>
</div>
<?php
// Phone Preview

// default preview banner
$previewUrl = url('/images/banner.png');

$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
$this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Preview'));
$this->endWidget();
echo $this->renderPartial('_phone_preview', array('model' => $model, 'previewUrl' => $previewUrl));
$this->endWidget();
?>

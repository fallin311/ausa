<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'target-form',
    //'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => false,
        ));

echo $form->errorSummary($model);

$button = $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'type' => 'primary',
    'toggle' => 'radio',
    'buttons' => array(
        array('label' => 'Use external link', 'active' => $model->target == 'external' ? true : false, 'htmlOptions' => array('id' => 'external-button')),
        array('label' => 'Use a offer', 'active' => $model->target == 'internal' ? true : false, 'htmlOptions' => array('id' => 'internal-button')),
    ),
        ), true);
echo $form->customRow($model, 'target', 'Target', $button, array('hint' => 'Would you like to open your own landing page (external link) or an internal offer within the app?'));
echo CHtml::activeHiddenField($model, 'target');

cs()->registerScript('offer-button', '
            $("#internal-button").click(function(){
                if (!$(this).hasClass("active")){
                    $("#Banner_target").val("internal");
                    $("#Banner_external_url").attr("disabled", "disabled");
                    $(".offer-select").removeAttr("disabled");
                }
            });
            $("#external-button").click(function(){
                if (!$(this).hasClass("active")){
                    $("#Banner_target").val("external");
                    $("#Banner_external_url").removeAttr("disabled");
                    $(".offer-select").attr("disabled", "disabled");
                }
            });
        ');

$htmlOptions1 = array('class' => 'span', 'maxlength' => 255, 'hint' => 'What website would you like to open when this banner is clicked?');
if ($model->target == 'internal'){
    $htmlOptions1['disabled'] = 'disabled';
}
echo $form->textFieldRow($model, 'external_url', $htmlOptions1);

// list of all available branches
// only for regular banners
if (is_object($vendor)){
$_offers = $vendor->offers(array('scopes' => array('planPublished')));
$offersForm = array();
$htmlOptions = array(
    'hint' => 'What offer would you like to show when this banner is clicked?',
    'class' => 'offer-select'
);
}else{
    $_offers = array();
    $offersForm = array();
}

// preselect and disable
if ($model->target == 'external'){
    $htmlOptions['disabled'] = 'disabled';
}


foreach ($_offers as $offer) {
    $offersForm[$offer->offer_id] = $offer->offer_title . ', ' . Shared::formatDateLonger($offer->start_date) . ' - ' . Shared::formatDateLonger($offer->end_date);
}

echo $form->radioButtonListRow($model, 'offer_id', $offersForm, $htmlOptions);

$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->endWidget();

$this->endWidget();

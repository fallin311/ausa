<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'offer-form',
    //'htmlOptions' => array('style' => 'margin-bottom: 0px;'),
    'enableAjaxValidation' => false,
        ));

echo $form->errorSummary($model);

echo $form->imageFieldRow($model, 'panorama_image', array(
    'class' => 'span',
    'imageOptions' => array(
        'configuration' => 'banner',
        'layout' => 'banner-layout',
        'noPhoto' => url('/images/banner.png'),
        'afterFinish' => '
console.log(response);
var timestamp = new Date().getTime();            
$("#phone-preview").css("background-image", "url(" + response["fullUrl"] + "?w=320&h=50&v=" + timestamp + ")");'
        )));

$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->endWidget();

$this->endWidget();
?>

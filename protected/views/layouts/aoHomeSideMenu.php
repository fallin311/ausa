<?php
$this->widget('zii.widgets.CMenu', array(
'htmlOptions' => array('class' => 'aside-menu'),
'encodeLabel' => false,
'items' => array(
    array('label' => '<i class="icon-chevron-right' . ($this->route == 'site/contact' ? ' icon-white' : '') . '"></i> Contact Us', 'url' => url('site/contact'), 'itemOptions' => array('title' => 'Contact Us'), 'active' => $this->route == 'site/contact' ? true : false),
    array('label' => '<i class="icon-chevron-right' . ($this->route == 'site/terms' ? ' icon-white' : '') . '"></i> Terms of Service', 'url' => url('site/terms'), 'itemOptions' => array('title' => 'Terms of Service'), 'active' => $this->route == 'site/terms' ? true : false),
    array('label' => '<i class="icon-chevron-right' . ($this->route == 'site/privacy' ? ' icon-white' : '') . '"></i> Privacy Policy', 'url' => url('site/privacy'), 'itemOptions' => array('title' => 'Privacy Policy'), 'active' => $this->route == 'site/privacy' ? true : false),
    array('label' => '<i class="icon-chevron-right' . ($this->route == 'site/vendors' ? ' icon-white' : '') . '"></i> Become a Vendor', 'url' => url('site/vendors'), 'itemOptions' => array('title' => 'Become a Vendor'), 'active' => $this->route == 'site/vendors' ? true : false),
),));
?>
<?php /* @var $this Controller */ ?>
<?php cs()->registerScript('height-mod', 'if ($("#column-holder").height() > $("#main-content-holder").height()) { $("#main-content-holder").css({"height":$("#column-holder").height()+"px"}); }'); ?>
<?php $this->beginContent('//layouts/aoHome'); ?>
<div class="header"></div>
<div class="main">
    <div class="container">
        <div id="column-holder" class="row wrapper pull-back">
            <div id="main-content-holder" class="span9 main-left">
                <?php echo $content; ?>
            </div>
            <div id="sidebar" class="span3 sidebar-right">
                <?php $this->renderPartial('//layouts/aoHomeSideMenu', array(null)); ?>
                <br/>
                <!--<div class="shadow-box">
                    <h4><i class="icon-question-sign"></i> Questions? Need Help?</h4>
                    <div>Check out our <a href="#">help documentation</a> or simply <a href="#">get in touch</a></div>
                </div>-->
            </div>
        </div>
        <div class="row footer">
            <div class="span6 left">Copyright © 2013 <?php echo Entity::model()->findByPk(app()->user->getEntity())->entity_name; ?></div>
            <div class="span6 right">Help &amp; Support | <a href="<?php echo url('site/privacy'); ?>">Privacy Policy</a> | <a href="<?php echo url('site/terms'); ?>">Terms of Service</a></div>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>
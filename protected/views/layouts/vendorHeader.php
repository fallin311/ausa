<?php if(app()->user->IsSalesman()||app()->user->IsAdmin()): ?>
    <div class="singleCol">
        <div class="ausa-contentMainInner">
            <div class="widget-content no-head-foot" style="padding: 0px;">
                <div class="ausa-salesTeamDashboard">
                    <div class="header">
                        <img class="img-rounded dashboard" src="<?php echo url('/image/display/' . $model->logo) . '?w=' . Image::SIZE_FULL_W . '&h=' . Image::SIZE_FULL_H ?>" />
                        <h1>
                            <span style="display: block; line-height: 35px;"><?php echo $model->vendor_name; ?></span>
                           <!-- <span style="display: block; margin-top: -2px; line-height: 17px; font-size: 17px; font-weight: normal; color: #646464; letter-spacing: 5px; text-indent: 4px;">
                                Some Subtitle Here... but what?
                            </span> -->
                        </h1>
                        <a data-title="Vendor Dashboard" data-content="This is the dashboard for your vendor (<?php echo $model->vendor_name; ?>).<br/><br/>
                           From here, you can configure settings and monitor data unique to this specific vendor." data-placement="left" rel="clickover" class="btn btn-large disabled st-vendor-helpPopover" data-original-title=""><i class="icon-question-sign"></i></a>
                    </div>
<?php else: ?>
    <div class="singleCol">
        <div class="ausa-contentMainInner">
            <div class="widget-content no-head-foot" style="padding: 0px;">
                <div class="ausa-vendorDashboard">
<?php endif; ?>

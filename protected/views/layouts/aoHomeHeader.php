<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/aoHome'); ?>
<div class="header">
    <?php
    /**
     * Usage: In the view, declare the $this->heading variable. Use proper html to utilize css. See example.
     * @example $this->heading='<div class="container"><h1>Title</h1><p class="lead">Description text.<p></div>';
     */
    echo $this->heading;
    ?>
</div>
<div class="main">
    <div class="container register">
        <?php echo $content; ?>
        <?php if($this->footer): ?>
        <div class="row footer">
            <div class="span6 left">Copyright © 2013 <?php echo Entity::model()->findByPk(app()->user->getEntity())->entity_name; ?></div>
            <div class="span6 right hidden-phone">Help &amp; Support | <a href="<?php echo url('site/privacy'); ?>">Privacy Policy</a> | <a href="<?php echo url('site/terms'); ?>">Terms of Service</a></div>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php $this->endContent(); ?>
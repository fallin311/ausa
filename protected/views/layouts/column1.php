<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="ausa-contentMain singleCol">
    <div class="ausa-contentMainInner">
    <?php echo $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>
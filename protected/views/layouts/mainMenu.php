<div class="ausa-header">
<?php

$entity = Entity::model()->findByPk(app()->user->getEntity());
$browser = new EWebBrowser();
$branding = '<img alt="AUSA Offers" src="' . url('images/mini-logo'.$entity->identifier.'.png') . '">';
//$branding .= '<span class="bg"><span class="title">AUSA</span><span class="subtitle">Offers</span></span>';
$userOptionsMenu = array(
    'class' => 'bootstrap.widgets.TbMenu',
    'htmlOptions' => array('class' => ((!app()->user->isGuest) ? 'pull-right user' : 'pull-right')),
    'encodeLabel' => false,
    'items' => array(
        '---'
    ),
);
if (app()->user->isGuest) {
    $homeLink = 'Home';
    if ($browser->browser == EWebBrowser::BROWSER_IE) {
        $userOptionsMenu['items'][] = array('label' => 'Sign In', 'url' => url('user/login'));
    } else {
        cs()->registerCss('login-padding', '.ausa-header .dropdown .dropdown-menu .nav-header {'
                . 'padding: 15px 20px 10px 20px;'
                . '}');
        $userOptionsMenu['items'][] = array('label' => 'Sign In', 'url' => array('#'), 'items' => array(
                array('label' => $this->renderPartial('//user/_signin', array(null), true))));
    }
} else {
    $homeLink = 'My Dashboard';
    $userOptionsMenu['items'][] = app()->user->getUser()->getUserMenu();
}

$this->widget('bootstrap.widgets.TbNavbar', array(
    'type' => 'inverse',
    'brand' => $branding,
    'brandUrl' => url('site/index'),
    //'fluid' => true,
    'collapse' => false,
    'items' => array(
        array(
            'class' => 'bootstrap.widgets.TbMenu',
            'htmlOptions' => array('class' => 'ausa-mainMenu'),
            'items' => array(
                '---',
                array('label' => $homeLink, 'url' => app()->user->getHomeUrl()),
                array('label' => 'Admin', 'items' => array(
                        array('label' => 'Sales Team', 'url' => array('salesTeam/index')),
                        array('label' => 'Chapters', 'url' => array('chapter/index')),
                        array('label' => 'Images', 'url' => array('image/index')),
                        array('label' => 'Categories', 'url' => url('/category')),
                        array('label' => 'Dashboard Banners', 'url' => url('/dashBanner')),
                        array('label' => 'Users', 'url' => url('/user')),
                        array('label' => 'Logs', 'url' => url('/admin/logs')),
                        array('label' => 'System Update', 'url' => url('/admin/systemUpdate')),
                    ), 'visible' => app()->user->IsAdmin()),
                array('label' => 'Vendors', 'url' => array('/vendor/index'), 'visible' =>
                    ((app()->user->IsAdmin()) ? true : ((app()->user->IsSalesman()) ? true : ((app()->user->IsChapterUser()) ? true : false))),
                ),
                array('label' => 'Members', 'items' => array(
                        array('label' => 'Members Dashboard', 'url' => url('/member')),
                        array('label' => 'Mass Upload', 'url' => url('/member/upload')),
                        array('label' => 'Add a Member', 'url' => url('/member/create')),
                    ), 'visible' => ((app()->user->IsAdmin()) ? true : ((app()->user->IsChapterUser()) ? true : false))),
                array('label' => 'Offers', 'items' => array(
                        array('label' => 'Offers Dashboard', 'url' => array('offer/index')),
                        array('label' => 'Offer Categories', 'url' => array('category/index'), 'visible' => app()->user->IsAdmin()),
                        array('label' => 'Offer Templates', 'url' => array('template/index'), 'visible' => app()->user->IsAdmin()),
                        array('label' => 'Banners', 'url' => url('/banner'), 'visible' => app()->user->IsAdmin()),
                    ), 'visible' => ((app()->user->IsAdmin()) ? true : ((app()->user->IsSalesman()) ? true : ((app()->user->IsVendor()) ? true : ((app()->user->IsChapterUser()) ? true : false))))),
                /*
                  array('label' => 'Reports', 'items' => array(
                  array('label' => 'Earnings Report', 'url' => url('/report/index')),
                  ), 'visible' => ((app()->user->IsAdmin()) ? true : ((app()->user->IsSalesman()) ? true : ((app()->user->IsVendor()) ? true : ((app()->user->IsChapterUser()) ? true : false))))),
                 */
                '---',
            ),
        ),
        $userOptionsMenu
    ),
));
?>
</div>
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <link rel="stylesheet" type="text/css" href="<?php echo app()->request->baseUrl; ?>/css/main.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo app()->request->baseUrl; ?>/css/<?php echo CUSTOM_CSS ?>.css" media="screen, projection" />
        <link href="<?php echo absUrl('/favicon'.Entity::model()->findByPk(app()->user->getEntity())->identifier.'.ico'); ?>" rel="shortcut icon"/>

        <title><?php echo h($this->pageTitle); ?></title>
    </head>

    <body id="ausa" class="ausa">
        <?php echo $content; ?>
    </body>
</html>

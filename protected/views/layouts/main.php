<?php 
if(app()->user->hasFlash('success')) {
    // display the success message
    cs()->registerScript('updates','setTimeout(function() { showSuccess(\''.app()->user->getFlash('success').'\'); }, 1500);');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <link rel="stylesheet" type="text/css" href="<?php echo app()->request->baseUrl; ?>/css/main.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo app()->request->baseUrl; ?>/css/<?php echo CUSTOM_CSS ?>.css" media="screen, projection" />
        <link href="<?php echo absUrl('/favicon'.Entity::model()->findByPk(app()->user->getEntity())->identifier.'.ico'); ?>" rel="shortcut icon"/>

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body id="ausa" class="ausa">
        <div class="ausa-body">
            <?php $this->renderPartial('//layouts/mainMenu', array(null)); ?>
            <div class="ausa-contentWrapper container">
                <div id="ausa-contentArea">
                    <div class="ausa-contentMain row-fluid">
                        <?php echo $content; ?>
                    </div>

                </div>
            </div>
        </div>
        <div style="position: fixed; color: #fff; line-height: 40px; z-index: 4;" class="navbar navbar-inverse navbar-fixed-bottom">
            <div class="navbar-inner">
                <div class="container">
                    <div class="row">
                        <div class="span6">&copy; <?php echo date('Y'); ?> AUSA Offers<span class="bullet"></span><a class="footerLink" href="<?php echo url('site/terms'); ?>">Terms of Use</a><span class="bullet"></span><a class="footerLink" href="<?php echo url('site/privacy'); ?>">Privacy Policy</a></div>
                        <div class="span6">
                            <div class="pull-right"><a class="backToTop btn btn-inverse btn-small" href="#top"><i class="icon-arrow-up"></i> top</a></div>
                            <div class="divider-vertical pull-right"></div>
                            <div class="pull-right">Designed &amp; Developed by <a class="footerLink" href="http://www.inradiussystems.com" target="_blank">InRadius Systems</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->widget('application.components.widgets.AOBackstretch'); ?>
    </body>
</html>

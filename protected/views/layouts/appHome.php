<?php
    cs()->registerCss('alerts',
        '.jquery-notify-bar {
            font-family: Candara,Cochin, Georgia, Times, "Times New Roman", serif;
            text-shadow: 1px 1px 2px #DDD;
            -moz-box-shadow: 0px 2px 8px #000000; /* FF3.5+ */;
            -webkit-box-shadow: 0px 2px 8px #000000; /* Saf3.0+, Chrome */;
            box-shadow: 0px 2px 8px #000000; /* Opera 10.5, IE 9.0 */;
            width: 100%;
            position: fixed;
            top: 0px;
            left: 0;
            z-index: 1041;
            color: #555;
            font-size: 18px;
            text-align: center;
            padding: 20px 0px;
            font-weight: bold;
            background-color: #EBEBEB; /* FF3.6 */;
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorStr=\'#EBEBEB\', EndColorStr=\'#CCCCCC\')";
            background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #EBEBEB),color-stop(1, #CCCCCC));/* Saf4+, Chrome */
        }
        .jquery-notify-bar.error {
            color: #FFF0F0;
            text-shadow: 1px 1px 1px #BD3A3A;
            background-color: #DB4444; /* FF3.6 */;
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorStr=\'#DB4444\', EndColorStr=\'#BD3A3A\')";
            background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #DB4444),color-stop(1, #BD3A3A));/* Saf4+, Chrome */
        }
        .jquery-notify-bar.success {
            color: #fff;
            text-shadow: #509C4B 1px 1px 1px;
            background-color: #8DC96F; /* FF3.6 */;
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorStr=\'#8DC96F\', EndColorStr=\'#509C4B\')";
            background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #8DC96F),color-stop(1, #509C4B));/* Saf4+, Chrome */
        }
        .notify-bar-close {
            position: absolute;
            left: 95%;
            font-size: 11px;
        }');
    $entity = Entity::model()->findByPk(app()->user->getEntity());
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <link href="<?php echo absUrl('/favicon'.$entity->identifier.'.ico'); ?>" rel="shortcut icon"/>

        <title><?php echo h($this->pageTitle); ?></title>
    </head>

    <body class="page-home">
        <?php
            echo $content;
            echo CHtml::script('var _gaq = _gaq || [];'
                    .'_gaq.push([\'_setAccount\', \''. $entity->ganalytics_id .'\']);'
                    .'_gaq.push([\'_trackPageview\']);'
                    .'(function() {'
                    .'var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;'
                    .'ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';'
                    .'var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);})();');
        ?>
    </body>
</html>
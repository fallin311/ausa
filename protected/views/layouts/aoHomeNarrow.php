<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/aoHome'); ?>
<div class="header">
    <?php
    /**
     * Usage: In the view, declare the $this->heading variable. Use proper html to utilize css. See example.
     * @example $this->heading='<div class="container"><h1>Title</h1><p class="lead">Description text.<p></div>';
     */
    echo $this->heading;
    if(!empty($this->heading)){ cs()->registerCss('top-margin',
        '.mid-container {
            top: -39px;
        }'); }
    ?>
</div>
<div class="mid-container wrapper">
    <?php echo $content; ?>
</div>
<div class="mid-container-foot"></div>
<?php $this->endContent(); ?>
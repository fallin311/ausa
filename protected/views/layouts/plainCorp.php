<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <?php $this->widget('application.components.widgets.AOAppHome'); ?>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo app()->request->baseUrl; ?>/css/<?php echo CUSTOM_CSS ?>.css" media="screen, projection" />
        <link href="<?php echo absUrl('/favicon'.Entity::model()->findByPk(app()->user->getEntity())->identifier.'.ico'); ?>" rel="shortcut icon"/>

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body class="page-home">
        <div class="ausa-header">
            <?php $this->renderPartial('//layouts/mainMenu', array(null)); ?>
        </div>
        <div id="content-holder" class="container fill">
            <div class="row-fluid corp-content">
                <div class="span3 inner-sidebar">
                    <div class="sidebar-nav sidebar-nav-fixed">
                        <?php
                        $this->widget('zii.widgets.CMenu', array(
                            'htmlOptions' => array('class' => 'corpSidebar'),
                            'encodeLabel' => false,
                            'items' => array(
                                array('label' => '<i class="icon-chevron-right' . ($this->route == 'site/contact' ? ' icon-white' : '') . '"></i> Contact Us', 'url' => url('site/contact'), 'itemOptions' => array('title' => 'Contact Us'), 'active' => $this->route == 'site/contact' ? true : false),
                                array('label' => '<i class="icon-chevron-right' . ($this->route == 'site/terms' ? ' icon-white' : '') . '"></i> Terms of Service', 'url' => url('site/terms'), 'itemOptions' => array('title' => 'Terms of Service'), 'active' => $this->route == 'site/terms' ? true : false),
                                array('label' => '<i class="icon-chevron-right' . ($this->route == 'site/privacy' ? ' icon-white' : '') . '"></i> Privacy Policy', 'url' => url('site/privacy'), 'itemOptions' => array('title' => 'Privacy Policy'), 'active' => $this->route == 'site/privacy' ? true : false),
                                array('label' => '<i class="icon-chevron-right' . ($this->route == 'site/vendors' ? ' icon-white' : '') . '"></i> Become a Vendor', 'url' => url('site/vendors'), 'itemOptions' => array('title' => 'Become a Vendor'), 'active' => $this->route == 'site/vendors' ? true : false),
                            ),));
                        ?>
                    </div>
                </div>
                <div class="span9">
                    <?php echo $content; ?>
                </div>
                <?php echo CHtml::script('var _gaq = _gaq || [];'
                        .'_gaq.push([\'_setAccount\', \'UA-37142115-1\']);'
                        .'_gaq.push([\'_trackPageview\']);'
                        .'(function() {'
                        .'var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;'
                        .'ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';'
                        .'var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);'
                        .'})();'); ?>
    </body>
</html>
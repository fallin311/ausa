<?php
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'limits-form',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => false,
        ));

echo $form->errorSummary($model);

echo $form->textFieldRow($model, 'global_limit', array('class' => 'span', 'maxlength' => 12, 'hint' => 'How many total times can this offer be redeemed? Keep zero for unlimited redeems.'));

echo $form->customRow($model, 'per_capita_limit_number', 'Allow', 
 CHtml::activeTextField($model, 'per_capita_limit_number', array('class' => 'span3')) . '&nbsp; redeems per &nbsp;' .
        CHtml::activeDropDownList($model, 'per_capita_limit_unit', 
        array(
            'campaign' => 'Offer',
            'month' => 'Month',
            'day' => 'Day',
            ), array('class' => 'span6')), 
        array('class' => 'span', 'hint' => 'How many times can one offer be redeemed'));
$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->endWidget();

$this->endWidget();
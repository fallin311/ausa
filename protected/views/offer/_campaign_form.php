<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'campaign-form',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => false,
        ));

echo "<div id='publishing-info' class='ausa-form-info'>";
$info = $model->getPublishingInfo();
Shared::debug($info);
//echo "<p>" . $info['limits'] . "</p>";
echo "</div>";

echo $form->errorSummary($model);

// once it is started or somebody downloaded the offer, we cannot change it
if ($model->published || $model->impressions){
    echo $form->uneditableRow($model, 'start_date', array('class' => 'span', 'hint' => 'First day your offer is going to be published. The offer has been published or someone downloaded it already.'));
}else{
    echo $form->datepickerRow($model, 'start_date', array('class' => 'span', 'hint' => 'First day your offer is going to be published.'));
}

$displayDuration = true;
// is there a custom end date??
$startDay = date('d', Shared::toTimestamp($model->start_date));
$endDay = date('d', Shared::toTimestamp($model->end_date));

if ($startDay != $endDay){
    $displayDuration = false;
}

// get the default value for duration
$months = 1;
if (strlen($model->end_date)){
    $diff = strtotime($model->end_date) - strtotime($model->start_date);
    $months = ceil($diff / Shared::month);
}

$minMonth = 1;

if ($model->published){
    // we might restrict the number of months. Make it no less than one
    // and do not go below the current month
    $diff = strtotime($model->end_date) - time();
    if ($diff > 0){
        $minMonth = ceil($diff / Shared::month);
    }
}

$monthsArr = array();
for ($i = $minMonth; $i <= 12; $i ++){
    if ($i == 1){
        $monthsArr[$i] = "1 Month";
    }else{
        $monthsArr[$i] = $i . " Months";
    }
}

if ($displayDuration){
echo $form->customRow($model, 'end_date', 'Publish for', CHtml::dropDownList('Offer[campaign_duration]', $months, $monthsArr) . "&nbsp;&nbsp;<span class='ajax-link' id='calendar-switch'>or select end date</span>", 
        array('class' => 'span', 'id' => 'publish-for'));
}

// by default hidden end_date select
echo "<div id='end-date' " . ($displayDuration ? "style='display: none;" : '') . "'>";
echo $form->datepickerRow($model, 'end_date', array('class' => 'span', 'hint' => 'Last day your offer is going to be published.'));
echo "</div>";

// TODO: check limits in the real time
    if (!$model->isNewRecord) {
        //if ($model->published == 0) {

            $button = $this->widget('bootstrap.widgets.TbButtonGroup', array(
                'type' => 'primary',
                'toggle' => 'radio', // 'checkbox' or 'radio'
                'buttons' => array(
                    array('label' => 'Not yet', 'active' => !$model->published),
                    array('label' => 'Publish the offer', 'active' => $model->published, 'type' => 'success', 'htmlOptions' => array('id' => 'publish-button')),
                ),
                    ), true);
            if ($model->published){
                $button = $this->widget('bootstrap.widgets.TbButton', array(
                'type' => 'danger',
                'toggle' => true, // 'checkbox' or 'radio'
                'label' => 'Unpublish',
                'size' => 'small',
                'htmlOptions' => array('id' => 'unpublish-button')
                    ), true);
                $element = 'The offer is <b>published</b>. ' . $button;
            }else{
                $button = $this->widget('bootstrap.widgets.TbButton', array(
                'type' => 'success',
                'toggle' => true, // 'checkbox' or 'radio'
                'label' => 'Publish',
                'size' => 'small',
                'htmlOptions' => array('id' => 'publish-button')
                    ), true);
                $element = 'The offer is <b>unpublished</b>. ' . $button;
            }
            echo $form->customRow($model, 'published', 'Publishing', $element, array('hint' => 'Publish the offer so it can be displayed in the cellphone app. Once it is published, it cannot be unpublished and access to some fields on this page will be limited.'));
            echo CHtml::activeHiddenField($model, 'published');

            cs()->registerScript('publish-button', '
            $("#publish-button").click(function(){
                if ($(this).hasClass("active")){
                    $("#Offer_published").val(0);
                }else{
                    $("#Offer_published").val(1);
                }
            });
            $("#unpublish-button").click(function(){
                if ($(this).hasClass("active")){
                    $("#Offer_published").val(1);
                }else{
                    $("#Offer_published").val(0);
                }
            });
        ');
        //} else {
        //    echo $form->customRow($model, 'published', 'Published', 'The offer is <b>published</b>.', array('hint' => 'Publish the offer so it can be displayed in the cellphone app. Once it is published, it cannot be unpublished and access to some fields on this page will be limited.'));
        //}
    } else {
        echo $form->customRow($model, 'published', 'Publishing', 'Save the offer first to see the publishing options.', array('hint' => 'Publish the offer so it can be displayed in the cellphone app. Once it is published, it cannot be unpublished and access to some fields on this page will be limited.'));
    }

cs()->registerScript('publish-offer-for', '
            // Update end date in the end date calendar together with drop box month select 
            $("#Offer_campaign_duration").change(function(){
                var months = $("#Offer_campaign_duration option:selected").val();
                
                // possible problem with date formatting on safari
                // stores array of (Y,M,D)
                var dateParts = $("#Offer_start_date").val().split("-");
                
                if (dateParts[1] + months > 12){
                    dateParts[0]++;
                    dateParts[1] -= 12;
                }
                
                dateParts[1] += months;

                var startDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
        
                var endDate = startDate.getFullYear() + "-";
                endDate += (startDate.getMonth() + 1) + "-";
                endDate += startDate.getDate();
                $("#Offer_end_date").val(endDate);
            });

            $("#calendar-switch").click(function(){
                $("#publish-for").hide();
                $("#end-date").show();
                
                $("#Offer_campaign_duration").remove();
            });
        ');

cs()->registerScript('offer-colision', '
        /**
        * Update content of info box to display if the offer can be published 
        * for the time period user selected. Also diables publishing button.
        */
        function formatOfferColision(input){
            console.log(input);
            $infoBox = $("#publishing-info");
            $button = $("#publish-button");
            var html = "<p>" + input.limits + "</p>";
            
            if (input.colisions.length > 0){
                html += "<div>These offers are published at the same time:</div><ul>";
                for (i in input.colisions) {
                    html += "<li><b>" + input.colisions[i].title + "</b><br />" + input.colisions[i].start_date + " - " + input.colisions[i].end_date + "</li>";
                }
                html += "</ul>";
            }
            
            if ($button.length && input.canPublish){
                $button.removeAttr("disabled");
            } else {
                $button.attr("disabled", "disabled");
            }

            $infoBox.html(html);
            refreshWookmark();
        }
        
        // render limitations the first time it is loaded
        formatOfferColision(' . json_encode($info) . ');
        
        function checkColisions(){
            $.ajax({
                data: "offerId=' . $model->offer_id . '&startDate=" + escape($("#Offer_start_date").val()) + "&endDate=" + escape($("#Offer_end_date").val()),
                type: "get",
                dataType: "json",
                url: "' . url('/offer/getPublishingInfo') . '",
                success: function(response){
                    formatOfferColision(response);
                }
            });
        }

        $("#Offer_start_date").change(checkColisions);
        $("#Offer_end_date").change(checkColisions);
        
        ');

$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->endWidget();

$this->endWidget();

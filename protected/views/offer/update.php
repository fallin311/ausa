<?php
$this->widget('application.components.widgets.AOGrid');
if (app()->user->IsSalesman() || app()->user->IsAdmin()) {
    $breadcrumbs = array(
        'My Dashboard' => app()->user->getHomeUrl(),
        Vendor::model()->findByPk(app()->user->getActiveVendor())->vendor_name . ' Dashboard' => url('vendor/dashboard'),
        'Offers' => url('vendor/offers'),
        'Edit an Offer',
    );
} else {
    $breadcrumbs = array(
        'My Dashboard' => url('vendor/dashboard'),
        'Offers' => url('vendor/offers'),
        'Edit an Offer',
    );
}

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));
?>

<?php
$this->beginWidget('application.components.widgets.AOStickyBar', array('sticky' => true, 'panelWrap' => true));
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'icon' => 'remove white',
    'type' => 'danger',
    'url' => url('/vendor/offers'),
    'label' => 'Cancel',
    'htmlOptions' => array('style' => 'margin: 5px 25px 0px 8px; float: right',)
));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'ajaxSubmit',
    'type' => 'success',
    'url' => url('/offer/update') . '/' . $model->offer_id,
    'htmlOptions' => array(
        'style' => 'margin: 5px 8px; float: right',
        'id' => 'save-offer-button',
    ),
    'loadingText' => 'processing...',
    'ajaxOptions' => array(
        'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
        'beforeSend' => 'function(){$("#save-offer-button").button("loading")}',
        'success' => 'function(response){
                
                if (response.length < 5){
                        window.location.href="' . url('offer') . '";
                }else{
                    $("#save-offer-button").button("reset");

                    // we are going to display error message
                    $("#ajax-errors-holder").show();
                    $("#ajax-errors").html(response);
                    showError(\'Could not save the offer. Please check the error fields below.\');
                }
            }',
    ),
    'icon' => 'ok white',
    'label' => 'Save',
));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'ajaxSubmit',
    'type' => '',
    'url' => url('/offer/update') . '/' . $model->offer_id,
    'htmlOptions' => array(
        'style' => 'margin: 5px 8px; float: right',
        'id' => 'apply-offer-button',
    ),
    'loadingText' => 'processing...',
    'ajaxOptions' => array(
        'data' => 'js:serializeAll()+"&isAjaxRequest=1"',
        'beforeSend' => 'function(){$("#apply-offer-button").button("loading")}',
        'success' => 'function(response){
                
                if (response.length < 5){
                    $("#ajax-errors-holder").hide();
                    $("#apply-offer-button").button("reset");
                    showSuccess(\'Offer changes have been updated.\');
                    $published = $("#Offer_published");
                    if ($published.length){
                        if ($published.val() == 1){
                            window.location.href="' . url('/offer/update/' . $model->offer_id) . '";
                        }
                    }
                }else{
                    console.log(response);
                    $("#apply-offer-button").button("reset");

                    // we are going to display error message
                    $("#ajax-errors-holder").show();
                    $("#ajax-errors").html(response);
                    showError(\'Could not save the offer. Please check the error fields below.\');
                }
            }',
    ),
    'icon' => 'ok',
    'label' => 'Apply',
));

$this->endWidget();
?>

<div style="width: 100%; margin-bottom: 25px; position: relative; display: none;" id="ajax-errors-holder">
    <?php $this->widget('application.components.widgets.AONotice', array('notice' => '<div id="ajax-errors"></div>', 'noticeType' => 'errors')); ?>
</div>

<?php
$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
$this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Update this Offer'));
$this->widget('ausa.widgets.AOButtonGroup', array(
    'type' => '',
    'buttons' => array(
        array('icon' => 'cog', 'items' => array(
                array('label' => 'Mobile Preview', 'icon' => 'plus', 'url' => '#'),
        )),
    ),
    'htmlOptions' => array('class' => 'mobile-dropdown')
));
$this->endWidget();
echo $this->renderPartial('_offer_form', array('model' => $model, 'templateDataProvider' => $templateDataProvider, 'vendor' => $vendor));
$this->endWidget();

// open a popup window
cs()->registerScript('mobile-preview', '
$(".mobile-dropdown ul li a").click(function(){
    newwindow=window.open("' . url('/user/mobilePreview') . '","mobile preview","height=850,width=720");
    if (window.focus) {
        newwindow.focus();
    }
    this.hide();
    return false;
});
'); ?>
<div class="ausa-contentMain row-fluid">
    <div id="ausa-contentMainTiles">
        <ul id="tiles">
            <li class="grid">
                <?php
                $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Important Dates'));
                echo $this->renderPartial('_campaign_form', array('model' => $model));
                $this->endWidget();
                ?>
            </li>
            <li class="grid">
                <?php
                // Membership form
                $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Limits'));
                echo $this->renderPartial('_limits_form', array('model' => $model));
                $this->endWidget();
                ?>
            </li>
            <li class="grid">
                <?php
                $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Redemptions'));
                echo $this->renderPartial('_redemption_form', array('model' => $model));
                $this->endWidget();
                ?>
            </li>
            <li class="grid">
                <?php
                $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
                $this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Branches'));
                echo $this->renderPartial('_branches_form', array('model' => $model, 'vendor' => $vendor));
                $this->endWidget();
                ?>
            </li>
        </ul>
    </div>
            
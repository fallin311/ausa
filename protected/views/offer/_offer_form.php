<?php
$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'offer-form',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => false,
        ));

Yii::import('ext.simpleModal.ModalWidget');
ModalWidget::loadAssets();

cs()->registerCss('select2-css', 'ul.select2-choices {
            width:369px;
            margin-bottom: 10px !important;
        }'
        . 'ul.select2-choices li.select2-search-field input.select2-input {
            height: 20px;
            padding: 4px 6px;
            margin-bottom: 0px;
        }'
        . 'div.select2-drop { margin-top: -12px !important; border-color: #CCC; }');

echo $form->errorSummary($model);
?>
<div style="position: relative; float: left; width: 50%">
<?php
if ($model->published) {
    echo $form->uneditableRow($model, 'offer_title', array('class' => 'span', 'maxlength' => 40, 'hint' => 'The offer title has to stay fixed once the offer is published.'));
} else {
    echo $form->textFieldRow($model, 'offer_title', array('class' => 'span', 'maxlength' => 40, 'hint' => 'How it is going to be presented to a member.'));
}

// Bootstrap WYSIWYG Textarea - Begin
/*
echo '<div class="form-row" style="margin-top:-6px; min-height:157px;"><label for="yw0">Description</label><div class="form-element"><div class="clearfix">';
$this->widget('bootstrap.widgets.TbWysiwyg', array(
    'model' => $model,
    'attribute' => 'offer_description',
    'htmlOptions' => array(
        'name' => 'Offer[offer_description]',
        'class' => 'span',
        'id' => 'Offer_offer_description',
        'style' => 'width: 371px; height: 150px;'),
    'buttons' => array(
       // 'emphasis',
        'font-styles',
        'link',
        'color',
        'html'),
));
echo '<a class="ausa-toolTip" rel="tooltip" data-original-title="Describe why the member should redeem the offer and what rules are related to its redemption."></a>';
echo '</div></div></div>';
*/
// Bootstrap WYSIWYG Textarea - End
echo $form->textAreaRow($model, 'offer_description', array('class' => 'span', 'style' => 'height: 150px;', 'hint' => 'Describe why the member should redeem the offer and what rules are related to its redemption.'));
?>
</div><div style="position: relative; float: left; width: 49%; border-left: 1px solid #EEE;">

    <?php
    $button = $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Select an offer template',
        'htmlOptions' => array('id' => 'template-button'),
        'type' => 'primary',
        'size' => 'large',
            ), true);
    echo $form->customRow($model, 'end_date', 'Offer Templates', $button, array('hint' => 'Save your time and get a title, description and image from a template.'));
    ?>

    <?php
    echo $form->imageFieldRow($model, 'offer_icon', array('class' => 'span', 'maxlength' => 10, 'imageOptions' => array(
            'configuration' => 'logo',
            'layout' => 'logo-layout'
            )));
    ?>
</div>
<div class="clearfix"></div>
    <?php
    $checkboxOptions = array('multiple' => 'multiple');
    if ($model->published) {
        $checkboxOptions['disabled'] = 'disabled';
    }

    echo '<div class="form-row" style="width: 50%;">'
    . '<label for="Offer_categoriesArr" class="required">Category <span class="required">*</span></label>'
    . '<a class="ausa-toolTip" rel="tooltip" data-original-title="Select Associated Category"></a>'
    . '<div class="form-element"><div class="clearfix">';
    $this->widget('ext.select2.ESelect2', array(
        'model' => $model,
        'attribute' => 'categoriesArr',
        'data' => CHtml::listData(Category::model()->published()->findAll(array('order' => 'category_name ASC')), 'category_id', 'category_name'),
        'options' => array('placeholder' => 'Select Associated Category', 'maximumSelectionSize' => $vendor->max_categories),
        'htmlOptions' => $checkboxOptions,
    ));
    echo '</div></div></div>';

    
    $this->beginWidget('application.components.widgets.AOPanelFoot');

    $this->endWidget();

    $this->endWidget();

    cs()->registerScript('select-template-popup', '
    /** display the crate / update popup */
    function showTemplatePopup(){
        $("#offer-template").modal({
            containerCss:{
		        width: 800,
		        height: 540,
		        padding: 0
	        }
        });
    }
    
    /** bind the action */
    $("#template-button").click(showTemplatePopup);
    
    /** make the offer clickable */
    $(".select-offer-template").live("click", function(){
        // preload the template
        $("#Offer_offer_title").val($(this).find(".template-title").html());

        $("#Offer_offer_description").val($(this).find(".template-description").html());
        
        // set the form field value, which is being submitted
        var imageId = $(this).find("img").data("id");
        $("#Offer_offer_icon").val(imageId);
        
        // set the correct image
        url = "' . url('/image/display') . '?img=" + imageId + "&w=' . Image::SIZE_PREVIEW_W . '&h=' . Image::SIZE_PREVIEW_H . '"
        $(".preview-image").attr("src", url);
        
        // close the modal dialog
        $.modal.close();
    });
    
');
    ?>
<div id="offer-template" style="display: none;">
<?php
$this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons blocks_images', 'panelTitle' => 'Select an Offer Template'));
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $templateDataProvider,
    'itemView' => '//template/_select_view',
    'htmlOptions' => array('style' => 'padding-top: 0;'),
));
$this->widget('application.components.widgets.AOPanelFoot');
?>
</div>
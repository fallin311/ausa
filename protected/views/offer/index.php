<?php
cs()->registerCss('offer-table', '
.offer-preview-icon {
    display: inline-block;
}
.offer-description {
    display: inline-block;
    padding-left: 15px;
}
');

if(app()->user->hasFlash('success')) {
    cs()->registerScript('updates','setTimeout(function() { showSuccess(\''.app()->user->getFlash('success').'\'); }, 500);');
}
?>
<?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true)); ?>
<div class="widget-content">
    <h2>Offers</h2>
    <div>
        <p>
            <b>Active/Max:</b> <?php echo $vendor->getTotalActiveOffers() ?> / <?php echo $vendor->max_offers ?>
        </p>
        <p>
            <b>New:</b> <?php echo $vendor->getTotalNewOffers() ?>
        </p>
        <p>
            <b>Expired:</b> <?php echo $vendor->getTotalExpiredOffers() ?>
        </p>
    </div>
</div>
<?php $this->endWidget(); ?>

<?php $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true)); ?>
<div class="widget-content dashboard no-head-foot">
    <?php
    $this->widget('application.components.widgets.AODashboard', array('items' => array(
            'createOffer' => array('text' => 'Create an Offer', 'icon' => 'dashboard-add', 'url' => url('/offer/create')),
            'offerReport' => array('text' => 'View Reports', 'icon' => 'dashboard-add', 'url' => url('/offer/reports')),
            'subscription' => array('text' => 'Extend my subscription', 'icon' => 'dashboard-add', 'url' => url('/vendor/subscription')),
            'helpMember' => array('text' => 'Need Help?', 'icon' => 'dashboard-help', 'url' => '#')), 'multiCol' => false));
    ?>
</div>

<?php $this->endWidget(); ?>

</div>
<div class="ausa-contentMain row-fluid">

    <?php
    $this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
    $this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'All Your Offers'));
    $this->widget('ausa.widgets.AOButtonGroup', array(
                'type'=>'',
                'buttons'=>array(
                    array('icon'=>'cog', 'items'=>array(
                        array('label'=>'Create Offer', 'icon' => 'plus', 'url'=>url('/offer/create')),
                        '---',
                        array('label'=>'Get Help', 'icon' => 'question-sign', 'url' => '#'),
                    )),
                ),
            ));
    $this->endWidget();

    $dataArray = array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $dataProvider,
        'columns' => array(
            array('name' => 'offer_title',
                'header' => 'Name',
                'type' => 'raw',
                'value' => '"<img class=\"offer-preview-icon\" src=\"" . $data->getImageUrl(80) . "\" alt=\"preview\" />
<div class=\"offer-description\">
<h5>" . $data->offer_title . "</h5>
" . $data->offer_description . "
</div>
"'),
            array('name' => 'status', 'type' => 'raw', 'value' => '$data->getColoredStatus()'),
            array('name' => 'start_date', 'type' => 'raw', 'header' => 'Dates', 'value' => '$data->getDatesForGrid()'),
            array('header' => 'Progress', 'type' => 'raw', 'value' => '$data->getStatsForGrid()'),
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{update}',
            ),
        ),
    );

    $this->widget('bootstrap.widgets.TbGridView', $dataArray);


    $this->endWidget();
    ?>  
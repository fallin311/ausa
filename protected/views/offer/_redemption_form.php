<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'redemption-form',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => false,
        ));

echo $form->errorSummary($model);

// quick and dirty fix of removed redemption types
Shared::debug($model->redemption_types);
if (is_array($model->redemption_types) && in_array(4, $model->redemption_types)) {
    $model->redemption_types = 4;
} else {
    $model->redemption_types = '';
}
Shared::debug($model->redemption_types);
$button = $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'type' => 'primary',
    'toggle' => 'radio', // 'checkbox' or 'radio'
    'buttons' => array(
        array('label' => 'Redeemable Offer', 'active' => $model->redemption_types == 4, 'htmlOptions' => array('id' => 'redeemable-button')),
        array('label' => 'Service or Ad', 'active' => $model->redemption_types != 4, 'htmlOptions' => array('id' => 'non-redeemable-button')),
    ),
        ), true);
echo $form->customRow($model, 'redemption_types', 'Coupon Type', $button, array('hint' => 'Select a service type for offers if you want to just advertise and there is no redemption in your branches.'));


echo CHtml::activeHiddenField($model, 'redemption_types');

cs()->registerScript('redeemable-button', '
            $("#redeemable-button").click(function(){
                $("#Offer_redemption_types").val("4");
                
            });
            $("#non-redeemable-button").click(function(){
                $("#Offer_redemption_types").val("");
            });
        ');

// redemption types are not implemented yet
/* echo $form->checkBoxListRow($model, 'redemption_types', array(
  UpdateOfferBehavior::EMAIL_REDEMPTION => 'Allow Email Offer',
  UpdateOfferBehavior::PRINTED_REDEMPTION => 'Allow Printed Offer',
  UpdateOfferBehavior::QR_SCAN_REDEMPTION => 'Allow QR scan at branch',
  UpdateOfferBehavior::SWIPE_REDEMPTION => 'Allow swipe redemption (default)',
  ), array('hint' => 'Current version supports only redeem through cellphone. Keep empty if you don\'t want the offer to be redeemed.'));
 */

$redemptionField = CHtml::activeTextField($model, 'redemption_code', array('class' => 'span6', 'maxlength' => 12));
$button = $this->widget('bootstrap.widgets.TbButton', array(
    'label' => 'Generate',
    'htmlOptions' => array('class' => 'random-code-button', 'style' => 'margin-left: 15px; margin-bottom: 10px;'),
    'type' => 'primary',
    'icon' => 'random white',
    'size' => 'small',
        ), true);

echo $form->customRow($model, 'redemption_code', 'Redemption Code', $redemptionField . $button, array('hint' => 'The code / barcode displayed after the offer is redeemed. The code is used to verify the offer or activate an discount option in your POS.'));

echo $form->textAreaRow($model, 'redemption_instructions', array('class' => 'span', 'style' => 'height: 60px;', 'hint' => 'Describe how to handle the offer and what are the limits. This information is for your staff, but is also visible to member who is redeeming the offer.'));

$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->endWidget();

$this->endWidget();

cs()->registerScript('randomCode', '
$(".random-code-button").click(function(){
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var randomString = "";
    for (var i=0; i<8; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomString += chars.substring(rnum,rnum+1);
    }
    $("#Offer_redemption_code").val(randomString);
        
});    
');
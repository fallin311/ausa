<?php

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'branches-form',
    'htmlOptions' => array('class' => 'no-foot'),
    'enableAjaxValidation' => false,
        ));

// list of all available branches
$_branches = $vendor->getLimitedBranches();
$branchesForm = array();
$htmlOptions = array();

if (count($model->participating_branches) == 0){
    $all = array();
    foreach ($_branches as $branch){
        //[] = $branch->branch_id;
        $all[] = $branch->branch_id;
    }
    $model->participating_branches = $all;
}

$htmlOptions['multiple'] = 'multiple';
foreach ($_branches as $branch){
    $branchesForm[$branch->branch_id] = $branch->branch_name . ', ' . $branch->branch_address;
}

cs()->registerCss('select2-css',
    'ul.select2-choices {
        width:348px;
        margin-bottom: 10px !important;
    }'
    .'ul.select2-choices li.select2-search-field input.select2-input {
        height: 20px;
        padding: 4px 6px;
        margin-bottom: 0px;
    }'
    .'div.select2-drop { margin-top: -12px !important; border-color: #CCC; }'
    .'.select2-container-multi .select2-choices .select2-search-choice {
        margin-right: 5px;
    }');
echo '<div class="form-row" style="width: 100%;">'
    .'<label for="Vendor_industry" class="required">Participating Branches </label>'
    .'<a class="ausa-toolTip" rel="tooltip" data-original-title="Which branches would you like members to be able to redeem this offer at?"></a>'
    .'<div class="form-element"><div class="clearfix">';
$this->widget('ext.select2.ESelect2', array(
    'model' => $model,
    'attribute' => 'participating_branches',
    'data' => $branchesForm,
    'options' => array('placeholder' => 'Select Participating Branches'),
    'htmlOptions' => $htmlOptions
));
echo '</div></div></div>';
//echo $form->checkBoxListRow($model, 'participating_branches', $branchesForm, $htmlOptions);

$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->endWidget();

$this->endWidget();
?>

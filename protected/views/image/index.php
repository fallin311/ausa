<?php

$this->layout = '//layouts/full';
$breadcrumbs = array(
    'My Dashboard' => app()->user->getHomeUrl(),
    'Images'
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));

$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => false));
$this->beginWidget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Images'));

$this->widget('ausa.widgets.AOButtonGroup', array(
    'type' => '',
    'buttons' => array(
        array('icon' => 'cog', 'items' => array(
                array('label' => 'Create Image', 'icon' => 'plus', 'url' => url('image/create')),
                '---',
                array('label' => 'Get Help', 'icon' => 'question-sign', 'url' => '#'),
        )),
    ),
));

$this->endWidget();

$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $dataProvider,
    //'template'=>"{items}",
    'columns' => array(
        //array('name' => 'vendor_id', 'header' => '#'),
        array('header' => 'Image', 'type'=>'raw', 
            'value' => '"<a href=\"" . $data->getUrl() . "\" target=\"_blank\"><img src=\"" . $data->getUrl(' . Image::SIZE_PREVIEW_W . ',' . Image::SIZE_PREVIEW_H . ') . "\" alt=\"preview\" /></a>"'),
        array ('header' => 'Info', 'type' => 'raw', 'value' => '$data->file_name . "<br />" . $data->resolution . " - " . floor($data->file_size / 1024) . "KB"'),
        array('name' => 'updated_on', 'header' => 'Last Modified'),
        array('name' => 'category', 'header' => 'Category'),
        array('name' => 'vendor_id', 'header' => 'Vendor', 'value' => '$data->vendor_id ? $data->vendor->vendor_name : ""'),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}{delete}',
        ),
    ),
));


$this->endWidget();
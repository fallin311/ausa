<?php

$this->layout = '//layouts/full';
$breadcrumbs = array(
    'My Dashboard' => app()->user->getHomeUrl(),
    'Images' => url('image'),
    'Update an Image'
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));

$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
$this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Update Image'));

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'image-form',
    'enableAjaxValidation' => false,
        ));

/* if ($model->category != null && !is_int($model->category)){
  $model->category = array_search($model->category, Image::$categories);
  } */
//Shared::debug($model->category);

echo $form->dropDownListRow($model, 'category', Image::$categories, array('hint' => 'Your offers will have expiration based on this timezone related to GMT', 'empty' => 'No Category'));
echo $form->textFieldRow($model, 'vendor_id', array('class' => 'span5', 'maxlength' => 5));
echo $form->imageFieldRow($model, 'long_id', array('class' => 'span', 'imageOptions' => array(
        'layout' => 'logo-layout'
        )));

$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'success',
    'htmlOptions' => array('style' => 'margin: 7px 5px; float: right'),
    'size' => 'small',
    'icon' => 'ok white',
    'label' => $model->isNewRecord ? 'Create' : 'Update',
));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'type' => '',
    'url' => url('/image'),
    'htmlOptions' => array('style' => 'margin: 7px 5px; float: left'),
    'size' => 'small',
    'icon' => 'remove',
    'label' => 'Cancel',
));

$this->endWidget();

$this->endWidget();

$this->endWidget();
?>
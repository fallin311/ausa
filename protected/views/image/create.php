
<?php

$this->layout = '//layouts/full';
$breadcrumbs = array(
    'My Dashboard' => app()->user->getHomeUrl(),
    'Images' => url('image'),
    'Create an Image'
);

$this->widget('application.components.widgets.AOUserHeaderBar', array('breadcrumbs' => $breadcrumbs));

$this->beginWidget('application.components.widgets.AOPanel', array('multiCol' => true));
$this->widget('application.components.widgets.AOPanelHead', array('panelIcon' => 'black-icons truck', 'panelTitle' => 'Create Image'));

$form = $this->beginWidget('ausa.widgets.AusaActiveForm', array(
    'id' => 'image-form',
    'enableAjaxValidation' => false,
        ));

echo CHtml::hiddenField('Image[image_id]', '', array('id' => 'Image_image_id'));
echo $form->dropDownListRow($model, 'category', Image::$categories, array('hint' => 'Your offers will have expiration based on this timezone related to GMT', 'empty' => 'No Category'));
echo $form->textFieldRow($model, 'vendor_id', array('class' => 'span5', 'maxlength' => 5));
?>


<?php // style of Bootstrap label  ?>
<div style="color: #444444; font: normal normal 400 14px/20px Arial; height: 20px; margin: 0px 0px 5px;">
    Upload Image
</div>
<?php
$this->widget('ext.imageupload.ImageUploadWidget', array(
    'image' => $model,
    'layout' => 'compact-layout',
    'afterUpload' => '
                $("#Image_image_id").val(response["imageId"]);
                alert(response["imageId"]);
                '
));
if ($model->getError('file_content')) {
    ?>
    <p class="help-block error">
        <?php echo $model->getError('file_content') ?>
    </p>
    <?php
}
?>


<?php
$this->beginWidget('application.components.widgets.AOPanelFoot');

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'success',
    'htmlOptions' => array('style' => 'margin: 7px 5px; float: right'),
    'size' => 'small',
    'icon' => 'ok white',
    'label' => $model->isNewRecord ? 'Create' : 'Update',
));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'type' => '',
    'url' => url('/image'),
    'htmlOptions' => array('style' => 'margin: 7px 5px; float: left'),
    'size' => 'small',
    'icon' => 'remove',
    'label' => 'Cancel',
));

$this->endWidget();

$this->endWidget();

$this->endWidget();
?>
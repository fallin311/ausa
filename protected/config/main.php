<?php

/**
 * Common configuration for development / test / production environment
 * everything can be overwriteen in the specific configuration file
 */
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => TITLE,
    // preloading 'log' component
    'preload' => array('less', 'log',
        // this is for PHPUnit workaround, dont load these extensions in cli or testing mode
        php_sapi_name() !== 'cli' ? 'bootstrap' : '', php_sapi_name() !== 'cli' ? 'ausa' : ''),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.models.forms.*',
        'application.models.behaviors.*',
        'application.components.*',
        'ext.scriptboost.*',
    //'ext.bootstrap.widgets.TbActiveForm'
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'drinkbeer',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array(
                'bootstrap.gii', // since 0.9.1
            ),
        ),
    ),
    // application components
    'components' => array(
        'widgetFactory' => array(
            'widgets' => array(
                'CJuiSlider' => array(
                    'scriptUrl' => AO_URI . 'js',
                ),
            ),
        ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl' => array('login'),
            'class' => 'WebUser',
        ),
        'amazon' => array(
            'class' => 'ext.amazon.AmazonComponent'
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                'login' => 'user/login',
                'register' => 'vendor/register',
                'apiv1/new-pin' => 'api/apiV1/newPin',
                'apiv1/get-data' => 'api/apiV1/getData',
                'apiv1/sync-data' => 'api/apiV1/syncData',
                'apiv1/get-dash-banner' => 'api/apiV1/getDashBanner',
                'apiv1/<action:\w+>' => 'api/apiV1/<action>',
                'apiv2/new-pin' => 'api/apiV2/newPin',
                'apiv2/get-data' => 'api/apiV2/getData',
                'apiv2/sync-data' => 'api/apiV2/syncData',
                'apiv2/get-dash-banner' => 'api/apiV2/getDashBanner',
                'apiv2/<action:\w+>' => 'api/apiV2/<action>',
                'image/display/<img:\w+>' => 'image/display',
                'banner/dasboard/<action:\w+>' => 'dashBanner/<action>',
                'banner/dasboard/<action:\w+>/<id:\d+>' => 'dashBanner/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        /* 'db' => array(
          //box652.bluehost.com;dbname=inradius_AUSA
          //mysql:host=localhost;dbname=inradius_ausa
          //'connectionString' => 'mysql:host=yoovii.com;dbname=inradius_AUSA',
          'connectionString' => 'mysql:host=ausaoffers.net;dbname=ausa_test',
          'emulatePrepare' => false,
          //'username' => 'inradius_ausa1',
          'username' => 'ausa_dev_user',
          //'password' => 'HPLgsQ(d0,&A',
          'password' => 'phasE:saSh63',
          'charset' => 'utf8',
          'schemaCachingDuration' => 200,
          'persistent' => true,
          'enableProfiling' => true, // disable for production
          'enableParamLogging' => true, // disable for production
          ), */
        'sessionCache' => array(
            'class' => 'CMemCache',
        ),
        'session' => array(
            'class' => 'system.web.CCacheHttpSession',
            //'connectionID' => 'db',
            'cacheID' => 'sessionCache',
            'timeout' => 90000,
        ),
        /* 'session' => array(
          'class' => 'system.web.CDbHttpSession',
          'connectionID' => 'db',
          'timeout' => 90000,
          ), */
        'cache' => array(
            //'class' => 'system.caching.CFileCache',
            'class' => 'system.caching.CMemCache',
            'servers' => array(
                array('host' => '127.0.0.1', 'port' => '11211')
            )
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                /* array(
                  'class' => 'CFileLogRoute',
                  'levels' => 'trace',
                  'categories' => 'system.db',
                  'logFile' => 'sql.log'
                  ), */
                // load web log route only for web, not phpunit
                // disable this in production (api and ajax calls will not work
                /* php_sapi_name() !== 'cli' ? array(
                  'class' => 'application.extensions.pqp.PQPLogRoute',
                  'categories' => 'application.*, exception.*, system.*',
                  'levels' => 'error, warning, info',
                  ) : array('class' => 'CWebLogRoute', 'enabled' => false), */
                array('class' => 'ext.CSSlowLogRoute',
                    'logSlow' => 3, //log profile entries slower than 3 seconds (you can use decimals here)
                    'logFrequent' => 3, //log profile entries which occured more than 3 times
                    'logFile' => 'slow-db.log'
                ),
            // log all the queries to file
            /* array(
              'class'=>'CFileLogRoute',
              'categories'=>'system.db.*',
              'logFile'=>'sql.log',
              ), */
            ),
        ),
        'browser' => array(
            'class' => 'application.extensions.browser.CBrowserComponent',
        ),
        'zipcode' => array(
            // get a city from zipcode
            'class' => 'application.extensions.zipcode.ZipcodeComponent',
        ),
        'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Bootstrap',
            'responsiveCss' => true
        ),
        'ausa' => array(
            'class' => 'ext.ausa.components.ausa',
        ),
        'usaepay' => array(
            'class' => 'ext.usaepay.UsaepayComponent',
            'wsdl' => 'https://sandbox.usaepay.com/soap/gate/0AE595C1/usaepay.wsdl',
            'sourceKey' => '7l520Yt5IhYm4M1x45q9Y29f023hjc66',
            'pin' => '1234'
        ),
        'usaepaytransaction' => array(
            'class' => 'ext.usaepay.UsaepayTransactionComponent'
        ),
        'less' => array(
            'class' => 'ext.less.components.LessCompiler',
            'forceCompile' => true, // remove on live site
            'paths' => array(
                'css/system.less' => 'css/system.css',
                'css/style.less' => 'css/style.css',
                'css/' . CUSTOM_CSS . '.less' => 'css/' . CUSTOM_CSS . '.css',
                'css/aoHome.less' => 'css/aoHome.css',
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'useHttps' => false,
        'productionUrl' => 'http://localhost:8000/ausa',
        'staticUrl' => 'http://localhost:8000/ausa', // cookieless domain serving json and images
        'mobileUrl' => 'http://localhost:8000/mobile',
        'dbDateFormat' => 'Y-m-d h:m:s', // how is the stuff stored inside database
        'adminEmail' => 'support@ausaoffers.com',
        'email' => array(
            'smtpEmailLimit' => 80, // how many emails send at once
            'maxSendTries' => 3, // give up after x tries
            'smtpPort' => 465,
            'smtpHost' => 'email-smtp.us-east-1.amazonaws.com',
            'smtpUser' => 'AKIAI7FYMIOALQOJLU2Q',
            'smtpPassword' => 'AgORs7UdMsDF8+yaiHVGFSQFo/3tSGdo+8wMrNSryulJ',
            'campaignSender' => 'support@ausaoffers.com', // default amazon validated email, from which all the emails are sent from
            'noReplySender' => 'support@ausaoffers.com',
            'senderName' => 'AUSA Offers', // system emails
            'testEmail' => false // when set on, all the emails will be sent from and to developers email address
        ),
        // logo will be resized to this size
        'maxLogoSize' => array(
            'width' => 350,
            'height' => 200,
        ),
        'thumbSize' => array(
            'width' => 200,
            'height' => 150,
        ),
        // image is automatically resized to this size
        'maxImageSize' => array(
            'width' => 1024,
            'height' => 800,
        ),
    ),
);
?>

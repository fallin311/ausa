<?php

return CMap::mergeArray(
                require(dirname(__FILE__) . '/local_development.php'), array(
            'components' => array(
                'fixture' => array(
                    'class' => 'system.test.CDbFixtureManager',
                ),
                /* uncomment the following to provide test database connection
                  'db'=>array(
                  'connectionString'=>'DSN for test database',
                  ),
                 */
                'log' => array(
                    'class' => 'CLogRouter',
                    'routes' => array(
                        array(
                            'class' => 'CFileLogRoute',
                            'levels' => 'error, warning',
                            'logFile' => 'test.log'
                        ),
                    ),
                ),
                'less' => array(
                    'class' => 'ext.less.components.LessCompiler',
                    'forceCompile' => false, // remove on live site
                    'paths' => array(
                        'css/system.less' => 'css/system.css',
                        'css/style.less' => 'css/style.css',
                        'css/custom.less' => 'css/custom.css',
                        'css/aoHome.less' => 'css/aoHome.css',
                    ),
                ),
            ),
                )
);

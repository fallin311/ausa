<?php

/**
 * This configuration is run on the windows development machine with full GUI
 */
return CMap::mergeArray(
                require(dirname(__FILE__) . '/main.php'), array(
            'components' => array(
                'db' => array(
                    //box652.bluehost.com;dbname=inradius_AUSA
                    //mysql:host=localhost;dbname=inradius_ausa
                    //'connectionString' => 'mysql:host=yoovii.com;dbname=inradius_AUSA',
                    'connectionString' => 'mysql:host=ausaoffers.net;dbname=ausa_dev',
                    'emulatePrepare' => true,
                    //'username' => 'inradius_ausa1',
                    'username' => 'ausa_dev_user',
                    //'password' => 'HPLgsQ(d0,&A',
                    'password' => 'phasE:saSh63',
                    'charset' => 'utf8',
                    'schemaCachingDuration' => 200,
                    'autoConnect' => true,
                    'persistent' => false,
                    'enableProfiling' => true, // disable for production
                    'enableParamLogging' => true, // disable for production
                ),
                // enable on linode only
                /*'assetManager' => array(
                    'class' => 'CAssetManager',
                    'baseUrl' => 'http://ausaoffers.net/assets'
                ),*/
                'log' => array(
                    'class' => 'CLogRouter',
                    'routes' => array(
                        array(
                            'class' => 'CFileLogRoute',
                            'levels' => 'error, warning',
                        ),
                        // load web log route only for web, not phpunit
                        // disable this in production (api and ajax calls will not work
                        /* php_sapi_name() !== 'cli' ? array(
                          'class' => 'application.extensions.pqp.PQPLogRoute',
                          'categories' => 'application.*, exception.*, system.*',
                          'levels' => 'error, warning, info',
                          ) : array('class' => 'CWebLogRoute', 'enabled' => false), */
                        array('class' => 'ext.CSSlowLogRoute',
                            'logSlow' => 3, //log profile entries slower than 3 seconds (you can use decimals here)
                            'logFrequent' => 3, //log profile entries which occured more than 3 times
                            'logFile' => 'slow-db.log'
                        ),
                        // log all the queries to file
                        array(
                            'class' => 'CFileLogRoute',
                            'categories' => 'system.db.*',
                            'logFile' => 'sql.log',
                        ),
                    ),
                ),
            ),
                )
);
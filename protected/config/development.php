<?php

/**
 * This configuration is run on the windows development machine with full GUI
 */
return CMap::mergeArray(
    require(dirname(__FILE__) . '/main.php'), array(

        'components' => array(
            'db' => array(
                //box652.bluehost.com;dbname=inradius_AUSA
                //mysql:host=localhost;dbname=inradius_ausa
                //'connectionString' => 'mysql:host=yoovii.com;dbname=inradius_AUSA',
                'connectionString' => 'mysql:host=ausaoffers.net;dbname=ausa_dev',
                'emulatePrepare' => false,
                //'username' => 'inradius_ausa1',
                'username' => 'ausa_dev_user',
                //'password' => 'HPLgsQ(d0,&A',
                'password' => 'phasE:saSh63',
                'charset' => 'utf8',
                'schemaCachingDuration' => 200,
                'persistent' => true,
                'enableProfiling' => false, // disable for production
                'enableParamLogging' => false, // disable for production
            ),
        ),
    )
);
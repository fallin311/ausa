<?php

/**
 * This configuration is run on the linode beta.ausaoffers.net
 */
return CMap::mergeArray(
                require(dirname(__FILE__) . '/main.php'), array(
            'components' => array(
                'db' => array(
                    'connectionString' => 'mysql:host=ausaoffers.net;dbname=ausa_dev',
                    'emulatePrepare' => true,
                    //'username' => 'inradius_ausa1',
                    'username' => 'ausa_dev_user',
                    //'password' => 'HPLgsQ(d0,&A',
                    'password' => 'phasE:saSh63',
                    'charset' => 'utf8',
                    'schemaCachingDuration' => 200,
                    'persistent' => false,
                    'enableProfiling' => false, // disable for production
                    'enableParamLogging' => false, // disable for production
                ),
                'cache' => array(
                    'class' => 'system.caching.CMemCache',
                    'servers' => array(
                        array('host' => '127.0.0.1', 'port' => '11211')
                    )
                ),
            ),
            'params' => array(
                'useHttps' => false,
             'productionUrl' => 'http://beta.ausaoffers.com',
             'staticUrl' => 'http://beta.ausaoffers.com', // cookieless domain serving json and images
             'mobileUrl' => 'http://mobile.ausaoffers.com',
             ) 
           )
);

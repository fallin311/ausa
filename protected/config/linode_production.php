<?php

/**
 * This configuration is run on the windows development machine with full GUI
 */
return CMap::mergeArray(
    require(dirname(__FILE__) . '/main.php'), array(
        'components' => array(
            'db' => array(
                'connectionString' => 'mysql:host=127.0.0.1;unix_socket=/tmp/mysql.sock;dbname=ausa',
                'emulatePrepare' => true, // make sure this one is true on Linux
                'username' => 'ausa_user',
                'password' => 'CoRE#saILS76',
                'charset' => 'utf8',
                'schemaCachingDuration' => 601,
                'persistent' => true,
                'autoConnect' => true,
                'enableProfiling' => false, // disable for production
                'enableParamLogging' => false, // disable for production
            ),
            'cache' => array(
                'class' => 'system.caching.CMemCache',
                'servers' => array(
                    array('host' => '127.0.0.1', 'port' => '11211')
                )
            ),
            /*'assetManager' => array(
                'class' => 'CAssetManager',
                'baseUrl' => 'http://ausaoffers.com/assets'
            ),*/
        ),
        'params' => array(
            'useHttps' => true,
            'productionUrl' => 'http://ausaoffers.com',
            'staticUrl' => 'http://ausaoffers.net', // cookieless domain serving json and images
            'mobileUrl' => 'http://mobile.ausaoffers.com',
        )
    )
);
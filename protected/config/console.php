<?php

/**
 * This configuration is run on the linux production machine using console
 * command. See file/protected/yiic.php to change between production and development
 */
defined('AO_TEST') or define('AO_TEST', 0);
defined('AO_URI') or define('AO_URI', '/');

$mainConfig = require(dirname(__FILE__) . '/main.php');

return CMap::mergeArray(
                $mainConfig, array(
            'components' => array(
                'db' => array(
                    'connectionString' => 'mysql:host=127.0.0.1;unix_socket=/tmp/mysql.sock;dbname=ausa',
                    'emulatePrepare' => true, // make sure this one is true on Linux
                    'username' => 'ausa_user',
                    'password' => 'CoRE#saILS76',
                    'charset' => 'utf8',
                    'schemaCachingDuration' => 600,
                    'persistent' => true,
                    'autoConnect' => true,
                    'enableProfiling' => false, // disable for production
                    'enableParamLogging' => false, // disable for production
                ),
                'less' => array(
                    'class' => 'ext.less.components.LessCompiler',
                    'basePath' => dirname(__FILE__) . '/../..',
                    'forceCompile' => false, // remove on live site
                    'paths' => array(
                        'css/system.less' => 'css/system.css',
                        'css/style.less' => 'css/style.css',
                        'css/custom.less' => 'css/custom.css',
                        'css/aoHome.less' => 'css/aoHome.css',
                    ),
                ),
            ),
                )
);

<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Ausa Offers',
    // preloading 'log' component
    'preload' => array('less', 'log',
        // this is for PHPUnit workaround, dont load these extensions in cli or testing mode
        php_sapi_name() !== 'cli' ? 'bootstrap' : '', php_sapi_name() !== 'cli' ? 'ausa' : ''),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.scriptboost.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        /*'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'drinkbeer',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array(
                'bootstrap.gii', // since 0.9.1
            ),
        ),*/
    ),
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl' => array('login'),
            'class' => 'WebUser',
        ),
        // uncomment the following to enable URLs in path-format

        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                'login'=>'user/login',
                'apiv1/new-pin' => 'api/apiV1/newPin',
                'apiv1/get-data' => 'api/apiV1/getData',
                'apiv1/sync-data' => 'api/apiV1/syncData',
                'apiv1/<action:\w+>' => 'api/apiV1/<action>',
                'image/display/<img:\w+>' => 'image/display',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'db' => array(
            //box652.bluehost.com;dbname=inradius_AUSA
            //mysql:host=localhost;dbname=inradius_ausa
            //'connectionString' => 'mysql:host=yoovii.com;dbname=inradius_AUSA',
            'connectionString' => 'mysql:host=ausaoffers.net;dbname=ausa_test',
            'emulatePrepare' => true,
            //'username' => 'inradius_ausa1',
            'username' => 'ausa_dev_user',
            //'password' => 'HPLgsQ(d0,&A',
            'password' => 'phasE:saSh63',
            'charset' => 'utf8',
            'schemaCachingDuration' => 200,
            'persistent' => true,
            'enableProfiling' => false, // disable for production
            'enableParamLogging' => false, // disable for production
        ),
        'session' => array(
            'class' => 'system.web.CDbHttpSession',
            'connectionID' => 'db',
            'timeout' => 900000,
        ),
        'cache' => array(
            // 'class' => 'system.caching.CFileCache',
	    'class' => 'system.caching.CMemCache',
            'servers' => array(
                array('host' => '127.0.0.1', 'port' => '11211')
            )
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // load web log route only for web, not phpunit
                // disable this in production (api and ajax calls will not work
                /*php_sapi_name() !== 'cli' ? array(
                    'class' => 'application.extensions.pqp.PQPLogRoute',
                    'categories' => 'application.*, exception.*, system.*',
                    'levels' => 'error, warning, info',
                        ) : array('class' => 'CWebLogRoute', 'enabled' => false),*/
                array('class' => 'ext.CSSlowLogRoute',
                    'logSlow' => 3, //log profile entries slower than 3 seconds (you can use decimals here)
                    'logFrequent' => 3, //log profile entries which occured more than 3 times
                    'logFile' => 'slow-db.log'
                ),
            ),
        ),
        'browser' => array(
            'class' => 'application.extensions.browser.CBrowserComponent',
        ),
        'zipcode' => array(
            // get a city from zipcode
            'class' => 'application.extensions.zipcode.ZipcodeComponent',
        ),
        'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Bootstrap',
        ),
        'ausa' => array(
            'class' => 'ext.ausa.components.ausa',
        ),
        'less' => array(
            'class' => 'ext.less.components.LessCompiler',
            'forceCompile' => false, // indicates whether to force compiling
            'paths' => array(
                'css/system.less' => 'css/system.css',
                'css/style.less' => 'css/style.css',
                'css/custom.less' => 'css/custom.css',
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'productionUrl' => 'http://localhost/ausa',
        'staticUrl' => 'http://localhost/ausa', // cookieless domain serving json and images
        'dbDateFormat' => 'Y-m-d h:m:s', // how is the stuff stored inside database
        'email' => array(
            'smtpEmailLimit' => 80, // how many emails send at once
            'maxSendTries' => 3, // give up after x tries
            'smtpPort' => 465,
            'smtpHost' => 'email-smtp.us-east-1.amazonaws.com',
            'smtpUser' => 'AKIAJ6M5GSZZ4LJULVYA',
            'smtpPassword' => 'Ajaj7EhQ98VF99c8jVd8Vc4WyJ9n2a8IXrbvJd2GaQNi',
            'campaignSender' => 'marketing@yoovii.com', // default amazon validated email, from which all the emails are sent from
            'yooviiSender' => 'no-reply@yoovii.com',
            'yooviiSenderName' => 'Yoovii', // system emails
            'testEmail' => false // when set on, all the emails will be sent from and to developers email address
        ),
        // logo will be resized to this size
        'maxLogoSize' => array(
            'width' => 350,
            'height' => 200,
        ),
        'thumbSize' => array(
            'width' => 200,
            'height' => 150,
        ),
        // image is automatically resized to this size
        'maxImageSize' => array(
            'width' => 1024,
            'height' => 800,
        ),
    ),
);

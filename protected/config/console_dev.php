<?php

/**
 * This configuration is run on the windows development machine for console
 * commands
 */
defined('AO_TEST') or define('AO_TEST', 1);
defined('AO_URI') or define('AO_URI', '/ausa/');

$mainConfig = require(dirname(__FILE__) . '/main.php');

return CMap::mergeArray(
                $mainConfig, array(
            'components' => array(
                'db' => array(
                    'connectionString' => 'mysql:host=ausaoffers.net;dbname=ausa_dev',
                    'emulatePrepare' => true,
                    'username' => 'ausa_dev_user',
                    'password' => 'phasE:saSh63',
                    'charset' => 'utf8',
                    'schemaCachingDuration' => 200,
                    'autoConnect' => true,
                    'persistent' => false,
                    'enableProfiling' => true, // disable for production
                    'enableParamLogging' => true, // disable for production
                ),
                'less' => array(
                    'class' => 'ext.less.components.LessCompiler',
                    'basePath' => dirname(__FILE__) . '/../',
                    'forceCompile' => false, // remove on live site
                    'paths' => array(
                        'css/system.less' => 'css/system.css',
                        'css/style.less' => 'css/style.css',
                        'css/custom.less' => 'css/custom.css',
                        'css/aoHome.less' => 'css/aoHome.css',
                    ),
                ),
            ),
                )
);

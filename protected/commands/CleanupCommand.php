<?php

/**
 * Delete unused images and old sessions from the database
 */
class CleanupCommand extends CConsoleCommand {

    function run($args) {
        // possible destinations:
        // banner - panorama_image
        // category - category_icon
        // chapter - logo
        // offer - offer_icon
        // offer_template - offer_image
        // sales_team - logo
        // vendor - logo
        
        // also, do not delete images with category set

        $chunkSize = 30;
        $count = Yii::app()->db->createCommand("SELECT COUNT(*) FROM image WHERE category IS NULL AND updated_on < DATE_SUB(NOW(), INTERVAL 1 DAY)")->queryScalar();
        $itterations = (int)($count / $chunkSize);
        //Shared::debug("we have $count images and $itterations itterations");
        for ($i = 0; $i <= $itterations; $i++){
            $images = Image::model()->findAllBySql("SELECT image_id, long_id FROM image WHERE category IS NULL AND updated_on < DATE_SUB(NOW(), INTERVAL 1 DAY) LIMIT " . $i * $chunkSize . "," . $chunkSize);
            $this->processBatch($images);
            //Shared::debug("processing batch " . $i * $chunkSize . "," . $chunkSize);
        }
        
    }

    function processBatch($images) {
        $imageFound = false;
        foreach ($images as $image){
            $dest = Banner::model()->findByAttributes(array('panorama_image' => $image->long_id));
            if ($dest != null){
                $imageFound = true;
            }
            
            $dest = Category::model()->findByAttributes(array('category_icon' => $image->long_id));
            if ($dest != null){
                $imageFound = true;
            }
            
            $dest = Chapter::model()->findByAttributes(array('logo' => $image->long_id));
            if ($dest != null){
                $imageFound = true;
            }
            
            $dest = Offer::model()->findByAttributes(array('offer_icon' => $image->long_id));
            if ($dest != null){
                $imageFound = true;
            }
            
            $dest = OfferTemplate::model()->findByAttributes(array('offer_image' => $image->long_id));
            if ($dest != null){
                $imageFound = true;
            }
            
            $dest = SalesTeam::model()->findByAttributes(array('logo' => $image->long_id));
            if ($dest != null){
                $imageFound = true;
            }
            
            $dest = Vendor::model()->findByAttributes(array('logo' => $image->long_id));
            if ($dest != null){
                $imageFound = true;
            }
            
            if (!$imageFound){
                Shared::debug("Deleting image {$image->image_id}");
                if ($image->delete()){
                    Shared::debug("image deleted");
                }else{
                    Shared::debug("cannot delete the image");
                }
            }
        }
    }
}

?>

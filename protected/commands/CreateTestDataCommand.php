<?php

require_once(dirname(__FILE__) . '/../helpers/shortcuts.php');

/**
 * Use a test database to create fake vendors and offers to simulate the application
 * under a load.
 * This command will delete content of the database, so make sure you are really running
 * in in a test environment.
 */
class CreateTestDataCommand extends CConsoleCommand {

// these vendors will be created accross all the chapters
    public $vendorCount = 40;

    /**
     * Lookup a chapter inside database and its location on a map
     * @var type 
     */
    public $chapterIds = array(
        array(
// el paso
            'id' => 1,
            'model' => null, // load on first use
            'minLat' => 31.719734,
            'maxLat' => 31.801646,
            'minLon' => -106.569486,
            'maxLon' => -106.33491
        ),
        array(
// Fort Bragg
            'id' => 194, // load on first use
            'model' => null, // load on first use
            'minLat' => 34.964185,
            'maxLat' => 35.152197,
            'minLon' => -79.035988,
            'maxLon' => -78.874969
        ),
        array(
// Las Cruces
            'id' => 195, // load on first use
            'model' => null, // load on first use
            'minLat' => 32.288874,
            'maxLat' => 32.38779,
            'minLon' => -106.793633,
            'maxLon' => -106.733208
        ),
        array(
// Fort Hood
            'id' => 208,
            'model' => null, // load on first use
            'minLat' => 31.133489,
            'maxLat' => 31.133489,
            'minLon' => -97.798233,
            'maxLon' => -97.632065
        )
    );
// first names used in a generator
    public $fistNames = array(
        'Ethan', 'Olivia',
        'Noah', 'Sophia',
        'Mason', 'Ava',
        'Jacob', 'Isabella',
        'Jack', 'Mia',
        'Aiden', 'Ella',
        'Jackson', 'Emily',
        'Logan', 'Lily',
        'Lucas', 'My',
        'Benjamin', 'Chloe',
        'William', 'Madison',
        'Ryan', 'Abigail',
        'Jayden', 'Amelia',
        'James', 'Charlotte',
        'Alexander', 'Avery',
        'Michael', 'Addison',
        'Owen', 'Harper',
        'Elijah', 'Grace',
        'Matthew', 'Hannah',
        'Joshua', 'Sofia',
        'Luke', 'Zoe',
        'Gabriel', 'Zoey',
        'Carter', 'Sophie',
        'Daniel', 'Aubrey',
        'Dylan', 'Natalie',
        'Nathan', 'Elizabeth',
        'Caleb', 'Brooklyn',
        'Oliver', 'Lucy',
        'Henry', 'Claire',
        'Andrew', 'Evelyn',
        'Gavin', 'Audrey',
        'Evan', 'Anna',
        'Landon', 'Layla',
        'Max', 'Lillian',
        'Eli', 'Samantha',
        'Samuel', 'Ellie',
        'Connor', 'Maya',
        'Isaac', 'Stella',
        'Tyler', 'Hailey',
        'Saul', 'Victor',
        'Travis', 'Ondrej'
    );
// list of 1000 most popular last names

    public $lastNames = array(
        'SMITH', 'JOHNSON', 'WILLIAMS', 'JONES', 'BROWN', 'DAVIS', 'MILLER', 'WILSON', 'MOORE', 'TAYLOR', 'ANDERSON', 'THOMAS', 'JACKSON', 'WHITE', 'HARRIS', 'MARTIN', 'THOMPSON', 'GARCIA', 'MARTINEZ', 'ROBINSON', 'CLARK', 'RODRIGUEZ', 'LEWIS', 'LEE', 'WALKER', 'HALL', 'ALLEN', 'YOUNG', 'HERNANDEZ', 'KING', 'WRIGHT', 'LOPEZ', 'HILL', 'SCOTT', 'GREEN', 'ADAMS', 'BAKER', 'GONZALEZ', 'NELSON', 'CARTER', 'MITCHELL', 'PEREZ', 'ROBERTS', 'TURNER', 'PHILLIPS', 'CAMPBELL', 'PARKER', 'EVANS', 'EDWARDS', 'COLLINS', 'STEWART', 'SANCHEZ', 'MORRIS', 'ROGERS', 'REED', 'COOK', 'MORGAN', 'BELL', 'MURPHY', 'BAILEY', 'RIVERA', 'COOPER', 'RICHARDSON', 'COX', 'HOWARD', 'WARD', 'TORRES', 'PETERSON', 'GRAY', 'RAMIREZ', 'JAMES', 'WATSON', 'BROOKS', 'KELLY', 'SANDERS', 'PRICE', 'BENNETT', 'WOOD', 'BARNES', 'ROSS', 'HENDERSON', 'COLEMAN', 'JENKINS', 'PERRY', 'POWELL', 'LONG', 'PATTERSON', 'HUGHES', 'FLORES', 'WASHINGTON', 'BUTLER', 'SIMMONS', 'FOSTER', 'GONZALES', 'BRYANT', 'ALEXANDER', 'RUSSELL', 'GRIFFIN', 'DIAZ', 'HAYES', 'MYERS', 'FORD', 'HAMILTON', 'GRAHAM', 'SULLIVAN', 'WALLACE', 'WOODS', 'COLE', 'WEST', 'JORDAN', 'OWENS', 'REYNOLDS', 'FISHER', 'ELLIS', 'HARRISON', 'GIBSON', 'MCDONALD', 'CRUZ', 'MARSHALL', 'ORTIZ', 'GOMEZ', 'MURRAY', 'FREEMAN', 'WELLS', 'WEBB', 'SIMPSON', 'STEVENS', 'TUCKER', 'PORTER', 'HUNTER', 'HICKS', 'CRAWFORD', 'HENRY', 'BOYD', 'MASON', 'MORALES', 'KENNEDY', 'WARREN', 'DIXON', 'RAMOS', 'REYES', 'BURNS', 'GORDON', 'SHAW', 'HOLMES', 'RICE', 'ROBERTSON', 'HUNT', 'BLACK', 'DANIELS', 'PALMER', 'MILLS', 'NICHOLS', 'GRANT', 'KNIGHT', 'FERGUSON', 'ROSE', 'STONE', 'HAWKINS', 'DUNN', 'PERKINS', 'HUDSON', 'SPENCER', 'GARDNER', 'STEPHENS', 'PAYNE', 'PIERCE', 'BERRY', 'MATTHEWS', 'ARNOLD', 'WAGNER', 'WILLIS', 'RAY', 'WATKINS', 'OLSON', 'CARROLL', 'DUNCAN', 'SNYDER', 'HART', 'CUNNINGHAM', 'BRADLEY', 'LANE', 'ANDREWS', 'RUIZ', 'HARPER', 'FOX', 'RILEY', 'ARMSTRONG', 'CARPENTER', 'WEAVER', 'GREENE', 'LAWRENCE', 'ELLIOTT', 'CHAVEZ', 'SIMS', 'AUSTIN', 'PETERS', 'KELLEY', 'FRANKLIN', 'LAWSON', 'FIELDS', 'GUTIERREZ', 'RYAN', 'SCHMIDT', 'CARR', 'VASQUEZ', 'CASTILLO', 'WHEELER', 'CHAPMAN', 'OLIVER', 'MONTGOMERY', 'RICHARDS', 'WILLIAMSON', 'JOHNSTON', 'BANKS', 'MEYER', 'BISHOP', 'MCCOY', 'HOWELL', 'ALVAREZ', 'MORRISON', 'HANSEN', 'FERNANDEZ', 'GARZA', 'HARVEY', 'LITTLE', 'BURTON', 'STANLEY', 'NGUYEN', 'GEORGE', 'JACOBS', 'REID', 'KIM', 'FULLER', 'LYNCH', 'DEAN', 'GILBERT', 'GARRETT', 'ROMERO', 'WELCH', 'LARSON', 'FRAZIER', 'BURKE', 'HANSON', 'DAY', 'MENDOZA', 'MORENO', 'BOWMAN', 'MEDINA', 'FOWLER', 'BREWER', 'HOFFMAN', 'CARLSON', 'SILVA', 'PEARSON', 'HOLLAND', 'DOUGLAS', 'FLEMING', 'JENSEN', 'VARGAS', 'BYRD', 'DAVIDSON', 'HOPKINS', 'MAY', 'TERRY', 'HERRERA', 'WADE', 'SOTO', 'WALTERS', 'CURTIS', 'NEAL', 'CALDWELL', 'LOWE', 'JENNINGS', 'BARNETT', 'GRAVES', 'JIMENEZ', 'HORTON', 'SHELTON', 'BARRETT', 'OBRIEN', 'CASTRO', 'SUTTON', 'GREGORY', 'MCKINNEY', 'LUCAS', 'MILES', 'CRAIG', 'RODRIQUEZ', 'CHAMBERS', 'HOLT', 'LAMBERT', 'FLETCHER', 'WATTS', 'BATES', 'HALE', 'RHODES', 'PENA', 'BECK', 'NEWMAN', 'HAYNES', 'MCDANIEL', 'MENDEZ', 'BUSH', 'VAUGHN', 'PARKS', 'DAWSON', 'SANTIAGO', 'NORRIS', 'HARDY', 'LOVE', 'STEELE', 'CURRY', 'POWERS', 'SCHULTZ', 'BARKER', 'GUZMAN', 'PAGE', 'MUNOZ', 'BALL', 'KELLER', 'CHANDLER', 'WEBER', 'LEONARD', 'WALSH', 'LYONS', 'RAMSEY', 'WOLFE', 'SCHNEIDER', 'MULLINS', 'BENSON', 'SHARP', 'BOWEN', 'DANIEL', 'BARBER', 'CUMMINGS', 'HINES', 'BALDWIN', 'GRIFFITH', 'VALDEZ', 'HUBBARD', 'SALAZAR', 'REEVES', 'WARNER', 'STEVENSON', 'BURGESS', 'SANTOS', 'TATE', 'CROSS', 'GARNER', 'MANN', 'MACK', 'MOSS', 'THORNTON', 'DENNIS', 'MCGEE', 'FARMER', 'DELGADO', 'AGUILAR', 'VEGA', 'GLOVER', 'MANNING', 'COHEN', 'HARMON', 'RODGERS', 'ROBBINS', 'NEWTON', 'TODD', 'BLAIR', 'HIGGINS', 'INGRAM', 'REESE', 'CANNON', 'STRICKLAND', 'TOWNSEND', 'POTTER', 'GOODWIN', 'WALTON', 'ROWE', 'HAMPTON', 'ORTEGA', 'PATTON', 'SWANSON', 'JOSEPH', 'FRANCIS', 'GOODMAN', 'MALDONADO', 'YATES', 'BECKER', 'ERICKSON', 'HODGES', 'RIOS', 'CONNER', 'ADKINS', 'WEBSTER', 'NORMAN', 'MALONE', 'HAMMOND', 'FLOWERS', 'COBB', 'MOODY', 'QUINN', 'BLAKE', 'MAXWELL', 'POPE', 'FLOYD', 'OSBORNE', 'PAUL', 'MCCARTHY', 'GUERRERO', 'LINDSEY', 'ESTRADA', 'SANDOVAL', 'GIBBS', 'TYLER', 'GROSS', 'FITZGERALD', 'STOKES', 'DOYLE', 'SHERMAN', 'SAUNDERS', 'WISE', 'COLON', 'GILL', 'ALVARADO', 'GREER', 'PADILLA', 'SIMON', 'WATERS', 'NUNEZ', 'BALLARD', 'SCHWARTZ', 'MCBRIDE', 'HOUSTON', 'CHRISTENSEN', 'KLEIN', 'PRATT', 'BRIGGS', 'PARSONS', 'MCLAUGHLIN', 'ZIMMERMAN', 'FRENCH', 'BUCHANAN', 'MORAN', 'COPELAND', 'ROY', 'PITTMAN', 'BRADY', 'MCCORMICK', 'HOLLOWAY', 'BROCK', 'POOLE', 'FRANK', 'LOGAN', 'OWEN', 'BASS', 'MARSH', 'DRAKE', 'WONG', 'JEFFERSON', 'PARK', 'MORTON', 'ABBOTT', 'SPARKS', 'PATRICK', 'NORTON', 'HUFF', 'CLAYTON', 'MASSEY', 'LLOYD', 'FIGUEROA', 'CARSON', 'BOWERS', 'ROBERSON', 'BARTON', 'TRAN', 'LAMB', 'HARRINGTON', 'CASEY', 'BOONE', 'CORTEZ', 'CLARKE', 'MATHIS', 'SINGLETON', 'WILKINS', 'CAIN', 'BRYAN', 'UNDERWOOD', 'HOGAN', 'MCKENZIE', 'COLLIER', 'LUNA', 'PHELPS', 'MCGUIRE', 'ALLISON', 'BRIDGES', 'WILKERSON', 'NASH', 'SUMMERS', 'ATKINS', 'WILCOX', 'PITTS', 'CONLEY', 'MARQUEZ', 'BURNETT', 'RICHARD', 'COCHRAN', 'CHASE', 'DAVENPORT', 'HOOD', 'GATES', 'CLAY', 'AYALA', 'SAWYER', 'ROMAN', 'VAZQUEZ', 'DICKERSON', 'HODGE', 'ACOSTA', 'FLYNN', 'ESPINOZA', 'NICHOLSON', 'MONROE', 'WOLF', 'MORROW', 'KIRK', 'RANDALL', 'ANTHONY', 'WHITAKER', 'OCONNOR', 'SKINNER', 'WARE', 'MOLINA', 'KIRBY', 'HUFFMAN', 'BRADFORD', 'CHARLES', 'GILMORE', 'DOMINGUEZ', 'ONEAL', 'BRUCE', 'LANG', 'COMBS', 'KRAMER', 'HEATH', 'HANCOCK', 'GALLAGHER', 'GAINES', 'SHAFFER', 'SHORT', 'WIGGINS', 'MATHEWS', 'MCCLAIN', 'FISCHER', 'WALL', 'SMALL', 'MELTON', 'HENSLEY', 'BOND', 'DYER', 'CAMERON', 'GRIMES', 'CONTRERAS', 'CHRISTIAN', 'WYATT', 'BAXTER', 'SNOW', 'MOSLEY', 'SHEPHERD', 'LARSEN', 'HOOVER', 'BEASLEY', 'GLENN', 'PETERSEN', 'WHITEHEAD', 'MEYERS', 'KEITH', 'GARRISON', 'VINCENT', 'SHIELDS', 'HORN', 'SAVAGE', 'OLSEN', 'SCHROEDER', 'HARTMAN', 'WOODARD', 'MUELLER', 'KEMP', 'DELEON', 'BOOTH', 'PATEL', 'CALHOUN', 'WILEY', 'EATON', 'CLINE', 'NAVARRO', 'HARRELL', 'LESTER', 'HUMPHREY', 'PARRISH', 'DURAN', 'HUTCHINSON', 'HESS', 'DORSEY', 'BULLOCK', 'ROBLES', 'BEARD', 'DALTON', 'AVILA', 'VANCE', 'RICH', 'BLACKWELL', 'YORK', 'JOHNS', 'BLANKENSHIP', 'TREVINO', 'SALINAS', 'CAMPOS', 'PRUITT', 'MOSES', 'CALLAHAN', 'GOLDEN', 'MONTOYA', 'HARDIN', 'GUERRA', 'MCDOWELL', 'CAREY', 'STAFFORD', 'GALLEGOS', 'HENSON', 'WILKINSON', 'BOOKER', 'MERRITT', 'MIRANDA', 'ATKINSON', 'ORR', 'DECKER', 'HOBBS', 'PRESTON', 'TANNER', 'KNOX', 'PACHECO', 'STEPHENSON', 'GLASS', 'ROJAS', 'SERRANO', 'MARKS', 'HICKMAN', 'ENGLISH', 'SWEENEY', 'STRONG', 'PRINCE', 'MCCLURE', 'CONWAY', 'WALTER', 'ROTH', 'MAYNARD', 'FARRELL', 'LOWERY', 'HURST', 'NIXON', 'WEISS', 'TRUJILLO', 'ELLISON', 'SLOAN', 'JUAREZ', 'WINTERS', 'MCLEAN', 'RANDOLPH', 'LEON', 'BOYER', 'VILLARREAL', 'MCCALL', 'GENTRY', 'CARRILLO', 'KENT', 'AYERS', 'LARA', 'SHANNON', 'SEXTON', 'PACE', 'HULL', 'LEBLANC', 'BROWNING', 'VELASQUEZ', 'LEACH', 'CHANG', 'HOUSE', 'SELLERS', 'HERRING', 'NOBLE', 'FOLEY', 'BARTLETT', 'MERCADO', 'LANDRY', 'DURHAM', 'WALLS', 'BARR', 'MCKEE', 'BAUER', 'RIVERS', 'EVERETT', 'BRADSHAW', 'PUGH', 'VELEZ', 'RUSH', 'ESTES', 'DODSON', 'MORSE', 'SHEPPARD', 'WEEKS', 'CAMACHO', 'BEAN', 'BARRON', 'LIVINGSTON', 'MIDDLETON', 'SPEARS', 'BRANCH', 'BLEVINS', 'CHEN', 'KERR', 'MCCONNELL', 'HATFIELD', 'HARDING', 'ASHLEY', 'SOLIS', 'HERMAN', 'FROST', 'GILES', 'BLACKBURN', 'WILLIAM', 'PENNINGTON', 'WOODWARD', 'FINLEY', 'MCINTOSH', 'KOCH', 'BEST', 'SOLOMON', 'MCCULLOUGH', 'DUDLEY', 'NOLAN', 'BLANCHARD', 'RIVAS', 'BRENNAN', 'MEJIA', 'KANE', 'BENTON', 'JOYCE', 'BUCKLEY', 'HALEY', 'VALENTINE', 'MADDOX', 'RUSSO', 'MCKNIGHT', 'BUCK', 'MOON', 'MCMILLAN', 'CROSBY', 'BERG', 'DOTSON', 'MAYS', 'ROACH', 'CHURCH', 'CHAN', 'RICHMOND', 'MEADOWS', 'FAULKNER', 'ONEILL', 'KNAPP', 'KLINE', 'BARRY', 'OCHOA', 'JACOBSON', 'GAY', 'AVERY', 'HENDRICKS', 'HORNE', 'SHEPARD', 'HEBERT', 'CHERRY', 'CARDENAS', 'MCINTYRE', 'WHITNEY', 'WALLER', 'HOLMAN', 'DONALDSON', 'CANTU', 'TERRELL', 'MORIN', 'GILLESPIE', 'FUENTES', 'TILLMAN', 'SANFORD', 'BENTLEY', 'PECK', 'KEY', 'SALAS', 'ROLLINS', 'GAMBLE', 'DICKSON', 'BATTLE', 'SANTANA', 'CABRERA', 'CERVANTES', 'HOWE', 'HINTON', 'HURLEY', 'SPENCE', 'ZAMORA', 'YANG', 'MCNEIL', 'SUAREZ', 'CASE', 'PETTY', 'GOULD', 'MCFARLAND', 'SAMPSON', 'CARVER', 'BRAY', 'ROSARIO', 'MACDONALD', 'STOUT', 'HESTER', 'MELENDEZ', 'DILLON', 'FARLEY', 'HOPPER', 'GALLOWAY', 'POTTS', 'BERNARD', 'JOYNER', 'STEIN', 'AGUIRRE', 'OSBORN', 'MERCER', 'BENDER', 'FRANCO', 'ROWLAND', 'SYKES', 'BENJAMIN', 'TRAVIS', 'PICKETT', 'CRANE', 'SEARS', 'MAYO', 'DUNLAP', 'HAYDEN', 'WILDER', 'MCKAY', 'COFFEY', 'MCCARTY', 'EWING', 'COOLEY', 'VAUGHAN', 'BONNER', 'COTTON', 'HOLDER', 'STARK', 'FERRELL', 'CANTRELL', 'FULTON', 'LYNN', 'LOTT', 'CALDERON', 'ROSA', 'POLLARD', 'HOOPER', 'BURCH', 'MULLEN', 'FRY', 'RIDDLE', 'LEVY', 'DAVID', 'DUKE', 'ODONNELL', 'GUY', 'MICHAEL', 'BRITT', 'FREDERICK', 'DAUGHERTY', 'BERGER', 'DILLARD', 'ALSTON', 'JARVIS', 'FRYE', 'RIGGS', 'CHANEY', 'ODOM', 'DUFFY', 'FITZPATRICK', 'VALENZUELA', 'MERRILL', 'MAYER', 'ALFORD', 'MCPHERSON', 'ACEVEDO', 'DONOVAN', 'BARRERA', 'ALBERT', 'COTE', 'REILLY', 'COMPTON', 'RAYMOND', 'MOONEY', 'MCGOWAN', 'CRAFT', 'CLEVELAND', 'CLEMONS', 'WYNN', 'NIELSEN', 'BAIRD', 'STANTON', 'SNIDER', 'ROSALES', 'BRIGHT', 'WITT', 'STUART', 'HAYS', 'HOLDEN', 'RUTLEDGE', 'KINNEY', 'CLEMENTS', 'CASTANEDA', 'SLATER', 'HAHN', 'EMERSON', 'CONRAD', 'BURKS', 'DELANEY', 'PATE', 'LANCASTER', 'SWEET', 'JUSTICE', 'TYSON', 'SHARPE', 'WHITFIELD', 'TALLEY', 'MACIAS', 'IRWIN', 'BURRIS', 'RATLIFF', 'MCCRAY', 'MADDEN', 'KAUFMAN', 'BEACH', 'GOFF', 'CASH', 'BOLTON', 'MCFADDEN', 'LEVINE', 'GOOD', 'BYERS', 'KIRKLAND', 'KIDD', 'WORKMAN', 'CARNEY', 'DALE', 'MCLEOD', 'HOLCOMB', 'ENGLAND', 'FINCH', 'HEAD', 'BURT', 'HENDRIX', 'SOSA', 'HANEY', 'FRANKS', 'SARGENT', 'NIEVES', 'DOWNS', 'RASMUSSEN', 'BIRD', 'HEWITT', 'LINDSAY', 'LE', 'FOREMAN', 'VALENCIA', 'ONEIL', 'DELACRUZ', 'VINSON', 'DEJESUS', 'HYDE', 'FORBES', 'GILLIAM', 'GUTHRIE', 'WOOTEN', 'HUBER', 'BARLOW', 'BOYLE', 'MCMAHON', 'BUCKNER', 'ROCHA', 'PUCKETT', 'LANGLEY', 'KNOWLES', 'COOKE', 'VELAZQUEZ', 'WHITLEY', 'NOEL', 'VANG'
    );
// rather existing domains we own
    public $emailDomains = array(
        'ausaoffers.net',
        'inradiussystems.com',
        'miaconcepts.com'
    );
    public $banners = array(
        'U5B0NrhPHJeWF',
        'gWgKb8f027cqr',
        '6nZHEBU43iUwk',
        'CvqXdG3dJhZHA',
        'kBxWcHGpamcLL'
    );

    /** associative array of category models */
    public $categories;
    // used to generate random text
    public $wordSource = 'Adult Aeroplane Air Aircraft Carrier Airforce Airport Album Alphabet Apple Arm Army Baby Baby Backpack Balloon Banana Bank Barbecue Bathroom Bathtub Bed Bed Bee Bible Bible Bird Bomb Book Boss Bottle Bowl Box Boy Brain Bridge Butterfly Button Cappuccino Car Car-race Carpet Carrot Cave Chair Chess Board Chief Child Chisel Chocolates Church Church Circle Circus Circus Clock Clown Coffee Coffee-shop Comet Compact Disc Compass Computer Crystal Cup Cycle Data Base Desk Diamond Dress Drill Drink Drum Dung Ears Earth Egg Electricity Elephant Eraser Explosive Eyes Family Fan Feather Festival Film Finger Fire Floodlight Flower Foot Fork Freeway Fruit Fungus Game Garden Gas Gate Gemstone Girl Gloves God Grapes Guitar Hammer Hat Hieroglyph Highway Horoscope Horse Hose Ice Ice-cream Insect Jet fighter Junk Kaleidoscope Kitchen Knife Leather jacket Leg Library Liquid Magnet Man Map Maze Meat Meteor Microscope Milk Milkshake Mist Money $$$$ Monster Mosquito Mouth Nail Navy Necklace Needle Onion PaintBrush Pants Parachute Passport Pebble Pendulum Pepper Perfume Pillow Plane Planet Pocket Post-office Potato Printer Prison Pyramid Radar Rainbow Record Restaurant Rifle Ring Robot Rock Rocket Roof Room Rope Saddle Salt Sandpaper Sandwich Satellite School Sex Ship Shoes Shop Shower Signature Skeleton Slave Snail Software Solid Space Shuttle Spectrum Sphere Spice Spiral Spoon Sports-car Spot Light Square Staircase Star Stomach Sun Sunglasses Surveyor Swimming Pool Sword Table Tapestry Teeth Telescope Television Tennis racquet Thermometer Tiger Toilet Tongue Torch Torpedo Train Treadmill Triangle Tunnel Typewriter Umbrella Vacuum Vampire Videotape Vulture Water Weapon Web Wheelchair Window Woman Worm X-ray';
    public $words;
    public $prefixes = array('Mr.', 'Ms.', 'Capt.', 'LT', 'Dr.', 'Gen.', 'Col.', 'Sir', 'Bro', 'Maj');

    /** assoc array chapter => array(stId1 => array(model, array(ids of salesman), stId2) */
    public $salesTeams = array();

    /** images we can use for vendors and offers */
    public $smallImages = array();

    /**
     * Innitiate the whole circus
     * @param type $args
     */
    public function run($args) {
// check we use the test database - important!
        if (!strstr(Yii::app()->db->connectionString, 'ausa_test')) {
            echo "Please configure this command with test database. Modify console.php configuration file and use ausa_test database connetion string.";
            exit(1);
        }

        Yii::import('application.models.behaviors.UpdateOfferBehavior');

        $this->words = explode(" ", $this->wordSource);

// Test stuff
//$this->addressGenerator();
        //$newDate = $this->addMonths(time(), -3);
        //echo Shared::toDatabase($newDate);
        //$this->clearDatabase();
        //$name = $this->nameGenerator();
        //$email = $this->emailAddressGenerator($name);
        //echo $name[0] . ' ' . $name[1] . ' - ' . $email;
        //$chapter = $this->getChapter();
        /* Shared::debug($chapter);
          $member = $this->createMember($chapter['model']);
          if ($member === false){
          echo "could not create the member";
          }else {
          echo "member created";
          }
          Shared::debug($member); */

        //$vendor = $this->createVendor($chapter);
        //$offer = $this->createOffer($vendor);
        // let the magic happen
        $this->clearDatabase();
        for ($i = 0; $i < 5; $i++) {
            $chapter = $this->getChapter();
            // 500 vendors in total
            for ($j = 0; $j < 3; $j++) {
                $vendor = $this->createVendor($chapter);
                if ($vendor != false) {
                    // create 5 offers, some of them will be valid
                    for ($k = 0; $k < 5; $k++) {
                        try {
                            $this->createOffer($vendor);
                        } catch (Exception $exc) {
                            echo $exc->getTraceAsString();
                        }
                    }
                }
            }
        }

        // create some members for the chapter
        for ($j = 0; $j < 500; $j++) {
            try {
                $chapter = $this->getChapter();
                $this->createMember($chapter['model']);
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        }
    }

    function getChapter($i = null) {
        if ($i == null || !is_numeric($i)) {
            $i = rand(0, count($this->chapterIds) - 1);
        }

        if ($this->chapterIds[$i]['model'] == null) {
            $this->chapterIds[$i]['model'] = Chapter::model()->findByPk($this->chapterIds[$i]['id']);
        }
        return $this->chapterIds[$i];
    }

    /**
     * Guess a random address from given chapter
     */
    function addressGenerator($chapterId = null, $maxTries = 5) {
        $googleApiUrl = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=';
        $precission = 1000000;
        $chapter = $this->getChapter($chapterId);

        $output = array();
        if ($maxTries == 0)
            return $output;

// load random location inside el paso
        $lat = rand($chapter['minLat'] * $precission, $chapter['maxLat'] * $precission) / $precission;
        $lon = rand($chapter['minLon'] * $precission, $chapter['maxLon'] * $precission) / $precission;

        $url = $googleApiUrl . $lat . ',' . $lon . "&sensor=false";

        $source = file_get_contents($url);
        $obj = json_decode($source);

        $found = false;
// did it find something?
        if (count($obj->results)) {
            $found = true;
// found at least one
            if (strlen($obj->results[0]->formatted_address)) {
                $output['formatted'] = $obj->results[0]->formatted_address;
            } else {
// not found, we will have to get new one
                $found = false;
            }
            $output['lat'] = $obj->results[0]->geometry->location->lat;
            $output['lon'] = $obj->results[0]->geometry->location->lng;

// get the zipcode
            foreach ($obj->results[0]->address_components as $component) {
                if (in_array('postal_code', $component->types)) {
                    $output['zipcode'] = $component->short_name;
                }
            }
            if (!isset($output['zipcode'])) {
// zipcode not found, get new one
                $found = false;
            }
        }

        if (!$found) {
            return ($this->addressGenerator($chapterId, $maxTries - 1));
        }
        return $output;
    }

    /**
     * Create random date in given interval.
     * Default is from now to one year in the future.
     * @param Date $startDate timestamp format of start date
     * @param Date $endDate timestamp format of end date
     */
    function dateGenerator($startDate = null, $endDate = null) {
        if ($startDate == null) {
            $startDate = time();
        }
        if ($endDate == null) {
            $endDate = strtotime(Shared::toDatabase($startDate) . " +12 month");
        }
        return rand($startDate, $endDate);
    }

    /**
     * Date helper adding number of months to given date
     * @param type $date
     * @param type $months
     * @return type
     */
    function addMonths($date, $months = 1) {
        return strtotime(Shared::toDatabase($date) . ' ' . ($months >= 0 ? '+' . $months : $months) . " month");
    }

    /**
     * There are some images uploaded in the test database. Make sure the range match
     */
    function getSmallImageId() {
        if ($this->smallImages == null) {
            $this->smallImages = array();
            $images = Image::model()->findAllBySQL('SELECT image_id, long_id FROM image WHERE image_id > 147 AND image_id < 162');
            foreach ($images as $image) {
                $this->smallImages[] = $image->long_id;
            }
        }
        return $this->smallImages[rand(0, count($this->smallImages) - 1)];
    }

    function getBannerImageId() {
        return $this->banners[rand(0, count($this->banners) - 1)];
    }

    /**
     * Date helper adding / substracting number of days to given date
     * @param type $date
     * @param type $days
     * @return type
     */
    function addDays($date, $days = 1) {
        return strtotime(Shared::toDatabase($date) . ' ' . ($days >= 0 ? '+' . $days : $days) . " day");
    }

    function clearDatabase() {
        // We cannot truncate tables referenced by forign key
        // possible solution is here
        // http://stackoverflow.com/questions/253849/cannot-truncate-table-because-it-is-being-referenced-by-a-foreign-key-constraint

        $db = Yii::app()->db;

        $db->createCommand("SET FOREIGN_KEY_CHECKS=0")->query();

        $db->createCommand("TRUNCATE TABLE membership")->query();

        $db->createCommand("TRUNCATE TABLE favorite_store")->query();

        $db->createCommand("TRUNCATE TABLE member")->query();

        $db->createCommand("TRUNCATE TABLE offer_category")->query();

        $db->createCommand("TRUNCATE TABLE offer_click")->query();

        $db->createCommand("TRUNCATE TABLE redemption")->query();

        $db->createCommand("TRUNCATE TABLE offer")->query();

        $db->createCommand("TRUNCATE TABLE branch")->query();

        $db->createCommand("TRUNCATE TABLE banner")->query();

        $db->createCommand("TRUNCATE TABLE vendor")->query();

        // delete all the users except admin
        $db->createCommand("DELETE FROM user WHERE user_id > 1")->query();

        $db->createCommand("SET FOREIGN_KEY_CHECKS=1")->query();
    }

    /**
     * Create a person name. Returns array [firstName, lastName]
     */
    public function nameGenerator() {
        $i = rand(0, count($this->fistNames) - 1);
        $j = rand(0, count($this->lastNames) - 1);
        return array($this->fistNames[$i], ucfirst(strtolower($this->lastNames[$j])));
    }

    public function companyNameGenerator() {
        $name = '';
        $count = rand(1, 3);
        for ($i = 0; $i < $count; $i++) {
            $name .= " " . $this->words[rand(0, count($this->words) - 1)];
        }
        return $name;
    }

    public function textGenerator() {
        $text = $this->words[rand(0, count($this->words) - 1)];
        $count = rand(10, 25);
        for ($i = 0; $i < $count; $i++) {
            $text .= " " . strtolower($this->words[rand(0, count($this->words) - 1)]);
        }
        return $text . '.';
    }

    public function emailAddressGenerator($name) {
        $num = rand(0, 999);
        $email = strtolower($name[0]{0}) . strtolower($name[1]) . $num;
        $email .= '@';
        $i = rand(0, count($this->emailDomains) - 1);
        return $email . $this->emailDomains[$i];
    }

    public function phoneNumberGenerator() {
        $number = '';
        $num = rand(111, 999);
        $number .= $num;
        $num = rand(100, 999);
        $number .= $num;
        $num = rand(1000, 9999);
        $number .= $num;
        return $number;
    }

    /**
     * returns random category from the databse
     */
    public function getCategory() {
        if ($this->categories == null) {
            $this->categories = Category::model()->published()->findAll();
        }
        return $this->categories[rand(0, count($this->categories) - 1)];
    }

    /**
     * Returns true with given probability
     * @param type $percentage
     */
    public function probability($percentage) {
        $p = rand(0, 100);
        if ($p < $percentage) {
            return true;
        }
        return false;
    }

    public function createMember($chapter) {
        $name = $this->nameGenerator();
        $email = $this->emailAddressGenerator($name);
        $phone = $this->phoneNumberGenerator();


        // lookup first to avoid confilcts
        $existing = Member::findByWhatever($email, $phone);
        if ($existing != null) {
            return false;
        }

        $attributes = array(
            'first_name' => $name[0],
            'last_name' => $name[1],
            'email_address' => $this->probability(95) ? $email : null,
            'phone_number' => $this->probability(90) ? $phone : null,
            'access_pin' => rand(10000, 99999),
            'source' => 'test',
            'prefix' => $this->probability(80) ? $this->prefixes[rand(0, count($this->prefixes) - 1)] : null,
        );

        // create member
        $member = Member::createModel('Member', $attributes);
        if (!$member->save()) {
            Shared::debug($member->getErrors());
            return false;
        }
        $join = $this->dateGenerator($this->addMonths(time(), -16), time());

        // make some members expired
        $expiration = $this->dateGenerator($this->addMonths(time(), -6), $this->addMonths(time(), 32));

        if (!is_object($member) || !is_object($chapter)) {
            Shared::debug($member);
            Shared::debug($chapter);
            return false;
        }
        // create membership
        $membership = Membership::create($member, $chapter);

        $membership->expiration_date = Shared::toDatabase($expiration);
        $membership->join_date = Shared::toDatabase($join);

        if (!$membership->save()) {
            Shared::debug($membership->getErrors());
            return false;
        }
        return $member;
    }

    function createVendor($chapter) {
        $vendorName = $this->companyNameGenerator();
        $user = $this->nameGenerator();
        $email = $this->emailAddressGenerator($user);
        $description = $this->textGenerator();
        $address = $this->addressGenerator($chapter);
        if (!isset($address['formatted'])) {
            return false;
        }

        $category = $this->getCategory();
        // some will be expired already
        $expiration = Shared::toDatabase($this->dateGenerator($this->addMonths(time(), -3), $this->addMonths(time(), 12)));

        $website = null;
        if ($this->probability(70)) {
            $website = 'http://' . Shared::urlize($vendorName) . '.com';
        }

        // some will have banners
        $maxBanners = 0;
        $bannerExpiration = null;
        if ($this->probability(10)) {
            $bannerExpiration = $expiration;
            if ($this->probability(30)) {
                $maxBanners = 2;
            } else {
                $maxBanners = 1;
            }
        }

        // different number of offers
        $maxOffers = 1;
        if ($this->probability(10)) {
            $maxOffers = 3;
        } else if ($this->probability(30)) {
            $maxOffers = 2;
        }

        $maxBranches = rand(1, 3);
        $published = 1;

        // some might be published
        if ($this->probability(20)) {
            $published = 0;
        }

        // and very few will be disabled right away
        $disabled = 0;
        if ($this->probability(4)) {
            $disabled = 1;
        }

        $attributes = array(
            'vendor' => array(
                'vendor_name' => $vendorName,
                'address' => $address['formatted'],
                'industry' => $category->category_id,
                'logo' => $this->getSmallImageId(),
                'vendor_description' => $description,
                'chapterId' => $chapter['id'],
                'website' => $website,
                'max_branches' => $maxBranches,
                'max_offers' => $maxOffers,
                'max_banners' => $maxBanners,
                'published' => $published,
                'plan_id' => 2,
                'disabled' => $disabled,
                'expiration' => Shared::toDatabase($expiration),
                'banner_expiration' => $bannerExpiration == null ? null : Shared::toDatabase($bannerExpiration),
            ),
            'user' => array(
                'first_name' => $user[0],
                'last_name' => $user[1],
                'email_address' => $email
            )
        );

        $vendor = Vendor::create($attributes);

        if ($vendor['success']) {
            // create additional branches
            if ($maxBranches > 1) {
                for ($i = 1; $i < $maxBranches; $i++) {
                    $branchName = "Branch #" . ($i + 1);
                    $address = $this->addressGenerator($chapter);
                    $input = array(
                        'branch_address' => $address['formatted'],
                        'branch_name' => $branchName,
                        'vendor_id' => $vendor['vendor']->vendor_id,
                    );
                    $branch = Branch::create($input);
                    $branch->save();
                }
            }

            return $vendor['vendor'];
        } else {
            return false;
        }
    }

    public function createOffer($vendor) {
        if (!is_object($vendor)) {
            return false;
        }
        $model = new Offer;
        $model->attachBehavior('updateOffer', new UpdateOfferBehavior());

        $offerName = $this->companyNameGenerator();
        $description = $this->textGenerator();
        $categories = array(
            $this->getCategory()->category_id
        );

        // some offers will have more categories
        if ($this->probability(30)) {
            $categories[] = $this->getCategory()->category_id;
        }

        $startDate = Shared::toDatabase($this->dateGenerator($this->addMonths(time(), -16), $this->addMonths(time(), 12)));
        $duration = rand(1, 12);

        $globalLimit = 0;
        if ($this->probability(15)) {
            $globalLimit = rand(10, 300);
        }

        $limitUnit = 'campaign';
        if ($this->probability(15)) {
            $limitUnit = 'day';
        } else if ($this->probability(15)) {
            $limitUnit = 'month';
        }

        $published = 1;
        if ($this->probability(20)) {
            $published = 0;
        }

        $branches = array();
        foreach ($vendor->activeBranches as $branch) {
            Shared::debug("searghing vendor branches");
            if ($this->probability(70)) {
                Shared::debug("adding offer branch");
                $branches[] = $branch->branch_id;
            }
        }

        $input = array(
            'vendor_id' => $vendor->vendor_id,
            'start_date' => Shared::toDatabase($startDate),
            'user_id' => 1,
            'campaign_duration' => $duration,
            'offer_icon' => $this->getSmallImageId(),
            'offer_title' => $offerName,
            'offer_description' => $description,
            'global_limit' => $globalLimit,
            'per_capita_limit_unit' => $limitUnit,
            //'published' => $published,
            'redemption_code' => Shared::generateRandomPassword(7),
            'participating_branches' => $branches,
            'redemption_types' => array(4), // only one so far
            'redemption_instructions' => $this->textGenerator(),
            'categoriesArr' => $categories
        );

        $offer = Offer::create($input, $model);
        $offer->save();
        $offer->saveCategories();

        // some validators are strict with publishing
        if ($published) {
            $offer->published = true;
            $offer->save();
        }
    }

}

?>

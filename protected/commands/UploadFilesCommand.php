<?php

/**
 * Push data from uploaded CSV file into database
 */
class UploadFilesCommand extends CConsoleCommand {
    
    public function run($args) {
        $sql = "SELECT * FROM cron_job
ORDER BY progress LIMIT 0,1";
        $job = CronJob::model()->findBySql($sql);
        if (is_object($job)){
            $job->process();
        }
    }
}
?>


/* Customizations for AUSA base theme */
body {
    background-color: #f4e0a4;
}
.ausa-body {
    min-height: 100%;
    /*height: auto !important;*/
    height: 100%;
    margin: 0 auto;
}
.ausa-toolTip {
    height: 9px;
    width: 12px;
    background: url(../images/helpTip.gif) 0 0 no-repeat;
    display: inline-block;
    position: absolute;
    right: 4px;
    top: 25px;
    cursor: pointer;
}
span.date { white-space: pre; }
.dropdown-menu li > a:hover,
.dropdown-menu li > a:focus,
.dropdown-submenu:hover > a {
    text-decoration: none;
    color: white;
    background-color: #AE432E;
    background-color: #A6402C;
    background-image: -moz-linear-gradient(top,#AE432E,#9A3B29);
    background-image: -webkit-gradient(linear,0 0,0 100%,from(#AE432E),to(#9A3B29));
    background-image: -webkit-linear-gradient(top,#AE432E,#9A3B29);
    background-image: -o-linear-gradient(top,#AE432E,#9A3B29);
    background-image: linear-gradient(to bottom,#AE432E,#9A3B29);
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffae432e',endColorstr='#ff9a3b29',GradientType=0);
}
.ausa-userSignin {
    margin-bottom: 0px;
}
.progress.ausaProgress {
    width: 75%;
    overflow: visible;
    margin: 5px 5px 15px 5px;
    border: 1px solid #CCC;
}
.progress-warning .ui-widget-header {
    background-color: #0E90D2;
    background-image: -moz-linear-gradient(top, #149BDF, #0480BE);
    background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#149BDF), to(#0480BE));
    background-image: -webkit-linear-gradient(top, #149BDF, #0480BE);
    background-image: -o-linear-gradient(top, #149BDF, #0480BE);
    background-image: linear-gradient(to bottom, #149BDF, #0480BE);
    background-repeat: repeat-x;
    filter: progid:dximagetransform.microsoft.gradient(startColorstr='#ff149bdf', endColorstr='#ff0480be', GradientType=0);
}
.ausa-contentMainInner #recurring-fields .form-row:last-child {
    border-bottom: 1px solid #DDD;
}
.ausa-header .navbar-fixed-top .navbar-inner div.container-fluid {
    max-width: 1125px;
    max-height: 40px;
    margin: 0 auto;
}

.ausa-header .navbar-fixed-top div.navbar-inner {
    height: 40px;
    filter: none;
    background-image: url(../images/spriteMapAOA.png) !important;
    background-position: 0 0;
    background-repeat: repeat-x;
}

.ausa-header .navbar-inverse .brand {
    float: left;
    display: block;
    padding: 1px 10px 1px;
    margin: 0 10px 0 -10px;
    text-shadow: 0 1px 0 rgba(0, 0, 0, 1);
}
.ausa-header .navbar-inverse .brand img {
    float: left;
    padding-right: 15px;
}
.ausa-header .navbar-inverse .brand .bg {
    height: 40px;
    float: left;
    line-height: 40px;
    font-family: 'Kameron', Tahoma, Verdana, Arial, sans-serif;
    text-transform: uppercase;
}
.ausa-header .navbar-inverse .brand .bg .title {
    display: block;
    line-height: 27px;
    font-size: 20px;
    letter-spacing: 2px;
}
.ausa-header .navbar-inverse .brand .bg .subtitle {
    display: block;
    line-height: 13px;
    font-size: 15px;
    margin-top: -4px;
}
.ausa-header ul.ausa-mainMenu {
    margin-left: 20px;
}
.ausa-header ul.nav li + .nav-header {
    margin-top: -10px;
}
.ausa-header .navbar-inverse .nav .active > a,
.ausa-header .navbar-inverse .nav .active > a:hover,
.ausa-header .navbar-inverse .nav .active > a:focus {
    background-color: #4A627F;
    border-color: #252525 !important;
    border-width: 0px 1px 0px 1px;
    border-style: solid;
    -webkit-box-shadow: inset 0 3px 8px rgba(0, 0, 0, 0.45);
    -moz-box-shadow: inset 0 3px 8px rgba(0, 0, 0, 0.45);
    box-shadow: inset 0 3px 8px rgba(0, 0, 0, 0.45);
    text-shadow: 0 2px 2px rgba(0, 0, 0, 0.9);
}
.ausa-header .navbar-inverse .nav .dropdown-menu .active > a, {
    border-width: 0px;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    text-shadow: 0 1px 1px rgba(0, 0, 0, 0.5);
    background-color: #AE432E;
    background-color: #A6402C;
    background-image: -moz-linear-gradient(top,#AE432E,#9A3B29);
    background-image: -webkit-gradient(linear,0 0,0 100%,from(#AE432E),to(#9A3B29));
    background-image: -webkit-linear-gradient(top,#AE432E,#9A3B29);
    background-image: -o-linear-gradient(top,#AE432E,#9A3B29);
    background-image: linear-gradient(to bottom,#AE432E,#9A3B29);
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffae432e',endColorstr='#ff9a3b29',GradientType=0);
}
.ausa-header .navbar-inverse .nav li.dropdown.open > .dropdown-toggle,
.ausa-header .navbar-inverse .nav li.dropdown.active > .dropdown-toggle,
.ausa-header .navbar-inverse .nav li.dropdown.open.active > .dropdown-toggle {
    background-color: #4a627f;
    border-color: black !important;
    -webkit-box-shadow: inset 0 3px 8px rgba(0, 0, 0, 0.45);
    -moz-box-shadow: inset 0 3px 8px rgba(0,0,0,0.45);
    box-shadow: inset 0 3px 8px rgba(0, 0, 0, 0.45);
    text-shadow: 0 2px 2px rgba(0, 0, 0, 0.9);
}
.ausa-header .navbar-inverse .nav > li.active > a {
    color: #FFFFFF;
}
.ausa-header .navbar-inverse .brand,
.ausa-header .navbar-inverse .nav > li > a {
    color: #e5e5e5;
}
.ausa-header .navbar-inverse .nav li.dropdown > .dropdown-toggle .caret {
    border-top-color: #e5e5e5;
    border-bottom-color: #e5e5e5;
}
.ausa-header .navbar-inverse .nav li.dropdown > .dropdown-toggle:hover .caret {
    border-top-color: #FFFFFF;
    border-bottom-color: #FFFFFF;
}
.ausa-header ul.ausa-mainMenu li.divider-vertical {
    margin-left: -1px;
}
.ausa-header .navbar-inverse .navbar-search .search-query {
    background-color: #6A320C;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
}
.ausa-header .navbar-inverse .divider-vertical {
    margin: 0px -1px;
    border-right-color: #140902;
    border-left-color: #6c8aaf;
}
.ausa-header i.icon-admin {
    width: 25px !important;
    height: 20px;
    vertical-align: middle;
    margin: -3px 5px 0px 0px;
    background-image: url(../images/defaultUser.png);
    background-color: #fff;
    background-position: 0 0;
    background-size: 100% 100%;
    border-style: solid;
    border-color: #000000;
    border-width: 1px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px;
    -webkit-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.9);
    -moz-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.9);
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.4);
}
.ausa-header .ausa-userInfoDrop .ausa-userAvatar {
    margin-bottom: 3px;
}
.ausa-header .ausa-userInfoDrop .ausa-userAvatar img {
    width: 80px;
    height: 60px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
}
.ausa-header .ausa-userInfoDrop .ausa-userInfoDropData h4.ausa-userInfoDropDataName {
    margin-bottom: 0px;
}
.ausa-header .dropdown .dropdown-menu .nav-header input[type="text"],
.ausa-header .dropdown .dropdown-menu .nav-header input[type="password"] {
    margin: 0px 0px 10px 0px;
}
.ausa-header .dropdown .dropdown-menu .nav-header #actions {
    text-transform: none;
}
.ausa-header .dropdown .dropdown-menu .nav-header #actions button#submit-signin {
    float: left;
    width: 100px;
    margin: 0px;
}
.ausa-header .dropdown .dropdown-menu .nav-header #actions .checkbox {
    float: right;
    line-height: 30px;
    margin: 0px;
    padding: 0px;
    font-size: 13px;
}
.ausa-header .dropdown .dropdown-menu .nav-header #actions .checkbox #LoginForm_rememberMe {
    margin: 0px;
    float: none;
}
.ausa-header .dropdown .dropdown-menu .nav-header ul.signin-divider {
    margin: 0px -20px;
}
.ausa-header ul.user li.dropdown ul.dropdown-menu li.nav-header span {
    padding-left: 10px;
}
.ausa-header .ausa-userInfoDrop .ausa-userInfoDropData {
    margin-left: 10px;
}
.ausa-header .ausa-userInfoDrop .ausa-userInfoDropData h4.ausa-userInfoDropDataName {
    margin-top: 0px;
    color: #333;
    font-size: 17px;
}
.ausa-contentHeader h1 {
	position: relative;
	z-index: 3;
	float: left;
	line-height: 60px;
	margin: 0;
	text-shadow: 0 1px 0 black;
}
.ausa-contentWrapper {
	color: #fff;
        padding-top: 90px;
	/*background: white url(../images/innerShadowBottom.png) left bottom repeat-x;*/
}
.ausa-contentMain #vendor-contentDashboard .ausa-contentMain {
    margin-bottom: 40px;
}
.ausa-contentMainInner {
    background-color: #FFF;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
    -moz-box-shadow: 0 1px 4px rgba(0,0,0,0.3), 0 0 40px rgba(0,0,0,0.1) inset;
    box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
}
.ausa-contentMainInner .grid-view .ausa-grid-view-container .table td.status-center {
    text-align: center;
    vertical-align: middle;
}
.ausa-contentMainInner .grid-view .ausa-grid-view-container .table td.image-center {
    width: 90px;
    text-align: center;
    vertical-align: middle;
}
.ausa-contentMainInner .form-row > label {
    font-family: "lucida grande", tahoma, verdana, arial, sans-serif;
    font-size: 11px;
    font-weight: bold;
    line-height: 14px;
    color: #666;
    width: 110px;
    position: relative;
    float: left;
    border-right: 1px solid #EEE;
    clear: left;
    margin-bottom: -5px;
    margin-top: 5px;
}
.ausa-contentMainInner .form-row > div.form-element {
    width: auto;
    margin-left: 150px;
    padding: 15px 20px 5px 20px;
    border-left: 1px solid #EEE;
    clear: right;
}
.ausa-contentMainInner .form-row > div.form-element input,
.ausa-contentMainInner .form-row > div.form-element select,
.ausa-contentMainInner .form-row > div.form-element textarea,
.ausa-contentMainInner .form-row > div.form-element span.uneditable-input,
.ausa-contentMainInner .form-row > div.form-element label.checkbox {
    margin-bottom: 10px;
}
.ausa-contentMainInner .form-row > div.form-element label.checkbox .ausa-toolTip {
    top: 20px;
}
.ausa-contentMainInner form.form-horizontal {
    margin-bottom: 0px !important;
    padding-top: 1px;
}
.ausa-contentMainInner .form-row > label span.required {
    color: transparent;
}
.ausa-contentMainInner .grid-view div.ausa-grid-view-container {
    position: relative;
    background-color: #FFF;
}
.ausa-contentMainInner .grid-view div.ausa-grid-view-container div.ausa-grid-view-loader {
    display: none;
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: 6;
    background-color: #000000;
    background-image: url(../images/spinner.gif);
    background-repeat: no-repeat;
    background-position: 50%;
    -ms-filter: ~'"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)"';
    filter: ~'alpha(opacity=50)';
    zoom: 1;
    opacity: 0.50;
}
.ausa-contentMainInner .grid-view-loading {
    background: none;
}
.ausa-contentMainInner .grid-view-loading div.ausa-grid-view-container div.ausa-grid-view-loader {
    display: block;
}
.ausa-contentMainInner table.table {
    height: 100%;
    padding: 0px;
    border-top: #D2D2D2 1px solid;
    -webkit-box-shadow: inset 0 0 5px #DDD;
    -moz-box-shadow: inset 0 0 5px #dddddd;
    box-shadow: inset 0 0 5px #DDD;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}
.ausa-contentMainInner table.table-bordered thead:first-child tr:first-child th:first-child,
.ausa-contentMainInner table.table-bordered tbody:first-child tr:first-child td:first-child,
.ausa-contentMainInner table.table-bordered thead:first-child tr:first-child th:last-child,
.ausa-contentMainInner table.table-bordered tbody:first-child tr:first-child td:last-child {
    -webkit-border-top-left-radius: 0px;
    border-top-left-radius: 0px;
    -moz-border-radius-topleft: 0px;
}
.ausa-contentMainInner table.table-bordered thead:last-child tr:last-child th:first-child,
.ausa-contentMainInner table.table-bordered tbody:last-child tr:last-child td:first-child,
.ausa-contentMainInner table.table-bordered tfoot:last-child tr:last-child td:first-child {
    -webkit-border-bottom-left-radius: 0px;
    border-bottom-left-radius: 0px;
    -moz-border-radius-bottomleft: 0px;
}
.ausa-contentMainInner table.table-bordered {
    border-left: none;
    border-right: none;
}
.ausa-contentMainInner .table tbody tr:hover td,
.ausa-contentMainInner .table tbody tr:hover th {
    background-color: whiteSmoke;
}
.ausa-contentMainInner table.items {
    margin-bottom: 0;
}
.grid-view-loading { background-position: 31px 0px; }
.ausa-contentMainInner .pagination {
    height: 40px;
    margin: 0;
    background: #F1F1F1;
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #F1F1F1), color-stop(100%, #DBDBDB));
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f1f1f1', endColorstr='#dbdbdb', GradientType=0 );
    border-top: 1px solid #C2C2C2;
    -webkit-border-radius: 0 0 3px 3px;
    -moz-border-radius: 0 0 3px 3px;
    border-radius: 0 0 3px 3px;
    -webkit-box-shadow: inset 0 1px 0 #F9F9F9, 0 0 3px #DDD;
    -moz-box-shadow: inset 0 1px 0 #f9f9f9, 0 0 3px #ddd;
    box-shadow: inset 0 1px 0 #F9F9F9, 0 0 3px #DDD;
}
.ausa-contentMainInner .pagination ul.yiiPager {
    margin-left: 10px;
    padding-top: 8px;
}
.ausa-contentMainInner .pagination li.disabled {
    display: none;
}
.ausa-contentMainInner .pagination a {
    display: inline-block;
    padding: 0 8px;
    line-height: 22px;
    text-decoration: none;
    text-transform: uppercase;
    background: #DADADA;
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #DADADA), color-stop(100%, #BDBDBD));
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#dadada', endColorstr='#bdbdbd', GradientType=0 );
    border: 1px solid #9F9F9F;
    color: #444;
    font-size: 11px;
    font-weight: bold;
    text-shadow: 0px -1px 0px rgba(255, 255, 255, .8), 0px 1px 0px rgba(255, 255, 255, 0.2);
    -webkit-box-shadow: inset 0 1px 0 #F9F9F9, 0 0 3px #CCC;
    -moz-box-shadow: inset 0 1px 0 #f9f9f9, 0 0 3px #cccc;
    box-shadow: inset 0 1px 0 #F9F9F9, 0 0 3px #CCC;
}
.ausa-contentMainInner .pagination a:hover,
.ausa-contentMainInner .pagination .active a {
    background: white;
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, white), color-stop(100%, #E5E5E5));
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e5e5e5', GradientType=0 );
}
.ausa-contentMainInner .pagination .active a {
    color: #666;
    cursor: pointer;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight {
    height: 267px;
    padding: 20px;
    position: relative;
    zoom: 1;
    color: white;
    border-top: 1px solid #666;
    border-bottom: 1px solid #666;
    -webkit-background-size: 100% 100%;
    -moz-background-size: 100% 100%;
    -o-background-size: 100% 100%;
    background: -webkit-gradient(linear, left top, right top, from(rgba(0, 0, 0, 0.25)), to(rgba(0, 0, 0, 0.25)), color-stop(0.4, transparent), color-stop(0.6, transparent)),-webkit-gradient(linear, left top, left bottom, from(#5E6165), to(#33373A));
    background: -o-linear-gradient(left, rgba(0, 0, 0, 0.251), rgba(0, 0, 0, 0) 40%, rgba(0, 0, 0, 0) 60%, rgba(0, 0, 0, 0.251)), -o-linear-gradient(top, #5E6165, #33373A) transparent;
    background-color: rgb(51, 55, 58);
}
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight {
    -moz-border-radius: 4px 4px 0px 0px;
    -webkit-border-radius: 4px 4px 0px 0px;
    border-radius: 4px 4px 0px 0px;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight a,
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight a:hover,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight a,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight a:hover {
    color: white;
    text-decoration: none;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight::before,
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight::after,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight::before,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight::after {
    display: block;
    content: ' ';
    position: absolute;
    left: 0;
    right: 0;
    z-index: 1;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight::before,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight::before {
    top: -10px;
    height: 20px;
    -webkit-background-size: 100% 100%;
    -moz-background-size: 100% 100%;
    -o-background-size: 100% 100%;
    background-size: 100% 100%;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight::before {
    background: url(../images/dashboard-top-shadow.png) no-repeat center top;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight::after,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight::after {
    bottom: 0;
    height: 5px;
    background: url(../images/dashboard-bottom-shadow.png) no-repeat center bottom;
    -webkit-background-size: 100% 100%;
    -moz-background-size: 100% 100%;
    -o-background-size: 100% 100%;
    background-size: 100% 100%;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .header,
.ausa-contentMainInner .ausa-vendorDashboard .header {
    height: 50px;
    padding: 10px 0px 10px 10px;
    overflow: hidden;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .header img.dashboard,
.ausa-contentMainInner .ausa-vendorDashboard .header img.dashboard {
    float: left;
    height: 50px;
    width: 63px;
    -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
    -moz-box-shadow: 0 1px 4px rgba(0,0,0,0.3), 0 0 40px rgba(0,0,0,0.1) inset;
    box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .header h1,
.ausa-contentMainInner .ausa-vendorDashboard .header h1 {
    float: left;
    margin: 0px 0px 0px 10px;
    line-height: 50px;
    max-width: 1042px;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .header a.st-vendor-helpPopover {
    float: right;
    margin: 7px 10px;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .breadcrumbs ul.breadcrumbs {
    padding: 1px 15px 0px;
}
.ausa-contentMainInner .ausa-salesTeamDashboard div #map_holder,
.ausa-contentMainInner .ausa-salesTeamDashboard div #map_holder {
    color: #000000;
}
.ausa-contentMainInner .ausa-vendorDashboard div #map_holder {
    border-top: 0px;
}
#map_holder > #map_canvas {
    background-color: transparent !important;
}
.ausa-contentMainInner .ausa-vendorDashboard div #map_holder > #map_canvas > div {
    -webkit-mask-image: url(../images/map-mask.png);
    -webkit-mask-size: 1126px 307px;
    -webkit-mask-position: 0;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight .columns .stats,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight .columns .stats {
    margin-top: -12px;
    margin-bottom: -10px;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight .columns .stats > li:first-child,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight .columns .stats > li:first-child {
    border-top: 0;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight .columns .stats > li,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight .columns .stats > li {
    border-top: 1px solid #333538;
    padding: 19px 0 20px;
    font-size: 16px;
    line-height: 16px;
    -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.15);
    -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.15);
    box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.15);
    -webkit-text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.75);
    -moz-text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.75);
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.75);
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight .columns .stats > li:hover,
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight .columns .stats > li > a:hover,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight .columns .stats > li:hover,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight .columns .stats > li > a:hover {
    padding-right: 0;
    -webkit-transition: padding-right 100ms;
    -moz-transition: padding-right 100ms;
    -ms-transition: padding-right 100ms;
    -o-transition: padding-right 100ms;
    transition: padding-right 100ms;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight .columns .stats > li:hover > strong,
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight .columns .stats > li > a:hover > strong,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight .columns .stats > li:hover > strong,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight .columns .stats > li > a:hover > strong {
    margin-right: 15px;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight .columns .stats > li > a,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight .columns .stats > li > a {
    display: block;
    color: inherit;
    margin: -19px 0 -20px;
    padding: 19px 5px 20px 0;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight .columns .stats > li > strong,
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight .columns .stats > li > a > strong,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight .columns .stats > li > strong,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight .columns .stats > li > a > strong {
    -webkit-text-shadow: 0 1px 4px black;
    -moz-text-shadow: 0 1px 4px black;
    text-shadow: 0 1px 4px black;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight .columns .stats > li > strong,
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight .columns .stats > li > a > strong,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight .columns .stats > li > strong,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight .columns .stats > li > a > strong {
    font-size: 48px;
    line-height: 48px;
    height: 48px;
    margin: -7px 10px -5px 0;
}
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight .columns .stats > li > strong,
.ausa-contentMainInner .ausa-salesTeamDashboard .dashboardHighlight .columns .stats > li > a > strong,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight .columns .stats > li > strong,
.ausa-contentMainInner .ausa-vendorDashboard .dashboardHighlight .columns .stats > li > a > strong {
    display: block;
    float: left;
    font-family: "Cabin Sketch";
    font-size: 60px;
    line-height: 60px;
    height: 60px;
    font-weight: 100;
    margin: -7px 10px -5px 0;
    -webkit-text-shadow: 0 1px 4px rgba(0, 0, 0, 0.25);
    -moz-text-shadow: 0 1px 4px rgba(0, 0, 0, 0.25);
    text-shadow: 0 1px 4px rgba(0, 0, 0, 0.25);
    -webkit-transition: margin-right 100ms;
    -moz-transition: margin-right 100ms;
    -ms-transition: margin-right 100ms;
    -o-transition: margin-right 100ms;
    transition: margin-right 100ms;
}

/* text content in the ausa panels */
.ausa-contentMainInner .ao-panel-content {
    padding: 20px 30px;
}

.popover {
    z-index: 2 !important;
    -webkit-border-radius: 8px !important;
    -moz-border-radius: 8px !important;
    border-radius: 8px !important;
}
.popover .popover-title {
    color: #F4E0A4;
    background-image: url(../images/spriteMapAOA.png) !important;
    background-position: 0 0;
    background-repeat: repeat-x;
}
.popover .popover-inner .popover-content p {
    color: #333;
}
.ausa-contentMain .widget-bottom .ausa-massUpload {
    height: 40px;
}
.ausa-contentMain .widget-bottom .ausa-massUpload .qq-uploader {
    height: 40px;
}
.ausa-contentMain .widget-bottom .ausa-massUpload .qq-uploader .qq-upload-drop-area {
    min-height: 40px;
    margin: 0px;
    background-color: black;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
    filter: alpha(opacity=50);
    zoom: 1;
    opacity: 0.50;
    border-radius: 0px 0px 4px 4px;
}
.ausa-contentMain .widget-bottom .ausa-massUpload .qq-uploader .qq-upload-drop-area span {
    color: #999;
}
.ausa-contentMain .widget-bottom .ausa-massUpload .qq-uploader .qq-upload-button {
    margin: 4px 0px 0px 7px;
}
.ausa-contentMain .widget-bottom .ausa-massUpload .qq-result {
    margin-left: 192px;
    margin-top: -35px;
    height: 40px;
    line-height: 30px;
    padding: 0px;
}
.navbar-fixed-bottom a.footerLink {
    color: #FFCA09;
}
.navbar-fixed-bottom a.footerLink:hover {
    color: #FFCA09;
    text-decoration: none;
    border-bottom: 1px dotted #FFCA09;
}
.jquery-notify-bar {
	font-family: Candara,Cochin, Georgia, Times, "Times New Roman", serif;
	text-shadow: 1px 1px 2px #DDD;
	-moz-box-shadow: 0px 2px 8px #424242; /* FF3.5+ */;
	-webkit-box-shadow: 0px 2px 8px #424242; /* Saf3.0+, Chrome */;
	box-shadow: 0px 2px 8px #424242; /* Opera 10.5, IE 9.0 */;
	width: 100%;
	position: fixed;
	top: 40px;
	left: 0;
	z-index: 3;
	color: #555;
	font-size: 18px;
	text-align: center;
	padding: 20px 0px;
	font-weight: bold;
	background-color: #EBEBEB; /* FF3.6 */;
	-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorStr='#EBEBEB', EndColorStr='#CCCCCC')";
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #EBEBEB),color-stop(1, #CCCCCC));/* Saf4+, Chrome */
}
.jquery-notify-bar.error {
	color: #FFF0F0;
	text-shadow: 1px 1px 1px #BD3A3A;
	background-color: #DB4444; /* FF3.6 */;
	-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorStr='#DB4444', EndColorStr='#BD3A3A')";
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #DB4444),color-stop(1, #BD3A3A));/* Saf4+, Chrome */
}
.jquery-notify-bar.success {
	color: #fff;
	text-shadow: #509C4B 1px 1px 1px;
	background-color: #8DC96F; /* FF3.6 */;
	-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorStr='#8DC96F', EndColorStr='#509C4B')";
	background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #8DC96F),color-stop(1, #509C4B));/* Saf4+, Chrome */
}
.notify-bar-close {
	position: absolute;
	left: 95%;
	font-size: 11px;
}
.loading-icon {
    background: url(../images/loading.gif) 0 0 no-repeat;
    width: 14px;
    height: 15px;
    display: block;
    margin: auto;
}


.ausa-unpublished {
    color: #FF0000;
}

// we should change colors to yellow-brownish scheme
.ausa-info {
    border: 1px solid;
    padding: 10px 10px 10px 35px;
    margin-bottom: 30px;
}

.ausa-form-info {
    border-bottom: 1px solid;
    padding: 10px 10px 10px 35px;
    background-image: url("../images/information.png");
}

.ausa-info, .ausa-form-info {
    background-repeat: no-repeat;
    background-position: 10px 10px;
    background-color: #D8E7FA;
    border-color: #9DBFEA;
    color: #00357B;
}

p.help-block {
    margin-top: -5px !important;
    font-size: 90%;
}
#map_canvas label {
    width: auto;
    display:inline;
}
<?php

$dirname = dirname(__FILE__);
$hostname = $_SERVER['SERVER_NAME'];
$yii = $dirname . '/yii/yii.php';

// find the domain ... it might have random subdomains
// $parts = explode(".", $hostname);
if ($hostname == 'beta.ausaoffers.com') {
    // production environment for static content and phone api
    // linux nginx does not want to merge the configuration file
    $config = $dirname . '/protected/config/linode_devel.php';
    defined('AO_TEST') or define('AO_TEST', 1);
    defined('AO_URI') or define('AO_URI', '/');
    defined('CUSTOM_CSS') or define('CUSTOM_CSS', 'custom');
    defined('TITLE') or define('TITLE', 'BETA');
    $yii = $dirname . '/yii/yii.php';
} else if ($hostname == 'ausaoffers.com' || $hostname == 'ausaoffers.net') {
    // production website accessible through the browser
    $config = $dirname . '/protected/config/linode_production.php';
    defined('AO_TEST') or define('AO_TEST', 0);
    defined('AO_URI') or define('AO_URI', '/');
    defined('CUSTOM_CSS') or define('CUSTOM_CSS', 'custom');
    defined('TITLE') or define('TITLE', 'AUSA Offers');
    $yii = $dirname . '/yii/yiilite.php';
} else if ($hostname == 'localhost') {
    // production website accessible through the browser//for now it is testing for MWR..
    $config = $dirname . '/protected/config/local_development.php';
    defined('AO_TEST') or define('AO_TEST', 1);
    defined('AO_URI') or define('AO_URI', '/');
    defined('CUSTOM_CSS') or define('CUSTOM_CSS', 'customAOA');
    defined('TITLE') or define('TITLE', 'AOA Offers');
    $yii = $dirname . '/yii/yii.php';
} else {
    // this is development or testing env
    if ($hostname == 'localhost') {
        defined('AO_TEST') or define('AO_TEST', 1);
        defined('AO_URI') or define('AO_URI', '/ausa/');
        defined('CUSTOM_CSS') or define('CUSTOM_CSS', 'custom');
        defined('TITLE') or define('TITLE', 'Local Offers');
        $config = $dirname . '/protected/config/local_development.php';
    } else {
        defined('AO_TEST') or define('AO_TEST', 1);
        defined('AO_URI') or define('AO_URI', '/ausa/');
        defined('CUSTOM_CSS') or define('CUSTOM_CSS', 'custom');
        defined('TITLE') or define('TITLE', 'OFFERS TEST');
        $config = $dirname . '/protected/config/local_test.php';
    }
}

// TODO: should match client timezone
if (!date_default_timezone_get()) {
    // Set a fallback timezone if the current php.ini does not contain a default timezone setting.
    // If the environment is setup correctly, we won't override the timezone.
    date_default_timezone_set("America/Denver");
}

if (AO_TEST) {
// remove the following lines when in production mode
    defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
} else {
    // production
    defined('YII_DEBUG') or define('YII_DEBUG', false);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 1);
}

require_once($yii);
require_once(dirname(__FILE__) . '/protected/helpers/shortcuts.php');


//echo $hostname;
//echo $yii;
//echo $config;
//exit;

Yii::setPathOfAlias('bootstrap', Yii::getPathOfAlias('application.extensions.bootstrap'));
Yii::setPathOfAlias('ausa.widgets', Yii::getPathOfAlias('application.components.widgets'));

Yii::createWebApplication($config)->run();


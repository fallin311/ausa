<?php
/**
 * This is the bootstrap file for test application.
 * This file should be removed when the application is deployed for production.
 */

// change the following paths if necessary
$yii=dirname(__FILE__).'/yii/yii.php';
$config=dirname(__FILE__).'/protected/config/test.php';

// remove the following line when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
defined('AO_URI') or define('AO_URI', '/ausa/');

// TODO: should match client timezone
if (!date_default_timezone_get()) {
    // Set a fallback timezone if the current php.ini does not contain a default timezone setting.
    // If the environment is setup correctly, we won't override the timezone.
    date_default_timezone_set("America/Denver");
}

require_once($yii);
require_once(dirname(__FILE__) . '/protected/helpers/shortcuts.php');
Yii::createWebApplication($config)->run();
